package jiran.com.inappbilling;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.Constants;
import com.anjlab.android.iab.v3.SkuDetails;
import com.anjlab.android.iab.v3.TransactionDetails;

import java.util.ArrayList;

import jiran.com.inappbilling.util.IabHelper;
import jiran.com.inappbilling.util.IabResult;
import jiran.com.inappbilling.util.Inventory;
import jiran.com.inappbilling.util.Purchase;
import jiran.com.tmfilemanager.R;

public class InAppBillingActivity extends Activity implements BillingProcessor.IBillingHandler {

    static final String ITEM_SKU = "android.test.purchased";
    private static final String TAG =
            "jiran.com.inappbilling";
    public static ArrayList<SkuDetails> products;
    //static final String ITEM_SKU = "android.test.canceled";
    //static final String ITEM_SKU = "jiran.com.tmfilemanager_billing_base";
    IabHelper mHelper;
    String base64EncodedPublicKey =
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAh98dVi6zHEF2U3lMbsi5OaIvubE6l1r0HrZ6J/LnQO/KVAO5N7HLlGrFXzt9EJ+8uhY2TqQiBfUK7MwrU1v32nGLPtDfrcTwYqS9CNDqJ5VWhrlTNXPu550awakre4EKZO0jwmr9fk9ajEcE7iBUXK2WP8KDATVFyw2sK2uq0NdFuZ9hW5AUrQYwKE29VfpmqQUhRzPLCiAOJHjwd1eEPxII58hKoNqpeCKn3pUCLQKzV4ij+7sTw/Q4wqLFBZp9s7qi0EG3pwTrkdYXq5G32am7PqE3fus2beO/6/c1qJEWi0tOED97Cve/1qSH8YtWEd+rD+k6klQKW3W8K0/FpwIDAQAB";
    Dialog mDialog;
    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener
            = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {

            if (result.isFailure()) {
                String str = "@INAPP mReceivedInventoryListener   Handle failure result.getMessage()=> " + result.getMessage() + ", result.getResponse()=> " + result.getResponse();
                Toast.makeText(InAppBillingActivity.this, str, Toast.LENGTH_SHORT).show();
                Log.d(TAG, str);
                // Handle failure
            } else {
                String str = "@INAPP mReceivedInventoryListener   Handle OK";
                Toast.makeText(InAppBillingActivity.this, str, Toast.LENGTH_SHORT).show();
                Log.d(TAG, str);
                mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU),
                        mConsumeFinishedListener);
            }
        }
    };
    private Button clickButton;
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
            new IabHelper.OnConsumeFinishedListener() {
                public void onConsumeFinished(Purchase purchase,
                                              IabResult result) {

                    if (result.isSuccess()) {
                        String str = "@INAPP mConsumeFinishedListener   Handle OK";
                        Toast.makeText(InAppBillingActivity.this, str, Toast.LENGTH_SHORT).show();
                        Log.d(TAG, str);
                        clickButton.setEnabled(true);
                    } else {
                        // handle error
                        String str = "@INAPP mConsumeFinishedListener   Handle error result.getMessage()=> " + result.getMessage() + ", result.getResponse()=> " + result.getResponse();
                        Toast.makeText(InAppBillingActivity.this, str, Toast.LENGTH_SHORT).show();
                        Log.d(TAG, str);
                    }
                }
            };
    private Button buyButton;
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result,
                                          Purchase purchase) {
            if (result.isFailure()) {
                String str = "@INAPP mPurchaseFinishedListener   Handle error result.getMessage()=> " + result.getMessage() + ", result.getResponse()=> " + result.getResponse();
                Log.d(TAG, str);
                Toast.makeText(InAppBillingActivity.this, str, Toast.LENGTH_SHORT).show();
                // Handle error
                return;
            } else if (purchase.getSku().equals(ITEM_SKU)) {
                String str = "@INAPP mPurchaseFinishedListener   Handle OK";
                Toast.makeText(InAppBillingActivity.this, str, Toast.LENGTH_SHORT).show();
                Log.d(TAG, str);
                consumeItem();
                buyButton.setEnabled(false);
            }

        }
    };
    private BillingProcessor bp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inapp_billing);
        bp = new BillingProcessor(this, base64EncodedPublicKey, this);
        //bp.initialize();

//        setContentView(R.layout.activity_inapp_billing);
//
//        LinearLayout title_left_btn = (LinearLayout) findViewById(R.id.title_left_btn);
//        title_left_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//
//        buyButton = (Button)findViewById(R.id.buyButton);
//        clickButton = (Button)findViewById(R.id.clickButton);
//        clickButton.setEnabled(false);
//
//        mHelper = new IabHelper(this, base64EncodedPublicKey);
//
//        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
//               public void onIabSetupFinished(IabResult result)
//               {
//                   if (!result.isSuccess()) {
//                       Log.d(TAG, "@INAPP In-app Billing setup failed: " +
//                               result);
//                   } else {
//                       Log.d(TAG, "@INAPP In-app Billing is set up OK");
//                   }
//               }
//           });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;

        bp.release();
    }

    public void buyClick(View view) {
        dialogSelectSKU();

//        mHelper.launchPurchaseFlow(this, ITEM_SKU, 10001,
//                mPurchaseFinishedListener, "mypurchasetoken");
    }

    public void dialogSelectSKU() {
        mDialog = new Dialog(InAppBillingActivity.this, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(InAppBillingActivity.this);
        View view = inflater.inflate(R.layout.dialog_select_sort, null, false);
        LinearLayout sort_by_name = (LinearLayout) view.findViewById(R.id.sort_by_name);
        TextView sort_by_name_txt = (TextView) view.findViewById(R.id.sort_by_name_txt);
        sort_by_name_txt.setText("android.test.purchased");
        sort_by_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                mHelper.launchPurchaseFlow(InAppBillingActivity.this, "android.test.purchased", 10001,
                        mPurchaseFinishedListener, "mypurchasetoken");
            }
        });
        LinearLayout sort_by_extension = (LinearLayout) view.findViewById(R.id.sort_by_extension);
        TextView sort_by_extension_txt = (TextView) view.findViewById(R.id.sort_by_extension_txt);
        sort_by_extension_txt.setText("android.test.canceled");
        sort_by_extension.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                mHelper.launchPurchaseFlow(InAppBillingActivity.this, "android.test.canceled", 10001,
                        mPurchaseFinishedListener, "mypurchasetoken");
            }
        });
        LinearLayout sort_by_date = (LinearLayout) view.findViewById(R.id.sort_by_date);
        TextView sort_by_date_txt = (TextView) view.findViewById(R.id.sort_by_date_txt);
        sort_by_date_txt.setText("jiran.com.tmfilemanager_billing_base");
        sort_by_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                mHelper.launchPurchaseFlow(InAppBillingActivity.this, "jiran.com.tmfilemanager_billing_base", 10001,
                        mPurchaseFinishedListener, "mypurchasetoken");
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void buttonClicked(View view) {
        clickButton.setEnabled(false);
        buyButton.setEnabled(true);
    }

    public void consumeItem() {
        Log.d(TAG, "consumeItem");
        mHelper.queryInventoryAsync(mReceivedInventoryListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        String str = "@INAPP onActivityResult   requestCode=> " + requestCode + ", resultCode=> " + resultCode;
        Toast.makeText(InAppBillingActivity.this, str, Toast.LENGTH_SHORT).show();
        Log.d(TAG, str);
        if (mHelper != null && !mHelper.handleActivityResult(requestCode,
                resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        // 구매한 아이템 정보
        SkuDetails sku = bp.getPurchaseListingDetails(productId);
        // 하트 100개 구매에 성공하였습니다! 메세지 띄우기
        String purchaseMessage = sku.title + "onProductPurchased, productId=> " + productId;
        //Common.showMessage(this, getCurrentFocus(), purchaseMessage);
        String str = purchaseMessage;
        Toast.makeText(InAppBillingActivity.this, str, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
        if (errorCode != Constants.BILLING_RESPONSE_RESULT_USER_CANCELED) {
            String errorMessage = "onBillingError (" + errorCode + "), error.toString() => " + error.toString();
            Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBillingInitialized() {
//        products = (ArrayList<SkuDetails>) bp.getPurchaseListingDetails(new InAppPurchaseItems().getIds());
//        // Sort ascending order
//        Collections.sort(products, new Comparator<SkuDetails>() {
//            @Override
//            public int compare(SkuDetails o1, SkuDetails o2) {
//                if (o1.priceLong > o2.priceLong) {
//                    return 1;
//                } else if (o1.priceLong < o2.priceLong) {
//                    return -1;
//                } else return 0;
//            }
//        });
//
//        // 결제 아이템 다이얼로그 설정
//        skusAdapter = new PurchaseHeartsAdapter(this);
//        View purchaseView = getLayoutInflater().inflate(R.layout.layout_dialog_heartstore, null);
//        ListView lvSkus = purchaseView.findViewById(R.id.lv_skus);
//        lvSkus.setAdapter(skusAdapter);
//
//        purchaseDialog = new MaterialDialog.Builder(getContext())
//                .customView(purchaseView, false)
//                .negativeText(R.string.cancel)
//                .onNegative(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                        dialog.dismiss();
//                    }
//                })
//                .build();
//
//        skusAdapter.update(products);

        mDialog = new Dialog(InAppBillingActivity.this, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(InAppBillingActivity.this);
        View view = inflater.inflate(R.layout.dialog_select_sort, null, false);
        LinearLayout sort_by_name = (LinearLayout) view.findViewById(R.id.sort_by_name);
        TextView sort_by_name_txt = (TextView) view.findViewById(R.id.sort_by_name_txt);
        sort_by_name_txt.setText("android.test.purchased");
        sort_by_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                purchaseProduct("android.test.purchased");
            }
        });
        LinearLayout sort_by_extension = (LinearLayout) view.findViewById(R.id.sort_by_extension);
        TextView sort_by_extension_txt = (TextView) view.findViewById(R.id.sort_by_extension_txt);
        sort_by_extension_txt.setText("android.test.canceled");
        sort_by_extension.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                purchaseProduct("android.test.canceled");
            }
        });
        LinearLayout sort_by_date = (LinearLayout) view.findViewById(R.id.sort_by_date);
        TextView sort_by_date_txt = (TextView) view.findViewById(R.id.sort_by_date_txt);
        sort_by_date_txt.setText("jiran.com.tmfilemanager_billing_base");
        sort_by_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                purchaseProduct("jiran.com.tmfilemanager_billing_base");
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void purchaseProduct(final String productId) {
        if (bp.isPurchased(productId)) {
            bp.consumePurchase(productId);
        }
        bp.purchase(this, productId);
    }
}
