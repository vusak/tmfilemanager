package jiran.com.flyingfile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import jiran.com.flyingfile.dialog.DialogTwoButton;
import jiran.com.flyingfile.dialog.DialogTwoButtonUtil;
import jiran.com.flyingfile.dialog.TwoButtonDialogOKCallback;
import jiran.com.flyingfile.receiver.NetworkChangeReceiver;
import jiran.com.flyingfile.util.Common;
import jiran.com.flyingfile.util.ProcessExitUtil;

@SuppressLint("NewApi")
public abstract class BaseFlyingFileFragmentActivity extends AppCompatActivity {
    public static final int REQUEST_PERMISSION_STORAGE = 10;
    public static final int REQUEST_PERMISSION_PHONESTATE = 20;
    public static final int REQUEST_PERMISSION_CONTACTS = 30;
    public static final int PERMISSION_RESULT_SUCCESS = 0;
    public static final int PERMISSION_RESULT_NEED_STORAGE = 1;
    public static final int PERMISSION_RESULT_NEED_PHONE = 2;
    public static final int PERMISSION_RESULT_NEED_CONTANCTS = 3;
    private Context context = null;
    private boolean isForeground = false;

    /**
     * 화면 생명주기 콜백
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        this.context = this;


        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub

        int nCheckPermissionResult = checkPermission();
        if (nCheckPermissionResult == PERMISSION_RESULT_SUCCESS) {
            functionLaunch();
        } else if (nCheckPermissionResult == PERMISSION_RESULT_NEED_STORAGE) {
            requestStoragePermission();
        } else if (nCheckPermissionResult == PERMISSION_RESULT_NEED_PHONE) {
            requestPhonePermission();
        } else if (nCheckPermissionResult == PERMISSION_RESULT_NEED_CONTANCTS) {
            requestContactsPermission();
        } else {

        }

        isForeground = true;
        super.onResume();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        isForeground = false;
        super.onPause();
    }

    protected void requestStoragePermission() {
        //저장 권한
        String[] permissions = {
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
        };
        ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSION_STORAGE);

    }

    protected void requestPhonePermission() {
        //전화 권한
        String[] permissions = {
                Manifest.permission.READ_PHONE_STATE
        };
        ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSION_PHONESTATE);
    }

    protected void requestContactsPermission() {
        //주소록 권한
        String[] permissions = {
                Manifest.permission.GET_ACCOUNTS
        };
        ActivityCompat.requestPermissions(this, permissions, REQUEST_PERMISSION_CONTACTS);
    }


    protected int checkPermission() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return PERMISSION_RESULT_SUCCESS;
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                ) {
            return PERMISSION_RESULT_NEED_STORAGE;
        } else {
            return PERMISSION_RESULT_SUCCESS;
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        // TODO Auto-generated method stub

        if (requestCode == REQUEST_PERMISSION_STORAGE) {
            if (grantResults != null && grantResults.length == 2 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED
                    ) {
                onPermissionResult(PERMISSION_RESULT_NEED_STORAGE, true);
            } else {
                onPermissionResult(PERMISSION_RESULT_NEED_STORAGE, false);
            }
        } else if (requestCode == REQUEST_PERMISSION_CONTACTS) {
            if (grantResults != null && grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onPermissionResult(PERMISSION_RESULT_NEED_CONTANCTS, true);
            } else {
                onPermissionResult(PERMISSION_RESULT_NEED_CONTANCTS, false);
            }
        } else if (requestCode == REQUEST_PERMISSION_PHONESTATE) {
            if (grantResults != null && grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                onPermissionResult(PERMISSION_RESULT_NEED_PHONE, true);
            } else {
                onPermissionResult(PERMISSION_RESULT_NEED_PHONE, false);
            }
        }


        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    public boolean isForeground() {
        return isForeground;
    }

    public void setForeground(boolean state) {
        this.isForeground = state;
    }

    protected void functionExit() {
        ProcessExitUtil.getInstance().processExit(context, null);
    }

    protected void functionExitDialogWhenFileTransferring() {


        TwoButtonDialogOKCallback okBtnCallback = new TwoButtonDialogOKCallback() {

            @Override
            public void onOK(DialogTwoButton dialog) {
                // TODO Auto-generated method stub
                functionExit();
            }
        };
        DialogTwoButtonUtil.getInstance().showExitFileTransferring(context, okBtnCallback, null, null);
    }

    protected void functionExitWhenLoggedInNot() {
        exit();
    }

    public void exit() {
        Common.functionExit(this);

    }

    abstract protected void onPermissionResult(int permission, boolean isSuccess);

    private void functionLaunch() {
        //받은파일함 경로 생성
        Common.getInstance().functionCreateReceivedPath(this);


        //네트워크 변경 관련 이벤트리시버인데 중복으로 동작하는것을 방지하기 위한 플래그임.
        //static 변수이기때문에 프로그램 최초 실행시 초기화를 해줘야함
        NetworkChangeReceiver.isRunning = false;
        //로그인 화면을 실행함
        functionStartActivity();
        //이 activity를 종료
        //	this.finish();
    }

    /**
     * 로그인 화면 실행
     */
    private void functionStartActivity() {
//		Intent intent = new Intent(this, this);
//		intent.putExtra("flag", getIntent().getIntExtra("flag", 0));
//		intent.putExtra("logout", getIntent().getBooleanExtra("logout", false));
//		intent.putExtra("mtom", getIntent().getBooleanExtra("mtom", false));
//		intent.putExtra("isnotify", getIntent().getBooleanExtra("isnotify", false));
//		startActivity(intent);
    }


}
