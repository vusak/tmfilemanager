package jiran.com.flyingfile.model;

public class FileIDConnectResult {
    private boolean success;
    private String host;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
