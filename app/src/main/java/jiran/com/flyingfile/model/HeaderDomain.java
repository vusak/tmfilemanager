package jiran.com.flyingfile.model;

/**
 * Created by jeon on 2016-10-15.
 */

public class HeaderDomain {
    String day;
    int contentCnt;
    boolean check;

    public HeaderDomain(int contentCnt, boolean check, String day) {
        this.contentCnt = contentCnt;
        this.check = check;
        this.day = day;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public int getContentCnt() {
        return contentCnt;
    }

    public void setContentCnt(int contentCnt) {
        this.contentCnt = contentCnt;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}