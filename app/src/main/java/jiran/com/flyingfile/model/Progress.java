package jiran.com.flyingfile.model;

public class Progress {

    private int flag;
    private int idx;
    private String target;
    private long total;
    private long progress;
    private long sendingData;


    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getProgress() {
        return progress;
    }

    public void setProgress(long progress) {
        this.progress = progress;
    }

    public long getSendingData() {
        return sendingData;
    }

    public void setSendingData(long sendingData) {
        this.sendingData = sendingData;
    }
}
