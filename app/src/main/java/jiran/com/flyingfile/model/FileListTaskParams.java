package jiran.com.flyingfile.model;


import jiran.com.flyingfile.util.LanguageUtil;

public class FileListTaskParams {

    private String url;
    private String email;
    private String myemail;
    private String fileid;
    //	private boolean isKorean;
    private LanguageUtil.Language lang;
    private int nBufsize = 0;


    public FileListTaskParams(String email, String myemail,
                              String fileid, LanguageUtil.Language lang) {
        super();
        this.email = email;
        this.myemail = myemail;
        this.fileid = fileid;
        this.lang = lang;
    }

    public int getnBufsize() {
        return nBufsize;
    }

    public void setnBufsize(int nBufsize) {
        this.nBufsize = nBufsize;
    }

    public LanguageUtil.Language getLang() {
        return lang;
    }

    public void setLang(LanguageUtil.Language lang) {
        this.lang = lang;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMyemail() {
        return myemail;
    }

    public void setMyemail(String myemail) {
        this.myemail = myemail;
    }

    public String getFileid() {
        return fileid;
    }

    public void setFileid(String fileid) {
        this.fileid = fileid;
    }

}
