package jiran.com.flyingfile.model;

import java.util.List;

public class FileListTaskResult {

    public static final int TYPE_OF_SENDER_DEVICE_WEB = 0;
    public static final int TYPE_OF_SENDER_DEVICE_PCAGENT = 1;
    public static final int TYPE_OF_SENDER_DEVICE_ANDROID = 2;
    public static final int TYPE_OF_SENDER_DEVICE_IOS = 3;


    private boolean success;
    private boolean secure;
    private String msg;
    private String host;
    private boolean realtime;
    private int senderdevicetype;
    private boolean append;
    private String ip;
    private String port;
    private List<FileListTaskItem> info;

    public int getSenderdevicetype() {
        return senderdevicetype;
    }

    public void setSenderdevicetype(int senderdevicetype) {
        this.senderdevicetype = senderdevicetype;
    }

    public boolean isAppend() {
        return append;
    }

    public void setAppend(boolean append) {
        this.append = append;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<FileListTaskItem> getInfo() {
        return info;
    }

    public void setInfo(List<FileListTaskItem> info) {
        this.info = info;
    }

    public boolean isSecure() {
        return secure;
    }

    public void setSecure(boolean secure) {
        this.secure = secure;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public boolean isRealtime() {
        return realtime;
    }

    public void setRealtime(boolean realtime) {
        this.realtime = realtime;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}
