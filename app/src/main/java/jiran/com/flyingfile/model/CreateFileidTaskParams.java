package jiran.com.flyingfile.model;


import jiran.com.flyingfile.util.LanguageUtil;

public class CreateFileidTaskParams {
    private String url;
    private String email;
    private String password;
    private boolean secure;
    //	private boolean isKorean;
    private LanguageUtil.Language lang;
    private int nBufsize = 0;


    public CreateFileidTaskParams(String url, String email, String password,
                                  boolean secure, LanguageUtil.Language lang) {
        super();
        this.url = url;
        this.email = email;
        this.password = password;
        this.secure = secure;
        this.lang = lang;
    }

    public LanguageUtil.Language getLang() {
        return lang;
    }

    public void setLang(LanguageUtil.Language lang) {
        this.lang = lang;
    }

    public int getnBufsize() {
        return nBufsize;
    }

    public void setnBufsize(int nBufsize) {
        this.nBufsize = nBufsize;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isSecure() {
        return secure;
    }

    public void setSecure(boolean secure) {
        this.secure = secure;
    }
}
