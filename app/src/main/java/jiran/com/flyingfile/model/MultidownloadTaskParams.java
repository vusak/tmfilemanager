package jiran.com.flyingfile.model;

import java.util.List;

import jiran.com.flyingfile.util.LanguageUtil;

public class MultidownloadTaskParams {

    private String url;
    private String fileid;
    private int bufsize;
    private long offset;
    private String myemail;
    private List<FileListTaskItem> itemlist;
    private String recvpath;
    private String host;
    private LanguageUtil.Language lang;
    private boolean isRealtime;
    private ExistFileItemParam existFiles = null;


    public MultidownloadTaskParams(
            String url,
            int bufsize,
            String myemail,
            List<FileListTaskItem> itemlist,
            String recvpath,
            long offset,
            String fileid,
            LanguageUtil.Language lang,
            String strHost,
            boolean isRealtime,
            ExistFileItemParam existFiles
    ) {
        super();
        this.url = url;
        this.bufsize = bufsize;
        this.myemail = myemail;
        this.itemlist = itemlist;
        this.recvpath = recvpath;
        this.offset = offset;
        this.fileid = fileid;
        this.lang = lang;
        this.host = strHost;
        this.isRealtime = isRealtime;
        this.existFiles = existFiles;
    }


    public ExistFileItemParam getExistFiles() {
        return existFiles;
    }

    public boolean isRealtime() {
        return isRealtime;
    }

    public void setRealtime(boolean isRealtime) {
        this.isRealtime = isRealtime;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public LanguageUtil.Language getLang() {
        return lang;
    }

    public void setLang(LanguageUtil.Language lang) {
        this.lang = lang;
    }

    public String getFileid() {
        return fileid;
    }

    public void setFileid(String fileid) {
        this.fileid = fileid;
    }

    public List<FileListTaskItem> getItemlist() {
        return itemlist;
    }

    public void setItemlist(List<FileListTaskItem> itemlist) {
        this.itemlist = itemlist;
    }

    public String getRecvpath() {
        return recvpath;
    }

    public void setRecvpath(String recvpath) {
        this.recvpath = recvpath;
    }

    public int getBufsize() {
        return bufsize;
    }

    public void setBufsize(int bufsize) {
        this.bufsize = bufsize;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMyemail() {
        return myemail;
    }

    public void setMyemail(String myemail) {
        this.myemail = myemail;
    }

    public long getOffset() {
        return offset;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }
}
