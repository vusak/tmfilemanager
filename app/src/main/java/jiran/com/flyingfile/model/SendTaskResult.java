package jiran.com.flyingfile.model;

import org.json.simple.JSONObject;

public class SendTaskResult {

    private JSONObject obj;

    public SendTaskResult(JSONObject obj) {
        super();
        this.obj = obj;
    }

    public JSONObject getObj() {
        return obj;
    }

    public void setObj(JSONObject obj) {
        this.obj = obj;
    }


}
