package jiran.com.flyingfile.model;

import java.util.List;
import java.util.Map;

import jiran.com.flyingfile.util.LanguageUtil;


public class SendRealtimeParams {

    private LanguageUtil.Language lang;
    private String fileid;
    private List<FileItem> filelist;
    private String email;
    private String host;
    private long totalsize;
    private int nBufsize;
    //	private List<SkipListItem> skipList;
    private Map<Integer, Long> skipList;


    public SendRealtimeParams(
            LanguageUtil.Language lang,
            String email,
            String fileid,
            List<FileItem> filelist,
            String host,
            long total
    ) {
        super();
        this.lang = lang;
        this.email = email;
        this.fileid = fileid;
        this.filelist = filelist;
        this.host = host;
        this.totalsize = total;
    }

    public Map<Integer, Long> getSkipList() {
        return skipList;
    }

    public void setSkipList(Map<Integer, Long> skipList) {
        this.skipList = skipList;
    }

    public int getnBufsize() {
        return nBufsize;
    }

    public void setnBufsize(int nBufsize) {
        this.nBufsize = nBufsize;
    }

    public long getTotalsize() {
        return totalsize;
    }


    public void setTotalsize(long totalsize) {
        this.totalsize = totalsize;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LanguageUtil.Language getLang() {
        return lang;
    }

    public void setLang(LanguageUtil.Language lang) {
        this.lang = lang;
    }

    public String getFileid() {
        return fileid;
    }

    public void setFileid(String fileid) {
        this.fileid = fileid;
    }

    public List<FileItem> getFilelist() {
        return filelist;
    }

    public void setFilelist(List<FileItem> filelist) {
        this.filelist = filelist;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }


}
