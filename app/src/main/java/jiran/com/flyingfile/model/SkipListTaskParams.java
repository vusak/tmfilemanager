package jiran.com.flyingfile.model;


import jiran.com.flyingfile.util.LanguageUtil;

public class SkipListTaskParams {

    private String fileid;
    private int bufsize;
    private LanguageUtil.Language lang;


    public LanguageUtil.Language getLang() {
        return lang;
    }

    public void setLang(LanguageUtil.Language lang) {
        this.lang = lang;
    }

    public int getBufsize() {
        return bufsize;
    }

    public void setBufsize(int bufsize) {
        this.bufsize = bufsize;
    }

    public String getFileid() {
        return fileid;
    }

    public void setFileid(String fileid) {
        this.fileid = fileid;
    }
}
