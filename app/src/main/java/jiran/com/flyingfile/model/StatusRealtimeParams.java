package jiran.com.flyingfile.model;


import jiran.com.flyingfile.util.LanguageUtil;

public class StatusRealtimeParams {

    private LanguageUtil.Language lang = null;
    private ParamFileInfo filename = null;
    private ParamFileInfo filesize = null;
    private String email = null;
    private int nBufsize = 0;
    private boolean append = false;


    public StatusRealtimeParams(
            LanguageUtil.Language lang,
            String email,
            String push,
            boolean issecure,
            ParamFileInfo filename,
            ParamFileInfo filesize) {
        super();
        this.email = email;
        this.lang = lang;
        this.filename = filename;
        this.filesize = filesize;
    }

    public boolean isAppend() {
        return append;
    }

    public void setAppend(boolean append) {
        this.append = append;
    }

    public int getnBufsize() {
        return nBufsize;
    }

    public void setnBufsize(int nBufsize) {
        this.nBufsize = nBufsize;
    }

    public LanguageUtil.Language getLang() {
        return lang;
    }

    public void setLang(LanguageUtil.Language lang) {
        this.lang = lang;
    }

    public ParamFileInfo getFilename() {
        return filename;
    }

    public void setFilename(ParamFileInfo filename) {
        this.filename = filename;
    }

    public ParamFileInfo getFilesize() {
        return filesize;
    }

    public void setFilesize(ParamFileInfo filesize) {
        this.filesize = filesize;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
