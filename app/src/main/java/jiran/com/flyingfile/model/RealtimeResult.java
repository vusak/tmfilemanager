package jiran.com.flyingfile.model;


public class RealtimeResult {

    //	private JSONObject obj = null;
    private String msg = null;
    private String fileid = null;
    private boolean success = false;
    private int port = 0;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getFileid() {
        return fileid;
    }

    public void setFileid(String fileid) {
        this.fileid = fileid;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
//	public Result(JSONObject obj) {
//		super();
//		this.obj = obj;
//	}
//
//	public JSONObject getObj() {
//		return obj;
//	}
//
//	public void setObj(JSONObject obj) {
//		this.obj = obj;
//	}
}
