package jiran.com.flyingfile.model;

import android.annotation.SuppressLint;

import java.util.HashMap;
import java.util.Map;

public class ExistFileItemParam {

    private StringBuffer buffer = null;
    private Map<Integer, Long> skipList = null;
    private boolean isResumDownload = false; //이어받기 여부

    @SuppressLint("UseSparseArrays")
    public ExistFileItemParam() {
        super();
        buffer = new StringBuffer();
        skipList = new HashMap<Integer, Long>();
    }

    public boolean isResumDownload() {
        return isResumDownload;
    }

    public void setResumDownload(boolean resumDownload) {
        isResumDownload = resumDownload;
    }

    public void addFilePointer(String strFilename, int idx, long filepointer) {
        if (buffer.length() > 0) {
            String strFormat = "|%d:%d";
            strFormat = String.format(strFormat, idx, filepointer);
            buffer.append(strFormat);
        } else {
            String strFormat = "%d:%d";
            strFormat = String.format(strFormat, idx, filepointer);
            buffer.append(strFormat);
        }

        skipList.put(idx, filepointer);
    }

    public String getParam() {
        return buffer.toString();
    }

    public Map<Integer, Long> getSkipList() {
        return skipList;
    }


    public boolean isAbleFileAppend() {
        boolean result = false;

        if (skipList.size() > 0)
            result = true;
        else
            result = false;

        return result;
    }


    public int getLengthSkipListSize() {
        return skipList.size();
    }
}
