package jiran.com.flyingfile.model;

public class FileListTaskItem {
    private String name;
    private long size;
    //	private long date;
    private String date_fmt;
    private String fileurl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    /*
    public long getDate() {
        return date;
    }
    public void setDate(long date) {
        this.date = date;
    }
    */
    public String getFileurl() {
        return fileurl;
    }

    public void setFileurl(String fileurl) {
        this.fileurl = fileurl;
    }

    public String getDate_fmt() {
        return date_fmt;
    }

    public void setDate_fmt(String date_fmt) {
        this.date_fmt = date_fmt;
    }

}
