package jiran.com.flyingfile.model;

public class CancelTaskResult {

    private boolean isSuccess;


    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

}
