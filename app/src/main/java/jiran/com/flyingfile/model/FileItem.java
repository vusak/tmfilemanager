package jiran.com.flyingfile.model;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.database.Cursor;
import android.provider.MediaStore;
import android.util.Log;

import java.io.File;
import java.util.List;

import jiran.com.flyingfile.util.FileTypeUtil;
import jiran.com.flyingfile.util.PermissionUtil;


public class FileItem extends File {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    ApplicationInfo applicationInfo;
    private boolean check;
    private boolean isDot;
    private String strFileType;
    private String strThumbnailPath = null;
    private long dateTaken;
    private int nProviderID = 0;
    private long nStoredFileLength = 0;
    private long nFileCntInFolder = 0;
    private int isFolder = -1;
    private boolean autoSkip = false;
    private boolean isPackageFolderShortCut = false;

    public FileItem(String path) {
        super(path);
        // TODO Auto-generated constructor stub
        isDot = false;
        check = false;
        strFileType = null;
    }

    @SuppressWarnings("unused")
    public static int deleteAll(Context context, FileItem item, DeletableListStruct struct) {

        if (item.isDot()) {
            if (struct != null)
                struct.nCntNotDelete++;
            return DELETE.FAIL.ordinal();
        }

        int nRes = DELETE.SUCCESS.ordinal();

        if (item.isDirectory()) {

            File[] files = item.listFiles();

            if (files.length == 0) {
                int res = item.delete(context);
                if (res == DELETE.SUCCESS.ordinal()) {
                    if (struct != null)
                        struct.nCntDelete++;
                }
//				return item.delete(context);
                return res;
            } else {
                for (int i = 0; i < files.length; i++) {
                    String strPath = files[i].getAbsolutePath();
                    FileItem innerItem = new FileItem(strPath);

                    int res = deleteAll(context, innerItem, struct);

                    if (res != FileItem.DELETE.SUCCESS.ordinal())
                        nRes = res;
                }

//				return item.delete(context);
                int res = item.delete(context);
                if (res == DELETE.SUCCESS.ordinal()) {
                    if (struct != null)
                        struct.nCntDelete++;
                }
                return res;
            }

        } else {
            //일반 파일이다.
            //0바이트이면 그냥 지워주자
            if (item.length() == 0) {
                int res = item.delete(context);
                if (res == DELETE.SUCCESS.ordinal()) {
                    if (struct != null)
                        struct.nCntDelete++;
                }
//				return item.delete(context);
                return res;
            } else {
                if (struct != null)
                    struct.nCntNotDelete++;
                return DELETE.FAIL.ordinal();
            }
        }

    }

    /**
     * 폴더 내부에 삭제 가능한 파일리스트를 전부 가져옴
     *
     * @return
     */
    public static void getDeletableList(List<FileItem> list, FileItem item, DeletableListStruct struct) {
        if (list == null)
            return;

        if (item.isDot())
            return;

        if (item.isDirectory()) {

            File[] files = item.listFiles();

            if (files.length == 0) {
                list.add(item);
            } else {
                for (int i = 0; i < files.length; i++) {
                    String strPath = files[i].getAbsolutePath();
                    FileItem innerItem = new FileItem(strPath);

                    getDeletableList(list, innerItem, struct);
                }
            }

        } else {
            //일반 파일이다.
            //0바이트이면 그냥 지워주자
            if (item.length() == 0)
                list.add(item);
            else
                struct.nCntNotDelete++;
        }
    }

    public boolean isAutoSkip() {
        return autoSkip;
    }

    public void setAutoSkip(boolean autoSKip) {
        this.autoSkip = autoSKip;
    }

    public long getDateTaken() {
        return dateTaken;
    }

    public void setDateTaken(long dateTaken) {
        this.dateTaken = dateTaken;
    }

    public boolean isPackageFolderShortCut() {
        return isPackageFolderShortCut;
    }

    public void setPackageFolderShortCut(boolean isPackageFolderShortCut) {
        this.isPackageFolderShortCut = isPackageFolderShortCut;
    }

    public ApplicationInfo getApplicationInfo() {
        return applicationInfo;
    }

    public void setApplicationInfo(ApplicationInfo applicationInfo) {
        this.applicationInfo = applicationInfo;
    }

    @Override
    public boolean isDirectory() {
        // TODO Auto-generated method stub
        if (isFolder < 0) {
            return super.isDirectory();
        } else {
            if (isFolder == 0)
                return false;
            else
                return true;
        }
    }

    public void setDirectory(boolean isDirectory) {
        if (isDirectory)
            isFolder = 1;
        else
            isFolder = 0;
    }

    public long getnFileCntInFolder() {
        return nFileCntInFolder;
    }

    public void setnFileCntInFolder(long nFileCntInFolder) {
        this.nFileCntInFolder = nFileCntInFolder;
    }

    public long getnStoredFileLength() {
        return nStoredFileLength;
    }

    public void setnStoredFileLength(long nStoredFileLength) {
        this.nStoredFileLength = nStoredFileLength;
    }

    @Override
    public String getAbsolutePath() {
        // TODO Auto-generated method stub
//		String strPath = super.getAbsolutePath();
//		strPath = strPath.replace("\n", "");
//		return strPath;
        return super.getAbsolutePath();
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        if (!isDot) {
            this.check = check;
        }
    }

    public boolean isDot() {
        return isDot;
    }

    public void setDot(boolean isDot) {
        this.isDot = isDot;
    }

    public String getStrFileType() {
        return strFileType;
    }

    public void setStrFileType(String strFileType) {
        this.strFileType = strFileType;
    }

    public String getStrThumbnailPath() {
        return strThumbnailPath;
    }

    public void setStrThumbnailPath(String strThumbnailPath) {
        this.strThumbnailPath = strThumbnailPath;
    }

	/*
	@Override
	public boolean delete() {
		// TODO Auto-generated method stub
		
		return super.delete();
	}
	*/

    public int getnProviderID() {
        return nProviderID;
    }

    public void setnProviderID(int nProviderID) {
        this.nProviderID = nProviderID;
    }

    @SuppressLint("NewApi")
    public int delete(Context context) {

//		if(!PermissionUtil.getInstance().isFileWritePermission(this.getAbsolutePath()))
        if (!PermissionUtil.getInstance().isAbleFileWritePath(this.getAbsolutePath()))
            return DELETE.RESTRICTION.ordinal();

        //미디어 스토어에서도 삭제해주자
        ContentResolver resolver = context.getContentResolver();

        FileTypeUtil fileTypeUtil = new FileTypeUtil();

        switch (fileTypeUtil.getFilyType(this)) {
            case FileTypeUtil.FLAG_PICTURE: {
                String strPath = this.getAbsolutePath();
                strPath = strPath.replace("'", "''");
                String selection = MediaStore.Images.Media.DATA + "='" + strPath + "'";
                Cursor cursor = resolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, selection, null, null);
                Log.d("FileItem", "[delete] selection : " + selection);
                //파일완전삭제 할때 여기서 null포인터 익셉션
                if (cursor != null && cursor.getCount() != 0) {
                    cursor.moveToFirst();
                    int nIndexImageID = cursor.getColumnIndex(MediaStore.Images.Media._ID);
                    int nImageID = cursor.getInt(nIndexImageID);
                    selection = MediaStore.Images.Media._ID + "=" + nImageID;
                    int nCnt = resolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, selection, null);
                    Log.d("aa", "cnt : " + nCnt);
                }
                cursor.close();
                break;
            }
            case FileTypeUtil.FLAG_MOVIE: {
                String strPath = this.getAbsolutePath();
                strPath = strPath.replace("'", "''");
                String selection = MediaStore.Images.Media.DATA + "='" + strPath + "'";

                Cursor cursor = resolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, selection, null, null);

                if (cursor != null && cursor.getCount() != 0) {
                    cursor.moveToFirst();
                    int nIndexImageID = cursor.getColumnIndex(MediaStore.Images.Media._ID);
                    int nImageID = cursor.getInt(nIndexImageID);
                    selection = MediaStore.Images.Media._ID + "=" + nImageID;
                    int nCnt = resolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, selection, null);
                    Log.d("aa", "cnt : " + nCnt);
                }
                cursor.close();
                break;
            }

            default: {

                super.delete();
                Log.d("aa", "aa");
                break;
            }
        }

        if (exists()) {
            boolean result = false;
            try {
//				result = super.delete();
//				super.deleteOnExit();

//				super.setWritable(true);
//				super.setExecutable(true);
//				super.setReadable(true);
                result = super.delete();
                Log.d("aa", "aa");

//				return true;
                if (result)
                    return DELETE.SUCCESS.ordinal();
                else
                    return DELETE.FAIL.ordinal();
            } catch (Exception e) {
//				return false;
                return DELETE.FAIL.ordinal();
//				return result;
            }
        } else {
            //파일이 존재하지 않는다면 삭제된것이다.
//			return true;
            return DELETE.SUCCESS.ordinal();
        }
    }


    /**
     * NONE : 삭제 아님.
     * FAIL : 삭제 실패
     * SUCCESS : 삭제 성공
     * RESTRICTION : 권한 없음
     * NONEMPTY : 빈폴더 아님
     */
    public enum DELETE {
        NONE, FAIL, SUCCESS, RESTRICTION, NONEMPTY
    }
}
