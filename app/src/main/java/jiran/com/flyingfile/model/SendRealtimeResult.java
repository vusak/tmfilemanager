package jiran.com.flyingfile.model;

import org.json.simple.JSONObject;

public class SendRealtimeResult {

    private JSONObject obj;

    public SendRealtimeResult(JSONObject obj) {
        super();
        this.obj = obj;
    }

    public JSONObject getObj() {
        return obj;
    }

    public void setObj(JSONObject obj) {
        this.obj = obj;
    }


}
