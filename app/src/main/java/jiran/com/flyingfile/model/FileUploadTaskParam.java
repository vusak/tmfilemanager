package jiran.com.flyingfile.model;

import java.io.File;
import java.util.List;

import jiran.com.flyingfile.util.LanguageUtil;


public class FileUploadTaskParam {

    private List<File> filelist;
    private String url;
    private String fileid;
    private LanguageUtil.Language lang;
    private int nBufsize = 0;


    public FileUploadTaskParam(List<File> filelist, String url, String fileid,
                               LanguageUtil.Language lang) {
        super();
        this.filelist = filelist;
        this.url = url;
        this.fileid = fileid;
        this.lang = lang;
    }

    public int getnBufsize() {
        return nBufsize;
    }

    public void setnBufsize(int nBufsize) {
        this.nBufsize = nBufsize;
    }

    public LanguageUtil.Language getLang() {
        return lang;
    }

    public void setLang(LanguageUtil.Language lang) {
        this.lang = lang;
    }

    public List<File> getFilelist() {
        return filelist;
    }

    public void setFilelist(List<File> filelist) {
        this.filelist = filelist;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFileid() {
        return fileid;
    }

    public void setFileid(String fileid) {
        this.fileid = fileid;
    }

}
