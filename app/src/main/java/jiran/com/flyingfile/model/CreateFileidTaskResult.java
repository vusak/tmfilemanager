package jiran.com.flyingfile.model;

public class CreateFileidTaskResult {

    private boolean success;
    private String fileid;
    //	private long date;
    private boolean secure;
    private String email;
    private String msg;
    private String date_fmt;
    private String host;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getFileid() {
        return fileid;
    }

    public void setFileid(String fileid) {
        this.fileid = fileid;
    }

    /*
    public long getDate() {
        return date;
    }
    public void setDate(long date) {
        this.date = date;
    }
    */
    public boolean isSecure() {
        return secure;
    }

    public void setSecure(boolean secure) {
        this.secure = secure;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDate_fmt() {
        return date_fmt;
    }

    public void setDate_fmt(String date_fmt) {
        this.date_fmt = date_fmt;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

}
