package jiran.com.flyingfile.model;


/**
 * 접속된 네트워크 정보를 취하는 클래스
 * NetworkInfoUtil의 단위 클래스(Domain)
 *
 * @author Bae
 */

public class LocalNetworkInfoDomain {

    private String dns1;
    private String dns2;
    private String gateway;
    private String ipAddress;
    private String leaseDuration;
    private String subnetmask;
    private String serverIP;

    public LocalNetworkInfoDomain() {
    }

    public LocalNetworkInfoDomain(String dns1, String dns2, String gateway,
                                  String ipAddress, String leaseDuration, String subnetmask,
                                  String serverIP) {
        super();
        this.dns1 = dns1;
        this.dns2 = dns2;
        this.gateway = gateway;
        this.ipAddress = ipAddress;
        this.leaseDuration = leaseDuration;
        this.subnetmask = subnetmask;
        this.serverIP = serverIP;
    }

    public String getDns1() {
        return dns1;
    }

    public void setDns1(String dns1) {
        this.dns1 = dns1;
    }

    public String getDns2() {
        return dns2;
    }

    public void setDns2(String dns2) {
        this.dns2 = dns2;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getLeaseDuration() {
        return leaseDuration;
    }

    public void setLeaseDuration(String leaseDuration) {
        this.leaseDuration = leaseDuration;
    }

    public String getSubnetmask() {
        return subnetmask;
    }

    public void setSubnetmask(String subnetmask) {
        this.subnetmask = subnetmask;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

}
