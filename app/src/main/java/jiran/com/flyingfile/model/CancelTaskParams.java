package jiran.com.flyingfile.model;


import jiran.com.flyingfile.util.LanguageUtil;

public class CancelTaskParams {

    private String fileid;
    private LanguageUtil.Language lang;


    public String getFileid() {
        return fileid;
    }

    public void setFileid(String fileid) {
        this.fileid = fileid;
    }

    public LanguageUtil.Language getLang() {
        return lang;
    }

    public void setLang(LanguageUtil.Language lang) {
        this.lang = lang;
    }
}
