package jiran.com.flyingfile.model;

import java.util.Map;

public class SkipListTaskResult {

    private boolean success;
    private Map<Integer, Long> itemList;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Map<Integer, Long> getItemList() {
        return itemList;
    }

    public void setItemList(Map<Integer, Long> itemList) {
        this.itemList = itemList;
    }
}
