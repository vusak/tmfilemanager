package jiran.com.flyingfile.model;

public class ParamFileInfo {
    private StringBuffer buffer = null;

    public ParamFileInfo() {
        buffer = new StringBuffer();
    }

    public void addString(String value) {
        buffer.append(value);
        buffer.append("|");
    }

    public void addInt(int value) {
        buffer.append(value);
        buffer.append("|");
    }

    public void addLong(long value) {
        buffer.append(value);
        buffer.append("|");
    }

    public String getListingString() {
        if (buffer == null || buffer.length() == 0)
            return null;
        else {
            String result = buffer.toString();
            result = result.substring(0, result.length() - 1);
            return result;
        }
    }
}
