package jiran.com.flyingfile.model;

import jiran.com.flyingfile.util.LanguageUtil.Language;

public class FileidDel_RealtimeTaskParams {

    private String fileid;
    private Language lang;
    private int nBufsize = 0;


    public FileidDel_RealtimeTaskParams(String fileid, Language lang) {
        super();
        this.fileid = fileid;
        this.lang = lang;
    }

    public String getFileid() {
        return fileid;
    }

    public void setFileid(String fileid) {
        this.fileid = fileid;
    }

    public Language getLang() {
        return lang;
    }

    public void setLang(Language lang) {
        this.lang = lang;
    }

    public int getnBufsize() {
        return nBufsize;
    }

    public void setnBufsize(int nBufsize) {
        this.nBufsize = nBufsize;
    }


}
