package jiran.com.flyingfile.model;

public class CustomDialogListItem {

    private String strContent = null;
    private int imgResource = 0;

    public CustomDialogListItem(String strContent) {
        super();
        this.strContent = strContent;
        this.imgResource = 0;
    }

    public CustomDialogListItem(String strContent, int imgResource) {
        super();
        this.strContent = strContent;
        this.imgResource = imgResource;
    }

    public String getStrContent() {
        return strContent;
    }

    public void setStrContent(String strContent) {
        this.strContent = strContent;
    }

    public int getImgResource() {
        return imgResource;
    }

    public void setImgResource(int imgResource) {
        this.imgResource = imgResource;
    }

}
