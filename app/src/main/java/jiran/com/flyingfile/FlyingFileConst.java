package jiran.com.flyingfile;

/**
 * Created by jeon-HP on 2017-05-24.
 */

public class FlyingFileConst {

    public static final int BUFFSIZE = 1048576 / 4;
    public static final int TIMEOUT = 120000;
    //	"이제는우리가헤어져야할시간" 의 해시값`
    public static final String ACCESS_KEY = "19AF6AD2627B4F032F759E816A832C58AD7A8F55";
    //FileID 생성
    public static final String URL_FILEID = "fileid";
    //업로드
    public static final String URL_UPLOAD = "up";
    //파일리스트 요청
    public static final String URL_FILELIST = "filelist";
    //실시간 파일전송 파일ID 생성
    public static final String URL_FILEID_REALTILE = "fileid_realtime";
    //실시간 파일전송 확인
    public static final String URL_TOUCH = "touch/";
    //실시간 파일id 삭제
    public static final String URL_FILEIDDEL_REALTIME = "fileiddel_realtime/";
    //파일포인터 리스트
    public static final String URL_SKIPLIST = "skiplist/";
    //취소
    public static final String URL_CANCEL = "cancel/";
    //정렬 값
    public static int sort = 0;
    public static int UDP_VERSION = 2;  // 필요시 수동 변경.(2미만으로는 금지)
    public static long MAX_SENDING_SIZE = 1024 * 1024 * 1024 * 10L;
    //ssl 도메인
    public static String URL_SSL_DOMAIN = "https://relay2.flying-file.com/";

    public enum DialogColor {
        COLORMODE_BLUE,
        COLORMODE_LIME
    }

}
