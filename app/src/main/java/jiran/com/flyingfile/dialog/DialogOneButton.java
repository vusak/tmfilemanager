package jiran.com.flyingfile.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import jiran.com.tmfilemanager.R;

public class DialogOneButton extends Dialog {


    public static DialogOneButton instance;
    public static boolean isUsing = false;
    private String strTitle;
    private String strContents;
    //PC탭 선택하는 메세지 정렬이 필요해서 따로 플래그 두자
    private boolean isPCTabMsg;
    private TextView tv_Title;
    private TextView tv_Contents;
    private TextView btn_Ok;
    private ListView lv_DialogList;
    private LinearLayout ll_Check;
    private ImageButton ib_Check;
    private TextView tv_CheckContents;
    private LinearLayout ll_PCTabMsg;
    private int gravity = Gravity.LEFT;

    public DialogOneButton(
            Context context,
            String strTitle,
            String strContents,
            int gravity
    ) {
        super(context, R.style.CustomDialog);
        // TODO Auto-generated constructor stub
        this.strTitle = strTitle;
        this.strContents = strContents;
        this.isPCTabMsg = false;
        this.gravity = gravity;
    }

    public DialogOneButton(
            Context context,
            String strTitle,
            String strContents,
            int gravity,
            boolean isPCTab
    ) {
        super(context, R.style.CustomDialog);
        // TODO Auto-generated constructor stub
        this.strTitle = strTitle;
        this.strContents = strContents;
        this.isPCTabMsg = isPCTab;
        this.gravity = gravity;
    }

    @Override
    public void setOnDismissListener(OnDismissListener listener) {
        super.setOnDismissListener(listener);
        instance = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub


        setContentView(R.layout.flyingfile_view_dialog_onebutton_lime);
        instance = this;


        tv_Title = (TextView) findViewById(R.id.TextView_Title);
        tv_Contents = (TextView) findViewById(R.id.TextView_Contents);
        btn_Ok = (TextView) findViewById(R.id.Button_Ok);
        lv_DialogList = (ListView) findViewById(R.id.ListView_DialogList);
        ll_Check = (LinearLayout) findViewById(R.id.LinearLayout_Check);
        ib_Check = (ImageButton) findViewById(R.id.ImageButton_Check);
        tv_CheckContents = (TextView) findViewById(R.id.TextView_CheckContents);
        ll_PCTabMsg = (LinearLayout) findViewById(R.id.LinearLayout_PCTab);

        tv_Title.setText(strTitle);


        if (tv_Contents != null) {
            if (isPCTabMsg) {
                tv_Contents.setVisibility(View.GONE);
                if (ll_PCTabMsg != null)
                    ll_PCTabMsg.setVisibility(View.VISIBLE);
            } else {
                if (strContents != null)
                    tv_Contents.setText(strContents);
                else
                    tv_Contents.setVisibility(View.GONE);

                if (ll_PCTabMsg != null)
                    ll_PCTabMsg.setVisibility(View.GONE);

            }
        }
        tv_Contents.setGravity(gravity);


        if (ll_Check != null) {
            ll_Check.setVisibility(View.GONE);
        }

        if (lv_DialogList != null) {
            lv_DialogList.setVisibility(View.GONE);
        }

        super.onCreate(savedInstanceState);
    }

    public void setOnOkListener(View.OnClickListener listener) {
        btn_Ok.setOnClickListener(listener);
    }

    public void setOnOkListener(String strCaption, View.OnClickListener listener) {
        btn_Ok.setText(strCaption);
        btn_Ok.setOnClickListener(listener);
    }

    public void setAdapter(
            ArrayAdapter<?> adapter,
            OnItemClickListener listener
    ) {
        lv_DialogList.setVisibility(View.VISIBLE);
        lv_DialogList.setAdapter(adapter);
        lv_DialogList.setOnItemClickListener(listener);
    }

    public ImageButton getCheckImageButton() {
        return ib_Check;
    }

    public ImageButton getCheckImageButton(String strCheckContents) {
        LinearLayout ll_Margin = (LinearLayout) findViewById(R.id.LinearLayout_CheckMargin);
        if (ll_Margin != null)
            ll_Margin.setVisibility(View.VISIBLE);
        ll_Check.setVisibility(View.VISIBLE);
        tv_CheckContents.setText(strCheckContents);
        return ib_Check;
    }

    public void setContentsGravity(int gravity) {
        tv_Contents.setGravity(gravity);
    }

}
