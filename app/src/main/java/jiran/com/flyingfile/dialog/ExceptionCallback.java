package jiran.com.flyingfile.dialog;

public interface ExceptionCallback {

    public void onException(Exception e);
}
