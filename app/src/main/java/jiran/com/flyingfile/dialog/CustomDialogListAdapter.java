package jiran.com.flyingfile.dialog;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import jiran.com.flyingfile.model.CustomDialogListItem;
import jiran.com.tmfilemanager.R;


public class CustomDialogListAdapter extends ArrayAdapter<CustomDialogListItem> {


    private int res;

    //	private int nDialogItemMode = 0;
//한글한글
    public CustomDialogListAdapter(Context context, int resource) {
        super(context, resource);
        // TODO Auto-generated constructor stub
        this.res = resource;
//		this.nDialogItemMode = 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        View view = convertView;
        if (view == null)
            view = View.inflate(getContext(), res, null);

        LinearLayout ll_Divider = (LinearLayout) view.findViewById(R.id.LinearLayout_Divider);
        if (position == (getCount() - 1)) {
            if (ll_Divider != null)
                ll_Divider.setVisibility(View.GONE);
        } else {
            if (ll_Divider != null)
                ll_Divider.setVisibility(View.VISIBLE);
        }

        TextView tv_Content = (TextView) view.findViewById(R.id.TextView_Content);
        tv_Content.setText(this.getItem(position).getStrContent());

        ImageView iv_Icon = (ImageView) view.findViewById(R.id.ImageView_PC_Icon);

//		LinearLayout ll_Icon_Layout = (LinearLayout) view.findViewById(R.id.LinearLayout_ItemLayout);

        if (this.getItem(position).getImgResource() == 0) {
            iv_Icon.setVisibility(View.GONE);
//			ll_Icon_Layout.setGravity(Gravity.CENTER);
        } else {
            iv_Icon.setVisibility(View.VISIBLE);
            iv_Icon.setImageResource(this.getItem(position).getImgResource());
//			ll_Icon_Layout.setGravity(Gravity.LEFT|Gravity.CENTER);
        }

        return view;
    }

}
