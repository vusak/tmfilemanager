package jiran.com.flyingfile.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.Normalizer;

import jiran.com.flyingfile.util.SharedPreferenceUtil;
import jiran.com.flyingfile.util.UnitTransfer;
import jiran.com.tmfilemanager.R;


@SuppressLint({"DefaultLocale", "HandlerLeak", "NewApi"})
public class FileTransferDialog extends Dialog
        implements View.OnClickListener {

    public static final int COUNT_MAX = 600;
    ImageView iv_transferIcon;
    TextView tv_Title;
    LinearLayout ll_Fileid;
    TextView tv_Fileid;
    TextView tv_CurrentFilename;
    ProgressBar pb_Progress;
    TextView tv_CurrentFilesize, TextView_Note3;
    ImageButton ib_Check;
    //	Button btn_Cancel;
//	Button btn_Close;
//	Button btn_SendToFriend;
//	Button btn_Continue;
    TextView btn_Cancel, btn_Close, btn_SendToFriend, btn_Continue;
    LinearLayout ll_Note;
    LinearLayout ll_fileinfo;
    Handler handler;
    //	LinearLayout ll_Left;
//	LinearLayout ll_Right;
    LinearLayout ll_Check;
    Timer timer = null;
    int nTotalCount;
    boolean isDownload;
    boolean isCheck;
    int m_CurrentIDX = 0;
    long m_nCurrentSize = 0;
    @SuppressWarnings("unused")
    private Context m_Context;
    private int jumpCnt = 0;
    private Bitmap bitmap_Check;
    private Bitmap bitmap_Uncheck;
    private boolean isSuccess;
    private boolean isRealTime;
    private boolean isFinish = false;

    public FileTransferDialog(
            Context context,
            int totalFileCount,
            boolean isDownload,
            boolean isRealtime
    ) {
        super(context, R.style.CustomDialog);
        // TODO Auto-generated constructor stub
        this.m_Context = context;
        this.nTotalCount = totalFileCount;
        this.isDownload = isDownload;
        this.isRealTime = isRealtime;
        isSuccess = false;
    }

    public void setJumpCnt(int jumpCnt) {
        this.jumpCnt = jumpCnt;
    }

    public int getnTotalCount() {
        return nTotalCount;
    }

    public void setnTotalCount(int nTotalCount) {
        this.nTotalCount = nTotalCount;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public boolean isRealTime() {
        return isRealTime;
    }

    public void setRealTime(boolean isRealTime) {
        this.isRealTime = isRealTime;
    }

    @SuppressWarnings("unused")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        setContentView(R.layout.flyingfile_cloud_transfer_dialog);

        handler = new Handler();

        bitmap_Check = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.fileview_check);
        bitmap_Uncheck = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.fileview_uncheck);

        iv_transferIcon = (ImageView) findViewById(R.id.transfer_icon);

        tv_Title = (TextView) findViewById(R.id.TextView_Title);
        ll_Fileid = (LinearLayout) findViewById(R.id.LinearLayout_FileID);
        tv_Fileid = (TextView) findViewById(R.id.TextView_FileID);
        ll_fileinfo = (LinearLayout) findViewById(R.id.LinearLayout_fileinfo);
        tv_CurrentFilename = (TextView) findViewById(R.id.TextView_CurrentFileName);
        pb_Progress = (ProgressBar) findViewById(R.id.ProgressBar_Progress);
        tv_CurrentFilesize = (TextView) findViewById(R.id.TextView_CurrentFileSize);
        TextView_Note3 = (TextView) findViewById(R.id.TextView_Note3);
        ib_Check = (ImageButton) findViewById(R.id.ImageButton_Check);
        btn_Cancel = (TextView) findViewById(R.id.Button_Cancel);
        btn_Close = (TextView) findViewById(R.id.Button_Close);
        btn_SendToFriend = (TextView) findViewById(R.id.Button_SendToFriend);
        btn_Continue = (TextView) findViewById(R.id.Button_Continue);
        ll_Note = (LinearLayout) findViewById(R.id.LinearLayout_Note);

//		ll_Left = (LinearLayout) findViewById(R.id.LinearLayout_Dialog_Bottom_Left);
//		ll_Right = (LinearLayout) findViewById(R.id.LinearLayout_Dialog_Bottom_Right);
        ll_Check = (LinearLayout) findViewById(R.id.LinearLayout_Check);

        ll_Note.setVisibility(View.GONE);


        TextView_Note3.setText(getContext().getString(R.string.Cloud_Dialog_Note3));
        isCheck = SharedPreferenceUtil.getInstance().getFileTransferDialogAutoClose(getContext());

        iv_transferIcon.setVisibility(View.GONE);
        ll_Fileid.setVisibility(View.GONE);
        pb_Progress.setMax(100);
        pb_Progress.setProgress(0);
        pb_Progress.setVisibility(View.GONE);

        btn_Close.setVisibility(View.GONE);


//		ll_Left.setVisibility(View.VISIBLE);
//		ll_Right.setVisibility(View.VISIBLE);
        btn_SendToFriend.setVisibility(View.GONE);

        btn_Continue.setVisibility(View.GONE);

        //업로드 다운로드 둘다 보여야함
        if (true) {
            ll_Check.setVisibility(View.VISIBLE);

            if (isCheck)
                ib_Check.setImageBitmap(bitmap_Check);
            else
                ib_Check.setImageBitmap(bitmap_Uncheck);
        } else
            ll_Check.setVisibility(View.GONE);


        ib_Check.setOnClickListener(this);

        this.setCancelable(false);

        super.onCreate(savedInstanceState);
    }

    public void setTitle(String strTitle) {
        tv_Title.setText(strTitle);
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setFileID(String strFileid, View.OnClickListener listener) {
        iv_transferIcon.setVisibility(View.VISIBLE);
        ll_Fileid.setVisibility(View.VISIBLE);
//		tv_Fileid.setText(strFileid);

        tv_Fileid.setText("");

        strFileid += " ";


        String[] strFileids = strFileid.split("");
        for (int i = 0; i < strFileids.length; i++) {
            if (strFileids[i].length() != 1)
                continue;
            try {
                Integer.parseInt(strFileids[i]);
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(strFileids[i]);
                //spannableStringBuilder.setSpan(new ForegroundColorSpan(0xFF0690cb), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                spannableStringBuilder.setSpan(new ForegroundColorSpan(0xFF53d769), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv_Fileid.append(spannableStringBuilder);
            } catch (Exception e) {
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(strFileids[i]);
                spannableStringBuilder.setSpan(new ForegroundColorSpan(0xFF333333), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv_Fileid.append(spannableStringBuilder);
            }
        }


//		ll_Left.setVisibility(View.GONE);
//		ll_Right.setVisibility(View.GONE);

        if (isRealTime && !isDownload) {
            ll_Note.setVisibility(View.VISIBLE);

            timer = new Timer(COUNT_MAX, tv_CurrentFilename);
            timer.sendEmptyMessage(0);

        } else {

            ll_Note.setVisibility(View.GONE);
        }

        tv_CurrentFilesize.setVisibility(View.GONE);
        btn_SendToFriend.setVisibility(View.VISIBLE);
        btn_SendToFriend.setText(m_Context.getResources().getString(R.string.FileId_Copy));
        btn_SendToFriend.setOnClickListener(listener);
    }

    public void initCountDown() {


        tv_CurrentFilename.setText(m_Context.getString(R.string.Cloud_Dialog_Init_Ment));
    }

    public void setProgressFileCnt(int nIdx, int nTotal, String strMsg) {


        String strFileInfo = null;
        strFileInfo = String.format("[ %d / %d ] %s", nIdx, nTotal, strMsg);

        Log.d("FileTransferDialog", "setProgressFileCnt : " + strFileInfo);

        int nMin = COUNT_MAX / 60;
        int nSec = COUNT_MAX % 60;
        String strFormat = " " + getContext().getString(R.string.Cloud_Timer_Format);
        String counter = String.format(strFormat, nMin, nSec);
        SpannableStringBuilder count = new SpannableStringBuilder();
        count.append(strMsg);
        count.append(" ");
        count.append(counter);
        //count.setSpan(new ForegroundColorSpan(0xf58d04),(strMsg).length(),count.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        count.setSpan(new ForegroundColorSpan(0xFF000000), (strMsg).length(), count.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv_CurrentFilename.setText(count);


        tv_CurrentFilename.setText(strFileInfo);

        ll_Note.setVisibility(View.VISIBLE);
    }

    public void setProgress(String target, int idx, long currentSize, long totalSize, String strName) {
        if (isFinish)
            return;

        if (timer != null) {
            timer.removeMessages(0);
            initCountDown();
        }
        ll_Note.setVisibility(View.GONE);
        pb_Progress.setVisibility(View.VISIBLE);

        m_CurrentIDX = idx;

        String strFileInfo = null;
        if (strName != null) {

            String temp = strName;
            boolean isNeedNormalize = Normalizer.isNormalized(temp, Normalizer.Form.NFD);
            if (isNeedNormalize) {
                temp = Normalizer.normalize(temp, Normalizer.Form.NFC);
            } else {
            }

            strFileInfo = String.format("[ %d / %d ] %s", idx + 1, nTotalCount, temp);
        } else {

            try {
                String temp = target;
                boolean isNeedNormalize = Normalizer.isNormalized(temp, Normalizer.Form.NFD);
                if (isNeedNormalize) {
                    temp = Normalizer.normalize(temp, Normalizer.Form.NFC);
                } else {
                }

                strFileInfo = String.format("[ %d / %d ] %s", idx + 1, nTotalCount, temp);
            } catch (Exception e) {

            }
        }

        if (strFileInfo != null)
            tv_CurrentFilename.setText(strFileInfo);

        m_nCurrentSize = currentSize;


        UnitTransfer.ProgressStruct struct = UnitTransfer.getInstance().progressConverse(currentSize, totalSize);
        tv_CurrentFilesize.setVisibility(View.VISIBLE);
        tv_CurrentFilesize.setText(struct.progress);

        pb_Progress.setProgress(struct.per);
        if (isDownload) {
            if (isRealTime())
                btn_SendToFriend.setVisibility(View.VISIBLE);
            else
                btn_SendToFriend.setVisibility(View.GONE);
        }
        btn_SendToFriend.setVisibility(View.GONE);
        iv_transferIcon.setVisibility(View.GONE);
        ll_Fileid.setVisibility(View.GONE);

//		ll_Left.setVisibility(View.VISIBLE);
//		ll_Right.setVisibility(View.VISIBLE);
    }

    public boolean isFinish() {
        return isFinish;
    }

    public void setFinish(boolean isError, boolean isCancel) {
        Log.d("FileTransferDialog", "setProgress(boolean,boolean) " + isError + "," + isCancel);
        if (isFinish)
            return;

        if (timer != null) {
            timer.removeMessages(0);
        }

        isFinish = true;
        if (m_nCurrentSize > 0) {
            m_CurrentIDX += 1;
        }

        if (m_CurrentIDX > getnTotalCount())
            m_CurrentIDX = getnTotalCount();


        Log.d("FileTransferDialog", "m_CurrentIDX : " + m_CurrentIDX);

        if (!isError) {
            isSuccess = true;
        }

        StringBuilder sb = new StringBuilder();
        sb.append(this.getContext().getString(R.string.FileDownloadDialog_All));
        sb.append(" ");
        sb.append(nTotalCount);
        sb.append(" ( ");
        sb.append(this.getContext().getString(R.string.FileDownloadDialog_Success));
        sb.append(" ");
        sb.append(m_CurrentIDX);
        sb.append(" / ");
        sb.append(this.getContext().getString(R.string.FileDownloadDialog_Fail));
        sb.append(" ");
        sb.append(nTotalCount - m_CurrentIDX);
        sb.append(" )");
        tv_CurrentFilename.setText(sb.toString());

        ll_Note.setVisibility(View.GONE);

        btn_Cancel.setVisibility(View.GONE);
        btn_Close.setVisibility(View.VISIBLE);


        pb_Progress.setVisibility(View.VISIBLE);
        tv_CurrentFilesize.setVisibility(View.VISIBLE);

        if (!isDownload && !isError) {
            if (!isRealTime()) {
                btn_SendToFriend.setVisibility(View.VISIBLE);
//				ll_Fileid_Option.setVisibility(View.VISIBLE);
//				ll_Left.setVisibility(View.GONE);
//				ll_Right.setVisibility(View.GONE);
            } else {
                btn_SendToFriend.setVisibility(View.GONE);
//				ll_Fileid_Option.setVisibility(View.GONE);
//				ll_Left.setVisibility(View.VISIBLE);
//				ll_Right.setVisibility(View.VISIBLE);
            }
        } else if (isDownload && isError && !isCancel) {
            if (!isRealTime) {
                btn_Continue.setVisibility(View.VISIBLE);
//				ll_Left.setVisibility(View.GONE);
//				ll_Right.setVisibility(View.GONE);
            }
        } else if (isDownload && !isError && isCancel) {

            if (!isRealTime) {
                btn_Continue.setVisibility(View.GONE);
//				ll_Left.setVisibility(View.VISIBLE);
//				ll_Right.setVisibility(View.VISIBLE);
            }
        }

        Log.d("FileTransferDialog", "setFinish : ");
        onContentChanged();

    }

    public void setFinish(boolean isError, boolean isCancel, int nSuccessedCnt) {
        Log.d("FileTransferDialog", "setProgress(boolean,boolean,int)" + jumpCnt + "," + isError + "," + isCancel + "," + nSuccessedCnt);
        if (isFinish) {
            return;
        }

        if (timer != null) {
//			timer.setStop(true);
            timer.removeMessages(0);
        }

        isFinish = true;
//		if(!isDownload)
        if (m_nCurrentSize > 0) {
            m_CurrentIDX += 1;
        }

        if (m_CurrentIDX > getnTotalCount())
            m_CurrentIDX = getnTotalCount();


        Log.d("FileTransferDialog", "m_CurrentIDX : " + m_CurrentIDX);

        if (!isError) {
            isSuccess = true;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(this.getContext().getString(R.string.FileDownloadDialog_All));
        sb.append(" ");
        sb.append(nTotalCount);
        sb.append(" ( ");
        sb.append(this.getContext().getString(R.string.FileDownloadDialog_Success));
        sb.append(" ");
        sb.append(nSuccessedCnt + jumpCnt);
        sb.append(" / ");
        sb.append(this.getContext().getString(R.string.FileDownloadDialog_Fail));
        sb.append(" ");
        sb.append(nTotalCount - nSuccessedCnt - jumpCnt);
        sb.append(" )");
        tv_CurrentFilename.setText(sb.toString());

        ll_Note.setVisibility(View.GONE);

        btn_Cancel.setVisibility(View.GONE);
        btn_Close.setVisibility(View.VISIBLE);


        pb_Progress.setVisibility(View.VISIBLE);
        tv_CurrentFilesize.setVisibility(View.VISIBLE);


        if (!isDownload && !isError) {
            if (!isRealTime()) {
                btn_SendToFriend.setVisibility(View.VISIBLE);
//				ll_Fileid_Option.setVisibility(View.VISIBLE);
//				ll_Left.setVisibility(View.GONE);
//				ll_Right.setVisibility(View.GONE);
            } else {
                btn_SendToFriend.setVisibility(View.GONE);
//				ll_Fileid_Option.setVisibility(View.GONE);
//				ll_Left.setVisibility(View.VISIBLE);
//				ll_Right.setVisibility(View.VISIBLE);
            }
        } else if (isDownload && isError && !isCancel) {
            if (!isRealTime) {
                btn_Continue.setVisibility(View.VISIBLE);
//				ll_Left.setVisibility(View.GONE);
//				ll_Right.setVisibility(View.GONE);
            }
        } else if (isDownload && !isError && isCancel) {

            if (!isRealTime) {
                btn_Continue.setVisibility(View.GONE);
//				ll_Left.setVisibility(View.VISIBLE);
//				ll_Right.setVisibility(View.VISIBLE);
            }
        }

        Log.d("FileTransferDialog", "setFinish : ");

    }


    public void setCancelBtnStatue() {
        if (!isRealTime) {
            btn_Continue.setVisibility(View.GONE);
//			ll_Left.setVisibility(View.VISIBLE);
//			ll_Right.setVisibility(View.VISIBLE);
        }
    }

    public void setOnCancelListener(String Caption, View.OnClickListener listener) {
        try {
            btn_Cancel.setText(Caption);
        } catch (Exception e) {

        }

        try {
            btn_Cancel.setOnClickListener(listener);
        } catch (Exception e) {

        }
    }

    public void setOnCloseListener(String Caption, View.OnClickListener listener) {
        btn_Close.setText(Caption);
        btn_Close.setOnClickListener(listener);
    }

    public void setOnContinueListener(String strCaption, View.OnClickListener listener) {
        btn_Continue.setText(strCaption);
        btn_Continue.setOnClickListener(listener);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        isCheck = !isCheck;

		/*
		Editor edit = pref.edit();
		edit.putBoolean("filetransfer_dialog", isCheck);
		edit.commit();
		*/
        SharedPreferenceUtil.getInstance().setFileTransferDialogAutoClose(getContext(), isCheck);

        if (isCheck)
            ib_Check.setImageBitmap(bitmap_Check);
        else
            ib_Check.setImageBitmap(bitmap_Uncheck);
    }

    @Override
    public void dismiss() {
        // TODO Auto-generated method stub
        FileStorageTransfer.getInstance().setUseFileTransfer(false);

        try {
            super.dismiss();
        } catch (Exception e) {
        }
    }

    class Timer extends Handler {

        int nCnt = 0;
        TextView tv_Cnt = null;
        boolean isStop = false;


        public Timer(int nCnt, TextView tv_Cnt) {
            super();
            this.nCnt = nCnt;
            this.tv_Cnt = tv_Cnt;
            isStop = false;
        }

        public boolean isStop() {
            return isStop;
        }

        public void setStop(boolean isStop) {
            this.isStop = isStop;
        }

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub

            nCnt--;
            int nMin = nCnt / 60;
            int nSec = nCnt % 60;
            String strFileInfo = null;
            strFileInfo = String.format("[%d/%d] %s", 0, nTotalCount, m_Context.getString(R.string.Cloud_Dialog_Init_Ment));
            String strFormat = " " + getContext().getString(R.string.Cloud_Timer_Format);
            String counter = String.format(strFormat, nMin, nSec);
            SpannableStringBuilder count = new SpannableStringBuilder();
            count.append(strFileInfo);
            count.append(" ");
            count.append(counter);
            //count.setSpan(new ForegroundColorSpan(0xFFF58D04),strFileInfo.length(),count.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            count.setSpan(new ForegroundColorSpan(0xFF000000), strFileInfo.length(), count.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tv_Cnt.setText(count);


            if (nCnt > 0 && !isStop) {
                sendEmptyMessageDelayed(0, 1000);
            } else {
                FileTransferDialog.this.setFinish(true, true);
                FileTransferDialog.this.dismiss();

                String strTitle = getContext().getString(R.string.Dialog_Password_Title);
                String strContents = getContext().getString(R.string.Cloud_Upload_Timeout_Count);
                final DialogOneButton dlg = new DialogOneButton(getContext(), strTitle, strContents, Gravity.CENTER);
                dlg.show();
                dlg.setOnOkListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        dlg.dismiss();
                    }
                });
            }

            super.handleMessage(msg);
        }
    }

}
