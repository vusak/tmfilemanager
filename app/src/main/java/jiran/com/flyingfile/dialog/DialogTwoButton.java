package jiran.com.flyingfile.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import jiran.com.tmfilemanager.MyApplication;
import jiran.com.tmfilemanager.R;


@SuppressLint("NewApi")
public class DialogTwoButton extends Dialog {


    public static DialogTwoButton instance;

    private String strTitle;
    private String strContents;

    private TextView tv_Title;
    private TextView tv_Contents;
    private TextView btn_Ok;
    private TextView btn_Cancel;
    private EditText et_Password;

    private ImageButton ib_Check;
    private TextView tv_Check;
    private LinearLayout ll_Check;
    private boolean isBtnRedColor = false; // 삭제다이어로그시 삭제버튼 삭상 변경유무

    private int nColorMode;


    public DialogTwoButton(
            Context context,
            String strTitle,
            String strContents,
            int nColorMode
    ) {
        super(context, R.style.CustomDialog);
        // TODO Auto-generated constructor stub
        this.strTitle = strTitle;
        this.strContents = strContents;
        this.nColorMode = nColorMode;
    }

    @Override
    public void setOnDismissListener(DialogInterface.OnDismissListener listener) {
        super.setOnDismissListener(listener);
        instance = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        if (nColorMode == MyApplication.COLORMODE_LIME) {
            setContentView(R.layout.flyingfile_view_dialog_twobutton_lime);
        } else if (nColorMode == MyApplication.COLORMODE_BLUE) {
            setContentView(R.layout.flyingfile_iew_dialog_twobutton_blue);
        }

        instance = this;

        tv_Title = (TextView) findViewById(R.id.TextView_Title);
        tv_Contents = (TextView) findViewById(R.id.TextView_Contents);
        btn_Ok = (TextView) findViewById(R.id.Button_Ok);
        btn_Cancel = (TextView) findViewById(R.id.Button_Cancel);
        et_Password = (EditText) findViewById(R.id.EditText_Password);

        ll_Check = (LinearLayout) findViewById(R.id.LinearLayout_Check);
        ib_Check = (ImageButton) findViewById(R.id.ImageButton_Check);
        tv_Check = (TextView) findViewById(R.id.TextView_Check);


        if (et_Password != null) {
            et_Password.setVisibility(View.GONE);
        }

        if (ll_Check != null) {
            ll_Check.setVisibility(View.GONE);
        }

        tv_Title.setText(strTitle);
        tv_Contents.setText(strContents);


        if (isBtnRedColor) {
            btn_Ok.setTextColor(Color.parseColor("#ff0000"));
        }

        super.onCreate(savedInstanceState);
    }


    public void setOnCheckboxListener(final String strCaption, View.OnClickListener listener) {
        ll_Check.setVisibility(View.VISIBLE);
        ib_Check.setOnClickListener(listener);
        tv_Check.post(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                tv_Check.setText(strCaption);
            }
        });
    }

    public ImageButton getCheckBox() {
        return ib_Check;
    }


    public void setContentsAlign(int gravity) {

        if (tv_Contents != null) {
            tv_Contents.setGravity(gravity);
        }
    }


    public void setOnOkListener(View.OnClickListener listener) {
        btn_Ok.setOnClickListener(listener);
    }

    public void setOnOkListener(final String strCaption, View.OnClickListener listener) {
        if (btn_Ok != null) {
            btn_Ok.post(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    btn_Ok.setText(strCaption);
                }
            });
            btn_Ok.setOnClickListener(listener);
        }
    }

    public void setOnCancelListener(View.OnClickListener listener) {
        btn_Cancel.setOnClickListener(listener);
    }


    public void setOnCancelListener(final String strCaption, View.OnClickListener listener) {
        btn_Cancel.post(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                btn_Cancel.setText(strCaption);
            }
        });
        btn_Cancel.setOnClickListener(listener);
    }

    /**
     * 현재 editText에 있는 string값을 가져온다.
     */
    public EditText getEditText() {
        return et_Password;
//		if(et_Password == null)
//			return null;
//		else
//			return et_Password.getText().toString();
    }

    /**
     * 다이얼로그에 있는 edittext를 활성화 시킴
     */
    public void setEditText(int inputType) {
        if (et_Password != null) {
            et_Password.setVisibility(View.VISIBLE);

            if (inputType != 0) {
                if (inputType == InputType.TYPE_TEXT_VARIATION_PASSWORD) {
                    et_Password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    et_Password.setInputType(inputType);
                }
            }
//			et_Password.requestFocus();
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInputFromWindow(et_Password.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);

            //Activity가 아닌 곳에서 키보드 띄울때 반드시 필요함
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
