package jiran.com.flyingfile.dialog;

public interface TwoButtonDialogCheckboxCallback {
    public void onCheck(DialogTwoButton dialog, boolean isCheck);
}
