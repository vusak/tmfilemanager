package jiran.com.flyingfile.dialog;

public class FileStorageTransfer {

    public static FileStorageTransfer instance = null;
    boolean isUseFileTransfer = false;

    public static FileStorageTransfer getInstance() {
        if (instance == null)
            instance = new FileStorageTransfer();
        return instance;
    }

    public static void setInstance(FileStorageTransfer instance) {
        FileStorageTransfer.instance = instance;
    }

    public boolean isUseFileTransfer() {
        return isUseFileTransfer;
    }

    public void setUseFileTransfer(boolean isUseFileTransfer) {
        this.isUseFileTransfer = isUseFileTransfer;
    }
}
