package jiran.com.flyingfile.dialog;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import jiran.com.flyingfile.util.SharedPreferenceUtil;
import jiran.com.flyingfile.util.UnitTransfer;
import jiran.com.flyingfile.wifidirect.activity.WifiDirectFileExplorerActivity;
import jiran.com.flyingfile.wifidirect.listener.WifiDirectTransferDialogListener;
import jiran.com.tmfilemanager.R;

@SuppressLint({"NewApi", "DefaultLocale"})
public class WifiDirectTransferDialog extends Dialog
        implements
        OnClickListener {
    private WifiDirectTransferDialogListener listener = null;
    private Context context = null;
    private TextView tv_Title;
    private TextView tv_CurrentFileInfo;
    private TextView tv_CurrentFileSize;
    private ProgressBar pBar_Progress;
    private TextView btn_Cancel;
    private TextView btn_Close;
    private boolean isSend = false;
    private boolean isCancel = false;
    private Bitmap bitmap_Check;
    private Bitmap bitmap_Uncheck;
    private ImageButton ib_Check;
    @SuppressWarnings("unused")
    private TextView tv_Check;
    private boolean isCheck;
    private boolean isFinish = false;
    private long m_nByteTotalFileSize = 0;
    private long m_nByteCurrentFileSize = 0;
    private int nCurrentCnt = 0;
    private int nTotalCnt = 0;
    private int jumpCnt = 0;

    public WifiDirectTransferDialog(
            Context context,
            WifiDirectTransferDialogListener listener
    ) {
        super(context, R.style.CustomDialog);
        this.context = context;
        this.listener = listener;
    }

    public boolean isFinish() {
        return isFinish;
    }

    public void setFinish(boolean isFinish) {
        this.isFinish = isFinish;
    }

    public boolean isSend() {
        return isSend;
    }

    public void setSend(boolean isSend) {
        this.isSend = isSend;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.flyingfile_view_wifidirect_filetransferdialog);

        bitmap_Check = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.fileview_check_lime);
        bitmap_Uncheck = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.fileview_uncheck_lime);

        tv_Title = (TextView) findViewById(R.id.TextView_Title);
        tv_CurrentFileInfo = (TextView) findViewById(R.id.TextView_CurrentFileName);
        tv_CurrentFileInfo.setSelected(true);
        tv_CurrentFileSize = (TextView) findViewById(R.id.TextView_CurrentFileSize);
        pBar_Progress = (ProgressBar) findViewById(R.id.ProgressBar_Progress);
        btn_Cancel = (TextView) findViewById(R.id.Button_Cancel);
        btn_Close = (TextView) findViewById(R.id.Button_Close);
        ib_Check = (ImageButton) findViewById(R.id.ImageButton_Check);

        isCheck = SharedPreferenceUtil.getInstance().getFileTransferDialogAutoClose(getContext());

        if (isCheck) {
            ib_Check.setImageBitmap(bitmap_Check);
        } else {
            ib_Check.setImageBitmap(bitmap_Uncheck);
        }

        //프로그레스를 전체를 100으로 놓는다.
        pBar_Progress.setMax(100);
        //초기에는 0으로 놓는다.
        pBar_Progress.setProgress(0);

        //처음에는 닫기버튼을 숨긴다.
        btn_Close.setVisibility(View.GONE);
        btn_Cancel.setVisibility(View.VISIBLE);

        btn_Close.setText(getContext().getString(R.string.Caption_Button_Close));
        btn_Cancel.setText(getContext().getString(R.string.Caption_Button_Cancel));

        this.setCancelable(false);

        btn_Close.setOnClickListener(this);
        btn_Cancel.setOnClickListener(this);
        ib_Check.setOnClickListener(this);

        super.onCreate(savedInstanceState);
    }

    //다이얼로그 타이틀 셋
    public void setTitle(String strTitle) {
        tv_Title.setText(strTitle);
    }

    public void setJumpCnt(int jumpCnt) {
        this.jumpCnt = jumpCnt;
    }

    public void setCurrentFileInfo(String strFileName, int nCurrentFileCount, int nTotalFileCount, long nCurrentFileSize, long nTotalFileSize) {
        String strFileInfo = null;

        nCurrentCnt = nCurrentFileCount;
        nTotalCnt = nTotalFileCount;

        if (nCurrentCnt == nTotalCnt) {
            Log.d("aa", "aa");
        }

        m_nByteCurrentFileSize = nCurrentFileSize;
        m_nByteTotalFileSize = nTotalFileSize;


        strFileInfo = String.format("[ %d / %d ] %s", nCurrentFileCount, nTotalFileCount, strFileName);


        UnitTransfer.ProgressStruct struct = UnitTransfer.getInstance().progressConverse(nCurrentFileSize, nTotalFileSize);

        pBar_Progress.setProgress(struct.per);
        tv_CurrentFileSize.setText(struct.progress);
        if (!isFinish) {
            tv_CurrentFileInfo.setText(strFileInfo);
        }
    }


    synchronized public void finishFileTransfer(int nCurrentFileCnt, int nTotalFileCnt, boolean isCancel) {
        //파일전송이 끝나면 취소버튼을 닫기버튼으로 바꾼다.
        if (isFinish()) {
            return;
        } else {
            setFinish(true);
        }
        TextView tv_Status = tv_CurrentFileInfo;
        if (m_nByteTotalFileSize == m_nByteCurrentFileSize) {

            if (nCurrentFileCnt < 0) {
                String strTemp = String.format("[ %d / %d ] %s", this.nCurrentCnt, nTotalCnt, getContext().getString(R.string.FileDownloadDialog_FileTransSuccess));
                tv_Status.setText(strTemp);
            } else {
                String strTemp = String.format("[ %d / %d ] %s", nCurrentFileCnt, nTotalCnt, getContext().getString(R.string.FileDownloadDialog_FileTransSuccess));
                tv_Status.setText(strTemp);
            }


            //파일 전송 성공인 경우만 창 자동으로 닫아주자
            if (isCheck) {
                if (!isSend) {
                    if (this.listener != null)
                        this.listener.onClose();
                }
                this.dismiss();
            }
        } else {
            if (isCancel) {

                if (nCurrentFileCnt < 0) {
                    String strTemp = String.format("[ %d / %d ] %s", this.nCurrentCnt, nTotalCnt, getContext().getString(R.string.FileDownloadDialog_FileTransCancel));
                    tv_Status.setText(strTemp);
                } else {
                    String strTemp = String.format("[ %d / %d ] %s", nCurrentFileCnt, nTotalCnt, getContext().getString(R.string.FileDownloadDialog_FileTransCancel));
                    tv_Status.setText(strTemp);
                }


            } else {

                if (nCurrentFileCnt < 0) {
                    String strTemp = String.format("[ %d / %d ] %s", this.nCurrentCnt, nTotalCnt, getContext().getString(R.string.FileDownloadDialog_FileTransFail));
                    tv_Status.setText(strTemp);
                } else {
                    String strTemp = String.format("[ %d / %d ] %s", nCurrentFileCnt, nTotalCnt, getContext().getString(R.string.FileDownloadDialog_FileTransFail));
                    tv_Status.setText(strTemp);
                }
            }

        }
        btn_Cancel.setVisibility(View.GONE);
        btn_Close.setVisibility(View.VISIBLE);

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

        int viewID = v.getId();

        if (viewID == R.id.Button_Cancel) {
            if (!isCancel) {

                if (this.listener != null)
                    this.listener.onCancel();
                finishFileTransfer(nCurrentCnt, nTotalCnt, true);
            }
        } else if (viewID == R.id.Button_Close) {
//파일전송 닫기 이벤트

            if (!isSend) {
                if (this.listener != null)
                    this.listener.onClose();
            }

            this.dismiss();
        } else if (viewID == R.id.ImageButton_Check) {
            if (isCheck) {
                isCheck = false;
                SharedPreferenceUtil.getInstance().setFileTransferDialogAutoClose(getContext(), isCheck);
            } else {
                isCheck = true;
                SharedPreferenceUtil.getInstance().setFileTransferDialogAutoClose(getContext(), isCheck);
            }
            if (isCheck) {
                ib_Check.setImageBitmap(bitmap_Check);
            } else {
                ib_Check.setImageBitmap(bitmap_Uncheck);
            }
        }
    }


    @Override
    public void dismiss() {
        if (m_nByteTotalFileSize == m_nByteCurrentFileSize) {
            if (context instanceof WifiDirectFileExplorerActivity) {
                ((WifiDirectFileExplorerActivity) context).listRefresh();
            }
        }
        super.dismiss();
    }
}
