package jiran.com.flyingfile.dialog;

public interface TwoButtonDialogOKCallback {
    public void onOK(DialogTwoButton dialog);
}
