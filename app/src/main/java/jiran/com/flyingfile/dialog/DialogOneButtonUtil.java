package jiran.com.flyingfile.dialog;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;

import jiran.com.flyingfile.util.FileManager;
import jiran.com.tmfilemanager.R;


public class DialogOneButtonUtil {


    private static DialogOneButtonUtil instance = null;

    public static DialogOneButtonUtil getInstance() {
        if (instance == null)
            instance = new DialogOneButtonUtil();
        return instance;
    }

    public void showFileIDNull(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_file_send);
            String strContents = context.getString(R.string.Cloud_Download_Error_NoFileID);
            final DialogOneButton dlg = new DialogOneButton(context, strTitle, strContents, Gravity.CENTER);
            dlg.setCancelable(false);
            dlg.show();
            dlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dlg.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showFileWriteAccessRestrict(Context context, final int intTitle, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(intTitle);
            String strContents = context.getString(R.string.FileWriteAccessRestrict);

            strContents = String.format(strContents, FileManager.getInstance().getSecondaryPackagePath(context).getAbsolutePath());

            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strContents, Gravity.LEFT);
            dialog.setCancelable(false);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (btnCallback != null)
                        btnCallback.onButtonClick();

                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showFileWriteAccessRestrict(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_file_delete);
            String strContents = context.getString(R.string.FileWriteAccessRestrict);

            strContents = String.format(strContents, FileManager.getInstance().getSecondaryPackagePath(context).getAbsolutePath());

            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strContents, Gravity.LEFT);
            dialog.setCancelable(false);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (btnCallback != null)
                        btnCallback.onButtonClick();

                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showFileDeleteNotEmptyFolder(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_file_delete);
            String strContents = context.getString(R.string.FileView_FileDelete_Folder);
            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strContents, Gravity.LEFT);
            dialog.setCancelable(false);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showNoSelectFiles(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.Dialog_Password_Title);
            String strMessage = context.getString(R.string.FileView_FileList_Delete_NoSelectedFile);
            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strMessage, Gravity.CENTER);
            dialog.setCancelable(false);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showIDOver64Character(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_join);
            String strMessage = context.getString(R.string.Join_Error_Message_Email_Is_Over);
            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strMessage, Gravity.LEFT);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showIDNotEmail(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_join);
            String strMessage = context.getString(R.string.Join_Error_Message_Email_Is_Invalid);
            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strMessage, Gravity.CENTER);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public DialogOneButton showDialog(Context context, CustomDialogListAdapter adapter, AdapterView.OnItemClickListener itemClickListener, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {

            String strTitle = context.getString(R.string.Dialog_Password_Title);
            String strBtnCaption = context.getString(R.string.Caption_Button_Close);
            final DialogOneButton dlg_AgentChoice = new DialogOneButton(context, strTitle, null, Gravity.LEFT);
            dlg_AgentChoice.setCancelable(false);
            dlg_AgentChoice.show();

            dlg_AgentChoice.setOnOkListener(strBtnCaption, new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (btnCallback != null)
                        btnCallback.onButtonClick();

                    dlg_AgentChoice.dismiss();
                }
            });
            dlg_AgentChoice.setAdapter(adapter, itemClickListener);

            return dlg_AgentChoice;

        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);

            return null;
        }
    }

    public void showDialog(
            Context context,
            String strTitle,
            String strContents,
            final ButtonCallback btnCallback,
            ExceptionCallback exceptionCallback) {
        try {
//			String strTitle = context.getString(R.string.Dialog_Password_Title);

            final DialogOneButton dlg = new DialogOneButton(context, strTitle, strContents, Gravity.LEFT);
            dlg.setCancelable(false);
            dlg.show();
            dlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dlg.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showFileIDNetworkError(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_send_fail);
            String strContents = context.getString(R.string.Cloud_Upload_Network_Error);
            final DialogOneButton dlg = new DialogOneButton(context, strTitle, strContents, Gravity.LEFT);
            dlg.setCancelable(false);
            dlg.show();
            dlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dlg.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showNoNetwork(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_non_connect);
            String strContents = context.getString(R.string.Cloud_Download_Error_Network);
            final DialogOneButton dlg = new DialogOneButton(context, strTitle, strContents, Gravity.CENTER);
            dlg.setCancelable(false);
            dlg.show();
            dlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dlg.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }

    }

    public void showFileIDNoSupportFolder(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            final DialogOneButton dlg = new DialogOneButton(
                    context,
                    context.getString(R.string.dialog_title_file_send),
                    context.getString(R.string.Dialog_Fileview_NoFolderRequest),
                    Gravity.LEFT);
            dlg.setCancelable(false);
            dlg.show();
            dlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dlg.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showLackOfStorageSize(Context context, long maxSize, final ButtonCallback btnCallbak, ExceptionCallback excpetionCallback) {
        try {
            String strFormat = context.getString(R.string.Cloud_FileUploadSize_Max);
            String strTitle = context.getString(R.string.dialog_title_file_send);

            final DialogOneButton dlg = new DialogOneButton(
                    context,
                    strTitle,
                    strFormat,
                    Gravity.LEFT);
            dlg.setCancelable(false);
            dlg.show();
            dlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallbak != null)
                        btnCallbak.onButtonClick();
                    dlg.dismiss();
                }
            });
        } catch (Exception e) {
            Log.e("@@@@@@@@@@@@@", "zzzzzzzzzzzzz" + e.getMessage());
            if (excpetionCallback != null)
                excpetionCallback.onException(e);
        }
    }

    public void showReceivedPathForceChange(Context context, String strDefaultStorage, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_receive_notice);
            String strContents = context.getString(R.string.ExternalStroageForceChange);
            strContents = String.format(strContents, strDefaultStorage);

            final DialogOneButton dlg = new DialogOneButton(context, strTitle, strContents, Gravity.LEFT);
            dlg.setCancelable(false);
            dlg.show();
            dlg.setOnOkListener(context.getString(R.string.Caption_Button_Ok), new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dlg.dismiss();
                }
            });

        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showFileTransferNoSupportEmptyFolder(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_file_send);
            String strContents = context.getString(R.string.Dialog_Fileview_EmptyFolderRequest);
            final DialogOneButton dlg =
                    new DialogOneButton(
                            context,
                            strTitle,
                            strContents,
                            Gravity.LEFT
                    );
            dlg.setCancelable(false);
            dlg.show();
            dlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();

                    dlg.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public DialogOneButton showUseUSBStorage(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.Dialog_Password_Title);
            String strContents = context.getString(R.string.Use_USB_Storage);
            final DialogOneButton dlg_OneBtn = new DialogOneButton(context, strTitle, strContents, Gravity.LEFT);
            dlg_OneBtn.setCancelable(false);
            dlg_OneBtn.show();
            dlg_OneBtn.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    dlg_OneBtn.dismiss();
                }
            });

            return dlg_OneBtn;

        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);

            return null;
        }
    }

    public void a(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {

        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showPcStorageFull(Context context) {
        try {
            final DialogOneButton dlg =
                    new DialogOneButton(
                            context,
                            context.getString(R.string.dialog_title_file_send),
                            context.getString(R.string.pc_storage_full),
                            Gravity.LEFT,
                            false
                    );
            dlg.setCancelable(false);
            dlg.show();
            dlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    dlg.dismiss();
                }
            });
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    public void showFilelistSending(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            final DialogOneButton dlg =
                    new DialogOneButton(
                            context,
                            context.getString(R.string.dialog_title_help),
                            context.getString(R.string.FilelistSending),
                            Gravity.LEFT,
                            false
                    );
            dlg.setCancelable(false);
            dlg.show();
            dlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dlg.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public DialogOneButton showPCTabError2(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {

        try {
            String strTitle = context.getString(R.string.Dialog_Password_Title);
            String strMessage = context.getString(R.string.dialog_title_non_connect);
            final DialogOneButton dlg_PCNotConnected = new DialogOneButton(context, strTitle, strMessage, Gravity.LEFT, true);
            dlg_PCNotConnected.setCancelable(false);
            dlg_PCNotConnected.show();
            dlg_PCNotConnected.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (btnCallback != null)
                        btnCallback.onButtonClick();

                    dlg_PCNotConnected.dismiss();
                }
            });

            return dlg_PCNotConnected;
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);

            return null;
        }
    }

    public DialogOneButton showPCTabError2(Context context, boolean isPCTab, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {

        try {
            String strTitle = context.getString(R.string.dialog_title_non_connect);
            String strMessage = context.getString(R.string.Dialog_Fileview_Description_PCTab_Error_Contents);
            final DialogOneButton dlg_PCNotConnected = new DialogOneButton(context, strTitle, strMessage, Gravity.LEFT, isPCTab);
            dlg_PCNotConnected.setCancelable(false);
            dlg_PCNotConnected.show();
            dlg_PCNotConnected.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (btnCallback != null)
                        btnCallback.onButtonClick();

                    dlg_PCNotConnected.dismiss();
                }
            });

            return dlg_PCNotConnected;
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);

            return null;
        }
    }

    public void showPCTabError(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {

            String strTitle = context.getString(R.string.Dialog_Password_Title);
            final DialogOneButton dialog = new DialogOneButton(context, strTitle, null, Gravity.LEFT, true);
            dialog.setCancelable(false);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null) {
                        btnCallback.onButtonClick();
                    }
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null) {
                exceptionCallback.onException(e);
            }
        }
    }

    public void showAlreadyFileTransfer(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            //파일 전송중
            String strTitle = context.getString(R.string.dialog_title_file_send);
            String strContents = context.getString(R.string.Dialog_AlreadyFileTransfer);
            final DialogOneButton dlg = new DialogOneButton(context, strTitle, strContents, Gravity.LEFT);
            dlg.setCancelable(false);
            dlg.show();
            dlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dlg.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showPermissionDenyPath(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {

            String strTitle = context.getString(R.string.dialog_title_file_delete);
            String strMessage = context.getString(R.string.NotPermissionDelete);
            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strMessage, Gravity.LEFT);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();

                    dialog.dismiss();
                }
            });


        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showIDNull(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.Dialog_Password_Title);
            String strMessage = context.getString(R.string.Join_Error_Message_Email_Is_Null);
            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strMessage, Gravity.CENTER);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showPasswordNull(
            Context context,
            String strTitle,
            final ButtonCallback btnCallback,
            ExceptionCallback exceptionCallback) {
        try {
            String strMessage = context.getString(R.string.Join_Error_Message_Password_Is_Null);
            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strMessage, Gravity.CENTER);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showPasswordOverCharacter(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_join);
            String strMessage = context.getString(R.string.Join_Error_Message_Password_Is_Over);
            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strMessage, Gravity.LEFT);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showPasswordRetypeNotMatch(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_join);
            String strMessage = context.getString(R.string.Join_Error_Message_Password_Is_NotMatch);
            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strMessage, Gravity.CENTER);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showPasswordRetypeNull(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_join);
            String strMessage = context.getString(R.string.Join_Password2_null);
            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strMessage, Gravity.CENTER);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showJoinNetworkError(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.Dialog_Password_Title);
            String strMessage = context.getString(R.string.Join_Error_Message_NetworkError);
            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strMessage, Gravity.CENTER);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showJoinSameIDandPassword(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_join);
            String strMessage = context.getString(R.string.Join_Error_Message_Password_Is_SameID);
            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strMessage, Gravity.LEFT);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showJoinFailed(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.Dialog_Password_Title);
            String strMessage = context.getString(R.string.Join_Error_Message_JoinFail);
            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strMessage, Gravity.CENTER);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showJoinSuccess(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_join);
            String strMessage = context.getString(R.string.Join_Success_Message);
            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strMessage, Gravity.CENTER);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showJoinExistID(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_join);
            String strMessage = context.getString(R.string.Join_Error_Message_ExistID);
            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strMessage, Gravity.LEFT);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showJoinNoAgreePolicy(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_join);
            String strMessage = context.getString(R.string.Join_No_Agree);
            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strMessage, Gravity.LEFT);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showLoginPasswordNotMatch(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_login);
            String strMessage = context.getString(R.string.LoginActivity_LoginError_Message);
            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strMessage, Gravity.LEFT, false);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showLoginNoNetwork(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_non_connect);
            String strMessage = context.getString(R.string.LOGIN_FAIL_DIALOG_CONTENTS);
            final DialogOneButton dialog = new DialogOneButton(context, strTitle, strMessage, Gravity.LEFT, false);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showNoGCM(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_login);
            String strContents = context.getString(R.string.GCMWait);
            final DialogOneButton dlg = new DialogOneButton(context, strTitle, strContents, Gravity.LEFT, false);
            dlg.show();
            dlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    dlg.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showLoginIDNull(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            final DialogOneButton loginDialog = new DialogOneButton
                    (context,
                            context.getString(R.string.dialog_title_login),
                            context.getString(R.string.LoginActivity_LoginError_Message_ID),
                            Gravity.CENTER,
                            false);

            loginDialog.show();
            loginDialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    loginDialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showLoginPasswordNull(Context context, final ButtonCallback btnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.Dialog_Password_Title);
            String strMessage = context.getString(R.string.LoginActivity_LoginError_Message_Password);
            final DialogOneButton loginDialog = new DialogOneButton(context, strTitle, strMessage, Gravity.CENTER, false);
            loginDialog.show();
            loginDialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick();
                    loginDialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public interface ButtonCallback {
        public void onButtonClick();
    }

    public interface ButtonCallback2 {
        public void onButtonClick(DialogOneButton dlg);
    }

}
