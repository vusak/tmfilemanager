package jiran.com.flyingfile.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import jiran.com.tmfilemanager.R;


public class CustomDialog extends Dialog {

    private TextView mTitleView;
    private TextView mContentView;
    private TextView mLeftButton;
    private TextView mRightButton;
    private CheckBox cb_Again;
    private ListView lv_ItemList = null;
    //	private ArrayAdapter<CustomDialogListItem> adapter_ItemList = null;
//	private CustomDialogListAdapter adapter_ItemList = null;
    private ArrayAdapter<?> adapter_ItemList = null;
    private String mTitle = null;
    private String mContent = null;
    private String mLeftButtonCaption = null;
    private String mRightButtonCaption = null;
    private String mCheckBoxCaption = null;
    private boolean isCenterAlign = false;
    private View.OnClickListener mLeftClickListener = null;
    private View.OnClickListener mRightClickListener = null;
    private OnCheckedChangeListener mCheckedChangeListener = null;
    private OnItemClickListener mOnItemClickListener = null;

    public CustomDialog(Context context) {
        // Dialog 배경을 투명 처리 해준다.
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.isCenterAlign = true;
    }

    public CustomDialog(Context context,
                        String title,
                        String leftButtonCaption,
//			CustomDialogListAdapter adapter,
                        ArrayAdapter<?> adapter,
                        OnItemClickListener itemClickListener,
                        View.OnClickListener singleListener) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.mTitle = title;
        this.mLeftButtonCaption = leftButtonCaption;
        this.adapter_ItemList = adapter;
        this.mOnItemClickListener = itemClickListener;
        this.mLeftClickListener = singleListener;
        this.isCenterAlign = true;
    }

    public CustomDialog(Context context,
                        String title,
                        String msg,
                        String leftButtonCaption,
                        String rightButtonCaption,
                        View.OnClickListener singleListener) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.mTitle = title;
        this.mContent = msg;
        this.mLeftButtonCaption = leftButtonCaption;
        this.mRightButtonCaption = rightButtonCaption;
        this.mLeftClickListener = singleListener;
        this.isCenterAlign = true;
    }

    public CustomDialog(Context context,
                        String title,
                        String content,
                        String leftButtonCaption,
                        String rightButtonCaption,
                        View.OnClickListener leftListener,
                        View.OnClickListener rightListener) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.mTitle = title;
        this.mContent = content;
        this.mLeftButtonCaption = leftButtonCaption;
        this.mRightButtonCaption = rightButtonCaption;
        this.mLeftClickListener = leftListener;
        this.mRightClickListener = rightListener;
        this.isCenterAlign = true;
    }

    public CustomDialog(Context context,
                        String title,
                        String content,
                        String leftButtonCaption,
                        String rightButtonCaption,
                        View.OnClickListener leftListener,
                        View.OnClickListener rightListener,
                        OnCheckedChangeListener checkedListener
    ) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.mTitle = title;
        this.mContent = content;
        this.mLeftButtonCaption = leftButtonCaption;
        this.mRightButtonCaption = rightButtonCaption;
        this.mLeftClickListener = leftListener;
        this.mRightClickListener = rightListener;
//		bCheckbox = isCheckbox;
        this.mCheckedChangeListener = checkedListener;
        this.isCenterAlign = true;
    }

    public CustomDialog(Context context,
                        String title,
                        String content,
                        String leftButtonCaption,
                        String checkboxCaption,
                        View.OnClickListener leftListener,
                        OnCheckedChangeListener checkedListener,
                        boolean isCenterAlign
    ) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.mTitle = title;
        this.mContent = content;
        this.mLeftButtonCaption = leftButtonCaption;
        this.mCheckBoxCaption = checkboxCaption;
        this.mLeftClickListener = leftListener;
        this.mCheckedChangeListener = checkedListener;
        this.isCenterAlign = isCenterAlign;
    }

    public CustomDialog(Context context,
                        String title,
                        String content,
                        String leftButtonCaption,
                        String checkboxCaption,
                        View.OnClickListener leftListener,
                        OnCheckedChangeListener checkedListener
    ) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.mTitle = title;
        this.mContent = content;
        this.mLeftButtonCaption = leftButtonCaption;
        this.mCheckBoxCaption = checkboxCaption;
        this.mLeftClickListener = leftListener;
        this.mCheckedChangeListener = checkedListener;
        this.isCenterAlign = true;
    }

    public CustomDialog(Context context,
                        String title,
                        String content,
                        String leftButtonCaption,
                        View.OnClickListener leftListener
    ) {
        super(context, android.R.style.Theme_Translucent_NoTitleBar);
        this.mTitle = title;
        this.mContent = content;
        this.mLeftButtonCaption = leftButtonCaption;
        this.mLeftClickListener = leftListener;
        this.isCenterAlign = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        lpWindow.dimAmount = 0.5f;
        getWindow().setAttributes(lpWindow);

        setContentView(R.layout.flyingfile_view_dialog_ineed_theme);

        setLayout();
        setTitle(mTitle);
        setContent(mContent);


        setLeftButtonCaption(mLeftButtonCaption);
        setRightButtonCaption(mRightButtonCaption);
        setCheckBoxCaption(mCheckBoxCaption);
        setClickListener(mLeftClickListener, mRightClickListener);
        setmCheckedChangeListener(mCheckedChangeListener);
        setList(lv_ItemList, adapter_ItemList);
        setListListener(lv_ItemList, mOnItemClickListener);
    }

    private void setTitle(String title) {
        mTitleView.setText(title);
    }

    private void setContent(String content) {
        mContentView.setText(content);
        if (!this.isCenterAlign) {
            mContentView.setGravity(Gravity.LEFT);
        } else {
            mContentView.setGravity(Gravity.CENTER);
        }
    }

    private void setLeftButtonCaption(String caption) {
        mLeftButton.setText(caption);
    }

    private void setRightButtonCaption(String caption) {
        if (mRightButton != null)
            mRightButton.setText(caption);
    }

    private void setCheckBoxCaption(String caption) {
        cb_Again.setText(caption);
    }

    private void setClickListener(View.OnClickListener left, View.OnClickListener right) {
        if (left != null && right != null) {
            mLeftButton.setOnClickListener(left);
            mRightButton.setOnClickListener(right);
        } else if (left != null && right == null) {
            mLeftButton.setOnClickListener(left);
        } else {

        }
    }

    @SuppressWarnings("null")
    private void setList(
            ListView lv_List,
//			CustomDialogListAdapter adapter
            ArrayAdapter<?> adapter
    ) {
        if (lv_List != null) {
            lv_List.setAdapter(adapter);
        } else {
            lv_List.setVisibility(View.GONE);
        }
    }

    @SuppressWarnings("null")
    private void setListListener(ListView lv_List, OnItemClickListener clickListener) {
        if (lv_List == null) {
            lv_List.setVisibility(View.GONE);
        } else {
            lv_List.setOnItemClickListener(clickListener);
        }
    }

    private void setmCheckedChangeListener(
            OnCheckedChangeListener mCheckedChangeListener) {
        cb_Again.setOnCheckedChangeListener(mCheckedChangeListener);
//		this.mCheckedChangeListener = mCheckedChangeListener;
    }

    /*
     * Layout
     */
    private void setLayout() {

        lv_ItemList = (ListView) findViewById(R.id.ListView_ItemList);
        if (adapter_ItemList == null)
            lv_ItemList.setVisibility(View.GONE);
        else
            lv_ItemList.setVisibility(View.VISIBLE);

        mTitleView = (TextView) findViewById(R.id.TextView_Dialog_Title);
        if (mTitle == null)
            mTitleView.setVisibility(View.GONE);
        else
            mTitleView.setVisibility(View.VISIBLE);

        mContentView = (TextView) findViewById(R.id.TextView_Dialog_Contents);
        if (mContent == null)
            mContentView.setVisibility(View.GONE);
        else
            mContentView.setVisibility(View.VISIBLE);


        if (mLeftButtonCaption != null && mRightButtonCaption != null) {
            LinearLayout ll_TwoButton = (LinearLayout) findViewById(R.id.LinearLayout_TwoButton);
            LinearLayout ll_OneButton = (LinearLayout) findViewById(R.id.LinearLayout_OneButton);
            ll_OneButton.setVisibility(View.GONE);
            ll_TwoButton.setVisibility(View.VISIBLE);

            mLeftButton = (TextView) findViewById(R.id.Button_T_Ok);
            mRightButton = (TextView) findViewById(R.id.Button_T_Cancel);
        } else {
            LinearLayout ll_TwoButton = (LinearLayout) findViewById(R.id.LinearLayout_TwoButton);
            LinearLayout ll_OneButton = (LinearLayout) findViewById(R.id.LinearLayout_OneButton);
            ll_OneButton.setVisibility(View.VISIBLE);
            ll_TwoButton.setVisibility(View.GONE);

            mLeftButton = (TextView) findViewById(R.id.Button_Ok);
            if (mLeftButtonCaption == null)
                mLeftButton.setVisibility(View.GONE);
            else
                mLeftButton.setVisibility(View.VISIBLE);

//			mRightButton = (Button) findViewById(R.id.Button_Cancel);
//			if(mRightButtonCaption == null)
//			{
//				mRightButton.setVisibility(View.GONE);
//			}
//			else
//			{
//				mRightButton.setVisibility(View.VISIBLE);
//			}
        }


//		if(mLeftButton.getVisibility()==View.VISIBLE && mRightButton.getVisibility()==View.VISIBLE)
//		{
//			
//			LinearLayout.LayoutParams params = (LayoutParams) mLeftButton.getLayoutParams();
////			params.gravity = Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL;
//			params.weight = 1;
//			params.width = LayoutParams.MATCH_PARENT;
//			params.height = LayoutParams.WRAP_CONTENT;
//			mLeftButton.setLayoutParams(params);
//			
//			
//			
//			params = (LayoutParams) mRightButton.getLayoutParams();
////			params.gravity = Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL;
//			params.weight = 1;
//			params.width = LayoutParams.MATCH_PARENT;
//			params.height = LayoutParams.WRAP_CONTENT;
//			mRightButton.setLayoutParams(params);
//			
//			LinearLayout ll_Dump = (LinearLayout) findViewById(R.id.LinearLayout_Dump);
//			ll_Dump.setVisibility(View.VISIBLE);
//			
//		}

        cb_Again = (CheckBox) findViewById(R.id.CheckBox_Again);
        if (mCheckBoxCaption == null)
            cb_Again.setVisibility(View.GONE);
        else
            cb_Again.setVisibility(View.VISIBLE);

        if (mCheckedChangeListener != null)
            cb_Again.setVisibility(View.VISIBLE);
        else
            cb_Again.setVisibility(View.GONE);

        if (mLeftClickListener == null)
            mLeftButton.setVisibility(View.GONE);
        else
            mLeftButton.setVisibility(View.VISIBLE);

        if (mRightClickListener == null) {
            if (mRightButton != null)
                mRightButton.setVisibility(View.GONE);
        } else {
            if (mRightButton != null)
                mRightButton.setVisibility(View.VISIBLE);
        }
    }

    public boolean isCenterAlign() {
        return isCenterAlign;
    }

    public void setCenterAlign(boolean isCenterAlign) {
        this.isCenterAlign = isCenterAlign;
    }


}
