package jiran.com.flyingfile.dialog;

public interface TwoButtonDialogCancelCallback {

    public void onCancel(DialogTwoButton dialog);

}
