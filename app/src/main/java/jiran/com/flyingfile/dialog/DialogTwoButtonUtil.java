package jiran.com.flyingfile.dialog;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import java.text.SimpleDateFormat;
import java.util.List;

import jiran.com.flyingfile.FlyingFileConst;
import jiran.com.flyingfile.model.FileItem;
import jiran.com.flyingfile.util.UnitTransfer;
import jiran.com.tmfilemanager.MyApplication;
import jiran.com.tmfilemanager.R;

public class DialogTwoButtonUtil {


    private static DialogTwoButtonUtil instance = null;

    public static DialogTwoButtonUtil getInstance() {
        if (instance == null)
            instance = new DialogTwoButtonUtil();
        return instance;
    }

    public void showDialog(Context context, String strContents, final TwoButtonDialogOKCallback okBtnCallback, final TwoButtonDialogCancelCallback cancelBtnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.Dialog_Password_Title);

            final DialogTwoButton dialog = new DialogTwoButton(context, strTitle, strContents, MyApplication.COLORMODE_BLUE);
            dialog.setCancelable(false);
            dialog.show();
            dialog.setOnCancelListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dialog);

                    dialog.dismiss();
                }
            });
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dialog);

                    dialog.dismiss();

                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showInputPassword(
            final Context context,
            final TwoButtonDialogOKCallback okBtnCallback,
            final TwoButtonDialogCancelCallback cancelBtnCallback,
            final boolean onOKisDissmiss,
            ExceptionCallback exceptionCallback) {
        try {

            String strTitle = context.getString(R.string.Dialog_Password_Title);
            String strContents = context.getString(R.string.Dialog_Password_Content);

            final DialogTwoButton dialog = new DialogTwoButton(context, strTitle, strContents, MyApplication.COLORMODE_BLUE);
            dialog.setCancelable(false);
            dialog.show();
            dialog.setEditText(InputType.TYPE_TEXT_VARIATION_PASSWORD);
            dialog.setOnCancelListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dialog);

                    dialog.dismiss();
                }
            });
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dialog);

                    if (onOKisDissmiss)
                        dialog.dismiss();

                }
            });


        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showSyncCheck(final Context context, final DialogTwoButtonUtil.ButtonCallback2 btnCallback,
                              long date, List<FileItem> data, String[] allItem) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String str_FileDate = dateFormat.format(date);
            String content = null;
            long totalSize = 0;
            for (int i = 0; i < data.size(); i++) {
                totalSize += data.get(i).length();
            }
            String fileTotalSize = UnitTransfer.getInstance().memoryConverse2(totalSize);
            if (date == 0) {
                content = String.format(context.getString(R.string.syncro_warring2), data.size(), fileTotalSize);
            } else {
                content = String.format(context.getString(R.string.syncro_warring1), str_FileDate, data.size(), fileTotalSize);
            }
            String strtotalSize = String.format(context.getString(R.string.backup_description_all), Integer.valueOf(allItem[0]), allItem[1]);

            final DialogTwoButton dlg =
                    new DialogTwoButton(
                            context,
                            context.getString(R.string.opt_title_syncro_folder),
                            content,
                            MyApplication.COLORMODE_BLUE
                    );
            dlg.setCancelable(false);
            dlg.show();
            dlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (btnCallback != null)
                        btnCallback.onButtonClick(dlg);
                    dlg.dismiss();
                }
            });
            if (date != 0 && data.size() != 0) {
                final ImageButton ib_Check = dlg.getCheckBox();
                ib_Check.setTag(false);
                ib_Check.setImageResource(R.drawable.fileview_uncheck);
                dlg.setOnCheckboxListener(strtotalSize, new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Boolean isCheck = (Boolean) ib_Check.getTag();
                        if (isCheck) {
                            ib_Check.setImageResource(R.drawable.fileview_uncheck);
                            ib_Check.setTag((Boolean) false);
                        } else {
                            ib_Check.setImageResource(R.drawable.fileview_check);
                            ib_Check.setTag((Boolean) true);
                        }
                    }
                });
            }
            dlg.setOnCancelListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dlg.dismiss();
                }
            });
        } catch (Exception e) {
            e.getStackTrace();
            e.printStackTrace();
        }
    }

    public void showMobileNetworkFileTransfer(final Context context, final TwoButtonDialogOKCallback okBtnCallback, final TwoButtonDialogCancelCallback cancelBtnCallback, ExceptionCallback exceptionCallback) {

        try {
            String strContents = context.getString(R.string.MobileNetworkDataTransferContents);
            final DialogTwoButton dlgTwoButton = new DialogTwoButton(context, context.getString(R.string.dialog_3g_lte_title), strContents, MyApplication.COLORMODE_BLUE);
            dlgTwoButton.show();
            dlgTwoButton.setContentsAlign(Gravity.LEFT);
            dlgTwoButton.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dlgTwoButton);

                    dlgTwoButton.dismiss();
                }
            });
            dlgTwoButton.setOnCancelListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dlgTwoButton);

                    dlgTwoButton.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showFileIDAppend(Context context, final TwoButtonDialogOKCallback okBtnCallback, final TwoButtonDialogCancelCallback cancelBtnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.app_name);
            String strContents = context.getString(R.string.Cloud_Caption_Continue_Contents);
            final DialogTwoButton dlg = new DialogTwoButton(context, strTitle, strContents, MyApplication.COLORMODE_BLUE);
            dlg.show();
            dlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dlg);

                    dlg.dismiss();
                }
            });
            dlg.setOnCancelListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dlg);

                    dlg.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showFileDelete(Context context, final TwoButtonDialogOKCallback okBtnCallback, final TwoButtonDialogCancelCallback cancelBtnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.delete_file_title);
            String strContents = context.getString(R.string.FileView_Delete_Msg);

            final DialogTwoButton alertDlg = new DialogTwoButton(context, strTitle, strContents, MyApplication.COLORMODE_BLUE);
            alertDlg.show();
            alertDlg.setOnOkListener(context.getString(R.string.DarkBlue_Caption_Delete), new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (okBtnCallback != null)
                        okBtnCallback.onOK(alertDlg);

                    alertDlg.dismiss();
                }
            });
            alertDlg.setOnCancelListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(alertDlg);

                    alertDlg.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showFinishWifiDirect(Context context, String strDeviceName, final TwoButtonDialogOKCallback okBtnCallback, final TwoButtonDialogCancelCallback cancelBtnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strContents = String.format(
                    context.getString(R.string.MtoM_CloseDialogMessage), strDeviceName);
            String strTitle = context.getString(R.string.dialog_title_disconnect);

            final DialogTwoButton dlg =
                    new DialogTwoButton(
                            context,
                            strTitle,
                            strContents,
                            FlyingFileConst.DialogColor.COLORMODE_LIME.ordinal()
                    );
            dlg.show();
            dlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dlg);

                    dlg.dismiss();
                }
            });
            dlg.setOnCancelListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dlg);

                    dlg.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showFileDeleteSwipeAlert(Context context, final TwoButtonDialogOKCallback okBtnCallback, final TwoButtonDialogCancelCallback cancelBtnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_complate_delete);
            String strMessage = context.getString(R.string.File_Swipe_AlertMessage);
            final DialogTwoButton confirmDlg = new DialogTwoButton(context, strTitle, strMessage, MyApplication.COLORMODE_BLUE);
            confirmDlg.show();
            confirmDlg.setOnCancelListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(confirmDlg);

                    confirmDlg.dismiss();
                }
            });
            confirmDlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (okBtnCallback != null)
                        okBtnCallback.onOK(confirmDlg);

                    confirmDlg.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showFileListTimeout(Context context, final TwoButtonDialogOKCallback okBtnCallback, final TwoButtonDialogCancelCallback cancelBtnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_non_connect);
            String strContents = context.getString(R.string.FileView_FileList_TimeoutMessage_Contents);
            final DialogTwoButton dlg_FileListTimeout = new DialogTwoButton(context, strTitle, strContents, MyApplication.COLORMODE_BLUE);
            dlg_FileListTimeout.show();

            dlg_FileListTimeout.setOnOkListener(context.getString(R.string.Caption_Button_Retry), new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dlg_FileListTimeout);

                    dlg_FileListTimeout.dismiss();
                }
            });
            dlg_FileListTimeout.setOnCancelListener(context.getString(R.string.Caption_Button_Cancel), new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dlg_FileListTimeout);

                    dlg_FileListTimeout.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public DialogTwoButton showConnectRetry(Context context, final TwoButtonDialogOKCallback okBtnCallback, final TwoButtonDialogCancelCallback cancelBtnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_non_connect);
            String strContents = context.getString(R.string.Dialog_Connect_Fail);

            final DialogTwoButton dlg_RetryDialog = new DialogTwoButton(context, strTitle, strContents, MyApplication.COLORMODE_BLUE);
            dlg_RetryDialog.show();
            dlg_RetryDialog.setOnOkListener(context.getString(R.string.Caption_Button_ReConnect), new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    dlg_RetryDialog.dismiss();
                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dlg_RetryDialog);
                }
            });
            dlg_RetryDialog.setOnCancelListener(context.getString(R.string.Caption_Button_No), new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    dlg_RetryDialog.dismiss();
                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dlg_RetryDialog);
                }
            });

            return dlg_RetryDialog;

        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);

            return null;
        }
    }

    public DialogTwoButton showOtherAgentLogin(Context context, final TwoButtonDialogOKCallback okBtnCallback, final TwoButtonDialogCancelCallback cancelBtnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_new_connect);
            String strContents = context.getString(R.string.OtherAgentLogin);

            final DialogTwoButton dlgTwoButton = new DialogTwoButton(context, strTitle, strContents, MyApplication.COLORMODE_BLUE);
            dlgTwoButton.setCancelable(false);
            dlgTwoButton.show();
            dlgTwoButton.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dlgTwoButton);

                    dlgTwoButton.dismiss();
                }
            });
            dlgTwoButton.setOnCancelListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dlgTwoButton);

                    dlgTwoButton.dismiss();
                }
            });
            return dlgTwoButton;
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
            return null;
        }
    }

    public DialogTwoButton showSdcardNoTopSelect(Context context, final TwoButtonDialogOKCallback okBtnCallback, final TwoButtonDialogCancelCallback cancelBtnCallback) {
        try {
            String strTitle = context.getString(R.string.external_dialog_title_guid);
            String strContents = context.getString(R.string.external_non_top_permission);

            final DialogTwoButton dlgTwoButton = new DialogTwoButton(context, strTitle, strContents, MyApplication.COLORMODE_BLUE);
            dlgTwoButton.setCancelable(false);
            dlgTwoButton.show();
            dlgTwoButton.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dlgTwoButton);
                    dlgTwoButton.dismiss();
                }
            });
            dlgTwoButton.setOnCancelListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dlgTwoButton);

                    dlgTwoButton.dismiss();
                }
            });
            return dlgTwoButton;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void showAgentShutdown(Context context, String strPCName, final TwoButtonDialogOKCallback okBtnCallback, final TwoButtonDialogCancelCallback cancelBtnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_exit);
            String strMessage = context.getString(R.string.Option_Dialog_Shutdown_Contents);
            strMessage = String.format(strMessage, strPCName);

            final DialogTwoButton dialog = new DialogTwoButton(context, strTitle, strMessage, MyApplication.COLORMODE_BLUE);
            dialog.show();
            dialog.setOnCancelListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dialog);

                    dialog.dismiss();
                }
            });
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dialog);

                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showLogoutFileTransferring(Context context, final TwoButtonDialogOKCallback okBtnCallback, final TwoButtonDialogCancelCallback cancelBtnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_logout);
            String strContents = context.getString(R.string.Dialog_FileSendingLogout);
            final DialogTwoButton dialog = new DialogTwoButton(context, strTitle, strContents, MyApplication.COLORMODE_BLUE);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dialog);
                    dialog.dismiss();
                }
            });
            dialog.setOnCancelListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dialog);

                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showExitFileTransferring(Context context, final TwoButtonDialogOKCallback okBtnCallback, final TwoButtonDialogCancelCallback cancelBtnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_exit);
            String strContents = context.getString(R.string.Dialog_FileSendingExit);
            final DialogTwoButton dialog = new DialogTwoButton(context, strTitle, strContents, MyApplication.COLORMODE_BLUE);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dialog);

                    dialog.dismiss();
                }
            });
            dialog.setOnCancelListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dialog);

                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showExit(Context context, final TwoButtonDialogOKCallback okBtnCallback, final TwoButtonDialogCancelCallback cancelBtnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_exit);
            String strContents = context.getString(R.string.Maint_Exit);
            final DialogTwoButton dialog = new DialogTwoButton(context, strTitle, strContents, MyApplication.COLORMODE_BLUE);
            dialog.show();
            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dialog);
                    dialog.dismiss();
                }
            });
            dialog.setOnCancelListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dialog);
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showAlreadyLogin(Context context, final TwoButtonDialogOKCallback okBtnCallback, final TwoButtonDialogCancelCallback cancelBtnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_logout);
            String strMessage = context.getString(R.string.Login_Already_Msg);
            final DialogTwoButton dialog = new DialogTwoButton(context, strTitle, strMessage, MyApplication.COLORMODE_BLUE);
            dialog.show();

            dialog.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dialog);

                    dialog.dismiss();
                }
            });
            dialog.setOnCancelListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dialog);

                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showWifiDirectFileTransferring(Context context, final TwoButtonDialogOKCallback okBtnCallback, final TwoButtonDialogCancelCallback cancelBtnCallback, ExceptionCallback exceptionCallback) {
        try {
            String strTitle = context.getString(R.string.dialog_title_cancel_exit);
            String strContents = context.getString(R.string.WifiDirect_Alert_FileTransferRunning);
            final DialogTwoButton dlg = new DialogTwoButton(context, strTitle, strContents, MyApplication.COLORMODE_BLUE);
            dlg.show();
            dlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    //와이파이다이렉트 실행 - 로그아웃 해야됨.

                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dlg);

                    dlg.dismiss();
                }
            });
            dlg.setOnCancelListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dlg);

                    dlg.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showFileAppendSend(
            Context context,
            String strFileName,
            final TwoButtonDialogOKCallback okBtnCallback,
            final TwoButtonDialogCancelCallback cancelBtnCallback,
            final TwoButtonDialogCheckboxCallback checkBoxCallback,
            ExceptionCallback exceptionCallback) {
        try {
            if (DialogTwoButton.instance != null && DialogTwoButton.instance.isShowing()) {
                DialogTwoButton.instance.dismiss();
            }

            final Bitmap bitmap_Check = BitmapFactory.decodeResource(context.getResources(), R.drawable.fileview_check);
            final Bitmap bitmap_UnCheck = BitmapFactory.decodeResource(context.getResources(), R.drawable.fileview_uncheck);

            String strTitle = context.getString(R.string.Dialog_FileView_Description_Title);
            String strContents = context.getString(R.string.FileAppendSend);
            String strCheckCaption = context.getString(R.string.FileAppendCheckbox);
            strContents = String.format(strContents, strFileName);
            final DialogTwoButton dlg = new DialogTwoButton(context, strTitle, strContents, MyApplication.COLORMODE_BLUE);
            dlg.show();
            dlg.setCancelable(false);
            final ImageButton ib_Check = dlg.getCheckBox();
            dlg.setOnCheckboxListener(strCheckCaption, new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Boolean isCheck = (Boolean) ib_Check.getTag();
                    if (isCheck) {
                        ib_Check.setImageBitmap(bitmap_UnCheck);
                        ib_Check.setTag((Boolean) false);
                        if (checkBoxCallback != null)
                            checkBoxCallback.onCheck(dlg, false);
                    } else {
                        ib_Check.setImageBitmap(bitmap_Check);
                        ib_Check.setTag((Boolean) true);
                        if (checkBoxCallback != null)
                            checkBoxCallback.onCheck(dlg, true);
                    }
                }
            });
            ib_Check.setImageBitmap(bitmap_UnCheck);
            ib_Check.setTag((Boolean) false);
            dlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dlg);

                    dlg.dismiss();
                }
            });
            dlg.setOnCancelListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dlg);

                    dlg.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    // pc -> agent  이어받기 다이어로그 체크박스
    public void showFileAppendReceive(
            Context context,
            String strFileName,
            final TwoButtonDialogOKCallback okBtnCallback,
            final TwoButtonDialogCancelCallback cancelBtnCallback,
            final TwoButtonDialogCheckboxCallback checkBoxCallback,
            ExceptionCallback exceptionCallback) {
        try {

            if (DialogTwoButton.instance != null && DialogTwoButton.instance.isShowing()) {
                DialogTwoButton.instance.dismiss();
            }
            final Bitmap bitmap_Check = BitmapFactory.decodeResource(context.getResources(), R.drawable.fileview_check);
            final Bitmap bitmap_UnCheck = BitmapFactory.decodeResource(context.getResources(), R.drawable.fileview_uncheck);

            String strTitle = context.getString(R.string.Dialog_FileView_Description_Title);
            String strContents = context.getString(R.string.FileAppendReceive);
            String strCheckCaption = context.getString(R.string.FileAppendCheckbox);
            strContents = String.format(strContents, strFileName);
            final DialogTwoButton dlg = new DialogTwoButton(context, strTitle, strContents, MyApplication.COLORMODE_BLUE);
            dlg.show();
            dlg.setCancelable(false);
            final ImageButton ib_Check = dlg.getCheckBox();
            dlg.setOnCheckboxListener(strCheckCaption, new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Boolean isCheck = (Boolean) ib_Check.getTag();
                    if (isCheck) {
                        ib_Check.setImageBitmap(bitmap_UnCheck);
                        ib_Check.setTag((Boolean) false);
                        if (checkBoxCallback != null)
                            checkBoxCallback.onCheck(dlg, false);
                    } else {
                        ib_Check.setImageBitmap(bitmap_Check);
                        ib_Check.setTag((Boolean) true);
                        if (checkBoxCallback != null)
                            checkBoxCallback.onCheck(dlg, true);
                    }
                }
            });
            ib_Check.setImageBitmap(bitmap_UnCheck);
            ib_Check.setTag((Boolean) false);
            dlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dlg);
                    dlg.dismiss();

                }
            });
            dlg.setOnCancelListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dlg);
                    dlg.dismiss();

                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showFileAppendReceiveJump(
            Context context,
            String strFileName,
            final TwoButtonDialogOKCallback okBtnCallback,
            final TwoButtonDialogCancelCallback cancelBtnCallback,
            final TwoButtonDialogCheckboxCallback checkBoxCallback,
            ExceptionCallback exceptionCallback) {
        try {

            final Bitmap bitmap_Check = BitmapFactory.decodeResource(context.getResources(), R.drawable.fileview_check);
            final Bitmap bitmap_UnCheck = BitmapFactory.decodeResource(context.getResources(), R.drawable.fileview_uncheck);

            String strTitle = context.getString(R.string.Dialog_Password_Title);
            String strContents = "";
            String strCheckCaption = context.getString(R.string.FileAppendCheckbox);
            strContents = String.format(strContents, strFileName);
            final DialogTwoButton dlg = new DialogTwoButton(context, strTitle, strContents, MyApplication.COLORMODE_BLUE);
            dlg.show();
            dlg.setCancelable(false);
            final ImageButton ib_Check = dlg.getCheckBox();
            dlg.setOnCheckboxListener(strCheckCaption, new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    Boolean isCheck = (Boolean) ib_Check.getTag();
                    if (isCheck) {
                        ib_Check.setImageBitmap(bitmap_UnCheck);
                        ib_Check.setTag((Boolean) false);
                        if (checkBoxCallback != null)
                            checkBoxCallback.onCheck(dlg, false);
                    } else {
                        ib_Check.setImageBitmap(bitmap_Check);
                        ib_Check.setTag((Boolean) true);
                        if (checkBoxCallback != null)
                            checkBoxCallback.onCheck(dlg, true);
                    }
                }
            });
            ib_Check.setImageBitmap(bitmap_UnCheck);
            ib_Check.setTag((Boolean) false);
            dlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dlg);

                    dlg.dismiss();
                }
            });
            dlg.setOnCancelListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dlg);

                    dlg.dismiss();
                }
            });
        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void showFileAppendFileid(
            Context context,
            String strFirstFilename,
            int nSkipListSize,
            final TwoButtonDialogOKCallback okBtnCallback,
            final TwoButtonDialogCancelCallback cancelBtnCallback,
            ExceptionCallback exceptionCallback) {
        try {


            String strTitle = context.getString(R.string.Dialog_FileView_Description_Title);
            String strContents = context.getString(R.string.FileAppendReceive);


            String strFilenameFormat = context.getString(R.string.FileAppendOthers);

            if (nSkipListSize == 1) {
                strFilenameFormat = strFirstFilename;
            } else {
                strFilenameFormat = String.format(strFilenameFormat, strFirstFilename, nSkipListSize);
            }

            strContents = String.format(strContents, strFilenameFormat);

            final DialogTwoButton dlg = new DialogTwoButton(context, strTitle, strContents, MyApplication.COLORMODE_BLUE);
            dlg.show();
            dlg.setCancelable(false);


            dlg.setOnOkListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dlg);

                    dlg.dismiss();
                }
            });
            dlg.setOnCancelListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dlg);

                    dlg.dismiss();
                }
            });

        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public void a(Context context, final TwoButtonDialogOKCallback okBtnCallback, final TwoButtonDialogCancelCallback cancelBtnCallback, ExceptionCallback exceptionCallback) {
        try {

        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    /**
     * 파일 제제
     *
     * @param context
     * @param okBtnCallback     확인 이벤트
     * @param cancelBtnCallback 취소이벤트
     * @param exceptionCallback 익셉션이벤트
     */
    public void showFileDeleteChoice(
            Context context,
            final TwoButtonDialogOKCallback okBtnCallback,
            final TwoButtonDialogCancelCallback cancelBtnCallback,
            ExceptionCallback exceptionCallback) {

        try {
            String strTitle = context.getString(R.string.delete_file_title);
            String strMessage = context.getString(R.string.FileView_Delete_Msg);
            final DialogTwoButton dialog = new DialogTwoButton(context, strTitle, strMessage, MyApplication.COLORMODE_BLUE);
            dialog.show();

            dialog.setOnCancelListener(context.getString(R.string.Caption_Button_Cancel), new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (cancelBtnCallback != null)
                        cancelBtnCallback.onCancel(dialog);
                    dialog.dismiss();
                }
            });

            //삭제
            dialog.setOnOkListener(context.getString(R.string.DarkBlue_Caption_Delete), new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (okBtnCallback != null)
                        okBtnCallback.onOK(dialog);

                    dialog.dismiss();
                }
            });

        } catch (Exception e) {
            if (exceptionCallback != null)
                exceptionCallback.onException(e);
        }
    }

    public interface ButtonCallback2 {
        public void onButtonClick(DialogTwoButton dlg);
    }

}
