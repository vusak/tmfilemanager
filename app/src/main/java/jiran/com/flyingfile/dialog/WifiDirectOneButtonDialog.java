package jiran.com.flyingfile.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import jiran.com.tmfilemanager.R;


public class WifiDirectOneButtonDialog extends Dialog
        implements
        OnClickListener {
    private String strTitle;
    private String strContents;

    private TextView tv_Title;
    private TextView tv_Contents;
    private Button btn_Ok;
    private ListView lv_DialogList;
    private LinearLayout ll_Check;
    private ImageButton ib_Check;
    private TextView tv_CheckContents;
    private TextView tv_Contents_Left;
    private TextView tv_GroupName;

    private WifiDirectOneButtonDialogListner listener = null;


    public WifiDirectOneButtonDialog(
            Context context,
            String strTitle,
            String strContents,
            WifiDirectOneButtonDialogListner listener
    ) {
        super(context, R.style.CustomDialog);
        // TODO Auto-generated constructor stub
        this.strTitle = strTitle;
        this.strContents = strContents;
        this.listener = listener;
    }


    public WifiDirectOneButtonDialog(
            Context context,
            String strTitle,
            String strContents
    ) {
        super(context, R.style.CustomDialog);
        // TODO Auto-generated constructor stub
        this.strTitle = strTitle;
        this.strContents = strContents;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        setContentView(R.layout.flyingfile_view_dialog_onebutton_blue);


        tv_Title = (TextView) findViewById(R.id.TextView_Title);
        tv_Contents = (TextView) findViewById(R.id.TextView_Contents);
        btn_Ok = (Button) findViewById(R.id.Button_Ok);
        lv_DialogList = (ListView) findViewById(R.id.ListView_DialogList);
        ll_Check = (LinearLayout) findViewById(R.id.LinearLayout_Check);
        ib_Check = (ImageButton) findViewById(R.id.ImageButton_Check);
        tv_CheckContents = (TextView) findViewById(R.id.TextView_CheckContents);
        tv_Contents_Left = (TextView) findViewById(R.id.TextView_Contents_Left);
        tv_GroupName = (TextView) findViewById(R.id.TextView_GroupName);

        tv_Contents_Left.setVisibility(View.GONE);
        tv_GroupName.setVisibility(View.GONE);
        tv_Title.setText(strTitle);
        tv_Contents.setText(strContents);

        if (ll_Check != null) {
            ll_Check.setVisibility(View.GONE);
        }

        if (lv_DialogList != null) {
            lv_DialogList.setVisibility(View.GONE);
        }
        btn_Ok.setOnClickListener(this);

        if (this.listener != null)
            this.listener.onOneClose();

        super.onCreate(savedInstanceState);
    }

    public void setAdapter(
            ArrayAdapter<?> adapter,
            OnItemClickListener listener
    ) {
        lv_DialogList.setVisibility(View.VISIBLE);
        lv_DialogList.setAdapter(adapter);
        lv_DialogList.setOnItemClickListener(listener);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        int viewID = v.getId();

        if (viewID == R.id.Button_Ok) {
            this.dismiss();
        }
    }


    public interface WifiDirectOneButtonDialogListner {
        public void onOneClose();
    }

}
