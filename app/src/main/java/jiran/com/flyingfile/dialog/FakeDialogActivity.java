package jiran.com.flyingfile.dialog;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;

import jiran.com.flyingfile.FlyingFileConst;
import jiran.com.tmfilemanager.R;

public class FakeDialogActivity extends Activity {


    public static final int FLAG_1 = 1;                //PC랑 파일 전송중임을 알리는 다이얼로그
    public static final int FLAG_2 = 2;                //와이파이다이렉트 소켓 연결 대기중을 알리는 프로그레스 다이얼로그
    public static final int FLAG_2_CLOSE = 3;        //위에꺼 다이얼로그 닫기

    public static final int FLAG_4 = 4;        //소켓 타임아웃

    public static final int FLAG_5 = 5;        //업데이트 훼이크 다이얼로그
    public static final int FLAG_6 = 6;        //와이파이 다이렉트 미지원 메세지
    public static FakeDialogFLAG5DialogListener flag5listener = null;
    private static ProgressDialog pDlg;
    private CustomDialog dlg_Custom;
    @SuppressWarnings("unused")
    private int nColorMode = 0;

    public static void setFlag5listener(FakeDialogFLAG5DialogListener flag5listener) {
        FakeDialogActivity.flag5listener = flag5listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        nColorMode = getIntent().getIntExtra("color", FlyingFileConst.DialogColor.COLORMODE_BLUE.ordinal());
        setDialog(getIntent());

        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // TODO Auto-generated method stub

        setDialog(intent);
        super.onNewIntent(intent);
    }

    public void setDialog(Intent intent) {
        int nFlag = intent.getIntExtra("flag", 0);

        switch (nFlag) {
            case FLAG_1: {
                OnClickListener leftListener = new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        if (dlg_Custom != null)
                            dlg_Custom.dismiss();
                    }
                };
                dlg_Custom = new CustomDialog(
                        this,
                        this.getString(R.string.dialog_title_file_send),
                        this.getString(R.string.Dialog_AlreadyFileTransfer),
                        this.getString(R.string.Caption_Button_Ok),
                        leftListener);

                if (dlg_Custom != null && dlg_Custom.isShowing()) {
                    dlg_Custom.dismiss();
                }
                dlg_Custom.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        // TODO Auto-generated method stub
                        FakeDialogActivity.this.finish();
                    }
                });
                dlg_Custom.show();
                break;
            }
            case FLAG_2: {
                if (pDlg != null && pDlg.isShowing()) {
                    pDlg.dismiss();
                    pDlg = null;
                }
                pDlg = new ProgressDialog(this);
                pDlg.setMessage(getString(R.string.MtoM_WaitConnect));
                pDlg.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        // TODO Auto-generated method stub
                        FakeDialogActivity.this.finish();
                    }
                });
                pDlg.setCancelable(false);
                pDlg.show();
                break;
            }
            case FLAG_2_CLOSE: {
                if (pDlg != null && pDlg.isShowing()) {
                    pDlg.dismiss();
                    pDlg = null;
                }
                this.finish();
                break;
            }
            case FLAG_4: {
                OnClickListener leftListener = new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        if (dlg_Custom != null)
                            dlg_Custom.dismiss();
                    }
                };
                dlg_Custom = new CustomDialog(
                        this,
                        this.getString(R.string.dialog_title_non_connect),
                        this.getString(R.string.MtoM_ConnectingFail),
                        this.getString(R.string.Caption_Button_Ok),
                        leftListener);

                if (dlg_Custom != null && dlg_Custom.isShowing()) {
                    dlg_Custom.dismiss();
                }
                dlg_Custom.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        // TODO Auto-generated method stub
                        FakeDialogActivity.this.finish();
                    }
                });
                dlg_Custom.show();
                break;
            }
            case FLAG_5: {
                try {
                    String strTitle = getString(R.string.Dialog_Password_Title);
                    String strContents = getString(R.string.Dialog_Update_Message);
                    final DialogOneButton dlg_Alert = new DialogOneButton(this, strTitle, strContents, Gravity.LEFT);
                    dlg_Alert.show();
                    dlg_Alert.setOnOkListener(getString(R.string.Caption_Button_Ok), new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            dlg_Alert.dismiss();
                        }
                    });
                    dlg_Alert.setOnDismissListener(new OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            // TODO Auto-generated method stub
                            if (flag5listener != null) {
                                flag5listener.onUpdateDialogDismiss();
                            }

                            try {
                                if (dlg_Alert != null && dlg_Alert.isShowing()) {
                                    dlg_Alert.dismiss();
                                }
                                FakeDialogActivity.this.finish();
                            } catch (Exception e) {
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
            case FLAG_6: {

                break;
            }

            default: {
                this.finish();
                break;
            }
        }
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }

    @Override
    public void finish() {
        // TODO Auto-generated method stub
        if (pDlg != null && pDlg.isShowing()) {
            pDlg.dismiss();
            pDlg = null;
        }
        super.finish();
    }


    public interface FakeDialogFLAG5DialogListener {

        public void onUpdateDialogDismiss();
    }

}
