package jiran.com.flyingfile.util;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class NetworkPingTest {

    private static NetworkPingTest instance = null;

    public static NetworkPingTest getInstance() {
        if (instance == null)
            instance = new NetworkPingTest();
        return instance;
    }

    public boolean isConnected(String strHost) {
        InetAddress in;

        try {
            in = InetAddress.getByName(strHost.toString());
        } catch (UnknownHostException e) {
            return false;
        }
        try {
            if (in.isReachable(5000)) {
                return true;
            } else {
                return false;
            }
        } catch (IOException e) {
            return false;
        }
    }

    public boolean isConnected(InetAddress in) {
        try {
            if (in.isReachable(5000)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

}
