package jiran.com.flyingfile.util;

import java.util.Comparator;

import jiran.com.flyingfile.model.FileItem;


/**
 * Created by jeon on 2016-10-13.
 */

public class FileItemCompareUtil {
    public static Comparator<? super FileItem> nameAsc = new Comparator<FileItem>() {

        public int compare(FileItem file1, FileItem file2) {

            if (file1.isDot() && !file2.isDot())
                return -1;
            else if (file2.isDot() && !file1.isDot())
                return 1;
            if (file1.isDirectory() && !file2.isDirectory())
                return -1;
            else if (file2.isDirectory() && !file1.isDirectory())
                return 1;

            if (file1.isDirectory()) {
                if (file2.isDirectory()) {
                    return String.valueOf(file1.getName().toLowerCase()).compareTo(file2.getName().toLowerCase());
                } else {
                    return -1;
                }
            } else {
                if (file2.isDirectory()) {
                    return 1;
                } else {
                    return String.valueOf(file1.getName().toLowerCase()).compareTo(file2.getName().toLowerCase());
                }
            }

        }
    };
    public static Comparator<? super FileItem> nameDesc = new Comparator<FileItem>() {

        public int compare(FileItem file1, FileItem file2) {

            if (file1.isDot() && !file2.isDot())
                return -1;
            else if (file2.isDot() && !file1.isDot())
                return 1;
            if (file1.isDirectory() && !file2.isDirectory())
                return -1;
            else if (file2.isDirectory() && !file1.isDirectory())
                return 1;
            if (file1.isDirectory()) {
                if (file2.isDirectory()) {
                    return String.valueOf(file1.getName().toLowerCase()).compareTo(file2.getName().toLowerCase()) * -1;
                } else {
                    return -1;
                }
            } else {
                if (file2.isDirectory()) {
                    return 1;
                } else {
                    return String.valueOf(file1.getName().toLowerCase()).compareTo(file2.getName().toLowerCase()) * -1;
                }
            }

        }
    };
    public static Comparator<? super FileItem> sizeAsc = new Comparator<FileItem>() {

        public int compare(FileItem file1, FileItem file2) {
            if (file1.isDot() && !file2.isDot())
                return -1;
            else if (file2.isDot() && !file1.isDot())
                return 1;
            if (file1.isDirectory() && !file2.isDirectory())
                return -1;
            else if (file2.isDirectory() && !file1.isDirectory())
                return 1;
            if (file1.isDirectory()) {
                if (file2.isDirectory()) {
                    return Long.valueOf(file1.length()).compareTo(file2.length());
                } else {
                    return -1;
                }
            } else {
                if (file2.isDirectory()) {
                    return 1;
                } else {
                    return Long.valueOf(file1.length()).compareTo(file2.length());
                }
            }

        }
    };
    public static Comparator<? super FileItem> sizeDesc = new Comparator<FileItem>() {

        public int compare(FileItem file1, FileItem file2) {
            if (file1.isDot() && !file2.isDot())
                return -1;
            else if (file2.isDot() && !file1.isDot())
                return 1;
            if (file1.isDirectory() && !file2.isDirectory())
                return -1;
            else if (file2.isDirectory() && !file1.isDirectory())
                return 1;
            if (file1.isDirectory()) {
                if (file2.isDirectory()) {
                    return Long.valueOf(file1.length()).compareTo(file2.length()) * -1;
                } else {
                    return -1;
                }
            } else {
                if (file2.isDirectory()) {
                    return 1;
                } else {
                    return Long.valueOf(file1.length()).compareTo(file2.length()) * -1;
                }
            }

        }
    };
    public static Comparator<? super FileItem> lastModifiedAsc = new Comparator<FileItem>() {

        public int compare(FileItem file1, FileItem file2) {
            if (file1.isDot() && !file2.isDot())
                return -1;
            else if (file2.isDot() && !file1.isDot())
                return 1;
            if (file1.isDirectory() && !file2.isDirectory())
                return -1;
            else if (file2.isDirectory() && !file1.isDirectory())
                return 1;
            if (file1.isDirectory()) {
                if (file2.isDirectory()) {
                    return Long.valueOf(file1.lastModified()).compareTo(file2.lastModified());
                } else {
                    return -1;
                }
            } else {
                if (file2.isDirectory()) {
                    return 1;
                } else {
                    return Long.valueOf(file1.lastModified()).compareTo(file2.lastModified());
                }
            }

        }
    };
    public static Comparator<? super FileItem> lastModifiedDesc = new Comparator<FileItem>() {

        public int compare(FileItem file1, FileItem file2) {
            if (file1.isDot() && !file2.isDot())
                return -1;
            else if (file2.isDot() && !file1.isDot())
                return 1;
            if (file1.isDirectory() && !file2.isDirectory())
                return -1;
            else if (file2.isDirectory() && !file1.isDirectory())
                return 1;
            if (file1.isDirectory()) {
                if (file2.isDirectory()) {
                    return Long.valueOf(file1.lastModified()).compareTo(file2.lastModified()) * -1;
                } else {
                    return -1;
                }
            } else {
                if (file2.isDirectory()) {
                    return 1;
                } else {
                    return Long.valueOf(file1.lastModified()).compareTo(file2.lastModified()) * -1;
                }
            }

        }
    };

    public static Comparator<? super FileItem> nameAsc2 = new Comparator<FileItem>() {

        public int compare(FileItem file1, FileItem file2) {
            return String.valueOf(file1.getName().toLowerCase()).compareTo(file2.getName().toLowerCase());
        }
    };
    public static Comparator<? super FileItem> nameDesc2 = new Comparator<FileItem>() {

        public int compare(FileItem file1, FileItem file2) {
            return String.valueOf(file1.getName().toLowerCase()).compareTo(file2.getName().toLowerCase()) * -1;
        }
    };
    public static Comparator<? super FileItem> sizeAsc2 = new Comparator<FileItem>() {

        public int compare(FileItem file1, FileItem file2) {
            return Long.valueOf(file1.length()).compareTo(file2.length());

        }
    };
    public static Comparator<? super FileItem> sizeDesc2 = new Comparator<FileItem>() {

        public int compare(FileItem file1, FileItem file2) {
            return Long.valueOf(file1.length()).compareTo(file2.length()) * -1;
        }
    };
    public static Comparator<? super FileItem> lastModifiedAsc2 = new Comparator<FileItem>() {

        public int compare(FileItem file1, FileItem file2) {
            return Long.valueOf(file1.lastModified()).compareTo(file2.lastModified());
        }
    };
    public static Comparator<? super FileItem> lastModifiedDesc2 = new Comparator<FileItem>() {

        public int compare(FileItem file1, FileItem file2) {
            return Long.valueOf(file1.lastModified()).compareTo(file2.lastModified()) * -1;
        }
    };
    public static Comparator<? super FileItem> sectionAsc = new Comparator<FileItem>() {

        public int compare(FileItem file1, FileItem file2) {
            return Long.valueOf(file1.getDateTaken()).compareTo(file2.getDateTaken()) * -1;
        }
    };


}
