package jiran.com.flyingfile.util;

import java.io.File;
import java.io.FileFilter;


public class FileFilterUtil {

    private static FileFilterUtil instance = null;

    public static FileFilterUtil getInstance() {
        if (instance == null)
            instance = new FileFilterUtil();
        return instance;
    }


    /**
     * 파일사이즈가 0이 아닌 모든 파일 & 모든 폴더
     *
     * @return
     */
    public FileFilter getBaseFileFilter() {
        return new FileFilter() {

            @Override
            public boolean accept(File pathname) {
                // TODO Auto-generated method stub

                //일반 파일인데 사이즈가 0인경우 스킵
                return filterCodeBase(pathname);
            }
        };
    }


    /**
     * 문서 파일 이거나 폴더인 경우(리커시브이기 때문에 폴더인경우도 필터 해야됨.
     *
     * @param target
     * @return
     */
    public FileFilter getDocumentFileOrFolderFilter() {
        return new FileFilter() {
            @Override
            public boolean accept(File pathname) {

                if (filterCodeOnlyFolder(pathname))
                    return true;
                else if (filterCodeDocument(pathname))
                    return true;
                else
                    return false;
            }
        };
    }


    public boolean filterCodeOnlyFolder(File target) {
        if (target == null)
            return false;

        if (!target.exists())
            return false;

        if (target.isDirectory())
            return true;
        else
            return false;
    }

    public boolean filterCodeDocument(File target) {
        if (!filterCodeOnlyFile(target))
            return false;

        return filterCodeDocument(target.getName());

    }

    public boolean filterCodeDocument(String target) {


        if (
                target.endsWith(".txt")
                        || target.endsWith(".pdf")
                        || target.endsWith(".doc")
                        || target.endsWith(".docx")
                        || target.endsWith(".xls")
                        || target.endsWith(".xlsx")
                        || target.endsWith(".ppt")
                        || target.endsWith(".pptx")
                        || target.endsWith(".hwp")
                ) {
            return true;
        } else
            return false;
    }

    public boolean filterCodeBase(File target) {
        if (!target.isDirectory() && target.length() == 0)
            return false;
        else
            return true;
    }

    public boolean filterCodeOnlyFile(File target) {
        if (target == null)
            return false;
        else if (!target.exists())
            return false;
        else if (target.length() == 0)
            return false;
        else if (target.isDirectory())
            return false;
        else
            return true;

    }

    public boolean filterCodeOnlyFile(String target) {
        if (target == null)
            return false;

        File file = new File(target);
        return filterCodeOnlyFile(file);
    }
}
