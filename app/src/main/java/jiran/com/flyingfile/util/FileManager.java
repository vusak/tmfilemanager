package jiran.com.flyingfile.util;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileFilter;
import java.io.UnsupportedEncodingException;
import java.text.Normalizer;

import jiran.com.flyingfile.custom.FileItemStringList;


@SuppressLint("NewApi")
@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class FileManager {

    private static FileManager instance = null;

    public static FileManager getInstance() {
        if (instance == null)
            instance = new FileManager();
        return instance;
    }

    public static String getRenameFileSavePath(String strPath) {

        String temp = strPath;
        boolean isNeedNormalize = Normalizer.isNormalized(temp, Normalizer.Form.NFD);
        if (isNeedNormalize) {
            temp = Normalizer.normalize(temp, Normalizer.Form.NFC);
        } else {
        }

        String strSuffix = null;

        int i = 0;
//		String result = strPath;
        String result = temp;

        while (true) {

//			File file = new File(result);

            File tempObj = new File(strPath);
            String strParentPath = tempObj.getParent();
            result = strParentPath + "/" + FileManager.getInstance().getAbleFileName(tempObj.getName(), strSuffix);

            File file = new File(result);

            //파일이 존재하지만 사이즈가 0인경우는 삭제해주자
            if (file.exists() && file.length() == 0) {
                file.delete();
            }


            if (file.exists()) {

                int nLastIndex = strPath.lastIndexOf('.');
                String strTempBeforePath;
                String strTempAfterPath;
                if (nLastIndex == -1) {
                    strTempBeforePath = strPath;
                    strTempAfterPath = "";
                } else {
                    strTempBeforePath = strPath.substring(0, nLastIndex);
                    strTempAfterPath = strPath.substring(nLastIndex);
                }
                result = strTempBeforePath + "(" + i + ")" + strTempAfterPath;
                strSuffix = "(" + i + ")";
                i++;

            } else {
                break;
            }
        }

        try {

            File tempObj = new File(strPath);
            String strParentPath = tempObj.getParent();
            result = strParentPath + "/" + FileManager.getInstance().getAbleFileName(tempObj.getName(), strSuffix);
        } catch (Exception e) {

        }

        return result;
    }

    public static boolean isRootDirectory(String path) {
        int idx = -1;
        int cnt = 0;
        do {
            idx = path.indexOf("/", idx + 1);
            if (idx != -1)
                cnt++;
        } while (idx != -1);

        if (cnt == 1)
            return true;
        else
            return false;
    }

    public File getSecondaryPackagePath(Context context) {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            return null;
        }

        File[] storage = context.getExternalFilesDirs(null);


        if (storage.length <= 1)
            return null;

        if (storage[1] == null)
            return null;


        File secondaryPath = storage[1];

        return secondaryPath.getParentFile();

    }

    public File getSecondaryStorage(Context context, String strPath) {

        File file = new File(strPath);
        return getSecondaryStorage(context, file);

    }

    public File getSecondaryStorage(Context context, File file) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT)
            return null;


        File[] storage = context.getExternalFilesDirs(null);
        if (storage.length <= 1)
            return null;

        if (storage[1] == null)
            return null;

        File secondaryStorage = storage[1];

        while (secondaryStorage.getAbsolutePath().split("/").length > 3) {
            secondaryStorage = secondaryStorage.getParentFile();
        }


        return secondaryStorage;
    }

    public String getAbleFileName(String strFileName, String strSuffix) {

        try {

            byte[] binary = strFileName.getBytes("utf-8");
            byte[] binarySuffix = null;
            boolean isCut = false;

            if (strSuffix != null)
                binarySuffix = strSuffix.getBytes("utf-8");

            int nLen = binary.length;
            if (binarySuffix != null)
                nLen += binarySuffix.length;

            byte[] wave = "~".getBytes("utf-8");


            while (nLen > (255 - wave.length)) {

                if (!isCut)
                    isCut = true;

                //확장자 앞에서 부터 잘라 나가야함
                String strTempFileName = "";
                String strExec = null;
                if (strFileName.contains(".")) {
                    String[] strSplit = strFileName.split("\\.");
                    for (int i = 0; i < strSplit.length - 1; i++) {
                        strTempFileName += strSplit[i];
                        if (i != strSplit.length - 2)
                            strTempFileName += ".";
                    }
//					strTempFileName = strSplit[0];

//					strExec = strSplit[1];
                    strExec = strSplit[strSplit.length - 1];
                } else {
                    strTempFileName = strFileName;
                }

                strTempFileName = strTempFileName.substring(0, strTempFileName.length() - 1);
                if (strExec != null) {
                    strFileName = strTempFileName + "." + strExec;
                } else {
                    strFileName = strTempFileName;
                }

                binary = strFileName.getBytes("utf-8");
                nLen = binary.length;

                if (binarySuffix != null)
                    nLen += binarySuffix.length;
            }

            if (isCut) {
                if (strFileName.contains(".")) {
                    String[] strSplit = strFileName.split("\\.");
                    String strTempFilename = "";
                    for (int i = 0; i < strSplit.length - 1; i++) {
                        strTempFilename += strSplit[i];
                        if (i != strSplit.length - 2)
                            strTempFilename += ".";
                    }

                    strTempFilename += "~";
                    if (strSuffix != null)
                        strTempFilename += strSuffix;

                    strFileName = strTempFilename + "." + strSplit[strSplit.length - 1];

                } else {

                    strFileName += "~";
                    if (strSuffix != null)
                        strFileName += strSuffix;
                }
            } else {
                if (strFileName.contains(".")) {
                    if (strSuffix != null) {
                        String[] strSplit = strFileName.split("\\.");
                        String strTempFilename = "";
                        for (int i = 0; i < strSplit.length - 1; i++) {
                            strTempFilename += strSplit[i];
                            if (i != strSplit.length - 2)
                                strTempFilename += ".";
                        }

//						strSplit[0] += strSuffix;
                        strTempFilename += strSuffix;

                        strFileName = strTempFilename + "." + strSplit[strSplit.length - 1];
                    }
                } else {

                    if (strSuffix != null) {
                        strFileName += strSuffix;
                    }
                }
            }

            return strFileName;

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            return null;
        }
    }

    @SuppressLint("DefaultLocale")
    public void executeFile(Context context, String path, String errorMsg) {
        //파일 실행 ....

        //파일 경로


        String selectFilePath = path;
        File file = new File(selectFilePath);

        MimeTypeMap mtm = MimeTypeMap.getSingleton();

        String fileName = file.getName();
        String extension = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()).toLowerCase();

        String mimeType = mtm.getMimeTypeFromExtension(extension);

        if (mimeType != null) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            //해당 파일의 확장자를 이용하여 mimeType을 찾고 해당 mimeType에 해당하는 액션을 수행한다.
            intent.setDataAndType(Uri.fromFile(file), mimeType);

            try {
                context.startActivity(intent);
            } catch (ActivityNotFoundException ane) {
                Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }

    public JSONObject getPrefFileidHeader(Context context, String strFileid) {

        String strObj = SharedPreferenceUtil.getInstance().getCache(context, strFileid);


        try {
            JSONObject obj = null;

            if (strObj == null) {
                obj = new JSONObject();
            } else {
                JSONParser parser = new JSONParser();
                obj = (JSONObject) parser.parse(strObj);
            }

            return obj;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean setPrefFileidHeader(Context context, String strFileid, String strJSON) {
        SharedPreferenceUtil.getInstance().setCache(context, strFileid, strJSON);

        return true;
    }

    public boolean removePrefFileidHeader(Context context, String strFileid) {

        SharedPreferenceUtil.getInstance().removeCache(context, strFileid);

        return true;
    }

    public void searchRecursiveAllPath(FileItemStringList res, String root) throws Exception {

        if (res == null)
            throw new Exception("arr is null");

        File fileRoot = new File(root);
        if (!fileRoot.exists())
            throw new Exception("file or path is not exist");

        File[] innerFiles = fileRoot.listFiles(new FileFilter() {

            @Override
            public boolean accept(File pathname) {
                if (!pathname.isDirectory() && pathname.length() == 0)
                    return false;
                else
                    return true;
            }
        });

        if (innerFiles.length == 0) {

            res.add(root);
        }

        for (int i = 0; i < innerFiles.length; i++) {
            if (innerFiles[i].isDirectory()) {
                searchRecursiveAllPath(res, innerFiles[i].getAbsolutePath());
            } else {
                res.add(innerFiles[i].getAbsolutePath());
            }
        }
    }


}
