package jiran.com.flyingfile.util;

import android.annotation.SuppressLint;

import java.io.File;

@SuppressLint("DefaultLocale")
public class FileTypeUtil {

    public static final int FLAG_DIRECTORY = 10;
    public static final int FLAG_PICTURE = 21;
    public static final int FLAG_MOVIE = 22;
    public static final int FLAG_MUSIC = 23;
    public static final int FLAG_DOCUMENT = 24;
    public static final int FLAG_OTHERS = 25;
    public static final int FLAG_APP = 26;
    public static final int FLAG_UNKNOWN = 0;

    public static final String FLAG_STRING_DIRECTORY = "Folder";
    public static final String FLAG_STRING_PICTURE = "Picture";
    public static final String FLAG_STRING_MOVIE = "Movie";
    public static final String FLAG_STRING_MUSIC = "Music";
    public static final String FLAG_STRING_DOCUMENT = "Document";
    public static final String FLAG_STRING_APP = "App";
    public static final String FLAG_STRING_UNKNOWN = "etc";

    public FileTypeUtil() {

    }

    public int getFilyType(File file) {
        String fileNameForType = file.getName();
        fileNameForType = fileNameForType.toLowerCase();
        if (file.isDirectory()) {
            //파일이 디렉토리인 경우
            return FLAG_DIRECTORY;
        } else {
            return getFilyType(fileNameForType);
        }
    }

    public int getFilyType(String fileNameForType) {
        if (fileNameForType != null) fileNameForType = fileNameForType.toLowerCase();
        if (fileNameForType.endsWith(".jpg") ||
                fileNameForType.endsWith(".jpeg") ||
                fileNameForType.endsWith(".png") ||
                fileNameForType.endsWith(".bmp") ||
                fileNameForType.endsWith(".gif") ||
                fileNameForType.endsWith(".tif") ||
                fileNameForType.endsWith(".tiff") ||
                fileNameForType.endsWith(".pcx")
                ) {
            //파일이 그림일 경우
            return FLAG_PICTURE;
        } else if (fileNameForType.endsWith("mp4") ||
                fileNameForType.endsWith("avi") ||
                fileNameForType.endsWith("mpeg") ||
                fileNameForType.endsWith("asf") ||
                fileNameForType.endsWith("wmv") ||
                fileNameForType.endsWith("mpg") ||
                fileNameForType.endsWith("k3g") ||
                fileNameForType.endsWith("3gp") ||
                fileNameForType.endsWith("skm") ||
                fileNameForType.endsWith("mkv") ||
                fileNameForType.endsWith("swf") ||
                fileNameForType.endsWith("mov") ||
                fileNameForType.endsWith("flv")) {
            //파일이 동영상일 경우
            return FLAG_MOVIE;
        } else if (fileNameForType.endsWith("mp3") ||
                fileNameForType.endsWith("wav") ||
                fileNameForType.endsWith("mid") ||
                fileNameForType.endsWith("flac") ||
                fileNameForType.endsWith("ape") ||
                fileNameForType.endsWith("wma") ||
                fileNameForType.endsWith("ogg")) {
            //파일이 음악일 경우
            return FLAG_MUSIC;
        } else if (fileNameForType.endsWith(".hwp")
                || fileNameForType.endsWith(".doc")
                || fileNameForType.endsWith(".docx")
                || fileNameForType.endsWith(".xls")
                || fileNameForType.endsWith(".xlsx")
                || fileNameForType.endsWith(".ppt")
                || fileNameForType.endsWith(".pptx")
                || fileNameForType.endsWith(".txt")
                || fileNameForType.endsWith(".pdf")
                ) {
            return FLAG_DOCUMENT;
        } else if (fileNameForType.endsWith(".apk")) {
            return FLAG_APP;
        } else {
            String[] regx = fileNameForType.split("\\.");
            if (regx.length >= 2) {
                return FLAG_OTHERS;
            } else {
                return FLAG_UNKNOWN;
            }
        }
    }

    public String getStrFileType(File file) {

        final int flag_FileType = getFilyType(file);

        String fileNameForType = file.getName();
        String[] regx = fileNameForType.split("\\.");

        switch (flag_FileType) {
            case FLAG_DIRECTORY:
                return "Folder";
            case FLAG_DOCUMENT:
                return "Document";
            case FLAG_MOVIE:
                return "Movie";
            case FLAG_MUSIC:
                return "Music";
            case FLAG_OTHERS: {
                String ext = regx[regx.length - 1];
                if (ext.length() > 10)
                    return "etc";
                else
                    return regx[regx.length - 1];
            }
            case FLAG_PICTURE:
                return "Picture";

            case FLAG_APP:
                return "APP";
            case FLAG_UNKNOWN:
            default:
                return "etc";
        }
    }

    public int getStrFileTypeNum(File file) {

        final int flag_FileType = getFilyType(file);

        String fileNameForType = file.getName();
        String[] regx = fileNameForType.split("\\.");

        switch (flag_FileType) {
            case FLAG_DIRECTORY:
                return FLAG_DIRECTORY;
            case FLAG_DOCUMENT:
                return FLAG_DOCUMENT;
            case FLAG_MOVIE:
                return FLAG_MOVIE;
            case FLAG_MUSIC:
                return FLAG_MUSIC;
            case FLAG_OTHERS:
                return FLAG_OTHERS;
            case FLAG_PICTURE:
                return FLAG_PICTURE;
            case FLAG_UNKNOWN:
                return FLAG_UNKNOWN;
            default:
                return FLAG_UNKNOWN;
        }
    }

}
