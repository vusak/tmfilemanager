package jiran.com.flyingfile.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


public class SharedPreferenceUtil {

    //SharedPreperence 이름
    public static final String PREFERENCE_NAME = "FLYINGMAGNER";
    public static final int FLAG_CATEGORYMODE_EXPLORER = 0;
    public static final int FLAG_CATEGORYMODE_PICTURE = 1;
    public static final int FLAG_CATEGORYMODE_MUSIC = 2;
    public static final int FLAG_CATEGORYMODE_MOVIE = 3;
    public static final int FLAG_CATEGORYMODE_DOCUMENT = 4;
    public static final int FLAG_CATEGORYMODE_APK = 5;
    private static final boolean DEFAULT_OPTION_VALUE_AUTOLOGIN = true;
    private static final boolean DEFAULT_OPTION_VALUE_AUTORUN = false;
    private static final boolean DEFAULT_OPTION_VALUE_FILERECEIVEPASSWORD = false;
    private static final boolean DEFAULT_OPTION_VALUE_WIFI_FILETRANSFER = false;
    private static final String DELAY_SHOW_AD_TMBR = "delay_show_ad_tmbr";
    private static final int DEFAULT_DELAY_SHOW_AD_TMBR = -1;
    private static SharedPreferenceUtil instance = null;

    public static SharedPreferenceUtil getInstance() {
        if (instance == null)
            instance = new SharedPreferenceUtil();
        return instance;
    }

    private SharedPreferences get(Context context) {

        return context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    private void setBoolean(SharedPreferences pref, String key, boolean value) {
        Editor edit = pref.edit();
        edit.putBoolean(key, value);
        edit.commit();
    }

    private void setString(SharedPreferences pref, String key, String value) {
        Editor edit = pref.edit();
        edit.putString(key, value);
        edit.commit();
    }

    private void setInt(SharedPreferences pref, String key, int value) {
        Editor edit = pref.edit();
        edit.putInt(key, value);
        edit.commit();
    }

    private void setLong(SharedPreferences pref, String key, long value) {
        Editor edit = pref.edit();
        edit.putLong(key, value);
        edit.commit();
    }

    private void remove(SharedPreferences pref, String key) {
        Editor edit = pref.edit();
        edit.remove(key);
        edit.commit();
    }

    /**
     * 아이디 얻어오기
     *
     * @param context
     * @return
     */
    public String getAccount(Context context) {
        SharedPreferences pref = get(context);

        return pref.getString("id", null);
    }

    /**
     * 아이디 저장
     *
     * @param context
     * @param email
     */
    public void setAccount(Context context, String email) {
        SharedPreferences pref = get(context);
        setString(pref, "id", email);
    }

    /**
     * 패스워드 얻어오기
     *
     * @param context
     * @return
     */
    public String getPassword(Context context) {
        SharedPreferences pref = get(context);
        return pref.getString("password", null);
    }

    /**
     * 패스워드 저장
     *
     * @param context
     * @param password
     */
    public void setPassword(Context context, String password) {
        SharedPreferences pref = get(context);
        setString(pref, "password", password);
    }

    /**
     * 서버경유 사용량 값 얻기
     *
     * @param context
     * @return
     */
    public long getMaxStorage(Context context) {
        SharedPreferences pref = get(context);
        return pref.getLong("nmaxstorage", 0);
    }

    /**
     * 서버경유 사용량 저장
     *
     * @param context
     * @param maxstorage
     */
    public void setMaxStorage(Context context, long maxstorage) {
        SharedPreferences pref = get(context);
        setLong(pref, "nmaxstorage", maxstorage);
    }

    /**
     * 파일전송 다이얼로그 자동 닫기
     *
     * @param context
     * @return
     */
    public boolean getFileTransferDialogAutoClose(Context context) {
        SharedPreferences pref = get(context);
        return pref.getBoolean("filetransfer_dialog", false);
    }

    public void setFileTransferDialogAutoClose(Context context, boolean option) {
        SharedPreferences pref = get(context);
        setBoolean(pref, "filetransfer_dialog", option);
    }

    /**
     * SD카드 경로가 바뀌었는지(마운트해제)확인
     *
     * @param context
     * @return
     */
    public boolean getDefaultPathChange(Context context) {
        SharedPreferences pref = get(context);
        return pref.getBoolean("defaultpathchange", false);
    }

    public void setDefaultPathChange(Context context, boolean option) {
        SharedPreferences pref = get(context);
        setBoolean(pref, "defaultpathchange", option);
    }

    /**
     * 단말기 부팅시 자동실행 옵션
     *
     * @param context
     * @return
     */
    public boolean getStartWhenDeviceBoot(Context context) {
        SharedPreferences pref = get(context);
        return pref.getBoolean("bootstate", DEFAULT_OPTION_VALUE_AUTORUN);
    }

    public void setStartWhenDeviceBoot(Context context, boolean option) {
        SharedPreferences pref = get(context);
        setBoolean(pref, "bootstate", option);
    }

    /**
     * 모바일 받은파일함 경로
     *
     * @param context
     * @return
     */
    public String getMobileDefaultPath(Context context) {
        SharedPreferences pref = get(context);
        return pref.getString("defaultpath", null);
    }

    public void setMobileDefaultPath(Context context, String strPath) {
        SharedPreferences pref = get(context);
        setString(pref, "defaultpath", strPath);
    }

    /**
     * 모바일 받은파일함 Uri (5.0 이상 버전 방식.)
     *
     * @param context
     * @return
     */
    public String getMobileDefaultUri(Context context) {
        SharedPreferences pref = get(context);
        return pref.getString("defaultUri", null);
    }

    public void setMobileDefaultUri(Context context, String strPath) {
        SharedPreferences pref = get(context);
        setString(pref, "defaultUri", strPath);
    }

    /**
     * ???(파일ID모듈에서 사용함)
     *
     * @param context
     * @param strFileid
     * @return
     */
    public String getCache(Context context, String strFileid) {
        SharedPreferences pref = get(context);
        return pref.getString("cache_" + strFileid, null);
    }

    public void setCache(Context context, String strFileid, String value) {
        SharedPreferences pref = get(context);
        setString(pref, "cache_" + strFileid, value);
    }

    public void removeCache(Context context, String strFileid) {
        SharedPreferences pref = get(context);
        remove(pref, "cache_" + strFileid);
    }

    /**
     * 와이파이 연결일 경우만 파일전송 허용
     *
     * @param context
     * @return
     */
    public boolean getFileTransferInWifi(Context context) {
        SharedPreferences pref = get(context);
        return pref.getBoolean("justwififiletransfer", DEFAULT_OPTION_VALUE_WIFI_FILETRANSFER);
    }

    /**
     * @param option   true : Wifi에서만 파일전송
     * @param callback 저장후 리턴
     */
    public void setFileTransferInWifi(Context context, boolean option, OnFileTransferInWifiConfiguration callback) {
        if (callback != null) {
            SharedPreferences pref = get(context);
            setBoolean(pref, "justwififiletransfer", option);
        }

        if (callback != null) {
            callback.onOption(option);
        }

    }

    /**
     * 파일중복 처리
     * 0. 다른 이름으로 저장
     * 1. 건너뛰기
     *
     * @param context
     * @return
     */
    public int getDuplication(Context context) {
        SharedPreferences pref = get(context);
        return pref.getInt("Duplication", EDuplication.RENAME.ordinal());
    }

    public void setDuplication(Context context, int category_flag) {
        SharedPreferences pref = get(context);
        setInt(pref, "Duplication", category_flag);
    }

    /**
     * 파일 전송시 암호화.
     *
     * @param context
     * @return 0 : 리스트 모드 1 : 그리드 모드
     */
    public boolean getCrypt(Context context) {
        SharedPreferences pref = get(context);
        return pref.getBoolean("Crypt", false);
    }

    public void setCrypt(Context context, boolean category_flag) {
        SharedPreferences pref = get(context);
        setBoolean(pref, "Crypt", category_flag);
    }

    /**
     * WiFi-Direct 끊어짐 여부.
     *
     * @param context
     */
    public boolean getWiFiDirectDisConnect(Context context) {
        SharedPreferences pref = get(context);
        return pref.getBoolean("wifi_direct", true);
    }

    public void setWiFiDirectDisConnect(Context context, boolean category_flag) {
        SharedPreferences pref = get(context);
        setBoolean(pref, "wifi_direct", category_flag);
    }

    public void setTab(Context context, int nCategory, int mode) {
        switch (nCategory) {
            case FLAG_CATEGORYMODE_PICTURE:
                setPictureTab(context, mode);
                break;
            case FLAG_CATEGORYMODE_DOCUMENT:
                setDocumentTab(context, mode);
                break;
            case FLAG_CATEGORYMODE_MUSIC:
                setMusicTab(context, mode);
                break;
            case FLAG_CATEGORYMODE_EXPLORER:
                setExplorerTab(context, mode);
                break;
            case FLAG_CATEGORYMODE_MOVIE:
                setMovieTab(context, mode);
                break;
        }
    }

    public int getTab(Context context, int nCategory) {
        switch (nCategory) {
            case FLAG_CATEGORYMODE_PICTURE:
                return getPictureTab(context);
            case FLAG_CATEGORYMODE_DOCUMENT:
                return getDocumentTab(context);
            case FLAG_CATEGORYMODE_MUSIC:
                return getMusicTab(context);
            case FLAG_CATEGORYMODE_EXPLORER:
                return getExplorerTab(context);
            case FLAG_CATEGORYMODE_MOVIE:
                return getMovieTab(context);
        }
        return 0;
    }

    /**
     * 마지막 사용자 카테고리값 (탐색기)
     *
     * @param context
     * @return 0 : 리스트 모드 1 : 그리드 모드
     */
    public int getExplorerTab(Context context) {
        SharedPreferences pref = get(context);
        return pref.getInt("ExplorerTab", 0);
    }

    public void setExplorerTab(Context context, int category_flag) {
        SharedPreferences pref = get(context);
        setInt(pref, "ExplorerTab", category_flag);
    }

    /**
     * 마지막 사용자 카테고리값 (사진)
     *
     * @param context
     * @return 0 : 리스트 모드 1 : 그리드 모드
     */
    public int getPictureTab(Context context) {
        SharedPreferences pref = get(context);
        return pref.getInt("PictureTab", 2);
    }

    public void setPictureTab(Context context, int category_flag) {
        SharedPreferences pref = get(context);
        setInt(pref, "PictureTab", category_flag);
    }

    /**
     * 마지막 사용자 카테고리값 (음악)
     *
     * @param context
     * @return 0 : 리스트 모드 1 : 그리드 모드
     */
    public int getMusicTab(Context context) {
        SharedPreferences pref = get(context);
        return pref.getInt("MusicTab", 0);
    }

    public void setMusicTab(Context context, int category_flag) {
        SharedPreferences pref = get(context);
        setInt(pref, "MusicTab", category_flag);
    }

    /**
     * 마지막 사용자 카테고리값 (동영상)
     *
     * @param context
     * @return 0 : 리스트 모드 1 : 그리드 모드
     */
    public int getMovieTab(Context context) {
        SharedPreferences pref = get(context);
        return pref.getInt("MovieTab", 2);
    }

    public void setMovieTab(Context context, int category_flag) {
        SharedPreferences pref = get(context);
        setInt(pref, "MovieTab", category_flag);
    }

    /**
     * 마지막 사용자 카테고리값 (문서)
     *
     * @param context
     * @return 0 : 리스트 모드 1 : 그리드 모드
     */
    public int getDocumentTab(Context context) {
        SharedPreferences pref = get(context);
        return pref.getInt("DocumentTab", 0);
    }

    public void setDocumentTab(Context context, int category_flag) {
        SharedPreferences pref = get(context);
        setInt(pref, "DocumentTab", category_flag);
    }

    /**
     * 사용자 Tab 정보 기억(FileID)
     *
     * @param context
     * @return
     */
    public int getTabFileId(Context context) {
        SharedPreferences pref = get(context);
        return pref.getInt("TabFileId1", SharedPreferenceUtil.FLAG_CATEGORYMODE_PICTURE);
    }

    public void setTabFileId(Context context, int value) {
        SharedPreferences pref = get(context);
        setInt(pref, "TabFileId1", value);
    }

    /**
     * 사용자 Tab 정보 기억(PC와 연결)
     *
     * @param context
     * @return
     */
    public int getTabWiFiDirect(Context context) {
        SharedPreferences pref = get(context);
        return pref.getInt("TabWiFiDirect1", SharedPreferenceUtil.FLAG_CATEGORYMODE_PICTURE);
    }

    public void setTabWiFiDirect(Context context, int value) {
        SharedPreferences pref = get(context);
        setInt(pref, "TabWiFiDirect1", value);
    }

    /**
     * Banner AD TM Browser
     */
    public void setDelayShowAdTMBrowser(Context context, long value) {
        SharedPreferences pref = get(context);
        setLong(pref, DELAY_SHOW_AD_TMBR, value);
    }

    public boolean isDelayedShowADTMBrowser(Context context) {
        SharedPreferences pref = get(context);
        long delay = pref.getLong(DELAY_SHOW_AD_TMBR, DEFAULT_DELAY_SHOW_AD_TMBR);
        long curr = System.currentTimeMillis();

        return delay == -1 || Math.abs(curr - delay) > (1000 * 60 * 60 * 24);
//        return delay == -1 || Math.abs(curr - delay) > (1000 * 10);
    }

    public enum EDuplication {
        RENAME, JUMP;
    }

    public interface OnFileTransferInWifiConfiguration {
        public void onOption(boolean option);
    }
}
