package jiran.com.flyingfile.util;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * 단위변환 유틸 클래스
 *
 * @author Bae
 */
@SuppressLint("DefaultLocale")
public class UnitTransfer {

    /**
     * 파일의 용량을 long타입으로 받아서 적절한 크기의 단위로 변환하여 리턴함
     *
     * @param 용량표기
     * @return 단위 변환된 용량표기
     */
    public static final int MEM_CONVERSE_FLAG_BYTE = 0;
    public static final int MEM_CONVERSE_FLAG_KB = 1;
    public static final int MEM_CONVERSE_FLAG_MB = 2;
    public static final int MEM_CONVERSE_FLAG_GB = 3;
    private static UnitTransfer instance = null;

    public static UnitTransfer getInstance() {
        if (instance == null)
            instance = new UnitTransfer();
        return instance;
    }

    /**
     * int형을 byte배열로 변환 하는 메소드
     *
     * @param integer
     * @return
     */
    public byte[] intToByteArray(final int integer) {

        ByteBuffer buff = ByteBuffer.allocate(Integer.SIZE / 8);
        buff.putInt(integer);
        buff.order(ByteOrder.LITTLE_ENDIAN);
        return buff.array();
    }

    /**
     * int형을 byte배열로 변환 하는 메소드
     *
     * @param integer
     * @return
     */
    public byte[] intToByteArray(final int integer, ByteOrder byteOrder) {

        ByteBuffer buff = ByteBuffer.allocate(Integer.SIZE / 8);
        buff.putInt(integer);
        buff.order(byteOrder);
        return buff.array();
    }

    /**
     * 바이트 배열을 int형으로 변환 하는 메소드
     *
     * @param bytes
     * @return
     */
    public int byteArrayToInt(byte[] bytes) {

        final int size = Integer.SIZE / 8;
        ByteBuffer buff = ByteBuffer.allocate(size);
        final byte[] newBytes = new byte[size];
        for (int i = 0; i < size; i++) {
            if (i + bytes.length < size) {
                newBytes[i] = (byte) 0x00;
            } else {
                newBytes[i] = bytes[i + bytes.length - size];
            }
        }
        buff = ByteBuffer.wrap(newBytes);
        buff.order(ByteOrder.LITTLE_ENDIAN);
//		buff.order(ByteOrder.BIG_ENDIAN);
        return buff.getInt();
    }

    /**
     * 바이트 배열을 int형으로 변환 하는 메소드
     * little엔디안 : C언어에서 사용하는 byte오더
     * big엔디안 : java에서 사용하는 byte오더
     *
     * @param bytes
     * @return
     */
    public int byteArrayToInt(byte[] bytes, ByteOrder byteOrder) {

        final int size = Integer.SIZE / 8;
        ByteBuffer buff = ByteBuffer.allocate(size);
        final byte[] newBytes = new byte[size];
        for (int i = 0; i < size; i++) {
            if (i + bytes.length < size) {
                newBytes[i] = (byte) 0x00;
            } else {
                newBytes[i] = bytes[i + bytes.length - size];
            }
        }
        buff = ByteBuffer.wrap(newBytes);
        buff.order(byteOrder);
        return buff.getInt();
    }

    /**
     * 파일을 바이트배열로 변환하는 메소드
     *
     * @param file
     * @return
     * @throws IOException
     */
    @SuppressWarnings("resource")
    public byte[] getBytesFromFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);
        long length = file.length();

        // You cannot create an array using a long type.
        // It needs to be an int type.
        // Before converting to an int type, check
        // to ensure that file is not larger than Integer.MAX_VALUE.
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        // Create the byte array to hold the data
        byte[] bytes = new byte[(int) length];
        // Read in the bytes
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }
        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }
        // Close the input stream and return bytes
        is.close();
        return bytes;
    }

    /**
     * 비트맵을 byte배열로
     *
     * @param bitmap
     * @return
     */
    public byte[] bitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    /**
     * 바이트배열을 비트맵으로
     *
     * @param byteArray
     * @return
     */
    public Bitmap byteArrayToBitmap(byte[] byteArray) {
        Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        return bitmap;
    }

    public MemoryStruct longToMemoryStruct(long memory, int nForceChange) {

        double dSize = memory;
        double dRes = 0;

        if (nForceChange >= 0) {
            MemoryStruct struct = new MemoryStruct();
            struct.nType = nForceChange;
            double dResult = dSize;
            switch (nForceChange) {
                case MemoryStruct.GB: {
                    dResult = dResult / (double) 1024;
//				return struct;
                }
                case MemoryStruct.MB: {
                    dResult = dResult / (double) 1024;
                }
                case MemoryStruct.KB: {
                    dResult = dResult / (double) 1024;
                    break;
                }
                default: {

                }
            }
            if (nForceChange == MemoryStruct.KB) {
                dResult = Math.ceil(dResult);
            }
            struct.dSize = dResult;
            return struct;
        }

        //B
        if (memory == 0) {
            MemoryStruct result = new MemoryStruct();
            result.nType = MemoryStruct.KB;
            result.dSize = 0;
            return result;
        }
        if (memory < 1024) {
            MemoryStruct result = new MemoryStruct();
            result.nType = MemoryStruct.KB;
            result.dSize = 1;
            return result;
        }

        //KB
        dRes = dSize / (double) 1024;

        //KB를 1024로 나누었는데 1보다 작으면 KB 표시
        if ((dRes / (double) 1024) < 1) {
            MemoryStruct result = new MemoryStruct();
            result.nType = MemoryStruct.KB;
            result.dSize = Math.ceil(dRes);
            return result;
        }

        //MB
        dRes = dRes / (double) 1024;

        //MB를 1024로 나누었는데 1보다 작으면 그냥 MB로 표시(소수점 2째자리 까지)
        if ((dRes / (double) 1024) < 1) {
            MemoryStruct result = new MemoryStruct();
            result.nType = MemoryStruct.MB;
            result.dSize = dRes;

//			String res = String.format("%.2f MB", dRes);
            return result;
        }

        //GB
        dRes = dRes / (double) 1024;

        MemoryStruct result = new MemoryStruct();
        result.nType = MemoryStruct.GB;
        result.dSize = dRes;

//		String res = String.format("%.2f GB", dRes);
        return result;
    }

    public String memoryConverse2(long byte_memory) {

        MemoryStruct struct = longToMemoryStruct(byte_memory, -1);


        switch (struct.nType) {
            case MemoryStruct.KB: {
                return struct.dSize + " KB";
            }
            case MemoryStruct.MB:
            case MemoryStruct.GB: {
                double size = struct.dSize;
                size = (int) (size * 10);
                size /= 10;
                return size + (struct.nType == MemoryStruct.MB ? " MB" : " GB");
            }

            default:
                return null;
        }

    }

    public String memoryConverseToMB(long nSize) {

        long nSizeMB = nSize / (1024 * 1024);

        return nSizeMB + "MB";

    }

    public ProgressStruct progressConverse(long nProgress, long nMax) {

        @SuppressWarnings("unused")
        double dProgress = nProgress;
        @SuppressWarnings("unused")
        double dTotal = nMax;
        String strFormat = "%d%% [ %s / %s ]";

        int nCurrentPercentage = 0;
        if (nProgress > nMax) {
            nProgress = nMax;
        }

        nCurrentPercentage = (int) (((double) nProgress / (double) nMax) * 100);

        String strResProgress = memoryConverse2(nProgress);
        String strResTotal = memoryConverse2(nMax);
        String strFileSize = String.format(strFormat, nCurrentPercentage, strResProgress, strResTotal);

        ProgressStruct resStruct = new ProgressStruct();
        resStruct.per = nCurrentPercentage;
        resStruct.progress = strFileSize;
        return resStruct;
    }

    // hex to byte[]
    public byte[] hexToByteArray(String hex) {
        if (hex == null || hex.length() == 0) {
            return null;
        }

        byte[] ba = new byte[hex.length() / 2];
        for (int i = 0; i < ba.length; i++) {
            ba[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return ba;
    }

    // byte[] to hex
    public String byteArrayToHex(byte[] ba) {
        if (ba == null || ba.length == 0) {
            return null;
        }

        StringBuffer sb = new StringBuffer(ba.length * 2);
        String hexNumber;
        for (int x = 0; x < ba.length; x++) {
            hexNumber = "0" + Integer.toHexString(0xff & ba[x]);

            sb.append(hexNumber.substring(hexNumber.length() - 2));
        }
        return sb.toString();
    }

    public byte[] longtoByteArray(long data) {
        return new byte[]{
                (byte) ((data >> 56) & 0xff),
                (byte) ((data >> 48) & 0xff),
                (byte) ((data >> 40) & 0xff),
                (byte) ((data >> 32) & 0xff),
                (byte) ((data >> 24) & 0xff),
                (byte) ((data >> 16) & 0xff),
                (byte) ((data >> 8) & 0xff),
                (byte) ((data >> 0) & 0xff),
        };
    }

    public long byteArraytoLong(byte[] data) {
        if (data == null || data.length != 8)
            return 0x0;

        return (long) ((long) (0xff & data[0]) << 56 |
                (long) (0xff & data[1]) << 48 |
                (long) (0xff & data[2]) << 40 |
                (long) (0xff & data[3]) << 32 |
                (long) (0xff & data[4]) << 24 |
                (long) (0xff & data[5]) << 16 |
                (long) (0xff & data[6]) << 8 |
                (long) (0xff & data[7]) << 0);
    }

    public byte[] chartoByteArray(char data) {
        return new byte[]{
                (byte) ((data >> 8) & 0xff),
                (byte) ((data >> 0) & 0xff),
        };
    }

    public char byteArraytoChar(byte[] data) {
        if (data == null || data.length != 2)
            return 0x0;

        return (char)
                ((long) (0xff & data[0]) << 8 |
                        (long) (0xff & data[1]) << 0);
    }

    public class MemoryStruct {
        public static final int KB = 0;
        public static final int MB = 1;
        public static final int GB = 2;

        int nType = 0;
        double dSize = 0;
    }

    public class ProgressStruct {
        public int per;
        public String progress;
    }

}
