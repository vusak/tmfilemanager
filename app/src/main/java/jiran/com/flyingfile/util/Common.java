package jiran.com.flyingfile.util;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Video.Thumbnails;
import android.util.Log;

import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import jiran.com.flyingfile.model.FileItem;
import jiran.com.tmfilemanager.MyApplication;


@SuppressLint({"DefaultLocale", "UseSparseArrays"})
public class Common {

    private static Common instance = null;
    boolean isUseFileTransfer = false;
    /**
     * 에이젼트에서 파일송수신 및 파일리스트 송수신 작업요청을 취소 한다.
     */
    private boolean fileListSendable = true;

    public static Common getInstance() {
        if (instance == null)
            instance = new Common();
        return instance;
    }

    public static boolean isNullorEmpty(String targetString) {
        if (targetString == null || targetString.trim().equals("") || targetString.trim().equals("null"))
            return true;

        return false;
    }

    public static void functionExit(final Context context) {
        ProcessExitUtil.getInstance().processExit(context, null);
    }

    /**
     * Process가 실행중인지 여부 확인.
     *
     * @param context, packageName
     * @return true/false
     */
    public static boolean isRunningProcess(Context context, String packageName) {
        boolean isRunning = false;
        ActivityManager actMng = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> list = actMng.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo rap : list) {
            if (rap.processName.equals(packageName)) {
                isRunning = true;
                break;
            }
        }
        return isRunning;
    }

    public boolean isUseFileTransfer() {
        return isUseFileTransfer;
    }

    public void setUseFileTransfer(boolean isUseFileTransfer) {
        this.isUseFileTransfer = isUseFileTransfer;
    }

    /**
     * 폴더 속 파일 갯수 얻는 함수
     */
    public long getFileCntInFoler(File obj) {
        if (!obj.isDirectory())
            return 0;

        File[] files = obj.listFiles(new FileFilter() {

            @Override
            public boolean accept(File pathname) {
                // TODO Auto-generated method stub
                try {
                    if (pathname.length() == 0)
                        return false;
                    else
                        return true;
                } catch (Exception e) {
                    return false;
                }
            }
        });
        long nCnt = 0;
        try {
            nCnt = files.length;
        } catch (Exception e) {
            nCnt = 0;
        }

        return nCnt;
    }

    public boolean isRootDirectory(String path) {
        int idx = -1;
        int cnt = 0;
        do {
            idx = path.indexOf("/", idx + 1);
            if (idx != -1)
                cnt++;
        } while (idx != -1);

        if (cnt == 1)
            return true;
        else
            return false;
    }

    /**
     * 미디어 스토어 사진 쿼리
     *
     * @param context
     * @return
     */
    private Cursor getImageMediaCursor(Context context) {
        Cursor mPictureCursor;
        String[] proj = {MediaStore.Images.Media._ID,
                MediaStore.Images.Media.DATA,
                MediaStore.Images.Media.DATE_MODIFIED,
                MediaStore.Images.Media.DATE_TAKEN
        };
        mPictureCursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                proj,
                null,
                null,
                MediaStore.Images.Media.DATE_MODIFIED + " desc");
        return mPictureCursor;
    }

    /**
     * 특정 폴더 미디어 스토어 사진 쿼리
     *
     * @param context
     * @return
     */

    private Map<Integer, String> getImageMediaThumbnailQuery(Context context) {
        //이미지아이디, 썸네일 경로
        Map<Integer, String> mapThumbnail = new HashMap<Integer, String>();
        ContentResolver resolver = context.getContentResolver();

        //썸네일 먼저 가져오자
        //썸네일 프로바이더 쿼리
        Cursor rightCursor = resolver.query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, null, null, null, null);
        if (rightCursor == null)
            return null;

        while (rightCursor.moveToNext()) {
            int nImageID = rightCursor.getInt(rightCursor.getColumnIndex(MediaStore.Images.Thumbnails.IMAGE_ID));
            String strThumbPath = rightCursor.getString(rightCursor.getColumnIndex(MediaStore.Images.Thumbnails.DATA));

            mapThumbnail.put(nImageID, strThumbPath);
        }
        rightCursor.close();
        return mapThumbnail;
    }

    public List<FileItem> functionSearchPictureFileItemList(
            Context context,
            boolean isAddThumbnail
    ) {
        List<FileItem> result = new ArrayList<FileItem>();

        //이미지아이디, 썸네일 경로
        Map<Integer, String> mapThumbnail = null;

        if (isAddThumbnail) {
            mapThumbnail = getImageMediaThumbnailQuery(context);
            if (mapThumbnail == null)
                return null;
        }


        //요청한 상위 경로가 null이다. 모든 이미지 파일을 검색해야함
        //이미지 컨텐트프로바이더에 쿼리
        Cursor leftCursor = getImageMediaCursor(context);

        if (leftCursor == null)
            return null;


        //커서조이너 믿을게 못되는듯..
        while (leftCursor.moveToNext()) {
            String strRealPath = leftCursor.getString(leftCursor.getColumnIndex(MediaStore.Images.Media.DATA));
            int nID = leftCursor.getInt(leftCursor.getColumnIndex(MediaStore.Images.Media._ID));
            long date = leftCursor.getLong(leftCursor.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN));


            if (!FileFilterUtil.getInstance().filterCodeOnlyFile(strRealPath))
                continue;

            FileItem item = new FileItem(strRealPath);
            if (date != 0) {
                item.setDateTaken(date);
            }
            if (isAddThumbnail) {
                //썸네일 이미지 가져오자
                String strThumbnail = mapThumbnail.get(nID);
                if (FileFilterUtil.getInstance().filterCodeOnlyFile(strThumbnail)) {
                    item.setStrThumbnailPath(strThumbnail);
                }
            }

            result.add(item);
        }
        leftCursor.close();


        return result;
    }

    private Cursor getVideoMediaCursor(Context context) {
        Cursor mVideoCursor;
        String[] proj = {MediaStore.Video.Media._ID,
                MediaStore.Video.Media.DATA,
                MediaStore.Video.Media.DATE_MODIFIED
        };
        mVideoCursor = context.getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, proj, null, null, MediaStore.Video.Media.DATE_MODIFIED + " desc");
        return mVideoCursor;
    }

    private Map<Integer, String> getVideoMediaThumbnailQuery(Context context) {

        ContentResolver resolver = context.getContentResolver();

        //이미지아이디, 썸네일 경로
        Map<Integer, String> mapThumbnail = new HashMap<Integer, String>();

        //썸네일 먼저 가져오자
        //썸네일 프로바이더 쿼리
        Cursor rightCursor = resolver.query(Thumbnails.EXTERNAL_CONTENT_URI, null, null, null, null);
        if (rightCursor == null)
            return null;

        while (rightCursor.moveToNext()) {
            int nImageID = rightCursor.getInt(rightCursor.getColumnIndex(Thumbnails.VIDEO_ID));
            String strThumbPath = rightCursor.getString(rightCursor.getColumnIndex(Thumbnails.DATA));

            mapThumbnail.put(nImageID, strThumbPath);

        }

        rightCursor.close();

        return mapThumbnail;

    }

    private String forceCreateViedoThumbnail(Context context, int nIDX, String strVideoPath) {
        try {
            ContentResolver resolver = context.getContentResolver();

            //미디어 스캔을 통해서도 썸네일이 생성이 안되있다.
            //						//썸네일을 직접 만들어 주자
            Bitmap bitmapThumbnail = ThumbnailUtils.createVideoThumbnail(strVideoPath, Thumbnails.MICRO_KIND);

            String strThumbnailFolder = Environment.getExternalStorageDirectory().getAbsolutePath() + "/.thumbnail";
            String strThumbnail = strThumbnailFolder + "/" + System.currentTimeMillis() + ".jpg";

            File folder = new File(strThumbnailFolder);
            if (!folder.exists()) {
                DocumentUtil.mkdirs(context, folder);
            }

            File file = new File(strThumbnail);
            file.createNewFile();


            FileOutputStream out = new FileOutputStream(file);
            bitmapThumbnail.compress(CompressFormat.JPEG, 100, out);

            ContentValues value = new ContentValues();
            value.put(Thumbnails.VIDEO_ID, nIDX);
            value.put(Thumbnails.DATA, strThumbnail);
            resolver.insert(Thumbnails.EXTERNAL_CONTENT_URI, value);

            return strThumbnail;

        } catch (Exception e) {
            Log.d("aa", "error");
            return null;
        }
    }

    public List<FileItem> functionSearchVideoFileItemList(
            Context context,
            boolean isAddThumbnail
    ) {
        List<FileItem> result = new ArrayList<FileItem>();

        //이미지아이디, 썸네일 경로
        Map<Integer, String> mapThumbnail = null;

        if (isAddThumbnail) {
            mapThumbnail = getVideoMediaThumbnailQuery(context);
            if (mapThumbnail == null)
                return null;
        }

        //이미지 컨텐트프로바이더에 쿼리
        Cursor leftCursor = getVideoMediaCursor(context);

        if (leftCursor == null)
            return null;

        //커서조이너 믿을게 못되는듯..
        while (leftCursor.moveToNext()) {
            String strRealPath = leftCursor.getString(leftCursor.getColumnIndex(MediaStore.Video.Media.DATA));
            int nID = leftCursor.getInt(leftCursor.getColumnIndex(MediaStore.Video.Media._ID));

            if (!FileFilterUtil.getInstance().filterCodeOnlyFile(strRealPath))
                continue;


            FileItem item = new FileItem(strRealPath);


            //썸네일 이미지 가져오자
            if (isAddThumbnail) {
                String strThumbnail = mapThumbnail.get(nID);
                if (!FileFilterUtil.getInstance().filterCodeOnlyFile(strThumbnail)) {
                    strThumbnail = forceCreateViedoThumbnail(context, nID, strRealPath);
                }
                item.setStrThumbnailPath(strThumbnail);
            }

            result.add(item);
        }
        leftCursor.close();

        return result;
    }

    /**
     * 앱 목록 가져오기.
     */
    public List<FileItem> functionSearchAPP(
            Context context
    ) {
        final Intent main = new Intent(Intent.ACTION_MAIN, null);
        main.addCategory(Intent.CATEGORY_LAUNCHER);
        final List<ResolveInfo> packagelist = context.getPackageManager().queryIntentActivities(
                main, 0);

        ArrayList<String> packageNames = new ArrayList<String>();


        ArrayList<FileItem> systemList = new ArrayList<>();
        ArrayList<FileItem> customList = new ArrayList<>();
        for (ResolveInfo pi : packagelist) {
            boolean isLive = false;
            for (int i = 0; i < systemList.size(); i++) {
                if (systemList.get(i).getAbsolutePath().equals(pi.activityInfo.applicationInfo.publicSourceDir)) {
                    isLive = true;
                }
            }
            for (int i = 0; i < customList.size(); i++) {
                if (customList.get(i).getAbsolutePath().equals(pi.activityInfo.applicationInfo.publicSourceDir)) {
                    isLive = true;
                }
            }
            if (isLive) continue;
            try {
                FileItem f1 = new FileItem(
                        pi.activityInfo.applicationInfo.publicSourceDir);

                f1.setStrFileType(String.valueOf(pi.activityInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM)); //시스템앱 여부.
                f1.setStrThumbnailPath((String) pi.activityInfo.applicationInfo.loadLabel(context.getPackageManager()));
                if ((pi.activityInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {
                    systemList.add(f1);
                } else if ((pi.activityInfo.applicationInfo.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0) {
                    systemList.add(f1);
                } else {
                    customList.add(0, f1);
                }
                f1.setApplicationInfo(pi.activityInfo.applicationInfo);
                packageNames.add(pi.activityInfo.packageName);
            } catch (Exception e) {
            }
        }

        Collections.sort(systemList, new Comparator<FileItem>() {
            @Override
            public int compare(FileItem lhs, FileItem rhs) {
                return String.valueOf(lhs.getStrThumbnailPath().toLowerCase()).compareTo(rhs.getStrThumbnailPath().toLowerCase());
            }
        });
        Collections.sort(customList, new Comparator<FileItem>() {
            @Override
            public int compare(FileItem lhs, FileItem rhs) {
                return String.valueOf(lhs.getStrThumbnailPath().toLowerCase()).compareTo(rhs.getStrThumbnailPath().toLowerCase());
            }
        });
        customList.addAll(systemList);
        return customList;
    }

    private Cursor getAudioMediaCursor(Context context) {
        Cursor mVideoCursor;
        String[] proj = {MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DATE_MODIFIED,
                MediaStore.Audio.Media.ALBUM_ID
        };
        mVideoCursor = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, proj, null, null, MediaStore.Audio.Media.DATE_MODIFIED + " desc");

        return mVideoCursor;
    }

    private Map<Integer, String> getAudioMediaThumbnailQuery(Context context) {


        ContentResolver resolver = context.getContentResolver();
        //이미지아이디, 썸네일 경로
        Map<Integer, String> mapThumbnail = new HashMap<Integer, String>();

        //썸네일 먼저 가져오자
        //썸네일 프로바이더 쿼리
        Cursor rightCursor = resolver.query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI, null, null, null, null);
        if (rightCursor == null)
            return null;

        while (rightCursor.moveToNext()) {
            int nImageID = rightCursor.getInt(rightCursor.getColumnIndex(MediaStore.Audio.Albums._ID));
            String strThumbPath = rightCursor.getString(rightCursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART));

            mapThumbnail.put(nImageID, strThumbPath);

        }
        rightCursor.close();

        return mapThumbnail;

    }

    public List<FileItem> functionSearchAudioFileItemList(
            Context context,
            boolean isAddThumbnail
    ) {
        List<FileItem> result = new ArrayList<FileItem>();

        //이미지아이디, 썸네일 경로

        Map<Integer, String> mapThumbnail = null;

        if (isAddThumbnail) {
            mapThumbnail = getAudioMediaThumbnailQuery(context);
            if (mapThumbnail == null)
                return null;
        }

        //이미지 컨텐트프로바이더에 쿼리
        Cursor leftCursor = getAudioMediaCursor(context);

        if (leftCursor == null)
            return null;

        //커서조이너 믿을게 못되는듯..
        while (leftCursor.moveToNext()) {
            String strRealPath = leftCursor.getString(leftCursor.getColumnIndex(MediaStore.Audio.Media.DATA));
            int nID = leftCursor.getInt(leftCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));

            if (!FileFilterUtil.getInstance().filterCodeOnlyFile(strRealPath))
                continue;

            FileItem item = new FileItem(strRealPath);

            if (isAddThumbnail) {
                //썸네일 이미지 가져오자
                String strThumbnail = mapThumbnail.get(nID);
                if (FileFilterUtil.getInstance().filterCodeOnlyFile(strThumbnail)) {
                    item.setStrThumbnailPath(strThumbnail);
                }
            }
            result.add(item);
        }
        leftCursor.close();


        return result;
    }

    public List<FileItem> functionSearchAllFileItemList(
            Context context,
            String folderRoot,
            FileFilter filter,
            DocumentSearchListener listener
    ) {
        ArrayList<FileItem> result = new ArrayList<FileItem>();
        Log.e("KHY", "functionSearchAllFileItemList: ");
        List<FileItem> list = functionSearchFileItemList(folderRoot, filter);
        if (list == null)
            return result;

        for (int i = 0; i < list.size(); i++) {
            if (listener != null) {
                if (!listener.onAbleListSend())
                    return null;
            }

            FileItem fileObj = new FileItem(list.get(i).getAbsolutePath());
            if (fileObj.isDirectory()) {
                List<FileItem> temp = functionSearchAllFileItemList(
                        context, fileObj.getAbsolutePath(),
                        filter,
                        listener
                );
                result.addAll(temp);
            } else {
                fileObj.setStrFileType("Document");
                result.add(fileObj);
                if (listener != null) {
                    if (!listener.onSearchDocument(result))
                        return null;
                }
            }
        }
        return result;
    }

    public boolean isFileListSendable() {
        return fileListSendable;
    }

    public void setFileListSendable(boolean fileListSendable) {
        this.fileListSendable = fileListSendable;
    }

    public List<FileItem> functionSearchFileItemList(
            File root,
            FileFilter filter
    ) {
        return functionSearchFileItemList(null, root.getAbsolutePath(), filter);
    }

    public List<FileItem> functionSearchFileItemList(
            String root,
            FileFilter filter
    ) {
        return functionSearchFileItemList(null, root, filter);
    }

    public List<FileItem> functionSearchFileItemList(
            File root
    ) {
        return functionSearchFileItemList(null, root.getAbsolutePath(), null);
    }

    public List<FileItem> functionSearchFileItemList(
            String strPath
    ) {
        return functionSearchFileItemList(null, strPath, null);
    }

    /**
     * 일반적으로는 null리턴이 되지 않으며 null이 리턴되는경우는 경로가 존재하지 않는경우 이다.
     *
     * @param context
     * @param strPath
     * @param filter
     * @return
     */
    public List<FileItem> functionSearchFileItemList(
            Context context,
            String strPath,
            FileFilter filter
    ) {

        //기본 파일함 경로를 file클래스를 활용하여 이하 파일들을 리스트에 임시 저장(a)
        //폴더는 제외
        //기본파일함 경로를 활용하여 각각 테이블 검색(이미지, 영상, 음악)
        //각각 cursor를 루프하여 그 이하 서브폴더에 있는 파일들은 제외하면서 검색하다가 기본파일함 경로에 딱 일치 하는 파일이 있으면 result리스트에 저장 하면서 a리스트에서 삭제
        //모든 카테고리를 확인 하였으면 나머지 a리스트에 있는 파일들을 result리스트에 저장하여 리턴


        //경로가 먼저 존재하는지 확인하고 존재하지 않으면 null을 리턴하자
        File rootFile = new File(strPath);
        if (strPath == null || rootFile == null || !rootFile.exists())
            return null;

        List<FileItem> result = new ArrayList<FileItem>();

        List<String> targetFileList = new ArrayList<String>();
        File targetFile = new File(strPath);

        File[] targetFileArray = null;
        if (filter == null)
            targetFileArray = targetFile.listFiles();
        else
            targetFileArray = targetFile.listFiles(filter);


        Map<Integer, String> mapThumbnail = null;


        if (targetFileArray != null) {
            //폴더는 제외하고 기본파일함경로에 있는 파일들을 리스트에 임시 저장
            //re : 원래는 wifi다이렉트화면에서 받은파일함 용도로 쓰려고 했지만 기본 탐색기에서 썸네일 추가하면서 폴더 제외부분은 외부에서 해주도록 하자.
            for (int i = 0; i < targetFileArray.length; i++) {
                //root경로인경우 경로 안쪽의 파일 갯수가 0이면 빼자
                if (Common.getInstance().isRootDirectory(strPath)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && targetFileArray[i].getAbsolutePath().equals("/storage/emulated")) {
                        targetFileList.add(targetFileArray[i].getAbsolutePath());
                    } else {
                        File[] files = targetFileArray[i].listFiles();
                        if (files != null && files.length > 0) {
                            targetFileList.add(targetFileArray[i].getAbsolutePath());
                        }
                    }
                } else {
                    targetFileList.add(targetFileArray[i].getAbsolutePath());
                }
            }
        }


        for (int i = 0; i < targetFileList.size(); i++) {
            FileItem item = new FileItem(targetFileList.get(i));
            result.add(item);
        }


        //넥서스 ㅈㄹ같은 폰에서 /storage/emulated 경로를 읽지 못한다. 스펙인지 뭔진 모르겠지만 없으면 "0" 폴더를 강제로 때려 넣자
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((strPath.equals("/storage/emulated")) && result.size() == 0) {
                FileItem tempFolder = new FileItem("/storage/emulated/0");
                tempFolder.setDirectory(true);
                result.add(tempFolder);
            }
        }


        return result;

    }

    //받은 root 경로를 기준으로 파일필터에 해당하는 하위의 모든 파일아이템을 구한다.
    private void recursiveAllFileList(
            List<FileItem> res,
            FileItem root,
            FileFilter filter
    ) throws Exception {

        if (res == null)
            throw new Exception("arr is null");

        if (!root.exists())
            throw new Exception("file or path is not exist");

//		File[] innerFiles = null;
        List<FileItem> innerFiles = null;
        if (filter == null)
            innerFiles = functionSearchFileItemList(root);
        else
            innerFiles = functionSearchFileItemList(root, filter);
        root.listFiles(filter);

        for (int i = 0; i < innerFiles.size(); i++) {
            FileItem item = innerFiles.get(i);
            if (item.isDirectory()) {
                recursiveAllFileList(
                        res,
                        item,
                        filter
                );
            } else {
                if (item.length() != 0) {
                    res.add(item);
                }
            }
        }
    }

    private void recursiveAllFileList(
            List<FileItem> res,
            String root,
            FileFilter filter
    ) throws Exception {
        FileItem rootItem = new FileItem(root);
        recursiveAllFileList(res, rootItem, filter);
    }

    /**
     * 언어 코드값 가져오기
     */
    public int functionGetLocale(Context context) {

        Locale systemLocale = context.getResources().getConfiguration().locale;
        String strLanguage = systemLocale.getLanguage();
        if (strLanguage.toLowerCase().equals(Locale.KOREA.getLanguage()) || strLanguage.toLowerCase().equals(Locale.KOREAN.getLanguage())) {
            return 1042;
        } else if (strLanguage.toLowerCase().equals(Locale.JAPANESE.getLanguage()) || strLanguage.toLowerCase().equals(Locale.JAPAN.getLanguage())) {
            return 1041;
        } else {
            return 0;
        }
    }

    /**
     * 받은 파일함 경로 생성
     *
     * @param context
     */
    public void functionCreateReceivedPath(Context context) {
        String strDefaultPath = MyApplication.getInstance().getReceivedStorage(context);
        File file = new File(strDefaultPath);
        if (!file.exists()) {
            DocumentUtil.mkdirs(context, file);
        }
    }

    public boolean isAvailableNetwork(Context context) {
        try {

            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (networkInfo.isConnectedOrConnecting())
                return true;
            else
                return false;


        } catch (Exception e) {
            // TODO: handle exception
            return false;
        }

    }

    public String getMobileIp(Context context) {
        String strMobileIP = "";
        try {
            ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo mobileNetwork = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            NetworkInfo wifiNetwork = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);


            if (mobileNetwork != null && mobileNetwork.isConnected())
                strMobileIP = NetworkUtil.getInstance().getLocalIpAddress();
            else if (wifiNetwork != null && wifiNetwork.isConnected())
                strMobileIP = NetworkUtil.getInstance().getNetworkInfo(context).getIpAddress();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return strMobileIP;
    }

    public int findFreePort() {
        ServerSocket socket = null;
        try {
            socket = new ServerSocket(0);
            socket.setReuseAddress(true);
            int port = socket.getLocalPort();
            try {
                socket.close();
            } catch (IOException e) {
                // Ignore IOException on close()
            }
            return port;
        } catch (IOException e) {
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                }
            }
        }
        return -1;
    }

    public int getCurrentVersionCode(Context context) {
        //현재 어플 버젼 변수
        int version = 0;
        PackageInfo pinfo;
        try {
            //현재 어플 버젼 가져오는거
            pinfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);

            //어플 버젼 가져와서 저장
            version = pinfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            version = 0;
        }

        return version;
    }

    public interface DocumentSearchListener {
        public boolean onAbleListSend();

        public boolean onSearchDocument(List<FileItem> result);
    }
}
