package jiran.com.flyingfile.util;

import android.content.Context;

import java.util.Locale;

public class LanguageUtil {

    private static LanguageUtil instance = null;

    public static LanguageUtil getInstance() {
        if (instance == null)
            instance = new LanguageUtil();
        return instance;
    }

    public Language getLanguage(Context context) {
        if (context == null)
            return Language.ETC;

        Locale systemLocale = context.getResources().getConfiguration().locale;
        String strLanguage = systemLocale.getLanguage();
        if (strLanguage.toLowerCase().equals(Locale.KOREAN.getLanguage()))
            return Language.KOREAN;
        else if (strLanguage.toLowerCase().equals(Locale.JAPANESE.getLanguage()))
            return Language.JAPANESE;
        else
            return Language.ETC;
    }

    public enum Language {
        ETC, KOREAN, JAPANESE
    }
}
