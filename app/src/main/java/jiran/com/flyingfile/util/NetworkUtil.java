package jiran.com.flyingfile.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo.State;
import android.net.wifi.WifiManager;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import jiran.com.flyingfile.model.LocalNetworkInfoDomain;

public class NetworkUtil {

    public static final int NETWORK_FLAG_UNCONNECT = 0;
    public static final int NETWORK_FLAG_WIFI = 1000;
    public static final int NETWORK_FLAG_MOBILENETWORK = 2000;

    private static NetworkUtil instance = null;

    public static NetworkUtil getInstance() {
        if (instance == null)
            instance = new NetworkUtil();

        return instance;
    }

    public int getNetworkMode(Context context) {
        try {
            ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            State wifi = conMan.getNetworkInfo(1).getState();    //wifi

            if (wifi == State.CONNECTED || wifi == State.CONNECTING) {
                return NETWORK_FLAG_WIFI;
            }

            State mobile = conMan.getNetworkInfo(0).getState();    //모바일네트워크
            if (mobile == State.CONNECTED || mobile == State.CONNECTING) {
                return NETWORK_FLAG_MOBILENETWORK;
            }
        } catch (NullPointerException e) {
            return NETWORK_FLAG_UNCONNECT;
        }

        return NETWORK_FLAG_UNCONNECT;
    }

    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public LocalNetworkInfoDomain getNetworkInfo(Context context) {
        try {
            WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

            DhcpInfo d = wm.getDhcpInfo();


            String dns1 = String.valueOf(intoIp(d.dns1));
            String dns2 = String.valueOf(intoIp(d.dns2));
            String gateway = String.valueOf(intoIp(d.gateway));
            String ipAddress = String.valueOf(intoIp(d.ipAddress));
            String leaseDuration = String.valueOf(d.leaseDuration);
            String subnetmask = String.valueOf(intoIp(d.netmask));
            String serverIP = String.valueOf(intoIp(d.serverAddress));

            return new LocalNetworkInfoDomain(dns1, dns2, gateway, ipAddress, leaseDuration, subnetmask, serverIP);
        } catch (Exception e) {
            return null;
        }
    }

    public String intoIp(int i) {
        //아이피를 xxx.xxx.xxx.xxx의 형태로 변환
        String result = (i & 0xFF) + "." +
                ((i >> 8) & 0xFF) + "." +
                ((i >> 16) & 0xFF) + "." +
                ((i >> 24) & 0xFF);

        return result;
    }

}
