package jiran.com.flyingfile.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.provider.DocumentFile;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import jiran.com.flyingfile.model.FileItem;

/**
 * Created by jeon on 2017-02-06.
 */

public class DocumentUtil {
    /**
     * @param context
     * @param canonicalPath 절대경로
     * @return path 방식을 uri content scheme 방식으로 변경한다.
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static DocumentFile getDocumentFile(@NonNull Context context, final String canonicalPath) {

        String sdCardUri = SharedPreferenceUtil.getInstance().getMobileDefaultUri(context);
        if (sdCardUri == null) {
            return null;
        }
        Uri treeUri = Uri.parse(sdCardUri);
        if (treeUri == null) {
            return null;
        }
        String baseFolder = null;
        if (baseFolder == null) {
            baseFolder = getExtSdCardFolder(context, canonicalPath);
        }
        if (baseFolder == null) {
            return null;
        }
        String relativePath = canonicalPath.substring(baseFolder.length() + 1);
        DocumentFile document = DocumentFile.fromTreeUri(context, treeUri);
        String[] parts = relativePath.split("\\/");
        for (int i = 0; i < parts.length; i++) {
            DocumentFile nextDocument = document.findFile(parts[i]);
            if (nextDocument == null) {
                if (i < parts.length - 1) {
                } else {
                    nextDocument = document.createFile("*/*", parts[i]);
                }
            }
            document = nextDocument;
        }
        return document;
    }

    public static DocumentFile getSubDocument(DocumentFile document, String fileName) {
        int length = fileName.length();
        for (DocumentFile doc : document.listFiles()) {
            String uri = doc.getUri().toString();
            String a = doc.getUri().toString().substring(uri.length() - length, uri.length());
            if (a.equals(fileName)) {
                return doc;
            }
        }

        return null;
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static String getExtSdCardFolder(Context context, @NonNull final String file) {
        String[] extSdPaths = getExtSdCardPaths(context);
        try {
            for (String extSdPath : extSdPaths) {
                if (file.startsWith(extSdPath)) {
                    return extSdPath;
                }
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static String[] getExtSdCardPaths(Context context) {
        List<String> paths = new ArrayList<>();
        for (File file : context.getExternalFilesDirs("external")) {
            if (file != null && !file.equals(context.getExternalFilesDir("external"))) {
                int index = file.getAbsolutePath().lastIndexOf("/Android/data");
                if (index < 0) {
                } else {
                    String path = file.getAbsolutePath().substring(0, index);
                    try {
                        path = new File(path).getCanonicalPath();
                    } catch (IOException e) {
                    }
                    paths.add(path);
                }
            }
        }
        return paths.toArray(new String[paths.size()]);
    }

    public static boolean mkdirs(Context context, File file) {
        if (file == null) return false;
        String defaultUri = SharedPreferenceUtil.getInstance().getMobileDefaultUri(context);
        if (defaultUri == null) { //일반 폴더 생성.
            return file.mkdirs();
        } else { // uri 방식 폴더 생성.
            try {
                int indexOfuri = file.getAbsolutePath().indexOf(FileManager.getInstance().getSecondaryStorage(context, "").getName());
                String mobileUri = file.getAbsolutePath().substring((indexOfuri + FileManager.getInstance().getSecondaryStorage(context, "").getName().length() + 1), file.getAbsolutePath().length());
                String[] split = mobileUri.split("/");
                DocumentFile doc = DocumentFile.fromTreeUri(context, Uri.parse(defaultUri));
                for (String item : split) {
                    DocumentFile doc2 = doc.findFile(item);
                    if (doc2 == null || doc2.isFile()) {
                        doc.createDirectory(item);
                    }
                    DocumentFile temp = doc.findFile(item);
                    if (temp == null) break;
                    doc = temp;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }
    }

    public static void getChildAllUri(Context context, String uri, HashMap<String, Uri> alName) {
        if (uri.startsWith("/")) {
            uri = uri.substring(1, uri.length());
        }
        String defaultUri = SharedPreferenceUtil.getInstance().getMobileDefaultUri(context);
        DocumentFile doc = DocumentFile.fromTreeUri(context, Uri.parse(defaultUri));
        if (!TextUtils.isEmpty(uri)) {
            String[] splite = uri.split("/");
            for (int i = 0; i < splite.length; i++) {
                doc = doc.findFile(splite[i]);
            }
        }
        String parentPath = null;
        for (DocumentFile item : doc.listFiles()) {
            Uri itemUri = item.getUri();
            String encodeResult = "";

            try {
                encodeResult = URLDecoder.decode(itemUri.toString(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            int splite1 = encodeResult.lastIndexOf("/");
            int splite2 = encodeResult.lastIndexOf(":");
            if (splite1 < splite2) splite1 = splite2;
            parentPath = encodeResult.substring(splite1 + 1, encodeResult.length());

            alName.put(parentPath, itemUri);
        }
    }

    public static FileItem.DELETE documentDelete(Context context, File file, HashMap<String, Uri> name) {
        try {


            if (file.isDirectory()) {
                Uri uri = name.get(file.getName());
                long size = getDeleteFolderSize(file);
                if (size == 0) {
                    DocumentFile doc = DocumentFile.fromSingleUri(context, uri);
                    boolean isDel = doc.delete();
                    if (isDel) {
                        return FileItem.DELETE.SUCCESS;
                    }
                } else {
                    return FileItem.DELETE.NONEMPTY;
                }
            } else {
                Uri uri = name.get(file.getName());
                DocumentFile doc = DocumentFile.fromSingleUri(context, uri);
                boolean isDel = doc.delete();
                if (isDel) {
                    return FileItem.DELETE.SUCCESS;
                }
            }
        } catch (Exception e) {
            Log.e("Document", e.toString());
        }

        return null;
    }

    public static long getDeleteFolderSize(File dir) {
        long size = 0;
        if (dir == null) return 0;
        if (dir.listFiles().length == 0) {
            dir.delete();
            return 0;
        }
        for (File file : dir.listFiles()) {
            if (file.isFile()) {
                size += file.length();
            } else {
                size += getDeleteFolderSize(file);
            }
            if (file.length() == 0) {
                file.delete();
            }
            if (dir.listFiles().length == 0) {
                dir.delete();
                return 0;
            }
            if (size > 0) { // 삭제안됨
                return 1;
            }
        }
        return size;
    }

    /**
     * URI 방식일 경우 Document 방식으로 outputstream 생성 하고 (아래메서드도 같이봐야됨.)
     *
     * @param fileExistFile 로컬에 저장될 파일
     * @param isForce       이어받기 여부 true :새로받기, false : 이어받기
     * @return
     * @throws Exception
     */
    public static OutputStream getOutputStream(Context service, File fileExistFile, boolean isForce) throws Exception {
        String str_uri = SharedPreferenceUtil.getInstance().getMobileDefaultUri(service);
        DocumentFile newFile_uri;
        DocumentFile pickedDir = DocumentFile.fromTreeUri(service, Uri.parse(str_uri));
        String path = fileExistFile.getCanonicalPath();
        File file_uri = new File(path);
        if (file_uri.exists() && !isForce) {
            newFile_uri = DocumentUtil.getDocumentFile(service, path);
            return service.getContentResolver().openOutputStream(newFile_uri.getUri(), "wa");
        } else {
            int indexOfuri = fileExistFile.getAbsolutePath().indexOf(FileManager.getInstance().getSecondaryStorage(service, "").getName());
            String mobileUri = fileExistFile.getAbsolutePath().substring((indexOfuri + FileManager.getInstance().getSecondaryStorage(service, "").getName().length() + 1), fileExistFile.getAbsolutePath().length());
            String[] splite = mobileUri.split("/");
            for (int i = 0; i < splite.length - 1; i++) {
                pickedDir = pickedDir.findFile(splite[i]);
            }
            newFile_uri = pickedDir.createFile("*/*", fileExistFile.getName());
            return service.getContentResolver().openOutputStream(newFile_uri.getUri());
        }
    }

    /**
     * 일반 파일 스트림 방식 Fileoutputstream 방식으로 생성한다.
     *
     * @param fileExistFile
     * @param isForce
     * @throws Exception
     */
    public static FileOutputStream getOutputStream(File fileExistFile, boolean isForce) throws Exception {

        if (isForce)
            return new FileOutputStream(fileExistFile);
        else
            return new FileOutputStream(fileExistFile, true);
    }

}