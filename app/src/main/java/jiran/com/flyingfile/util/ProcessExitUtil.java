package jiran.com.flyingfile.util;


import android.content.Context;

import jiran.com.tmfilemanager.MyApplication;


public class ProcessExitUtil {
    //	private MultidownloadTask tempDownloadTask = null;
//	private SendTask tempSendTask = null;
//	public MultidownloadTask getTempDownloadTask() {
//		return tempDownloadTask;
//	}
//	public void setTempDownloadTask(MultidownloadTask tempDownloadTask) {
//		this.tempDownloadTask = tempDownloadTask;
//	}
//	public SendTask getTempSendTask() {
//		return tempSendTask;
//	}
//	public void setTempSendTask(SendTask tempSendTask) {
//		this.tempSendTask = tempSendTask;
//	}
    private static ProcessExitUtil instance = null;

    public static ProcessExitUtil getInstance() {
        if (instance == null)
            instance = new ProcessExitUtil();
        return instance;
    }


    public void processExit(Context context, String string) {
        processExit(context);
    }

    public void processExit(
            final Context context
    ) {


        MyApplication app = (MyApplication) context.getApplicationContext();
        app.setStrCurrentPCPathForFileTransfer(null);
        //2014_07_07 모바일앱 기본 폴더 설정 기능 추가
        app.setStrCurrentMobilePath(app.getReceivedStorage(context));


        MyApplication.MAIN_TAB_CURRENT_MODE = -1;


        //메인화면 종료


        SharedPreferenceUtil.getInstance().setMaxStorage(context, 0);
    }
}
