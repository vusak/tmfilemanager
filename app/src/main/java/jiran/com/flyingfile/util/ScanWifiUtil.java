package jiran.com.flyingfile.util;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo.State;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.util.List;

@SuppressLint("NewApi")
public class ScanWifiUtil {

    private static String strBeforeSSID = null;
    private static int nBeforeNetworkID = 0;
    private static ScanWifiUtil instance = null;
    private static boolean isRunning = false;
    private static Context context;
    private static WifiManager wifiMan;
    private static BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

			/*
			if(android.os.Build.VERSION.SDK_INT == 16)
			{
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			*/

            ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            State wifi = conMan.getNetworkInfo(1).getState(); // wifi
            if (wifi == State.CONNECTED || wifi == State.CONNECTING) {
                context.unregisterReceiver(this);
                isRunning = false;
                return;
            }

            List<ScanResult> wifiList = wm.getScanResults();

            ScanResult result = null;

            //스캔된 결과에서 기존 연결했던것과 일치하는것을 찾자
            for (int i = 0; i < wifiList.size(); i++) {
                String ssid = wifiList.get(i).SSID;
                if (ssid == null)
                    continue;

                String strSSID = '\"' + ssid + '\"';
                strSSID = strSSID.replace("\"", "");
                String strBeforeSSID = null;
                if (getStrBeforeSSID() != null)
                    strBeforeSSID = getStrBeforeSSID().replace("\"", "");


                if (getStrBeforeSSID() != null && strBeforeSSID.equals(strSSID)) {
                    //찾았다!
                    result = wifiList.get(i);


                    break;
                }
            }

            if (wifiMan != null) {
                if (result != null) {
                    //기존 정보로 붙어보자


                    if (!addNewAccessPoint(result)) {
                        //연결에 실패했다 그냥 신호 쎈걸로 붙어보자


                        result = calculateBestAP(wifiList);

                        addNewAccessPoint(result);
                    }
                } else {
                    //기존정보가 없다


                    //그냥 가장 쎈걸로 붙어보자
                    result = calculateBestAP(wifiList);

                    addNewAccessPoint(result);
                }
            }

            context.unregisterReceiver(this);
            isRunning = false;

        }

        //가장 신호 센 ap 리턴
        public ScanResult calculateBestAP(List<ScanResult> sResults) {

            ScanResult bestSignal = null;
            for (ScanResult result : sResults) {
                if (bestSignal == null
                        || WifiManager.compareSignalLevel(bestSignal.level, result.level) < 0)
                    bestSignal = result;
            }

            try {
                String message = String.format("%s networks found. %s is the strongest. %s is the bsid",
                        sResults.size(), bestSignal.SSID, bestSignal.BSSID);
                Log.d("sResult", message);
            } catch (Exception e) {

            }

            return bestSignal;
        }

        //특정 AP 접속
        @SuppressLint("DefaultLocale")
        public boolean addNewAccessPoint(ScanResult scanResult) {

            try {
                WifiConfiguration wfc = new WifiConfiguration();
                wfc.SSID = '\"' + scanResult.SSID + '\"';


                String strCap = scanResult.capabilities;


                if (strCap.toLowerCase().contains("open")) {
                    wfc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                    wfc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                    wfc.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                    wfc.allowedAuthAlgorithms.clear();
                    wfc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                    wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                    wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
                    wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                    wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                } else if (strCap.toLowerCase().contains("wep")) {
                    wfc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                    wfc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                    wfc.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                    wfc.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                    wfc.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
                    wfc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                    wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                    wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
                    //				    	wfc.wepKeys[0] = "\"".concat(password).concat("\"");
                    wfc.wepTxKeyIndex = 0;

                } else if (strCap.toLowerCase().contains("wpa")) {
                    wfc.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                    wfc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                    wfc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                    wfc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                    wfc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                    wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                    wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                    //				    	wfc.preSharedKey = "\"".concat(password).concat("\"");
                } else {
                    wfc.hiddenSSID = true;
                    wfc.status = WifiConfiguration.Status.ENABLED;
                    wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                    wfc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                    wfc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                    wfc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                    wfc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                    wfc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                }

                List<WifiConfiguration> wcList = wifiMan.getConfiguredNetworks();
                boolean isAlreadyStoredInfo = false;
                String strSSID = '\"' + scanResult.SSID + '\"';
                int nNetworkID = 0;
                for (int i = 0; i < wcList.size(); i++) {
                    String strTempSSID = wcList.get(i).SSID;
                    if (strTempSSID.equals(strSSID)) {
                        nNetworkID = wcList.get(i).networkId;
                        isAlreadyStoredInfo = true;
                        break;
                    }
                }

                Log.d("peerlistactivity", "" + strSSID);

                if (isAlreadyStoredInfo) {
                    //이미 설정에 저장되어 있는 프로파일
                    boolean b = wifiMan.enableNetwork(nNetworkID, true);
                    Log.d("peerlistactivity", "이미 설정에 저장되어 있는 프로파일" + b);

                    return b;
                } else {
                    //설정에 저장되어있는 프로파일이 아니다.
                    int res = wifiMan.addNetwork(wfc);
                    boolean b = wifiMan.enableNetwork(res, true);

                    Log.d("peerlistactivity", "설정에 저장되어있는 프로파일이 아니다." + b);

                    return b;
                }
            } catch (Exception e) {
                return false;
            }
        }
    };

    public static String getStrBeforeSSID() {
        return strBeforeSSID;
    }

    public static void setStrBeforeSSID(String strBeforeSSID) {
        ScanWifiUtil.strBeforeSSID = strBeforeSSID;
    }

    public static int getnBeforeNetworkID() {
        return nBeforeNetworkID;
    }

    public static void setnBeforeNetworkID(int nBeforeNetworkID) {
        ScanWifiUtil.nBeforeNetworkID = nBeforeNetworkID;
    }

    public static ScanWifiUtil getInstance() {
        if (instance == null)
            instance = new ScanWifiUtil();
        return instance;
    }
}
