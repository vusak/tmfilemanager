package jiran.com.flyingfile.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.text.format.Formatter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

@SuppressLint({"NewApi", "DefaultLocale"})
public class SDMemoryChecker {
    private static SDMemoryChecker instance = null;

    public static SDMemoryChecker getInstance() {
        if (instance == null)
            instance = new SDMemoryChecker();
        return instance;
    }

    @SuppressWarnings("unused")
    public static String getExternalStorages(Context context) {
        String[] dirs = null;
        String res = null;


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            BufferedReader bufReader = null;
            try {
                bufReader = new BufferedReader(new FileReader("/proc/mounts"));
                ArrayList<String> list = new ArrayList<String>();
                String line;
                while ((line = bufReader.readLine()) != null) {
                    if (line.contains("vfat") || line.contains("/mnt")) {
                        StringTokenizer tokens = new StringTokenizer(line, " ");
                        String s = tokens.nextToken();
                        s = tokens.nextToken(); // Take the second token, i.e. mount point

                        if (s.equals(Environment.getExternalStorageDirectory().getPath())) {
                            list.add(s);
                        } else if (line.contains("/dev/block/vold")) {
                            if (!line.contains("/mnt/secure") && !line.contains("/mnt/asec") && !line.contains("/mnt/obb") && !line.contains("/dev/mapper") && !line.contains("tmpfs")) {
                                list.add(s);
                            }
                        }
                    }
                }

                dirs = new String[list.size()];
                for (int i = 0; i < list.size(); i++) {
//					dirs[i] = list.get(i);
                    String path = list.get(i);
                    path = path.toLowerCase();
                    if (path.contains("ext") && path.contains("sd")) {
                        res = list.get(i);
                    }
                }
            } catch (FileNotFoundException e) {
            } catch (IOException e) {
            } finally {
                if (bufReader != null) {
                    try {
                        bufReader.close();
                    } catch (IOException e) {
                    }
                }
            }
        } else {

            File file = FileManager.getInstance().getSecondaryStorage(context, "");
            if (file != null) {
                res = file.getAbsolutePath();
            }
        }

        return res;
    }

    public boolean isExternalStorage(Context context, String strPath) {

        if (strPath == null)
            return false;

        String strExternalPath = SDMemoryChecker.getExternalStorages(context);
        if (strExternalPath == null)
            return false;

        if (strPath.startsWith(strExternalPath)) {
            return true;
        } else {
            return false;
        }
    }

    public String getFormattedInternalTotalMemory(Context context) {

        long internalTotalMemory = getInternalTotalMemory(context);
        if (internalTotalMemory < 0)
            return null;

        try {
            return Formatter.formatFileSize(context, internalTotalMemory);
        } catch (Exception e) {
            return null;
        }
    }

    public String getFormattedInternalFreeMemory(Context context) {
        long internalFreeMemory = getInternalFreeMemory(context);
        if (internalFreeMemory < 0)
            return null;

        try {
            return Formatter.formatFileSize(context, internalFreeMemory);
        } catch (Exception e) {
            return null;
        }
    }

    public String getFormattedExternalTotalMemory(Context context) {

        long externalTotalMemory = getExternalTotalMemory(context);

        if (externalTotalMemory < 0)
            return null;

        try {
            return Formatter.formatFileSize(context, externalTotalMemory);
        } catch (Exception e) {
            return null;
        }
    }

    public String getFormattedExternalFreeMemory(Context context) {

        long externalFreeMemory = getExternalFreeMemory(context);

        if (externalFreeMemory < 0)
            return null;

        try {
            return Formatter.formatFileSize(context, externalFreeMemory);
        } catch (Exception e) {
            return null;
        }
    }

    @SuppressWarnings("deprecation")
    public long getInternalTotalMemory(Context context) {

        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());


        long blockSize = 0;
        long totalBlocks = 0;


        if (Build.VERSION.SDK_INT >= 18) {
            blockSize = stat.getBlockSizeLong();
            totalBlocks = stat.getBlockCountLong();
        } else {
            blockSize = stat.getBlockSize();
            totalBlocks = stat.getBlockCount();
        }

        return totalBlocks * blockSize;
    }

    @SuppressWarnings("deprecation")
    public long getInternalFreeMemory(Context context) {


        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());


        long blockSize = 0;
        long availableBlocks = 0;


        if (Build.VERSION.SDK_INT >= 18) {
            blockSize = stat.getBlockSizeLong();
            availableBlocks = stat.getAvailableBlocksLong();
        } else {
            blockSize = stat.getBlockSize();
            availableBlocks = stat.getAvailableBlocks();
        }

        return availableBlocks * blockSize;

    }

    @SuppressWarnings("deprecation")
    public long getExternalTotalMemory(Context context) {

        String file = SDMemoryChecker.getExternalStorages(context);

        if (file == null)
            return -1;


        StatFs stat = new StatFs(file);


        long blockSize = 0;
        long totalBlocks = 0;

        if (Build.VERSION.SDK_INT >= 18) {
            blockSize = stat.getBlockSizeLong();
            totalBlocks = stat.getBlockCountLong();
        } else {
            blockSize = stat.getBlockSize();
            totalBlocks = stat.getBlockCount();
        }

        return totalBlocks * blockSize;

    }

    @SuppressWarnings("deprecation")
    public long getExternalFreeMemory(Context context) {
        String file = SDMemoryChecker.getExternalStorages(context);

        if (file == null)
            return -1;


        StatFs stat = new StatFs(file);


        long blockSize = 0;
        long availableBlocks = 0;

        if (Build.VERSION.SDK_INT >= 18) {
            blockSize = stat.getBlockSizeLong();
            availableBlocks = stat.getAvailableBlocksLong();
        } else {
            blockSize = stat.getBlockSize();
            availableBlocks = stat.getAvailableBlocks();
        }

        return availableBlocks * blockSize;
    }

    private boolean externalMemoryAvailable() {
        return Environment.getExternalStorageState().
                equals(Environment.MEDIA_MOUNTED);
    }

    /**
     * sdcard 가용량을 리턴하는 함수
     *
     * @return
     */
    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public long getAvailableExternalMemorySize() {
        if (externalMemoryAvailable()) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());

            long blockSize = 0;
            long availableBlocks = 0;

            if (Build.VERSION.SDK_INT >= 18) {
                //sdk 18 버젼에서는 메모리 확인이 int에서 long 타입으로 바뀜
                blockSize = stat.getBlockSizeLong();
                availableBlocks = stat.getAvailableBlocksLong();
            } else {
                blockSize = stat.getBlockSize();
                availableBlocks = stat.getAvailableBlocks();
            }

            return availableBlocks * blockSize;
        } else {
            return -1;
        }
    }

    public long getFreeMemorySize() {
        return getFreeMemorySize(null);
    }

    @SuppressWarnings("deprecation")
    public long getFreeMemorySize(String strPath) {
        if (externalMemoryAvailable()) {

            StatFs stat = null;
            if (strPath == null) {
                File path = Environment.getExternalStorageDirectory();
                stat = new StatFs(path.getPath());
            } else {
                stat = new StatFs(strPath);
            }

            long blockSize = 0;
            long freeBlocks = 0;

            if (Build.VERSION.SDK_INT >= 18) {
                blockSize = stat.getBlockSizeLong();
                freeBlocks = stat.getFreeBlocksLong();
            } else {
                blockSize = stat.getBlockSize();
                freeBlocks = stat.getFreeBlocks();
            }

            return freeBlocks * blockSize;
        } else {
            return -1;
        }
    }


    public long getTotalMemorySize() {
        return getTotalMemorySize(null);
    }

    @SuppressWarnings("deprecation")
    public long getTotalMemorySize(String strPath) {
        if (externalMemoryAvailable()) {

            StatFs stat = null;
            if (strPath == null) {
                File path = Environment.getExternalStorageDirectory();
                stat = new StatFs(path.getPath());
            } else {
                stat = new StatFs(strPath);
            }

            long blockSize = 0;
            long totalBlocks = 0;

            if (Build.VERSION.SDK_INT >= 18) {
                blockSize = stat.getBlockSizeLong();
                totalBlocks = stat.getBlockCountLong();
            } else {
                blockSize = stat.getBlockSize();
                totalBlocks = stat.getBlockCount();
            }

            return totalBlocks * blockSize;
        } else {
            return -1;
        }
    }


    /**
     * sdcard 특정 경로에 메모리 가용량 리턴하는 함수
     *
     * @return
     */
    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public long getAvailableExternalMemorySize(String path) {
        if (externalMemoryAvailable()) {
            StatFs stat = new StatFs(path);
            long blockSize = 0;
            long availableBlocks = 0;

            if (Build.VERSION.SDK_INT >= 18) {
                //sdk 18 버젼에서는 메모리 확인이 int에서 long 타입으로 바뀜
                blockSize = stat.getBlockSizeLong();
                availableBlocks = stat.getAvailableBlocksLong();
            } else {
                blockSize = stat.getBlockSize();
                availableBlocks = stat.getAvailableBlocks();
            }
            return availableBlocks * blockSize;
        } else {
            return -1;
        }
    }

}
