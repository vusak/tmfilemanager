package jiran.com.flyingfile.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;

public class PermissionUtil {


    private static PermissionUtil instance = null;

    public static PermissionUtil getInstance() {
        if (instance == null)
            instance = new PermissionUtil();
        return instance;
    }


    public boolean isAbleFileWritePath(String strPath) {
        try {
            File folder = new File(strPath);
            if (folder.isDirectory()) {
                File file = new File(strPath + "/a.txt");

                boolean alreadyExist = file.exists();

                try {
                    FileOutputStream fos = new FileOutputStream(file, true);
                    fos.close();

                    if (!alreadyExist)
                        file.delete();
                    return true;
                } catch (Exception e) {
                    return false;
                }
            } else {
                try {
                    FileOutputStream fos = new FileOutputStream(folder, true);
                    fos.close();
                    return true;
                } catch (Exception e) {
                    return false;
                }
            }
        } catch (Exception e) {
            return false;
        }
    }


    public boolean isSecondaryStorage(Context context, String strPath) {

        File secondaryStorage = FileManager.getInstance().getSecondaryStorage(context, strPath);

        if (secondaryStorage == null)
            return false;

        String strSecondaryPath = secondaryStorage.getAbsolutePath();
        strSecondaryPath = strSecondaryPath.toLowerCase(Locale.ENGLISH);
        strPath = strPath.toLowerCase(Locale.ENGLISH);

        if (strPath.startsWith(strSecondaryPath)) {
            return true;
        } else {
            return false;
        }
    }


    @SuppressLint("NewApi")
    public boolean isExistPath(String strPath) {
        File file = new File(strPath + "/a.txt");

        boolean alreadyExist = file.exists();
        try {
            FileOutputStream fos = new FileOutputStream(file, true);
            fos.close();

            if (!alreadyExist)
                file.delete();
            return true;
        } catch (Exception e) {
            File folder = new File(strPath);
            if (folder.isDirectory()) {
                File[] subFiles = new File(strPath).listFiles();
                if (subFiles == null || subFiles.length == 0)
                    return false;
                else
                    return true;
            } else {
                return false;
            }
        }
    }


    @SuppressLint({"NewApi", "DefaultLocale"})
    public boolean isFolderWritePermission(String strPath) {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            File file = new File(strPath + "/a.txt");
            boolean alreadyExist = file.exists();
            try {
                FileOutputStream fos = new FileOutputStream(file, true);
                fos.close();
                if (!alreadyExist)
                    file.delete();

                return true;
            } catch (FileNotFoundException e) {
                return false;
            } catch (IOException e) {
                return false;
            }
        } else {
            return true;
        }
    }

    @SuppressLint({"NewApi", "DefaultLocale"})
    public boolean isFileWritePermission(String strPath) {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            File file = new File(strPath);
            try {
                file.createNewFile();
                return true;
            } catch (FileNotFoundException e) {
                return false;
            } catch (IOException e) {
                return false;
            }
        } else {
            return true;
        }
    }

}
