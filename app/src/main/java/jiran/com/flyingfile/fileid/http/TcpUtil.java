package jiran.com.flyingfile.fileid.http;

import java.io.IOException;
import java.io.InputStream;

public class TcpUtil {

    public boolean recv(int nDes, InputStream is, byte[] res) throws IOException {

        int nTempReadCnt = 0;

        while (nDes > 0) {
            int nTemp = is.read(res, nTempReadCnt, nDes);
            if (nTemp <= 0)
                return false;

            nTempReadCnt = nTemp + nTempReadCnt;
            nDes -= nTemp;
        }

        return true;
    }

}