package jiran.com.flyingfile.fileid.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.MemoryCategory;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.StringSignature;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jiran.com.flyingfile.model.FileItem;
import jiran.com.flyingfile.util.FileTypeUtil;
import jiran.com.flyingfile.util.UnitTransfer;
import jiran.com.tmfilemanager.R;


/**
 * Created by jeon on 2016-10-13.
 */

public class DetailGalleryActivity extends FragmentActivity {
    public static List<FileItem> data;
    private MyViewPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private TextView tv_detail_gallery_title, tv_detail_gallery_size;
    private ImageView iv_detail_gallery_check;
    private int currentPosition = 0;
    private ArrayList<FileItem> realPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(0, 0);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flyingfile_activity_detail_gallery);

        currentPosition = getIntent().getIntExtra("position", 0);
        mSectionsPagerAdapter = new MyViewPagerAdapter();
        tv_detail_gallery_title = (TextView) findViewById(R.id.tv_detail_gallery_title);
        tv_detail_gallery_size = (TextView) findViewById(R.id.tv_detail_gallery_size);
        iv_detail_gallery_check = (ImageView) findViewById(R.id.iv_detail_gallery_check);
        Glide.get(this).setMemoryCategory(MemoryCategory.HIGH);

        FileTypeUtil fileTypeUtil = new FileTypeUtil();
        realPosition = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            final int flag_FileType = fileTypeUtil.getFilyType(data.get(i));
            if (flag_FileType == FileTypeUtil.FLAG_PICTURE) {
                if (currentPosition == i) {
                    currentPosition = realPosition.size();
                }
                realPosition.add(data.get(i));
            }
        }
        mViewPager = (ViewPager) findViewById(R.id.pager_detail_gallery);
        mViewPager.setPageTransformer(true, new DepthPageTransformer());

        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(currentPosition);


        tv_detail_gallery_title.setText(realPosition.get(currentPosition).getName());
        tv_detail_gallery_size.setText(UnitTransfer.getInstance().memoryConverse2(realPosition.get(currentPosition).length()));
        iv_detail_gallery_check.setImageResource(realPosition.get(currentPosition).isCheck() ? R.drawable.fileview_check : R.drawable.fileview_uncheck);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPosition = position;
                tv_detail_gallery_title.setText(realPosition.get(currentPosition).getName());
                tv_detail_gallery_size.setText(UnitTransfer.getInstance().memoryConverse2(realPosition.get(currentPosition).length()));
                iv_detail_gallery_check.setImageResource(realPosition.get(currentPosition).isCheck() ? R.drawable.fileview_check : R.drawable.fileview_uncheck);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        findViewById(R.id.ibtn_detail_gallery_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        iv_detail_gallery_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FileItem item = realPosition.get(currentPosition);
                if (item.isCheck()) {
                    iv_detail_gallery_check.setImageResource(R.drawable.fileview_uncheck);
                    item.setCheck(false);
                } else {
                    iv_detail_gallery_check.setImageResource(R.drawable.fileview_check);
                    item.setCheck(true);
                }
            }
        });


    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("position", currentPosition);
    }

    @Override
    protected void onDestroy() {
        data = null;
        overridePendingTransition(0, 0);
        try {
            Glide.get(getBaseContext()).clearDiskCache();
            Glide.get(getBaseContext()).clearMemory();
        } catch (Exception e) {
        }
        super.onDestroy();

    }

    //	adapter
    public class MyViewPagerAdapter extends PagerAdapter {
        RequestManager a;
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
            a = Glide.with(getBaseContext());
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {

            layoutInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.flyingfile_item_activity_detail_gallery, container, false);

            final ImageView imageViewPreview = (ImageView) view.findViewById(R.id.iv_detail_gallery_content);
            final ProgressBar progress_wheel = (ProgressBar) view.findViewById(R.id.progress_wheel);

            a.load(realPosition.get(position).getAbsolutePath())
                    .error(R.drawable.icon_non_image_error)
                    .thumbnail(0.5f)
                    .override(400, 400)
                    .signature(new StringSignature(String.valueOf(new File(realPosition.get(position).getAbsolutePath()).lastModified())))
                    .dontTransform()
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            progress_wheel.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(imageViewPreview);

            container.addView(view);
            return view;
        }

        public int getCount() {
            return realPosition.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == ((View) obj);
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    class DepthPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.75f;

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.setAlpha(1);
                view.setTranslationX(0);
                view.setScaleX(1);
                view.setScaleY(1);

            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                view.setAlpha(1 - position);

                // Counteract the default slide transition
                view.setTranslationX(pageWidth * -position);

                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE
                        + (1 - MIN_SCALE) * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }

}
