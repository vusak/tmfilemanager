package jiran.com.flyingfile.fileid.adapter;

import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;

import java.io.File;
import java.io.IOException;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import jiran.com.flyingfile.callback.DeleteCallback;
import jiran.com.flyingfile.callback.FileListCheckedChangeCallback;
import jiran.com.flyingfile.callback.FilelistSetCallback;
import jiran.com.flyingfile.custom.RotateTransformation;
import jiran.com.flyingfile.custom.SectionedRecyclerViewAdapter;
import jiran.com.flyingfile.model.FileItem;
import jiran.com.flyingfile.model.HeaderDomain;
import jiran.com.flyingfile.util.DocumentUtil;
import jiran.com.flyingfile.util.FileManager;
import jiran.com.flyingfile.util.FileTypeUtil;
import jiran.com.flyingfile.util.LanguageUtil;
import jiran.com.flyingfile.util.PermissionUtil;
import jiran.com.flyingfile.util.SharedPreferenceUtil;
import jiran.com.flyingfile.util.UnitTransfer;
import jiran.com.tmfilemanager.MyApplication;
import jiran.com.tmfilemanager.R;


/**
 * @author Aidan Follestad (afollestad)
 */
public class BaseFileAdapter extends SectionedRecyclerViewAdapter<BaseFileAdapter.MainVH> implements Filterable {
    private Context context;
    private List<FileItem> contentData;
    private List<FileItem> dummyData;
    private List<HeaderDomain> sectionData;
    private int mode = 0;
    private int theme = THEME.BLUE.ordinal();
    private ArrayList<String> list_Checked = null;
    private long nCheckedTotalSize = 0;
    private ImageButton ib_CheckAll;
    private RequestManager mGlideRequestManager;
    private SimpleDateFormat format = new SimpleDateFormat("yyyy-M-d");
    private File parentFolder = null;
    private FileListCheckedChangeCallback checkChangeCallback = null;
    private OnItemClickListener itemListener = null;
    private FilelistSetCallback filelistSetCallback = null;

    public BaseFileAdapter(Context context, FilelistSetCallback filelistSetCallback, List<FileItem> data, ImageButton ib_CheckAll, int theme) {
        this.filelistSetCallback = filelistSetCallback;
        this.contentData = data;
        this.dummyData = data;
        this.context = context;
        this.ib_CheckAll = ib_CheckAll;
        this.list_Checked = new ArrayList<>();
        this.sectionData = new ArrayList<>();
        this.mGlideRequestManager = Glide.with(context);
        this.theme = theme;
    }

    public int getCount() {
        return contentData.size();
    }

    public FileItem getItem(int position) {
        if (contentData.size() == 0) return null;
        try {
            return contentData.get(position);
        } catch (Exception e) {
            return null;
        }
    }

    public List<FileItem> getAll() {
        return contentData;
    }

    public void addAll(List<FileItem> list) {
        contentData.addAll(list);
    }

    public void clear() {
        contentData.clear();
        dummyData.clear();
    }

    public void remove(int position) {
        contentData.remove(position);
    }

    public File getParentFile() {
        if (parentFolder == null) {
            MyApplication app = (MyApplication) context.getApplicationContext();
            String strPath = app.getStrCurrentMobilePath();
            parentFolder = new File(strPath);
        }

        return parentFolder;
    }

    public void setParentFile(File item) {
        this.parentFolder = item;
    }

    public FileListCheckedChangeCallback getCheckChangeCallback() {
        return checkChangeCallback;
    }

    public void setCheckChangeCallback(FileListCheckedChangeCallback checkChangeCallback) {
        this.checkChangeCallback = checkChangeCallback;
    }

    public void setSectionData(List<HeaderDomain> selctionData) {
        this.sectionData = selctionData;
    }

    public void setMode(int mode) {
        this.mode = mode;

    }

    public void setOnItemClickListener(OnItemClickListener itemListener) {
        this.itemListener = itemListener;
    }

    public FilelistSetCallback getFilelistSetCallback() {
        return filelistSetCallback;
    }

    @Override
    public int getSectionCount() {

        if (sectionData != null && (mode == MODE.SECTION.ordinal() || mode == MODE.APK.ordinal())) {
            return sectionData.size() + 1;
        } else {
            if (contentData.size() > 0) {
                return 1 + 1;
            } else {
                return 0;
            }
        }
    }

    @Override
    public int getItemCount(int section) {

        if (sectionData == null) {
            return 0;
        } else if (mode == MODE.SECTION.ordinal() || mode == MODE.APK.ordinal()) {
            if (section == sectionData.size()) return 1;
            return sectionData.get(section).getContentCnt();
        } else {
            if (contentData == null)
                return 0;
            else {
                if (section == 0) {
                    return contentData.size();
                } else {
                    return 1;
                }
            }

        }
    }

    @Override
    public void onBindHeaderViewHolder(final MainVH holder, final int section) {
        holder.v_header_empty.setBackgroundColor(Color.parseColor("#ffffff"));
        if (getSectionCount() - 1 == section) {
            holder.ll_headerLayout.setVisibility(View.GONE);
        } else if (sectionData != null && sectionData.size() > 0 && holder.ll_headerLayout != null && mode == MODE.SECTION.ordinal()) {
            holder.ll_headerLayout.setVisibility(View.VISIBLE);
            final HeaderDomain header = sectionData.get(section);
            holder.tv_headerText.setText(changeDate(header.getDay()));
            holder.ibtn_header_imagemodel_view.setImageResource(header.isCheck() ?
                    (theme == THEME.BLUE.ordinal() ? R.drawable.fileview_check : R.drawable.fileview_check_lime) :
                    (theme == THEME.BLUE.ordinal() ? R.drawable.fileview_uncheck : R.drawable.fileview_uncheck_lime));

            holder.ibtn_header_imagemodel_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!header.isCheck()) {
                        dayCheck(true, header.getDay());
                        holder.ibtn_header_imagemodel_view.setImageResource(theme == THEME.BLUE.ordinal() ? R.drawable.fileview_check : R.drawable.fileview_check_lime);
                        header.setCheck(true);
                    } else {
                        dayCheck(false, header.getDay());
                        holder.ibtn_header_imagemodel_view.setImageResource(theme == THEME.BLUE.ordinal() ? R.drawable.fileview_uncheck : R.drawable.fileview_uncheck_lime);
                        header.setCheck(false);
                    }
                    isAllCheck();
                }
            });
        } else if (sectionData != null && sectionData.size() > 0 && holder.ll_headerLayout != null && mode == MODE.APK.ordinal()) {
            holder.ll_headerLayout.setVisibility(View.VISIBLE);
            final HeaderDomain header = sectionData.get(section);
            holder.tv_headerText.setText(changeDate(header.getDay()));
            holder.ibtn_header_imagemodel_view.setVisibility(View.GONE);
            holder.v_header_line.setVisibility(View.GONE);
            holder.v_header_empty.setBackgroundColor(Color.parseColor("#E5E5E5"));
            holder.tv_headerText.setTextColor(context.getResources().getColor(R.color.notice_title_color));
        } else {
            holder.ll_headerLayout.setVisibility(View.GONE);
        }

    }

    @Override
    public void onBindViewHolder(MainVH holder, int section, int relativePosition, final int position) {

        if (section == getSectionCount() - 1) {
            holder.ll_file_content.setVisibility(View.GONE);
            return;
        }
        holder.ll_file_content.setVisibility(View.VISIBLE);
        final FileItem item = getItem(position);
        if (item == null) return;
        if (holder.TextView_FileName != null)
            holder.TextView_FileName.setText(item.getAbsolutePath());

        if (mode == MODE.GRIDVIEW.ordinal() || mode == MODE.LISTVIEW.ordinal() || mode == MODE.APK.ordinal()) {


            holder.nPos = position;
            if (mode == MODE.LISTVIEW.ordinal()) {
                if (holder.ll_Divider != null)
                    holder.ll_Divider.setVisibility(View.VISIBLE);
            }


            if (holder.ll_Background != null) {
                if (position % 2 == 0)
                    holder.ll_Background.setBackgroundResource(R.drawable.list_selector_color_blue);
                else
                    holder.ll_Background.setBackgroundResource(R.drawable.list_selector_color_white);
            }

            if (holder.iv_SubImage != null)
                holder.iv_SubImage.setVisibility(View.GONE);


            if (item.isDot()) {
                //상위 폴더 보기에는 체크박스 필요 음슴

                if (holder.ib_Checkbox != null) {
                    holder.ib_Checkbox.setVisibility(View.INVISIBLE);
                }

                if (holder.tv_FileName != null)
                    holder.tv_FileName.setText(R.string.FileView_FileList_Back);


                if (holder.tv_FileSize != null && mode == MODE.LISTVIEW.ordinal())
                    holder.tv_FileSize.setVisibility(View.INVISIBLE);

                if (holder.tv_FileDate != null)
                    holder.tv_FileDate.setVisibility(View.GONE);

                //도트일때는 파일 정보 숨기기
                if (holder.fl_Info != null) {
                    holder.fl_Info.setVisibility(View.GONE);
                } else {
                    if (holder.tv_FileDate != null)
                        holder.tv_FileDate.setVisibility(View.GONE);
                    if (holder.tv_FileSize != null && mode == MODE.LISTVIEW.ordinal())
                        holder.tv_FileSize.setVisibility(View.INVISIBLE);
                }


            } else {
                //도트가 아닐때는 파일 정보 보이기
                if (holder.fl_Info != null) {
                    holder.fl_Info.setVisibility(View.VISIBLE);
                } else {
                    if (holder.tv_FileDate != null)
                        holder.tv_FileDate.setVisibility(View.VISIBLE);
                    if (holder.tv_FileSize != null && mode == MODE.LISTVIEW.ordinal())
                        holder.tv_FileSize.setVisibility(View.VISIBLE);
                }

                if (holder.ib_Checkbox != null) {
                    holder.ib_Checkbox.setVisibility(View.VISIBLE);
                }

                if (holder.tv_FileDate != null)
                    holder.tv_FileDate.setVisibility(View.VISIBLE);

                if (holder.tv_FileSize != null && mode == MODE.LISTVIEW.ordinal())
                    holder.tv_FileSize.setVisibility(View.VISIBLE);

                if (item.isDirectory()) {
                    //폴더일경우

                    if (holder.tv_FileName != null) {

                        String strFilename;
                        if (item.isPackageFolderShortCut())
                            strFilename = context.getString(R.string.ExternalStorage);
                        else
                            strFilename = item.getName();

                        boolean isNeedNormalize = Normalizer.isNormalized(strFilename, Normalizer.Form.NFD);
                        if (isNeedNormalize) {
                            strFilename = Normalizer.normalize(strFilename, Normalizer.Form.NFC);
                            holder.tv_FileName.setText(strFilename);
                        } else {
                            holder.tv_FileName.setText(strFilename);
                        }
                    }


                    long innerFileLen = item.getnFileCntInFolder();
                    if (holder.tv_FileSize != null && mode == MODE.LISTVIEW.ordinal()) {
                        //root경로이고 버전이 6.0이상이면 보여주지 않는다.
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && FileManager.getInstance().isRootDirectory(getParentFile().getAbsolutePath())) {
                            holder.tv_FileSize.setVisibility(View.GONE);
                        } else {
                            holder.tv_FileSize.setVisibility(View.VISIBLE);
                            StringBuffer sbTemp = new StringBuffer();
                            sbTemp.append(innerFileLen);
                            sbTemp.append(context.getString(R.string.FileView_FileNumber));
                            holder.tv_FileSize.setText(sbTemp.toString());
                        }

                    }


                    /**
                     * 정렬기능 추가(15.11.04)
                     */
                    if (holder.tv_FileDate != null) {
                        holder.tv_FileDate.setVisibility(View.VISIBLE);
                        //파일 날짜 표시
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        String systemDate = format.format(System.currentTimeMillis());
                        String fileDate = format.format(item.lastModified());
                        if (systemDate.equals(fileDate)) {
                            format = new SimpleDateFormat("HH:mm");
                        }
                        holder.tv_FileDate.setText(format.format(item.lastModified()));
                    }
                } else {


                    if (holder.tv_FileName != null) {

                        String strPath = item.getAbsolutePath();
                        File file = new File(strPath);

                        String temp;
                        if (item.isPackageFolderShortCut())
                            temp = context.getString(R.string.ExternalStorage);
                        else
                            temp = file.getName();


                        boolean isNeedNormalize = Normalizer.isNormalized(temp, Normalizer.Form.NFD);
                        if (isNeedNormalize) {
                            temp = Normalizer.normalize(temp, Normalizer.Form.NFC);
                            holder.tv_FileName.setText(temp);
                        } else {
                            holder.tv_FileName.setText(temp);
                        }
                    }

                    if (holder.tv_FileSize != null) {

                        String strPath = item.getAbsolutePath();
                        File file = new File(strPath);
                        holder.tv_FileSize.setText(UnitTransfer.getInstance().memoryConverse2(file.length()));

                    }


                    if (holder.tv_FileDate != null) {

                        //파일 날짜 표시
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        String systemDate = format.format(System.currentTimeMillis());
                        String fileDate = format.format(item.lastModified());
                        if (systemDate.equals(fileDate)) {
                            format = new SimpleDateFormat("HH:mm");
                        }
                        holder.tv_FileDate.setText(format.format(item.lastModified()));
                    }
                    //폴더가 아닌경우

                }
            }
            if (holder.iv_Thumb != null) {

                FileTypeUtil fileTypeUtil = new FileTypeUtil();
                final int flag_FileType = fileTypeUtil.getFilyType(item);
                int nRes = 0;
                if (mode == MODE.LISTVIEW.ordinal())
                    nRes = R.drawable.fileview_etc_ic;
                else if (mode == MODE.GRIDVIEW.ordinal())
                    nRes = R.drawable.fileview_etc_ic_small;

                switch (flag_FileType) {
                    case FileTypeUtil.FLAG_PICTURE:
                        //파일이 그림일 경우
                        if (mode == MODE.LISTVIEW.ordinal())
                            nRes = R.drawable.fileview_image_ic;
                        else if (mode == MODE.GRIDVIEW.ordinal())
                            nRes = R.drawable.fileview_image_ic_small;
                        break;
                    case FileTypeUtil.FLAG_MOVIE:
                        //파일이 동영상일 경우
                        if (mode == MODE.LISTVIEW.ordinal())
                            nRes = R.drawable.fileview_movie_ic;
                        else if (mode == MODE.GRIDVIEW.ordinal())
                            nRes = R.drawable.fileview_movie_ic_small;
                        if (holder.iv_SubImage != null) {
                            holder.iv_SubImage.setImageResource(R.drawable.fileview_movie_ic);
                            holder.iv_SubImage.setVisibility(View.VISIBLE);
                        }
                        break;
                    case FileTypeUtil.FLAG_MUSIC:
                        //파일이 음악일 경우
                        if (mode == MODE.LISTVIEW.ordinal())
                            nRes = R.drawable.fileview_music_ic;
                        else if (mode == MODE.GRIDVIEW.ordinal())
                            nRes = R.drawable.fileview_music_ic_small;
                        break;
                    case FileTypeUtil.FLAG_DOCUMENT:
                        //파일이 문서일 경우
                        if (mode == MODE.LISTVIEW.ordinal())
                            nRes = R.drawable.fileview_docment_ic;
                        else if (mode == MODE.GRIDVIEW.ordinal())
                            nRes = R.drawable.fileview_docment_ic_small;
                        break;
                    case FileTypeUtil.FLAG_APP: //앱인 경우.
                        if (mode == MODE.LISTVIEW.ordinal())
                            nRes = R.drawable.fileview_apk_ic;
                        else if (mode == MODE.GRIDVIEW.ordinal())
                            nRes = R.drawable.fileview_apk_ic_small;
                        break;
                    case FileTypeUtil.FLAG_OTHERS:
                    case FileTypeUtil.FLAG_UNKNOWN:
                    default:
                        //그외 기타 파일일 경우
                        if (mode == MODE.LISTVIEW.ordinal())
                            nRes = R.drawable.fileview_etc_ic;
                        else if (mode == MODE.GRIDVIEW.ordinal())
                            nRes = R.drawable.fileview_etc_ic_small;

                        break;
                }

                //이미지뷰를 구분하기 위해 태그를 집어넣자
                //     holder.iv_Thumb.setImageResource(nRes);
                if (item.isDot()) {
                    mGlideRequestManager
                            .load((mode == MODE.LISTVIEW.ordinal()) ? R.drawable.fileview_dotfolder_ic : R.drawable.fileview_dotfolder_ic_small)
                            .error((mode == MODE.LISTVIEW.ordinal()) ? R.drawable.fileview_dotfolder_ic : R.drawable.fileview_dotfolder_ic_small)
                            .into(holder.iv_Thumb);
                } else if (item.isDirectory()) {
                    mGlideRequestManager
                            .load((mode == MODE.LISTVIEW.ordinal()) ? R.drawable.fileview_folder_ic : R.drawable.fileview_folder_ic_small)
                            .error((mode == MODE.LISTVIEW.ordinal()) ? R.drawable.fileview_folder_ic : R.drawable.fileview_folder_ic_small)
                            .into(holder.iv_Thumb);
                } else if (flag_FileType == FileTypeUtil.FLAG_APP) {
                    holder.iv_Thumb.setImageResource(nRes);
                    try {
                        if (holder.tv_FileDate != null) holder.tv_FileDate.setVisibility(View.GONE);

                        ApplicationInfo appInfo = null;
                        if (item.getApplicationInfo() != null) {
                            appInfo = item.getApplicationInfo();
                        } else {
                            PackageInfo packageInfo = context.getPackageManager()
                                    .getPackageArchiveInfo(item.getAbsolutePath(), PackageManager.GET_ACTIVITIES);
                            if (packageInfo != null) {
                                appInfo = packageInfo.applicationInfo;
                            }
                        }

                        if (appInfo != null) {
                            if (Build.VERSION.SDK_INT >= 8) {
                                appInfo.sourceDir = item.getAbsolutePath();
                                appInfo.publicSourceDir = item.getAbsolutePath();
                            }
                            CharSequence appName = appInfo.loadLabel(context.getPackageManager());
                            Drawable icon = appInfo.loadIcon(context.getPackageManager());
                            float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60,
                                    context.getResources().getDisplayMetrics());
                            int pix = (int) px;
                            icon.setBounds(0, 0, pix, pix);
                            holder.tv_FileName.setText(appName);
                            mGlideRequestManager.load("").placeholder(icon).into(holder.iv_Thumb);
                        } else {
                            mGlideRequestManager
                                    .load((mode == MODE.LISTVIEW.ordinal()) ? R.drawable.fileview_apk_ic : R.drawable.fileview_apk_ic_small)
                                    .into(holder.iv_Thumb);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    String videothumbPath = null;
                    if (item.getStrThumbnailPath() == null && flag_FileType == FileTypeUtil.FLAG_MOVIE) {
                        videothumbPath = getThumnailPath(context, item.getAbsolutePath());
                    }
                    byte[] imageData = null;
                    if (item.getStrThumbnailPath() == null && flag_FileType == FileTypeUtil.FLAG_PICTURE) {
                        try {
                            ExifInterface exif = new ExifInterface(item.getAbsolutePath());
                            imageData = exif.getThumbnail();
                        } catch (Exception e) {
                        }
                        ;
                    }

                    if (imageData != null) {
                        int orientation = GetExifOrientation(item.getAbsolutePath());
                        mGlideRequestManager
                                .load(imageData)
                                .asBitmap()
                                .transform(new RotateTransformation(context, orientation))
                                .signature(new StringSignature(String.valueOf(item.getAbsolutePath() + new File(item.getAbsolutePath()).lastModified())))
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .error((mode == MODE.LISTVIEW.ordinal()) ? (flag_FileType == FileTypeUtil.FLAG_PICTURE ? R.drawable.fileview_image_ic : R.drawable.fileview_movie_ic) : (flag_FileType == FileTypeUtil.FLAG_PICTURE ? R.drawable.fileview_image_ic_small : R.drawable.fileview_movie_ic_small)) // Uri of the picture
                                .placeholder((mode == MODE.LISTVIEW.ordinal()) ? (flag_FileType == FileTypeUtil.FLAG_PICTURE ? R.drawable.fileview_image_ic : R.drawable.fileview_movie_ic) : (flag_FileType == FileTypeUtil.FLAG_PICTURE ? R.drawable.fileview_image_ic_small : R.drawable.fileview_movie_ic_small))
                                .into(holder.iv_Thumb);
                    } else if (item.getStrThumbnailPath() != null || videothumbPath != null) {
                        int orientation = GetExifOrientation(item.getAbsolutePath());
                        mGlideRequestManager
                                .load(item.getStrThumbnailPath() != null ? item.getStrThumbnailPath() : videothumbPath)
                                .asBitmap()
                                .transform(new RotateTransformation(context, orientation))
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .signature(new StringSignature(String.valueOf(item.getAbsolutePath() + item.getStrThumbnailPath() != null ? item.lastModified() : new File(videothumbPath).lastModified())))
                                .error((mode == MODE.LISTVIEW.ordinal()) ? (flag_FileType == FileTypeUtil.FLAG_PICTURE ? R.drawable.fileview_image_ic : R.drawable.fileview_movie_ic) : (flag_FileType == FileTypeUtil.FLAG_PICTURE ? R.drawable.fileview_image_ic_small : R.drawable.fileview_movie_ic_small)) // Uri of the picture
                                .into(holder.iv_Thumb);
                    } else if (item.getStrThumbnailPath() == null && flag_FileType == FileTypeUtil.FLAG_PICTURE && item.getAbsolutePath() != null) {
                        mGlideRequestManager
                                .load(item.getAbsolutePath())
                                .asBitmap()
                                .centerCrop()
                                .dontTransform()
                                .dontAnimate()
                                .signature(new StringSignature(String.valueOf(item.getAbsolutePath() + new File(item.getAbsolutePath()).lastModified())))
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .error((mode == MODE.LISTVIEW.ordinal()) ? (flag_FileType == FileTypeUtil.FLAG_PICTURE ? R.drawable.fileview_image_ic : R.drawable.fileview_movie_ic) : (flag_FileType == FileTypeUtil.FLAG_PICTURE ? R.drawable.fileview_image_ic_small : R.drawable.fileview_movie_ic_small)) // Uri of the picture
                                .into(holder.iv_Thumb);

                    } else {
                        holder.iv_Thumb.setImageResource(nRes);
                        mGlideRequestManager
                                .load(nRes)
                                .into(holder.iv_Thumb);
                    }
                }
            }
        } else if (mode == MODE.SECTION.ordinal()) {
            int orientation;
            FileTypeUtil fileTypeUtil = new FileTypeUtil();
            final int flag_FileType = fileTypeUtil.getFilyType(item);
            if (item.getStrThumbnailPath() != null) {
                orientation = GetExifOrientation(item.getAbsolutePath());
                mGlideRequestManager
                        .load(item.getStrThumbnailPath())
                        .asBitmap()
                        .transform(new RotateTransformation(context, orientation))
                        .signature(new StringSignature(String.valueOf(item.getAbsolutePath() + new File(item.getAbsolutePath()).lastModified())))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error((mode == MODE.LISTVIEW.ordinal()) ? (flag_FileType == FileTypeUtil.FLAG_PICTURE ? R.drawable.fileview_image_ic : R.drawable.fileview_movie_ic) : (flag_FileType == FileTypeUtil.FLAG_PICTURE ? R.drawable.fileview_image_ic_small : R.drawable.fileview_movie_ic_small)) // Uri of the picture
                        .placeholder((mode == MODE.LISTVIEW.ordinal()) ? (flag_FileType == FileTypeUtil.FLAG_PICTURE ? R.drawable.fileview_image_ic : R.drawable.fileview_movie_ic) : (flag_FileType == FileTypeUtil.FLAG_PICTURE ? R.drawable.fileview_image_ic_small : R.drawable.fileview_movie_ic_small))
                        .into(holder.iv_Thumb);
            } else if (item.getAbsolutePath() != null) {
                mGlideRequestManager
                        .load(item.getAbsolutePath())
                        .asBitmap()
                        .dontTransform()
                        .signature(new StringSignature(String.valueOf(item.getAbsolutePath() + new File(item.getAbsolutePath()).lastModified())))
                        .dontAnimate()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error((mode == MODE.LISTVIEW.ordinal()) ? (flag_FileType == FileTypeUtil.FLAG_PICTURE ? R.drawable.fileview_image_ic : R.drawable.fileview_movie_ic) : (flag_FileType == FileTypeUtil.FLAG_PICTURE ? R.drawable.fileview_image_ic_small : R.drawable.fileview_movie_ic_small)) // Uri of the picture
                        .placeholder((mode == MODE.LISTVIEW.ordinal()) ? (flag_FileType == FileTypeUtil.FLAG_PICTURE ? R.drawable.fileview_image_ic : R.drawable.fileview_movie_ic) : (flag_FileType == FileTypeUtil.FLAG_PICTURE ? R.drawable.fileview_image_ic_small : R.drawable.fileview_movie_ic_small))
                        .into(holder.iv_Thumb);
            }
            holder.ib_Checkbox.setVisibility(View.VISIBLE);
        }
        if (holder.iv_Thumb != null) {
            holder.iv_Thumb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemListener.onItemClick(position);
                }
            });
        }
        if (holder.fl_view_filelist_content != null) {
            holder.fl_view_filelist_content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemListener.onItemClick(position);
                }
            });
        }
        if (holder.ib_Checkbox != null) {
            if (mode == MODE.SECTION.ordinal()) {
                holder.ib_Checkbox.setImageResource(item.isCheck() ?
                        (theme == THEME.BLUE.ordinal() ? R.drawable.fileview_check2 : R.drawable.fileview_check_lime2) :
                        (R.drawable.fileview_uncheck_lime2));
            } else {
                holder.ib_Checkbox.setImageResource(item.isCheck() ?
                        (theme == THEME.BLUE.ordinal() ? R.drawable.fileview_check : R.drawable.fileview_check_lime) :
                        (theme == THEME.BLUE.ordinal() ? R.drawable.fileview_uncheck : R.drawable.fileview_uncheck_lime));
            }
            holder.ib_Checkbox.setTag(item.getAbsolutePath());
            final MainVH finalViewHolder = holder;
            holder.ib_Checkbox.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO 파일리스트에 있는 체크박스 클릭 이벤트
                    if (item.isCheck()) {
                        if (mode == MODE.SECTION.ordinal()) {
                            finalViewHolder.ib_Checkbox.setImageResource(R.drawable.fileview_uncheck_lime2);
                        } else {
                            finalViewHolder.ib_Checkbox.setImageResource(theme == THEME.BLUE.ordinal() ? R.drawable.fileview_uncheck : R.drawable.fileview_uncheck_lime);
                        }

                        String strFilePath = (String) finalViewHolder.ib_Checkbox.getTag();
                        long nFileSize = item.length();
                        list_Checked.remove(strFilePath);
                        if (!item.isDirectory())
                            nCheckedTotalSize -= nFileSize;
                        item.setCheck(false);
                    } else {
                        if (mode == MODE.SECTION.ordinal()) {
                            finalViewHolder.ib_Checkbox.setImageResource(theme == THEME.BLUE.ordinal() ? R.drawable.fileview_check2 : R.drawable.fileview_check_lime2);
                        } else {
                            finalViewHolder.ib_Checkbox.setImageResource(theme == THEME.BLUE.ordinal() ? R.drawable.fileview_check : R.drawable.fileview_check_lime);
                        }

                        String strFilePath = (String) finalViewHolder.ib_Checkbox.getTag();
                        list_Checked.add(strFilePath);
                        if (!item.isDirectory())
                            nCheckedTotalSize += item.length();
                        item.setCheck(true);
                    }
                    if (mode == MODE.SECTION.ordinal()) {
                        isDayCheck(item.getDateTaken() > 0 ? item.getDateTaken() : item.lastModified());
                    }
                    isAllCheck();
                    if (checkChangeCallback != null)
                        checkChangeCallback.onCheckedChange(getList_Checked().size(), getCount(), nCheckedTotalSize);
                }
            });
        }
    }

    private String getThumnailPath(Context context, String filePath) {
        String thubmnailPath;
        String where = MediaStore.Video.Thumbnails.VIDEO_ID
                + " In ( select _id from video where _data =?)";
        final String[] VIDEO_THUMBNAIL_TABLE = new String[]{MediaStore.Video.Media._ID, // 0
                MediaStore.Video.Media.DATA, // 1
        };
        Uri videoUri = MediaStore.Video.Thumbnails.getContentUri("external");

        Cursor c = context.getContentResolver().query(videoUri,
                VIDEO_THUMBNAIL_TABLE, where, new String[]{filePath}, null);

        if ((c != null) && c.moveToFirst()) {
            thubmnailPath = c.getString(1);
            c.close();
            return thubmnailPath;
        } else {
            c.close();
            return null;
        }
    }

    @Override
    public int getItemViewType(int section, int relativePosition, int absolutePosition) {
        return super.getItemViewType(section, relativePosition, absolutePosition);


    }

    @Override
    public MainVH onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = 0;
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                layout = R.layout.flyingfile_grid_header_imagemodel_view;
                break;
            case VIEW_TYPE_ITEM:
                if (mode == MODE.SECTION.ordinal()) {
                    layout = R.layout.flyingfile_view_file_section_item;
                } else if (mode == MODE.LISTVIEW.ordinal() || mode == MODE.APK.ordinal()) {
                    layout = R.layout.flyingfile_view_filelist_item;
                } else if (mode == MODE.GRIDVIEW.ordinal()) {
                    layout = R.layout.flyingfile_view_filegrid_item;
                }

                break;
            case TYPE_FOOTER:
                layout = R.layout.flyingfile_layout_footer;
                break;
        }

        View v = LayoutInflater.from(parent.getContext())
                .inflate(layout, parent, false);
        return new MainVH(v);
    }

    public void isAllCheck() {
        if (contentData.size() == list_Checked.size() && contentData.size() > 0 && list_Checked.size() > 0) {
            ib_CheckAll.setImageResource(theme == THEME.BLUE.ordinal() ? R.drawable.fileview_check : R.drawable.fileview_check_lime);

            ib_CheckAll.setTag(true);
        } else {
            ib_CheckAll.setImageResource(theme == THEME.BLUE.ordinal() ? R.drawable.fileview_uncheck : R.drawable.fileview_uncheck_lime);
            ib_CheckAll.setTag(false);
        }
    }

    public void isSelectionCheck() {
        if (contentData.size() == list_Checked.size() && contentData.size() > 0 && list_Checked.size() > 0) {
            for (int i = 0; i < sectionData.size(); i++) {
                sectionData.get(i).setCheck(true);
            }
        }
    }

    public void dayCheck(boolean isCheck, String day) {
        int start = 0;
        int end = 0;
        int cnt = 0;
        boolean isChange = false;
        for (int i = 0; i < sectionData.size(); i++) {
            if (start == i) {
                start = 0;
            } else {
                start += sectionData.get(i - 1).getContentCnt();
            }
            cnt++;
            end += sectionData.get(i).getContentCnt();
            if (sectionData.get(i).getDay().equals(day)) {
                for (int j = start; j < end; j++) {

                    FileItem item = contentData.get(j);
                    String fileDate;
                    if (contentData.get(j).getDateTaken() > 0) {
                        fileDate = format.format(contentData.get(j).getDateTaken());
                    } else {
                        fileDate = format.format(contentData.get(j).lastModified());
                    }

                    if (!item.isDot()) {
                        if (fileDate.equals(day)) {
                            if (isCheck) {
                                if (!item.isCheck()) {
                                    String strFilePath = item.getAbsolutePath();
                                    long nFileSize = item.length();
                                    list_Checked.add(strFilePath);
                                    if (!item.isDirectory())
                                        nCheckedTotalSize += nFileSize;
                                    item.setCheck(isCheck);
                                }
                            } else {
                                if (item.isCheck()) {
                                    String strFilePath = item.getAbsolutePath();
                                    long nFileSize = item.length();
                                    list_Checked.remove(strFilePath);
                                    if (!item.isDirectory())
                                        nCheckedTotalSize -= nFileSize;
                                    item.setCheck(isCheck);
                                }
                            }
                        }
                    }
                }
            }
        }

        try {
            notifyDataSetChanged();
        } catch (Exception e) {

        }

        if (checkChangeCallback != null) {
            checkChangeCallback.onCheckedChange(list_Checked.size(), getCount(), nCheckedTotalSize);
        }
    }

    private void isDayCheck(final long intDay) {
        String day = format.format(intDay);
        dayCheck(day);
    }

    public void isAllDayCheck() {
        for (int i = 0; i < sectionData.size(); i++) {
            dayCheck(sectionData.get(i).getDay());
        }
    }

    private void dayCheck(String day) {
        boolean isAllCheck = true;
        boolean isAllUnCheck = true;
        int start = 0;
        int end = 0;
        int cnt = 0;
        boolean isChange = false;
        for (int i = 0; i < sectionData.size(); i++) {
            if (start == i) {
                start = 0;
            } else {
                start += sectionData.get(i - 1).getContentCnt();
            }
            cnt++;
            end += sectionData.get(i).getContentCnt();
            if (sectionData.get(i).getDay().equals(day)) {
                for (int j = start; j < end; j++) {
                    cnt++;
                    FileItem item = contentData.get(j);
                    if (item.isCheck()) {
                        isAllCheck = false;
                    } else {
                        isAllUnCheck = false;
                    }

                }
                if (isAllCheck) {
                    if (!sectionData.get(i).isCheck()) {
                        sectionData.get(i).setCheck(true);
                        isChange = true;
                    }
                } else {
                    if (sectionData.get(i).isCheck()) {
                        sectionData.get(i).setCheck(false);
                        isChange = true;
                    }
                }
                if (isAllUnCheck) {
                    if (!sectionData.get(i).isCheck()) {
                        sectionData.get(i).setCheck(true);
                        isChange = true;
                    }
                } else {
                    if (sectionData.get(i).isCheck()) {
                        sectionData.get(i).setCheck(false);
                        isChange = true;
                    }
                }

            }
        }
        if (isChange) {
            notifyDataSetChanged();
        }
    }

    public void setCheckAll(boolean isCheck) {

        String lastDate = "";
        for (int i = 0; i < getCount(); i++) {
            FileItem item = getItem(i);
            if (!item.isDot()) {
                if (isCheck) {
                    if (!item.isCheck()) {
                        String strFilePath = getItem(i).getAbsolutePath();
                        long nFileSize = getItem(i).length();
                        list_Checked.add(strFilePath);
                        if (!item.isDirectory())
                            nCheckedTotalSize += nFileSize;
                        item.setCheck(isCheck);
                    }
                } else {
                    if (item.isCheck()) {
                        String strFilePath = getItem(i).getAbsolutePath();
                        long nFileSize = getItem(i).length();
                        list_Checked.remove(strFilePath);
                        if (!item.isDirectory())
                            nCheckedTotalSize -= nFileSize;
                        item.setCheck(isCheck);
                    }
                }
            }

            if (mode == MODE.SECTION.ordinal()) {
                for (int j = 0; j < sectionData.size(); j++) {
                    sectionData.get(j).setCheck(isCheck);
                }
            }
        }
        try {
            notifyDataSetChanged();
        } catch (Exception e) {

        }

        if (checkChangeCallback != null) {
            checkChangeCallback.onCheckedChange(list_Checked.size(), getCount(), nCheckedTotalSize);
        }
    }

    public List<HeaderDomain> setAPKSection(Context context, List<FileItem> contentData) {
        final List<HeaderDomain> alContent = new ArrayList<>();
        try {
            int systemCnt = 0, customCnt = 0;


            if (contentData.size() > 0) {
                FileItem item = contentData.get(0);
                if (item.length() == 0) return null;
                for (int i = 0; i < contentData.size(); i++) {
                    if (Integer.parseInt(contentData.get(i).getStrFileType()) != 0) {
                        systemCnt++;
                    } else {
                        customCnt++;
                    }
                }
            }


            alContent.add(new HeaderDomain(customCnt, false, "   " + context.getString(R.string.string_custom_app) + " (" + customCnt + ") "));
            alContent.add(new HeaderDomain(systemCnt, false, "   " + context.getString(R.string.string_system_app) + " (" + systemCnt + ") "));
            return alContent;
        } catch (Exception e) {
        }
        return alContent;
    }

    public void clearCheck() {
        nCheckedTotalSize = 0;
        list_Checked.clear();
        if (contentData == null) return;
        if (sectionData == null) return;
        for (int i = 0; i < getCount(); i++) {
            getItem(i).setCheck(false);
        }
        for (int i = 0; i < sectionData.size(); i++) {
            sectionData.get(i).setCheck(false);
        }
        if (checkChangeCallback != null) {
            checkChangeCallback.onCheckedChange(list_Checked.size(), getCount(), nCheckedTotalSize);
        }
    }

    public void notifiCheckedTotalSize() {
        if (checkChangeCallback != null) {
            checkChangeCallback.onCheckedChange(list_Checked.size(), getCount(), nCheckedTotalSize);
        }
    }

    public ArrayList<String> getList_Checked() {
        return list_Checked;
    }

    public void addCheckedTotalSize(long total) {
        nCheckedTotalSize += total;
    }

    public long get_Integer_FileSize() {
        return nCheckedTotalSize;
    }

    public List<FileItem.DELETE> deleteAll(Context context, List<FileItem> file, DeleteCallback deleteCallback) {
        List<FileItem.DELETE> result = new ArrayList<>();
        StringBuilder alArray = new StringBuilder();

        for (int i = 0; i < file.size(); i++) {
            FileItem item = file.get(i);
            if (!item.isCheck()) {
                result.add(FileItem.DELETE.NONE);
            } else {
                result.add(FileItem.DELETE.FAIL);
                if (!PermissionUtil.getInstance().isAbleFileWritePath(item.getAbsolutePath())) {
                    result.set(i, FileItem.DELETE.RESTRICTION);
                    continue;
                }
                if (item.isDirectory()) {
                    long size = DocumentUtil.getDeleteFolderSize(item.getAbsoluteFile());
                    if (size == 0) {
                        result.set(i, FileItem.DELETE.SUCCESS);
                    } else {
                        result.set(i, FileItem.DELETE.NONEMPTY);
                    }
                } else {

                    if (item.exists()) {
                        try {
                            boolean delete = item.delete();
                            if (delete) {
                                result.set(i, FileItem.DELETE.SUCCESS);
                                //삭제되었으면 미디어 스캔 값을 삭제한다.
                                FileTypeUtil fileTypeUtil = new FileTypeUtil();
                                switch (fileTypeUtil.getFilyType(item)) {
                                    case FileTypeUtil.FLAG_PICTURE:
                                    case FileTypeUtil.FLAG_MOVIE: {
                                        String strPath = item.getAbsolutePath();
                                        strPath = strPath.replace("'", "''");
                                        alArray.append(MediaStore.Images.Media.DATA)
                                                .append("='")
                                                .append(strPath)
                                                .append("' AND ");
                                        if ((i + 1) % 500 == 0) {
                                            contentDelete(alArray);
                                            alArray = new StringBuilder();
                                        }
                                        break;
                                    }
                                }
                            } else {
                                result.set(i, FileItem.DELETE.FAIL);
                            }
                        } catch (Exception e) {
                            result.set(i, FileItem.DELETE.FAIL);
                        }
                    } else {
                        //파일이 존재하지 않는다면 삭제된것이다.
                        result.set(i, FileItem.DELETE.SUCCESS);
                    }
                }
            }
        }


        if (alArray.toString().length() > 5) {
            contentDelete(alArray);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {//롤리팝 && 외장 메모리인경우 삭제 방식.

            String sdCardUri = SharedPreferenceUtil.getInstance().getMobileDefaultUri(context);
            if (sdCardUri == null) {
                return result;
            } else { //uri값 저장.
                HashMap<String, String> hash = new HashMap<>();
                for (int i = 0; i < file.size(); i++) {
                    if (file.get(i).isCheck()) {
                        String path = file.get(i).getAbsolutePath();
                        path = path.substring(0, path.lastIndexOf("/"));
                        hash.put(path, path);
                    }
                }
                HashMap<String, Uri> alName = new HashMap<>();
                Iterator it2 = hash.values().iterator();
                while (it2.hasNext()) {
                    String i = (String) it2.next();
                    String defaultPath = DocumentUtil.getExtSdCardFolder(context, i);
                    if (defaultPath != null) {
                        DocumentUtil.getChildAllUri(context, i.replace(defaultPath, ""), alName);
                    }
                }
                int cntSuccess = 0;
                for (int i = 0; i < file.size(); i++) {
                    if (file.get(i).isCheck()) {
                        if (result.get(i) == FileItem.DELETE.RESTRICTION) {
                            FileItem.DELETE intDelete = DocumentUtil.documentDelete(context, file.get(i), alName);
                            if (intDelete != null) {
                                result.set(i, intDelete);
                            }
                        }
                        cntSuccess = cntSuccess + 1;
                        if (deleteCallback != null) deleteCallback.onProgressDelete(cntSuccess);
                    }
                }
            }

        }
        return result;
    }

    private void contentDelete(StringBuilder alArray) {
        //미디어 스토어에서도 삭제해주자 //쿼리가 너무 길면 익셉션나서 분리함.
        ContentResolver resolver = context.getContentResolver();
        if (alArray.length() > 0) {
            Cursor cursor = resolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, alArray.toString().substring(0, alArray.length() - 5), null, null);
            StringBuilder alIdArray = new StringBuilder();
            if (cursor != null && cursor.moveToFirst()) {
                int cnt = 1;
                do {
                    int nImageID = cursor.getInt(cursor.getColumnIndex(MediaStore.Images.Media._ID));
                    alIdArray.append(MediaStore.Images.Media._ID)
                            .append("=").append(nImageID).append(" AND ");
                    cnt++;
                    if (cnt % 500 == 0) {
                        int nCnt = resolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, alIdArray.toString().substring(0, alIdArray.length() - 5), null);
                        alIdArray = new StringBuilder();
                    }
                } while (cursor.moveToNext());
                if (alIdArray.toString().length() > 5) {
                    int nCnt = resolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, alIdArray.toString().substring(0, alIdArray.length() - 5), null);
                }
            }
            cursor.close();
        }

    }

    @Override
    synchronized public Filter getFilter() {
        // TODO Auto-generated method stub
        Filter filter = new Filter() {


            @SuppressWarnings("unchecked")
            @Override
            synchronized protected void publishResults(CharSequence constraint, FilterResults results) {
                // TODO Auto-generated method stub
                contentData = (ArrayList<FileItem>) results.values;
                if (contentData == null) {
                    contentData = new ArrayList<FileItem>();
                }
                notifyDataSetChanged();
            }

            @Override
            synchronized protected FilterResults performFiltering(CharSequence constraint) {
                // TODO Auto-generated method stub
                FilterResults results = new FilterResults();
                ArrayList<FileItem> filteredArrList = new ArrayList<FileItem>();
                if (constraint == null || constraint.length() == 0) {
                    results.count = dummyData.size();
                    results.values = dummyData;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < dummyData.size(); i++) {
                        String strPath = dummyData.get(i).getAbsolutePath();
                        String data = dummyData.get(i).getName();


                        boolean isNeedNormalize = Normalizer.isNormalized(data, Normalizer.Form.NFD);
                        if (isNeedNormalize) {
                            data = Normalizer.normalize(data, Normalizer.Form.NFC);
                        }

                        if (data.toLowerCase().matches(".*" + constraint.toString() + ".*")) {
//							String strThumbnailPath = mOriginalValues.get(i).getStrThumbnailPath();
                            FileItem item = new FileItem(strPath);
                            item.setStrThumbnailPath(dummyData.get(i).getStrThumbnailPath());
                            filteredArrList.add(item);
                        }
                    }
                    if (filteredArrList.size() == 0) {
                        //일치하는게 없으면 아예보여주지 말자
                    } else {
                        results.count = filteredArrList.size();
                        results.values = filteredArrList;
                    }
                }


                return results;
            }
        };
        return filter;
    }

    private String changeDate(String preDate) {
        try {

            java.util.Calendar cal = java.util.Calendar.getInstance();
            int year = cal.get(cal.YEAR);
            String[] arrDate = preDate.split("-");
            boolean isKor = LanguageUtil.getInstance().getLanguage(context).equals(LanguageUtil.Language.KOREAN);
            if (arrDate[0].equals(String.valueOf(year)) && isKor) {
                return arrDate[1] + context.getString(R.string.month) + arrDate[2] + context.getString(R.string.day);
            } else {
                return arrDate[0] + context.getString(R.string.year) + arrDate[1] + context.getString(R.string.month) + arrDate[2] + context.getString(R.string.day);
            }
        } catch (Exception e) {
        }
        return preDate;

    }

    public synchronized int GetExifOrientation(String filepath) {
        int degree = 0;
        ExifInterface exif = null;

        try {
            exif = new ExifInterface(filepath);
        } catch (IOException e) {
            Log.e("cannot", "cannot read exif");
            e.printStackTrace();
        }

        if (exif != null) {
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);

            if (orientation != -1) {
                // We only recognize a subset of orientation tag values.
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        degree = 90;
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        degree = 180;
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        degree = 270;
                        break;
                }

            }
        }
        return degree;
    }

    public enum MODE {
        LISTVIEW, GRIDVIEW, SECTION, APK
    }


    public enum THEME {
        BLUE, LIME
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public static class MainVH extends RecyclerView.ViewHolder {
        final TextView title;
        //header
        private final LinearLayout ll_headerLayout, ll_file_content;
        private final TextView tv_headerText;
        private final ImageButton ibtn_header_imagemodel_view;
        //end of header
        private final TextView TextView_FileName;
        private View v_header_line, v_header_empty;
        private ImageButton ib_Checkbox;
        private LinearLayout ll_Divider;
        private ImageView iv_Thumb;
        private TextView tv_FileName;
        private TextView tv_FileSize;
        private TextView tv_FileDate;
        private ImageView iv_SubImage;
        private LinearLayout ll_Background;
        private FrameLayout fl_Info;
        private FrameLayout fl_view_filelist_content;
        //end of content
        private int nPos;

        public MainVH(View itemView) {
            super(itemView);
            ll_headerLayout = (LinearLayout) itemView.findViewById(R.id.ll_headerLayout);
            v_header_line = (View) itemView.findViewById(R.id.v_header_line);
            v_header_empty = (View) itemView.findViewById(R.id.v_header_empty);
            ll_file_content = (LinearLayout) itemView.findViewById(R.id.ll_file_content);
            tv_headerText = (TextView) itemView.findViewById(R.id.tv_headerText);
            ibtn_header_imagemodel_view = (ImageButton) itemView.findViewById(R.id.ibtn_header_imagemodel_view);
            title = (TextView) itemView.findViewById(R.id.title);
            TextView_FileName = (TextView) itemView.findViewById(R.id.TextView_FileName);
            /// end header
            fl_view_filelist_content = (FrameLayout) itemView.findViewById(R.id.fl_view_filelist_content);
            fl_Info = (FrameLayout) itemView.findViewById(R.id.FrameLayout_FileInfoLayout);
            ib_Checkbox = (ImageButton) itemView.findViewById(R.id.ImageButton_FileCheck);
            iv_SubImage = (ImageView) itemView.findViewById(R.id.ImageView_SubImage);
            iv_Thumb = (ImageView) itemView.findViewById(R.id.ImageView_Icon);

            ll_Background = (LinearLayout) itemView.findViewById(R.id.LinearLayout_Background);
            ll_Divider = (LinearLayout) itemView.findViewById(R.id.LinearLayout_Divider);
            tv_FileDate = (TextView) itemView.findViewById(R.id.TextView_FileDate);
            tv_FileName = (TextView) itemView.findViewById(R.id.TextView_FileName);
            tv_FileSize = (TextView) itemView.findViewById(R.id.TextView_FileMemory);
            //end content
        }
    }
}