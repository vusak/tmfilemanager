package jiran.com.flyingfile.fileid.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import jiran.com.flyingfile.util.SharedPreferenceUtil;
import jiran.com.tmfilemanager.R;


/**
 * Created by jeon on 2016-12-23.
 */

public class CategoryTabFragment extends Fragment {

//    public boolean colorMode = true; //true: 블루모드 , false:청록색모드
//    public int defaultTab = -1;
//    int prePosition;
//    private int tabCount = 6;
//    private LinearLayout[] arrLinear = new LinearLayout[tabCount];
//    private ImageView[] arrIbtn = new ImageView[tabCount];
//    private TextView[] arrTv = new TextView[tabCount];
//    private int maxScrollX;
//    private HorizontalScrollView sv;
//    private ImageView iv_category_tab_right_arrow, iv_category_tab_left_arrow;
//    private View.OnClickListener tabClick;
//
//    public void setColorMode(boolean colorMode) {
//        this.colorMode = colorMode;
//    }
//
//    public void setCreateListener(View.OnClickListener tabClick) {
//        this.tabClick = tabClick;
//    }
//
//    public void setTab(int tab) {
//        defaultTab = tab;
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater,
//                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.flyingfile_fragment_category_tab, container, false);
//
//        LinearLayout ll_category_tab_container = (LinearLayout) view.findViewById(R.id.ll_category_tab_container);
//        sv = new HorizontalScrollView(getActivity());
//        sv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
//        sv.setHorizontalScrollBarEnabled(false);
//        LinearLayout ll = new LinearLayout(getActivity());
//        ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
//        ll.setOrientation(LinearLayout.HORIZONTAL);
//        sv.addView(ll);
//        for (int i = 0; i < 6; i++) {
//            ll.addView(initTab(i));
//        }
//        //  setTabState(SharedPreferenceUtil.FLAG_CATEGORYMODE_PICTURE);
//
//        iv_category_tab_right_arrow = (ImageView) view.findViewById(R.id.iv_category_tab_right_arrow);
//        iv_category_tab_left_arrow = (ImageView) view.findViewById(R.id.iv_category_tab_left_arrow);
//        ViewTreeObserver vto = sv.getViewTreeObserver();
//        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                try {
//                    sv.getViewTreeObserver().removeGlobalOnLayoutListener(this);
//                    maxScrollX = sv.getChildAt(0)
//                            .getMeasuredWidth() - getActivity().getWindowManager().getDefaultDisplay().getWidth();
//                } catch (Exception e) {
//                }
//
//            }
//        });
//
//        sv.setOnTouchListener(new View.OnTouchListener() { // 스크롤시 화살표 숨기는 기능.
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//
//
//                iv_category_tab_right_arrow.setVisibility(View.VISIBLE);
//                iv_category_tab_left_arrow.setVisibility(View.VISIBLE);
//                if (sv.getScrollX() <= maxScrollX && sv.getScrollX() >= (maxScrollX - 50)) {
//                    iv_category_tab_right_arrow.setVisibility(View.GONE);
//                    if (prePosition < sv.getScrollX()) {
//                        sv.scrollTo(maxScrollX, 0);
//                    }
//                } else if (sv.getScrollX() >= 0 && sv.getScrollX() <= 50) {
//                    iv_category_tab_left_arrow.setVisibility(View.GONE);
//                    if (prePosition > sv.getScrollX()) {
//                        sv.scrollTo(0, 0);
//                    }
//                }
//                prePosition = sv.getScrollX();
//                return false;
//            }
//        });
//        ll_category_tab_container.addView(sv);
//        if (defaultTab != -1) {
//            setTabState(defaultTab);
//        }
//        return view;
//    }
//
//    /**
//     * 동적으로 탭추가.
//     *
//     * @param position 탭번호 SharedPreferenceUtil 에서 값 가져 오시오.
//     * @return
//     */
//    private View initTab(int position) {
//        DisplayMetrics dm = getActivity().getResources().getDisplayMetrics();
//        int width = dm.widthPixels;
//        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width / 5, ViewGroup.LayoutParams.WRAP_CONTENT);
//        LinearLayout.LayoutParams layoutParamswm = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        LinearLayout.LayoutParams layoutParamsmm = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        LinearLayout.LayoutParams lineparams = new LinearLayout.LayoutParams(1, ViewGroup.LayoutParams.WRAP_CONTENT);
//        FrameLayout ll_box = new FrameLayout(getActivity());
//        ll_box.setLayoutParams(layoutParams);
//        LinearLayout ll = new LinearLayout(getActivity());
//        ll.setOrientation(LinearLayout.VERTICAL);
//        ll.setGravity(Gravity.CENTER);
//        ll.setLayoutParams(layoutParamsmm);
//        ll.setBackgroundResource(colorMode ? R.drawable.ic_bg2_blue : R.drawable.ic_bg);
//        ImageView ll_ib = new ImageView(getActivity());
//        ll_ib.setId(position);
//        ll_ib.setBackgroundColor(Color.TRANSPARENT);
//        TextView ll_tv = new TextView(getActivity());
//        ll_tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, getActivity().getResources().getDimension(R.dimen.FontSize_File_Contents));
//
//
//        ll_tv.setLayoutParams(layoutParamswm);
//        ImageView iv = new ImageView(getActivity());
//        iv.setLayoutParams(lineparams);
//        iv.setBackgroundResource(R.drawable.line_bg_blue);
//        //iv.setBackgroundColor(Color.RED);
//        ll.addView(ll_ib);
//        ll.addView(ll_tv);
//        ll_box.addView(ll);
//        if (position != 0) {
//            ll_box.addView(iv);
//        }
//        ll_ib.setOnClickListener(tabClick);
//
//        arrLinear[position] = ll;
//        arrIbtn[position] = ll_ib;
//        arrTv[position] = ll_tv;
//
//        switch (position) {
//            case SharedPreferenceUtil.FLAG_CATEGORYMODE_EXPLORER: { //탐색기탭
//                ll_ib.setImageResource(R.drawable.ic_off00);
//                ll_tv.setText(R.string.MtoM_Tab_Caption_Inbox);
//            }
//            break;
//            case SharedPreferenceUtil.FLAG_CATEGORYMODE_PICTURE: { //사진 탭
//                ll_ib.setImageResource(R.drawable.ic_off01);
//                ll_tv.setText(R.string.MtoM_Tab_Caption_Picture);
//            }
//            break;
//            case SharedPreferenceUtil.FLAG_CATEGORYMODE_MUSIC: { // 음악탭
//                ll_ib.setImageResource(R.drawable.ic_off02);
//                ll_tv.setText(R.string.MtoM_Tab_Caption_Music);
//            }
//            break;
//            case SharedPreferenceUtil.FLAG_CATEGORYMODE_MOVIE: { //영상탭
//                ll_ib.setImageResource(R.drawable.ic_off03);
//                ll_tv.setText(R.string.MtoM_Tab_Caption_Video);
//            }
//            break;
//            case SharedPreferenceUtil.FLAG_CATEGORYMODE_DOCUMENT: { //문서탭
//                ll_ib.setImageResource(R.drawable.ic_off04);
//                ll_tv.setText(R.string.MtoM_Tab_Caption_Document);
//            }
//            break;
//            case SharedPreferenceUtil.FLAG_CATEGORYMODE_APK: {//aPK 탭
//                ll_ib.setImageResource(R.drawable.ic_off05);
//                ll_tv.setText(R.string.MtoM_Tab_Caption_APK);
//            }
//            break;
//
//        }
//
//        return ll_box;
//    }
//
//    public void setTabState(int nCategory) {
//        try {
//            for (int i = 0; i < tabCount; i++) {
//
//                arrLinear[i].setBackgroundResource(colorMode ? R.drawable.ic_bg2_blue : R.drawable.ic_bg);
//                arrTv[i].setTextColor(Color.BLACK);
//            }
//            arrIbtn[0].setImageResource(R.drawable.ic_off00);
//            arrIbtn[1].setImageResource(R.drawable.ic_off01);
//            arrIbtn[2].setImageResource(R.drawable.ic_off02);
//            arrIbtn[3].setImageResource(R.drawable.ic_off03);
//            arrIbtn[4].setImageResource(R.drawable.ic_off04);
//            arrIbtn[5].setImageResource(R.drawable.ic_off05);
//
//            arrLinear[nCategory].setBackgroundResource(colorMode ? R.drawable.ic_bg2_ov_blue : R.drawable.ic_bg2_ov);
//            arrTv[nCategory].setTextColor(Color.WHITE);
//            switch (nCategory) {
//                case SharedPreferenceUtil.FLAG_CATEGORYMODE_EXPLORER: {
//                    arrIbtn[nCategory].setImageResource(colorMode ? R.drawable.ic_on00_blue : R.drawable.ic_on00);
//                    break;
//                }
//                case SharedPreferenceUtil.FLAG_CATEGORYMODE_PICTURE: {
//                    arrIbtn[nCategory].setImageResource(colorMode ? R.drawable.ic_on01_blue : R.drawable.ic_on01);
//                    break;
//                }
//                case SharedPreferenceUtil.FLAG_CATEGORYMODE_MUSIC: {
//                    arrIbtn[nCategory].setImageResource(colorMode ? R.drawable.ic_on02_blue : R.drawable.ic_on02);
//                    break;
//                }
//
//                case SharedPreferenceUtil.FLAG_CATEGORYMODE_MOVIE: {
//                    arrIbtn[nCategory].setImageResource(colorMode ? R.drawable.ic_on03_blue : R.drawable.ic_on03);
//                    break;
//                }
//                case SharedPreferenceUtil.FLAG_CATEGORYMODE_DOCUMENT: {
//                    arrIbtn[nCategory].setImageResource(colorMode ? R.drawable.ic_on04_blue : R.drawable.ic_on04);
//                    break;
//                }
//                case SharedPreferenceUtil.FLAG_CATEGORYMODE_APK: {
//                    arrIbtn[nCategory].setImageResource(colorMode ? R.drawable.ic_on05_blue : R.drawable.ic_on05);
//                    break;
//                }
//                default:
//                    break;
//            }
//        } catch (Exception e) {
//            e.getStackTrace();
//        }
//        if (nCategory == SharedPreferenceUtil.FLAG_CATEGORYMODE_APK) {
//            sv.scrollTo(maxScrollX, 0);
//            iv_category_tab_right_arrow.setVisibility(View.GONE);
//            iv_category_tab_left_arrow.setVisibility(View.VISIBLE);
//        } else if (nCategory == SharedPreferenceUtil.FLAG_CATEGORYMODE_EXPLORER) {
//            iv_category_tab_right_arrow.setVisibility(View.VISIBLE);
//            iv_category_tab_left_arrow.setVisibility(View.GONE);
//        }
//    }
}
