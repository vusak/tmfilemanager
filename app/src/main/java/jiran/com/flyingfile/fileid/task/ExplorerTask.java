package jiran.com.flyingfile.fileid.task;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import jiran.com.flyingfile.fileid.adapter.BaseFileAdapter;
import jiran.com.flyingfile.model.FileItem;
import jiran.com.flyingfile.util.Common;
import jiran.com.flyingfile.util.DocumentUtil;
import jiran.com.flyingfile.util.FileFilterUtil;
import jiran.com.flyingfile.util.FileItemCompareUtil;
import jiran.com.flyingfile.util.FileManager;
import jiran.com.flyingfile.util.SharedPreferenceUtil;
import jiran.com.tmfilemanager.MyApplication;

/**
 * Created by jeon on 2016-10-13.
 */

public class ExplorerTask extends AsyncTask<Void, Void, List<FileItem>> {


    private BaseFileAdapter adapter;

    private String strbasePath;
    private Context context;
    private int sortFlag;
    private boolean isAddDot;
    private int nCategory;

    /***
     *
     * @param context
     * @param adapter
     * @param strPath
     * @param nCategory
     * @param sortFlag
     * @param isAddDot : 상위폴더로 이동 유무 (FileIDDownload에는 없음.)
     */
    public ExplorerTask(Context context, BaseFileAdapter adapter,
                        String strPath, int nCategory, int sortFlag, boolean isAddDot) {
        this.context = context;
        this.adapter = adapter;
        this.strbasePath = strPath;
        this.nCategory = nCategory;
        this.sortFlag = sortFlag;
        this.isAddDot = isAddDot;
    }

    @Override
    protected void onPreExecute() {
        try {
            Glide.get(context).clearMemory();
            Glide.get(context).clearDiskCache();
        } catch (Exception e) {
        }
        if (adapter.getFilelistSetCallback() != null) {
            adapter.getFilelistSetCallback().onStartLoadFilelist();
        }
        adapter.clear();
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(List<FileItem> result) {
        super.onPostExecute(result);
        adapter.addAll(result);
        if (nCategory != SharedPreferenceUtil.FLAG_CATEGORYMODE_APK) {
            setSort(sortFlag);
        }
        adapter.notifiCheckedTotalSize();
        adapter.notifyDataSetChanged();
        if (adapter.getFilelistSetCallback() != null) {
            adapter.getFilelistSetCallback().onFinishLoadFilelist(strbasePath, result.size());
        }
    }

    private void setSort(int nSortFlag) {
        Collections.sort(adapter.getAll(), FileItemCompareUtil.lastModifiedAsc);
    }

    @Override
    protected List<FileItem> doInBackground(Void... params) {
        // TODO Auto-generated method stub


        List<FileItem> list;
        List<FileItem> result = new ArrayList<>();
        switch (nCategory) {
            case SharedPreferenceUtil.FLAG_CATEGORYMODE_EXPLORER: {
                //선택된 아이템 초기화

                FileItem parent = new FileItem(strbasePath);

                if (!parent.exists())
                    DocumentUtil.mkdirs(context, parent);

                adapter.setParentFile(parent);

                //현재 경로 저장
                MyApplication.getInstance().setStrCurrentMobilePath(strbasePath);

                list = Common.getInstance().functionSearchFileItemList(
                        context,
                        strbasePath,
                        FileFilterUtil.getInstance().getBaseFileFilter()
                );
                boolean isRoot = Common.getInstance().isRootDirectory(strbasePath);
                if (isAddDot) {
                    //뒤로가기 폴더 먼저 추가하고
                    if (!isRoot) {
                        FileItem dotItem = new FileItem("");
                        dotItem.setDot(true);
                        result.add(dotItem);
                    }
                }
                if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
                    //킷캣이상이고 외장SDCard가 꽂혀 있으면 바로가기 경로 추가해주자
                    File[] fileStorage = context.getExternalFilesDirs(null);
                    if (isRoot && fileStorage.length > 1 && fileStorage[1] != null) {
                        if (FileManager.getInstance().getSecondaryPackagePath(context).listFiles().length > 0) {
                            FileItem item = new FileItem(FileManager.getInstance().getSecondaryPackagePath(context).getAbsolutePath());
                            item.setnFileCntInFolder(item.list().length);
                            item.setPackageFolderShortCut(true);
                            result.add(item);
                        }
                    }
                }
                if (list != null) {
                    for (int i = 0; i < list.size(); i++) {
                        //디렉토리이면 리스트에 추가하고 아닌것는 임시 리스트에 넣었다가 나중에 한번에 추가하자
                        if (list.get(i).isDirectory()) {
                            list.get(i).setnFileCntInFolder(Common.getInstance().getFileCntInFoler(list.get(i)));

                            if (!isRoot) {
                                result.add(list.get(i));
                            } else {
                                try {
                                    String strPath = list.get(i).getAbsolutePath();
                                    if (strPath.contains("emulated")) {
                                        result.add(list.get(i));
                                    } else {
                                        if (list.get(i).list().length != 0) {
                                            result.add(list.get(i));
                                        }
                                    }

                                } catch (Exception e) {
                                }
                            }
                        } else {
                            FileItem item = list.get(i);
                            if (item.length() != 0)
                                result.add(list.get(i));
                        }
                    }
                }

                break;
            }
            case SharedPreferenceUtil.FLAG_CATEGORYMODE_DOCUMENT: {
                System.out.println("!!!!!!!!!!!" + android.os.Environment.getExternalStorageDirectory().getAbsolutePath());
                list = Common.getInstance().functionSearchAllFileItemList(
                        context,
                        android.os.Environment.getExternalStorageDirectory().getAbsolutePath(),
                        FileFilterUtil.getInstance().getDocumentFileOrFolderFilter(),
                        new Common.DocumentSearchListener() {

                            @Override
                            public boolean onSearchDocument(List<FileItem> result) {
                                return true;
                            }

                            @Override
                            public boolean onAbleListSend() {
                                return Common.getInstance().isFileListSendable();
                            }
                        });
                if (list != null) {
                    for (int i = 0; i < list.size(); i++) {
                        FileItem item = list.get(i);
                        if (item.length() != 0) {
                            result.add(list.get(i));
                        }
                    }
                }

                File secondry = FileManager.getInstance().getSecondaryStorage(context, "");
                if (secondry != null) {
                    List<FileItem> list2 = Common.getInstance().functionSearchAllFileItemList(
                            context,
                            secondry.getAbsolutePath(),
                            FileFilterUtil.getInstance().getDocumentFileOrFolderFilter(),
                            new Common.DocumentSearchListener() {
                                @Override
                                public boolean onSearchDocument(List<FileItem> result) {
                                    return true;
                                }

                                @Override
                                public boolean onAbleListSend() {
                                    return Common.getInstance().isFileListSendable();
                                }
                            });
                    if (list2 != null) {
                        for (int i = 0; i < list2.size(); i++) {
                            FileItem item = list2.get(i);
                            if (item.length() != 0) {
                                result.add(list2.get(i));
                            }
                        }
                    }
                }

                break;
            }
            case SharedPreferenceUtil.FLAG_CATEGORYMODE_MUSIC: {
                list = Common.getInstance().functionSearchAudioFileItemList(context, true);

                if (list != null) {
                    for (int i = 0; i < list.size(); i++) {
                        FileItem item = list.get(i);
                        if (item.length() != 0) {
                            result.add(list.get(i));
                        }
                    }
                }

                break;
            }
            case SharedPreferenceUtil.FLAG_CATEGORYMODE_PICTURE: {
                list = Common.getInstance().functionSearchPictureFileItemList(context, true);
                if (list != null) {
                    for (int i = 0; i < list.size(); i++) {
                        FileItem item = list.get(i);
                        if (item.length() != 0) {
                            result.add(list.get(i));
                        }
                    }
                }
                break;
            }
            case SharedPreferenceUtil.FLAG_CATEGORYMODE_MOVIE: {
                list = Common.getInstance().functionSearchVideoFileItemList(context, true);

                if (list != null) {
                    for (int i = 0; i < list.size(); i++) {
                        FileItem item = list.get(i);
                        if (item.length() != 0) {
                            result.add(list.get(i));
                        }
                    }
                }
                break;
            }
            case SharedPreferenceUtil.FLAG_CATEGORYMODE_APK: {
                list = Common.getInstance().functionSearchAPP(context);
                if (list != null) {
                    result.addAll(list);
                }
            }
            default:
                break;
        }

        return result;
    }

    public class Params {
        public String strPath;
        public int category;
        public boolean isAddDot;
        int sortFlag;

        public Params(
                String strPath,
                int category,
                int sortFlag,
                boolean isAddDot
        ) {
            super();
            this.strPath = strPath;
            this.category = category;
            this.sortFlag = sortFlag;
            this.isAddDot = isAddDot;
        }

    }
}
