package jiran.com.flyingfile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import java.util.ArrayList;

import jiran.com.flyingfile.wifidirect.activity.WifiDirectActivity;
import jiran.com.tmfilemanager.R;

/**
 * Created by jeon-HP on 2017-05-24.
 */

public class FlyingFileActivity extends Activity {

    public static ArrayList<Activity> actList = new ArrayList<Activity>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flyingfile_activity);
        findViewById(R.id.btn_file_id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //파일아이디 기능 시작.
//                Intent intent = new Intent(FlyingFileActivity.this,FileIDActivity.class);
//                startActivity(intent);
            }
        });
        findViewById(R.id.btn_wifi_direct).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //와이파이 다이렉트 기능 시작.
                Intent intent = new Intent(FlyingFileActivity.this, WifiDirectActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

}
