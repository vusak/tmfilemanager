package jiran.com.flyingfile.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import jiran.com.flyingfile.callback.NetworkChangeListener;
import jiran.com.flyingfile.util.NetworkUtil;


public class NetworkChangeReceiver extends BroadcastReceiver {


    public static boolean isRunning;


    public static int BEFORE_NETWORK_MODE = -1;
    public static String BEFORE_IP = null;
    public static String BEFORE_WIFI_SSID = null;


    public static NetworkChangeListener changeListener = null;

    public static NetworkChangeListener getChangeListener() {
        return changeListener;
    }

    public static void setChangeListener(NetworkChangeListener changeListener) {
        NetworkChangeReceiver.changeListener = changeListener;
    }


    @Override
    public void onReceive(final Context context, Intent intent) {
        // TODO Auto-generated method stub

        if (isRunning)
            return;

        isRunning = true;


        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {

            Log.d("aaa", "CONNECTIVITY_ACTION");

            String strMobileIP = null;
            String strSSID = null;


            ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);


            NetworkInfo mobileNetwork = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            NetworkInfo wifiNetwork = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            if (mobileNetwork != null && mobileNetwork.isConnected()) {
                strMobileIP = NetworkUtil.getInstance().getLocalIpAddress();
                if (BEFORE_IP == null || !BEFORE_IP.equals(strMobileIP)) {
                    BEFORE_NETWORK_MODE = -1;
                }
            } else if (wifiNetwork != null && wifiNetwork.isConnected()) {
                try {
                    strMobileIP = NetworkUtil.getInstance().getNetworkInfo(context).getIpAddress();
                } catch (Exception e) {
                    // TODO: handle exception
                    strMobileIP = "";
                }

                WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                strSSID = wm.getConnectionInfo().getSSID();

                if (BEFORE_WIFI_SSID == null || !BEFORE_WIFI_SSID.equals(strSSID)) {
                    BEFORE_NETWORK_MODE = -1;
                }

            }

            if (
                    mobileNetwork != null &&
                            mobileNetwork.isConnected() &&
                            BEFORE_NETWORK_MODE != NetworkUtil.NETWORK_FLAG_MOBILENETWORK
                    )

            {


                if (changeListener != null)
                    changeListener.onChangeNetwork(NetworkChangeListener.FLAG_MOBILE_NETWORK);

            } else if (
                    wifiNetwork != null &&
                            wifiNetwork.isConnected() &&
                            BEFORE_NETWORK_MODE != NetworkUtil.NETWORK_FLAG_WIFI
                    ) {
                if (changeListener != null)
                    changeListener.onChangeNetwork(NetworkChangeListener.FLAG_WIFI_NETWORK);

            } else if (BEFORE_NETWORK_MODE != NetworkUtil.NETWORK_FLAG_UNCONNECT) {

                try {
                    boolean wfConnected = wifiNetwork.isConnected();
                    boolean wfConnecting = wifiNetwork.isConnectedOrConnecting();

                    boolean mnConnected = mobileNetwork.isConnected();
                    boolean mnConnecting = mobileNetwork.isConnectedOrConnecting();

                    if (wfConnected || wfConnecting || mnConnected || mnConnecting) {

                        isRunning = false;
                        return;
                    }

                } catch (Exception e) {


                }
                //아무 연결 안됨
                if (changeListener != null)
                    changeListener.onChangeNetwork(NetworkChangeListener.FLAG_NONEABLE);
            }


        } else if (intent.getAction().equals("android.net.wifi.WIFI_STATE_CHANGED")) {

            Log.d("aaa", "WIFI_STATE_CHANGED");


            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

            if (wifiManager != null) {
                if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
                } else if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_DISABLED && BEFORE_NETWORK_MODE != NetworkUtil.NETWORK_FLAG_UNCONNECT) {
                } else if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_UNKNOWN) {
                }
            } else {
            }

            if (wifiManager != null) {
                try {
                    if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {

                    } else {

                    }
                } catch (Exception e) {

                }

            }

        } else if (intent.getAction().equals("android.net.wifi.p2p.STATE_CHANGED")) {

            Log.d("aaa", "android.net.wifi.p2p.STATE_CHANGED");

        }

        isRunning = false;

    }
}
