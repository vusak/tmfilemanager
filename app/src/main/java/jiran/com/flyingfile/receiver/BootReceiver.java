package jiran.com.flyingfile.receiver;


import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import jiran.com.flyingfile.util.SharedPreferenceUtil;


/**
 * 서비스가 실행중인 상태에서 디바이스 종료시
 * 해당 리시버가 부팅액션브로드캐스트를 받아서 서비스를 재시작함
 *
 * @author Bae
 */
@SuppressLint("DefaultLocale")
public class BootReceiver extends BroadcastReceiver {

    public static boolean IS_SHUTDOWN = false;


    @SuppressLint("NewApi")
    @Override
    public void onReceive(final Context context, Intent intent) {
        // TODO Auto-generated method stub


        Log.d("BRReceiver", intent.getAction());

        if (intent.getAction().equals(Intent.ACTION_SHUTDOWN)) {
            IS_SHUTDOWN = true;
        } else if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)
                && SharedPreferenceUtil.getInstance().getStartWhenDeviceBoot(context)
                ) {
            //만약 bootstate가 true인경우(사용자가 명시적으로 종료 시키지 않은상태인경우
            //그리고 이미 실행이 되어 있다면 밑에꺼 수행하면 안될거 같음

            IS_SHUTDOWN = false;

        }
    }

}
