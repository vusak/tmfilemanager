package jiran.com.flyingfile.receiver;


import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import jiran.com.flyingfile.callback.StorageListener;
import jiran.com.flyingfile.util.PermissionUtil;
import jiran.com.tmfilemanager.MyApplication;


public class StorageReceiver extends BroadcastReceiver {

    public static StorageListener storageListener;

    private boolean USE_LISTENER = true;


    @SuppressLint("NewApi")
    @Override
    public void onReceive(final Context context, Intent intent) {
        // TODO Auto-generated method stub


        if (BootReceiver.IS_SHUTDOWN)
            return;

        final MyApplication app = (MyApplication) context.getApplicationContext();
        String strReceivedPath = app.getReceivedStorage(context);
        if (intent.getAction().equals(Intent.ACTION_MEDIA_UNMOUNTED)) {
            Log.d("StorageReceiver", "ACTION_MEDIA_UNMOUNTED");
            if (storageListener != null) {
                if (USE_LISTENER)
                    storageListener.onUpdateMounted(intent, false);
            } else {

                //설정된 경로에 쓰기권한이 없으면 자동으로 기본경로로 바꿔주자
                if (!PermissionUtil.getInstance().isExistPath(strReceivedPath)) {
                    app.setReceivedStorage(context, app.getDefaultStorage());
                }

                String strCurrentPath = app.getStrCurrentMobilePath();
                if (!PermissionUtil.getInstance().isExistPath(strCurrentPath)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        app.setStrCurrentMobilePath(app.getDefaultStorage());
                    } else {
                        app.setStrCurrentMobilePath(app.getDefaultStorage());

                    }
                }
            }
        } else if (intent.getAction().equals(Intent.ACTION_MEDIA_MOUNTED)) {
            Log.d("StorageReceiver", "ACTION_MEDIA_MOUNTED");
            if (USE_LISTENER) {
                if (storageListener != null)
                    storageListener.onUpdateMounted(intent, true);
            }
        }
    }
}
