package jiran.com.flyingfile.wifidirect.model;

import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.util.Log;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class WifiDirectSocket {

    private static WifiDirectSocket instance = null;
    private static Channel channel = null;
    private ServerSocket srvSocket = null;
    private Socket socket = null;

    public static WifiDirectSocket getInstance() {
        if (instance == null)
            instance = new WifiDirectSocket();
        return instance;
    }

    public static Channel getChannel() {
        return channel;
    }

    public static void setChannel(Channel channel) {
        WifiDirectSocket.channel = channel;
    }

    public ServerSocket getSrvSocket() {
        return srvSocket;
    }

    public void setSrvSocket(ServerSocket srvSocket) {
        this.srvSocket = srvSocket;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public void close() throws IOException {
        Log.d("WIFIDirectSocket", "[close] function");

        if (srvSocket != null) {
            srvSocket.close();
            srvSocket = null;
        }
        if (socket != null) {
            socket.close();
            socket = null;
        }
    }

    /**
     * 와이파이다이렉트로 연결된 소켓이 살아있으면 일반 파일 전송이 되면 안된다.
     * 반대로 일반 파일전송이 실행중일때 와이파이다이렉트로 파일전송도 되면 안된다.
     * 그러기 위한 플래그를 현재 와이파이다이렉트 소켓이 살아있는지 여부로 판단하자
     */
    public boolean isConnecting() {
        if (getSocket() == null) {
            //소켓이 null이면 소켓연결이 안되있는거
            return false;
        }
        if (getSocket().isClosed()) {
            //소켓이 닫혀있으면 소켓 연결이 안되있는거
            return false;
        }

        return true;
    }

}
