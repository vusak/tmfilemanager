package jiran.com.flyingfile.wifidirect.model;

import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.widget.ImageButton;
import android.widget.TextView;

public class WifiDirect {

    public static final int BUFFER = 8192;

    public static final int FLAG_TOTAL_INFORMATION = 100;
    public static final int FLAG_ALREADY_FILE_TRANSFER = 101;

    /**
     * client가 되는녀석이 먼저 connect성공 메세지를 보내고
     * host가 connect성공 메세지를 받으면 화면을 띄우고
     * 파일 송수신 준비 상태 메세지를 보낸다
     * client는 파일 송수신 준비 상태 메세지를 받으면 그때 화면을 띄워주자
     */
    public static final int FLAG_CONNECT_REQUEST = 103;
    public static final int FLAG_CONNECT_RESPONSE = 104;


    public static final int FLAG_TYPE_IS_CLIENT = 110;
    public static final int FLAG_TYPE_IS_HOST = 120;

    public static final int FLAG_TYPE_TEST_REQEUST = 130;
    public static final int FLAG_TYPE_TEST_RESPONSE = 131;

    public static final int FLAG_FILE_INFORMATION = 150;
    public static final int FLAG_FILE_APPEND_REQUEST = 151;
    public static final int FLAG_FILE_APPEND_RESPONSE = 152;
    public static final int FLAG_FILE_JUMP_RESPONSE = 153;

    public static final int FLAG_TYPE_EXIT = 160;

    //	public static final int FLAG_FILE_APPEND_READY = 153;
    public static final int FLAG_FILE_DATA = 250;
    public static final int FLAG_FILE_FINISH = 253;
    public static final int FLAG_FILE_ERROR = 255;

    public static final int FLAG_FILE_CANCEL = 254;
    public static boolean otherCrypt;
    //내 p2p device
    private static WifiP2pDevice p2pDev = null;
    //내 p2p정보
    private static WifiP2pInfo p2pInfo = null;
    private static Channel p2pChannel = null;
    //상대 devicename
    private static String strOtherDeviceName = null;
    //상대 deviceaddress
    private static String strOtherDeviceAddress = null;

    //상대 dev정보
//	private static WifiP2pDevice otherp2pDev = null;
//	public static WifiP2pDevice getOtherp2pDev() {
//		return otherp2pDev;
//	}
//	public static void setOtherp2pDev(WifiP2pDevice otherp2pDev) {
//		WIFIDirect.otherp2pDev = otherp2pDev;
//	}
    private static boolean isWifiDirectConf = false;
    private static ImageButton ib_WifiDirectConf = null;
    private static TextView tv_ThisDeviceName = null;

    public static WifiP2pDevice getP2pDev() {
        return p2pDev;
    }

    public static void setP2pDev(WifiP2pDevice p2pDev) {
        WifiDirect.p2pDev = p2pDev;
    }

    public static WifiP2pInfo getP2pInfo() {
        return p2pInfo;
    }

    public static void setP2pInfo(WifiP2pInfo p2pInfo) {
        WifiDirect.p2pInfo = p2pInfo;
    }

    public static Channel getP2pChannel() {
        return p2pChannel;
    }

    public static void setP2pChannel(Channel p2pChannel) {
        WifiDirect.p2pChannel = p2pChannel;
    }

    public static String getStrOtherDeviceName() {
        return strOtherDeviceName;
    }

    public static void setStrOtherDeviceName(String strOtherDeviceName) {
        WifiDirect.strOtherDeviceName = strOtherDeviceName;
    }

    public static String getStrOtherDeviceAddress() {
        return strOtherDeviceAddress;
    }

    public static void setStrOtherDeviceAddress(String strOtherDeviceAddress) {
        WifiDirect.strOtherDeviceAddress = strOtherDeviceAddress;
    }

    public static boolean isWifiDirectConf() {
        return isWifiDirectConf;
    }

    public static void setWifiDirectConf(boolean isWifiDirectConf) {
        WifiDirect.isWifiDirectConf = isWifiDirectConf;
    }

    public static ImageButton getIb_WifiDirectConf() {
        return ib_WifiDirectConf;
    }

    public static void setIb_WifiDirectConf(ImageButton ib_WifiDirectConf) {
        WifiDirect.ib_WifiDirectConf = ib_WifiDirectConf;
    }

    public static TextView getTv_ThisDeviceName() {
        return tv_ThisDeviceName;
    }

    public static void setTv_ThisDeviceName(TextView tv_ThisDeviceName) {
        WifiDirect.tv_ThisDeviceName = tv_ThisDeviceName;
    }

    public static boolean isOtherCrypt() {
        return otherCrypt;
    }

    public static void setOtherCrypt(boolean otherCrypt) {
        WifiDirect.otherCrypt = otherCrypt;
    }
}
