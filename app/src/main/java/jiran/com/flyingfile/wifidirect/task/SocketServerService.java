package jiran.com.flyingfile.wifidirect.task;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.nio.ByteOrder;

import jiran.com.flyingfile.dialog.FakeDialogActivity;
import jiran.com.flyingfile.fileid.http.TcpUtil;
import jiran.com.flyingfile.util.Common;
import jiran.com.flyingfile.util.SharedPreferenceUtil;
import jiran.com.flyingfile.util.UnitTransfer;
import jiran.com.flyingfile.wifidirect.WifiDirectServiceManager;
import jiran.com.flyingfile.wifidirect.activity.WifiDirectFileExplorerActivityUtil;
import jiran.com.flyingfile.wifidirect.model.WifiDirect;
import jiran.com.flyingfile.wifidirect.model.WifiDirectSocket;


@SuppressLint("NewApi")
public class SocketServerService extends IntentService {

    public static final String ACTION_SERVER = "jiran.com.flyingfile.wifi_direct.FILE_SERVER";
    public static final String EXTRAS_FILE_PATH = "file_path";
    public static final String EXTRAS_GROUP_OWNER_ADDRESS = "go_host";
    public static final String EXTRAS_GROUP_OWNER_PORT = "go_port";
    public static final int SOCKET_PORT = 10101;
    public static final String EXTRAS_HANDLER = "handler";
    private static final int SOCKET_TIMEOUT = 15000;
    public static boolean isRunning = false;
    public Handler handlerServer = new Handler();
    private int nSrvSocketTimeout = 0;

    public SocketServerService(String name) {
        super(name);
        // TODO Auto-generated constructor stub
    }

    public SocketServerService() {
        super("SocketServerService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // TODO Auto-generated method stub


        final Context context = getApplicationContext();

//		int nRecvCnt = 0;
//		int nDesCnt = 0;

        if (intent.getAction().equals(ACTION_SERVER)) {

            if (isRunning)
                return;
            else
                isRunning = true;


            //받기

            Log.d("SocketServerService", "[onHandleIntent] function");

            ServerSocket srvSocket = null;
            try {
                srvSocket = new ServerSocket(SOCKET_PORT);
            } catch (IOException e1) {
                // TODO Auto-generated catch block
            }
            WifiDirectSocket.getInstance().setSrvSocket(srvSocket);


            try {
                srvSocket.setSoTimeout(SOCKET_TIMEOUT);
                nSrvSocketTimeout = SOCKET_TIMEOUT;
            } catch (SocketException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }


            boolean isClose = false;
            while (!isClose) {
                try {

                    Socket client = srvSocket.accept();
                    srvSocket.setSoTimeout(0);
                    nSrvSocketTimeout = 0;

                    boolean isUseFileTransfer = false;
//						if(FileTransfer.isFileTransferRunning())
//							isUseFileTransfer = true;
//						else
//							isUseFileTransfer = false;

                    //내가 파일전송 사용중이다.
                    //이미 파일 전송중임을 알리는 메세지를 상대방에게 알려주자
//						SocketMtoMFileSender.getInstance().sendAlreadyFileTransfer(socket);

                    if (isUseFileTransfer) {
                        //사용자에게 파일 전송 사용중임을 알리자
                        handlerServer.post(new Runnable() {

                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                Intent intent = new Intent(context, FakeDialogActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra("flag", FakeDialogActivity.FLAG_1);
                                context.startActivity(intent);
                            }
                        });
                    }

                    //모든 서비스를 종료하자
//						WIFIDirectServiceManager.getInstance().finishAllService(context);

                    //소켓 커넥트가 들어왔다.
                    //클라이언트인지 확인하자
                    //추가로 상대 devicename도 얻어오자

                    byte[] bType = new byte[1];
                    byte[] bNameLen = new byte[4];

                    BufferedInputStream bis = new BufferedInputStream(client.getInputStream());
						
						/*
						nRecvCnt = 0;
						nDesCnt = bType.length;
						while(nRecvCnt!=nDesCnt){
							nRecvCnt = bis.read(bType, nRecvCnt, bType.length)+nRecvCnt;
						}
						*/
                    boolean bTcpRes = new TcpUtil().recv(bType.length, bis, bType);
                    if (!bTcpRes)
                        throw new IOException();

                    bTcpRes = new TcpUtil().recv(bNameLen.length, bis, bNameLen);
                    if (!bTcpRes)
                        throw new IOException();

                    int nType = bType[0];
                    int nNameLen = UnitTransfer.getInstance().byteArrayToInt(bNameLen, ByteOrder.BIG_ENDIAN);

                    byte[] bName = new byte[nNameLen];

                    bTcpRes = new TcpUtil().recv(bName.length, bis, bName);
                    if (!bTcpRes)
                        throw new IOException();

                    String strName = new String(bName);
                    WifiDirect.setStrOtherDeviceName(strName);

                    //디바이스 주소도 얻어와야함
                    byte[] bAddressLen = new byte[4];
                    byte[] bAddress = null;


                    bTcpRes = new TcpUtil().recv(bAddressLen.length, bis, bAddressLen);
                    if (!bTcpRes)
                        throw new IOException();

                    int nAddressLen = UnitTransfer.getInstance().byteArrayToInt(bAddressLen, ByteOrder.BIG_ENDIAN);
                    bAddress = new byte[nAddressLen];

                    bTcpRes = new TcpUtil().recv(bAddress.length, bis, bAddress);
                    if (!bTcpRes)
                        throw new IOException();

                    String strAddress = new String(bAddress);

                    //파일전송 사용중인지 플래그
                    byte[] bFileTransferUse = new byte[1];
                    bTcpRes = new TcpUtil().recv(bFileTransferUse.length, bis, bFileTransferUse);
                    if (!bTcpRes)
                        throw new IOException();

                    byte[] bVersion = new byte[1];
                    bTcpRes = new TcpUtil().recv(bVersion.length, bis, bVersion);
                    if (!bTcpRes)
                        throw new IOException();

                    Log.d("WifiDirectClient", "[onHandleIntent]server  appversion : " + bVersion[0]);
                    if (bFileTransferUse[0] == 'T') {
                        //상대방이 파일전송 사용중이다.

                        if (!isUseFileTransfer) {
                            //위에서 띄우지 않았으므로 여기서 띄워야함
                            //사용자에게 파일 전송 사용중임을 알리자
                            handlerServer.post(new Runnable() {

                                @Override
                                public void run() {
                                    // TODO Auto-generated method stub
                                    Intent intent = new Intent(context, FakeDialogActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.putExtra("flag", FakeDialogActivity.FLAG_1);
                                    context.startActivity(intent);
                                }
                            });
                        }

                        //모든 서비스를 종료하자
                        WifiDirectServiceManager.getInstance().finishAllService(context);

                    } else if (bFileTransferUse[0] == 'F') {
                        WifiDirect.setStrOtherDeviceAddress(strAddress);

                        if (nType == WifiDirect.FLAG_TYPE_IS_CLIENT) {

                            //클라이언트가 맞다
                            //응답을 보내주고 리시브 서비스를 실행하자
                            WifiDirectFileSender.getInstance().sendTypeIsHost(client, WifiDirect.getP2pDev().deviceName,
                                    WifiDirect.getP2pDev().deviceAddress, isUseFileTransfer,
                                    Common.getInstance().getCurrentVersionCode(context),
                                    SharedPreferenceUtil.getInstance().getCrypt(context), this);

                            Log.d("SocketServerService", "[onHandleIntent] socket accept!!!");

                            if (isUseFileTransfer) {
                                //내가 파일전송 사용중이다.
                                //모든 서비스를 종료하자
                                WifiDirectServiceManager.getInstance().finishAllService(context);

                            } else {
                                //요때 소켓을 세팅해주자
                                WifiDirectSocket.getInstance().setSocket(client);

                                if (WifiDirect.getP2pInfo() != null) {
                                    handlerServer.post(new Runnable() {

                                        @Override
                                        public void run() {
                                            WifiDirectFileExplorerActivityUtil.run(context, WifiDirect.getP2pInfo());
                                        }
                                    });
                                }

                            }
                        } else {
                            Log.d("SocketServerService", "[onHandleIntent] is not client TT");
                        }
                    }
                } catch (Exception e) {
                    WifiDirectServiceManager.getInstance().finishAllService(context);
                    Log.d("SocketServerService", "[onHandleIntent] socket error");
                    isClose = true;

                    if (nSrvSocketTimeout != 0) {
                        Intent fakeDialog = new Intent(context, FakeDialogActivity.class);
                        fakeDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        fakeDialog.putExtra("flag", FakeDialogActivity.FLAG_4);
                        context.startActivity(fakeDialog);
                    }

                }
            }


            isRunning = false;

            //receive서비스만 실행하고 서비스는 종료한다.
            stopSelf();


        }

    }
}
