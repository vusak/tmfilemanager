package jiran.com.flyingfile.wifidirect.model;

import android.annotation.SuppressLint;

import java.io.File;

import jiran.com.flyingfile.util.FileManager;

@SuppressLint("NewApi")
public class DomainFileInfo {
    private String strFileSavePath = null;
    private long nFileSize = 0;
    private long nFilePointer = 0;
    //	private FileOutputStream fos = null;
    private boolean isDirectory = false;
    private File file;

    public DomainFileInfo() {
        super();
    }

    public boolean isDirectory() {
        return isDirectory;
    }

    public void setDirectory(boolean isDirectory) {
        this.isDirectory = isDirectory;
    }

    public String getStrFileSavePath() {
        return strFileSavePath;
    }

    public void setStrFileSavePath(String strFileSavePath) {

		/*
		String temp = strFileSavePath;
		boolean isNeedNormalize = Normalizer.isNormalized(temp, Normalizer.Form.NFD);
		if(isNeedNormalize){
			temp = Normalizer.normalize(temp, Normalizer.Form.NFC);
		}else{
		}
		*/
//		this.strFileSavePath = temp;
        this.strFileSavePath = strFileSavePath;
    }

    public long getnFileSize() {
        return nFileSize;
    }

    public void setnFileSize(long nFileSize) {
        this.nFileSize = nFileSize;
    }

    public String getRenameFileSavePath() {
        int i = 0;
        String strPath = getStrFileSavePath();
        String result = strPath;

        String strSuffix = null;

        while (true) {
//			File file = new File(result);

            File tempObj = new File(strPath);
            String strParentPath = tempObj.getParent();
            result = strParentPath + "/" + FileManager.getInstance().getAbleFileName(tempObj.getName(), strSuffix);

            File file = new File(result);

            //파일이 존재하지만 사이즈가 0인경우는 삭제해주자
            if (file.exists() && file.length() == 0) {
                file.delete();
            }


            if (file.exists()) {

                int nLastIndex = strPath.lastIndexOf('.');
                String strTempBeforePath;
                String strTempAfterPath;
                if (nLastIndex == -1) {
                    strTempBeforePath = strPath;
                    strTempAfterPath = "";
                } else {
                    strTempBeforePath = strPath.substring(0, nLastIndex);
                    strTempAfterPath = strPath.substring(nLastIndex);
                }
                result = strTempBeforePath + "(" + i + ")" + strTempAfterPath;
                strSuffix = "(" + i + ")";
                i++;

            } else {
                break;
            }
        }

        try {
            File tempObj = new File(strPath);
            String strParent = tempObj.getParent();
            result = strParent + "/" + FileManager.getInstance().getAbleFileName(tempObj.getName(), strSuffix);
        } catch (Exception e) {

        }

        return result;
    }

    public long getnFilePointer() {
        return nFilePointer;
    }

    public void setnFilePointer(long nFilePointer) {
        this.nFilePointer = nFilePointer;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
