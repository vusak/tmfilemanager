package jiran.com.flyingfile.wifidirect.model;

public class WifiDirectFileTransferStatus {
    private int nCurrentSendCount = 0;
    private int nTotalSendCount = 0;
    private long nCurrentSendSize = 0;
    private long nTotalSendSize = 0;
    private int nJumpSendCount = 0;
    private String strFileName = null;

    public int getnCurrentSendCount() {
        return nCurrentSendCount;
    }

    public void setnCurrentSendCount(int nCurrentSendCount) {
        this.nCurrentSendCount = nCurrentSendCount;
    }

    public int getnTotalSendCount() {
        return nTotalSendCount;
    }

    public void setnTotalSendCount(int nTotalSendCount) {
        this.nTotalSendCount = nTotalSendCount;
    }

    public long getnCurrentSendSize() {
        return nCurrentSendSize;
    }

    public void setnCurrentSendSize(long nCurrentSendSize) {
        this.nCurrentSendSize = nCurrentSendSize;
    }

    public long getnTotalSendSize() {
        return nTotalSendSize;
    }

    public void setnTotalSendSize(long nTotalSendSize) {
        this.nTotalSendSize = nTotalSendSize;
    }

    public String getStrFileName() {
        return strFileName;
    }

    public void setStrFileName(String strFileName) {
        this.strFileName = strFileName;
    }

    public int getnJumpSendCount() {
        return nJumpSendCount;
    }

    public void setnJumpSendCount(int nJumpSendCount) {
        this.nJumpSendCount = nJumpSendCount;
    }
}