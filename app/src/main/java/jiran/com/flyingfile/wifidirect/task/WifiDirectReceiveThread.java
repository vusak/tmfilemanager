package jiran.com.flyingfile.wifidirect.task;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.p2p.WifiP2pInfo;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;

import jiran.com.flyingfile.custom.Crypt;
import jiran.com.flyingfile.fileid.http.TcpUtil;
import jiran.com.flyingfile.util.DocumentUtil;
import jiran.com.flyingfile.util.SDMemoryChecker;
import jiran.com.flyingfile.util.SharedPreferenceUtil;
import jiran.com.flyingfile.util.UnitTransfer;
import jiran.com.flyingfile.wifidirect.WifiDirectServiceManager;
import jiran.com.flyingfile.wifidirect.activity.WifiDirectFileExplorerActivity;
import jiran.com.flyingfile.wifidirect.listener.WifiDirectFileExplorerHandler;
import jiran.com.flyingfile.wifidirect.listener.WifiDirectReceiveThreadCallback;
import jiran.com.flyingfile.wifidirect.model.DomainFileInfo;
import jiran.com.flyingfile.wifidirect.model.WifiDirect;
import jiran.com.flyingfile.wifidirect.model.WifiDirectFileTransferStatus;
import jiran.com.flyingfile.wifidirect.model.WifiDirectSocket;
import jiran.com.tmfilemanager.MyApplication;


@SuppressLint("NewApi")
public class WifiDirectReceiveThread extends Thread {
    public static final String SOCKET_EXCEPTION_MSG = "-1";
    public static final boolean USE_FOLDER_RENAME = false;
    public static boolean isReceiving = false;
    public static boolean isRunning = false;
    public static boolean isReady = false;
    public static String mStrCurrentFilePath;
    private static WifiP2pInfo beforeInfo = null;
    private static WifiDirectFileExplorerHandler handler = null;
    //폴더를 리네임 시키기 위한 맵
    //파일전송 최초 시작시 초기화 시켜줘야함
    HashMap<String, String> folderRenameMap = null;
    private Context context;
    private Socket socket = null;
    private WifiDirectFileTransferStatus status = null;
    private WifiDirectReceiveThreadCallback callback = null;
    private long logData = 0;

    public WifiDirectReceiveThread(
            final Context context,
            WifiDirectReceiveThreadCallback callback
    ) {
        super();
        this.context = context;
        this.callback = callback;

    }

    public static WifiDirectFileExplorerHandler getHandler() {
        return handler;
    }

    public static void setHandler(WifiDirectFileExplorerHandler handler) {
        WifiDirectReceiveThread.handler = handler;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub


        if (isRunning) {

        } else {
            isRunning = true;
        }

        Log.d("MTOM", "receive handleintent");

        Log.d("MTOM2", "function onHandleIntent");

        Log.d("SocketReceiveService", "[onHandleIntent] function");


        setSocket(WifiDirectSocket.getInstance().getSocket());

        if (socket == null)
            Log.d("SocketReceiveService", "[onHandleIntent] socket is null");
        else
            Log.d("SocketReceiveService", "[onHandleIntent] socket is not null");


        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        try {
            bis = new BufferedInputStream(socket.getInputStream(), WifiDirect.BUFFER);
            bos = new BufferedOutputStream(socket.getOutputStream(), WifiDirect.BUFFER);

            Log.d("MTOM2", "init input output...");

        } catch (Exception e) {
            finishService();

            Log.d("MTOM2", "init input output error");

            return;
        }

        boolean bTcpRes = false;
        int nRes = 1;

        status = null;

        isReady = true;


        int infoNDisplayFileCnt = 0;
        int infoNRealFileCnt = 0;
        long infoNFileTotalSize = 0;
        String infoStrFileName = null;
        DomainFileInfo infoDomainFileInfo = null;
        FileOutputStream fos = null;
        OutputStream os = null;

        while (nRes > 0) {
            byte[] bType = new byte[1];
            try {
                isReceiving = false;

                Log.d("MTOM2", "wait...");

                Log.d("MTOM", "receive wait");

                bTcpRes = new TcpUtil().recv(bType.length, bis, bType);
                if (!bTcpRes)
                    throw new Exception("nRcvLen is zero(0) or under");


                Log.d("MTOM", "receive success");
                isReceiving = true;
            } catch (Exception e) {
                finishService();
                Log.d("SocketReceiveService", "[onHandleIntent] socket error!!");
                Log.d("MTOM", "receive exception");
                return;
            }

            int nType = bType[0];
            if (nType < 0)
                nType += 256;
            Log.d("MTOM2", "readSuccess : " + nType);
            Log.d("SocketReceiveService", "[onHandleIntent] receive Type : " + nType);

            switch (nType) {
                case WifiDirect.FLAG_TYPE_EXIT: {
                    //종료 메세지

                    callback.onExit();

                    break;
                }
                case WifiDirect.FLAG_TOTAL_INFORMATION: {

                    /**
                     * 4 : 화면에 보여줘야 할 갯수
                     * 4 : 실제 갯수
                     * 8 : 전체 사이즈

                     */

                    if (USE_FOLDER_RENAME) {
                        //여기서 초기화 해줘야함
                        if (folderRenameMap != null)
                            folderRenameMap.clear();
                        folderRenameMap = new HashMap<String, String>();
                    }


                    status = new WifiDirectFileTransferStatus();


                    byte[] bCnt = new byte[4];
                    try {
                        bTcpRes = new TcpUtil().recv(bCnt.length, bis, bCnt);
                        if (!bTcpRes)
                            throw new Exception("nRcvLen is zero(0) or under");
                    } catch (Exception e) {
                        finishService();
                        return;
                    }
                    infoNDisplayFileCnt = UnitTransfer.getInstance().byteArrayToInt(bCnt, ByteOrder.BIG_ENDIAN);


                    byte[] bRealCnt = new byte[4];
                    try {
                        bTcpRes = new TcpUtil().recv(bRealCnt.length, bis, bRealCnt);
                        if (!bTcpRes)
                            throw new Exception("nRcvLen is zero(0) or under");
                    } catch (Exception e) {
                        finishService();
                        return;
                    }
                    infoNRealFileCnt = UnitTransfer.getInstance().byteArrayToInt(bRealCnt, ByteOrder.BIG_ENDIAN);

                    byte[] bSize = new byte[8];
                    try {
                        bTcpRes = new TcpUtil().recv(bSize.length, bis, bSize);
                        if (!bTcpRes)
                            throw new Exception("nRcvLen is zero(0) or under");
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        finishService();
                        return;
                    }
                    infoNFileTotalSize = UnitTransfer.getInstance().byteArraytoLong(bSize);

                    String str_UpperSavePath = MyApplication.getInstance().getReceivedStorage(context);
                    //DavidKwon File DownLoad Storage가 외장인지 내장인지 구분 하고.....
                    long availMemory = 0;
                    if (SDMemoryChecker.getInstance().isExternalStorage(context, str_UpperSavePath)) {
                        //외장
                        availMemory = SDMemoryChecker.getInstance().getExternalFreeMemory(context);
                    } else {
                        //내장
                        availMemory = SDMemoryChecker.getInstance().getInternalFreeMemory(context);
                    }

                    //가용 메모리가 부족하면 경고창 띄우자
                    if (infoNFileTotalSize > availMemory) {
                        if (handler != null) {
                            handler.sendMsgNomoreMemoryDialog();
                            break;
                        }
                    }

                    if (handler != null) {
                        handler.sendMsgFileTransferDialogShow(false);
                        status.setnTotalSendCount(infoNDisplayFileCnt);
                        status.setnTotalSendSize(infoNFileTotalSize);
                        handler.sendMsgFileTransferDialogChangeInfo(status.getStrFileName(),
                                status.getnCurrentSendCount(),
                                status.getnTotalSendCount(),
                                status.getnCurrentSendSize(),
                                status.getnTotalSendSize());
                        logData = 0;
                    }


                    break;
                }
                case WifiDirect.FLAG_FILE_APPEND_REQUEST: {
                    //보내는쪽으로 응답이 돌아온거임
                    byte[] data = new byte[9];
                    byte[] bFilePointer = new byte[8];
                    long nFilePointer = 0;

                    try {
                        bTcpRes = new TcpUtil().recv(data.length, bis, data);
                        if (!bTcpRes)
                            throw new Exception();

                        System.arraycopy(data, 0, bFilePointer, 0, 8);
                        nFilePointer = UnitTransfer.getInstance().byteArraytoLong(bFilePointer);
                        if (callback != null)
                            callback.onNeedFileAppend(nFilePointer, data[8]);

                    } catch (Exception e) {
                        errorFileReceive(e.getMessage(), infoNRealFileCnt);
                        exceptionFileTransfer();
                        nRes = -1;
                    }


                    break;
                }
                case WifiDirect.FLAG_FILE_APPEND_RESPONSE: {

                    File tempFile = new File(infoDomainFileInfo.getStrFileSavePath());


                    //응답 대기
                    boolean isAppend = false;
                    byte[] bAppendFilePointer = new byte[8];
                    long nAppendFilePointer = 0;
                    try {
                        bTcpRes = new TcpUtil().recv(bAppendFilePointer.length, bis, bAppendFilePointer);
                        if (!bTcpRes)
                            throw new Exception();

                        nAppendFilePointer = UnitTransfer.getInstance().byteArraytoLong(bAppendFilePointer);

                        if (nAppendFilePointer == 0)
                            isAppend = false;
                        else {
                            isAppend = true;
                            status.setnCurrentSendSize(status.getnCurrentSendSize() + nAppendFilePointer);
                            if (tempFile.length() == nAppendFilePointer) {
                                status.setnJumpSendCount(status.getnJumpSendCount() + 1);
                                handler.sendJumpAdd(status.getnJumpSendCount());
                            }
                        }


                    } catch (Exception e) {
                        errorFileReceive(e.getMessage(), infoNRealFileCnt);
                        exceptionFileTransfer();
                        nRes = -1;
                    }


                    if (handler != null) {
                        status.setStrFileName(tempFile.getName());
                        status.setnCurrentSendCount(status.getnCurrentSendCount() + 1);
                        handler.sendMsgFileTransferDialogChangeInfo(status.getStrFileName(),
                                status.getnCurrentSendCount(),
                                status.getnTotalSendCount(),
                                status.getnCurrentSendSize(),
                                status.getnTotalSendSize());
                    }


                    //이어받기 이면 리네임 하면 안됨.
                    if (!isAppend)
                        infoStrFileName = infoDomainFileInfo.getRenameFileSavePath();
                    else
                        infoStrFileName = infoDomainFileInfo.getStrFileSavePath();

                    //부모 경로를 생성하자
                    File file = new File(infoStrFileName);
                    DocumentUtil.mkdirs(context, file.getParentFile());

                    mStrCurrentFilePath = infoStrFileName;

                    infoDomainFileInfo.setFile(file);
                    if (fos != null) {
                        try {
                            fos.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    try {
                        if (SharedPreferenceUtil.getInstance().getMobileDefaultUri(context) == null) {
                            fos = DocumentUtil.getOutputStream(file, false);
                        } else {
                            os = DocumentUtil.getOutputStream(context, file, false);
                        }
                    } catch (Exception e) {
                        errorFileReceive(e.getMessage(), infoNRealFileCnt);
                        exceptionFileTransfer();
                        nRes = -1;
                    }


                    break;
                }
                case WifiDirect.FLAG_FILE_INFORMATION: {
                    /**
                     * 4 : 파일이름 길이
                     * N : 파일이름(=상대 경로)
                     */


                    //파일이름 길이
                    byte[] bFileNameLen = new byte[4];
                    try {
                        bTcpRes = new TcpUtil().recv(bFileNameLen.length, bis, bFileNameLen);
                        if (!bTcpRes)
                            throw new Exception(SOCKET_EXCEPTION_MSG);
                    } catch (Exception e) {
                        errorFileReceive(e.getMessage(), infoNRealFileCnt);
                        exceptionFileTransfer();
                        nRes = -1;
                    }
                    int nFileNameLen = UnitTransfer.getInstance().byteArrayToInt(bFileNameLen, ByteOrder.BIG_ENDIAN);


                    //파일이름(상대경로)
                    byte[] bFileName = new byte[nFileNameLen];
                    try {
                        bTcpRes = new TcpUtil().recv(bFileName.length, bis, bFileName);
                        if (!bTcpRes)
                            throw new IOException(SOCKET_EXCEPTION_MSG);
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        errorFileReceive(e.getMessage(), infoNRealFileCnt);
                        exceptionFileTransfer();
                        nRes = -1;
                    }
                    infoStrFileName = new String(bFileName);


                    if (USE_FOLDER_RENAME) {
                        //구분자가 있으면 자체가 폴더이거나 폴더 내부의 파일임
                        if (infoStrFileName.indexOf(File.separator) > 0) {
                            String strRelativePath = infoStrFileName;                        //a/
                            String strFolderName = infoStrFileName.substring(0, infoStrFileName.indexOf(File.separator));  //a
                            String strOriginFolderName = strFolderName;        //a
                            String strFolderFullPath = MyApplication.getInstance().getReceivedStorage(context) + "/" + strFolderName;       // /~/a
                            String strOriginFolderFullPath = strFolderFullPath;                                            // /~/a

                            File fileRenameTarget = new File(strFolderFullPath);            // /~/a

                            int nIdx = 0;
                            while (fileRenameTarget.exists()) {
                                if (folderRenameMap.containsKey(strOriginFolderName))
                                    break;

                                String temp = strOriginFolderFullPath + " (" + nIdx + ")";            // /~/a (0)
                                fileRenameTarget = new File(temp);                                                // /~/a (0)
                                nIdx++;
                            }

                            if (!folderRenameMap.containsKey(strOriginFolderName)) {
                                strRelativePath = fileRenameTarget.getName() + strRelativePath.substring(strRelativePath.indexOf(File.separator), strRelativePath.length());                //a (0)/b.jpg
                                if (fileRenameTarget.getAbsolutePath().equals(strOriginFolderFullPath)) {
                                    //리네임 안한 상태다
                                    folderRenameMap.put(strOriginFolderName, null);
                                } else {
                                    folderRenameMap.put(strOriginFolderName, strRelativePath);
                                }
                            }
                        }
                    }


                    if (USE_FOLDER_RENAME) {
                        infoDomainFileInfo = new DomainFileInfo();
                        if (infoStrFileName.indexOf(File.separator) > 0) {
                            String strTestFolderName = infoStrFileName.substring(0, infoStrFileName.indexOf(File.separator));
                            String strNeedRename = null;
                            if ((strNeedRename = folderRenameMap.get(strTestFolderName)) != null) {

                                strNeedRename = strNeedRename.substring(0, strNeedRename.indexOf(File.separator)) + infoStrFileName.subSequence(infoStrFileName.indexOf(File.separator), infoStrFileName.length());
                                infoDomainFileInfo.setStrFileSavePath(MyApplication.getInstance().getReceivedStorage(context) + "/" + strNeedRename);
                            } else {
                                infoDomainFileInfo.setStrFileSavePath(MyApplication.getInstance().getReceivedStorage(context) + "/" + infoStrFileName);
                            }
                        } else {
                            infoDomainFileInfo.setStrFileSavePath(MyApplication.getInstance().getReceivedStorage(context) + "/" + infoStrFileName);
                        }
                    } else {
                        infoDomainFileInfo = new DomainFileInfo();
                        infoDomainFileInfo.setStrFileSavePath(MyApplication.getInstance().getReceivedStorage(context) + "/" + infoStrFileName);
                    }


                    if (infoStrFileName.endsWith(File.separator)) {
                        //폴더이다.


                        File file = new File(infoDomainFileInfo.getStrFileSavePath());

                        //폴더를 생성하자
                        DocumentUtil.mkdirs(context, file);

                        infoDomainFileInfo = null;

//						nFileReceivedCount++;

                    } else {
                        //파일인 경우 이어받기 여부를 보내는쪽에서 결정해야함.
                        //우선 파일 포인터를 보내주고 응답을 대기해야된다.

                        File tempFile = new File(infoDomainFileInfo.getStrFileSavePath());

                        //다이얼로그에 표시
                        if (handler != null) {
                            status.setStrFileName(tempFile.getName());
                            handler.sendMsgFileTransferDialogChangeInfo(status.getStrFileName(),
                                    status.getnCurrentSendCount(),
                                    status.getnTotalSendCount(),
                                    status.getnCurrentSendSize(),
                                    status.getnTotalSendSize());
                        }


                        byte[] bAppendType = new byte[1];
                        bAppendType[0] = (byte) WifiDirect.FLAG_FILE_APPEND_REQUEST;

                        byte[] bAppendFilePointer = null;

                        bAppendFilePointer = UnitTransfer.getInstance().longtoByteArray(tempFile.length());


                        //* 1 : [ 0 : save as, 1 : skip ]
                        byte[] bIsJump = new byte[1];
                        bIsJump[0] = (byte) SharedPreferenceUtil.getInstance().getDuplication(context);
                        try {
                            ByteBuffer buffer = ByteBuffer.allocate(bAppendType.length + bAppendFilePointer.length + bIsJump.length);
                            buffer.put(bAppendType);
                            buffer.put(bAppendFilePointer);
                            buffer.put(bIsJump);
                            bos.write(buffer.array(), 0, buffer.array().length);
                            bos.flush();
                        } catch (Exception e) {
                            errorFileReceive(e.getMessage(), infoNRealFileCnt);
                            exceptionFileTransfer();
                            nRes = -1;
                        }

                    }


                    break;
                }
                case WifiDirect.FLAG_FILE_CANCEL: {
                    errorFileReceive("-1", infoNRealFileCnt);
                    callback.onError(nType, true);
                    break;
                }
                case WifiDirect.FLAG_FILE_DATA: {
                    /**
                     * 버퍼사이즈만큼 파일 데이터가 들어옴
                     */
                    //읽자
                    byte[] bTempBuf = new byte[WifiDirect.BUFFER];

                    try {
                        bTcpRes = new TcpUtil().recv(bTempBuf.length, bis, bTempBuf);
                        if (!bTcpRes)
                            throw new IOException(SOCKET_EXCEPTION_MSG);

                    } catch (IOException e) {
                        // TODO: handle exception
                        errorFileReceive(e.getMessage(), infoNRealFileCnt);
                        exceptionFileTransfer();
                        nRes = -1;
                    }


                    //파일에 쓰자
                    try {
                        Log.i("Receiver isCrypt ", "" + WifiDirect.isOtherCrypt());
                        if (WifiDirect.isOtherCrypt()) {
                            Crypt crypt = new Crypt();
                            bTempBuf = crypt.encrypt(bTempBuf);
                        }

                        if (fos != null) {
                            fos.write(bTempBuf, 0, bTempBuf.length);
                            fos.flush();
                        }
                        if (os != null) {
                            os.write(bTempBuf, 0, bTempBuf.length);
                            os.flush();
                        }

                        logData += bTempBuf.length;

                        //다이얼로그에 표시
                        if (handler != null) {
                            status.setnCurrentSendSize(status.getnCurrentSendSize() + WifiDirect.BUFFER);
                            handler.sendMsgFileTransferDialogChangeInfo(status.getStrFileName(),
                                    status.getnCurrentSendCount(),
                                    status.getnTotalSendCount(),
                                    status.getnCurrentSendSize(),
                                    status.getnTotalSendSize());
                        }

                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        errorFileReceive(e.getMessage(), infoNRealFileCnt);
                        exceptionFileTransfer();
                        nRes = -1;
                    }
                    break;
                }
                case WifiDirect.FLAG_FILE_FINISH: {
                    /**
                     * <마지막 데이터이다>
                     * 4 : 데이터 길이
                     * N : 데이터
                     */

                    //데이터 길이
                    byte[] bDataLen = new byte[4];
                    try {

                        bTcpRes = new TcpUtil().recv(bDataLen.length, bis, bDataLen);
                        if (!bTcpRes)
                            throw new IOException(SOCKET_EXCEPTION_MSG);

                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        errorFileReceive(e.getMessage(), infoNRealFileCnt);
                        exceptionFileTransfer();
                        nRes = -1;
                    }
                    int nDataLen = UnitTransfer.getInstance().byteArrayToInt(bDataLen, ByteOrder.BIG_ENDIAN);


                    //데이터를 읽자
                    byte[] bTempBuf = new byte[nDataLen];
                    try {

                        bTcpRes = new TcpUtil().recv(bTempBuf.length, bis, bTempBuf);
                        if (!bTcpRes)
                            throw new IOException(SOCKET_EXCEPTION_MSG);
                    } catch (IOException e) {
                        // TODO: handle exception
                        errorFileReceive(e.getMessage(), infoNRealFileCnt);
                        exceptionFileTransfer();
                        nRes = -1;
                    }


                    //데이터를 파일에 쓰자
                    try {
                        if (WifiDirect.isOtherCrypt()) {
                            Crypt crypt = new Crypt();
                            bTempBuf = crypt.encrypt(bTempBuf);
                        }
                        if (fos != null) {
                            fos.write(bTempBuf, 0, bTempBuf.length);
                            fos.flush();
                            fos.close();
                        }
                        if (os != null) {
                            os.write(bTempBuf, 0, bTempBuf.length);
                            os.flush();
                            os.close();
                        }


                        //다이얼로그에 표시
                        if (handler != null) {
                            status.setnCurrentSendSize(status.getnCurrentSendSize() + nDataLen);
                            handler.sendMsgFileTransferDialogChangeInfo(status.getStrFileName(),
                                    status.getnCurrentSendCount(),
                                    status.getnTotalSendCount(),
                                    status.getnCurrentSendSize(),
                                    status.getnTotalSendSize());
                        }

                        //미디어 스캔 한번 때려줌
                        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + infoStrFileName)));

                        mStrCurrentFilePath = null;

                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        errorFileReceive(e.getMessage(), infoNRealFileCnt);
                        exceptionFileTransfer();
                        nRes = -1;
                    }


                    if (handler != null) {
                        if (status.getnCurrentSendCount() == status.getnTotalSendCount()) {
                            handler.sendMsgFileTransferFinish(
                                    status.getnCurrentSendCount(),
                                    status.getnTotalSendCount()
                            );
                        }
                    }


                    break;
                }
                case WifiDirect.FLAG_FILE_ERROR: {
                    errorFileReceive(null, infoNRealFileCnt);
                    exceptionFileTransfer();
                    nRes = -1;
                    break;
                }
                case WifiDirect.FLAG_TYPE_TEST_REQEUST: {
                    byte[] bPacket = new byte[1];
                    bType[0] = (byte) WifiDirect.FLAG_TYPE_TEST_RESPONSE;
                    try {
                        bos.write(bPacket, 0, bPacket.length);
                        bos.flush();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                    }
                    break;
                }

                default: {


                    if (WifiDirectFileExplorerActivity.p2pInfo == null) {
                        WifiDirectFileExplorerActivity.p2pInfo = beforeInfo;
                    }

                    if (!WifiDirectFileExplorerActivity.p2pInfo.isGroupOwner) {
                        Intent serviceIntent = new Intent(context, SocketClientService.class);
                        serviceIntent.setAction(SocketClientService.ACTION_CLIENT);
                        serviceIntent.putExtra(SocketClientService.EXTRAS_FILE_PATH, "");
                        serviceIntent.putExtra(SocketClientService.EXTRAS_GROUP_OWNER_ADDRESS, WifiDirectFileExplorerActivity.p2pInfo.groupOwnerAddress.getHostAddress());


                        try {
                            socket.close();
                        } catch (Exception e) {
                            // TODO Auto-generated catch block

                        }
                        WifiDirectSocket.getInstance().setSocket(null);

                        isReceiving = false;
                        ComponentName name = context.startService(serviceIntent);
                        WifiDirectServiceManager.setServiceClient(name);
                    }

                    finishService();
                    nRes = -1;

                    break;
                }
            }
        }
        //파일사이즈 보내야됨(로그 - 보내도 그만 안보내도 그만)
        finishService();

        isReceiving = false;

        isRunning = false;

        super.run();
    }


    public void finishService() {
        if (handler != null) {
            try {
                handler.sendMsgFileTransferFinish(
                        status.getnCurrentSendCount(),
                        status.getnTotalSendCount()
                );
            } catch (Exception e) {

            }
            isReceiving = false;

            Log.d("MTOM", "finishService");
        }
        isReady = false;
    }


    private void exceptionFileTransfer() {
//		if(nRes == -1)
        {

            if (WifiDirectFileExplorerActivity.p2pInfo == null) {
                WifiDirectFileExplorerActivity.p2pInfo = beforeInfo;
            }

            if (!WifiDirectFileExplorerActivity.p2pInfo.isGroupOwner) {
                Intent serviceIntent = new Intent(context, SocketClientService.class);
                serviceIntent.setAction(SocketClientService.ACTION_CLIENT);
                serviceIntent.putExtra(SocketClientService.EXTRAS_FILE_PATH, "");
                serviceIntent.putExtra(SocketClientService.EXTRAS_GROUP_OWNER_ADDRESS, WifiDirectFileExplorerActivity.p2pInfo.groupOwnerAddress.getHostAddress());


                try {
                    socket.close();
                } catch (Exception e) {
                    // TODO Auto-generated catch block

                }
                WifiDirectSocket.getInstance().setSocket(null);
                ComponentName name = context.startService(serviceIntent);
                WifiDirectServiceManager.setServiceClient(name);
                isReceiving = false;
            }
        }

        if (handler != null)
            handler.sendMsgFileTransferFinish(
                    status.getnCurrentSendCount(),
                    status.getnTotalSendCount()
            );
    }


    private void errorFileReceive(String msg, int nTotalCnt) {
        Log.d("SocketReceiveService", "[errorFileReceive]+" + msg);
        if (msg == null) {
        } else if (msg.equals("-1")) {
            if (mStrCurrentFilePath != null) {
//				File file = new File(mStrCurrentFilePath);
//				file.delete();
//				context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://"+mStrCurrentFilePath)));
            }

        }

        handler.sendMsgSocketWaitDialog();
    }
}
