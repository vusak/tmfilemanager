package jiran.com.flyingfile.wifidirect;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pGroup;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;

import java.io.IOException;

import jiran.com.flyingfile.wifidirect.model.WifiDirect;
import jiran.com.flyingfile.wifidirect.model.WifiDirectSocket;
import jiran.com.flyingfile.wifidirect.task.SocketClientService;
import jiran.com.flyingfile.wifidirect.task.SocketServerService;
import jiran.com.flyingfile.wifidirect.task.WifiDirectReceiveThread;

/**
 * Created by jeon-HP on 2017-05-25.
 */

public class WifiDirectServiceManager {

    private static WifiDirectServiceManager instance = null;
    private static ComponentName serviceClient = null;
    private static ComponentName serviceServer = null;
    private static ComponentName serviceReceiver = null;

    public static WifiDirectServiceManager getInstance() {
        if (instance == null)
            instance = new WifiDirectServiceManager();
        return instance;
    }

    public static void setServiceClient(ComponentName serviceClient) {
        WifiDirectServiceManager.serviceClient = serviceClient;
    }

    public static void setServiceServer(ComponentName serviceServer) {
        WifiDirectServiceManager.serviceServer = serviceServer;
    }

    public void finishAllService(Context context) {

        Log.d("FileExplorerActivity", "[onBackPressed] function");
        try {
            WifiDirectSocket.getInstance().close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Intent serviceClient = new Intent(context, SocketClientService.class);
        Intent serviceServer = new Intent(context, SocketServerService.class);

        context.stopService(serviceClient);
        context.stopService(serviceServer);

        SocketClientService.isRunning = false;
        SocketServerService.isRunning = false;
        WifiDirectReceiveThread.isReceiving = false;


        final WifiP2pManager mWifiDirectManager = (WifiP2pManager) context.getSystemService(Context.WIFI_P2P_SERVICE);


        if (mWifiDirectManager != null && WifiDirect.getP2pChannel() != null) {
            mWifiDirectManager.requestGroupInfo(WifiDirect.getP2pChannel(), new WifiP2pManager.GroupInfoListener() {
                @Override
                public void onGroupInfoAvailable(WifiP2pGroup group) {
                    if (group != null && mWifiDirectManager != null && WifiDirect.getP2pChannel() != null
                            ) {
                        mWifiDirectManager.removeGroup(WifiDirect.getP2pChannel(), new WifiP2pManager.ActionListener() {

                            @Override
                            public void onSuccess() {
                                Log.d("aa", "aa");
                            }

                            @Override
                            public void onFailure(int reason) {
                                Log.d("aa", "aa");
                            }
                        });
                    }
                }
            });
        }
    }

}
