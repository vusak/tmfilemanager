package jiran.com.flyingfile.wifidirect.model;

import java.io.BufferedOutputStream;

import jiran.com.flyingfile.custom.FileItemStringList;


public class WifiDirectFileSendDomain {
    //	private List<String> list_FileList = null;
    private FileItemStringList<String> list_FileList = null;
    private int index = 0;
    private BufferedOutputStream bos = null;
    private long filePointer = 0;
    private boolean isCancel = false;


    public WifiDirectFileSendDomain() {
        super();
    }

    public boolean isCancel() {
        return isCancel;
    }

    public void setCancel(boolean isCancel) {
        this.isCancel = isCancel;
    }

    public long getFilePointer() {
        return filePointer;
    }

    public void setFilePointer(long filePointer) {
        this.filePointer = filePointer;
    }

    public BufferedOutputStream getBos() {
        return bos;
    }

    public void setBos(BufferedOutputStream bos) {
        this.bos = bos;
    }

    public FileItemStringList<String> getList_FileList() {
        return list_FileList;
    }

    public void setList_FileList(FileItemStringList<String> list_FileList) {
        this.list_FileList = list_FileList;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
