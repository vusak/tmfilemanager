package jiran.com.flyingfile.wifidirect.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import jiran.com.flyingfile.wifidirect.model.WifiDirect;
import jiran.com.tmfilemanager.R;


@SuppressLint("InlinedApi")
public class WifiDirectActivityDeviceAdapter extends ArrayAdapter<WifiP2pDevice> {

    private int mResource = 0;
    private Context mContext = null;

    //1.0.2버전 추가
    private WifiP2pManager manager;

    public WifiDirectActivityDeviceAdapter(Context context, int resource) {
        super(context, resource);
        // TODO Auto-generated constructor stub
        this.mResource = resource;
        this.mContext = context;
        this.manager = (WifiP2pManager) context.getSystemService(Context.WIFI_P2P_SERVICE);
    }

    @SuppressLint("NewApi")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View view = convertView;
        if (view == null)
            view = View.inflate(mContext, mResource, null);

        //요거 가지고 공구리 하면 될듯<디바이스 아이템 구조체>
        WifiP2pDevice item = getItem(position);


        TextView tv_Name = (TextView) view.findViewById(R.id.TextView_Peer);
        tv_Name.setText(item.deviceName);

        TextView tv_Status = (TextView) view.findViewById(R.id.TextView_Status);
        TextView btn_CancelConnect = (TextView) view.findViewById(R.id.Button_CancelConnect);

        btn_CancelConnect.setVisibility(View.VISIBLE);
        switch (item.status) {
            case WifiP2pDevice.AVAILABLE: {
                tv_Status.setText(getContext().getString(R.string.MtoM_Available));
                btn_CancelConnect.setVisibility(View.GONE);
                break;
            }
            case WifiP2pDevice.CONNECTED: {
                tv_Status.setText(getContext().getString(R.string.MtoM_Connected));
                break;
            }
            case WifiP2pDevice.FAILED: {
                tv_Status.setText(getContext().getString(R.string.MtoM_Failed));
                //tv_Status.setText(getContext().getString(R.string.MtoM_Available));
                btn_CancelConnect.setVisibility(View.GONE);
                break;
            }
            case WifiP2pDevice.INVITED: {
                tv_Status.setText(getContext().getString(R.string.MtoM_Invited));
                break;
            }
            case WifiP2pDevice.UNAVAILABLE: {
                tv_Status.setText(getContext().getString(R.string.MtoM_Unavailable));
                break;
            }

            default:
                break;
        }

        btn_CancelConnect.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


//				WIFIDirectServiceManager.getInstance().finishAllService(getContext());

                //1.0.2버전 추가
                manager.cancelConnect(WifiDirect.getP2pChannel(), new ActionListener() {

                    @Override
                    public void onSuccess() {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onFailure(int reason) {
                        // TODO Auto-generated method stub

                    }
                });

            }
        });


        return view;
    }

}
