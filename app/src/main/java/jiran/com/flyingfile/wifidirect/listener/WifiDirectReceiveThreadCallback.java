package jiran.com.flyingfile.wifidirect.listener;


import jiran.com.flyingfile.wifidirect.model.WifiDirectFileSendDomain;
import jiran.com.flyingfile.wifidirect.model.WifiDirectFileTransferStatus;

public interface WifiDirectReceiveThreadCallback {
    public void onEvent(
            int nCurrentType,
            int nNextType,
            WifiDirectFileSendDomain param,
            WifiDirectFileExplorerHandler handler,
            WifiDirectFileTransferStatus status
    );

    public void onError(
            int nType,
            boolean isCancel
    );

    public void onNeedFileAppend(long nFilePointer, int isJump);

    public void onExit();
}
