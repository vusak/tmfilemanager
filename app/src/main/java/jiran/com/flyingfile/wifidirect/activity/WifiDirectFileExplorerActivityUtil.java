package jiran.com.flyingfile.wifidirect.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pInfo;

import java.util.List;

import jiran.com.flyingfile.FlyingFileActivity;

public class WifiDirectFileExplorerActivityUtil {

    public static void run(Context context) {
//		if(!FileExplorerActivity.isRunning)
        {
            Intent intent = new Intent(context, WifiDirectFileExplorerActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            context.startActivity(intent);
            //	finishActivity();
        }

    }

    public static void run(Context context, WifiP2pInfo info) {
//		if(!FileExplorerActivity.isRunning)
        {
            Intent intent = new Intent(context, WifiDirectFileExplorerActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.putExtra("info", info);
            context.startActivity(intent);
            //	finishActivity();
        }
    }

    //단말기 선택 액티비티 종료 함.
    public static void finishActivity() {
        List<Activity> alAct = FlyingFileActivity.actList;
        for (int i = 0; i < alAct.size(); i++) {
            Activity activity = alAct.get(i);
            String name = activity.getLocalClassName();
            System.out.println(name + "mmM");
            if (alAct.get(i).getLocalClassName().equals("jiran.com.flyingfile.wifidirect.activity.WifiDirectActivity")) {
                alAct.get(i).finish();
                ;
                alAct.remove(i);
                break;
            }
        }
    }

}
