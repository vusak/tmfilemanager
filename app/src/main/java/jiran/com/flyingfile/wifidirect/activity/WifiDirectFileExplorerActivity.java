package jiran.com.flyingfile.wifidirect.activity;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.Socket;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jiran.com.flyingfile.BaseFlyingFileFragmentActivity;
import jiran.com.flyingfile.FlyingFileActivity;
import jiran.com.flyingfile.callback.FileListCheckedChangeCallback;
import jiran.com.flyingfile.callback.FilelistSetCallback;
import jiran.com.flyingfile.callback.StorageListener;
import jiran.com.flyingfile.custom.FileItemStringList;
import jiran.com.flyingfile.dialog.DialogOneButton;
import jiran.com.flyingfile.dialog.DialogOneButtonUtil;
import jiran.com.flyingfile.dialog.DialogTwoButton;
import jiran.com.flyingfile.dialog.DialogTwoButtonUtil;
import jiran.com.flyingfile.dialog.FakeDialogActivity;
import jiran.com.flyingfile.dialog.TwoButtonDialogCancelCallback;
import jiran.com.flyingfile.dialog.TwoButtonDialogCheckboxCallback;
import jiran.com.flyingfile.dialog.TwoButtonDialogOKCallback;
import jiran.com.flyingfile.model.FileItem;
import jiran.com.flyingfile.model.HeaderDomain;
import jiran.com.flyingfile.util.Common;
import jiran.com.flyingfile.util.DocumentUtil;
import jiran.com.flyingfile.util.FileManager;
import jiran.com.flyingfile.util.NetworkPingTest;
import jiran.com.flyingfile.util.PermissionUtil;
import jiran.com.flyingfile.util.SharedPreferenceUtil;
import jiran.com.flyingfile.wifidirect.WifiDirectServiceManager;
import jiran.com.flyingfile.wifidirect.listener.WifiDirectFileExplorerHandler;
import jiran.com.flyingfile.wifidirect.listener.WifiDirectReceiveThreadCallback;
import jiran.com.flyingfile.wifidirect.model.WifiDirect;
import jiran.com.flyingfile.wifidirect.model.WifiDirectFileSendDomain;
import jiran.com.flyingfile.wifidirect.model.WifiDirectFileTransferStatus;
import jiran.com.flyingfile.wifidirect.model.WifiDirectSocket;
import jiran.com.flyingfile.wifidirect.task.SocketClientService;
import jiran.com.flyingfile.wifidirect.task.WifiDirectFileSender;
import jiran.com.flyingfile.wifidirect.task.WifiDirectReceiveThread;
import jiran.com.tmfilemanager.MyApplication;
import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.fagment.FolderFlyingFileFragment;
import jiran.com.tmfilemanager.fagment.FolderFragment;

interface OnChangePathListener {
    public void onChangePath(String strPath);
}

@SuppressLint({"NewApi", "HandlerLeak"})
public class WifiDirectFileExplorerActivity extends BaseFlyingFileFragmentActivity
        implements
        OnClickListener,
        OnScrollListener,
        FileListCheckedChangeCallback,
        OnChangePathListener,
        StorageListener,
        WifiDirectReceiveThreadCallback,
        FilelistSetCallback {


    //이미지로더 관련
    protected static final String STATE_PAUSE_ON_SCROLL = "STATE_PAUSE_ON_SCROLL";
    protected static final String STATE_PAUSE_ON_FLING = "STATE_PAUSE_ON_FLING";
    // p2p정보
    public static WifiP2pInfo p2pInfo;
    public static boolean isRunning = false;
    public static boolean mSearchMode = false;
    //쓰레드가 동작중임을 알리는 플래그
    public static boolean isFileSendRunning = false;
    public Toolbar toolbar;
    public TextView toolbar_title;
    public ImageView toolbar_title_arrow;
    protected boolean pauseOnScroll = false;

    // private static boolean isCanFinish = false;
    protected boolean pauseOnFling = true;
    WifiDirectReceiveThread receiveThread = null;
    LinearLayout toolbar_title_layout, top_Storage_item_menu_layout, main_title_dimd_layout, main_dimd_layout;
    FrameLayout search_layout;
    EditText search_edit;
    Button search_edit_clear_btn;
    AlertDialog.Builder builder;
    AlertDialog alertDialog;
    boolean isFileAppendCheck = false;
    boolean isForceFileAppend = false;
    int getIsJump = SharedPreferenceUtil.EDuplication.RENAME.ordinal();
    // UI핸들러
    private WifiDirectFileExplorerHandler handler;
    // 전송버튼
    private Button btn_Send;
    //삭제 버튼
    private ImageView btn_Delete;
    // 닫기버튼
    private ImageView btn_Close;
    // 선택버튼
    private ImageView btn_Select;
    // 정렬버튼
    private ImageView btn_Sort;
    // 체크박스 이미지
    private Bitmap bitmap_Check;
    private Bitmap bitmap_Uncheck;
    // 와이파이다이렉트 관련 메세지 받기 위한 브로드캐스트 리시버
    private BroadcastReceiver receiver = null;
    private int nSeekCnt = 10;
    private LinearLayout ll_Menu = null;
    private LinearLayout ll_explorer_search;
    private TextView tv_Path = null;
    //private Button btn_ReceivedPath = null;
    private String strCurrentPath = null;
    private boolean isStopDiscovery = false;
    public Handler handler_WifiDirectSocketSeeker = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Log.d("FileExplorerActivity", "handler_WifiDirectSocketSeeker");

            if (WifiDirectSocket.getInstance().getSocket() == null
                    || WifiDirectSocket.getInstance().getSocket().isClosed()) {

                Log.d("FileExplorerActivity", "handler_WifiDirectSocketSeeker - Socket is Closed");
                WifiDirectFileExplorerActivity.this.finish();
            } else {
                if (nSeekCnt > 0) {
                    sendEmptyMessageDelayed(0, 1000);
                    nSeekCnt--;
                } else {
                    nSeekCnt = 10;
                }
            }
            super.handleMessage(msg);
        }
    };
    private boolean isCreate = false;
    //	private RecyclerView recycle_upload = null;
//	private BaseFileAdapter adapter = null;
    private List<FileItem> contentData = null;
    //소켓 다시 붙을때까지 떠있는 프로그레스 다이얼로그
    private ProgressDialog pWaitingSocketDialog = null;
    private Animation showAniBottom, goneAniBottom;
    private WifiDirectFileSendDomain tempFileTransferParam = null;
    private WifiDirectFileExplorerHandler tempFileTransferHandler = null;
    private WifiDirectFileTransferStatus tempFileTransferStatus = null;
    private DialogOneButton dlg_OneBtn = null;
    private Context context;
    private ProgressDialog dlg_DeleteProgress = null;

    //이미지로더 관련
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        pauseOnScroll = savedInstanceState.getBoolean(STATE_PAUSE_ON_SCROLL, true);
        pauseOnFling = savedInstanceState.getBoolean(STATE_PAUSE_ON_FLING, true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        if (receiveThread == null || !receiveThread.isAlive()) {
            receiveThread = new WifiDirectReceiveThread(this, this);
            receiveThread.start();
        }

        FlyingFileActivity.actList.remove(this);
        context = this;


        MyApplication app = (MyApplication) getApplicationContext();


        if (SharedPreferenceUtil.getInstance().getDefaultPathChange(this)) {

            SharedPreferenceUtil.getInstance().setDefaultPathChange(this, false);

            DialogOneButtonUtil.getInstance().showReceivedPathForceChange(this, app.getDefaultStorage(), null, null);
            SharedPreferenceUtil.getInstance().setMobileDefaultUri(this, null);
        }


        isCreate = true;

        nSeekCnt = 10;
        setContentView(R.layout.flyingfile_activity_file_explorer);

        Intent fakeDialog = new Intent(this, FakeDialogActivity.class);
        fakeDialog.putExtra("flag", FakeDialogActivity.FLAG_2_CLOSE);
        fakeDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(fakeDialog);

        try {
            p2pInfo = (WifiP2pInfo) getIntent().getExtras().get("info");
        } catch (Exception e) {

        }


        // 인스턴스 초기화
        btn_Close = (ImageView) findViewById(R.id.Button_Close);
        btn_Delete = (ImageView) findViewById(R.id.Button_Delete);
        btn_Select = (ImageView) findViewById(R.id.Button_Select);
        btn_Sort = (ImageView) findViewById(R.id.list_bottom_menu_sort);


        contentData = new ArrayList<>();

        //신규 추가
        //현재 경로
        //받은파일함 버튼
        tv_Path = (TextView) findViewById(R.id.TextView_Path);


        btn_Send = (Button) findViewById(R.id.Button_Send);

        // 체크박스 이미지 초기화
        this.bitmap_Check = BitmapFactory.decodeResource(getResources(),
                R.drawable.fileview_check_lime);
        this.bitmap_Uncheck = BitmapFactory.decodeResource(getResources(),
                R.drawable.fileview_uncheck_lime);

        // UI컨트롤 핸들러
        // UI변경은 이 핸들러를 통해서만 한다.
        handler = new WifiDirectFileExplorerHandler(this);
        // 서비스에서 핸들러를 사용할수 있도록 하기위해 파라미터로 넘겨주자
        WifiDirectReceiveThread.setHandler(handler);

        // 이벤트 등록
        btn_Send.setOnClickListener(this);
        btn_Close.setOnClickListener(this);
        btn_Delete.setOnClickListener(this);
        btn_Select.setOnClickListener(this);
        btn_Sort.setOnClickListener(this);


        receiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                String strAction = intent.getAction();
                if (strAction
                        .equals(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION)) {
                } else if (strAction.equals(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION)) {
                    Log.d("FileExplorerActivity", "WIFI_P2P_CONNECTION_CHANGED_ACTION");

                    //연결 변화 <- 여기서 양쪽 소켓 연결 필요
                    NetworkInfo networkInfo = intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);


                    try {
                        if (networkInfo.getType() == 13) {
                            //13이 wifi_p2p이다.
                            if (networkInfo.isConnected()) {
                                Log.d("FileExplorerActivity", "WIFI_P2P_CONNECTION_CHANGED_ACTION - isConnected");
                            } else {
                                Log.d("FileExplorerActivity", "WIFI_P2P_CONNECTION_CHANGED_ACTION - isDisConnected");
                                sendMessagePacket();
                                handler_WifiDirectSocketSeeker.sendEmptyMessage(0);
                            }
                        }
                    } catch (Exception e) {
                        Log.d("FileExplorerActivity", "WIFI_P2P_CONNECTION_CHANGED_ACTION - isParseException");
                        sendMessagePacket();
                        handler_WifiDirectSocketSeeker.sendEmptyMessage(0);
                    }

                }
            }
        };


        // 리시버 등록
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        registerReceiver(receiver, filter);
        init();


        //handler.sendMsgFileCheckCnt(0, adapter.getCount(), adapter.getList_Checked().size());


        super.onCreate(savedInstanceState);
//        FragmentManager fm = getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = fm.beginTransaction();
//        fragmentTransaction.replace(R.id.LinearLayout_Category, FolderFragment);
//        fragmentTransaction.commit();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, new FolderFlyingFileFragment(WifiDirectFileExplorerActivity.this, Utils.getInternalDirectoryPath(), true))
                .commit();
        SharedPreferenceUtil.getInstance().setWiFiDirectDisConnect(context, true);

    }

    public void setTitle(String title) {
        setTitle(title, false);
    }

    public void setTitle(String title, boolean top_menu) {
        toolbar_title.setText(title);
        if (top_menu) {
            toolbar_title_arrow.setVisibility(View.VISIBLE);
        } else {
            toolbar_title_arrow.setVisibility(View.GONE);
        }
    }

    public void listRefresh() {
        Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fm instanceof FolderFlyingFileFragment) {
            ((FolderFlyingFileFragment) fm).listRefresh();
        }
    }

    public void toolbarMenu(int res) {
        toolbar.getMenu().clear();
        toolbar.inflateMenu(res);
    }

    public void setSearchMode() {
        mSearchMode = true;
        search_layout.setVisibility(View.VISIBLE);
        search_edit.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(search_edit, InputMethodManager.SHOW_IMPLICIT);
    }

    public void finishSearchMode() {
        mSearchMode = false;
        search_edit.setText("");
        search_layout.setVisibility(View.GONE);
    }

    private void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar_title_layout = (LinearLayout) toolbar.findViewById(R.id.toolbar_title_layout);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title_arrow = (ImageView) toolbar.findViewById(R.id.toolbar_title_arrow);

        top_Storage_item_menu_layout = (LinearLayout) findViewById(R.id.top_Storage_item_menu_layout);

        main_title_dimd_layout = (LinearLayout) findViewById(R.id.main_title_dimd_layout);
        main_dimd_layout = (LinearLayout) findViewById(R.id.main_dimd_layout);
        main_title_dimd_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dimd_back(null);
            }
        });

        toolbar_title_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = toolbar_title.getText().toString();
                if (toolbar_title_arrow.isShown()) {
                    topMenu(false);
                }
            }
        });

//		toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
//			@Override
//			public boolean onMenuItemClick(MenuItem item) {
//				int id = item.getItemId();
//
//				//noinspection SimplifiableIfStatement
//				if (id == R.id.action_search) {
//					setSearchMode();
//					return true;
//				}else if (id == R.id.action_view_change) {
//					Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
//					if(fm instanceof FolderFlyingFileFragment) {
//						((FolderFlyingFileFragment) fm).changeListGridView();
//					}
//					return true;
//				}
//				return false;
//			}
//		});
        search_layout = (FrameLayout) findViewById(R.id.search_layout);
        search_edit = (EditText) findViewById(R.id.search_edit);
        search_edit_clear_btn = (Button) findViewById(R.id.search_edit_clear_btn);

        //toolbar.setTitleTextColor(Color.WHITE);
        //toolbar.setTitle(getResources().getString(R.string.app_name));
        setTitle(getResources().getString(R.string.app_name));
        //toolbarMenu(R.menu.main);

        search_edit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // Your piece of code on keyboard search click
                    String search = search_edit.getText().toString();
                    if (search != null && search.length() > 0) {
                        Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                        String path = Utils.getInternalDirectoryPath();
                        if (fm instanceof FolderFlyingFileFragment) {
                            if (((FolderFlyingFileFragment) fm).mListType != 1 && ((FolderFlyingFileFragment) fm).mListType != 2)
                                path = ((FolderFlyingFileFragment) fm).mCurrentPath;
                        }
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(search_edit.getWindowToken(), 0);

                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.content_frame, new FolderFlyingFileFragment(WifiDirectFileExplorerActivity.this, path, search))
                                .commit();
                    } else {
                    }
                    return true;
                }
                return false;
            }
        });

        search_edit_clear_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                search_edit.setText("");
            }
        });

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        top_Storage_item_menu_layout.removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(this);
        View top_menu_view0 = inflater.inflate(R.layout.list_top_menu_item, null, false);
        TextView team_name_tv0 = (TextView) top_menu_view0.findViewById(R.id.team_name_tv);
        team_name_tv0.setText(getString(R.string.internaldirectory));
        top_menu_view0.setTag(getString(R.string.internaldirectory));
        top_menu_view0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topMenu(true);
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof FolderFragment && ((FolderFragment) fm).mCurrentPath.contains(Utils.getInternalDirectoryPath())) {
                    //동작하지않음.
                } else {
                    topMenu(true);
                    ((FolderFlyingFileFragment) fm).folderListDisplay(Utils.getInternalDirectoryPath());
                    ((FolderFlyingFileFragment) fm).mStartPath = Utils.getInternalDirectoryPath();
                }
            }
        });
        top_Storage_item_menu_layout.addView(top_menu_view0);

        if (Utils.isSDcardDirectory(true)) {
            View top_menu_view1 = inflater.inflate(R.layout.list_top_menu_item, null, false);
            TextView team_name_tv1 = (TextView) top_menu_view1.findViewById(R.id.team_name_tv);
            team_name_tv1.setText(getString(R.string.sdcard));
            top_menu_view1.setTag(getString(R.string.sdcard));
            top_menu_view1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    topMenu(true);
                    Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                    if (fm instanceof FolderFragment && ((FolderFlyingFileFragment) fm).mCurrentPath.contains(Utils.getSDcardDirectoryPath())) {
                        //동작하지않음.
                    } else {
                        ((FolderFlyingFileFragment) fm).folderListDisplay(Utils.getSDcardDirectoryPath());
                        ((FolderFlyingFileFragment) fm).mStartPath = Utils.getSDcardDirectoryPath();
                        topMenu(true);
                    }
                }
            });
            top_Storage_item_menu_layout.addView(top_menu_view1);
        }
    }

    public void dimd_back(View v) {
        onBackPressed();
    }

    public boolean topMenu(boolean check) {
        if (top_Storage_item_menu_layout.getVisibility() == View.VISIBLE) {
            goneAniBottom = AnimationUtils.loadAnimation(this, R.anim.main_top_info_slide_up);
            top_Storage_item_menu_layout.setAnimation(goneAniBottom);
            top_Storage_item_menu_layout.setVisibility(View.GONE);
            main_title_dimd_layout.setVisibility(View.GONE);
            main_dimd_layout.setVisibility(View.GONE);
            return true;
        } else {
            if (!check) {
                String position = "";
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof FolderFlyingFileFragment) {
                    if (Utils.isSDcardDirectory(true) && ((FolderFlyingFileFragment) fm).mCurrentPath.contains(Utils.getSDcardDirectoryPath())) {
                        position = getResources().getString(R.string.sdcard);
                    } else {
                        position = getResources().getString(R.string.internaldirectory);
                    }
                }
//				else if(fm instanceof StorageFragment){
//					position = ((StorageFragment)fm).mTeam.getTeamName();
//				}
                int count = top_Storage_item_menu_layout.getChildCount();
                if (position != null && position.length() > 0) {
                    for (int i = 0; i < count; i++) {
                        View view = top_Storage_item_menu_layout.getChildAt(i);
                        String name = (String) view.getTag();
                        ImageView team_check_iv = (ImageView) view.findViewById(R.id.team_check_iv);
                        if (position.equals(name)) {
                            team_check_iv.setVisibility(View.VISIBLE);
                        } else {
                            team_check_iv.setVisibility(View.GONE);
                        }
                    }
                }
                showAniBottom = AnimationUtils.loadAnimation(this, R.anim.main_top_info_slide_down);
                top_Storage_item_menu_layout.setVisibility(View.VISIBLE);
                main_title_dimd_layout.setVisibility(View.VISIBLE);
                main_dimd_layout.setVisibility(View.VISIBLE);
                top_Storage_item_menu_layout.setAnimation(showAniBottom);
            }
            return false;
        }
    }

    @Override
    public void finish() {

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    BufferedOutputStream bos = new BufferedOutputStream(WifiDirectSocket.getInstance().getSocket().getOutputStream());

                    WifiDirectFileSender.getInstance().sendExit(bos);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                WifiDirect.setStrOtherDeviceName(null);

                if (!isStopDiscovery) {
                    WifiP2pManager mWifiP2pManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
                    if (Build.VERSION.SDK_INT > 15) {
                        if (mWifiP2pManager != null)
                            try {
                                mWifiP2pManager.stopPeerDiscovery(WifiDirect.getP2pChannel(), new ActionListener() {

                                    @Override
                                    public void onSuccess() {
                                    }

                                    @Override
                                    public void onFailure(int reason) {
                                    }
                                });
                            } catch (Exception e) {
                            }
                    }

                    isStopDiscovery = true;
                }

                WifiDirectServiceManager.getInstance().finishAllService(WifiDirectFileExplorerActivity.this);
                try {

                    if (receiver != null) {
                        unregisterReceiver(receiver);
                        receiver = null;
                    }
                } catch (Exception e) {
                }

                WifiDirectFileExplorerActivity.super.finish();
            }
        }).start();
    }

    @Override
    public void onBackPressed() {

        if (mSearchMode) {
            finishSearchMode();
            return;
        }

        if (topMenu(true)) {
            return;
        }

        Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fm instanceof FolderFlyingFileFragment) {
            if (((FolderFlyingFileFragment) fm).onBackPressed()) {
                return;
            }
        }
        String strName = WifiDirect.getStrOtherDeviceName();
        //dialogWifiDirectExit(strName);

        TwoButtonDialogOKCallback okBtnCallback = new TwoButtonDialogOKCallback() {

            @Override
            public void onOK(DialogTwoButton dialog) {
                WifiDirectFileExplorerActivity.this.finish();

            }
        };
        DialogTwoButtonUtil.getInstance().showFinishWifiDirect(this, strName, okBtnCallback, null, null);

    }

    private List<HeaderDomain> setSection() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-M-d");
        int contentCnt = 0;
        final List<HeaderDomain> alContent = new ArrayList<>();
        String lastDate = "";

        if (contentData.size() > 0) {
            for (int i = 0; i < contentData.size(); i++) {
                FileItem item = contentData.get(0);
                if (item.length() == 0) return null;
                String fileDate;
                if (contentData.get(i).getDateTaken() > 0) {
                    fileDate = format.format(contentData.get(i).getDateTaken());
                } else {
                    fileDate = format.format(contentData.get(i).lastModified());
                }
                if (!fileDate.equals(lastDate)) {
                    if (contentCnt > 0) {
                        alContent.add(new HeaderDomain(contentCnt, false, lastDate));
                        contentCnt = 0;
                    }
                    lastDate = fileDate;
                }
                contentCnt++;
            }
            if (contentCnt > 0) {
                alContent.add(new HeaderDomain(contentCnt, false, lastDate));
            }
        }
        return alContent;
    }

    @Override
    public void onClick(View v) {
        /**
         * 정렬기능 추가(15.11.04)
         */
        Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
//		switch (v.getId()) {
//		case R.id.Button_ReceivedPath:
//		{
//			adapter.clear();
//			break;
//		}
//		default:
//			break;
//		}

        switch (v.getId()) {
            case R.id.Button_Send: {
                // 전송버튼
                ArrayList<String> selectList = new ArrayList<>();
                if (fm instanceof FolderFlyingFileFragment) {
                    selectList = ((FolderFlyingFileFragment) fm).getselectList();
                } else {
                    selectList = new ArrayList<String>();
                }


                if (selectList.size() == 0) {
                    //dialogAlarm(context.getString(R.string.FileView_FileList_Delete_NoSelectedFile));
                    //DialogOneButtonUtil.getInstance().showNoSelectFiles(this, null, null);
                    if (fm instanceof FolderFlyingFileFragment) {
                        ((FolderFlyingFileFragment) fm).noSelectSnackbar();
                    }
                    return;
                }
                //이어받기 체크박스 초기화
                isFileAppendCheck = false;
                if (!WifiDirectReceiveThread.isReceiving) {
                    List<String> selectedList = selectList;
                    FileItemStringList<String> searchedList = new FileItemStringList<String>();
                    for (int i = 0; i < selectedList.size(); i++) {
                        File file = new File(selectedList.get(i));
                        if (file.isDirectory())
                            try {
                                FileManager.getInstance().searchRecursiveAllPath(searchedList, file.getAbsolutePath());
                            } catch (Exception e) {
                            }
                        else
                            searchedList.add(file.getAbsolutePath());
                    }

                    //전체 선택한 파일 사이즈가 0인경우 전송안함(폴더만 선택한 경우)
                    if (searchedList.getTotalsize() == 0) {
                        DialogOneButtonUtil.getInstance().showFileTransferNoSupportEmptyFolder(this, null, null);
                    } else if (searchedList.size() != 0) {
                        WifiDirectFileSendDomain param = new WifiDirectFileSendDomain();
                        param.setList_FileList(searchedList);

                        /**
                         * 파일 전송에 사용한 asynctask가 썸네일 작업을 하는동안 파일 전송이 안된다.
                         * 일반 쓰레드로 돌려야 할거 같다.
                         */
                        //일반 쓰레드로 돌리자
                        if (!isFileSendRunning) {
                            ThreadFileSend sendThread = new ThreadFileSend(0, param, handler, this, null);
                            sendThread.start();
                        }

                        //전송후 초기화!!
                        if (fm instanceof FolderFlyingFileFragment) {
                            ((FolderFlyingFileFragment) fm).finishActionMode();
                        }
                    } else {
                        //DialogOneButtonUtil.getInstance().showNoSelectFiles(this, null, null);
                        if (fm instanceof FolderFlyingFileFragment) {
                            //((FolderFlyingFileFragment)fm).noSelectSnackbar();
                        }
                    }
                }
            }
            break;
            case R.id.Button_Delete:
                if (fm instanceof FolderFlyingFileFragment) {
                    ((FolderFlyingFileFragment) fm).deleteMethod();
                }

//			List<String> selectedList = adapter.getList_Checked();
//
//			boolean isSecondaryPath = false;
//
//			for(int i=0;i<selectedList.size();i++){
//				File file = new File(selectedList.get(i));
//				isSecondaryPath = PermissionUtil.getInstance().isSecondaryStorage(this, file.getAbsolutePath());
//			}
//            boolean isAbleFileWritePath = PermissionUtil.getInstance().isAbleFileWritePath(strCurrentPath);
//            String default_uri = SharedPreferenceUtil.getInstance().getMobileDefaultUri(this);
//            if(default_uri != null) { // 롤리팝 이상 sd카드 일 경우.
//                    TwoButtonDialogOKCallback okBtnCallback = new TwoButtonDialogOKCallback() {
//
//                        @Override
//                        public void onOK(DialogTwoButton dialog) {
//                            // TODO Auto-generated method stub
//                            DeleteTask task = new DeleteTask();
//                            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
//                                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                            }else{
//                                task.execute();
//                            }
//                        }
//                    };
//                    DialogTwoButtonUtil.getInstance().showFileDelete(this, okBtnCallback, null, null);
//            }else if(!isAbleFileWritePath && isSecondaryPath) {
//				DialogOneButtonUtil.getInstance().showFileWriteAccessRestrict(this, null, null);
//			}else if(selectedList.size() != 0){
//				TwoButtonDialogOKCallback okBtnCallback = new TwoButtonDialogOKCallback() {
//
//					@Override
//					public void onOK(DialogTwoButton dialog) {
//						// TODO Auto-generated method stub
//						DeleteTask task = new DeleteTask();
//						if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
//							task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//						}else{
//							task.execute();
//						}
//					}
//				};
//				DialogTwoButtonUtil.getInstance().showFileDelete(this, okBtnCallback, null, null);
//			}else{
//
//				DialogOneButtonUtil.getInstance().showNoSelectFiles(this, null, null);
//			}
                break;
            case R.id.Button_Select:
                if (fm instanceof FolderFlyingFileFragment) {
                    if (FolderFlyingFileFragment.mActionMode) {
                        ((FolderFlyingFileFragment) fm).finishActionMode();
                    } else {
                        ((FolderFlyingFileFragment) fm).setActionMode();
                    }
                }
                break;
            case R.id.list_bottom_menu_sort:
                if (fm instanceof FolderFlyingFileFragment) {
                    if (FolderFlyingFileFragment.mActionMode) {
                        ((FolderFlyingFileFragment) fm).dialogSetSort();
                    } else {
                        ((FolderFlyingFileFragment) fm).dialogSetSort();
                    }
                }
                break;
            case R.id.Button_Close: {
                String strName = WifiDirect.getStrOtherDeviceName();
                // 닫기버튼
                //dialogWifiDirectExit(strName);

                TwoButtonDialogOKCallback okBtnCallback = new TwoButtonDialogOKCallback() {

                    @Override
                    public void onOK(DialogTwoButton dialog) {
                        // TODO Auto-generated method stub
                        WifiDirectFileExplorerActivity.this.finish();
                    }
                };
                DialogTwoButtonUtil.getInstance().showFinishWifiDirect(this, strName, okBtnCallback, null, null);
                break;
            }
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    public void dialogWifiDirectExit(String strName) {
        String strContents = String.format(
                context.getString(R.string.MtoM_CloseDialogMessage), strName);
        String strTitle = context.getString(R.string.dialog_title_disconnect);
        builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.dialog_base, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(strTitle);
        TextView content = (TextView) view.findViewById(R.id.content);
        content.setText(strContents);
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                WifiDirectFileExplorerActivity.this.finish();

                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
            }
        });
        builder.setView(view);
        alertDialog = builder.create();
        alertDialog.show();
    }

    public void dialogAlarm(String str) {
        builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.dialog_base, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(context.getString(R.string.Dialog_Password_Title));
        TextView content = (TextView) view.findViewById(R.id.content);
        content.setText(str);
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setVisibility(View.GONE);

        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
            }
        });
        builder.setView(view);
        alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    synchronized public void onEvent(
            int nCurrentType,
            int nNextType,
            WifiDirectFileSendDomain param,
            WifiDirectFileExplorerHandler handler,
            WifiDirectFileTransferStatus status
    ) {

        WifiDirectFileExplorerActivity.this.isFileSendRunning = false;

        if (nNextType >= 0) {

            this.tempFileTransferParam = param;
            this.tempFileTransferHandler = handler;
            this.tempFileTransferStatus = status;

            ThreadFileSend thread = new ThreadFileSend(nNextType, param, handler, this, status);
            thread.start();
        }
    }

    @Override
    synchronized public void onError(int nType, boolean isCancel) {

        if (isCancel) {
            if (tempFileTransferParam != null) {
                tempFileTransferParam.setCancel(true);
            }

            try {
                if (tempFileTransferStatus != null) {
                    handler.sendMsgFileTransferFinish(
                            tempFileTransferStatus.getnCurrentSendCount(),
                            tempFileTransferStatus.getnTotalSendCount(),
                            true
                    );
                } else {
                    handler.sendMsgFileTransferFinish(
                            -1,
                            -1,
                            true
                    );
                }
            } catch (Exception e) {

            }

            isFileSendRunning = false;
        } else {
            WifiDirectFileExplorerActivity.this.isFileSendRunning = false;

            if (!WifiDirectFileExplorerActivity.p2pInfo.isGroupOwner) {
                Intent serviceIntent = new Intent(WifiDirectFileExplorerActivity.this, SocketClientService.class);
                serviceIntent.setAction(SocketClientService.ACTION_CLIENT);
                serviceIntent.putExtra(SocketClientService.EXTRAS_FILE_PATH, "");
                serviceIntent.putExtra(
                        SocketClientService.EXTRAS_GROUP_OWNER_ADDRESS,
                        WifiDirectFileExplorerActivity.p2pInfo.groupOwnerAddress
                                .getHostAddress());

                try {
                    WifiDirectSocket.getInstance().getSocket().close();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                WifiDirectSocket.getInstance().setSocket(null);

                ComponentName name = startService(serviceIntent);
                WifiDirectServiceManager.setServiceClient(name);

            }
        }
    }

    @Override
    synchronized public void onNeedFileAppend(final long nFilePointer, final int getIsJump) {
        // TODO Auto-generated method stub
        WifiDirectFileExplorerActivity.this.isFileSendRunning = false;


        boolean isAppend = false;
        boolean isJump = false;
        this.getIsJump = getIsJump;
        try {

            if (isForeground()) {
                int index = tempFileTransferParam.getIndex();
                File file = new File(tempFileTransferParam.getList_FileList().get(index));
                if (nFilePointer > 0 && (nFilePointer < file.length())) {
                    isAppend = true;
                } else if (nFilePointer > 0 && (nFilePointer == file.length()) && getIsJump == SharedPreferenceUtil.EDuplication.JUMP.ordinal()) {
                    isAppend = true;
                    isJump = true;
                } else {
                    isAppend = false;
                }
            } else {
                isAppend = false;
            }

        } catch (Exception e) {
            isAppend = false;
        }


        if (isAppend) {
            //전체파일 적용 체크 되있는 건지 확인
            if (isJump) {
                tempFileTransferParam.setFilePointer(nFilePointer);
                new ThreadFileSend(WifiDirect.FLAG_FILE_APPEND_RESPONSE, tempFileTransferParam, tempFileTransferHandler, WifiDirectFileExplorerActivity.this, tempFileTransferStatus).start();
            } else if (isFileAppendCheck) {
                //안물어보고 사용자 마지막 선택대로 수행
                if (isForceFileAppend) {
                    tempFileTransferParam.setFilePointer(nFilePointer);
                } else {
                    tempFileTransferParam.setFilePointer(0);
                }
                ThreadFileSend thread = new ThreadFileSend(WifiDirect.FLAG_FILE_APPEND_RESPONSE, tempFileTransferParam, tempFileTransferHandler, WifiDirectFileExplorerActivity.this, tempFileTransferStatus);
                thread.start();
            } else {
                //사용자한테 물어봐야 함
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        TwoButtonDialogOKCallback okBtnCallback = new TwoButtonDialogOKCallback() {

                            @Override
                            public void onOK(DialogTwoButton dialog) {
                                if (isFileAppendCheck) {
                                    isForceFileAppend = true;
                                }

                                tempFileTransferParam.setFilePointer(nFilePointer);

                                ThreadFileSend thread = new ThreadFileSend(WifiDirect.FLAG_FILE_APPEND_RESPONSE, tempFileTransferParam, tempFileTransferHandler, WifiDirectFileExplorerActivity.this, tempFileTransferStatus);
                                thread.start();
                            }
                        };
                        TwoButtonDialogCancelCallback cancelBtnCallback = new TwoButtonDialogCancelCallback() {

                            @Override
                            public void onCancel(DialogTwoButton dialog) {
                                if (isFileAppendCheck)
                                    isForceFileAppend = false;

                                tempFileTransferParam.setFilePointer(0);

                                ThreadFileSend thread = new ThreadFileSend(WifiDirect.FLAG_FILE_APPEND_RESPONSE, tempFileTransferParam, tempFileTransferHandler, WifiDirectFileExplorerActivity.this, tempFileTransferStatus);
                                thread.start();
                            }
                        };
                        TwoButtonDialogCheckboxCallback checkBoxCallback = new TwoButtonDialogCheckboxCallback() {

                            @Override
                            public void onCheck(DialogTwoButton dialog, boolean isCheck) {
                                isFileAppendCheck = isCheck;
                            }
                        };
                        DialogTwoButtonUtil.getInstance().showFileAppendSend(WifiDirectFileExplorerActivity.this, tempFileTransferStatus.getStrFileName(), okBtnCallback, cancelBtnCallback, checkBoxCallback, null);
                    }
                });
            }
        } else {
            tempFileTransferParam.setFilePointer(0);
            ThreadFileSend thread = new ThreadFileSend(WifiDirect.FLAG_FILE_APPEND_RESPONSE, tempFileTransferParam, tempFileTransferHandler, WifiDirectFileExplorerActivity.this, tempFileTransferStatus);
            thread.start();
        }
    }

    @Override
    public void onExit() {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                WifiDirectFileExplorerActivity.this.finish();
            }
        });
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

    }

    private void sendMessagePacket() {
        final Socket socket = WifiDirectSocket.getInstance().getSocket();
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    if (NetworkPingTest.getInstance().isConnected(
                            socket.getInetAddress())) {

                    } else {
                        Log.d("FileExplorerActivity", "NetworkPingTest is Closed");
                        try {
                            socket.close();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                        WifiDirectSocket.getInstance().setSocket(null);
                    }
                } catch (Exception e) {
                    try {
                        socket.close();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    WifiDirectSocket.getInstance().setSocket(null);
                }
            }
        }).start();
    }

    @Override
    protected void onDestroy() {
        isCreate = false;
        isRunning = false;

        if (receiver != null) {
            unregisterReceiver(receiver);
        }

        //액티비티 닫힐때 이거 다이얼로그 안닫아주면 window leaked에러 발생할수도 있다.
        handler.sendMsgFileTransferDialogDismiss();
        FlyingFileActivity.actList.remove(this);
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        int nCheckPermissionResult = checkPermission();
        if (nCheckPermissionResult == PERMISSION_RESULT_SUCCESS) {
            beforeResume();
        } else {
            WifiDirectFileExplorerActivity.this.finish();
        }
        super.onResume();
    }

    private void beforeResume() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_SHARED.equals(state)) {
            if (dlg_OneBtn == null || !dlg_OneBtn.isShowing()) {
                dlg_OneBtn = DialogOneButtonUtil.getInstance().showUseUSBStorage(this, null, null);

            }
        }

        isRunning = true;

        Log.d("fileexplorerscroll", "SCROLL_STATE_FLING : " + OnScrollListener.SCROLL_STATE_FLING);
        Log.d("fileexplorerscroll", "SCROLL_STATE_IDLE : " + OnScrollListener.SCROLL_STATE_IDLE);
        Log.d("fileexplorerscroll", "SCROLL_STATE_TOUCH_SCROLL : " + OnScrollListener.SCROLL_STATE_TOUCH_SCROLL);

    }

    @Override
    public void onCheckedChange(int nSelectedCount, int nTotalCount, long nSelectedFileSize) {
        handler.sendMsgFileCheckCnt(nSelectedCount, nTotalCount, nSelectedFileSize);
    }

    @Override
    public void onChangePath(String strPath) {

    }

    @Override
    public void onUpdateMounted(Intent intent, boolean isMounted) {
        //외장 마운트 풀리면 이쪽으로 들어옴

        if (!isMounted) {

            MyApplication app = (MyApplication) getApplicationContext();
            String strReceivedPath = app.getReceivedStorage(this);
            if (!PermissionUtil.getInstance().isExistPath(strReceivedPath)) {
                //받은파일함이 외장 SDCard인경우
                app.setReceivedStorage(this, app.getDefaultStorage());
                SharedPreferenceUtil.getInstance().setMobileDefaultUri(this, null);

                if (!isCreate) {
                    SharedPreferenceUtil.getInstance().setDefaultPathChange(this, true);
                } else {
                    DialogOneButtonUtil.getInstance().showReceivedPathForceChange(this, app.getDefaultStorage(), null, null);
                }
            }
//
//			boolean isRoot = Common.getInstance().isRootDirectory(strCurrentPath);
//			if(isRoot){
//				if(nCurrentTab == SharedPreferenceUtil.FLAG_CATEGORYMODE_EXPLORER){
//					setListMode(nCurrentListModeFlag);
//					setCategory(SharedPreferenceUtil.FLAG_CATEGORYMODE_EXPLORER);
//				}
//			}
//			else
//			if(!PermissionUtil.getInstance().isExistPath(strCurrentPath)){
//				app.setStrCurrentMobilePath(app.getDefaultStorage());
//				strCurrentPath = app.getDefaultStorage();
//				if(nCurrentTab == SharedPreferenceUtil.FLAG_CATEGORYMODE_EXPLORER){
//					setListMode(nCurrentListModeFlag);
//					setCategory(SharedPreferenceUtil.FLAG_CATEGORYMODE_EXPLORER);
//				}
//			}
        } else {

            boolean isRoot = Common.getInstance().isRootDirectory(strCurrentPath);

//			if(isRoot){
//				if(nCurrentTab == SharedPreferenceUtil.FLAG_CATEGORYMODE_EXPLORER){
//					setListMode(nCurrentListModeFlag);
//					setCategory(SharedPreferenceUtil.FLAG_CATEGORYMODE_EXPLORER);
//				}
//			}
        }

    }

    @Override
    protected void onPermissionResult(int flag, boolean isSuccess) {
        if (!isSuccess)
            WifiDirectFileExplorerActivity.this.finish();
    }

    @Override
    public void onStartLoadFilelist() {
        File file = new File(MyApplication.getInstance().getReceivedStorage(this));
        if (!file.exists()) {
            DocumentUtil.mkdirs(getBaseContext(), file);
        }
        handler.sendMsgLoadingShow();
    }

    @Override
    public void onFinishLoadFilelist(String strPath, int nTotalFileCount) {

        strCurrentPath = strPath;
        handler.sendMsgLoadingDismiss();

        WifiDirectFileExplorerActivity.this.onChangePath(strPath);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_wifi, menu);
        //return super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            setSearchMode();
            return true;
        } else if (id == R.id.action_view_change) {
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof FolderFlyingFileFragment) {
                ((FolderFlyingFileFragment) fm).changeListGridView();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class ThreadFileSend extends Thread {
        private WifiDirectFileSendDomain param = null;
        private WifiDirectFileExplorerHandler handler = null;
        private WifiDirectFileTransferStatus status = null;
        private WifiDirectReceiveThreadCallback callback = null;

        private int nType = 0;

        public ThreadFileSend(
                int nType,
                WifiDirectFileSendDomain param,
                WifiDirectFileExplorerHandler handler,
                WifiDirectReceiveThreadCallback callback,
                WifiDirectFileTransferStatus status
        ) {
            super();
            this.nType = nType;
            this.param = param;
            this.handler = handler;
            this.callback = callback;
            if (nType == 0)
                this.status = new WifiDirectFileTransferStatus();
            else
                this.status = status;
        }


        @Override
        public synchronized void run() {
            if (WifiDirectFileExplorerActivity.this.isFileSendRunning) {
                return;
            } else {
                WifiDirectFileExplorerActivity.this.isFileSendRunning = true;
            }


            switch (nType) {
                case 0: {
                    //맨 처음 시작
                    if (param.isCancel()) {
                        callback.onError(nType, true);
                        return;
                    }

                    //파일전송 다이얼로그를 띄워줌
                    this.handler.sendMsgFileTransferDialogShow(true);

                    //리스트 인덱스 0으로 초기화
                    param.setIndex(0);

                    //사용자가 선택한 파일리스트 가져옴
                    FileItemStringList list_SelectedFile = param.getList_FileList();


                    //폴더가 아닌 파일 갯수
                    int nFileCntNotFolder = list_SelectedFile.getnTotalCntNotFolder();

                    // 전체 파일 사이즈 구하기
                    long nTotalFileSize = list_SelectedFile.getTotalsize();


                    //현재 파일전송 상태값 저장
                    this.status.setnTotalSendCount(nFileCntNotFolder);
                    this.status.setnTotalSendSize(nTotalFileSize);
                    this.status.setStrFileName("");

                    //다이얼로그 그리기
                    this.handler.sendMsgFileTransferDialogChangeInfo(
                            this.status.getStrFileName(),
                            this.status.getnCurrentSendCount(),
                            this.status.getnTotalSendCount(),
                            this.status.getnCurrentSendSize(),
                            this.status.getnTotalSendSize());


                    try {
                        //아웃풋스트림 초기화
                        BufferedOutputStream bos = new BufferedOutputStream(WifiDirectSocket.getInstance().getSocket().getOutputStream(), WifiDirect.BUFFER);

                        //파라미터에 저장
                        param.setBos(bos);
                    } catch (Exception e) {
                        e.printStackTrace();
                        //예외처리 필요

                        callback.onError(nType, false);

                        return;
                    }

                    callback.onEvent(nType, WifiDirect.FLAG_TOTAL_INFORMATION, param, handler, status);

                    break;
                }
                case WifiDirect.FLAG_TOTAL_INFORMATION: {

                    if (param.isCancel()) {
                        callback.onError(nType, true);
                        return;
                    }

                    //아웃풋스트림
                    BufferedOutputStream bos = param.getBos();

                    //폴더가 아닌 파일 갯수
                    int nFileCntNotFolder = this.status.getnTotalSendCount();

                    //사용자가 선택한 리스트
                    FileItemStringList list_SelectedFile = param.getList_FileList();

                    //전체 선택한 파일 사이즈
                    long nTotalFileSize = this.status.getnTotalSendSize();


                    // 파일 갯수 보내기
                    int nRes = WifiDirectFileSender.getInstance().sendFileTotal(bos, nFileCntNotFolder, list_SelectedFile.size(), nTotalFileSize);

                    if (nRes < 0) {
                        //보내기 에러
                        callback.onError(nType, false);
                        return;
                    }
                    callback.onEvent(nType, WifiDirect.FLAG_FILE_INFORMATION, param, handler, status);

                    break;
                }
                case WifiDirect.FLAG_FILE_INFORMATION: {
                    if (param.isCancel()) {
                        callback.onError(nType, true);
                        return;
                    }

                    //현재 인덱스
                    int index = param.getIndex();

                    //사용자가 선택한 리스트
                    FileItemStringList<String> list_SelectedFile = param.getList_FileList();

                    //인덱스가 리스트 사이즈보다 크거나 같으면 리스트가 끝난거다
                    if (index >= list_SelectedFile.size()) {
                        this.handler.sendMsgFileTransferFinish(
                                this.status.getnCurrentSendCount(),
                                this.status.getnTotalSendCount()
                        );

                        WifiDirectFileExplorerActivity.this.isFileSendRunning = false;
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            setCategory(nCurrentTab);
//                        }
//                    });

                        return;
                    }


                    //파일 경로
                    String strFilePath = list_SelectedFile.get(index);

                    //파일 객체
                    File fileObject = new File(strFilePath);

                    //파일 이름
                    String strFileName = fileObject.getName();

                    //아웃풋스트림
                    BufferedOutputStream bos = param.getBos();

                    //노멀라이즈 필요한지 확인
                    boolean isNeedNormalize = Normalizer.isNormalized(strFileName, Normalizer.Form.NFD);
                    if (isNeedNormalize) {
                        //노멀라이즈가 필요하다면 시키자
                        strFileName = Normalizer.normalize(strFileName, Normalizer.Form.NFC);
                    }
                    if (strFilePath.toLowerCase().endsWith(".apk")) { // 파일전송시 apk 파일의 이름을 앱이름으로 바인딩.
                        PackageInfo packageInfo = context.getPackageManager()
                                .getPackageArchiveInfo(strFilePath, PackageManager.GET_ACTIVITIES);
                        if (packageInfo != null) {
                            ApplicationInfo appInfo = packageInfo.applicationInfo;
                            if (Build.VERSION.SDK_INT >= 8) {
                                appInfo.sourceDir = strFilePath;
                                appInfo.publicSourceDir = strFilePath;
                            }
                            CharSequence appName = packageInfo.applicationInfo.loadLabel(context.getPackageManager());
                            strFileName = appName.toString().replace("\n", "") + ".apk";
                        }
                    }

                    //현재 인덱스에 해당하는 파일 객체가 폴더가 아닌경우에만 화면에 그리자
                    if (!fileObject.isDirectory()) {

                        //현재 상태값 저장
                        this.status.setStrFileName(strFileName);
                        this.status.setnCurrentSendCount(this.status.getnCurrentSendCount() + 1);

                        //다이얼로그에 그리자
                        this.handler.sendMsgFileTransferDialogChangeInfo(
                                this.status.getStrFileName(),
                                this.status.getnCurrentSendCount(),
                                this.status.getnTotalSendCount(),
                                this.status.getnCurrentSendSize(),
                                this.status.getnTotalSendSize());
                    }


                    //상대방은 어차피 받은파일함에 저장하기 때문에 풀패스가 필요없다.
                    //상대경로만 알려주면 됨.
                    Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                    if (fm instanceof FolderFlyingFileFragment) {
                        //폴더전송 때문에 추가됨.
                        //만약 받은파일함에서 폴더를 전송해야 하는경우 그 상대경로를 보내줘야 폴더를 생성함

                        //선택된 파일 경로에서 현재 경로만큼을 자름
                        strFilePath = strFilePath.substring(((FolderFlyingFileFragment) fm).mCurrentPath.length(), strFilePath.length());

                        //상대경로이기 때문에 앞대가리에 "/" 가 붙으면 제거
                        if (strFilePath.length() > 0 && strFilePath.startsWith("/"))
                            strFilePath = strFilePath.substring(1, strFilePath.length());

                        //만약 폴더이면 맨뒤에 "/" 붙임 - 상대에게 폴더임을 알리는 수단
                        if (fileObject.isDirectory())
                            strFilePath += "/";
                    } else {
                        strFilePath = strFileName;
                    }


                    int nRes = WifiDirectFileSender.getInstance().sendFileInfo(bos, strFilePath);
                    if (nRes < 0) {
                        //에러
                        callback.onError(nType, false);
                        return;
                    }


                    if (fileObject.isDirectory()) {
                        //폴더인경우 다시 다음 파일 정보 전송
                        //폴더인경우 다음 파일로 넘어가기 위해 인덱스를 올리자
                        index++;
                        //param에 현재 인덱스 저장
                        param.setIndex(index);

                        callback.onEvent(nType, WifiDirect.FLAG_FILE_INFORMATION, param, handler, status);
                    } else {
                        //일반 파일인경우 응답을 기다림
                        //파일을 받는쪽에서의 응답을 기다려야 하므로 -1로 다음 이벤트를 보내게 하여
                        //응답을 기다리도록 해주자
                        callback.onEvent(nType, -1, param, handler, status);
                    }


                    break;
                }
                case WifiDirect.FLAG_FILE_APPEND_RESPONSE: {
                    if (param.isCancel()) {
                        callback.onError(nType, true);
                        return;
                    }

                    //파일 받는쪽에서 받은 파일 포인터
                    //만약 이게 0이면 이어받기 안하는거임
                    long nFilePointer = param.getFilePointer();

                    //아웃풋스트림 객체
                    BufferedOutputStream bos = param.getBos();


                    //현재 상태값 저장
                    this.status.setnCurrentSendSize(this.status.getnCurrentSendSize() + nFilePointer);

                    //다이얼로그에 그리자
                    this.handler.sendMsgFileTransferDialogChangeInfo(
                            this.status.getStrFileName(),
                            this.status.getnCurrentSendCount(),
                            this.status.getnTotalSendCount(),
                            this.status.getnCurrentSendSize(),
                            this.status.getnTotalSendSize());


                    int nRes = WifiDirectFileSender.getInstance().sendFileAppend(bos, nFilePointer);
                    if (nRes < 0) {
                        //에러
                        callback.onError(nType, false);
                        return;
                    }

                    callback.onEvent(nType, WifiDirect.FLAG_FILE_DATA, param, handler, status);

                    break;
                }
                case WifiDirect.FLAG_FILE_DATA: {
                    if (param.isCancel()) {
                        callback.onError(nType, true);
                        return;
                    }

                    //현재 인덱스
                    int index = param.getIndex();

                    //사용자가 선택한 리스트
                    FileItemStringList<String> list_SelectedFile = param.getList_FileList();

                    //파일 경로
                    String strFilePath = list_SelectedFile.get(index);

                    //파일 객체
                    File fileObj = new File(strFilePath);

                    //파일포인터
                    long nFilePointer = param.getFilePointer();
                    try {
                        //파일스트림
                        FileInputStream fis = new FileInputStream(fileObj);

                        //아웃풋스트림
                        BufferedOutputStream bos = param.getBos();

                        //만약 파일포인터가 0보다 크면 그만큼 스킵 시키자
                        if (nFilePointer > 0) {
                            fis.skip(nFilePointer);

                            if (nFilePointer == fileObj.length() && getIsJump == SharedPreferenceUtil.EDuplication.JUMP.ordinal()) {
                                this.status.setnJumpSendCount(this.status.getnJumpSendCount() + 1);
                                this.handler.sendJumpAdd(this.status.getnJumpSendCount());
                            }
                        }

                        int nRes = WifiDirectFileSender.getInstance().sendFile(param, this.handler, this.status, index, fis, bos, SharedPreferenceUtil.getInstance().getCrypt(context));
                        if (nRes == -1) {
                            //에러
                            throw new Exception();
                        }
                        if (nRes == -2) {
                            //취소
                            callback.onError(nType, true);
                            return;
                        }
                    } catch (Exception e) {
                        callback.onError(nType, false);
                        return;
                    }


                    //1개의 파일 전송이 끝난거임
                    //인덱스를 올려주자
                    index++;
                    param.setIndex(index);

                    callback.onEvent(nType, WifiDirect.FLAG_FILE_INFORMATION, param, handler, status);

                    break;
                }

                default:
                    break;
            }


            super.run();
        }
    }
}