package jiran.com.flyingfile.wifidirect.task;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import jiran.com.flyingfile.custom.Crypt;
import jiran.com.flyingfile.fileid.http.TcpUtil;
import jiran.com.flyingfile.util.UnitTransfer;
import jiran.com.flyingfile.wifidirect.listener.WifiDirectFileExplorerHandler;
import jiran.com.flyingfile.wifidirect.model.WifiDirect;
import jiran.com.flyingfile.wifidirect.model.WifiDirectFileSendDomain;
import jiran.com.flyingfile.wifidirect.model.WifiDirectFileTransferStatus;

public class WifiDirectFileSender {
    private static WifiDirectFileSender instance = null;

    public static WifiDirectFileSender getInstance() {
        if (instance == null)
            instance = new WifiDirectFileSender();
        return instance;
    }


    public boolean sendExit(
            BufferedOutputStream bos
    ) {

        byte[] bType = new byte[1];
        bType[0] = (byte) WifiDirect.FLAG_TYPE_EXIT;

        try {
            bos.write(bType, 0, bType.length);
            bos.flush();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }


    public int sendFileTotal(
//			Socket socket,
            BufferedOutputStream bos,
            int nFileCnt,
            int nRealCnt,
            long nTotalFileSize) {
        //파일 갯수 보내는 함수
        /**
         * 1 : 타입
         * 4 : 화면에 보여줘야 할 갯수
         * 4 : 실제 갯수
         * 8 : 전송할 파일 전체 사이즈
         */

        byte[] bType = new byte[1];
        bType[0] = WifiDirect.FLAG_TOTAL_INFORMATION;
        byte[] bFileCnt = UnitTransfer.getInstance().intToByteArray(nFileCnt, ByteOrder.BIG_ENDIAN);
        byte[] bFileRealCnt = UnitTransfer.getInstance().intToByteArray(nRealCnt, ByteOrder.BIG_ENDIAN);
        byte[] bFileSize = UnitTransfer.getInstance().longtoByteArray(nTotalFileSize);

//		BufferedOutputStream bos = null;

        try {
//			bos = new BufferedOutputStream(socket.getOutputStream());
            bos.write(bType, 0, bType.length);
            bos.write(bFileCnt, 0, bFileCnt.length);
            bos.write(bFileRealCnt, 0, bFileRealCnt.length);
            bos.write(bFileSize, 0, bFileSize.length);
            bos.flush();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            //소켓 에러
            return -1;
        }

        return 1;
    }

    public int sendFileAppend(
            BufferedOutputStream bos,
            long nFilePointer
    ) {

        /**
         * 1 : 타입
         * 8 : 파일포인터
         */

        byte[] bType = new byte[1];
        bType[0] = (byte) WifiDirect.FLAG_FILE_APPEND_RESPONSE;

        byte[] bFilePointer = UnitTransfer.getInstance().longtoByteArray(nFilePointer);

        try {
            bos.write(bType, 0, bType.length);
            bos.write(bFilePointer, 0, bFilePointer.length);
            bos.flush();
        } catch (Exception e) {
            return -1;
        }

        return 1;
    }

    public int sendFileInfo(
//			Socket socket,
            BufferedOutputStream bos,
            String strFilePath) {
        //파일 정보 보내는 함수
        /**
         * 1 : 타입
         * 4 : 파일 이름 길이
         * n : 파일 이름
         */

        //타입
        byte[] bType = new byte[1];
        bType[0] = (byte) WifiDirect.FLAG_FILE_INFORMATION;
        //파일이름
        byte[] bFileName = strFilePath.getBytes();
        //파일이름 길이
        byte[] bFileNameLen = UnitTransfer.getInstance().intToByteArray(bFileName.length, ByteOrder.BIG_ENDIAN);
        //파일 사이즈
//		byte[] bFilePointer = UnitTransfer.getInstance().longtoByteArray(filesize);

//		BufferedOutputStream bos = null;

        try {
//			bos = new BufferedOutputStream(socket.getOutputStream());
            bos.write(bType, 0, bType.length);
            bos.write(bFileNameLen, 0, bFileNameLen.length);
            bos.write(bFileName, 0, bFileName.length);
//			bos.write(bFilePointer, 0, bFilePointer.length);
            bos.flush();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            //소켓 에러
            return -1;
        }

        return 1;
    }

    public void errorSendFile(String strMsg) {
        Log.e("flyingfile", strMsg);
    }


    public void sendAlreadyFileTransfer(Socket socket) {
        try {
            BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
            byte[] bType = new byte[1];
            bType[0] = WifiDirect.FLAG_ALREADY_FILE_TRANSFER;

            bos.write(bType, 0, bType.length);
            bos.flush();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    //	public void sendConnectRequest(Socket socket)
    public void sendConnectRequest(BufferedOutputStream bos) {
        try {
//			BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
            byte[] bType = new byte[1];
            bType[0] = WifiDirect.FLAG_CONNECT_REQUEST;
            byte[] bIntegerType = UnitTransfer.getInstance().intToByteArray(WifiDirect.FLAG_CONNECT_RESPONSE);

            ByteBuffer buf = ByteBuffer.allocate(bType.length + bIntegerType.length);
            buf.put(bType);
            buf.put(bIntegerType);

//			bos.write(bType, 0, bType.length);
            bos.write(buf.array(), 0, buf.array().length);
            bos.flush();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //	public void sendConnectResponse(Socket socket)
    public void sendConnectResponse(BufferedOutputStream bos) {
        try {
//			BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
            byte[] bType = new byte[1];
            bType[0] = WifiDirect.FLAG_CONNECT_RESPONSE;
            byte[] bIntegerType = UnitTransfer.getInstance().intToByteArray(WifiDirect.FLAG_CONNECT_RESPONSE);

            ByteBuffer buf = ByteBuffer.allocate(bType.length + bIntegerType.length);
            buf.put(bType);
            buf.put(bIntegerType);

//			bos.write(bType, 0, bType.length);
            bos.write(buf.array(), 0, buf.array().length);
            bos.flush();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //타입 / 디바이스이름 / 디바이스 주소

    /**
     * @param strMyName          : 디바이스이름
     * @param strMyDeviceAddress : 디바이스 주소
     * @param isUseFileTransfer  : 파일전송중 유무 (T/F)
     * @param versionCode        : 안드로이드 버전코드
     */
    public void sendTypeIsClient(
            Socket socket,
            String strMyName,
            String strMyDeviceAddress,
            boolean isUseFileTransfer,
            int versionCode
    ) {
        try {
            BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
            byte[] bType = new byte[1];
            bType[0] = WifiDirect.FLAG_TYPE_IS_CLIENT;

            byte[] bNameLen = null;
            byte[] bName = strMyName.getBytes();
            bNameLen = UnitTransfer.getInstance().intToByteArray(bName.length, ByteOrder.BIG_ENDIAN);

            byte[] bAddressLen = null;
            byte[] bAddress = strMyDeviceAddress.getBytes();
            bAddressLen = UnitTransfer.getInstance().intToByteArray(bAddress.length, ByteOrder.BIG_ENDIAN);

            byte[] bFileTransferUse = new byte[1];
            if (isUseFileTransfer)
                bFileTransferUse[0] = 'T';
            else
                bFileTransferUse[0] = 'F';

            byte[] bVersionCode = new byte[1];
            bVersionCode[0] = (byte) (versionCode);


            ByteBuffer bBuf = ByteBuffer.allocate(bType.length + bNameLen.length + bName.length + bAddressLen.length + bAddress.length + bFileTransferUse.length + bVersionCode.length);
            bBuf.put(bType);
            bBuf.put(bNameLen);
            bBuf.put(bName);
            bBuf.put(bAddressLen);
            bBuf.put(bAddress);
            bBuf.put(bFileTransferUse);
            bBuf.put(bVersionCode);


            bos.write(bBuf.array(), 0, bBuf.array().length);
            bos.flush();

//			bos.write(bType, 0, bType.length);
//			bos.flush();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public void sendTypeIsHost(
            final Socket socket,
            String strMyName,
            String strDeviceAddress,
            boolean isUseFileTransfer,
            int versionCode,
            final boolean isCrypt,
            final SocketServerService socketServerService) {
        try {
            BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
            BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
            byte[] bType = new byte[1];
            bType[0] = WifiDirect.FLAG_TYPE_IS_HOST;

            byte[] bNameLen = null;
            byte[] bName = strMyName.getBytes();
            bNameLen = UnitTransfer.getInstance().intToByteArray(bName.length, ByteOrder.BIG_ENDIAN);

            byte[] bAddressLen = null;
            byte[] bAddress = strDeviceAddress.getBytes();
            bAddressLen = UnitTransfer.getInstance().intToByteArray(bAddress.length, ByteOrder.BIG_ENDIAN);

            byte[] bFileTransferUse = new byte[1];
            if (isUseFileTransfer)
                bFileTransferUse[0] = 'T';
            else
                bFileTransferUse[0] = 'F';

            byte[] bVersionCode = new byte[1];
            bVersionCode[0] = (byte) (versionCode);

            final byte[] bCrypt = new byte[1];
            bCrypt[0] = (byte) (isCrypt ? 1 : 0);

            ByteBuffer bBuf = ByteBuffer.allocate(bType.length + bNameLen.length + bName.length + bAddressLen.length + bAddress.length + bFileTransferUse.length + bVersionCode.length + bCrypt.length);
            bBuf.put(bType);
            bBuf.put(bNameLen);
            bBuf.put(bName);
            bBuf.put(bAddressLen);
            bBuf.put(bAddress);
            bBuf.put(bFileTransferUse);
            bBuf.put(bVersionCode);
            bBuf.put(bCrypt);


            bos.write(bBuf.array(), 0, bBuf.array().length);
            bos.flush();


            boolean bTcpRes = new TcpUtil().recv(bCrypt.length, bis, bCrypt);
            if (!bTcpRes)
                throw new IOException();

            WifiDirect.setOtherCrypt(bCrypt[0] == 1 ? true : false);

            Log.i("WIFI-Direct send", "WiFiDirect Sender isCrypt : " + isCrypt + "and  " + bCrypt[0]);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public boolean sendCancel(BufferedOutputStream bos) {

        byte[] bType = new byte[1];
        bType[0] = (byte) WifiDirect.FLAG_FILE_CANCEL;

        try {
            bos.write(bType, 0, bType.length);
            bos.flush();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    /**
     * 실제 파일을 전송하는 함수
     *
     * @param fis =	파일 스트림
     * @param bos =	파일 소켓 outputstream
     * @return =	파일 읽기 성공/실패 여부
     * @throws IOException =	파일 소켓으로 send 도중 에러 처리
     */
    public int sendFile(
            WifiDirectFileSendDomain param,
            WifiDirectFileExplorerHandler handler,
            WifiDirectFileTransferStatus status
            , int idx,
            FileInputStream fis,
            BufferedOutputStream bos,
            boolean isCrypt)

    {


        if (fis == null || bos == null) {
            errorSendFile("");
            return -1;
        }
        Log.i("SocketClientSErvice", " isCrypt : " + String.valueOf(isCrypt));
        //임시로 파일 읽은 결과 버퍼
        byte[] bBuf = new byte[WifiDirect.BUFFER];

        //파일 read한 사이즈 저장
        int nFileReadSize = 0;

        long nTempSaveTime = 0;

        /**
         * 만약 파일을 read한 사이즈(nFileReadSize)가 버퍼 사이즈와 같으면 <- 파일을 아직 다 안읽은 상태
         *
         * 1 : 타입
         * 262144 : 파일 데이터
         *
         * 의 형태로 보내고
         * 버퍼사이즈 보다 작으면 <- 파일을 다 읽은 상태 이거나 마지막 파일 데이터
         *
         * 1 : 타입
         * 4 : 데이터사이즈
         * N : 파일 데이터
         *
         * 의 형태로 보낸다.
         */

        //파일을 다 읽을때까지 루프를 돌자
        do {
            if (param.isCancel()) {

                try {
                    WifiDirectFileSender.getInstance().sendCancel(bos);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    fis.close();
                } catch (Exception e) {

                }
                return -2;
            }


            try {
                nTempSaveTime = System.currentTimeMillis();
                nFileReadSize = fis.read(bBuf, 0, WifiDirect.BUFFER);
                Log.d("FileTransfer", "[sendFile] nFileReadSize : " + nFileReadSize + "(" + (System.currentTimeMillis() - nTempSaveTime) + ")ms");
            } catch (IOException e) {
                // 파일 읽기 실패
                return -1;
            }

            if (nFileReadSize < 0)
                nFileReadSize = 0;

            if (nFileReadSize < WifiDirect.BUFFER) {
                //읽은 파일 사이즈가 버퍼사이즈 보다 작다.
                /**
                 * 1 : 타입(253)
                 * 4 : 데이터 사이즈
                 * N : 데이터
                 *
                 */

                //전체 보낼 패킷 사이즈 변수
                int nTempTotalPacketLen = 0;

                //타입 253
                byte[] bType = new byte[1];
                bType[0] = (byte) WifiDirect.FLAG_FILE_FINISH;
                nTempTotalPacketLen += bType.length;

                //파일 읽은 데이터 량(byte단위)
                byte[] bDataLen = UnitTransfer.getInstance().intToByteArray(nFileReadSize);
                nTempTotalPacketLen += bDataLen.length;

                //버퍼에 쌓인 파일 데이터 = bBuf
                nTempTotalPacketLen += nFileReadSize;

                //패킷을 합치자
                ByteBuffer bSendPacket = ByteBuffer.allocate(nTempTotalPacketLen);
                bSendPacket.put(bType);
                bSendPacket.put(bDataLen);
                if (isCrypt) {
                    Crypt crypt = new Crypt();
                    bBuf = crypt.encrypt(bBuf);
                }
                bSendPacket.put(bBuf, 0, nFileReadSize);
                nTempSaveTime = System.currentTimeMillis();


                try {
                    bos.write(bSendPacket.array(), 0, bSendPacket.array().length);
                    bos.flush();
                } catch (Exception e) {
                    errorSendFile(e.getMessage());
                    return -1;
                }

                Log.d("aa", "" + bType.length);

                status.setnCurrentSendSize(status.getnCurrentSendSize() + nFileReadSize);
                handler.sendMsgFileTransferDialogChangeInfo(status.getStrFileName(),
                        status.getnCurrentSendCount(),
                        status.getnTotalSendCount(),
                        status.getnCurrentSendSize(),
                        status.getnTotalSendSize());

                Log.d("FileTransfer", "[sendFile] write : " + nTempTotalPacketLen + "(" + (System.currentTimeMillis() - nTempSaveTime) + ")ms");

                //루프를 종료하자
                break;
            } else if (nFileReadSize == WifiDirect.BUFFER) {
                //읽은 파일 사이즈가 버퍼사이즈와 같다.

                /**
                 * 1 : 타입 250
                 * 262144 : 데이터 사이즈
                 */

                //전체 보낼 패킷 사이즈 변수
                int nTempTotalPacketLen = 0;

                //타입 250
                byte[] bType = new byte[1];
                bType[0] = (byte) WifiDirect.FLAG_FILE_DATA;
                nTempTotalPacketLen += bType.length;

                //읽은 데이터 길이 = bBuf
                nTempTotalPacketLen += bBuf.length;

                //패킷을 합치자
                ByteBuffer bSendPacket = ByteBuffer.allocate(nTempTotalPacketLen);
                bSendPacket.put(bType);
                Log.i("Sender NoCrypt? ", isCrypt + "" + Arrays.toString(bBuf));
                if (isCrypt) {
                    Crypt crypt = new Crypt();
                    bBuf = crypt.encrypt(bBuf);
                    Log.i("Sender Crypt ", "" + Arrays.toString(bBuf));
                }
                bSendPacket.put(bBuf);


                nTempSaveTime = System.currentTimeMillis();
                try {
                    bos.write(bSendPacket.array(), 0, nTempTotalPacketLen);
                    bos.flush();
                } catch (Exception e) {
                    errorSendFile(e.getMessage());
                    return -1;
                }
                Log.d("FileTransfer", "[sendFile] write : " + nTempTotalPacketLen + "(" + (System.currentTimeMillis() - nTempSaveTime) + ")ms");

                status.setnCurrentSendSize(status.getnCurrentSendSize() + WifiDirect.BUFFER);
                handler.sendMsgFileTransferDialogChangeInfo(status.getStrFileName(),
                        status.getnCurrentSendCount(),
                        status.getnTotalSendCount(),
                        status.getnCurrentSendSize(),
                        status.getnTotalSendSize());

                Log.d("aa", "" + nTempTotalPacketLen);


                //버퍼 초기화
                bSendPacket.clear();
                bSendPacket = null;

                //파일이 아직 안끝났으니 루프를 계속 하자
            }

        } while (nFileReadSize != 0);


        return 1;
    }
}
