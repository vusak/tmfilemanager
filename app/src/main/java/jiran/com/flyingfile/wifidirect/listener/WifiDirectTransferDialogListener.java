package jiran.com.flyingfile.wifidirect.listener;

public interface WifiDirectTransferDialogListener {

    public void onCancel();

    public void onClose();
}
