package jiran.com.flyingfile.wifidirect.listener;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedOutputStream;
import java.io.Serializable;

import jiran.com.flyingfile.dialog.WifiDirectOneButtonDialog;
import jiran.com.flyingfile.dialog.WifiDirectTransferDialog;
import jiran.com.flyingfile.util.UnitTransfer;
import jiran.com.flyingfile.wifidirect.activity.WifiDirectFileExplorerActivity;
import jiran.com.flyingfile.wifidirect.model.WifiDirectSocket;
import jiran.com.flyingfile.wifidirect.task.WifiDirectFileSender;
import jiran.com.flyingfile.wifidirect.task.WifiDirectReceiveThread;
import jiran.com.tmfilemanager.R;


public class WifiDirectFileExplorerHandler extends Handler
        implements
        Serializable,
        WifiDirectTransferDialogListener,
        WifiDirectOneButtonDialog.WifiDirectOneButtonDialogListner {

    public static final int FLAG_HANDLER_LOADING_SHOW = 0;
    public static final int FLAG_HANDLER_LOADING_DISMISS = 1;
    public static final int FLAG_HANDLER_FILE_SENDING_PROGRESS_SHOW = 2;
    public static final int FLAG_HANDLER_FILE_SENDING_PROGRESS_DISMISS = 3;
    public static final int FLAG_HANDLER_FILE_RECEIVE_PROGRESS_SHOW = 4;
    public static final int FLAG_HANDLER_FILE_RECEIVE_PROGRESS_DISMISS = 5;
    //	public static final int FLAG_HANDLER_FILETRANSFER_INITIALIZE = 6;
    public static final int FLAG_HANDLER_FILETRANSFER_PROGRESSCHANGE = 7;
    public static final int FLAG_HANDLER_FILETRANSFER_FINISH = 8;
    public static final int FLAG_HANDLER_FILETRANSFER_DIALOG_SHOW = 9;
    public static final int FLAG_HANDLER_FILETRANSFER_DIALOG_DISMISS = 10;
    public static final int FLAG_HANDLER_FILETRANSFER_JUMP = 11;
    public static final int FLAG_HANDLER_FILEDELETE_DIALOG_SHOW = 21;
    public static final int FLAG_HANDLER_FILEDELETE_DIALOG_DISMISS = 22;
    public static final int FLAG_HANDLER_FILESORT_DIALOG_SHOW = 31;
    public static final int FLAG_HANDLER_FILESORT_DIALOG_DISMISS = 32;
    public static final int FLAG_CHECKCNT = 33;
    public static final int FLAG_HANDLER_ALREADY_FILETRANSFER = 34;
    public static final int FLAG_HANDLER_SOCKET_WAIT_DIALOG = 35;
    public static final int FLAG_HANDLER_SOCKET_WAIT_DIALOG_DISMISS = 36;
    public static final int FLAG_HANDLER_NOMORE_MEMORY_DIALOG_DISMISS = 37;
    /**
     *
     */
    private static final long serialVersionUID = -3413624160376471919L;
    private Context context = null;
    private long m_nCurrentProgress;
    private long m_nTotalProgress;
    private Dialog dlg_Progress = null;
    private WifiDirectTransferDialog dlg_FileTransfer = null;
    private WifiDirectOneButtonDialog dlg_NomoreMemory = null;
    /**
     * 메인 액티비티 뷰 컨트롤
     */
    private TextView tv_Cnt = null;


//	private Dialog dlg_FileDelete = null;
//	private Dialog dlg_FileSort = null;


    public WifiDirectFileExplorerHandler(Context context) {
        super();
        this.context = context;
    }

    public TextView getTv_Cnt() {
        return tv_Cnt;
    }

    public void setTv_Cnt(TextView tv_Cnt) {
        this.tv_Cnt = tv_Cnt;
    }

    @SuppressWarnings("unused")
    @Override
    public void handleMessage(Message msg) {
        // TODO Auto-generated method stub
        Bundle data = msg.getData();
        int nFlag = data.getInt("flag");
        switch (nFlag) {
            case FLAG_HANDLER_LOADING_SHOW: {
                if (dlg_Progress != null && dlg_Progress.isShowing()) {
                    dlg_Progress.dismiss();
                    dlg_Progress = null;
                }
                ProgressDialog pDlg = new ProgressDialog(context);
                pDlg.setMessage(context.getString(R.string.FileView_FileList_Load_Mobile));
                pDlg.setCancelable(false);
                dlg_Progress = pDlg;
                dlg_Progress.show();
                break;
            }
            case FLAG_HANDLER_LOADING_DISMISS: {
                if (dlg_Progress != null && dlg_Progress.isShowing()) {
                    dlg_Progress.dismiss();
                    dlg_Progress = null;
                }
                break;
            }

            case FLAG_HANDLER_FILE_SENDING_PROGRESS_SHOW: {
                if (dlg_Progress != null && dlg_Progress.isShowing()) {
                    dlg_Progress.dismiss();
                    dlg_Progress = null;
                }
                ProgressDialog pDlg = new ProgressDialog(context);
                pDlg.setMessage("파일을 전송중입니다...");
                pDlg.setCancelable(false);
                dlg_Progress = pDlg;
                dlg_Progress.show();
                break;
            }
            case FLAG_HANDLER_FILE_SENDING_PROGRESS_DISMISS: {
                if (dlg_Progress != null && dlg_Progress.isShowing()) {
                    dlg_Progress.dismiss();
                    dlg_Progress = null;
                }
                break;
            }

            case FLAG_HANDLER_FILE_RECEIVE_PROGRESS_SHOW: {
                if (dlg_Progress != null && dlg_Progress.isShowing()) {
                    dlg_Progress.dismiss();
                    dlg_Progress = null;
                }
                ProgressDialog pDlg = new ProgressDialog(context);
                pDlg.setMessage("파일을 수신중입니다...");
                pDlg.setCancelable(false);
                dlg_Progress = pDlg;
                dlg_Progress.show();
                break;
            }
            case FLAG_HANDLER_FILE_RECEIVE_PROGRESS_DISMISS: {
                if (dlg_Progress != null && dlg_Progress.isShowing()) {
                    dlg_Progress.dismiss();
                    dlg_Progress = null;
                }
                break;
            }
            case FLAG_HANDLER_FILETRANSFER_PROGRESSCHANGE: {
                //파일전송 다이얼로그 카운트 및 프로그레스 변동

                String strFileName = data.getString("filename");
                int nCurrentFileCount = data.getInt("currentfilecount");
                int nTotalFileCount = data.getInt("totalfilecount");
                long nCurrentFileSize = data.getLong("currentfilesize");
                long nTotalFileSize = data.getLong("totalfilesize");

                m_nCurrentProgress = nCurrentFileSize;
                m_nTotalProgress = nTotalFileSize;

                if (dlg_FileTransfer != null) {
                    dlg_FileTransfer.setCurrentFileInfo(strFileName, nCurrentFileCount, nTotalFileCount, nCurrentFileSize, nTotalFileSize);
                }

                break;
            }
            case FLAG_HANDLER_FILETRANSFER_JUMP: {
                //파일전송 다이얼로그 카운트 및 프로그레스 변동

                int jumpCnt = data.getInt("jumpCnt");


                if (dlg_FileTransfer != null) {
                    dlg_FileTransfer.setJumpCnt(jumpCnt);
                }

                break;
            }
            case FLAG_HANDLER_FILETRANSFER_FINISH: {
                //파일 전송 끝났을때
                //파일전송 다이얼로그 버튼 캡션 - 끝났을때는 닫기버튼
                int nCurrentFileCount = data.getInt("currentfilecount");
                int nTotalFileCount = data.getInt("totalfilecount");
                boolean isCancel = data.getBoolean("iscancel");

                if (dlg_FileTransfer != null) {
                    dlg_FileTransfer.finishFileTransfer(nCurrentFileCount, nTotalFileCount, isCancel);
                }

                //성공 했을때만 표시
                if ((m_nTotalProgress != 0) && (m_nCurrentProgress == m_nTotalProgress)) {

                    m_nCurrentProgress = 0;
                    m_nTotalProgress = 0;
                }

                break;
            }
            case FLAG_HANDLER_FILETRANSFER_DIALOG_SHOW: {

                m_nCurrentProgress = 0;
                m_nTotalProgress = 0;

                //파일전송 파일 전송 다이얼로그 show
                boolean isSend = data.getBoolean("issend");

                //파일전송 다이얼로그 타이틀
                String strTitle = null;
                if (isSend)
                    strTitle = context.getString(R.string.Dialog_FileView_Description_Title);
                else
                    strTitle = context.getString(R.string.Dialog_FileView_Description_Title);

                if (dlg_FileTransfer != null && dlg_FileTransfer.isShowing()) {
                    try {
                        dlg_FileTransfer.dismiss();
                    } catch (Exception e) {

                    }
                    dlg_FileTransfer = null;
                }

//			((FileExplorerActivity)context).dismissProgressDialog();

                //150605 수정
                dlg_FileTransfer = new WifiDirectTransferDialog(context, this);
                //간헐적으로 다이얼로그 show 할때 dispatch에러 뜸
                //확인해볼 필요 있음 context쪽
                dlg_FileTransfer.show();
                dlg_FileTransfer.setSend(isSend);
                dlg_FileTransfer.setTitle(strTitle);

//			((FileExplorerActivity)context).showProgressDialog();

                break;
            }
            case FLAG_HANDLER_FILETRANSFER_DIALOG_DISMISS: {
                m_nCurrentProgress = 0;
                m_nTotalProgress = 0;

                //파일전송 다이얼로그 닫기
                if (dlg_FileTransfer != null && dlg_FileTransfer.isShowing()) {
                    try {
                        dlg_FileTransfer.dismiss();
                    } catch (Exception e) {

                    }
                    dlg_FileTransfer = null;
                }

//			((FileExplorerActivity)context).dismissProgressDialog();

                break;
            }
            case FLAG_HANDLER_FILEDELETE_DIALOG_SHOW: {
                //파일 삭제 다이얼로그 열기
                break;
            }
            case FLAG_HANDLER_FILEDELETE_DIALOG_DISMISS: {
                //파일 삭제 다이얼로그 닫기
                break;
            }
            case FLAG_HANDLER_FILESORT_DIALOG_SHOW: {
                //파일 정렬 다이얼로그 열기
                break;
            }
            case FLAG_HANDLER_FILESORT_DIALOG_DISMISS: {
                //파일 정렬 다이얼로그 닫기
                break;
            }
            case FLAG_CHECKCNT: {
                //갯수 최신화
                if (tv_Cnt != null) {
                    int nCnt = data.getInt("cnt");
                    int nTotal = data.getInt("total");
                    long size = data.getLong("size");
				
				/*
				String strCnt = context.getString(R.string.MtoM_CheckedCount);
				strCnt = String.format(strCnt, nCnt, nTotal);
				tv_Cnt.setText(strCnt);
				*/

                    if (nCnt > 0) {
                        tv_Cnt.setVisibility(View.VISIBLE);
                        String strFormat = context.getString(R.string.FileList_Format_FileCount);
                        String strSize = UnitTransfer.getInstance().memoryConverse2(size);
                        strFormat = String.format(strFormat, nCnt, strSize);
                        tv_Cnt.setText(strFormat);
                    } else {
                        tv_Cnt.setVisibility(View.GONE);
                    }
				
				/*
				String strFormat = context.getString(R.string.FileList_Format_FileCount);
				String strSize = UnitTransfer.getInstance().memoryConverse2(size);
				strFormat = String.format(strFormat, nCnt, strSize);
				*/
                }

                break;
            }

            case FLAG_HANDLER_NOMORE_MEMORY_DIALOG_DISMISS: {
                if (dlg_NomoreMemory != null && dlg_NomoreMemory.isShowing()) {
                    try {
                        dlg_NomoreMemory.dismiss();
                    } catch (Exception e) {

                    }
                    dlg_NomoreMemory = null;
                }

                dlg_NomoreMemory = new WifiDirectOneButtonDialog(context, context.getString(R.string.Dialog_FileView_Description_Title),
                        context.getString(R.string.Cloud_Download_Error_LocalMemoryLow), this);
                dlg_NomoreMemory.show();
                break;
            }

            default:
                break;
        }

        super.handleMessage(msg);
    }

    public void sendMsgLoadingShow() {
        Bundle data = new Bundle();
        data.putInt("flag", FLAG_HANDLER_LOADING_SHOW);
        Message msg = new Message();
        msg.setData(data);
        sendMessage(msg);
    }

    public void sendMsgLoadingDismiss() {
        Bundle data = new Bundle();
        data.putInt("flag", FLAG_HANDLER_LOADING_DISMISS);
        Message msg = new Message();
        msg.setData(data);
        sendMessage(msg);
    }

    public void sendMsgFileSendingShow() {
        Bundle data = new Bundle();
        data.putInt("flag", FLAG_HANDLER_FILE_SENDING_PROGRESS_SHOW);
        Message msg = new Message();
        msg.setData(data);
        sendMessage(msg);
    }

    public void sendMsgFileSendingDismiss() {
        Bundle data = new Bundle();
        data.putInt("flag", FLAG_HANDLER_FILE_SENDING_PROGRESS_DISMISS);
        Message msg = new Message();
        msg.setData(data);
        sendMessage(msg);
    }

    public void sendMsgFileReceivingShow() {
        Bundle data = new Bundle();
        data.putInt("flag", FLAG_HANDLER_FILE_RECEIVE_PROGRESS_SHOW);
        Message msg = new Message();
        msg.setData(data);
        sendMessage(msg);
    }

    public void sendMsgFileReceivingDismiss() {
        Bundle data = new Bundle();
        data.putInt("flag", FLAG_HANDLER_FILE_RECEIVE_PROGRESS_DISMISS);
        Message msg = new Message();
        msg.setData(data);
        sendMessage(msg);
    }

    public void sendMsgFileTransferDialogShow(boolean isSend) {
        Bundle data = new Bundle();
        data.putInt("flag", FLAG_HANDLER_FILETRANSFER_DIALOG_SHOW);
        data.putBoolean("issend", isSend);
        Message msg = new Message();
        msg.setData(data);
        sendMessage(msg);
    }

    public void sendJumpAdd(int jumpCnt) {
        Bundle data = new Bundle();
        data.putInt("flag", FLAG_HANDLER_FILETRANSFER_JUMP);
        data.putInt("jumpCnt", jumpCnt);
        Message msg = new Message();
        msg.setData(data);
        sendMessage(msg);
    }

    public void sendMsgFileTransferDialogDismiss() {
        Bundle data = new Bundle();
        data.putInt("flag", FLAG_HANDLER_FILETRANSFER_DIALOG_DISMISS);
        Message msg = new Message();
        msg.setData(data);
        sendMessage(msg);
    }

    public void sendMsgFileTransferDialogChangeInfo(String strFileName, int nCurrentFileCount, int nTotalFileCount, long nCurrentFileSize, long nTotalFileSize) {
        Bundle data = new Bundle();
        data.putInt("flag", FLAG_HANDLER_FILETRANSFER_PROGRESSCHANGE);
        data.putString("filename", strFileName);
        data.putInt("currentfilecount", nCurrentFileCount);
        data.putInt("totalfilecount", nTotalFileCount);
        data.putLong("currentfilesize", nCurrentFileSize);
        data.putLong("totalfilesize", nTotalFileSize);
        Message msg = new Message();
        msg.setData(data);
        sendMessage(msg);
    }

    public void sendMsgFileTransferFinish(
            int nCurrentFileCnt,
            int nTotalFileCnt,
            boolean isCancel
    ) {
        Bundle data = new Bundle();
        data.putInt("flag", FLAG_HANDLER_FILETRANSFER_FINISH);
        data.putInt("currentfilecount", nCurrentFileCnt);
        data.putInt("totalfilecount", nTotalFileCnt);
        data.putBoolean("iscancel", isCancel);
        Message msg = new Message();
        msg.setData(data);
        sendMessage(msg);
    }

    public void sendMsgFileTransferFinish(
            int nCurrentFileCnt,
            int nTotalFileCnt
    ) {
        Bundle data = new Bundle();
        data.putInt("flag", FLAG_HANDLER_FILETRANSFER_FINISH);
        data.putInt("currentfilecount", nCurrentFileCnt);
        data.putInt("totalfilecount", nTotalFileCnt);
        data.putBoolean("iscancel", false);
        Message msg = new Message();
        msg.setData(data);
        sendMessage(msg);
    }

    public void sendMsgFileCheckCnt(int nCnt, int nTotal, long size) {
//		if(nTotal < 0)
//			nTotal = 0;
        Bundle data = new Bundle();
        data.putInt("flag", FLAG_CHECKCNT);
        data.putInt("cnt", nCnt);
        data.putInt("total", nTotal);
        data.putLong("size", size);
        Message msg = new Message();
        msg.setData(data);
        sendMessage(msg);
    }

    public void sendMsgSocketWaitDialog() {
        Bundle data = new Bundle();
        data.putInt("flag", FLAG_HANDLER_SOCKET_WAIT_DIALOG);
        Message msg = new Message();
        msg.setData(data);
        sendMessage(msg);
    }

    public void sendMsgSocketWaitDialogDismiss() {
        Bundle data = new Bundle();
        data.putInt("flag", FLAG_HANDLER_SOCKET_WAIT_DIALOG_DISMISS);
        Message msg = new Message();
        msg.setData(data);
        sendMessage(msg);
    }

    public void sendMsgNomoreMemoryDialog() {
        Bundle data = new Bundle();
        data.putInt("flag", FLAG_HANDLER_NOMORE_MEMORY_DIALOG_DISMISS);
        Message msg = new Message();
        msg.setData(data);
        sendMessage(msg);
    }


    @Override
    public void onCancel() {
        // TODO Auto-generated method stub


        if (!dlg_FileTransfer.isSend()) {
            try {
                BufferedOutputStream bos = new BufferedOutputStream(WifiDirectSocket.getInstance().getSocket().getOutputStream());
                WifiDirectFileSender.getInstance().sendCancel(bos);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        ((WifiDirectFileExplorerActivity) context).onError(0, true);


        String strTempCurrentPath = WifiDirectReceiveThread.mStrCurrentFilePath;
        if (strTempCurrentPath != null) {
            //전송중인 파일을 삭제하자
//			File file = new File(strTempCurrentPath);
//			file.delete();
//			context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://"+strTempCurrentPath)));
        }
    }

    @Override
    public void onClose() {
        //((WifiDirectFileExplorerActivity)context).refreshRcvFileList();

    }

    @Override
    public void onOneClose() {
        try {
            BufferedOutputStream bos = new BufferedOutputStream(WifiDirectSocket.getInstance().getSocket().getOutputStream());
            WifiDirectFileSender.getInstance().sendCancel(bos);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ((WifiDirectFileExplorerActivity) context).onError(0, true);
        //((WifiDirectFileExplorerActivity)context).refreshRcvFileList();

//		String strTempCurrentPath = SocketReceiveService.mStrCurrentFilePath;
        String strTempCurrentPath = WifiDirectReceiveThread.mStrCurrentFilePath;
        if (strTempCurrentPath != null) {
            //전송중인 파일을 삭제하자
//			File file = new File(strTempCurrentPath);
//			file.delete();
//			context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://"+strTempCurrentPath)));
        }
    }

}
