package jiran.com.flyingfile.wifidirect.task;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import jiran.com.flyingfile.dialog.FakeDialogActivity;
import jiran.com.flyingfile.fileid.http.TcpUtil;
import jiran.com.flyingfile.util.Common;
import jiran.com.flyingfile.util.SharedPreferenceUtil;
import jiran.com.flyingfile.util.UnitTransfer;
import jiran.com.flyingfile.wifidirect.WifiDirectServiceManager;
import jiran.com.flyingfile.wifidirect.activity.WifiDirectFileExplorerActivityUtil;
import jiran.com.flyingfile.wifidirect.model.WifiDirect;
import jiran.com.flyingfile.wifidirect.model.WifiDirectSocket;

@SuppressLint("NewApi")
public class SocketClientService extends IntentService {

    public static final String ACTION_CLIENT = "jiran.com.flyingfile.wifi_direct.FILE_CLIENT";
    //	private static final int SOCKET_TIMEOUT = 5000;
    public static final String EXTRAS_FILE_PATH = "file_path";
    public static final String EXTRAS_GROUP_OWNER_ADDRESS = "go_host";
    public static final String EXTRAS_GROUP_OWNER_PORT = "go_port";
//    public static final int SOCKET_PORT = 8988;

    public static final String EXTRAS_HANDLER = "handler";

    public static boolean isRunning = false;

    public Handler handlerClient = new Handler();

    public SocketClientService(String name) {
        super(name);
        // TODO Auto-generated constructor stub
    }

    public SocketClientService() {
        super("SocketClientService");
        // TODO Auto-generated constructor stub
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        // TODO Auto-generated method stub

        Log.d("WifiDirectClient", "[onHandleIntent] function");

        final Context context = getApplicationContext();

        if (isRunning)
            return;
        else
            isRunning = true;
        Log.d("SocketClientService", "[onHandleIntent] function");
        if (intent.getAction().equals(ACTION_CLIENT)) {

            //전송
            String host = intent.getExtras().getString(EXTRAS_GROUP_OWNER_ADDRESS);
            Socket socket = new Socket();
            int port = SocketServerService.SOCKET_PORT;

            //가끔가다가 시점차이로 소켓 커넥트에 실패하는거 같더라
            //10번까지 재시도 해보자. 10초동안 해보자
            int nCnt = 10;
            while (nCnt > 0) {
                try {

                    socket.connect((new InetSocketAddress(host, port)), 3000);
                    Log.d("WifiDirectClient", "[onHandleIntent] connect");

                    Log.d("SocketClientService", "[onHandleIntent] socket connect!!");


                    Log.d("WifiDirectClient", "[onHandleIntent] setSocket");

                    boolean isUseFileTransfer = false;

                    //내가 파일전송 사용중이다.
                    //이미 파일 전송중임을 알리는 메세지를 상대방에게 알려주자
                    if (isUseFileTransfer) {
                        //사용자에게 파일 전송 사용중임을 알리자
                        handlerClient.post(new Runnable() {

                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                Intent intent = new Intent(context, FakeDialogActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra("flag", FakeDialogActivity.FLAG_1);
                                context.startActivity(intent);
                            }
                        });
                    }

                    //커넥트가 성공함
                    nCnt = 0;

                    //커넥트 성공후 시점을 맞추기 위해 1바이트씩 패킷 교환을 한다
                    //자기 자신이 클라이언트이기 때문에 클라이언트임을 알린다.
                    try {
                        WifiDirectFileSender.getInstance().sendTypeIsClient(socket, WifiDirect.getP2pDev().deviceName, WifiDirect.getP2pDev().deviceAddress, isUseFileTransfer, Common.getInstance().getCurrentVersionCode(context));
                    } catch (Exception e) {
                    }
                    //WifiDirectFileSender.getInstance().sendTypeIsClient(socket, WifiDirect.getP2pDev().deviceName, WifiDirect.getP2pDev().deviceAddress, isUseFileTransfer, Common.getInstance().getCurrentVersionCode(context));

                    Log.d("WifiDirectClient", "[onHandleIntent] setSync");

                    //응답을 기다렸다가 리시브서비스로 넘어간다
                    byte[] bType = new byte[1];
                    BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
                    BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());

                    boolean bTcpRes = new TcpUtil().recv(bType.length, bis, bType);
                    if (!bTcpRes)
                        throw new IOException();

                    int nType = bType[0];

                    Log.d("WifiDirectClient", "[onHandleIntent] read");

                    //디바이스 이름도 얻어와야함
                    byte[] bNameLen = new byte[4];
                    byte[] bName = null;


                    bTcpRes = new TcpUtil().recv(bNameLen.length, bis, bNameLen);
                    if (!bTcpRes)
                        throw new IOException();

                    int nNameLen = UnitTransfer.getInstance().byteArrayToInt(bNameLen, ByteOrder.BIG_ENDIAN);

                    Log.d("WifiDirectClient", "[onHandleIntent] nNameLen : " + nNameLen);

                    bName = new byte[nNameLen];

                    bTcpRes = new TcpUtil().recv(bName.length, bis, bName);
                    if (!bTcpRes)
                        throw new IOException();

                    String strName = new String(bName);

                    Log.d("WifiDirectClient", "[onHandleIntent] nName nName : " + strName);

                    //디바이스 주소도 얻어와야함
                    byte[] bAddressLen = new byte[4];
                    byte[] bAddress = null;

                    bTcpRes = new TcpUtil().recv(bAddressLen.length, bis, bAddressLen);
                    if (!bTcpRes)
                        throw new IOException();

                    int nAddressLen = UnitTransfer.getInstance().byteArrayToInt(bAddressLen, ByteOrder.BIG_ENDIAN);
                    bAddress = new byte[nAddressLen];

                    bTcpRes = new TcpUtil().recv(bAddress.length, bis, bAddress);
                    if (!bTcpRes)
                        throw new IOException();

                    String strAddress = new String(bAddress);

                    Log.d("WifiDirectClient", "[onHandleIntent] strAddress : " + strAddress);

                    //파일전송 사용중인지 플래그도 받자
                    byte[] bFileTransferUse = new byte[1];

                    bTcpRes = new TcpUtil().recv(bFileTransferUse.length, bis, bFileTransferUse);
                    if (!bTcpRes)
                        throw new IOException();

                    byte[] bVersioCode = new byte[1];
                    bTcpRes = new TcpUtil().recv(bVersioCode.length, bis, bVersioCode);
                    if (!bTcpRes)
                        throw new IOException();
                    final byte[] bCrypt = new byte[1];
                    bTcpRes = new TcpUtil().recv(bCrypt.length, bis, bCrypt);
                    if (!bTcpRes)
                        throw new IOException();


                    WifiDirect.setOtherCrypt(bCrypt[0] == 1 ? true : false);
                    WifiDirect.setStrOtherDeviceAddress(strAddress);
                    WifiDirect.setStrOtherDeviceName(strName);

                    final byte[] bCrypt2 = new byte[1];
                    bCrypt2[0] = (byte) (SharedPreferenceUtil.getInstance().getCrypt(context) ? 1 : 0);

                    ByteBuffer bBuf = ByteBuffer.allocate(bCrypt2.length);
                    bBuf.put(bCrypt2);
                    bos.write(bBuf.array(), 0, bBuf.array().length);
                    bos.flush();

                    Log.d("WifiDirectClient", "[onHandleIntent]client  appversion : " + bVersioCode[0]);
                    if (bFileTransferUse[0] == 'T') {
                        //상대방이 파일전송 사용중이다.

                        if (!isUseFileTransfer) {
                            //사용자에게 파일 전송 사용중임을 알리자
                            handlerClient.post(new Runnable() {

                                @Override
                                public void run() {
                                    // TODO Auto-generated method stub
                                    Intent intent = new Intent(context, FakeDialogActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.putExtra("flag", FakeDialogActivity.FLAG_1);
                                    context.startActivity(intent);
                                }
                            });
                        }

                        //모든 서비스를 종료하자
                        WifiDirectServiceManager.getInstance().finishAllService(context);
                    } else if (bFileTransferUse[0] == 'F') {
                        if (nType == WifiDirect.FLAG_TYPE_IS_HOST) {

                            if (isUseFileTransfer) {
                                //내가 파일전송 사용중이다.
                                //위에서 이미 다이얼로그 띄움

                                //모든 서비스를 종료하자
                                WifiDirectServiceManager.getInstance().finishAllService(context);
                            } else {
                                //여기서부터 대기
                                WifiDirectSocket.getInstance().setSocket(socket);


                                Log.d("MTOM2", "ReceiverServiceStart");

                                if (WifiDirect.getP2pInfo() != null) {
                                    handlerClient.post(new Runnable() {

                                        @Override
                                        public void run() {
                                            WifiDirectFileExplorerActivityUtil.run(context, WifiDirect.getP2pInfo());
                                        }
                                    });
                                }


                                Log.d("WifiDirectClient", "[onHandleIntent] startReceiveService");


                                Log.d("SocketClientService", "[onHandleIntent] start SocketReceivService!!");
                            }
                        } else {
                            Log.d("SocketClientService", "[onHandleIntent] failed Receive Host Type");
                        }
                    }
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    //커넥트가 실패함 10번까지 재시도 해보자
                    Log.d("SocketClientService", "[onHandleIntent] socket error : " + e.getMessage());

                    Log.d("WifiDirectClient", "[onHandleIntent] Exception");

                    Log.d("MTOM2", "ConnectFail");

                    nCnt--;

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }

                }

            }

            if (WifiDirectSocket.getInstance().getSocket() == null || !WifiDirectSocket.getInstance().getSocket().isConnected()) {
                WifiDirectServiceManager.getInstance().finishAllService(context);
                Intent fakeDialog = new Intent(context, FakeDialogActivity.class);
                fakeDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                fakeDialog.putExtra("flag", FakeDialogActivity.FLAG_4);
                context.startActivity(fakeDialog);
            }
            isRunning = false;

            Log.d("WifiDirectClient", "[onHandleIntent] stopSelf");

            stopSelf();


        }

    }

}
