package jiran.com.flyingfile.wifidirect.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.net.wifi.p2p.WifiP2pManager.ChannelListener;
import android.net.wifi.p2p.WifiP2pManager.ConnectionInfoListener;
import android.net.wifi.p2p.WifiP2pManager.PeerListListener;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Method;

import jiran.com.flyingfile.BaseFlyingFileFragmentActivity;
import jiran.com.flyingfile.FlyingFileActivity;
import jiran.com.flyingfile.dialog.FakeDialogActivity;
import jiran.com.flyingfile.util.ProcessExitUtil;
import jiran.com.flyingfile.util.ScanWifiUtil;
import jiran.com.flyingfile.util.SharedPreferenceUtil;
import jiran.com.flyingfile.wifidirect.WifiDirectServiceManager;
import jiran.com.flyingfile.wifidirect.adapter.WifiDirectActivityDeviceAdapter;
import jiran.com.flyingfile.wifidirect.model.WifiDirect;
import jiran.com.flyingfile.wifidirect.model.WifiDirectSocket;
import jiran.com.flyingfile.wifidirect.task.SocketClientService;
import jiran.com.flyingfile.wifidirect.task.SocketServerService;
import jiran.com.tmfilemanager.R;

@SuppressLint("NewApi")
public class WifiDirectActivity
        extends
        BaseFlyingFileFragmentActivity
        implements
        PeerListListener,
        ConnectionInfoListener,
        OnItemClickListener,
        OnClickListener,
        ActionListener,
        ChannelListener {

    public Toolbar toolbar;
    public ImageButton toolbar_back_btn;
    public TextView toolbar_title;
    boolean isAllowDiscoverPeers = true;
    boolean isDisConnectWifi = false;
    private WifiP2pManager mWifiP2pManager = null;
    private IntentFilter mWifiP2PIntentFilter = null;
    private IntentFilter mWifiIntentFilter = null;
    private ListView lv_Peers = null;
    //private TextView tv_MyName = null;
    private WifiDirectActivityDeviceAdapter mAdapter = null;
    private Button btn_Refresh = null;
    private LinearLayout ll_WaitDevice = null;
    //private ImageButton ib_SideMenu = null;
    private boolean isWifiEnabled = false;
    private boolean isDiscoverSuccess = false;
    private boolean isRunningDiscoverThread = false;

    //wifi관련 이벤트(wifi접속 해제시키자)
    private BroadcastReceiver mWifiStateReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub

            String strAction = intent.getAction();

            if (strAction.equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

                if (wifiManager != null) {
                    if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
                        Log.d("peerlistactivity", "Wifi_Enabled");
                        wifiManager.disconnect();
                    } else if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_DISABLED) {
                        Log.d("peerlistactivity", "Wifi_Disabled");
                    } else if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_UNKNOWN) {
                        Log.d("peerlistactivity", "Wifi_Unknown");
                    }
                } else {

                }
            } else if (strAction.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if (networkInfo.isConnected()) {
                    //루나 폰만 문제 현상을 찾았다 다음번에 다른 폰이 있으면 여기다가 계속 Feature 처리 해나갈 예정이다...
                    if (!Build.MODEL.contains("TG-L800")) {
                        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                        wifiManager.disconnect();
                    }
                }
            }
        }
    };
    //wifi다이렉트 관련 이벤트
    private BroadcastReceiver mWifiP2PReceiver = new BroadcastReceiver() {

        @Override
        public synchronized void onReceive(Context context, Intent intent) {
            // TODO 자세한 사항은 WIFIDirectGlobalReceiver 참조할것

            String strAction = intent.getAction();
            if (strAction.equals(WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION)) {
                int state = intent.getIntExtra(WifiP2pManager.EXTRA_DISCOVERY_STATE, -1);
                switch (state) {
                    case WifiP2pManager.WIFI_P2P_DISCOVERY_STARTED: {
                        //isRunDiscovery = true;
                        Log.d("WifiDirectActivity", "[onReceive] WIFI_P2P_DISCOVERY_STARTED");
                        break;
                    }
                    case WifiP2pManager.WIFI_P2P_DISCOVERY_STOPPED: {
                        //isRunDiscovery = false;
                        Log.d("WifiDirectActivity", "[onReceive] WIFI_P2P_DISCOVERY_STOPPED");


                        //1.0.2 버전 (ActionListener를 별도로 구현 다른 부분과 꼬이는 경우가 있는듯함. 여기서 discover 실패하면 다시 초기화 하도록)
                        Log.d("dicoverPeers", "WIFI_P2P_DISCOVERY_STOPPED");

                        break;
                    }

                    default:
                        break;
                }
            } else if (strAction.equals(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION)) {
                //와이파이 다이렉트 상태 변화(설정상태 변화)
                int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
                switch (state) {
                    case WifiP2pManager.WIFI_P2P_STATE_ENABLED: {
                        //wifi p2p 사용 가능 상태
                        //아이스크림 샌드위치 때문에 여기서 디스커버 해야함
                        Log.d("WifiDirectActivity", "[onReceive] WIFI_P2P_STATE_ENABLED");
                        //단말기 탐색

                        //1.0.2 버전 (이벤트 부분을 setFailedDiscoveryListener로 변경)
                        Log.d("dicoverPeers", "WIFI_P2P_STATE_CHANGED_ACTION");
                        if (isAllowDiscoverPeers) {
//						mWifiP2pManager.discoverPeers(WIFIDirect.getP2pChannel(), setFailedDiscoveryListener());
                            discoverPeerOnThread();
                        }
                        break;
                    }
                    case WifiP2pManager.WIFI_P2P_STATE_DISABLED: {
                        //wifi p2p 사용 불가 상태
                        Log.d("WifiDirectActivity", "[onReceive] WIFI_P2P_STATE_DISABLED");
                        break;
                    }
                    default: {

                        break;
                    }
                }
            } else if (strAction.equals(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION)) {
                //디스커버리 결과값 리스트(단말기 리스트)
                mWifiP2pManager.requestPeers(WifiDirect.getP2pChannel(), WifiDirectActivity.this);

                Log.d("WifiDirectActivity", "[onReceive] WIFI_P2P_PEERS_CHANGED_ACTION");
            } else if (strAction.equals(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION)) {

                Log.d("WifiDirectActivity", "[onReceive] WIFI_P2P_CONNECTION_CHANGED_ACTION");


                //연결 변화 <- 여기서 양쪽 소켓 연결 필요
                NetworkInfo networkInfo = intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);


                if (networkInfo.getType() == 13) {
                    //13이 wifi_p2p이다.
                    if (networkInfo.isConnected()) {
                        mWifiP2pManager.requestConnectionInfo(WifiDirect.getP2pChannel(), WifiDirectActivity.this);
                    } else {
                        //1.0.2 버전 (이벤트 부분을 setFailedDiscoveryListener로 변경)
                        Log.d("dicoverPeers", "WIFI_P2P_CONNECTION_CHANGED_ACTION");
                        if (isAllowDiscoverPeers) {
//							mWifiP2pManager.discoverPeers(WIFIDirect.getP2pChannel(), setFailedDiscoveryListener());
                            discoverPeerOnThread();
                        }
                        //단말기 탐색
                    }
                } else {
                }
            } else if (strAction.equals(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION)) {
                //내 상태 변화
                WifiP2pDevice dev = (WifiP2pDevice) intent.getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);
                String strName = dev.deviceName;
                final String strResult = strName;
//				tv_MyName.post(new Runnable() {
//
//					@Override
//					public void run() {
//						// TODO Auto-generated method stub
//						tv_MyName.setEllipsize(TruncateAt.END);
//						tv_MyName.setLines(2);
//						tv_MyName.setText(strResult);
//
//					}
//				});
                WifiDirect.setP2pDev(dev);

                Log.d("WifiDirectActivity", "WIFI_P2P_THIS_DEVICE_CHANGED_ACTION");
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        overridePendingTransition(R.anim.slide_left_in, R.anim.hold);
        setContentView(R.layout.flyingfile_view_mtom2);
        FlyingFileActivity.actList.add(this);
        super.setForeground(true);
        //인스턴스 초기화
        initInstance();

        //브로드캐스트 리시버에서 받을 이벤트 필터 초기화
        initIntentFilter();

        //이벤트 등록
        initEvent();


        Channel channel = mWifiP2pManager.initialize(this, getMainLooper(), this);
        WifiDirect.setP2pChannel(channel);

        //리스트뷰 어댑터 등록
        lv_Peers.setAdapter(mAdapter);

        initSystemConfiguration();


        //wifi다이렉트 관련 이벤트 등록
        registerReceiver(mWifiP2PReceiver, mWifiP2PIntentFilter);
        //wifi 관련 이벤트 등록(onresume에서 수행시키고 onpause에서 중지 시키자


        super.onCreate(savedInstanceState);
        SharedPreferenceUtil.getInstance().setWiFiDirectDisConnect(this, true);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_back_btn = toolbar.findViewById(R.id.toolbar_back_btn);
        toolbar_back_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
//		toolbar.setTitleTextColor(Color.WHITE);
//		toolbar.setTitle(getResources().getString(R.string.app_name));
        setTitle(getResources().getString(R.string.left_menu_file_sender_wifi));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public void setTitle(String title) {
        toolbar_title.setText(title);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // TODO Auto-generated method stub
        return super.onTouchEvent(event);
    }

    public void initSystemConfiguration() {
        //시스템 설정 하기전 상태값을 기억하고 있다가 화면 나가면 돌려놓자
        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        isWifiEnabled = wm.isWifiEnabled();


        //wifi연결 끊기전에 네트웍아이디 기억해 놓자
        String strBeforeSSID = wm.getConnectionInfo().getSSID();
        if (strBeforeSSID != null)
            ScanWifiUtil.setStrBeforeSSID(strBeforeSSID);

        //아이스크림 샌드위치인 경우에는 wifi다이렉트 버튼을 on 시켜주자.
        int nSDKINT = Build.VERSION.SDK_INT;
        if (nSDKINT == 14 || nSDKINT == 15) {
            enableP2p();
        } else if (nSDKINT > 15) {
            //젤리빈 이상부터는 wifi를 on 시켜주자
            //와이파이 꺼져 있을 경우에만 on 시켜주자
            if (!wm.isWifiEnabled())
                wm.setWifiEnabled(true);
        }

    }

    @Override
    protected void onResume() {
        // TODO 화면 들어올때 수행


        if (isAllowDiscoverPeers)
            discoverPeerOnThread();


        //와이파이다이렉트가 연결중이면 바로 탐색기 화면을 띄우자
        if (WifiDirectSocket.getInstance().isConnecting()) {
            WifiDirectFileExplorerActivityUtil.run(this);
        }


        //젤리빈 이하만 와이파이 끊음.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            startReiver();
        }
        //연결 실패해서 돌아왔을때 와이파이 끊기.
        if (!SharedPreferenceUtil.getInstance().getWiFiDirectDisConnect(this)) {
            startReiver();
            Log.i("@@@@@@@@@@@@@@@@", "wifi 끊자.");
            isDisConnectWifi = true;
        }

        super.onResume();
    }

    /**
     * 와이파이 해제 하기.
     */
    private void startReiver() {
        try {
            if (mWifiStateReceiver != null)
                registerReceiver(mWifiStateReceiver, mWifiIntentFilter);
        } catch (Exception e) {

        }
    }

    @Override
    protected void onPause() {
        // TODO 화면 벗어날때 수행
        //wifi다이렉트 관련 이벤트 해제


        try {
            if (mWifiStateReceiver != null) {
                unregisterReceiver(mWifiStateReceiver);
            }
        } catch (Exception e) {

        }


        super.onPause();
    }

    @Override
    protected void onPermissionResult(int permission, boolean isSuccess) {
        // TODO Auto-generated method stub
        if (!isSuccess)
            ProcessExitUtil.getInstance().processExit(this);
    }

    @Override
    public void finish() {

        try {
            unregisterReceiver(mWifiP2PReceiver);
        } catch (Exception e) {

        }

        try {
            unregisterReceiver(mWifiStateReceiver);
        } catch (Exception e) {

        }

        mWifiP2pManager.cancelConnect(WifiDirect.getP2pChannel(), this);
        mWifiP2pManager.removeGroup(WifiDirect.getP2pChannel(), this);

        if (Build.VERSION.SDK_INT > 15) {
            if (mWifiP2pManager != null)
                mWifiP2pManager.stopPeerDiscovery(WifiDirect.getP2pChannel(), this);
        }

        //wifi 설정값을 원래대로 돌려놓자
        final WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        wm.setWifiEnabled(isWifiEnabled);

        if (Build.VERSION.SDK_INT == 14 || Build.VERSION.SDK_INT == 15) {
            disableP2p();
        }


        Intent data = new Intent();
        data.putExtra("iswifi", isWifiEnabled);
        setResult(RESULT_OK, data);


        if (isWifiEnabled && Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT ||
                isWifiEnabled && isDisConnectWifi && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            IntentFilter filter = new IntentFilter();
            filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
            BroadcastReceiver receiver = new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {
                    // TODO Auto-generated method stub

                    String action = intent.getAction();
                    if (action.equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
                        wm.setWifiEnabled(true);
                    }
                    unregisterReceiver(this);
                }
            };
            registerReceiver(receiver, filter);
            wm.setWifiEnabled(false);

        }

        super.finish();
        overridePendingTransition(R.anim.hold, R.anim.slide_right_out);
    }

    /**
     * 인스턴스 초기화
     */
    public void initInstance() {
        lv_Peers = (ListView) findViewById(R.id.ListView_Available_Device);
        //tv_MyName = (TextView) findViewById(R.id.TextView_MyDevice);
        mAdapter = new WifiDirectActivityDeviceAdapter(this, R.layout.flyingfile_view_wifidirect_item);
        btn_Refresh = (Button) findViewById(R.id.Button_Refresh);
        mWifiP2pManager = (WifiP2pManager) getSystemService(WIFI_P2P_SERVICE);
        mWifiP2PIntentFilter = new IntentFilter();
        mWifiIntentFilter = new IntentFilter();

        ll_WaitDevice = (LinearLayout) findViewById(R.id.LinearLayout_WaitDevice);

        //ib_SideMenu = (ImageButton) findViewById(R.id.ImageButton_SideMenu);
    }

    /**
     * 인텐트 필터 초기화
     */
    public void initIntentFilter() {
        //브로드캐스트 리시버에서 받을 이벤트 필터
        mWifiP2PIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mWifiP2PIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mWifiP2PIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mWifiP2PIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
        mWifiP2PIntentFilter.addAction(WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION);

        //와이파이 관련 이벤트 필터
        mWifiIntentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        mWifiIntentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
    }

    /**
     * 이벤트 등록
     */
    public void initEvent() {
        //이벤트 등록
        lv_Peers.setOnItemClickListener(this);
        btn_Refresh.setOnClickListener(this);
        //ib_SideMenu.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.Button_Refresh: {

                ll_WaitDevice.setVisibility(View.VISIBLE);
                //어댑터에 있는 아이템 모두 제거
                mAdapter.clear();


                if (Build.VERSION.SDK_INT > 15) {
                    if (mWifiP2pManager != null)
                        mWifiP2pManager.stopPeerDiscovery(WifiDirect.getP2pChannel(), this);
                }


                //1.0.2 버전 (이벤트 부분을 setFailedDiscoveryListener로 변경)
                Log.d("dicoverPeers", "onclick");
                if (isAllowDiscoverPeers) {
                    discoverPeerOnThread();
                }

                break;
            }
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.setForeground(false);
//		Intent intent = new Intent(this, FlyingFileActivity.class);
//        intent.putExtra("isFirst", false);
//		startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.setForeground(false);
        FlyingFileActivity.actList.remove(this);
        super.onDestroy();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        // TODO Auto-generated method stub
        switch (parent.getId()) {
            case R.id.ListView_Available_Device: {


                WifiP2pDevice device = mAdapter.getItem(position);
                /**
                 * 0 : connected
                 * 1 : invited
                 * 2 : failed
                 * 3 : available
                 * 4 : unavailable
                 */
                int nStatus = device.status;
                if (nStatus == WifiP2pDevice.AVAILABLE) {
                    String strAddress = device.deviceAddress;
                    connect(strAddress);
                    SharedPreferenceUtil.getInstance().setWiFiDirectDisConnect(this, false);
                }
                break;
            }

            default:
                break;
        }
    }

    public void connect(String strAddress) {
        // TODO Auto-generated method stub
        final WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = strAddress;
        config.wps.setup = WpsInfo.PBC;
        mWifiP2pManager.connect(WifiDirect.getP2pChannel(), config, new ActionListener() {

            @Override
            public void onSuccess() {
                // TODO Auto-generated method stub
                Log.d("aa", "aa");
            }

            @Override
            public void onFailure(int reason) {
                // TODO Auto-generated method stub
                Log.d("aa", "aa");
            }
        });
    }

    @Override
    public void onConnectionInfoAvailable(WifiP2pInfo info) {
        // TODO Auto-generated method stub
        /**
         * requestconnectioninfo에 대한 콜백으로 들어온다.
         *
         * groupFormed : 이거는 우선 무조건 true인거 같고
         * isGroupOwner : 이거는 이 그룹의 주인인지 아닌지 판단
         * 즉, connect 주체가 자신이면 true이고, 상대방으로부터 connect 요청을 받은것이면 false가 된다.
         *
         * groupOwner는 랜덤으로 선정된다.
         */


        if (info.groupFormed) {
            WifiDirect.setP2pInfo(info);

            Intent fakeDialog = new Intent(WifiDirectActivity.this, FakeDialogActivity.class);
            fakeDialog.putExtra("flag", FakeDialogActivity.FLAG_2);
            fakeDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            WifiDirectActivity.this.startActivity(fakeDialog);

            if (info.isGroupOwner) {
                //owner가 되는애가 서버 열고 커넥트 대기해야함.

                if (!SocketServerService.isRunning) {
                    Intent serviceIntent = new Intent(WifiDirectActivity.this, SocketServerService.class);
                    serviceIntent.setAction(SocketServerService.ACTION_SERVER);
                    serviceIntent.putExtra(SocketServerService.EXTRAS_FILE_PATH, "");
                    serviceIntent.putExtra(SocketServerService.EXTRAS_GROUP_OWNER_ADDRESS, info.groupOwnerAddress.getHostAddress());
                    ComponentName name = WifiDirectActivity.this.startService(serviceIntent);
                    WifiDirectServiceManager.setServiceServer(name);
                    Log.d("MTOM", "group owner server run!");
                }
            } else {

                if (!SocketClientService.isRunning) {
                    //owner아닌애는 커넥트 시도 해야함
                    Intent serviceIntent = new Intent(WifiDirectActivity.this, SocketClientService.class);
                    serviceIntent.setAction(SocketClientService.ACTION_CLIENT);
                    serviceIntent.putExtra(SocketClientService.EXTRAS_FILE_PATH, "");
                    serviceIntent.putExtra(SocketClientService.EXTRAS_GROUP_OWNER_ADDRESS, info.groupOwnerAddress.getHostAddress());

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    Log.d("MTOM", "group client run!");
                    ComponentName name = WifiDirectActivity.this.startService(serviceIntent);
                    WifiDirectServiceManager.setServiceClient(name);
                }
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPeersAvailable(WifiP2pDeviceList peers) {
        // TODO Auto-generated method stub
        /**
         * discover가 완료 하여
         * WIFI_P2P_PEERS_CHANGED_ACTION에서
         * request peer에 대한 콜백 함수인듯 함
         * 여기서 각 peer리스트를 최신화 하던가 아니면 바로 커넥션을 하던가 해야 될듯
         * 아무튼 리스트 받아오는 단계임
         *
         * connect 하였을때 WIFI_P2P_PEERS_CHANGED_ACTION로 들어가기 때문에
         * 다시 이쪽으로도 들어옴
         *
         */
        mAdapter.clear();

        for (WifiP2pDevice d : peers.getDeviceList()) { //...
            mAdapter.add(d);
        }

//		Iterator<WifiP2pDevice> iter = peers.getDeviceList().iterator();
//		while(iter.hasNext())
//		{
//			mAdapter.add(iter.next());
//		}

        ll_WaitDevice.setVisibility(View.GONE);

        mAdapter.notifyDataSetChanged();

//		mWifiP2pManager.discoverPeers(WIFIDirect.getP2pChannel(), this);
    }

    @Override
    public void onSuccess() {
        // TODO 와이파이다이렉트 매니저 관련 함수 성공 콜백
        Log.d("WifiDirectActivity", "[onSuccess] function");
    }

    @Override
    public void onFailure(int reason) {
        // TODO 와이파이다이렉트 매니저 관련 함수 실패 콜백
        Log.d("WifiDirectActivity", "[onFailure] function reason : " + reason);
        switch (reason) {
            case WifiP2pManager.P2P_UNSUPPORTED: {
                //와이파이 다이렉트 지원 안함
                break;
            }
            case WifiP2pManager.ERROR: {
                //알수없는 오류
                break;
            }
            case WifiP2pManager.BUSY: {

                //1.0.2버전 제거
			/*
			//사용중?? busy상태?
			initInstance();

			//브로드캐스트 리시버에서 받을 이벤트 필터 초기화
			initIntentFilter();

//			mChannel = mWifiP2pManager.initialize(this, getMainLooper(), this);
			Channel channel = mWifiP2pManager.initialize(this, getMainLooper(), this);
			WIFIDirect.setP2pChannel(channel);
			*/

                break;
            }

            default:
                break;
        }

    }

    @Override
    public void onChannelDisconnected() {
        // TODO 와이파이다이렉트매니저 초기화 할때 파라미터로 들어가는 콜백함수?
        Log.d("WifiDirectActivity", "[onChannelDisconnected] function");
    }

    public void enableP2p() {

        try {
            Method method = mWifiP2pManager.getClass().getMethod("enableP2p", Channel.class);
            method.invoke(mWifiP2pManager, WifiDirect.getP2pChannel());
        } catch (Exception e) {

        }

    }

    //	public static boolean isRunningDisable = false;
    public void disableP2p() {
        try {
            Method method = mWifiP2pManager.getClass().getMethod("disableP2p", Channel.class);
            method.invoke(mWifiP2pManager, WifiDirect.getP2pChannel());
        } catch (Exception e) {

        }
        //				isRunningDisable = true;
    }

    public ActionListener setFailedDiscoveryListener() {
        ActionListener listener = new ActionListener() {

            @Override
            public void onSuccess() {
                // TODO Auto-generated method stub
                Log.d("WifiDirectActivity", "setDiscoveryListener onSuccess");
            }

            @Override
            public void onFailure(int reason) {
                // TODO Auto-generated method stub
                switch (reason) {
                    case WifiP2pManager.P2P_UNSUPPORTED: {
                        //와이파이 다이렉트 지원 안함
                        break;
                    }
                    case WifiP2pManager.ERROR: {
                        //알수없는 오류
                        break;
                    }
                    case WifiP2pManager.BUSY: {
                        //1.0.2버전 추가
                        Log.d("dicoverPeers", "setFailedDiscoveryListener");
                        if (isAllowDiscoverPeers) {
                            discoverPeerOnThread();
                        }
                        break;
                    }

                    default:
                        break;
                }
            }
        };
        return listener;
    }

    public synchronized void discoverPeerOnThread() {

        isDiscoverSuccess = false;
        Thread thread = new Thread(new Runnable() {

            @Override
            public synchronized void run() {
                // TODO Auto-generated method stub
                isRunningDiscoverThread = true;
                while (!isDiscoverSuccess) {
                    Channel channel = WifiDirect.getP2pChannel();

                    if (channel != null) {
                        mWifiP2pManager.discoverPeers(channel, new ActionListener() {

                            @Override
                            public void onSuccess() {
                                // TODO Auto-generated method stub
                                isDiscoverSuccess = true;
                                Log.d("discoverPeerOnThread", "discover Success");
                            }

                            @Override
                            public void onFailure(int reason) {
                                // TODO Auto-generated method stub
                                Log.d("discoverPeerOnThread", "discover Fail");
                            }
                        });
                    }

                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
                isRunningDiscoverThread = false;
            }
        });

        if (!isRunningDiscoverThread)
            thread.start();
    }

}
