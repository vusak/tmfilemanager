package jiran.com.flyingfile.custom;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

/**
 * Created by jeon on 2016-11-04.
 * 그리드 라이브러리 이미지 회전
 */

public class RotateTransformation extends BitmapTransformation {

    private float rotateRotationAngle = 0f;

    public RotateTransformation(Context context, int rotation) {
        super(context);

        this.rotateRotationAngle = rotation;
    }

    @Override
    protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
        Matrix matrix = new Matrix();

        matrix.postRotate(rotateRotationAngle);
        if (rotateRotationAngle == 0) {
            return toTransform;
        } else {
            return Bitmap.createBitmap(toTransform, 0, 0, toTransform.getWidth(), toTransform.getHeight(), matrix, true);
        }
    }

    @Override
    public String getId() {
        return "rotate" + rotateRotationAngle;
    }

}