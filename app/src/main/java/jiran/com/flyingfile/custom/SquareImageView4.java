package jiran.com.flyingfile.custom;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.ImageView;

/**
 * Created by User on 2016-09-22.
 */
public class SquareImageView4 extends ImageView {

    public SquareImageView4(Context context) {
        super(context);
    }

    public SquareImageView4(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public SquareImageView4(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        DisplayMetrics dm = getContext().getApplicationContext().getResources().getDisplayMetrics();


        Resources r = getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, r.getDisplayMetrics());
        int width = (int) (dm.widthPixels / 4 - px);
        setMeasuredDimension(width, width);
        //   super.onMeasure(widthMeasureSpec/4, widthMeasureSpec/4);
    }
}
