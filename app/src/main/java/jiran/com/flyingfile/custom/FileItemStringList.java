package jiran.com.flyingfile.custom;

import java.io.File;
import java.util.ArrayList;

public class FileItemStringList<T> extends ArrayList<T> {

    /**
     *
     */
    private static final long serialVersionUID = 6838617914419226933L;
    int nTotalCntNotFolder = 0;
    long totalsize = 0;

    public int getnTotalCntNotFolder() {
        return nTotalCntNotFolder;
    }

    public long getTotalsize() {
        return totalsize;
    }

    @Override
    public boolean add(T object) {
        File file = new File((String) object);
        if (!file.isDirectory()) {
            totalsize += file.length();
            nTotalCntNotFolder += 1;
        }
        return super.add(object);
    }

    @Override
    public void clear() {
        totalsize = 0;
        nTotalCntNotFolder = 0;
        super.clear();
    }

    @Override
    public T remove(int index) {
        File file = new File((String) get(index));
        if (!file.isDirectory()) {
            totalsize -= file.length();
            nTotalCntNotFolder -= 1;
        }

        return super.remove(index);
    }


}
