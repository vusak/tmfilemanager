package jiran.com.flyingfile.custom;

/**
 * Created by jeon on 2017-03-15.
 * RC4
 */

public class Crypt {
    private final byte[] S = new byte[256];
    private final byte[] T = new byte[256];
    private int keylen;

    public Crypt(String myUUID, String targetUUID, long fileLength) {
        byte[] key = generateKey(myUUID, targetUUID, fileLength);
        init(key);

    }

    public Crypt() {
        byte[] key = generateKey();
        init(key);

    }

    /**
     * @param myUUID     : 파일 전송자 UUID
     * @param targetUUID : 파일 수신자 UUID
     * @param fileLength : 전체 파일 길이(건너뛰기 포함)
     * @return N byte로 된 암호화 키
     */
    public byte[] generateKey(String myUUID, String targetUUID, long fileLength) {
        int keySize = 10;
        byte[] bMyUUID = myUUID.getBytes();
        byte[] bTargetUUID = targetUUID.getBytes();
        byte[] bKey = new byte[keySize];
        fileLength = fileLength % keySize;
        for (int i = 0; i < keySize; i++) {
            bKey[i] = (byte) ((bMyUUID[i] + bTargetUUID[i]) << fileLength);
        }
        return bKey;
    }

    /**
     * @return N byte로 된 암호화 키
     */
    public byte[] generateKey() {
        return "flyingfile".getBytes();
    }

    public void init(byte[] key) {
        if (key.length < 1 || key.length > 256) {
            throw new IllegalArgumentException(
                    "key must be between 1 and 256 bytes");
        } else {
            keylen = key.length;
            for (int i = 0; i < 256; i++) {
                S[i] = (byte) i;
                T[i] = key[i % keylen];
            }
            int j = 0;
            for (int i = 0; i < 256; i++) {
                j = (j + S[i] + T[i]) & 0xFF;
                byte temp = S[i];
                S[i] = S[j];
                S[j] = temp;
            }
        }
    }

    public byte[] encrypt(final byte[] plaintext) {
        final byte[] ciphertext = new byte[plaintext.length];
        int i = 0, j = 0, k, t;
        for (int counter = 0; counter < plaintext.length; counter++) {
            i = (i + 1) & 0xFF;
            j = (j + S[i]) & 0xFF;
            byte temp = S[i];
            S[i] = S[j];
            S[j] = temp;
            t = (S[i] + S[j]) & 0xFF;
            k = S[t];
            ciphertext[counter] = (byte) (plaintext[counter] ^ k);
        }
        return ciphertext;
    }

    public byte[] decrypt(final byte[] ciphertext) {
        return encrypt(ciphertext);
    }
}
