package jiran.com.flyingfile.custom;

import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import jiran.com.flyingfile.FlyingFileConst;
import jiran.com.flyingfile.callback.ProgressListener;

public class ProgressOutputStream extends BufferedOutputStream {
    private final ProgressListener listener;
    private long transferred;
    private File file;

    public ProgressOutputStream(final OutputStream out, final ProgressListener listener) {
        super(out, FlyingFileConst.BUFFSIZE);
        this.listener = listener;
        this.transferred = 0;

        Log.d("CustomFilebody", "CountingOutputStream()");

//		if(!isClosed)
        this.listener.started(getFile());

    }

    public void write(byte[] b, int off, int len) throws IOException {

//		mnCurrentSize+=len;


        out.write(b, off, len);
        this.transferred += len;

//		if(!isClosed)
        this.listener.transferred(getFile(), this.transferred);
    }

    public void write(int b) throws IOException {

        out.write(b);
        this.transferred++;
        this.listener.transferred(getFile(), this.transferred);
    }

    public void addTransferred(long transferred) {
        //	this.transferred +=transferred;
        this.listener.transferred(getFile(), this.transferred);
    }

    public long getTransferred() {
        return transferred;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }


}
