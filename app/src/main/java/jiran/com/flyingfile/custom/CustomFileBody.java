package jiran.com.flyingfile.custom;

import org.apache.http.entity.mime.content.FileBody;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import jiran.com.flyingfile.FlyingFileConst;

public class CustomFileBody extends FileBody {

    private ProgressListener listener;

    public CustomFileBody(File file, ProgressListener listener) {
        super(file);
        // TODO Auto-generated constructor stub
        this.listener = listener;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        // TODO Auto-generated method stub
//		String strName = getFilename();
        return super.getInputStream();
    }

    @Override
    public long getContentLength() {
        // TODO Auto-generated method stub
        return super.getContentLength();
    }

    @Override
    public File getFile() {
        // TODO Auto-generated method stub
        return super.getFile();
    }

    @Override
    public String getFilename() {
        // TODO Auto-generated method stub
        return super.getFilename();
    }

    @Override
    public String getTransferEncoding() {
        // TODO Auto-generated method stub
        return super.getTransferEncoding();
    }

    @Override
    public void writeTo(OutputStream outstream) throws IOException {
        // TODO Auto-generated method stub
        super.writeTo(new CountingOutputStream(outstream, this.listener));
    }


    public static interface ProgressListener {
        void transferred(File file, long num);

        void started(File file);
    }

    public class CountingOutputStream extends BufferedOutputStream {

        private final ProgressListener listener;
        private long transferred;

        public CountingOutputStream(final OutputStream out, final ProgressListener listener) {
            super(out, FlyingFileConst.BUFFSIZE);
            this.listener = listener;
            this.transferred = 0;
            this.listener.started(CustomFileBody.this.getFile());
        }

        public void write(byte[] b, int off, int len) throws IOException {
            out.write(b, off, len);
            this.transferred += len;
            this.listener.transferred(CustomFileBody.this.getFile(), this.transferred);
        }

        public void write(int b) throws IOException {
            out.write(b);
            this.transferred++;
            this.listener.transferred(CustomFileBody.this.getFile(), this.transferred);
        }
    }

}
