package jiran.com.flyingfile.callback;

import jiran.com.flyingfile.model.SkipListTaskResult;

public interface SkipListTaskListener {
    public void onSkipListRequest();

    public void onSkipListResult(SkipListTaskResult result);
}
