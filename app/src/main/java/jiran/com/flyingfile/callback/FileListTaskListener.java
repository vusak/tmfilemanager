package jiran.com.flyingfile.callback;

import jiran.com.flyingfile.model.FileListTaskResult;

public interface FileListTaskListener {

    public void onRequest();

    public void onResponse(FileListTaskResult result);
}
