package jiran.com.flyingfile.callback;

import jiran.com.flyingfile.model.StatusResult;

public interface StatusTaskListener {
    public void onRequest();

    public void onResponse(StatusResult result);
}
