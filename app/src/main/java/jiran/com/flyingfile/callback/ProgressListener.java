package jiran.com.flyingfile.callback;

import java.io.File;

/**
 * Created by jeon-HP on 2017-05-24.
 */

public interface ProgressListener {
    void transferred(File file, long num);

    void started(File file);
}
