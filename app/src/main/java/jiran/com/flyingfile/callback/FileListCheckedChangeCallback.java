package jiran.com.flyingfile.callback;

public interface FileListCheckedChangeCallback {
    public void onCheckedChange(int nSelectedCount, int nTotalCount, long nSelectedFileSize);
}
