package jiran.com.flyingfile.callback;

import jiran.com.flyingfile.model.FileidDel_RealtimeTaskResult;

public interface FileidDel_RealtimeTaskListener {

    public void onFileidDelRealtimeRequest();

    public void onFileidDelRealtimeResponse(FileidDel_RealtimeTaskResult result);
}
