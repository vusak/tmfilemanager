package jiran.com.flyingfile.callback;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.bumptech.glide.Glide;

/**
 * Created by jeon on 2016-11-07.
 */

public class CustomScrollListener extends RecyclerView.OnScrollListener {
    Context context;

    public CustomScrollListener(Context context) {
        this.context = context;
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        switch (newState) {
            case RecyclerView.SCROLL_STATE_IDLE:
                try {
                    if (Glide.with(context).isPaused()) {
                        Glide.with(context).resumeRequests();
                    }
                } catch (Exception e) {
                }

                break;
//            case RecyclerView.SCROLL_STATE_DRAGGING:
//                if(!Glide.with(context).isPaused()) {
//                    Glide.with(context).pauseRequests();
//                }
//                break;
        }


    }


    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        try {
            if (dy == 0) return;
            if (Math.abs(dy) <= 10) {
                if (Glide.with(context).isPaused()) {
                    Glide.with(context).resumeRequests();
                }
            } else {
                if (!Glide.with(context).isPaused()) {
                    Glide.with(context).pauseRequests();
                }
            }
        } catch (Exception e) {
        }
    }


}