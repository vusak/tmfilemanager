package jiran.com.flyingfile.callback;

public interface DeleteCallback {

    //파일 삭제 진행
    void onProgressDelete(int progress);

}
