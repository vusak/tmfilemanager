package jiran.com.flyingfile.callback;

import java.io.File;

/**
 * Created by jeon-HP on 2017-05-24.
 */

public interface OnReadListener {
    public void onStart(String file, int index, long nReadCnt);

    public void onRead(File file, int index, long nReadCnt);

    public void onFinish(String file, int index, long nReadCnt, long fileSize);
}