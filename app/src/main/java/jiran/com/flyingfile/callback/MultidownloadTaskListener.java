package jiran.com.flyingfile.callback;


public interface MultidownloadTaskListener {

    public void onMultiDownloadStart(long nTotalFileSize);

    public void onMultiDownloadProgress(String target, int nIdx, int nTotalCnt, long nCurrentProgress, long nTotalProgress);

    public void onMultiDownloadFinish(String target, int nIdx, int nTotalCnt, long nCurrentProgress, long nTotalProgress, int jumpCnt);

    public void onMultiDownloadFileComplete(String targetPath);

    public void onMultiDownloadError(String msg, boolean isForceCancel, String strCurrentSavePath, int jumpCnt);
//	public void onMultiDownloadNetworkError();
}
