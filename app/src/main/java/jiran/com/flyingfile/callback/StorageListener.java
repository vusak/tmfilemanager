package jiran.com.flyingfile.callback;

import android.content.Intent;

public interface StorageListener {

    public void onUpdateMounted(Intent intent, boolean isMounted);

}

