package jiran.com.flyingfile.callback;

public interface SendTaskListener {

    public void onSendRequest(long nTotalFileSize);

    public void onSendFinish(int jumpCnt);

    public void onSendError(int jumpCnt);

    public void onOneFileComplete(int idx, long currentSize, long maxSize, String target);

    public void onSendProgress(int idx, long currentSize, long maxSize, String target);

    public void onServerLog(boolean isDirect, long sendingSize); // 직접전송 여부, 전송크기

}
