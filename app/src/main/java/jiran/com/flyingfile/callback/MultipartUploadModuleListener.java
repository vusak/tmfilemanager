package jiran.com.flyingfile.callback;

import java.io.File;

public interface MultipartUploadModuleListener {

    public void onProgress(File currentSendFile, long nSentSize, long nTotalSize);

    public void onFinish(File currentSendFile, long nSentSize);

    public void onError(File currentSendFile, long nSentSize, long nTotalSize);
}
