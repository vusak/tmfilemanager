package jiran.com.flyingfile.callback;

import jiran.com.flyingfile.model.RealtimeResult;

public interface FileidRealtimeTaskListener {
    public void onCreateRequest();

    public void onCreateResponse(RealtimeResult RealtimeResult);
}
