package jiran.com.flyingfile.callback;

import jiran.com.flyingfile.model.CancelTaskResult;

public interface CancelTaskListener {
    public void onCancelTaskResult(CancelTaskResult result);
}
