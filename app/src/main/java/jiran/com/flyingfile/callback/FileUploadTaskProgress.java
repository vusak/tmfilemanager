package jiran.com.flyingfile.callback;

import java.io.File;

public class FileUploadTaskProgress {

    public static final int FLAG_PROGRESS = 111;
    public static final int FLAG_FINISH = 222;
    public static final int FLAG_ERROR = 333;

    private long total;
    private long current;
    private int idx;
    private File file;
    private int flag;


    public FileUploadTaskProgress(int flag, long total, long current, int idx,
                                  File file) {
        super();
        this.total = total;
        this.current = current;
        this.idx = idx;
        this.file = file;
        this.flag = flag;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public long getCurrent() {
        return current;
    }

    public void setCurrent(long current) {
        this.current = current;
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

}
