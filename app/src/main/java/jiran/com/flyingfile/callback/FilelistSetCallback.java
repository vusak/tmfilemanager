package jiran.com.flyingfile.callback;

public interface FilelistSetCallback {

    public void onStartLoadFilelist();

    public void onFinishLoadFilelist(String strPath, int nTotalFileCount);
}
