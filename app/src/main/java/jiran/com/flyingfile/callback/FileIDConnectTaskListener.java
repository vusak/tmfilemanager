package jiran.com.flyingfile.callback;

import jiran.com.flyingfile.model.FileIDConnectResult;

public interface FileIDConnectTaskListener {

    public void onResponse(FileIDConnectResult connectResult, String strFileid);
}
