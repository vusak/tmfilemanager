package jiran.com.flyingfile.callback;

import jiran.com.flyingfile.model.StatusRealtimeTaskResult;

public interface StatusRealtimeTaskListener {

    public void onStatusRealtimeRequest();

    public void onStatusRealtimeResult(StatusRealtimeTaskResult result);
}
