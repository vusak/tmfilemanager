package jiran.com.flyingfile.callback;

import jiran.com.flyingfile.model.CreateFileidTaskResult;

public interface CreateFileidTaskListener {

    public void onResponse(CreateFileidTaskResult result);

}
