package jiran.com.flyingfile.callback;

public interface NetworkChangeListener {
    public static final int FLAG_MOBILE_NETWORK = 111;
    public static final int FLAG_WIFI_NETWORK = 222;
    public static final int FLAG_NONEABLE = 333;

    public void onChangeNetwork(int nFlag);
}
