package jiran.com.flyingfile.callback;

public interface FileIDTcpConnectCallback {
    public void onConnectTcp(boolean isConnect);
}
