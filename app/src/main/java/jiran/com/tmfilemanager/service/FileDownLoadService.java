package jiran.com.tmfilemanager.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.Stack;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.IntroActivity;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.common.ClipBoard;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.network.JMF_network;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by user on 2016-09-06.
 */
public class FileDownLoadService extends Service {

    public static ArrayList<FileItem> downloadFileList;
    //    public static String mTeam_id;
//    public static String mObject_id;
    public static String mCurrentPath;

    private static Stack<FileItem> listStack;

    private NotificationManager mNotificationManager;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.d("FileDownLoadService", "onCreate");
        super.onCreate();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        Log.d("FileDownLoadService", "onStart");
        super.onStart(intent, startId);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("FileDownLoadService", "onStartCommand");
        if (downloadFileList != null && downloadFileList.size() > 0) {
            listStack = new Stack<>();
            for (int i = downloadFileList.size() - 1; i >= 0; i--) {
                listStack.add(downloadFileList.get(i));
            }
            sendNotification("Downloading...", "", "");
            requestDownlaod();
        } else {
            stopSelf();
            mCurrentPath = "";
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.d("FileDownLoadService", "onDestroy");
        mCurrentPath = "";
        super.onDestroy();
    }

    public void requestDownlaod() {
        if (listStack != null && listStack.size() > 0) {
            FileItem currentItem = listStack.get(listStack.size() - 1);
            listStack.pop();

            try {
                Log.i("JMF UploadTask", "team_id => " + currentItem.getTeamId() + " object_id => " + currentItem.getId());

                JMF_network.filedownload(currentItem.getTeamId(), currentItem.getId(), mCurrentPath, currentItem.getName(), null)
                        .flatMap(gigaPodPair -> {
                            return Observable.just(gigaPodPair);
                        })
                        .doOnError(throwable -> {

                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(gigaPodPair -> {
                                    if (gigaPodPair.first) {
                                        Log.i("JMF filedownload", " savePath =>=>" + ((String) gigaPodPair.second));
                                    } else {
                                        sendNotification("Downloading Fail.", "", "", 1);
                                    }
                                    requestDownlaod();
                                },
                                throwable -> {
                                    requestDownlaod();
                                    sendNotification("Downloading Fail.", "", "", 1);
                                    throwable.printStackTrace();
                                });

            } catch (Exception e) {
                sendNotification("Downloading Fail.", "", "", 1);
                e.printStackTrace();
            }
        } else {
            sendNotification("Download complete.", "", "");
            ClipBoard.clear();
            stopSelf();
            mCurrentPath = "";
        }
    }

    public boolean deleteFile(File file) {
        File[] fileList = file.listFiles();
        boolean success = true;
        for (File temp : fileList) {
            if (temp.isDirectory()) {
                success = deleteFile(temp);
                if (!success) {
                    break;
                }
                temp.delete();
            } else {
                if (temp.delete()) {
//                    ArrayList<FileItem> tempList = MainActivity.mMds.selectTagFiles(temp.getPath());
//                    for(FileItem sub : tempList){
//                        MainActivity.mMds.deleteFile(sub.getPath());
//                    }
                    MainActivity.mMds.deleteFile(temp.getPath());
                } else {
                    success = false;
                    break;
                }
            }
        }
        return success;
    }

    public void sendNotification(String msg, String path, String name) {
        sendNotification(msg, path, name, 0);
    }

    public void sendNotification(String msg, String path, String name, int id) {

        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        msg += name;

        Intent intent = new Intent(this, IntroActivity.class);
        intent.putExtra("NO_ACTION", true);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setTicker(msg)
                        .setContentText(msg)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setAutoCancel(true);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(id, mBuilder.build());
    }
}
