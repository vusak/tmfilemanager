package jiran.com.tmfilemanager.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.IntroActivity;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.common.ClipBoard;
import jiran.com.tmfilemanager.common.MimeTypes;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.network.JMF_network;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by user on 2016-09-06.
 */
public class FileUploadService extends Service {

    public static ArrayList<FileItem> uploadFileList;
    public static String mTeam_id;
    public static String mObject_id;
    private static Stack<FileItem> listStack;
    private static int num = 1;
    private NotificationManager mNotificationManager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.d("FileUploadService", "onCreate");
        super.onCreate();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        Log.d("FileUploadService", "onStart");
        super.onStart(intent, startId);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("FileUploadService", "onStartCommand");
        num = 1;
        if (uploadFileList != null && uploadFileList.size() > 0) {
            listStack = new Stack<>();
            for (int i = uploadFileList.size() - 1; i >= 0; i--) {
                listStack.add(uploadFileList.get(i));
            }
            //sendNotification("Uploading...", "", "", 0);
            showNotification("Uploading...", "", "", 0);
            requestUpload();
        } else {
            stopSelf();
            mTeam_id = "";
            mObject_id = "";
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.d("FileUploadService", "onDestroy");
        mTeam_id = "";
        mObject_id = "";
        super.onDestroy();
    }

    public void requestUpload() {
        if (listStack != null && listStack.size() > 0) {
            FileItem currentItem = listStack.get(listStack.size() - 1);
            listStack.pop();

            try {
                Log.i("JMF requestUpload", "team_id => " + mTeam_id + " object_id => " + mObject_id);

//                List<MultipartBody.Part> parts= new ArrayList();
//                for(FileItem fileItem : fileItems){
//                    File file = new File(currentItem.getPath());
//
//                    RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
//                    MultipartBody.Part part = MultipartBody.Part.createFormData("filedata", file.getName(), requestBody);
//                    parts.add(part);
//                }

                List<MultipartBody.Part> parts = new ArrayList();
                File file = new File(currentItem.getPath());

                String mime = MimeTypes.getMimeType(file);
                RequestBody requestBody;
                if (mime != null) {
                    requestBody = RequestBody.create(MediaType.parse(mime), file);
                } else {
                    requestBody = RequestBody.create(MediaType.parse("*/*"), file);
                }

                MultipartBody.Part part = MultipartBody.Part.createFormData("filedata", file.getName(), requestBody);
                parts.add(part);

                JMF_network.fileUpload(mTeam_id, mObject_id, parts)
                        .flatMap(gigaPodPair -> {
                            return Observable.just(gigaPodPair);
                        })
                        .doOnError(throwable -> {

                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(gigaPodPair -> {
                                    if (gigaPodPair.first) {
                                        Log.i("JMF fileUpload", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
//                                    mBuilder.setContentText("Upload complete");
//                                    // Removes the progress bar
//                                    mBuilder.setProgress(0, 0, false);
//                                    mNotifyManager.notify(0, mBuilder.build());

                                        if (ClipBoard.isMove()) {
                                            File select = new File(currentItem.getPath());
                                            if (select != null && select.exists()) {
                                                if (select.isDirectory()) {
                                                    deleteFile(select);
                                                    select.delete();
                                                } else {
                                                    if (select.delete()) {
                                                        MainActivity.mMds.deleteFile(currentItem.getPath());
                                                    }
                                                }
                                            }
                                        }
                                        int current = uploadFileList.size() - listStack.size();
                                        showNotification("Upload => (" + current + "/" + uploadFileList.size() + ")" + currentItem.getName(), "", "", 0);
                                    } else {
                                        sendNotification("Upload Fail.=> " + currentItem.getName(), "", "");
                                    }
                                    requestUpload();
                                },
                                throwable -> {
                                    sendNotification("Upload Fail.=> " + currentItem.getName(), "", "");
                                    requestUpload();
                                    throwable.printStackTrace();
                                });
            } catch (Exception e) {

                e.printStackTrace();
            }
        } else {
            //sendNotification("Upload complete", "", "", 0);
            //showNotification("Upload complete", "", "", 0);
            ClipBoard.clear();
            stopSelf();
            mTeam_id = "";
            mObject_id = "";
        }
    }

    public boolean deleteFile(File file) {
        File[] fileList = file.listFiles();
        boolean success = true;
        for (File temp : fileList) {
            if (temp.isDirectory()) {
                success = deleteFile(temp);
                if (!success) {
                    break;
                }
                temp.delete();
            } else {
                if (temp.delete()) {
//                    ArrayList<FileItem> tempList = MainActivity.mMds.selectTagFiles(temp.getPath());
//                    for(FileItem sub : tempList){
//                        MainActivity.mMds.deleteFile(sub.getPath());
//                    }
                    MainActivity.mMds.deleteFile(temp.getPath());
                } else {
                    success = false;
                    break;
                }
            }
        }
        return success;
    }

    public void sendNotification(String msg, String path, String name) {
        //sendNotification(msg, path, name, num++);
        showNotification(msg, path, name, num++);
    }

    public void sendNotification(String msg, String path, String name, int id) {

        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        msg += name;

        Intent intent = new Intent(this, IntroActivity.class);
        intent.putExtra("path", path);

        //PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent , PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setTicker(msg)
                        .setContentText(msg)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setAutoCancel(true);

        //mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(id, mBuilder.build());
    }


    public void showNotification(String msg, String path, String name, int id) {
        Resources r = getResources();
        msg += name;

        //displayCustomNotificationForOrders(r.getString(R.string.app_name), msg);

        //PendingIntent pi = PendingIntent.getActivity(this, 0, new Intent(this, IntroActivity.class), 0);

        Notification notification = new NotificationCompat.Builder(this)
                .setTicker(r.getString(R.string.app_name))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(r.getString(R.string.app_name))
                .setContentText(msg)
                //.setContentIntent(pi)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .build();

        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationChannel channel = new NotificationChannel("notify_001",
//                    "Channel human readable title",
//                    NotificationManager.IMPORTANCE_DEFAULT);
//            mNotificationManager.createNotificationChannel(channel);

            NotificationChannel notificationChannel = new NotificationChannel("notify_001",
                    "Channel human readable title", NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setShowBadge(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }

        mNotificationManager.notify(id, notification);
    }


//    private void displayCustomNotificationForOrders(String title, String description) {
//        if (mNotificationManager == null) {
//            mNotificationManager = (NotificationManager) getSystemService
//                    (Context.NOTIFICATION_SERVICE);
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationCompat.Builder builder;
//            Intent intent = new Intent(this, IntroActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            PendingIntent pendingIntent;
//            int importance = NotificationManager.IMPORTANCE_HIGH;
//            if (mChannel == null) {
//                mChannel = new NotificationChannel
//                        ("0", title, importance);
//                mChannel.setDescription(description);
//                mChannel.enableVibration(true);
//                mNotificationManager.createNotificationChannel(mChannel);
//            }
//            builder = new NotificationCompat.Builder(this);
//
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
//                    Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            pendingIntent = PendingIntent.getActivity(this, 1251, intent, PendingIntent.FLAG_ONE_SHOT);
//            builder.setContentTitle(title)
//                    .setSmallIcon(getNotificationIcon()) // required
//                    .setContentText(description)  // required
//                    .setDefaults(Notification.DEFAULT_ALL)
//                    .setAutoCancel(true)
//                    .setLargeIcon(BitmapFactory.decodeResource
//                            (getResources(), R.mipmap.ic_launcher))
//                    //.setBadgeIconType(R.mipmap.ic_launcher)
//                    .setContentIntent(pendingIntent)
//                    .setSound(RingtoneManager.getDefaultUri
//                            (RingtoneManager.TYPE_NOTIFICATION));
//            Notification notification = builder.build();
//            mNotificationManager.notify(0, notification);
//        } else {
//
//            Intent intent = new Intent(this, IntroActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            PendingIntent pendingIntent = null;
//
//            pendingIntent = PendingIntent.getActivity(this, 1251, intent, PendingIntent.FLAG_ONE_SHOT);
//
//            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                    .setContentTitle(title)
//                    .setContentText(description)
//                    .setAutoCancel(true)
//                    .setColor(ContextCompat.getColor(getBaseContext(), R.color.colorPrimary))
//                    .setSound(defaultSoundUri)
//                    .setSmallIcon(getNotificationIcon())
//                    .setContentIntent(pendingIntent)
//                    .setStyle(new NotificationCompat.BigTextStyle().setBigContentTitle(title).bigText(description));
//
//            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(1251, notificationBuilder.build());
//        }
//    }
//
//    private int getNotificationIcon() {
//        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
//        return useWhiteIcon ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
//    }
}
