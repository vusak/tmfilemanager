package jiran.com.tmfilemanager.fagment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.InitActivity;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.activity.viewer.ImageSlideViewerActivity;
import jiran.com.tmfilemanager.adapter.CategoryAdapter;
import jiran.com.tmfilemanager.common.ClipBoard;
import jiran.com.tmfilemanager.common.FolderSelectTotalDialog;
import jiran.com.tmfilemanager.common.ListFunctionUtils;
import jiran.com.tmfilemanager.common.MimeTypes;
import jiran.com.tmfilemanager.common.SortUtils;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.CurrentItem;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.data.StorageListItem;
import jiran.com.tmfilemanager.data.TagItem;
import jiran.com.tmfilemanager.data.TeamItem;
import jiran.com.tmfilemanager.layout.FlowLayout;
import jiran.com.tmfilemanager.network.JMF_network;
import jiran.com.tmfilemanager.service.FileObserverService;
import jiran.com.tmfilemanager.settings.MyFileSettings;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by user on 2016-08-03.
 */


public class CategoryFragment extends Fragment {

    public static boolean mActionMode = false;
    public static TeamItem mTeam;
    public Context mMainContext;
    public int mListType = 0;
    public String mCurrentPath;
    public String mStartPath;
    public TagItem mTagItem;
    public String mSearch;
    public ArrayList<FileItem> mSearchArray;
    Dialog mDialog;
    HashMap<String, Integer> listPositionMap;
    LinearLayout top_tag_layout, top_category_layout;
    LinearLayout tag_content_list_layout;
    ImageView tag_contents_arrow;
    LinearLayout spinner_layout;
    TextView select_tag_name, select_tag_count;
    private AbsListView mListView;
    private GridView mGridView;

    //add Category Menu
    private RecyclerView mRecyclerView;
    //private LinearLayout mFoler_path_layout;
    //private TextView mFoler_path;
//    private HorizontalScrollView category_scroll_layout;
    private ImageView category_app_layout, category_image_layout, category_video_layout, category_music_layout, category_document_layout, category_zip_layout;
    private int mView_Type = 0;
    private CategoryAdapter mCategoryAdapter;
    private FlowLayout tag_content_layout;
    private Animation showAni, goneAni;


    public CategoryFragment() {
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public CategoryFragment(Context context, String path, boolean startBack) {
        mMainContext = context;
        mCurrentPath = path;
        if (startBack) {
            mStartPath = path;
        } else {
            mStartPath = null;
        }
        mView_Type = MainActivity.mViewType;
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public CategoryFragment(Context context, String path, boolean startBack, int viewType) {
        mMainContext = context;
        mCurrentPath = path;
        if (startBack) {
            mStartPath = path;
        } else {
            mStartPath = null;
        }
        mView_Type = viewType;
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public CategoryFragment(Context context, int type, TagItem item) {
        mMainContext = context;
        mListType = type;
        mTagItem = item;
        mView_Type = MainActivity.mViewType;
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public CategoryFragment(Context context, String currentPath, String search) {
        mMainContext = context;
        //mStartPath = currentPath;
        mCurrentPath = currentPath;
        mSearch = search;
        mView_Type = MainActivity.mViewType;
        setRetainInstance(true);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        CategoryAdapter.sdcardUse = Utils.isSDcardDirectory(true);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mMainContext == null) {
            Intent intent = new Intent(getActivity(), InitActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        }
        ((MainActivity) mMainContext).toolbar.setBackgroundResource(R.color.colorTitleBG);
        View rootView = inflater.inflate(R.layout.frag_category, container, false);
        initLayout(inflater, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        ((MainActivity) mMainContext).finishSearchMode();
        //((MainActivity)mMainContext).toolbarMenu(R.menu.main);
        if (mListType == 1) {
            ((MainActivity) getActivity()).setTitle(getString(R.string.left_menu_tag));
        }
        //getActivity().setTitle(mTagItem.getName());
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void initLayout(LayoutInflater inflater, View rootView) {
        //final MainActivity context = (MainActivity) getActivity();
        top_tag_layout = (LinearLayout) rootView.findViewById(R.id.top_tag_layout);
        top_category_layout = (LinearLayout) rootView.findViewById(R.id.top_category_layout);
        tag_content_list_layout = (LinearLayout) rootView.findViewById(R.id.tag_content_list_layout);
        tag_contents_arrow = (ImageView) rootView.findViewById(R.id.tag_contents_arrow);
        tag_content_layout = (FlowLayout) rootView.findViewById(R.id.tag_content_layout);
        select_tag_name = (TextView) rootView.findViewById(R.id.select_tag_name);
        select_tag_count = rootView.findViewById(R.id.select_tag_count);

        spinner_layout = (LinearLayout) rootView.findViewById(R.id.spinner_layout);

        if (mListType == 1) {
            top_tag_layout.setVisibility(View.VISIBLE);
            top_category_layout.setVisibility(View.GONE);
            spinner_layout.setVisibility(View.GONE);
            tag_contents_arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tag_content_layout.getVisibility() == View.VISIBLE) {
                        goneAni = AnimationUtils.loadAnimation(mMainContext, R.anim.main_top_info_slide_up);
                        tag_content_layout.setAnimation(goneAni);
                        tag_content_layout.setVisibility(View.GONE);
                        tag_contents_arrow.setImageResource(R.mipmap.list_dropdown);

//                        ExpandCollapseAnimation animation = null;
//                        animation = new ExpandCollapseAnimation(tag_content_list_layout, 200, 1, getActivity());
//                        tag_content_list_layout.startAnimation(animation);
                    } else {
                        showAni = AnimationUtils.loadAnimation(mMainContext, R.anim.main_top_info_slide_down);
                        tag_content_layout.setVisibility(View.VISIBLE);
                        tag_content_layout.setAnimation(showAni);
                        tag_contents_arrow.setImageResource(R.mipmap.list_dropup);

//                        ExpandCollapseAnimation animation = null;
//                        animation = new ExpandCollapseAnimation(tag_content_list_layout, 200, 0, getActivity());
//                        tag_content_list_layout.startAnimation(animation);
                    }
                }
            });
            tag_content_layout.setVisibility(View.GONE);
            displayTagList(false);
        } else {
            top_tag_layout.setVisibility(View.GONE);
            top_category_layout.setVisibility(View.VISIBLE);
            spinner_layout.setVisibility(View.VISIBLE);
        }

        listPositionMap = new HashMap<String, Integer>();

//        category_scroll_layout = (HorizontalScrollView) rootView.findViewById(R.id.category_scroll_layout);
        //mFoler_path_layout =  (LinearLayout) rootView.findViewById(R.id.foler_path_layout);
        category_app_layout = (ImageView) rootView.findViewById(R.id.category_app_layout);
        category_app_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCurrentPath = getResources().getString(R.string.category_app);
                setCategoryIcon();
                if (mView_Type == 0) {
                    changeListView(false);
                } else {
                    changeGridView(false);
                }
            }
        });
        category_image_layout = (ImageView) rootView.findViewById(R.id.category_image_layout);
        category_image_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCurrentPath = getResources().getString(R.string.category_image);
                setCategoryIcon();
                if (mView_Type == 0) {
                    changeListView(false);
                } else {
                    changeGridView(false);
                }
            }
        });
        category_video_layout = (ImageView) rootView.findViewById(R.id.category_video_layout);
        category_video_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCurrentPath = getResources().getString(R.string.category_video);
                setCategoryIcon();
                if (mView_Type == 0) {
                    changeListView(false);
                } else {
                    changeGridView(false);
                }
            }
        });
        category_music_layout = (ImageView) rootView.findViewById(R.id.category_music_layout);
        category_music_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCurrentPath = getResources().getString(R.string.category_music);
                setCategoryIcon();
                if (mView_Type == 0) {
                    changeListView(false);
                } else {
                    changeGridView(false);
                }
            }
        });
        category_document_layout = (ImageView) rootView.findViewById(R.id.category_document_layout);
        category_document_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCurrentPath = getResources().getString(R.string.category_document);
                setCategoryIcon();
                if (mView_Type == 0) {
                    changeListView(false);
                } else {
                    changeGridView(false);
                }
            }
        });
        category_zip_layout = (ImageView) rootView.findViewById(R.id.category_zip_layout);
        category_zip_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCurrentPath = getResources().getString(R.string.category_zip);
                setCategoryIcon();
                if (mView_Type == 0) {
                    changeListView(false);
                } else {
                    changeGridView(false);
                }
            }
        });

        Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner);
        ImageView spinner_arrow = (ImageView) rootView.findViewById(R.id.spinner_arrow);
        int i = 0;
        String[] paths;
        //String[]paths = {getResources().getString(R.string.category_loacl_storage), getResources().getString(R.string.category_team_storage)};
        if (MultiFragment.teamItams != null && MultiFragment.teamItams.size() > 0) {
            paths = new String[MultiFragment.teamItams.size() + 1];
            paths[i++] = getResources().getString(R.string.category_loacl_storage);
            for (TeamItem team : MultiFragment.teamItams) {
                paths[i++] = team.getTeamName();
            }
        } else {
            paths = new String[1];
            paths[0] = getResources().getString(R.string.category_loacl_storage);
            spinner_arrow.setVisibility(View.GONE);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mMainContext,
                R.layout.frag_folder_path_item_spinner_tv, paths) {
            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
//                tv.setBackgroundResource(R.color.main_my_storage_bg);
//                // Set the Text color
//                tv.setTextColor(Color.parseColor("#FF414141"));
//                tv.setTextSize(20);

                return view;
                //return super.getDropDownView(position, convertView, parent);
            }
        };
        adapter.setDropDownViewResource(R.layout.frag_folder_path_item_spinner_tv);
        spinner.setAdapter(adapter);
        SpinnerInteractionListener listener = new SpinnerInteractionListener();
        spinner.setOnTouchListener(listener);
        spinner.setOnItemSelectedListener(listener);

        spinner.setSelection(0);
        if (MultiFragment.teamItams != null && MultiFragment.teamItams.size() > 0) {
            spinner.setEnabled(true);
        } else {
            spinner.setEnabled(false);
        }


        mListView = (ListView) rootView.findViewById(R.id.folder_listview);
        mListView.setEmptyView(rootView.findViewById(R.id.empty_layout));
        mGridView = (GridView) rootView.findViewById(R.id.folder_gridview);
        mGridView.setEmptyView(rootView.findViewById(R.id.empty_layout));

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.folder_recyclerview);

        if (mView_Type == 0) {
            changeListView(false);
        } else {
            changeGridView(false);
        }

        //bottom menu
        finishActionMode();
        setCategoryIcon();
    }

    public void setCategoryIcon() {
        category_image_layout.setImageResource(mCurrentPath.equals(getString(R.string.category_image)) ? R.drawable.ic_history_circle_peacock_32 : R.drawable.ic_history_off_circle_peacock_32);
        category_video_layout.setImageResource(mCurrentPath.equals(getString(R.string.category_video)) ? R.drawable.ic_video_circle_navy_32 : R.drawable.ic_video_off_circle_navy_32);
        category_music_layout.setImageResource(mCurrentPath.equals(getString(R.string.category_music)) ? R.drawable.ic_music_circle_pink_32 : R.drawable.ic_music_off_circle_pink_32);
        category_document_layout.setImageResource(mCurrentPath.equals(getString(R.string.category_document)) ? R.drawable.ic_document_circle_blue_32 : R.drawable.ic_document_off_circle_blue_32);
        category_app_layout.setImageResource(mCurrentPath.equals(getString(R.string.category_app)) ? R.drawable.ic_app_circle_green_32 : R.drawable.ic_app_off_circle_green_32);
        category_zip_layout.setImageResource(mCurrentPath.equals(getString(R.string.category_zip)) ? R.drawable.ic_zip_circle_red_32 : R.drawable.ic_zip_off_circle_red_32);
    }

    public void changeListGridView() {
        mCategoryAdapter.listChange = true;
        if (mView_Type == 0) {
            MainActivity.mViewType = 1;
            listPositionMap.put(mCurrentPath, mListView.getFirstVisiblePosition());
            changeGridView(true);
        } else {
            MainActivity.mViewType = 0;
            listPositionMap.put(mCurrentPath, mGridView.getFirstVisiblePosition());
            changeListView(true);
        }
    }

    public void changeGridView() {
        if (mView_Type == 0) {
            mCategoryAdapter.listChange = true;
            MainActivity.mViewType = 1;
            listPositionMap.put(mCurrentPath, mListView.getFirstVisiblePosition());
            changeGridView(true);
        }
    }

    public void changeListView() {
        if (mView_Type == 1) {
            mCategoryAdapter.listChange = true;
            MainActivity.mViewType = 0;
            listPositionMap.put(mCurrentPath, mGridView.getFirstVisiblePosition());
            changeListView(true);
        }
    }

    private void changeGridView(boolean listChange) {
        //final MainActivity context = (MainActivity) getActivity();
        if (mListType == 1) {
            ((MainActivity) getActivity()).setTitle(mTagItem.getName());
        } else {
            ((MainActivity) getActivity()).setTitle(mCurrentPath);
        }
        ((MainActivity) getActivity()).setSelectedTitle("");
        mView_Type = 1;
        changeActionModeList();
        if (listChange) {
            mCategoryAdapter = new CategoryAdapter(getActivity(), null, mView_Type, mCategoryAdapter.getList());
        } else {
            mCategoryAdapter = new CategoryAdapter(getActivity(), null, mView_Type);
        }
        mGridView.setVisibility(View.VISIBLE);
        mListView.setVisibility(View.GONE);
        mGridView.setOnItemClickListener((parent, view, position, id) -> {
            if (mActionMode) {
                mCategoryAdapter.setCheck(position);
                mCategoryAdapter.notifyDataSetChanged();
                toolbarMenuIconChange();
                int count = mCategoryAdapter.getCheckCount();
                ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
            } else {
                final File file = new File(((FileItem) mGridView.getAdapter()
                        .getItem(position)).getPath());

                if (file.isDirectory()) {
                    mSearch = null;
                    if ((mListType == 1 || mListType == 2) && mStartPath == null) {
                        mStartPath = file.getAbsolutePath();
                    }
                    //Scroll Position Set
                    if (mListType == 1) {
                        listPositionMap.put(mTagItem.getName(), mGridView.getFirstVisiblePosition());
                    } else if (mListType == 2) {
                        listPositionMap.put(getResources().getString(R.string.category_favorite), mGridView.getFirstVisiblePosition());
                    } else {
                        listPositionMap.put(mCurrentPath, mGridView.getFirstVisiblePosition());
                    }
                    folderListDisplay(file.getAbsolutePath());
                    // go to the top of the ListView
                    //mGridView.setSelection(0);
                } else {
                    //if ( MimeTypes.isPicture(file)) {
                    if (MimeTypes.isPicture(file) || MimeTypes.isPdf(file) || MimeTypes.isVideo(file)) {
                        Intent intent = new Intent(mMainContext, ImageSlideViewerActivity.class);
                        ImageSlideViewerActivity.mDataSource = mCategoryAdapter.getImageList();
                        ImageSlideViewerActivity.mPosition = 0;
                        for (FileItem item : ImageSlideViewerActivity.mDataSource) {
                            if (item.getPath().equals(file.getPath())) {
                                break;
                            } else {
                                ImageSlideViewerActivity.mPosition++;
                            }
                        }
                        ((MainActivity) mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_SLIDE_VIEWER);
                    } else {
                        Utils.openFile(mMainContext, file);
                    }
                }
            }
//                {
//                    final File file = new File(((FileItem) mGridView.getAdapter()
//                            .getItem(position)).getPath());
//
//
//                    if ( MimeTypes.isPicture(file)) {
//                        Intent intent = new Intent(mMainContext, ImageSlideViewerActivity.class);
//                        ImageSlideViewerActivity.mDataSource = mCategoryAdapter.getImageList();
//                        ImageSlideViewerActivity.mPosition = 0;
//                        for(FileItem item : ImageSlideViewerActivity.mDataSource){
//                            if(item.getPath().equals(file.getPath())){
//                                break;
//                            }else{
//                                ImageSlideViewerActivity.mPosition++;
//                            }
//                        }
//                        ((MainActivity)mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_SLIDE_VIEWER);
//                    }else {
//                        Utils.openFile(mMainContext, file);
//                    }
//
//                }
        });

        mGridView.setOnItemLongClickListener((parent, view, position, id) -> {
            mCategoryAdapter.setCheck(position);
            int count = mCategoryAdapter.getCheckCount();
            ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
            setActionMode();
            toolbarMenuIconChange();
            return true;
        });
        mGridView.setAdapter(mCategoryAdapter);

        if (mSearch != null && mSearch.length() > 0) {
            folderSearchListDisplay(mCurrentPath, mSearch);
        } else if (mListType == 1) {
            folderTagListDisplay(mTagItem);
        } else if (mListType == 2) {
            folderFavoriteListDisplay(getResources().getString(R.string.category_favorite));
        } else {
            folderListDisplay(mCurrentPath);
        }
//        mFolderAdapter.addFiles(mCurrentPath);
//        mFolderAdapter.notifyDataSetChanged();
    }


    private void changeListView(boolean listChange) {
        //final MainActivity context = (MainActivity) getActivity();
        if (mListType == 1) {
            //getActivity().setTitle(mTagItem.getName());
        } else {
            ((MainActivity) getActivity()).setTitle(mCurrentPath);
        }
        ((MainActivity) getActivity()).setSelectedTitle("");
        mView_Type = 0;
        changeActionModeList();
        if (listChange) {
            mCategoryAdapter = new CategoryAdapter(getActivity(), null, mView_Type, mCategoryAdapter.getList());
        } else {
            mCategoryAdapter = new CategoryAdapter(getActivity(), null, mView_Type);
        }
        mListView.setVisibility(View.VISIBLE);
        mGridView.setVisibility(View.GONE);
        mListView.setOnItemClickListener((parent, view, position, id) -> {
            if (mActionMode) {
                mCategoryAdapter.setCheck(position);
                mCategoryAdapter.notifyDataSetChanged();
                toolbarMenuIconChange();
                int count = mCategoryAdapter.getCheckCount();
                ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
            } else {
                final File file = new File(((FileItem) mListView.getAdapter()
                        .getItem(position)).getPath());

                if (file.isDirectory()) {
                    mSearch = null;
                    if ((mListType == 1 || mListType == 2) && mStartPath == null) {
                        mStartPath = file.getAbsolutePath();
                    }
                    //Scroll Position Set
                    if (mListType == 1) {
                        listPositionMap.put(mTagItem.getName(), mListView.getFirstVisiblePosition());
                    } else if (mListType == 2) {
                        listPositionMap.put(getResources().getString(R.string.category_favorite), mListView.getFirstVisiblePosition());
                    } else {
                        listPositionMap.put(mCurrentPath, mListView.getFirstVisiblePosition());
                    }
                    folderListDisplay(file.getAbsolutePath());
                    // go to the top of the ListView
                    //mListView.setSelection(0);
                } else {
                    //if ( MimeTypes.isPicture(file)) {
                    if (MimeTypes.isPicture(file) || MimeTypes.isPdf(file) || MimeTypes.isVideo(file)) {
                        Intent intent = new Intent(mMainContext, ImageSlideViewerActivity.class);
                        ImageSlideViewerActivity.mDataSource = mCategoryAdapter.getImageList();
                        ImageSlideViewerActivity.mPosition = 0;
                        for (FileItem item : ImageSlideViewerActivity.mDataSource) {
                            if (item.getPath().equals(file.getPath())) {
                                break;
                            } else {
                                ImageSlideViewerActivity.mPosition++;
                            }
                        }
                        ((MainActivity) mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_SLIDE_VIEWER);
                    } else {
                        Utils.openFile(mMainContext, file);
                    }
                }
            }
//                {
//                    final File file = new File(((FileItem) mListView.getAdapter()
//                            .getItem(position)).getPath());
//
//
//                    if ( MimeTypes.isPicture(file)) {
//                        Intent intent = new Intent(mMainContext, ImageSlideViewerActivity.class);
//                        ImageSlideViewerActivity.mDataSource = mCategoryAdapter.getImageList();
//                        ImageSlideViewerActivity.mPosition = 0;
//                        for(FileItem item : ImageSlideViewerActivity.mDataSource){
//                            if(item.getPath().equals(file.getPath())){
//                                break;
//                            }else{
//                                ImageSlideViewerActivity.mPosition++;
//                            }
//                        }
//                        ((MainActivity)mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_SLIDE_VIEWER);
//                    }else {
//                        Utils.openFile(mMainContext, file);
//                    }
//
//                }
        });

        mListView.setOnItemLongClickListener((parent, view, position, id) -> {
            mCategoryAdapter.setCheck(position);
            int count = mCategoryAdapter.getCheckCount();
            ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
            setActionMode();
            toolbarMenuIconChange();
            return true;
        });
        mListView.setAdapter(mCategoryAdapter);
        if (mSearch != null && mSearch.length() > 0) {
            folderSearchListDisplay(mCurrentPath, mSearch);
        } else if (mListType == 1) {
            folderTagListDisplay(mTagItem);
        } else if (mListType == 2) {
            folderFavoriteListDisplay(getResources().getString(R.string.category_favorite));
        } else {
            folderListDisplay(mCurrentPath);
        }
//        mFolderAdapter.addFiles(mCurrentPath);
//        mFolderAdapter.notifyDataSetChanged();
    }

    public void setScrollPosition(String path) {
        //Scroll Position Set
        int position;
        if (listPositionMap.containsKey(path)) {
            position = listPositionMap.get(path);
            listPositionMap.remove(path);
        } else {
            position = 0;
        }

        if (mView_Type == 0) {
            mListView.setSelection(position);
        } else {
            mGridView.setSelection(position);
        }
    }

    public void folderListDisplay(String path) {
        mCurrentPath = path;
//        category_scroll_layout.post(new Runnable() {
//            public void run() {
//            }
//        });

        if (mCurrentPath.equals(getResources().getString(R.string.category_document)) ||
                mCurrentPath.equals(getResources().getString(R.string.category_app)) ||
                mCurrentPath.equals(getResources().getString(R.string.category_zip)) ||
                mCurrentPath.equals(getResources().getString(R.string.category_recentfile)) ||
                mCurrentPath.equals(getResources().getString(R.string.category_largefile))) {
            if (mCategoryAdapter.listChange) {
                mCategoryAdapter.listChange = false;
                mCategoryAdapter.notifyDataSetChanged();
            } else {
                if (mCurrentPath.equals(getResources().getString(R.string.category_document)) && ListFunctionUtils.mTempDocuContent.size() > 0) {
                    SortUtils.sortList(ListFunctionUtils.mTempDocuContent, mCurrentPath, MyFileSettings.getSortType());
                    mCategoryAdapter.addContent(ListFunctionUtils.mTempDocuContent, mCurrentPath);
                    mCategoryAdapter.notifyDataSetChanged();
                } else if (mCurrentPath.equals(getResources().getString(R.string.category_app)) && ListFunctionUtils.mTempAppContent.size() > 0) {
                    SortUtils.sortList(ListFunctionUtils.mTempAppContent, mCurrentPath, MyFileSettings.getSortType());
                    mCategoryAdapter.addContent(ListFunctionUtils.mTempAppContent, mCurrentPath);
                    mCategoryAdapter.notifyDataSetChanged();
                } else if (mCurrentPath.equals(getResources().getString(R.string.category_zip)) && ListFunctionUtils.mTempZipContent.size() > 0) {
                    SortUtils.sortList(ListFunctionUtils.mTempZipContent, mCurrentPath, MyFileSettings.getSortType());
                    mCategoryAdapter.addContent(ListFunctionUtils.mTempZipContent, mCurrentPath);
                    mCategoryAdapter.notifyDataSetChanged();
                } else if (mCurrentPath.equals(getResources().getString(R.string.category_recentfile)) && ListFunctionUtils.mTempRecentContent.size() > 0) {
                    SortUtils.sortList(ListFunctionUtils.mTempRecentContent, mCurrentPath, MyFileSettings.getSortType());
                    mCategoryAdapter.addContent(ListFunctionUtils.mTempRecentContent, mCurrentPath);
                    mCategoryAdapter.notifyDataSetChanged();
                } else if (mCurrentPath.equals(getResources().getString(R.string.category_largefile)) && ListFunctionUtils.mTempHugeContent.size() > 0) {
                    SortUtils.sortList(ListFunctionUtils.mTempHugeContent, mCurrentPath, MyFileSettings.getSortType());
                    mCategoryAdapter.addContent(ListFunctionUtils.mTempHugeContent, mCurrentPath);
                    mCategoryAdapter.notifyDataSetChanged();
                } else {
                    LoadingTask mTask = new LoadingTask(getActivity(), mCurrentPath);
                    mTask.execute("");
                }
            }
        } else {
            mCategoryAdapter.addFiles(path, 0);
        }
        //Scroll Position Set
        setScrollPosition(path);
    }

    public void folderTagListDisplay(TagItem tag) {
        //mFoler_path.setText(tag.getName());
        mCurrentPath = "/";
//        category_scroll_layout.post(new Runnable() {
//            public void run() {
//            }
//        });

        if (tag != null) {
            mCategoryAdapter.addTagFiles(tag);

            //Scroll Position Set
            setScrollPosition(tag.getName());
        } else {
            onBackPressed();
        }

    }

    public void folderSearchListDisplay(String currentPath, String search) {
        //mFoler_path.setText(title);
//        category_scroll_layout.post(new Runnable() {
//            public void run() {
//            }
//        });
        if (mSearchArray != null) {
            mCategoryAdapter.addContent(mSearchArray, currentPath);
            mCategoryAdapter.notifyDataSetChanged();
        } else {
            SearchTask mTask = new SearchTask(getActivity(), currentPath);
            mTask.execute(search);
        }
    }

    public void folderFavoriteListDisplay(String title) {
        //mFoler_path.setText(title);
        mCurrentPath = "/";
//        category_scroll_layout.post(new Runnable() {
//            public void run() {
//            }
//        });
        mCategoryAdapter.addFavoriteFiles();

        //Scroll Position Set
        setScrollPosition(title);
    }

    public void toolbarMenuIconChange() {
        ArrayList<FileItem> items = mCategoryAdapter.getCheckList();
        getActivity().setTitle(items.size() + " " + getResources().getString(R.string.selected));
        boolean allfiles = true;
        boolean allfavorite = true;
        if (items != null && items.size() > 0) {
            for (FileItem item : items) {
                if (allfiles) {
                    File file = new File(item.getPath());
                    if (file.isDirectory()) {
                        allfiles = false;
                    }
                }
                if (allfavorite) {
                    if (!item.getFavorite()) {
                        allfavorite = false;
                    }
                }
            }
//            if(allhide){
//                ((MainActivity)mMainContext).toolbar.getMenu().getItem(0).setIcon(R.mipmap.ic_unhide);
//            }else{
//                ((MainActivity)mMainContext).toolbar.getMenu().getItem(0).setIcon(R.mipmap.ic_hide);
//            }

            if (items.size() == 1 && new File(items.get(0).getPath()).isFile()) {
                ((MainActivity) mMainContext).toolbar.getMenu().getItem(0).setVisible(true);
            } else {
                ((MainActivity) mMainContext).toolbar.getMenu().getItem(0).setVisible(false);
            }
            if (allfiles) {
                ((MainActivity) mMainContext).toolbar.getMenu().getItem(1).setVisible(true);
                ((MainActivity) mMainContext).toolbar.getMenu().getItem(4).setVisible(true);
            } else {
                ((MainActivity) mMainContext).toolbar.getMenu().getItem(1).setVisible(false);
                ((MainActivity) mMainContext).toolbar.getMenu().getItem(4).setVisible(false);
            }
            if (allfavorite) {
                ((MainActivity) mMainContext).toolbar.getMenu().getItem(5).setIcon(R.mipmap.ic_unfavorite);
            } else {
                ((MainActivity) mMainContext).toolbar.getMenu().getItem(5).setIcon(R.mipmap.ic_favorite);
            }
        } else {
            ((MainActivity) mMainContext).toolbar.getMenu().getItem(0).setVisible(false);
            ((MainActivity) mMainContext).toolbar.getMenu().getItem(1).setVisible(false);
            ((MainActivity) mMainContext).toolbar.getMenu().getItem(5).setIcon(R.mipmap.ic_favorite);
        }
    }

    public void setActionMode() {
        mActionMode = true;
        getActivity().setTitle("0 " + getResources().getString(R.string.selected));
        ((MainActivity) mMainContext).toolbar.getMenu().clear();
        ((MainActivity) mMainContext).toolbar.inflateMenu(R.menu.action_mode);
        ((MainActivity) mMainContext).finishSearchMode();
        ((MainActivity) mMainContext).multiSelectMode();
        mCategoryAdapter.notifyDataSetChanged();
    }

    public void finishActionMode() {
        if (mTagItem != null) {
            ((MainActivity) getActivity()).setTitle(mTagItem.getName());
        } else {
            ((MainActivity) getActivity()).setTitle(mCurrentPath);
        }

        mActionMode = false;
        changeActionModeList();
//        if(ClipBoard.isEmpty()){
//            list_bottom_menu_paste.setVisibility(View.GONE);
//        }else{
//            list_bottom_menu_paste.setVisibility(View.VISIBLE);
//        }
        ((MainActivity) mMainContext).finishMultiSelectMode();
        mCategoryAdapter.setCheckClear();
        mCategoryAdapter.notifyDataSetChanged();
    }

    public void changeActionModeList() {
        if (mMainContext != null && ((MainActivity) mMainContext).toolbar != null) {
            ((MainActivity) mMainContext).toolbar.getMenu().clear();
            ((MainActivity) mMainContext).toolbar.inflateMenu(R.menu.main_no_newfolder);
        }
    }

    public void listAllcheck() {
        if (mCategoryAdapter.isAllChecked()) {
            mCategoryAdapter.setCheckClear();
            mCategoryAdapter.notifyDataSetChanged();
        } else {
            mCategoryAdapter.setAllCheck();
            mCategoryAdapter.notifyDataSetChanged();
        }
        int count = mCategoryAdapter.getCheckCount();
        ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
    }

    public void listCheckClear() {
        mCategoryAdapter.setCheckClear();
        mCategoryAdapter.notifyDataSetChanged();
    }

    public boolean onBackPressed() {

        if (mActionMode) {
            finishActionMode();
            return true;
        } else if (mCurrentPath != null && mCurrentPath.equals("/")) {
            //getActivity().finish();
            ((MainActivity) mMainContext).initFragment();
            return false;
        } else {
            try {
                if (mSearch != null && mSearch.length() > 0) {
                    mSearch = null;
                    if (mCurrentPath.equals(Utils.getInternalDirectoryPath())) {
                        ((MainActivity) mMainContext).initFragment();
                    } else {
                        folderListDisplay(mCurrentPath);
                    }
                } else {
                    if (mStartPath != null && mStartPath.equals(mCurrentPath)) {
                        if (mListType == 1) {
                            folderTagListDisplay(mTagItem);
                        } else if (mListType == 2) {
                            folderFavoriteListDisplay(getResources().getString(R.string.category_favorite));
                        } else {
                            ((MainActivity) mMainContext).initFragment();
                        }
                    } else {
                        if (mCurrentPath.equals(Utils.getInternalDirectoryPath())) {
                            ((MainActivity) mMainContext).initFragment();
                        } else {
                            File file = new File(mCurrentPath);
                            folderListDisplay(file.getParent());
                        }
                    }
                }
            } catch (Exception e) {
                //getActivity().finish();
                ((MainActivity) mMainContext).initFragment();
                return false;
            }
            // get position of the previous folder in ListView
            //mListView.setSelection(mListAdapter.getPosition(file.getPath()));
            return true;
        }
    }

    public void shareMethod() {

        int check_count = mCategoryAdapter.getCheckCount();
        ArrayList<FileItem> items = mCategoryAdapter.getCheckList();
        if (items != null && items.size() > 0) {
            Utils.sendFiles(mMainContext, items);
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        finishActionMode();
    }

    public void secretMethod() {
        final int check_count = mCategoryAdapter.getCheckCount();
        final ArrayList<FileItem> items = mCategoryAdapter.getCheckList();
        if (items != null && items.size() > 0) {
            mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
            LayoutInflater inflater = LayoutInflater.from(mMainContext);
            View view = inflater.inflate(R.layout.dialog_base, null, false);
            TextView title = (TextView) view.findViewById(R.id.title);
            title.setText(getResources().getString(R.string.left_menu_secretfolder));
            TextView content = (TextView) view.findViewById(R.id.content);
            content.setText(getResources().getString(R.string.secretfolder_do_you_want_save_select_file_in_secretfolder_1) + check_count
                    + getResources().getString(R.string.secretfolder_do_you_want_save_select_file_in_secretfolder_2));
            TextView cancel = (TextView) view.findViewById(R.id.cancel);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                }
            });
            TextView ok = (TextView) view.findViewById(R.id.ok);
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (FileItem item : items) {
                        File from = new File(item.getPath());
//                        File directory = from.getParentFile();
//                        File to = new File(directory, item.getName().replaceFirst(".", ""));
//                        from.renameTo(to);
//                        item.setPath(directory.getPath() + File.separator + item.getName().replaceFirst(".", ""));
//                        item.setName(item.getName().replaceFirst(".", ""));
                        // 시스템으로부터 현재시간(ms) 가져오기
                        long now = System.currentTimeMillis();
                        Utils.moveToSecretDirectory(from, "." + Utils.stripFilenameExtension(from.getName()), mMainContext);
                    }
                    //Toast.makeText(mMainContext, check_count + getResources().getString(R.string.secretfolder_move_to_secretfolder), Toast.LENGTH_SHORT).show();
                    Toast.makeText(mMainContext, getResources().getString(R.string.secretfolder_move_to_secretfolder), Toast.LENGTH_SHORT).show();
                    listRefresh();
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                }
            });
            mDialog.setContentView(view);
            mDialog.show();
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        finishActionMode();
    }

    public void hideMethod() {
        int check_count = mCategoryAdapter.getCheckCount();
        ArrayList<FileItem> items = mCategoryAdapter.getCheckList();
        if (items != null && items.size() > 0) {
            boolean allHide = true;
            for (FileItem item : items) {
                if (!item.getName().startsWith(".")) {
                    allHide = false;
                    break;
                }
            }
            for (FileItem item : items) {
                if (allHide) {
                    File from = new File(item.getPath());
                    File directory = from.getParentFile();
                    File to = new File(directory, item.getName().replaceFirst(".", ""));
                    from.renameTo(to);
                    item.setPath(directory.getPath() + File.separator + item.getName().replaceFirst(".", ""));
                    item.setName(item.getName().replaceFirst(".", ""));
                } else {
                    if (!item.getName().startsWith(".")) {
                        File from = new File(item.getPath());
                        File directory = from.getParentFile();
                        File to = new File(directory, "." + item.getName());
                        from.renameTo(to);
                        item.setPath(directory.getPath() + File.separator + "." + item.getName());
                        item.setName("." + item.getName());
                    }
                }
            }
            if (allHide) {
                Toast.makeText(mMainContext, check_count + getResources().getString(R.string.snackbar_file_unhide), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mMainContext, check_count + getResources().getString(R.string.snackbar_file_hide), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        finishActionMode();
    }

    public void copyMethod(final boolean oriRemove) {
        int count = mCategoryAdapter.getCheckCount();

        if (count > 0) {
            ArrayList<FileItem> items = mCategoryAdapter.getCheckList();
//            boolean checkFolder = false;
//            for(FileItem item : items){
//                if(item.isDirectory()){
//                    checkFolder = true;
//                }
//            }
            if (oriRemove) {
                ClipBoard.cutMove(items);
//                Snackbar.make(viewPos, getResources().getString(R.string.snackbar_move), Snackbar.LENGTH_SHORT)
//                        .setAction("Action", null).show();
            } else {
                ClipBoard.cutCopy(items);
//                Snackbar.make(viewPos, getResources().getString(R.string.snackbar_copy), Snackbar.LENGTH_SHORT)
//                        .setAction("Action", null).show();
            }

            FolderSelectTotalDialog.ORI_FILE_TYPE = FolderSelectTotalDialog.LOCAL_MEMORY;
            FolderSelectTotalDialog selectDialog = new FolderSelectTotalDialog(getActivity(), R.style.Theme_TransparentBackground, getActivity(), !oriRemove, false);
            selectDialog.show();

        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        //finishActionMode();
    }

    public void favoriteMethod() {
        int check_count = mCategoryAdapter.getCheckCount();
        ArrayList<FileItem> items = mCategoryAdapter.getCheckList();
        if (items != null && items.size() > 0) {
            boolean allFavorite = true;
            for (FileItem item : items) {
                if (!item.getFavorite()) {
                    allFavorite = false;
                    break;
                }
            }
            for (FileItem item : items) {
                if (allFavorite) {
//                    if(item.getTagId() != null && item.getTagId().length() > 0){
//                        MainActivity.mMds.updateFileFavorite(item.getId(), "N");
//                    }else
                    {
                        MainActivity.mMds.deleteTag(item.getId());
                        item.setFavorite(false);
                        //mCategoryAdapter.removeFavoriteFiles(item);
                    }
                    item.setId("");
                } else {
//                    if(item.getFavorite() != null && item.getFavorite().length() == 0){
//                        MainActivity.mMds.updateFileFavorite(item.getId(), "Y");
//                    }else{
//                        MainActivity.mMds.insertFavoriteItem(item.getPath());
//                    }
                    item.setId(MainActivity.mMds.insertFavoriteItem(item.getPath()) + "");
                    item.setFavorite(true);
                }
            }

            if (allFavorite) {
                Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_file_unfavorite), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_file_favorite), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        mCategoryAdapter.notifyDataSetChanged();
        finishActionMode();
    }

    public void tagMethod(TagItem tag) {

        final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);

        ArrayList<FileItem> items = mCategoryAdapter.getCheckList();
        if (items != null && items.size() > 0) {
            for (FileItem item : items) {
                if (tag == null) {
                    MainActivity.mMds.deleteTag(item.getId());
                    item.setId("");
                } else {
                    item.setId(MainActivity.mMds.insertTagItem(tag.getName(), item.getPath()) + "");
                }
            }
//            Snackbar.make(viewPos, "태그", Snackbar.LENGTH_SHORT)
//                    .setAction("Action", null).show();
            mCategoryAdapter.refresh();
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        finishActionMode();
    }

    public void deleteMethod() {
        int check_count = mCategoryAdapter.getCheckCount();

        if (check_count > 0) {
            dialogDeleteFile(check_count);
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
            //finishActionMode();
        }
    }

    public void renameMethod() {
        int check_count = mCategoryAdapter.getCheckCount();
        if (check_count > 0) {
            dialogRename();
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
            //finishActionMode();
        }
    }

    public void dialogRename() {
        ArrayList<FileItem> items = mCategoryAdapter.getCheckList();
        FileItem item = items.get(0);
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        final View view = inflater.inflate(R.layout.dialog_new_folder, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.dialog_rename_file));
        final EditText edt = (EditText) view.findViewById(R.id.edt);
        edt.setText(Utils.stripFilenameExtension(item.getName()));
        edt.requestFocus();
        InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView create = (TextView) view.findViewById(R.id.create);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String folder_name = edt.getText().toString();
                if (folder_name != null && folder_name.length() > 0) {
                    File select = new File(item.getPath());
                    if (select.isDirectory()) {
                        //renameStorageFolder(item, folder_name);

                    } else {
                        //renameStorageFile(item, folder_name);
                        folder_name = folder_name + "." + Utils.getFilenameExtension(item.getName());
                    }

                    File to = new File(select.getParent(), folder_name);
                    if (select.renameTo(to)) {
                        item.setPath(to.getAbsolutePath());
                        item.setName(to.getName());
                        mCategoryAdapter.notifyDataSetChanged();
                    }

                    InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                } else {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_input_foldername), Toast.LENGTH_SHORT).show();
                }

            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    //    public void sendFileIDMethod() {
//        ArrayList<FileItem> tempList = mFolderAdapter.getCheckList();
//
//        if(tempList.size() > 0){
//            boolean isDirectory = false;
//            for(FileItem item : tempList){
//                File file = new File(item.getPath());
//                if(file.isDirectory()){
//                    isDirectory = true;
//                    break;
//                }
//            }
//            if(isDirectory){
//                View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
//                Snackbar.make(viewPos, getResources().getString(R.string.Dialog_Fileview_NoFolderRequest), Snackbar.LENGTH_SHORT)
//                        .setAction("Action", null).show();
//            }else{
//                FileIDActivity.selectSendFileIDList = mFolderAdapter.getCheckList();
//                Intent intent = new Intent(mMainContext, FileIDActivity.class);
//                ((MainActivity)mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_FILE_ID_SEND);
//            }
//        }else{
//            View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
//            Snackbar.make(viewPos, getResources().getString(R.string.snackbar_no_select), Snackbar.LENGTH_SHORT)
//                    .setAction("Action", null).show();
//        }
//    }
    public void sortDate() {
        if (MyFileSettings.getSortType() == SortUtils.SORT_DATE) {
            MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
        } else {
            MyFileSettings.setSortType(SortUtils.SORT_DATE);
        }
        mCategoryAdapter.reSort();
    }

    public void sortName() {
        if (MyFileSettings.getSortType() == SortUtils.SORT_ALPHA) {
            MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
        } else {
            MyFileSettings.setSortType(SortUtils.SORT_ALPHA);
        }
        mCategoryAdapter.reSort();
    }

    public void sortType() {
        if (MyFileSettings.getSortType() == SortUtils.SORT_TYPE) {
            MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
        } else {
            MyFileSettings.setSortType(SortUtils.SORT_TYPE);
        }
        mCategoryAdapter.reSort();
    }

    public void sortSize() {
        if (MyFileSettings.getSortType() == SortUtils.SORT_SIZE) {
            MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
        } else {
            MyFileSettings.setSortType(SortUtils.SORT_SIZE);
        }
        mCategoryAdapter.reSort();
    }

    public void dialogSetSort() {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.dialog_select_sort, null, false);
        LinearLayout sort_by_name = (LinearLayout) view.findViewById(R.id.sort_by_name);
        TextView sort_by_name_txt = (TextView) view.findViewById(R.id.sort_by_name_txt);
        if (MyFileSettings.getSortType() != SortUtils.SORT_ALPHA) {
            sort_by_name_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        } else {
            if (!MyFileSettings.getSortReverse()) {
                sort_by_name_txt.setText(sort_by_name_txt.getText().toString() + " ↑");
            } else {
                sort_by_name_txt.setText(sort_by_name_txt.getText().toString() + " ↓");
            }
        }
        sort_by_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                if (MyFileSettings.getSortType() == SortUtils.SORT_ALPHA) {
                    MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
                } else {
                    MyFileSettings.setSortType(SortUtils.SORT_ALPHA);
                }

                mCategoryAdapter.reSort();
            }
        });
        LinearLayout sort_by_extension = (LinearLayout) view.findViewById(R.id.sort_by_extension);
        TextView sort_by_extension_txt = (TextView) view.findViewById(R.id.sort_by_extension_txt);
        if (MyFileSettings.getSortType() != SortUtils.SORT_TYPE) {
            sort_by_extension_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        } else {
            if (!MyFileSettings.getSortReverse()) {
                sort_by_extension_txt.setText(sort_by_extension_txt.getText().toString() + " ↑");
            } else {
                sort_by_extension_txt.setText(sort_by_extension_txt.getText().toString() + " ↓");
            }
        }
        sort_by_extension.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                if (MyFileSettings.getSortType() == SortUtils.SORT_TYPE) {
                    MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
                } else {
                    MyFileSettings.setSortType(SortUtils.SORT_TYPE);
                }
                mCategoryAdapter.reSort();
            }
        });
        LinearLayout sort_by_date = (LinearLayout) view.findViewById(R.id.sort_by_date);
        TextView sort_by_date_txt = (TextView) view.findViewById(R.id.sort_by_date_txt);
        if (MyFileSettings.getSortType() != SortUtils.SORT_DATE) {
            sort_by_date_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        } else {
            if (!MyFileSettings.getSortReverse()) {
                sort_by_date_txt.setText(sort_by_date_txt.getText().toString() + " ↑");
            } else {
                sort_by_date_txt.setText(sort_by_date_txt.getText().toString() + " ↓");
            }
        }
        sort_by_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                if (MyFileSettings.getSortType() == SortUtils.SORT_DATE) {
                    MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
                } else {
                    MyFileSettings.setSortType(SortUtils.SORT_DATE);
                }
                mCategoryAdapter.reSort();
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void dialogDeleteFile(final int check_count) {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.dialog_base, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.dialog_delete_file));
        TextView content = (TextView) view.findViewById(R.id.content);
        content.setText(getResources().getString(R.string.dialog_delete_file_info1) + check_count + getResources().getString(R.string.dialog_delete_file_info2));
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<FileItem> items = mCategoryAdapter.getCheckList();

                boolean success = true;

                for (FileItem item : items) {
                    File select = new File(item.getPath());
                    if (select != null && select.exists()) {
                        if (select.isDirectory()) {
                            success = deleteFile(select);
                            if (!success) {
                                break;
                            }
                            select.delete();
                        } else {
                            if (select.delete()) {
//                                MainActivity.mMds.deleteTagPath(item.getPath());
//                                MainActivity.mMds.deleteFavorite(item.getFavoriteId());
                                MainActivity.mMds.deleteFile(item.getPath());
                            } else {
                                success = false;
                                break;
                            }
                        }
                    }
                }
                if (success) {
                    Toast.makeText(mMainContext, check_count + getResources().getString(R.string.snackbar_file_delete), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_delete_error), Toast.LENGTH_SHORT).show();
                }
                mCategoryAdapter.refresh();
                finishActionMode();

                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public boolean deleteFile(File file) {
        File[] fileList = file.listFiles();
        boolean success = true;
        for (File temp : fileList) {
            if (temp.isDirectory()) {
                success = deleteFile(temp);
                if (!success) {
                    break;
                }
                temp.delete();
            } else {
                if (temp.delete()) {
//                    ArrayList<FileItem> tempList = MainActivity.mMds.selectTagFiles(temp.getPath());
//                    for(FileItem sub : tempList){
//                        MainActivity.mMds.deleteFile(sub.getPath());
//                    }
                    MainActivity.mMds.deleteFile(temp.getPath());
                } else {
                    success = false;
                    break;
                }
            }
        }
        return success;
    }

    public void listRefresh() {
        //mFolderAdapter.refresh();
        if (ImageSlideViewerActivity.mDataSource != null && ImageSlideViewerActivity.mPosition >= 0) {
            FileItem item = ImageSlideViewerActivity.mDataSource.get(ImageSlideViewerActivity.mPosition);
            ArrayList<FileItem> items = mCategoryAdapter.getList();
            int pos = 0;
            for (FileItem temp : items) {
                if (item.getPath().equals(temp.getPath())) {
                    break;
                } else {
                    pos++;
                }
            }
            if (mView_Type == 0) {
                mListView.setSelection(pos);
            } else {
                mGridView.setSelection(pos);
            }
        }
        ImageSlideViewerActivity.mDataSource = mCategoryAdapter.getImageList();
        ;
        ImageSlideViewerActivity.mPosition = -1;

        finishActionMode();
    }

    public ArrayList<FileItem> getCheckList() {
        return mCategoryAdapter.getCheckList();
    }

    public void dialogSelectTag() {
        ArrayList<FileItem> items = mCategoryAdapter.getCheckList();
        if (items == null || items.size() == 0) {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
            return;
        }

        dialogNewTag(items.get(0).getPath());
    }


    public void dialogNewTag(String path) {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        final LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.dialog_new_tag, null, false);

        final FlowLayout dialog_tag_content_layout = (FlowLayout) view.findViewById(R.id.dialog_tag_content_layout);
        String tagName = "";
        final ArrayList<FileItem> tag = MainActivity.mMds.selectTagFiles(path);
        for (FileItem item : tag) {
            View tag_view = inflater.inflate(R.layout.dialog_tag_list_item, null, false);
            TextView tag_name = (TextView) tag_view.findViewById(R.id.tag_name);
            tag_name.setText(item.getTagName());
            tag_view.setTag(item.getTagName());
            tag_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog_tag_content_layout.removeView(v);
                }
            });
            dialog_tag_content_layout.addView(tag_view);
        }

        final EditText edt = (EditText) view.findViewById(R.id.edt);
        edt.requestFocus();
        InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView tag_add_bt = (TextView) view.findViewById(R.id.tag_add_bt);
        tag_add_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
                String edt_name = edt.getText().toString().trim();
                if (edt_name != null && edt_name.length() > 0) {
                    int count = dialog_tag_content_layout.getChildCount();
                    boolean duplicate = false;

                    if (count >= 10) {
                        Toast.makeText(mMainContext, "Max Count is 10...", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    for (int i = 0; i < count; i++) {
                        View childView = dialog_tag_content_layout.getChildAt(i);
                        if (((String) childView.getTag()).equals(edt_name)) {
                            Toast.makeText(mMainContext, "Duplicate Tag...", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }

                    View tag_view = inflater.inflate(R.layout.dialog_tag_list_item, null, false);
                    TextView tag_name = (TextView) tag_view.findViewById(R.id.tag_name);
                    tag_view.setTag(edt_name);
                    tag_name.setText(edt_name);
                    tag_view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog_tag_content_layout.removeView(v);
                        }
                    });
                    dialog_tag_content_layout.addView(tag_view);
                } else {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
                }
            }
        });


        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView create = (TextView) view.findViewById(R.id.create);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = dialog_tag_content_layout.getChildCount();
                if (count > 0) {
//                    int select = (int)edt.getTag();
//                    //Toast.makeText(mMainContext, tag_name + " => " + select, Toast.LENGTH_SHORT).show();
//                    MainActivity.mMds.insertTagItem(tag_name, select + "");
//                    ((MainActivity)mMainContext).displayTagList();
//
//                    ArrayList<TagItem> tagItams = MainActivity.mMds.getAllTag();
//                    for(TagItem item : tagItams){
//                        if(item.getName().equals(tag_name)){
//                            tagMethod(item);
//                            break;
//                        }
//                    }

                    ArrayList<FileItem> items = mCategoryAdapter.getCheckList();
                    if (items != null && items.size() > 0) {
                        for (FileItem item : items) {
                            for (FileItem tagItem : tag) {
                                MainActivity.mMds.deleteTag(tagItem.getId());
                            }
                            for (int i = 0; i < count; i++) {
                                View childView = dialog_tag_content_layout.getChildAt(i);
                                item.setId(MainActivity.mMds.insertTagItem((String) childView.getTag(), item.getPath()) + "");
                            }
                            item.setTagName("tag");
                        }
                        if (mCurrentPath.equals(getString(R.string.category_recentfile))) {
                            if (mView_Type == 0) {
                                changeListView(false);
                            } else {
                                changeGridView(false);
                            }
                        } else {
                            mCategoryAdapter.refresh();
                        }
                        Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_add_tag), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
                    }
                    finishActionMode();
                    InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                } else {
                    if (tag != null && tag.size() > 0) {
                        ArrayList<FileItem> items = mCategoryAdapter.getCheckList();
                        if (items != null && items.size() > 0) {
                            for (FileItem item : items) {
                                for (FileItem tagItem : tag) {
                                    MainActivity.mMds.deleteTag(tagItem.getId());
                                }
                                item.setTagName("");
                            }
                            if (mCurrentPath.equals(getString(R.string.category_recentfile))) {
                                if (mView_Type == 0) {
                                    changeListView(false);
                                } else {
                                    changeGridView(false);
                                }
                            } else {
                                mCategoryAdapter.refresh();
                            }
                            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_remove_tag), Toast.LENGTH_SHORT).show();
                        }
                        finishActionMode();
                        InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                        if (mDialog != null) {
                            mDialog.dismiss();
                        }
                    } else {
                        Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_input_tagname), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void dialogNewFolder() {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        final View view = inflater.inflate(R.layout.dialog_new_folder, null, false);
        final EditText edt = (EditText) view.findViewById(R.id.edt);
        edt.requestFocus();
        InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView create = (TextView) view.findViewById(R.id.create);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String folder_name = edt.getText().toString();
                if (folder_name != null && folder_name.length() > 0) {
                    File file = new File(mCurrentPath);
                    Log.e("readable->", "" + file.canRead());
                    Log.e("writable->", "" + file.canWrite());
                    Log.e("executable->", "" + file.canExecute());

                    String createPath = file.getAbsolutePath() + File.separator + folder_name;

                    File create = new File(createPath);

                    if (create.exists()) {
                        Toast.makeText(mMainContext, getResources().getString(R.string.overwrite_title_folder), Toast.LENGTH_SHORT).show();
                    } else {
                        //create.mkdirs();
                        if (create.mkdirs()) {
                            //success
                        } else {
                            //fail
                            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_folder_create_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                    listRefresh();

                    InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                } else {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_input_foldername), Toast.LENGTH_SHORT).show();
                }

            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void initSecretFolder() {
        //String password = PreferenceHelper.getString(PreferenceHelper.password, "");
        String password = MainActivity.mMds.getSecretUserPassword();
        if (password != null && password.length() > 0) {
            secretMethod();
        } else {
            mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
            LayoutInflater inflater = LayoutInflater.from(mMainContext);
            View view = inflater.inflate(R.layout.dialog_base, null, false);
            TextView title = (TextView) view.findViewById(R.id.title);
            title.setText(getResources().getString(R.string.left_menu_secretfolder));
            TextView content = (TextView) view.findViewById(R.id.content);
            content.setText(getResources().getString(R.string.secretfolder_activate_your_secretfolder_now));
            TextView cancel = (TextView) view.findViewById(R.id.cancel);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                    finishActionMode();
                }
            });
            TextView ok = (TextView) view.findViewById(R.id.ok);
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_frame, new SecretFolderFragment(mMainContext, Utils.getInternalDirectoryPath(), true))
                            .commit();
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                    finishActionMode();
                }
            });
            mDialog.setContentView(view);
            mDialog.show();
        }
    }

    public void displayTagList(final boolean delete) {
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        ArrayList<TagItem> tagItams = MainActivity.mMds.getAllTag();
        tag_content_layout.removeAllViews();
        if (tagItams != null && tagItams.size() > 0) {
            int totalCount = tagItams.size();
            for (int i = 0; i < totalCount; ) {
//            }
//            for(final TagItem item : tagItams){
                if (mTagItem == null && i == 0) {
                    mTagItem = tagItams.get(0);
                }

                final TagItem item_1 = tagItams.get(i++);
                View tag_view = inflater.inflate(R.layout.frag_multi_tag_list_item, null, false);

                TextView tag_name = (TextView) tag_view.findViewById(R.id.tag_name);
                TextView tag_count = tag_view.findViewById(R.id.tag_count);
                tag_name.setText(item_1.getName());
                tag_count.setText(item_1.getCount());
                if (mTagItem.getName().equals(item_1.getName())) {
                    //getActivity().setTitle(item_1.getName());
//                    tag_name.setBackgroundResource(R.drawable.round_tag_select_background);
//                    tag_name.setTextColor(Color.WHITE);
                    select_tag_name.setText(item_1.getName());
                    select_tag_count.setText(item_1.getCount());
                }
                tag_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTagItem = item_1;
                        displayTagList(false);
                        tag_content_layout.setVisibility(View.GONE);
                        if (mView_Type == 0) {
                            changeListView(false);
                        } else {
                            changeGridView(false);
                        }
                    }
                });
                tag_name.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
                        LayoutInflater inflater = LayoutInflater.from(mMainContext);
                        View view = inflater.inflate(R.layout.dialog_base, null, false);
                        TextView title = (TextView) view.findViewById(R.id.title);
                        title.setText(getResources().getString(R.string.dialog_delete_tag));
                        TextView content = (TextView) view.findViewById(R.id.content);
                        content.setText(getResources().getString(R.string.dialog_delete_tag_info));
                        TextView cancel = (TextView) view.findViewById(R.id.cancel);
                        cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (mDialog != null) {
                                    mDialog.dismiss();
                                }
                            }
                        });
                        TextView ok = (TextView) view.findViewById(R.id.ok);
                        ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (mDialog != null) {
                                    mDialog.dismiss();
                                }
                                ArrayList<FileItem> tagFiles = mCategoryAdapter.getTagFiles(item_1);
                                for (FileItem tagFile : tagFiles) {
                                    MainActivity.mMds.deleteTag(tagFile.getId());
                                }

                                if (mTagItem.getId().equals(item_1.getId())) {
                                    mTagItem = null;
                                    displayTagList(false);
                                    if (mView_Type == 0) {
                                        changeListView(false);
                                    } else {
                                        changeGridView(false);
                                    }
                                } else {
                                    displayTagList(false);
                                }

                            }
                        });
                        mDialog.setContentView(view);
                        mDialog.show();
                        return false;
                    }
                });
                tag_content_layout.addView(tag_view);
            }
        }
    }

    public void moveToStorageRoot(int position) {
        TeamItem selectItem = MultiFragment.teamItams.get(position);
        try {
            ((MainActivity) mMainContext).showProgress();
            JMF_network.getRootPath(selectItem.getId())
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        ((MainActivity) mMainContext).dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                if (gigaPodPair.first) {
                                    Log.i("JMF getRootPath", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    JsonObject current = result.getAsJsonObject("current");

                                    getStorageList(selectItem, current.get("object_id").getAsString(), "");
                                    //Toast.makeText(mMainContext, "object_id => " + object_id, Toast.LENGTH_SHORT).show();
                                } else {
                                    ((MainActivity) mMainContext).dismissProgress();
                                    Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            ((MainActivity) mMainContext).dismissProgress();
            e.printStackTrace();
            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void getStorageList(TeamItem team, String objectId, String offset) {
        try {
            ((MainActivity) mMainContext).showProgress();
            String sort = "";
            if (MyFileSettings.getSortReverse()) {
                sort = "+";
            } else {
                sort = "-";
            }
            switch (MyFileSettings.getSortType()) {
                case SortUtils.SORT_ALPHA:
                    sort += "name";
                    break;
                case SortUtils.SORT_TYPE:
                    sort += "extension";
                    break;
                case SortUtils.SORT_SIZE:
                    sort += "size";
                    break;
                case SortUtils.SORT_DATE:
                    sort += "regdate";
                    break;
                default:
                    sort += "name";
                    break;
            }
            String assort = "";
            if (mCurrentPath.equals(getResources().getString(R.string.category_app))) {
                assort = "application";
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_image))) {
                assort = "picture";
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_video))) {
                assort = "movie";
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_music))) {
                assort = "music";
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_document))) {
                assort = "document";
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_zip))) {
                assort = "archive";
            }

            String finalAssort = assort;
            JMF_network.getFolderList(team.getId(), objectId, sort, CategoryStorageFragment.LIST_LIMIT, offset, assort)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        ((MainActivity) mMainContext).dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF getStorageList", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");

                                    JsonObject current = result.getAsJsonObject("current");
                                    CurrentItem currentItem = new CurrentItem();
                                    currentItem.setObjectId(current.get("object_id").getAsString());
                                    currentItem.setNode(current.get("node").getAsString());
                                    currentItem.setName(current.get("name").getAsString());
                                    currentItem.setCategoryId("");
                                    currentItem.setSize(current.get("size").getAsString());
                                    currentItem.setRegdate(Utils.formatDate(current.get("regdate").getAsString()));
                                    currentItem.setModified(Utils.formatDate(current.get("modified").getAsString()));

                                    JsonObject lists = result.getAsJsonObject("lists");
                                    //isLastpage = lists.get("lastpage").getAsBoolean();
                                    JsonArray rows = lists.getAsJsonArray("rows");
                                    ArrayList<FileItem> storageList = new ArrayList<FileItem>();
                                    for (int i = 0; i < rows.size(); i++) {
                                        FileItem item = new FileItem();
                                        JsonObject row = rows.get(i).getAsJsonObject();
                                        item.setTeamId(team.getId());
                                        item.setId(row.get("object_id").getAsString());
                                        item.setSObjectType(row.get("object_type").getAsString());
                                        item.setSNode(row.get("node").getAsString());
                                        item.setName(row.get("name").getAsString());
                                        item.setSCreator(row.get("creator").getAsString());
                                        item.setSCreatorName(row.get("creator_name").getAsString());
                                        item.setSMemo(row.get("memo").getAsString());
                                        item.setSize(row.get("size").getAsString());
                                        item.setSExtension(row.get("extension").getAsString());
                                        item.setSThumbnail(row.get("thumbnail").getAsString());
                                        item.setSUrlPath(row.get("url_path").getAsString());
                                        item.setRegDate(Utils.formatDate(row.get("regdate").getAsString()));
                                        item.setSModifedDate(Utils.formatDate(row.get("modified").getAsString()));

                                        storageList.add(item);
                                    }

                                    StorageListItem storageListItem = new StorageListItem(currentItem, storageList);

                                    getActivity().getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.content_frame, new CategoryStorageFragment(mMainContext, mCurrentPath, team, storageListItem))
                                            .commit();
                                } else {
                                    Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            ((MainActivity) mMainContext).dismissProgress();
            e.printStackTrace();
            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    //Search Method
    private class SearchTask extends AsyncTask<String, Void, ArrayList<FileItem>> {
        private final Context context;
        private ProgressDialog pr_dialog;
        private String mCurrentPath;

        private String mSearchStr;

        private SearchTask(Context c, String currentPath) {
            context = c;
            File file = new File(currentPath);
            if (file != null && file.isDirectory()) {
                mCurrentPath = currentPath;
            } else {
                mCurrentPath = Utils.getInternalDirectoryPath();
            }
        }

        @Override
        protected void onPreExecute() {
            pr_dialog = ProgressDialog.show(context, null,
                    context.getResources().getString(R.string.action_search));
            pr_dialog.setCanceledOnTouchOutside(true);
        }

        @Override
        protected ArrayList<FileItem> doInBackground(String... params) {
            //String location = BrowserTabsAdapter.getCurrentBrowserFragment().mCurrentPath;
            mSearchStr = params[0];
            return Utils.searchInDirectory(mCurrentPath, mSearchStr);
        }

        @Override
        protected void onPostExecute(final ArrayList<FileItem> files) {
            int len = files != null ? files.size() : 0;
            mSearchArray = files;
            pr_dialog.dismiss();
            if (len == 0) {
                try {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_search_result_empty), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_search_result_empty), Toast.LENGTH_SHORT).show();
                }
            } else {
                SortUtils.sortList(files, mCurrentPath, MyFileSettings.getSortType());
                mCategoryAdapter.addContent(files, mCurrentPath);
                mCategoryAdapter.notifyDataSetChanged();
            }
            setScrollPosition(mSearchStr);
        }
    }

    private class LoadingTask extends AsyncTask<String, Void, ArrayList<FileItem>> {
        private final Context context;
        private ProgressDialog pr_dialog;
        private String mCurrentPath;

        private LoadingTask(Context c, String currentPath) {
            context = c;
            mCurrentPath = currentPath;
        }

        @Override
        protected void onPreExecute() {
            pr_dialog = ProgressDialog.show(context, null,
                    context.getResources().getString(R.string.action_loading));
            pr_dialog.setCanceledOnTouchOutside(true);
        }

        @Override
        protected ArrayList<FileItem> doInBackground(String... params) {
            //String location = BrowserTabsAdapter.getCurrentBrowserFragment().mCurrentPath;

            if (mCurrentPath.equals(getResources().getString(R.string.category_document))) {
                return ListFunctionUtils.listAllDocumentFiles(context);
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_app))) {
                return ListFunctionUtils.listAllAPKFiles(context);
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_zip))) {
                return ListFunctionUtils.listAllZipFiles(context);
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_recentfile))) {
                return ListFunctionUtils.listDaysFiles(context, ListFunctionUtils.LIST_DAY);
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_largefile))) {
                return ListFunctionUtils.listHugeFiles(context, FileObserverService.NEW_FILE_SIZE_LIMIT_100MB);
            } else {                return Utils.searchInDirectory(mCurrentPath, params[0]);
            }
        }

        @Override
        protected void onPostExecute(final ArrayList<FileItem> files) {
            int len = files != null ? files.size() : 0;
            if (pr_dialog != null && pr_dialog.isShowing()) {
                pr_dialog.dismiss();
            }
            if (len == 0) {
                Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_search_result_empty), Toast.LENGTH_SHORT).show();
            } else {
                SortUtils.sortList(files, mCurrentPath, MyFileSettings.getSortType());
                mCategoryAdapter.addContent(files, mCurrentPath);
                mCategoryAdapter.notifyDataSetChanged();
            }
        }
    }

    public class SpinnerInteractionListener implements AdapterView.OnItemSelectedListener, View.OnTouchListener {

        boolean userSelect = false;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            userSelect = true;
            return false;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            if (userSelect) {
                // Your selection handling code here
                if (pos == 0) {
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_frame, new CategoryFragment(mMainContext, getResources().getString(R.string.category_image), true))
                            .commit();
                } else {
                    moveToStorageRoot(pos - 1);
                }


//                switch (pos) {
//                    case 0:
//                        // Whatever you want to happen when the first item gets selected
//                        //folderListDisplay(Utils.getInternalDirectoryPath());
//                        Toast.makeText(mMainContext, ""+pos, Toast.LENGTH_SHORT).show();
//                        break;
//                    case 1:
//                        // Whatever you want to happen when the second item gets selected
//                        //folderListDisplay(Utils.getSDcardDirectoryPath());
//                        Toast.makeText(mMainContext, ""+pos, Toast.LENGTH_SHORT).show();
//                        break;
//                }
                userSelect = false;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }
}
