package jiran.com.tmfilemanager.fagment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import jiran.com.flyingfile.wifidirect.activity.WifiDirectFileExplorerActivity;
import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.InitActivity;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.activity.viewer.ImageSlideViewerActivity;
import jiran.com.tmfilemanager.adapter.FolderFlyingFileAdapter;
import jiran.com.tmfilemanager.common.ListFunctionUtils;
import jiran.com.tmfilemanager.common.MimeTypes;
import jiran.com.tmfilemanager.common.SortUtils;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.data.TagItem;
import jiran.com.tmfilemanager.service.FileObserverService;
import jiran.com.tmfilemanager.settings.MyFileSettings;

/**
 * Created by user on 2016-08-03.
 */


public class FolderFlyingFileFragment extends Fragment {

    public static boolean mActionMode = false;
    private static int mView_Type = 0;
    public Context mWifiDirectContext;
    public int mListType = 0;
    public String mCurrentPath;
    public String mStartPath;
    public TagItem mTagItem;
    public String mSearch;
    public ArrayList<FileItem> mSearchArray;
    AlertDialog.Builder builder;
    AlertDialog alertDialog;
    HashMap<String, Integer> listPositionMap;
    private AbsListView mListView;
    private GridView mGridView;

    //    private ImageView list_bottom_menu_paste;
//    private ImageView list_bottom_menu_sort;
//    private ImageView list_bottom_menu_check;
    private LinearLayout mFoler_path_layout;
    //private TextView mFoler_path;
    private HorizontalScrollView foler_path_scroll_layout;
    private FolderFlyingFileAdapter mFolderAdapter;

    public FolderFlyingFileFragment() {
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public FolderFlyingFileFragment(Context context, String path, boolean startBack) {
        mWifiDirectContext = context;
        mCurrentPath = path;
        if (startBack) {
            mStartPath = path;
        } else {
            mStartPath = null;
        }
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public FolderFlyingFileFragment(Context context, int type, TagItem item) {
        mWifiDirectContext = context;
        mListType = type;
        mTagItem = item;
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public FolderFlyingFileFragment(Context context, String currentPath, String search) {
        mWifiDirectContext = context;
        //mStartPath = currentPath;
        mCurrentPath = currentPath;
        mSearch = search;
        setRetainInstance(true);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mWifiDirectContext == null) {
            Intent intent = new Intent(getActivity(), InitActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        }
        ((WifiDirectFileExplorerActivity) mWifiDirectContext).toolbar.setBackgroundResource(R.color.colorTitleBG);
        View rootView = inflater.inflate(R.layout.frag_folder_flyingfile, container, false);
        initLayout(inflater, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        ((WifiDirectFileExplorerActivity) mWifiDirectContext).finishSearchMode();
        ((WifiDirectFileExplorerActivity) mWifiDirectContext).toolbarMenu(R.menu.main_thum);
        setTitle();
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {

//        File file = new File(mCurrentPath);
//        if(mListType != 2 && file != null && file.isDirectory()){
//            if(ClipBoard.isEmpty()){
//                list_bottom_menu_paste.setVisibility(View.GONE);
//            }else{
//                list_bottom_menu_paste.setVisibility(View.VISIBLE);
//            }
//        }else{
//            list_bottom_menu_paste.setVisibility(View.GONE);
//        }
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void setTitle() {
        if (Utils.isSDcardDirectory(true)) {// || (MultiFragment.teamItams != null && MultiFragment.teamItams.size() > 0)){
            if (mCurrentPath.contains(Utils.getInternalDirectoryPath())) {
                ((WifiDirectFileExplorerActivity) mWifiDirectContext).setTitle(getResources().getString(R.string.internaldirectory), true);
            } else {
                ((WifiDirectFileExplorerActivity) mWifiDirectContext).setTitle(getResources().getString(R.string.sdcard), true);
            }
        } else {
            ((WifiDirectFileExplorerActivity) mWifiDirectContext).setTitle(getResources().getString(R.string.internaldirectory));
        }
    }

    public void initLayout(LayoutInflater inflater, View rootView) {
        //final MainActivity context = (MainActivity) getActivity();
        listPositionMap = new HashMap<String, Integer>();

        foler_path_scroll_layout = (HorizontalScrollView) rootView.findViewById(R.id.foler_path_scroll_layout);
        mFoler_path_layout = (LinearLayout) rootView.findViewById(R.id.foler_path_layout);

        mListView = (ListView) rootView.findViewById(R.id.folder_listview);
        mListView.setEmptyView(rootView.findViewById(R.id.empty_layout));
        mGridView = (GridView) rootView.findViewById(R.id.folder_gridview);
        mGridView.setEmptyView(rootView.findViewById(R.id.empty_layout));

        if (mView_Type == 0) {
            changeListView();
        } else {
            changeGridView();
        }

//        //bottom menu
//        list_bottom_menu_paste =  (ImageView) rootView.findViewById(R.id.list_bottom_menu_paste);
//        list_bottom_menu_paste.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                //Toast.makeText(mMainContext, "붙여넣기", Toast.LENGTH_SHORT).show();
//                if(mCurrentPath.equals(getResources().getString(R.string.category_document))
//                        || mCurrentPath.equals(getResources().getString(R.string.category_recentfile))
//                        || mCurrentPath.equals(getResources().getString(R.string.category_largefile))
//                        || mCurrentPath.equals(getResources().getString(R.string.category_image))
//                        || mCurrentPath.equals(getResources().getString(R.string.category_video))
//                        || mCurrentPath.equals(getResources().getString(R.string.category_music))
//                        || mListType == 2){
//                    final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
//                    Snackbar.make(viewPos, getResources().getString(R.string.copyfail), Snackbar.LENGTH_SHORT)
//                            .setAction("Action", null).show();
//                    ClipBoard.clear();
//                    list_bottom_menu_paste.setVisibility(View.GONE);
//                }else{
//                    final PasteTaskExecutor ptc = new PasteTaskExecutor(getActivity(), mCurrentPath);
//                    ptc.start();
//                    list_bottom_menu_paste.setVisibility(View.GONE);
//                }
//            }
//        });
    }

    public void changeListGridView() {
        if (mView_Type == 0) {
            changeGridView();
        } else {
            changeListView();
        }
    }

    public void changeGridView() {
        //final MainActivity context = (MainActivity) getActivity();
        mView_Type = 1;
        changeActionModeList();
        mFolderAdapter = new FolderFlyingFileAdapter(getActivity(), null, mView_Type);
        mGridView.setVisibility(View.VISIBLE);
        mListView.setVisibility(View.GONE);
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mActionMode) {
                    mFolderAdapter.setCheck(position);
                    mFolderAdapter.notifyDataSetChanged();
                } else {
                    final File file = new File(((FileItem) mGridView.getAdapter()
                            .getItem(position)).getPath());

                    if (file.isDirectory()) {
                        mSearch = null;
                        if ((mListType == 1 || mListType == 2) && mStartPath == null) {
                            mStartPath = file.getAbsolutePath();
                        }
                        //Scroll Position Set
                        if (mListType == 1) {
                            listPositionMap.put(mTagItem.getName(), mGridView.getFirstVisiblePosition());
                        } else if (mListType == 2) {
                            listPositionMap.put(getResources().getString(R.string.category_favorite), mGridView.getFirstVisiblePosition());
                        } else {
                            listPositionMap.put(mCurrentPath, mGridView.getFirstVisiblePosition());
                        }
                        folderListDisplay(file.getAbsolutePath());
                        // go to the top of the ListView
                        //mGridView.setSelection(0);
                    } else {
                        if (MimeTypes.isPicture(file)) {
                            Intent intent = new Intent(mWifiDirectContext, ImageSlideViewerActivity.class);
                            intent.putExtra("FileSender", true);
                            ImageSlideViewerActivity.mDataSource = mFolderAdapter.getImageList();
                            ImageSlideViewerActivity.mPosition = 0;
                            for (FileItem item : ImageSlideViewerActivity.mDataSource) {
                                if (item.getPath().equals(file.getPath())) {
                                    break;
                                } else {
                                    ImageSlideViewerActivity.mPosition++;
                                }
                            }
                            ((WifiDirectFileExplorerActivity) mWifiDirectContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_SLIDE_VIEWER);
                        } else {
                            Utils.openFile(mWifiDirectContext, file);
                        }
                    }
                }
            }
        });

        mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                mFolderAdapter.setCheck(position);
                setActionMode();
                return true;
            }
        });
        mGridView.setAdapter(mFolderAdapter);
        if (mSearch != null && mSearch.length() > 0) {
            folderSearchListDisplay(mCurrentPath, mSearch);
        } else if (mListType == 1) {
            folderTagListDisplay(mTagItem);
        } else if (mListType == 2) {
            folderFavoriteListDisplay(getResources().getString(R.string.category_favorite));
        } else {
            folderListDisplay(mCurrentPath);
        }
    }


    public void changeListView() {
        //final MainActivity context = (MainActivity) getActivity();
        mView_Type = 0;
        changeActionModeList();
        mFolderAdapter = new FolderFlyingFileAdapter(getActivity(), null, mView_Type);
        mListView.setVisibility(View.VISIBLE);
        mGridView.setVisibility(View.GONE);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mActionMode) {
                    mFolderAdapter.setCheck(position);
                    mFolderAdapter.notifyDataSetChanged();

                } else {
                    final File file = new File(((FileItem) mListView.getAdapter()
                            .getItem(position)).getPath());

                    if (file.isDirectory()) {
                        mSearch = null;
                        if ((mListType == 1 || mListType == 2) && mStartPath == null) {
                            mStartPath = file.getAbsolutePath();
                        }
                        //Scroll Position Set
                        if (mListType == 1) {
                            listPositionMap.put(mTagItem.getName(), mListView.getFirstVisiblePosition());
                        } else if (mListType == 2) {
                            listPositionMap.put(getResources().getString(R.string.category_favorite), mListView.getFirstVisiblePosition());
                        } else {
                            listPositionMap.put(mCurrentPath, mListView.getFirstVisiblePosition());
                        }
                        folderListDisplay(file.getAbsolutePath());
                        // go to the top of the ListView
                        //mListView.setSelection(0);
                    } else {
                        if (MimeTypes.isPicture(file)) {
                            Intent intent = new Intent(mWifiDirectContext, ImageSlideViewerActivity.class);
                            intent.putExtra("FileSender", true);
                            ImageSlideViewerActivity.mDataSource = mFolderAdapter.getImageList();
                            ImageSlideViewerActivity.mPosition = 0;
                            for (FileItem item : ImageSlideViewerActivity.mDataSource) {
                                if (item.getPath().equals(file.getPath())) {
                                    break;
                                } else {
                                    ImageSlideViewerActivity.mPosition++;
                                }
                            }
                            ((WifiDirectFileExplorerActivity) mWifiDirectContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_SLIDE_VIEWER);
                        } else {
                            Utils.openFile(mWifiDirectContext, file);
                        }
                    }
                }
            }
        });
        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                mFolderAdapter.setCheck(position);
                setActionMode();
                return true;
            }
        });

        mListView.setAdapter(mFolderAdapter);
        if (mSearch != null && mSearch.length() > 0) {
            folderSearchListDisplay(mCurrentPath, mSearch);
        } else if (mListType == 1) {
            folderTagListDisplay(mTagItem);
        } else if (mListType == 2) {
            folderFavoriteListDisplay(getResources().getString(R.string.category_favorite));
        } else {
            folderListDisplay(mCurrentPath);
        }
    }

    public void setActionMode() {
        mActionMode = true;
        mFolderAdapter.notifyDataSetChanged();
    }

    public void finishActionMode() {
        mActionMode = false;
        mFolderAdapter.setCheckClear();
        mFolderAdapter.notifyDataSetChanged();
    }

    public void setScrollPosition(String path) {
        //Scroll Position Set
        int position;
        if (listPositionMap.containsKey(path)) {
            position = listPositionMap.get(path);
            listPositionMap.remove(path);
        } else {
            position = 0;
        }

        if (mView_Type == 0) {
            mListView.setSelection(position);
        } else {
            mGridView.setSelection(position);
        }
    }

    public void folderListDisplay(String path) {
        mCurrentPath = path;
        //mFoler_path.setText(mCurrentPath);
        setFolderFathLayout(mCurrentPath);
        foler_path_scroll_layout.post(new Runnable() {
            public void run() {
                foler_path_scroll_layout.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        });

        if (mCurrentPath.equals(getResources().getString(R.string.category_document))
                || mCurrentPath.equals(getResources().getString(R.string.category_recentfile))
                || mCurrentPath.equals(getResources().getString(R.string.category_largefile))) {
            LoadingTask mTask = new LoadingTask(getActivity(), mCurrentPath);
            mTask.execute("");
        } else {
            mFolderAdapter.addFiles(path, 0);
        }
        //Scroll Position Set
        setScrollPosition(path);
        setTitle();
    }

    public void folderTagListDisplay(TagItem tag) {
        //mFoler_path.setText(tag.getName());
        setFolderFathLayout(tag.getName());
        mCurrentPath = "/";
        foler_path_scroll_layout.post(new Runnable() {
            public void run() {
                foler_path_scroll_layout.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        });
        mFolderAdapter.addTagFiles(tag);

        //Scroll Position Set
        setScrollPosition(tag.getName());
    }

    public void folderSearchListDisplay(String currentPath, String search) {
        //mFoler_path.setText(title);
        setFolderFathLayout(search, true);
        foler_path_scroll_layout.post(new Runnable() {
            public void run() {
                foler_path_scroll_layout.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        });
        if (mSearchArray != null) {
            mFolderAdapter.addContent(mSearchArray);
            mFolderAdapter.notifyDataSetChanged();
        } else {
            SearchTask mTask = new SearchTask(getActivity(), currentPath);
            mTask.execute(search);
        }
    }

    public void folderFavoriteListDisplay(String title) {
        //mFoler_path.setText(title);
        setFolderFathLayout(title);
        mCurrentPath = "/";
        foler_path_scroll_layout.post(new Runnable() {
            public void run() {
                foler_path_scroll_layout.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        });
        mFolderAdapter.addFavoriteFiles();

        //Scroll Position Set
        setScrollPosition(title);
    }

    public void setFolderFathLayout(String path) {
        setFolderFathLayout(path, false);
    }

    public void setFolderFathLayout(String path, boolean search) {
        mFoler_path_layout.removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(mWifiDirectContext);
        //Home Image add
        String HHOOMMEEPATH = "";
        boolean internal = true;
        if (path.contains(Utils.getInternalDirectoryPath())) {
            internal = true;
            String[] lastPath = Utils.getInternalDirectoryPath().split("/");
            path = File.separator + "HHOOMMEE" + File.separator + lastPath[lastPath.length - 1] + path.replaceFirst(Utils.getInternalDirectoryPath(), "");
            HHOOMMEEPATH = Utils.getInternalDirectoryPath().replaceFirst(File.separator + lastPath[lastPath.length - 1], "");
        } else if (Utils.isSDcardDirectory(true) && path.contains(Utils.getSDcardDirectoryPath())) {
            internal = false;
            String[] lastPath = Utils.getSDcardDirectoryPath().split("/");
            path = File.separator + "HHOOMMEE" + File.separator + lastPath[lastPath.length - 1] + path.replaceFirst(Utils.getSDcardDirectoryPath(), "");
            HHOOMMEEPATH = Utils.getSDcardDirectoryPath().replaceFirst(File.separator + lastPath[lastPath.length - 1], "");
        }
        String[] arrayPath = path.split("/");
        if (path.contains("/") && arrayPath.length > 1) {

            String makePath = "";
            int homeIndex = -1;
            for (int i = 1; i < arrayPath.length; i++) {
                String sPath = arrayPath[i];
                if (sPath.equals("HHOOMMEE")) {
                    homeIndex = i;
                    View home_view = inflater.inflate(R.layout.frag_folder_path_item_home, null, false);
                    makePath += HHOOMMEEPATH;
                    home_view.setTag(makePath);
                    home_view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //((MainActivity)mMainContext).initFragment();
//                            if(Utils.isSDcardDirectory(true)){
//
//                            }
                        }
                    });
                    mFoler_path_layout.addView(home_view);
                } else {
                    View path_view;
                    TextView foler_path;
                    if (homeIndex == (i - 1)) {
//                        if(Utils.isSDcardDirectory(true)){
//                            path_view = inflater.inflate(R.layout.frag_folder_path_item_spinner, null, false);
//                            Spinner spinner = (Spinner) path_view.findViewById(R.id.spinner);
//
//                            String[]paths = {"/" + getResources().getString(R.string.internaldirectory), "/" + getResources().getString(R.string.sdcard)};
//                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(mWifiDirectContext,
//                                    R.layout.frag_folder_path_item_spinner_tv, paths){
//                                @Override
//                                public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//                                    View view = super.getDropDownView(position, convertView, parent);
//                                    TextView tv = (TextView) view;
//                                    tv.setBackgroundResource(R.color.gray5);
//                                    // Set the Text color
//                                    tv.setTextColor(Color.parseColor("#FFFFFFFF"));
//                                    tv.setTextSize(20);
//
//                                    return view;
//                                    //return super.getDropDownView(position, convertView, parent);
//                                }
//                            };
//                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                            spinner.setAdapter(adapter);
//                            SpinnerInteractionListener listener = new SpinnerInteractionListener();
//                            spinner.setOnTouchListener(listener);
//                            spinner.setOnItemSelectedListener(listener);
//                            makePath += ("/" + sPath);
//                            if(internal){
//                                spinner.setSelection(0);
//                                //foler_path.setText("/" + getResources().getString(R.string.internaldirectory));
//                            }else{
//                                spinner.setSelection(1);
//                                //foler_path.setText("/" + getResources().getString(R.string.sdcard));
//                            }
//                        }else
                        {
                            path_view = inflater.inflate(R.layout.frag_folder_path_item, null, false);
                            foler_path = (TextView) path_view.findViewById(R.id.foler_path);
                            if (homeIndex == (i - 1)) {
//                                if(internal){
//                                    foler_path.setText("/" + getResources().getString(R.string.internaldirectory));
//                                }else{
//                                    foler_path.setText("/" + getResources().getString(R.string.sdcard));
//                                }
                                if (internal) {
                                    //foler_path.setText("" + getResources().getString(R.string.internaldirectory));
                                    foler_path.setBackgroundResource(R.mipmap.list_folder);
                                } else {
                                    //foler_path.setText("" + getResources().getString(R.string.sdcard));
                                    foler_path.setBackgroundResource(R.mipmap.list_folder);
                                }
                            } else {
                                //foler_path.setText("/" + sPath);
                                foler_path.setText(" > " + sPath);
                            }
                            makePath += ("/" + sPath);
                            path_view.setTag(makePath);
                            path_view.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    folderListDisplay((String) v.getTag());
                                }
                            });
                        }
                    } else {
                        path_view = inflater.inflate(R.layout.frag_folder_path_item, null, false);
                        foler_path = (TextView) path_view.findViewById(R.id.foler_path);
                        foler_path.setText(" > " + sPath);

                        makePath += ("/" + sPath);
                        path_view.setTag(makePath);
                        path_view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                folderListDisplay((String) v.getTag());
                            }
                        });
                    }
                    mFoler_path_layout.addView(path_view);
                }
            }
        } else {
            View path_view = inflater.inflate(R.layout.frag_folder_path_item, null, false);
            TextView foler_path = (TextView) path_view.findViewById(R.id.foler_path);
            if (search) {
                foler_path.setText("\"" + path + "\" " + getResources().getString(R.string.action_search_result));
            } else {
                if (path.equals(getResources().getString(R.string.category_document)) ||
                        path.equals(getResources().getString(R.string.category_recentfile)) ||
                        path.equals(getResources().getString(R.string.category_largefile)) ||
                        path.equals(getResources().getString(R.string.category_image)) ||
                        path.equals(getResources().getString(R.string.category_video)) ||
                        path.equals(getResources().getString(R.string.category_favorite)) ||
                        path.equals(getResources().getString(R.string.category_download)) ||
                        path.equals(getResources().getString(R.string.category_music))) {
                    foler_path.setText(File.separator + path);
                    View home_view = inflater.inflate(R.layout.frag_folder_path_item_home, null, false);
                    home_view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //((MainActivity)mMainContext).initFragment();
                        }
                    });
                    mFoler_path_layout.addView(home_view);
                } else {
                    foler_path.setText(path);
                }
            }
            mFoler_path_layout.addView(path_view);
        }
    }

    public void changeActionModeList() {
        if (mView_Type == 0) {
            ((WifiDirectFileExplorerActivity) mWifiDirectContext).toolbarMenu(R.menu.main_wifi);
        } else {
            ((WifiDirectFileExplorerActivity) mWifiDirectContext).toolbarMenu(R.menu.main_thum);
        }
    }

    public boolean onBackPressed() {
        if (mActionMode) {
            finishActionMode();
            return true;
        } else if (mCurrentPath != null && mCurrentPath.equals("/")) {
            //getActivity().finish();
            if (mSearch != null && mSearch.length() > 0) {
                mSearch = null;
                folderListDisplay(mCurrentPath);
                return true;
            }
            return false;
        } else {
            try {
                if (mSearch != null && mSearch.length() > 0) {
                    mSearch = null;
                    folderListDisplay(mCurrentPath);
//                    if (mCurrentPath.equals(Utils.getInternalDirectoryPath())) {
//                        return false;
//                    } else {
//                        folderListDisplay(mCurrentPath);
//                    }
                } else {
                    if (mStartPath != null && mStartPath.equals(mCurrentPath)) {
                        if (mListType == 1) {
                            folderTagListDisplay(mTagItem);
                        } else if (mListType == 2) {
                            folderFavoriteListDisplay(getResources().getString(R.string.category_favorite));
                        } else {
                            return false;
                        }
                    } else {
                        if (mCurrentPath.equals(Utils.getInternalDirectoryPath())) {
                            return false;
                        } else {
                            File file = new File(mCurrentPath);
                            folderListDisplay(file.getParent());
                        }
                    }
                }
            } catch (Exception e) {
                return false;
            }
            // get position of the previous folder in ListView
            //mListView.setSelection(mListAdapter.getPosition(file.getPath()));
            return true;
        }
    }

    public void deleteMethod() {
        int check_count = mFolderAdapter.getCheckCount();

        if (check_count > 0) {
            dialogDeleteFile(check_count);
        } else {
//            final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
//            Snackbar.make(viewPos, getResources().getString(R.string.snackbar_no_select), Snackbar.LENGTH_SHORT)
//                    .setAction("Action", null).show();

            Toast.makeText(mWifiDirectContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
            //finishActionMode();
        }
    }

    public void noSelectSnackbar() {
//            final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
//            Snackbar.make(viewPos, getResources().getString(R.string.snackbar_no_select), Snackbar.LENGTH_SHORT)
//                    .setAction("Action", null).show();

        Toast.makeText(mWifiDirectContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
    }

    public void dialogDeleteFile(final int check_count) {
        final Dialog dialog = new Dialog(mWifiDirectContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mWifiDirectContext);
        View view = inflater.inflate(R.layout.dialog_base, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.dialog_delete_file));
        TextView content = (TextView) view.findViewById(R.id.content);
        content.setText(getResources().getString(R.string.dialog_delete_file_info1) + check_count + getResources().getString(R.string.dialog_delete_file_info2));
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<FileItem> items = mFolderAdapter.getCheckList();

                boolean success = true;

                for (FileItem item : items) {
                    File select = new File(item.getPath());
                    if (select != null && select.exists()) {
                        if (select.isDirectory()) {
                            success = deleteFile(select);
                            if (!success) {
                                break;
                            }
                            select.delete();
                        } else {
                            if (select.delete()) {
//                                MainActivity.mMds.deleteTagPath(item.getPath());
//                                MainActivity.mMds.deleteFavorite(item.getFavoriteId());
                                MainActivity.mMds.deleteFile(item.getPath());
                            } else {
                                success = false;
                                break;
                            }
                        }
                    }
                }
                if (success) {
                    Toast.makeText(mWifiDirectContext, check_count + getResources().getString(R.string.snackbar_file_delete), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mWifiDirectContext, getResources().getString(R.string.snackbar_delete_error), Toast.LENGTH_SHORT).show();
                }

                mFolderAdapter.refresh();
//                for(FileItem item : items){
//                    mFolderAdapter.removeFiles(item);
//                }
                //mFolderAdapter.refresh(mCurrentPath);
                finishActionMode();

                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        dialog.setContentView(view);
        dialog.show();
    }

//    public void dialogDeleteFile(final int check_count){
//        builder = new AlertDialog.Builder(mWifiDirectContext);
//        LayoutInflater inflater = LayoutInflater.from(mWifiDirectContext);
//        View view = inflater.inflate(R.layout.dialog_base, null, false);
//        TextView title = (TextView)view.findViewById(R.id.title);
//        title.setText(getResources().getString(R.string.dialog_delete_file));
//        TextView content = (TextView)view.findViewById(R.id.content);
//        content.setText(getResources().getString(R.string.dialog_delete_file_info1) + check_count + getResources().getString(R.string.dialog_delete_file_info2));
//        TextView cancel = (TextView)view.findViewById(R.id.cancel);
//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(alertDialog != null){
//                    alertDialog.dismiss();
//                }
//            }
//        });
//        TextView ok = (TextView)view.findViewById(R.id.ok);
//        ok.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
//                ArrayList<FileItem> items = mFolderAdapter.getCheckList();
//
//                boolean success = true;
//
//                for(FileItem item : items){
//                    File select = new File(item.getPath());
//                    if(select != null && select.exists()){
//                        if(select.isDirectory()){
//                            success = deleteFile(select);
//                            if(!success){
//                                break;
//                            }
//                            select.delete();
//                        }else{
//                            if(select.delete()){
//                                MainActivity.mMds.deleteFile(item.getId());
//                            }else{
//                                success = false;
//                                break;
//                            }
//                        }
//                    }
//                }
//                if(success) {
////                    Snackbar.make(viewPos, check_count + getResources().getString(R.string.snackbar_file_delete), Snackbar.LENGTH_SHORT)
////                            .setAction("Action", null).show();
//                    Toast.makeText(mWifiDirectContext, check_count + getResources().getString(R.string.snackbar_file_delete), Toast.LENGTH_SHORT).show();
//                }else{
////                    Snackbar.make(viewPos, getResources().getString(R.string.snackbar_delete_error), Snackbar.LENGTH_SHORT)
////                            .setAction("Action", null).show();
//                    Toast.makeText(mWifiDirectContext, getResources().getString(R.string.snackbar_delete_error), Toast.LENGTH_SHORT).show();
//                }
//                mFolderAdapter.refresh();
//
//                if(alertDialog != null){
//                    alertDialog.dismiss();
//                }
//            }
//        });
//        builder.setView(view);
//        alertDialog = builder.create();
//        alertDialog.show();
//    }

    public boolean deleteFile(File file) {
        File[] fileList = file.listFiles();
        boolean success = true;
        for (File temp : fileList) {
            if (temp.isDirectory()) {
                success = deleteFile(temp);
                if (!success) {
                    break;
                }
                temp.delete();
            } else {
                if (temp.delete()) {
//                    ArrayList<FileItem> tempList = MainActivity.mMds.selectTagFiles(temp.getPath());
//                    for(FileItem sub : tempList){
//                        MainActivity.mMds.deleteFile(sub.getPath());
//                    }
                    MainActivity.mMds.deleteFile(temp.getPath());
                } else {
                    success = false;
                    break;
                }
            }
        }
        return success;
    }

    public void listRefresh() {
        mFolderAdapter.refresh();
        if (ImageSlideViewerActivity.mDataSource != null && ImageSlideViewerActivity.mPosition >= 0) {
            FileItem item = ImageSlideViewerActivity.mDataSource.get(ImageSlideViewerActivity.mPosition);
            ArrayList<FileItem> items = mFolderAdapter.getList();
            int pos = 0;
            for (FileItem temp : items) {
                if (item.getPath().equals(temp.getPath())) {
                    break;
                } else {
                    pos++;
                }
            }
            if (mView_Type == 0) {
                mListView.setSelection(pos);
            } else {
                mGridView.setSelection(pos);
            }
        }
        ImageSlideViewerActivity.mDataSource = null;
        ImageSlideViewerActivity.mPosition = -1;
    }

    public void dialogNewFolder() {
        builder = new AlertDialog.Builder(mWifiDirectContext);
        LayoutInflater inflater = LayoutInflater.from(mWifiDirectContext);
        final View view = inflater.inflate(R.layout.dialog_new_folder, null, false);
        final EditText edt = (EditText) view.findViewById(R.id.edt);

        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
            }
        });
        TextView create = (TextView) view.findViewById(R.id.create);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String folder_name = edt.getText().toString();
                if (folder_name != null && folder_name.length() > 0) {
                    File file = new File(mCurrentPath);
                    Log.e("readable->", "" + file.canRead());
                    Log.e("writable->", "" + file.canWrite());
                    Log.e("executable->", "" + file.canExecute());

                    String createPath = file.getAbsolutePath() + File.separator + folder_name;

                    File create = new File(createPath);

                    if (create.exists()) {
//                        final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
//                        Snackbar.make(viewPos, getResources().getString(R.string.overwrite_title_folder), Snackbar.LENGTH_SHORT)
//                                .setAction("Action", null).show();
                        Toast.makeText(mWifiDirectContext, getResources().getString(R.string.overwrite_title_folder), Toast.LENGTH_SHORT).show();
                    } else {
                        //create.mkdirs();
                        if (create.mkdirs()) {
                            //success
                        } else {
                            //fail
//                            final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
//                            Snackbar.make(viewPos, getResources().getString(R.string.snackbar_folder_create_error), Snackbar.LENGTH_SHORT)
//                                    .setAction("Action", null).show();
                            Toast.makeText(mWifiDirectContext, getResources().getString(R.string.snackbar_folder_create_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                    listRefresh();

                    if (alertDialog != null) {
                        alertDialog.dismiss();
                    }
                } else {
//                    final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
//                    Snackbar.make(viewPos, getResources().getString(R.string.snackbar_input_foldername), Snackbar.LENGTH_SHORT)
//                            .setAction("Action", null).show();
                    Toast.makeText(mWifiDirectContext, getResources().getString(R.string.snackbar_input_foldername), Toast.LENGTH_SHORT).show();
                }

            }
        });
        builder.setView(view);
        alertDialog = builder.create();
        alertDialog.show();
    }

    //추가 메소드
    public ArrayList<String> getselectList() {
        ArrayList<FileItem> selctList = mFolderAdapter.getCheckList();
        ArrayList<String> selectStringList = new ArrayList<String>();
        for (FileItem temp : selctList) {
            File file = new File(temp.getPath());
            selectStringList.add(file.getAbsolutePath());
        }
        return selectStringList;
    }

    public void dialogSetSort() {
        builder = new AlertDialog.Builder(mWifiDirectContext);
        LayoutInflater inflater = LayoutInflater.from(mWifiDirectContext);
        View view = inflater.inflate(R.layout.dialog_select_sort, null, false);
        LinearLayout sort_by_name = (LinearLayout) view.findViewById(R.id.sort_by_name);
        TextView sort_by_name_txt = (TextView) view.findViewById(R.id.sort_by_name_txt);
        if (MyFileSettings.getSortType() != SortUtils.SORT_ALPHA) {
            sort_by_name_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        } else {
            if (!MyFileSettings.getSortReverse()) {
                sort_by_name_txt.setText(sort_by_name_txt.getText().toString() + " ↑");
            } else {
                sort_by_name_txt.setText(sort_by_name_txt.getText().toString() + " ↓");
            }
        }
        sort_by_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
                if (MyFileSettings.getSortType() == SortUtils.SORT_ALPHA) {
                    MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
                } else {
                    MyFileSettings.setSortType(SortUtils.SORT_ALPHA);
                }

                mFolderAdapter.reSort();
            }
        });
        LinearLayout sort_by_extension = (LinearLayout) view.findViewById(R.id.sort_by_extension);
        TextView sort_by_extension_txt = (TextView) view.findViewById(R.id.sort_by_extension_txt);
        if (MyFileSettings.getSortType() != SortUtils.SORT_TYPE) {
            sort_by_extension_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        } else {
            if (!MyFileSettings.getSortReverse()) {
                sort_by_extension_txt.setText(sort_by_extension_txt.getText().toString() + " ↑");
            } else {
                sort_by_extension_txt.setText(sort_by_extension_txt.getText().toString() + " ↓");
            }
        }
        sort_by_extension.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
                if (MyFileSettings.getSortType() == SortUtils.SORT_TYPE) {
                    MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
                } else {
                    MyFileSettings.setSortType(SortUtils.SORT_TYPE);
                }
                mFolderAdapter.reSort();
            }
        });
        LinearLayout sort_by_date = (LinearLayout) view.findViewById(R.id.sort_by_date);
        TextView sort_by_date_txt = (TextView) view.findViewById(R.id.sort_by_date_txt);
        if (MyFileSettings.getSortType() != SortUtils.SORT_DATE) {
            sort_by_date_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        } else {
            if (!MyFileSettings.getSortReverse()) {
                sort_by_date_txt.setText(sort_by_date_txt.getText().toString() + " ↑");
            } else {
                sort_by_date_txt.setText(sort_by_date_txt.getText().toString() + " ↓");
            }
        }
        sort_by_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog != null) {
                    alertDialog.dismiss();
                }
                if (MyFileSettings.getSortType() == SortUtils.SORT_DATE) {
                    MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
                } else {
                    MyFileSettings.setSortType(SortUtils.SORT_DATE);
                }
                mFolderAdapter.reSort();
            }
        });
        builder.setView(view);
        alertDialog = builder.create();
        alertDialog.show();
    }

    //Search Method
    private class SearchTask extends AsyncTask<String, Void, ArrayList<FileItem>> {
        private final Context context;
        private ProgressDialog pr_dialog;
        private String mCurrentPath;

        private String mSearchStr;

        private SearchTask(Context c, String currentPath) {
            context = c;
            File file = new File(currentPath);
            if (file != null && file.isDirectory()) {
                mCurrentPath = currentPath;
            } else {
                mCurrentPath = Utils.getInternalDirectoryPath();
            }
        }

        @Override
        protected void onPreExecute() {
            pr_dialog = ProgressDialog.show(context, null,
                    context.getResources().getString(R.string.action_search));
            pr_dialog.setCanceledOnTouchOutside(true);
        }

        @Override
        protected ArrayList<FileItem> doInBackground(String... params) {
            //String location = BrowserTabsAdapter.getCurrentBrowserFragment().mCurrentPath;
            mSearchStr = params[0];
            return Utils.searchInDirectory(mCurrentPath, mSearchStr);
        }

        @Override
        protected void onPostExecute(final ArrayList<FileItem> files) {
            int len = files != null ? files.size() : 0;
            mSearchArray = files;
            pr_dialog.dismiss();
            if (len == 0) {
//                final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
//                Snackbar.make(viewPos, getResources().getString(R.string.snackbar_search_result_empty), Snackbar.LENGTH_SHORT)
//                        .setAction("Action", null).show();
                Toast.makeText(mWifiDirectContext, getResources().getString(R.string.snackbar_search_result_empty), Toast.LENGTH_SHORT).show();
            } else {
                SortUtils.sortList(files, mCurrentPath, MyFileSettings.getSortType());
                mFolderAdapter.addContent(files);
                mFolderAdapter.notifyDataSetChanged();
            }
            setScrollPosition(mSearchStr);
        }
    }

    private class LoadingTask extends AsyncTask<String, Void, ArrayList<FileItem>> {
        private final Context context;
        private ProgressDialog pr_dialog;
        private String mCurrentPath;

        private LoadingTask(Context c, String currentPath) {
            context = c;
            mCurrentPath = currentPath;
        }

        @Override
        protected void onPreExecute() {
            pr_dialog = ProgressDialog.show(context, null,
                    context.getResources().getString(R.string.action_loading));
            pr_dialog.setCanceledOnTouchOutside(true);
        }

        @Override
        protected ArrayList<FileItem> doInBackground(String... params) {
            //String location = BrowserTabsAdapter.getCurrentBrowserFragment().mCurrentPath;

            if (mCurrentPath.equals(getResources().getString(R.string.category_document))) {
                return ListFunctionUtils.listAllDocumentFiles(context);
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_recentfile))) {
                return ListFunctionUtils.listDaysFiles(context, ListFunctionUtils.LIST_DAY);
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_largefile))) {
                return ListFunctionUtils.listHugeFiles(context, FileObserverService.NEW_FILE_SIZE_LIMIT_100MB);
            } else {
                return Utils.searchInDirectory(mCurrentPath, params[0]);
            }
        }

        @Override
        protected void onPostExecute(final ArrayList<FileItem> files) {
            int len = files != null ? files.size() : 0;
            if (pr_dialog != null && pr_dialog.isShowing()) {
                pr_dialog.dismiss();
            }
            if (len == 0) {
//                final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
//                Snackbar.make(viewPos, getResources().getString(R.string.snackbar_search_result_empty), Snackbar.LENGTH_SHORT)
//                        .setAction("Action", null).show();
                Toast.makeText(mWifiDirectContext, getResources().getString(R.string.snackbar_search_result_empty), Toast.LENGTH_SHORT).show();
            } else {
                SortUtils.sortList(files, mCurrentPath, MyFileSettings.getSortType());
                mFolderAdapter.addContent(files);
                mFolderAdapter.notifyDataSetChanged();
            }
        }
    }

    public class SpinnerInteractionListener implements AdapterView.OnItemSelectedListener, View.OnTouchListener {

        boolean userSelect = false;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            userSelect = true;
            return false;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            if (userSelect) {
                // Your selection handling code here
                switch (pos) {
                    case 0:
                        // Whatever you want to happen when the first item gets selected
                        folderListDisplay(Utils.getInternalDirectoryPath());
                        break;
                    case 1:
                        // Whatever you want to happen when the second item gets selected
                        folderListDisplay(Utils.getSDcardDirectoryPath());
                        break;
                }
                userSelect = false;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }
}
