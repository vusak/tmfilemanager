package jiran.com.tmfilemanager.fagment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.SwipeDismissBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.CardView;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import jiran.com.circleprogress.DonutProgress;
import jiran.com.flyingfile.util.SharedPreferenceUtil;
import jiran.com.flyingfile.wifidirect.activity.WifiDirectActivity;
import jiran.com.inappbilling.InAppBillingActivity;
import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.InitActivity;
import jiran.com.tmfilemanager.activity.LoginActivity;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.activity.team.SearchTeamActivity;
import jiran.com.tmfilemanager.activity.team.TeamManagentActivity;
import jiran.com.tmfilemanager.common.ListFunctionUtils;
import jiran.com.tmfilemanager.common.SortUtils;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.data.TagItem;
import jiran.com.tmfilemanager.data.TeamItem;
import jiran.com.tmfilemanager.data.UserItem;
import jiran.com.tmfilemanager.layout.FlowLayout;
import jiran.com.tmfilemanager.network.JMF_network;
import jiran.com.tmfilemanager.utils.Util;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by user on 2016-08-03.
 */


public class MultiFragment extends Fragment {
    public static ArrayList<TeamItem> teamItams;
    private static int TEAM_MORE_COUNT = 5;
    private static boolean TEAM_MORE = true;
    private static boolean TAG_MORE = true;
    public Context mMainContext;
    LinearLayout sd_card_memory_layout;
    LinearLayout sd_card_visiable_layout, recent_layout, download_layout;
    LinearLayout team_visiable_layout, team_storage_content_layout, more_team_list_bt, more_tag_list_bt;
    ImageView team_storage_add_bt, team_storage_search_bt, team_storage_move_bt, team_storage_refresh_bt;
    ImageView more_team_list_bt_iv, more_tag_list_bt_iv;
    DonutProgress sd_card_memory;
    TextView sdcard_total_size;
    TextView sdcard_use_size;
    TextView memory_use_size;
    boolean doubleBackToExitPressedOnce = false;
    InputFilter filterText = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {
            if (!Utils.teamnamevalidate(source)) {
                Toast.makeText(mMainContext, getString(R.string.dialog_make_team_toast_info), Toast.LENGTH_SHORT).show();
                return "";
            }
            return null;
        }
    };
    InputFilter filterLength = new InputFilter.LengthFilter(20);
    // TMBrowser banner
    CardView bannerCardView;
    RelativeLayout bannerLayout;
    ImageButton bannerCloseButton;
    Button bannerTryButton;
    private ScrollView home_scrollview;
    private LinearLayout category_app_layout, category_image_layout, category_video_layout, category_music_layout, category_document_layout, category_zip_layout;
    private LinearLayout tag_empty_layout;
    private FlowLayout tag_content_layout;
    private Animation showAni, goneAni;
    private Dialog mDialog;
    private boolean TEAM_MOVE_MODE = false;
    private ProgressDialog pr_dialog;

    public MultiFragment() {
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public MultiFragment(Context context) {
        mMainContext = context;
        setRetainInstance(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MainActivity) mMainContext).setTheme(R.style.AppTheme_NoActionBar);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mMainContext == null) {
            Intent intent = new Intent(getActivity(), InitActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        }
        teamItams = null;
        ((MainActivity) mMainContext).toolbar.setBackgroundResource(R.color.colorTitleBG);
        View rootView;// = inflater.inflate(R.layout.frag_multi, container, false);
//        if (Utils.checkIsTablet(mMainContext)) {
//            rootView = inflater.inflate(R.layout.tablet_frag_multi, container, false);
//        } else {
//            rootView = inflater.inflate(R.layout.frag_multi, container, false);
//        }
        rootView = inflater.inflate(R.layout.frag_multi, container, false);
        home_scrollview = (ScrollView) rootView.findViewById(R.id.home_scrollview);
        initLayout(inflater, rootView);

//        if(MainActivity.homeScrollX != 0 || MainActivity.homeScrollY != -1){
//            //this is important. scrollTo doesn't work in main thread.
//            home_scrollview.post(new Runnable()
//            {
//                @Override
//                public void run()
//                {
//                    //home_scrollview.scrollTo(MainActivity.homeScrollX, MainActivity.homeScrollY);
//                    home_scrollview.setScrollX(MainActivity.homeScrollX);
//                    home_scrollview.setScrollY(MainActivity.homeScrollY);
//                }
//            });
//        }
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            if (bannerLayout == null)
                return;
            if (Util.getPackageList((MainActivity) mMainContext, Util.PACKAGE_TM_BROWSER)
                    || !SharedPreferenceUtil.getInstance().isDelayedShowADTMBrowser(mMainContext)) {
                bannerLayout.setVisibility(View.GONE);
            } else
                bannerLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        ((MainActivity) mMainContext).finishSearchMode();
        ((MainActivity) mMainContext).toolbarMenu(R.menu.search);

        super.onActivityCreated(savedInstanceState);
    }

    public void recentFolder() {
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, new FolderFragment(mMainContext, getResources().getString(R.string.category_recentfile), true))
                .commit();
        MainActivity.homeScrollX = home_scrollview.getScrollX();
        MainActivity.homeScrollY = home_scrollview.getScrollY();
    }

    public void downloadFolder() {
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, new FolderFragment(mMainContext, Utils.getDownloadDirectoryPath(), true, 0))
                .commit();
        MainActivity.homeScrollX = home_scrollview.getScrollX();
        MainActivity.homeScrollY = home_scrollview.getScrollY();
    }

    @Override
    public void onResume() {
//        SpannableString content = new SpannableString(getResources().getString(R.string.app_name));
//        content.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        content.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.NORMAL), 2, content.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ((MainActivity) getActivity()).setTitle(getResources().getString(R.string.left_menu_home));

        //Toast.makeText(getActivity(), mPath, Toast.LENGTH_SHORT).show();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onBackPressed() {
        //Toast.makeText(getActivity(), "onBackPressed", Toast.LENGTH_SHORT).show();
        dialogExitTest();
        return true;
    }

    public void dialogExitTest() {
        if (((MainActivity) mMainContext).exit_layout.isShown()) {
            ((MainActivity) mMainContext).exit_layout.setVisibility(View.GONE);
            ((MainActivity) mMainContext).navigationViewAction(true);
        } else {
            ((MainActivity) mMainContext).exit_layout.setVisibility(View.VISIBLE);
            ((MainActivity) mMainContext).navigationViewAction(false);
        }

    }

    public long getTotalMemory() {

        String str1 = "/proc/meminfo";
        String str2;
        String[] arrayOfString;
        long initial_memory = 0;
        try {
            FileReader localFileReader = new FileReader(str1);
            BufferedReader localBufferedReader = new BufferedReader(
                    localFileReader, 8192);
            str2 = localBufferedReader.readLine();// meminfo
            arrayOfString = str2.split("\\s+");
            for (String num : arrayOfString) {
                Log.i(str2, num + "\t");
            }
            // total Memory
            initial_memory = Integer.valueOf(arrayOfString[1]).intValue() * 1024;
            localBufferedReader.close();
            return initial_memory;
        } catch (IOException e) {
            return -1;
        }


    }

    public void initLayout(LayoutInflater inflater, View rootView) {
        // TM Browser Banner
        bannerCardView = rootView.findViewById(R.id.cardview_banner);
        implementSwipeDismiss();
        bannerLayout = rootView.findViewById(R.id.layout_banner);
        bannerTryButton = rootView.findViewById(R.id.button_banner_try);
        bannerTryButton.setOnClickListener(v -> {
            Util.launchApp(getActivity(), Util.PACKAGE_TM_BROWSER);
        });
        bannerCloseButton = rootView.findViewById(R.id.button_banner_close);
        bannerCloseButton.setOnClickListener(v -> {
            bannerClose();
        });
        if (Util.getPackageList((MainActivity) mMainContext, Util.PACKAGE_TM_BROWSER)
                || !SharedPreferenceUtil.getInstance().isDelayedShowADTMBrowser(mMainContext)) {
            bannerLayout.setVisibility(View.GONE);
        } else
            bannerLayout.setVisibility(View.VISIBLE);

        //final MainActivity context = (MainActivity) getActivity();
        LinearLayout internal_storage_memory_layout = (LinearLayout) rootView.findViewById(R.id.internal_storage_memory_layout);
        DonutProgress external_storage_memory = (DonutProgress) rootView.findViewById(R.id.internal_storage_memory);
        internal_storage_memory_layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, new FolderFragment(mMainContext, Utils.getInternalDirectoryPath(), true))
                        .commit();
                MainActivity.homeScrollX = home_scrollview.getScrollX();
                MainActivity.homeScrollY = home_scrollview.getScrollY();
            }
        });
        TextView external_total_size = (TextView) rootView.findViewById(R.id.external_total_size);
        TextView external_use_size = (TextView) rootView.findViewById(R.id.external_use_size);
        long total = Utils.getTotalExternalMemorySize();
        long free = Utils.getExternalMemorySize();
        external_total_size.setText(ListFunctionUtils.formatCalculatedSize(total, 0));
        external_use_size.setText(ListFunctionUtils.formatCalculatedSize(total - free, 1));
        int external_size = (int) ((free * 100) / total);
        external_storage_memory.setProgress(100 - external_size);
        //external_storage_memory.setProgress(external_size);

        sd_card_visiable_layout = (LinearLayout) rootView.findViewById(R.id.sd_card_visiable_layout);
        recent_layout = (LinearLayout) rootView.findViewById(R.id.recent_layout);
        download_layout = (LinearLayout) rootView.findViewById(R.id.download_layout);

        sd_card_memory_layout = (LinearLayout) rootView.findViewById(R.id.sd_card_memory_layout);
        sd_card_memory = (DonutProgress) rootView.findViewById(R.id.sd_card_memory);
        sd_card_memory_layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, new FolderFragment(mMainContext, Utils.getSDcardDirectoryPath(), true))
                        .commit();
                MainActivity.homeScrollX = home_scrollview.getScrollX();
                MainActivity.homeScrollY = home_scrollview.getScrollY();
            }
        });
        sdcard_total_size = (TextView) rootView.findViewById(R.id.sdcard_total_size);
        sdcard_use_size = (TextView) rootView.findViewById(R.id.sdcard_use_size);

        if (Utils.isSDcardDirectory(true)) {
            setSDcardMemoryLayout(true);
        } else {
            setSDcardMemoryLayout(false);
        }


        //TEAM Layout
        team_visiable_layout = (LinearLayout) rootView.findViewById(R.id.team_visiable_layout);
        team_storage_add_bt = (ImageView) rootView.findViewById(R.id.team_storage_add_bt);
        team_storage_search_bt = (ImageView) rootView.findViewById(R.id.team_storage_search_bt);
        team_storage_move_bt = (ImageView) rootView.findViewById(R.id.team_storage_move_bt);
        team_storage_refresh_bt = (ImageView) rootView.findViewById(R.id.team_storage_refresh_bt);

        team_storage_content_layout = (LinearLayout) rootView.findViewById(R.id.team_storage_content_layout);
        more_team_list_bt = (LinearLayout) rootView.findViewById(R.id.more_team_list_bt);
        more_team_list_bt_iv = (ImageView) rootView.findViewById(R.id.more_team_list_bt_iv);
        more_tag_list_bt = (LinearLayout) rootView.findViewById(R.id.more_tag_list_bt);
        more_tag_list_bt_iv = (ImageView) rootView.findViewById(R.id.more_tag_list_bt_iv);
        more_team_list_bt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                displayTeamList(!TEAM_MORE, TEAM_MOVE_MODE);
            }
        });
        //TEST DATA
        if (LoginActivity.mLoginToken != null && LoginActivity.mLoginToken.length() > 0) {
            loginAction();
        } else {
            logoutAction();
        }

        RelativeLayout func_filereceive = (RelativeLayout) rootView.findViewById(R.id.func_filereceive);
        func_filereceive.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mMainContext, "준비중...", Toast.LENGTH_SHORT).show();

            }
        });

        LinearLayout func_secretfolder = rootView.findViewById(R.id.func_secretfolder);
        func_secretfolder.setOnClickListener(v -> {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, new SecretFolderFragment(mMainContext, Utils.getSecretFolderPath(), true))
                    .commit();
            MainActivity.homeScrollX = home_scrollview.getScrollX();
            MainActivity.homeScrollY = home_scrollview.getScrollY();
        });

        LinearLayout func_favorite = rootView.findViewById(R.id.func_favorite);
        func_favorite.setOnClickListener(v -> {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, new FolderFragment(mMainContext, 2, null))
                    .commit();
            MainActivity.homeScrollX = home_scrollview.getScrollX();
            MainActivity.homeScrollY = home_scrollview.getScrollY();
        });

        RelativeLayout func_e_album = (RelativeLayout) rootView.findViewById(R.id.func_e_album);
        func_e_album.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mMainContext, "준비중...", Toast.LENGTH_SHORT).show();
            }
        });

        LinearLayout func_wifi_direct = rootView.findViewById(R.id.func_wifi_direct);
        func_wifi_direct.setOnClickListener(v -> {
            //와이파이 다이렉트 기능 시작.
            Intent intent = new Intent(mMainContext, WifiDirectActivity.class);
            startActivity(intent);
        });


//        RelativeLayout memory_clear = (RelativeLayout) rootView.findViewById(R.id.memory_clear);
//        memory_clear.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.content_frame, new ClearMainFragment(mMainContext))
//                        .commit();
//            }
//        });
//
//        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
//        ActivityManager activityManager = (ActivityManager) getActivity().getSystemService(ACTIVITY_SERVICE);
//        activityManager.getMemoryInfo(mi);
//        long totalMemory;
//        if (Build.VERSION.SDK_INT > 15) {
//            totalMemory = mi.totalMem / 1048576L;
//        } else {
//            totalMemory = getTotalMemory() / 1048576L;
//        }
//        long availableMegs = mi.availMem / 1048576L;
//        long usedMemory = totalMemory - availableMegs;
//
//        int per = (int) ((usedMemory * 100) / totalMemory);
//        memory_use_size =  (TextView) rootView.findViewById(R.id.memory_use_size);
//        memory_use_size.setText(per + "%");

        //Category
        category_app_layout = (LinearLayout) rootView.findViewById(R.id.category_app_layout);
        category_image_layout = (LinearLayout) rootView.findViewById(R.id.category_image_layout);
        category_video_layout = (LinearLayout) rootView.findViewById(R.id.category_video_layout);
        category_music_layout = (LinearLayout) rootView.findViewById(R.id.category_music_layout);
        category_document_layout = (LinearLayout) rootView.findViewById(R.id.category_document_layout);
        category_zip_layout = (LinearLayout) rootView.findViewById(R.id.category_zip_layout);

        category_app_layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mMainContext, "개발중...", Toast.LENGTH_SHORT).show();
                ((MainActivity) mMainContext).appFolder(null);
                MainActivity.homeScrollX = home_scrollview.getScrollX();
                MainActivity.homeScrollY = home_scrollview.getScrollY();
            }
        });

        category_image_layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                getActivity().getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.content_frame, new FolderFragment(mMainContext, getResources().getString(R.string.category_image), true))
//                        .commit();
                ((MainActivity) mMainContext).pictureFolder(null);
                MainActivity.homeScrollX = home_scrollview.getScrollX();
                MainActivity.homeScrollY = home_scrollview.getScrollY();
            }
        });
        category_video_layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                getActivity().getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.content_frame, new FolderFragment(mMainContext, getResources().getString(R.string.category_video), true))
//                        .commit();
                ((MainActivity) mMainContext).movieFolder(null);
                MainActivity.homeScrollX = home_scrollview.getScrollX();
                MainActivity.homeScrollY = home_scrollview.getScrollY();
            }
        });
        category_music_layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                getActivity().getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.content_frame, new FolderFragment(mMainContext, getResources().getString(R.string.category_music), true))
//                        .commit();
                ((MainActivity) mMainContext).musicFolder(null);
                MainActivity.homeScrollX = home_scrollview.getScrollX();
                MainActivity.homeScrollY = home_scrollview.getScrollY();

            }
        });
        category_document_layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                getActivity().getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.content_frame, new FolderFragment(mMainContext, getResources().getString(R.string.category_document), true))
//                        .commit();
                ((MainActivity) mMainContext).documentFolder(null);
                MainActivity.homeScrollX = home_scrollview.getScrollX();
                MainActivity.homeScrollY = home_scrollview.getScrollY();
            }
        });
        category_zip_layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                getActivity().getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.content_frame, new FolderFragment(mMainContext, Utils.getFavoriteDirectoryPath()))
//                        .commit();
                //Toast.makeText(mMainContext, "개발중...", Toast.LENGTH_SHORT).show();
                ((MainActivity) mMainContext).zipFolder(null);
                MainActivity.homeScrollX = home_scrollview.getScrollX();
                MainActivity.homeScrollY = home_scrollview.getScrollY();

            }
        });
//        category_download_layout.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.content_frame, new FolderFragment(mMainContext, Utils.getDownloadDirectoryPath(), true, 0))
//                        .commit();
//            }
//        });
//        category_recent_layout.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.content_frame, new FolderFragment(mMainContext, getResources().getString(R.string.category_recentfile), true))
//                        .commit();
//            }
//        });
//        category_huge_layout.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.content_frame, new FolderFragment(mMainContext, getResources().getString(R.string.category_largefile), true))
//                        .commit();
//            }
//        });
//        category_hdvd_layout.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
//                Snackbar.make(viewPos, getResources().getString(R.string.snackbar_implementing), Snackbar.LENGTH_SHORT)
//                        .setAction("Action", null).show();
//
//                File dir = new File(Utils.getHDVDirectoryPath());
//                if(dir.exists()){
//                    getActivity().getSupportFragmentManager().beginTransaction()
//                            .replace(R.id.content_frame, new FolderFragment(mMainContext, Utils.getHDVDirectoryPath(), true, 0))
//                            .commit();
//                }else{
//                    String ReviewUrl = "https://play.google.com/store/apps/details?id=com.ne.hdv";
//                    Intent intent = new Intent(Intent.ACTION_VIEW);
//                    intent.setData(Uri.parse(ReviewUrl));
//                    getActivity().startActivity(intent);
//                }
//
//            }
//        });
//        category_popular_layout.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
////                Snackbar.make(viewPos, getResources().getString(R.string.snackbar_implementing), Snackbar.LENGTH_SHORT)
////                        .setAction("Action", null).show();
//                String url = "http://jmobile.io/apps";
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//                intent.setData(Uri.parse(url));
//                getActivity().startActivity(intent);
//            }
//        });


        //TAG DISPLAY
        tag_content_layout = (FlowLayout) rootView.findViewById(R.id.tag_content_layout);
        tag_empty_layout = (LinearLayout) rootView.findViewById(R.id.tag_empty_layout);
        displayTagList();
        //TAG DISPLAY

    }

    public void dialogNewTag() {
        dialogNewTag(null);
    }

    public void dialogNewTag(final TagItem item) {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        final View view = inflater.inflate(R.layout.dialog_new_tag, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        final EditText edt = (EditText) view.findViewById(R.id.edt);
        if (item != null) {
            title.setText(getResources().getString(R.string.dialog_modify_tag));
            edt.setText(item.getName());
        }
        edt.requestFocus();
        InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);


        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView create = (TextView) view.findViewById(R.id.create);
        if (item != null) {
            create.setText(getResources().getString(R.string.dialog_modify));
        }
        create.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                String tag_name = edt.getText().toString();
                if (tag_name != null && tag_name.length() > 0) {
                    int select = (int) edt.getTag();
                    //Toast.makeText(mMainContext, tag_name + " => " + select, Toast.LENGTH_SHORT).show();
                    if (item != null) {
                        MainActivity.mMds.updateTagItem(item.getId(), tag_name, select + "");
                    } else {
                        MainActivity.mMds.insertTagItem(tag_name, select + "");
                    }
                    ((MainActivity) mMainContext).displayTeamList();
                    displayTagList();

                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                } else {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_input_tagname), Toast.LENGTH_SHORT).show();
                }

            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void addTeam() {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.dialog_make_teamstorage, null, false);

        EditText edt_team_name = (EditText) view.findViewById(R.id.edt_team_name);
        edt_team_name.setPrivateImeOptions("defaultInputmode=english;");
        //edt_team_name.setFilters(new InputFilter[]{filterText, filterLength});

//        edt_team_name.requestFocus();
//        InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);

        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_team_name.getWindowToken(), 0);
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String teamName = edt_team_name.getText().toString();
                if (teamName == null || teamName.length() == 0) {
                    Toast.makeText(mMainContext, getResources().getString(R.string.dialog_make_team_input_teamname_hint), Toast.LENGTH_SHORT).show();
                    return;
                }

                InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_team_name.getWindowToken(), 0);
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                //Toast.makeText(mMainContext, teamName, Toast.LENGTH_SHORT).show();

                try {
                    ((MainActivity) mMainContext).showProgress();
                    JMF_network.createTeam(teamName)
                            .flatMap(gigaPodPair -> {
                                return Observable.just(gigaPodPair);
                            })
                            .doOnError(throwable -> {
                                ((MainActivity) mMainContext).dismissProgress();
                            })
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(gigaPodPair -> {
                                        ((MainActivity) mMainContext).dismissProgress();
                                        if (gigaPodPair.first) {
                                            JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                            MultiFragment.teamItams = null;
                                            ((MainActivity) mMainContext).initFragment();
                                            Utils.notiAlert(mMainContext, getString(R.string.team_make_storage_complete));
                                        } else {
                                            Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                        }
                                    },
                                    throwable -> {
                                        throwable.printStackTrace();
                                    });
                } catch (Exception e) {
                    ((MainActivity) mMainContext).dismissProgress();
                    e.printStackTrace();
                    Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
                }
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void displayTeamList() {
        displayTeamList(TEAM_MORE, TEAM_MOVE_MODE);
    }

    public void displayTeamList(boolean more, boolean move) {
        ((MainActivity) mMainContext).dismissProgress();
        TEAM_MORE = more;
        TEAM_MOVE_MODE = move;
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        team_storage_add_bt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addTeam();
            }
        });
        team_storage_search_bt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(mMainContext, SearchTeamActivity.class);
                ((MainActivity) mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_SEARCH_TEAM_ACTIVITY);

            }
        });
        team_storage_move_bt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                displayTeamList(TEAM_MORE, !TEAM_MOVE_MODE);
            }
        });

        team_storage_refresh_bt.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                teamItams = null;
                ((MainActivity) mMainContext).showProgress();
                loginAction();
            }
        });

        team_storage_content_layout.removeAllViews();
        ((MainActivity) mMainContext).displayTeamList();
        if (LoginActivity.mLoginToken != null && LoginActivity.mLoginToken.length() > 0) {
            team_visiable_layout.setVisibility(View.VISIBLE);
            if (teamItams != null && teamItams.size() > 0) {
                boolean myTeam = false;
                final int totalCount = teamItams.size();
                for (int i = 0; i < totalCount; ) {
//            }
//            for(final TagItem item : tagItams){
                    final int pos = i;
                    final TeamItem item = teamItams.get(i++);
                    if (item.getMyRole().equals("leader")) {
                        myTeam = true;
                    }

                    View team_view;// = inflater.inflate(R.layout.list_multifrag_team, null, false);
//                    if (Utils.checkIsTablet(mMainContext)) {
//                        team_view = inflater.inflate(R.layout.tablet_list_multifrag_team, null, false);
//                    } else {
//                        team_view = inflater.inflate(R.layout.list_multifrag_team, null, false);
//                    }
                    team_view = inflater.inflate(R.layout.list_multifrag_team, null, false);


                    DonutProgress team_use_storage_percent = (DonutProgress) team_view.findViewById(R.id.team_use_storage_percent);
                    team_use_storage_percent.setProgress(item.getPercent());

                    TextView team_name = (TextView) team_view.findViewById(R.id.team_name);
                    team_name.setText(item.getTeamName());
                    if (item.getTeamLeader()) {
                        team_name.setTextColor(mMainContext.getResources().getColorStateList(R.color.cyan_a100));
                    } else {
                        team_name.setTextColor(mMainContext.getResources().getColorStateList(R.color.black_a100));
                    }
                    TextView team_use_storage_size = (TextView) team_view.findViewById(R.id.team_use_storage_size);

                    team_use_storage_size.setText(ListFunctionUtils.formatCalculatedSize(item.getVolumeUsage(), 0));
                    TextView team_total_storage_size = (TextView) team_view.findViewById(R.id.team_total_storage_size);
                    team_total_storage_size.setText(ListFunctionUtils.formatCalculatedSize(item.getVolume(), 0));

                    int count = item.getMemberCount();

                    if (Utils.checkIsTablet(mMainContext)) {
                        LinearLayout user_list_layout = (LinearLayout) team_view.findViewById(R.id.user_list_layout);
                        user_list_layout.removeAllViews();

                        for (int num = 0; num < count; num++) {
                            View user_view = inflater.inflate(R.layout.tablet_list_multifrag_team_user, null, false);
                            final ImageView team_user_profile = (ImageView) user_view.findViewById(R.id.team_user_profile);
                            UserItem user = item.getUserList().get(num);
                            if (user.getEmail().equals(LoginActivity.mUserItem.getEmail())) {
                                team_user_profile.setBackgroundResource(R.drawable.stroke_peacock_circle);
                            }
                            if (user.getProfileThumb() != null && user.getProfileThumb().length() > 0) {
                                Log.i("team_user_profile5", "team_user_profile5 => " + user.getProfileThumb());
                                Glide.with(mMainContext).load(user.getProfileThumb()).asBitmap().centerCrop().placeholder(R.drawable.ic_profile_gray_24).into(new BitmapImageViewTarget(team_user_profile) {
                                    @Override
                                    protected void setResource(Bitmap resource) {
                                        RoundedBitmapDrawable circularBitmapDrawable =
                                                RoundedBitmapDrawableFactory.create(mMainContext.getResources(), resource);
                                        circularBitmapDrawable.setCircular(true);
                                        team_user_profile.setImageDrawable(circularBitmapDrawable);
                                    }
                                });
                            } else {
                                team_user_profile.setImageResource(R.drawable.ic_profile_gray_24);
                            }
                            user_list_layout.addView(user_view);
                        }
                        user_list_layout.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ((MainActivity) mMainContext).getTeamInfo(pos);
                            }
                        });
                    } else {
                        if (count > 0) {
                            final ImageView team_user_profile1 = (ImageView) team_view.findViewById(R.id.team_user_profile1);
                            team_user_profile1.setVisibility(View.VISIBLE);
                            UserItem user = item.getUserList().get(0);
                            if (user.getEmail().equals(LoginActivity.mUserItem.getEmail())) {
                                team_user_profile1.setBackgroundResource(R.drawable.stroke_peacock_circle);
                            }
                            if (user.getProfileThumb() != null && user.getProfileThumb().length() > 0) {
                                Log.i("team_user_profile1", "team_user_profile1 => " + user.getProfileThumb());
                                Glide.with(mMainContext).load(user.getProfileThumb()).asBitmap().centerCrop().placeholder(R.drawable.ic_profile_gray_24).into(new BitmapImageViewTarget(team_user_profile1) {
                                    @Override
                                    protected void setResource(Bitmap resource) {
                                        RoundedBitmapDrawable circularBitmapDrawable =
                                                RoundedBitmapDrawableFactory.create(mMainContext.getResources(), resource);
                                        circularBitmapDrawable.setCircular(true);
                                        team_user_profile1.setImageDrawable(circularBitmapDrawable);
                                    }
                                });
                            } else {
                                team_user_profile1.setImageResource(R.drawable.ic_profile_gray_24);
                            }

//                        team_user_profile1.setOnClickListener(new OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                Intent intent = new Intent(mMainContext, MemberManageActivity.class);
//                                intent.putExtra("UserItem", user);
//                                intent.putExtra("TeamItem", item);
//                                startActivity(intent);
//                            }
//                        });
                        }
                        if (count > 1) {
                            final ImageView team_user_profile2 = (ImageView) team_view.findViewById(R.id.team_user_profile2);
                            team_user_profile2.setVisibility(View.VISIBLE);
                            UserItem user = item.getUserList().get(1);
                            if (user.getEmail().equals(LoginActivity.mUserItem.getEmail())) {
                                team_user_profile2.setBackgroundResource(R.drawable.stroke_peacock_circle);
                            }
                            if (user.getProfileThumb() != null && user.getProfileThumb().length() > 0) {
                                Log.i("team_user_profile2", "team_user_profile2 => " + user.getProfileThumb());
                                Glide.with(mMainContext).load(user.getProfileThumb()).asBitmap().centerCrop().placeholder(R.drawable.ic_profile_gray_24).into(new BitmapImageViewTarget(team_user_profile2) {
                                    @Override
                                    protected void setResource(Bitmap resource) {
                                        RoundedBitmapDrawable circularBitmapDrawable =
                                                RoundedBitmapDrawableFactory.create(mMainContext.getResources(), resource);
                                        circularBitmapDrawable.setCircular(true);
                                        team_user_profile2.setImageDrawable(circularBitmapDrawable);
                                    }
                                });
                            } else {
                                team_user_profile2.setImageResource(R.drawable.ic_profile_gray_24);
                            }

//                        team_user_profile2.setOnClickListener(new OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                Intent intent = new Intent(mMainContext, MemberManageActivity.class);
//                                intent.putExtra("UserItem", user);
//                                intent.putExtra("TeamItem", item);
//                                startActivity(intent);
//                            }
//                        });
                        }
                        if (count > 2) {
                            final ImageView team_user_profile3 = (ImageView) team_view.findViewById(R.id.team_user_profile3);
                            team_user_profile3.setVisibility(View.VISIBLE);
                            UserItem user = item.getUserList().get(2);
                            if (user.getEmail().equals(LoginActivity.mUserItem.getEmail())) {
                                team_user_profile3.setBackgroundResource(R.drawable.stroke_peacock_circle);
                            }
                            if (user.getProfileThumb() != null && user.getProfileThumb().length() > 0) {
                                Log.i("team_user_profile3", "team_user_profile3 => " + user.getProfileThumb());
                                Glide.with(mMainContext).load(user.getProfileThumb()).asBitmap().centerCrop().placeholder(R.drawable.ic_profile_gray_24).into(new BitmapImageViewTarget(team_user_profile3) {
                                    @Override
                                    protected void setResource(Bitmap resource) {
                                        RoundedBitmapDrawable circularBitmapDrawable =
                                                RoundedBitmapDrawableFactory.create(mMainContext.getResources(), resource);
                                        circularBitmapDrawable.setCircular(true);
                                        team_user_profile3.setImageDrawable(circularBitmapDrawable);
                                    }
                                });
                            } else {
                                team_user_profile3.setImageResource(R.drawable.ic_profile_gray_24);
                            }

//                        team_user_profile3.setOnClickListener(new OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                Intent intent = new Intent(mMainContext, MemberManageActivity.class);
//                                intent.putExtra("UserItem", user);
//                                intent.putExtra("TeamItem", item);
//                                startActivity(intent);
//                            }
//                        });
                        }
                        if (count > 3) {
                            final ImageView team_user_profile4 = (ImageView) team_view.findViewById(R.id.team_user_profile4);
                            team_user_profile4.setVisibility(View.VISIBLE);
                            UserItem user = item.getUserList().get(3);
                            if (user.getEmail().equals(LoginActivity.mUserItem.getEmail())) {
                                team_user_profile4.setBackgroundResource(R.drawable.stroke_peacock_circle);
                            }
                            if (user.getProfileThumb() != null && user.getProfileThumb().length() > 0) {
                                Log.i("team_user_profile4", "team_user_profile4 => " + user.getProfileThumb());
                                Glide.with(mMainContext).load(user.getProfileThumb()).asBitmap().centerCrop().placeholder(R.drawable.ic_profile_gray_24).into(new BitmapImageViewTarget(team_user_profile4) {
                                    @Override
                                    protected void setResource(Bitmap resource) {
                                        RoundedBitmapDrawable circularBitmapDrawable =
                                                RoundedBitmapDrawableFactory.create(mMainContext.getResources(), resource);
                                        circularBitmapDrawable.setCircular(true);
                                        team_user_profile4.setImageDrawable(circularBitmapDrawable);
                                    }
                                });
                            } else {
                                team_user_profile4.setImageResource(R.drawable.ic_profile_gray_24);
                            }

//                        team_user_profile4.setOnClickListener(new OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                Intent intent = new Intent(mMainContext, MemberManageActivity.class);
//                                intent.putExtra("UserItem", user);
//                                intent.putExtra("TeamItem", item);
//                                startActivity(intent);
//                            }
//                        });
                        }
                        if (count > 4) {
                            final ImageView team_user_profile5 = (ImageView) team_view.findViewById(R.id.team_user_profile5);
                            team_user_profile5.setVisibility(View.VISIBLE);
                            UserItem user = item.getUserList().get(4);
                            if (user.getEmail().equals(LoginActivity.mUserItem.getEmail())) {
                                team_user_profile5.setBackgroundResource(R.drawable.stroke_peacock_circle);
                            }
                            if (user.getProfileThumb() != null && user.getProfileThumb().length() > 0) {
                                Log.i("team_user_profile5", "team_user_profile5 => " + user.getProfileThumb());
                                Glide.with(mMainContext).load(user.getProfileThumb()).asBitmap().centerCrop().placeholder(R.drawable.ic_profile_gray_24).into(new BitmapImageViewTarget(team_user_profile5) {
                                    @Override
                                    protected void setResource(Bitmap resource) {
                                        RoundedBitmapDrawable circularBitmapDrawable =
                                                RoundedBitmapDrawableFactory.create(mMainContext.getResources(), resource);
                                        circularBitmapDrawable.setCircular(true);
                                        team_user_profile5.setImageDrawable(circularBitmapDrawable);
                                    }
                                });
                            } else {
                                team_user_profile5.setImageResource(R.drawable.ic_profile_gray_24);
                            }

//                        team_user_profile5.setOnClickListener(new OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                Intent intent = new Intent(mMainContext, MemberManageActivity.class);
//                                intent.putExtra("UserItem", user);
//                                intent.putExtra("TeamItem", item);
//                                startActivity(intent);
//                            }
//                        });
                        }

                        if (count > 5) {
                            TextView team_user_profile6 = (TextView) team_view.findViewById(R.id.team_user_profile6);
                            team_user_profile6.setVisibility(View.VISIBLE);
                            team_user_profile6.setText("+" + (count - 5));

                            team_user_profile6.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(mMainContext, TeamManagentActivity.class);
                                    intent.putExtra("TeamItem", item);
                                    ((MainActivity) mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_TEAM_MANAGEMENT_ACTIVITY);
                                }
                            });
                        }

                    }

                    LinearLayout team_move_layout = (LinearLayout) team_view.findViewById(R.id.team_move_layout);
                    if (move) {
                        team_move_layout.setVisibility(View.VISIBLE);
                        TextView team_up_bt = (TextView) team_view.findViewById(R.id.team_up_bt);
                        team_up_bt.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (pos > 0) {
                                    Collections.swap(teamItams, pos - 1, pos);
                                    displayTeamList(TEAM_MORE, TEAM_MOVE_MODE);
                                }
                            }
                        });
                        TextView team_down_bt = (TextView) team_view.findViewById(R.id.team_down_bt);
                        team_down_bt.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (pos < totalCount) {
                                    Collections.swap(teamItams, pos, pos + 1);
                                    displayTeamList(TEAM_MORE, TEAM_MOVE_MODE);
                                }
                            }
                        });
                    } else {
                        team_move_layout.setVisibility(View.GONE);
                    }

                    team_view.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            getActivity().getSupportFragmentManager().beginTransaction()
//                                    .replace(R.id.content_frame, new FolderFragment(mMainContext, Utils.getInternalDirectoryPath(), true))
//                                    .commit();
//                            MainActivity.homeScrollX = home_scrollview.getScrollX();
//                            MainActivity.homeScrollY = home_scrollview.getScrollY();
                            //((MainActivity)mMainContext).moveToStorageRoot(pos);
                            ((MainActivity) mMainContext).getTeamInfo(pos);
                            //moveToStargeRoot(pos);
                        }
                    });
                    team_storage_content_layout.addView(team_view);

                    if ((i > (TEAM_MORE_COUNT - 1)) && more) {
                        while (i < totalCount) {
                            TeamItem check = teamItams.get(i++);
                            if (check.getMyRole().equals("leader")) {
                                myTeam = true;
                            }
                        }
                        break;
                    }


                    TextView team_list_inappbilling = (TextView) team_view.findViewById(R.id.team_list_inappbilling);
                    if (item.getTeamLeader()) {
                        // premium inapp
                        team_list_inappbilling.setVisibility(View.GONE);
//                        team_list_inappbilling.setVisibility(View.VISIBLE);
                        team_list_inappbilling.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(mMainContext, InAppBillingActivity.class);
                                ((MainActivity) mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_INAPP_BILLING_ACTIVITY);
                            }
                        });
                    } else {
                        team_list_inappbilling.setVisibility(View.GONE);
                    }
                }

                if (myTeam) {
                    team_storage_add_bt.setVisibility(View.GONE);
                } else {
                    team_storage_add_bt.setVisibility(View.VISIBLE);
                }
            } else {
                team_storage_add_bt.setVisibility(View.VISIBLE);
                View team_view = inflater.inflate(R.layout.list_multifrag_team_init, null, false);
                TextView team_info_tv = (TextView) team_view.findViewById(R.id.team_info_tv);
                team_info_tv.setText(LoginActivity.mUserName + " " + getString(R.string.team_make_storage_title));
                TextView team_info_sub_tv = (TextView) team_view.findViewById(R.id.team_info_sub_tv);
                team_info_sub_tv.setText(getString(R.string.team_make_storage_info));
                team_view.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addTeam();
                    }
                });
                team_storage_content_layout.addView(team_view);
            }
            if (more) {
                if (teamItams.size() > TEAM_MORE_COUNT) {
                    more_team_list_bt.setVisibility(View.VISIBLE);
                    more_team_list_bt_iv.setImageResource(R.mipmap.list_dropdown);
                } else {
                    more_team_list_bt.setVisibility(View.GONE);
                }
            } else {
                if (teamItams.size() > TEAM_MORE_COUNT) {
                    more_team_list_bt.setVisibility(View.VISIBLE);
                    more_team_list_bt_iv.setImageResource(R.mipmap.list_dropup);
                } else {
                    more_team_list_bt.setVisibility(View.GONE);
                }
            }
        } else {
            team_visiable_layout.setVisibility(View.INVISIBLE);
            View team_view = inflater.inflate(R.layout.list_multifrag_team_init, null, false);
            team_view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Intent intent = new Intent(mMainContext, SignUpActivity.class);
//                    getActivity().startActivityForResult(intent, MainActivity.REQUEST_CODE_SIGN_UP_ACTIVITY);
                    Intent intent = new Intent(mMainContext, LoginActivity.class);
                    ((MainActivity) mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_LOGIN_ACTIVITY);
                }
            });
            team_storage_content_layout.addView(team_view);
        }
    }

    public void displayTagList() {
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        ArrayList<TagItem> tagItams = MainActivity.mMds.getAllTag();
        tag_content_layout.removeAllViews();
        if (tagItams != null && tagItams.size() > 0) {
            tag_empty_layout.setVisibility(View.GONE);

            int totalCount = tagItams.size();
            if (totalCount > 5) {
                more_tag_list_bt.setVisibility(View.VISIBLE);
                more_tag_list_bt.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TAG_MORE = !TAG_MORE;
                        displayTagList();
                    }
                });
                if (TAG_MORE) {
                    totalCount = 5;
                    more_tag_list_bt_iv.setImageResource(R.mipmap.list_dropdown);
                } else {
                    more_tag_list_bt_iv.setImageResource(R.mipmap.list_dropup);
                }
            } else {
                more_tag_list_bt.setVisibility(View.GONE);
            }
            for (int i = 0; i < totalCount; ) {
//            }
//            for(final TagItem item : tagItams){
                final TagItem item_1 = tagItams.get(i++);
                View tag_view = inflater.inflate(R.layout.frag_multi_tag_list_item, null, false);

                TextView tag_name = tag_view.findViewById(R.id.tag_name);
                tag_name.setText(item_1.getName());
                TextView tag_count = tag_view.findViewById(R.id.tag_count);
                tag_count.setText("" + item_1.getCount());
                LinearLayout tag_layout = tag_view.findViewById(R.id.tag_layout);
                tag_layout.setOnClickListener(v -> {
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_frame, new CategoryFragment(mMainContext, 1, item_1))
                            .commit();
                    MainActivity.homeScrollX = home_scrollview.getScrollX();
                    MainActivity.homeScrollY = home_scrollview.getScrollY();
                });
                tag_layout.setOnLongClickListener(v -> {
                    dialogDeleteTag(item_1);
                    //dialogNewTag(item_1);
                    return false;
                });
                tag_content_layout.addView(tag_view);
            }
        }
    }

    public void dialogDeleteTag(final TagItem tag) {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.dialog_base, null, false);
        //LinearLayout dialog_tag_list_layout = (LinearLayout)view.findViewById(R.id.dialog_tag_list_layout);
        //LinearLayout dialog_tag_list_layout = (LinearLayout)view.findViewById(R.id.dialog_copy_info_icon);

        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.dialog_delete_tag));

        TextView content = (TextView) view.findViewById(R.id.content);
        content.setText(getResources().getString(R.string.dialog_delete_tag_info));

        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                displayTagList();
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                pr_dialog = ProgressDialog.show(mMainContext, null,
                        mMainContext.getResources().getString(R.string.action_deleting));
                pr_dialog.setCanceledOnTouchOutside(true);
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList<FileItem> tagFiles = ListFunctionUtils.listTagFiles(tag.getName(), mMainContext);
                        for (FileItem tagFile : tagFiles) {
                            MainActivity.mMds.deleteTag(tagFile.getId());
                        }
                        pr_dialog.dismiss();
                        ((MainActivity) mMainContext).displayTeamList();
                        displayTagList();
                        Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_delete), Toast.LENGTH_SHORT).show();
                    }
                }, 0);
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void setSDcardMemoryLayout(boolean visiable) {
        if (visiable) {
            sd_card_memory_layout.setVisibility(View.VISIBLE);
            sd_card_visiable_layout.setVisibility(View.VISIBLE);
            recent_layout.setVisibility(View.GONE);
            download_layout.setVisibility(View.GONE);
            long sdTotal = Utils.getTotalSDCaredMemorySize();
            long sdFree = Utils.getSDCardMemorySize();
            sdcard_total_size.setText(ListFunctionUtils.formatCalculatedSize(sdTotal, 0));
            sdcard_use_size.setText(ListFunctionUtils.formatCalculatedSize(sdTotal - sdFree, 1));
            int sdcard_size = 0;
            if (sdTotal > 0 && sdFree > 0) {
                sdcard_size = (int) ((sdFree * 100) / sdTotal);
            }
            sd_card_memory.setProgress(100 - sdcard_size);
            //sd_card_memory.setProgress(sdcard_size);
        } else {
            sd_card_memory_layout.setVisibility(View.GONE);
            sd_card_visiable_layout.setVisibility(View.GONE);
            recent_layout.setVisibility(View.VISIBLE);
            download_layout.setVisibility(View.VISIBLE);
        }
    }

    public void logoutAction() {
        teamItams = null;
        displayTeamList(false, false);
    }

    public void loginActionOri() {
        //teamItams = teamTestData();
        if (teamItams != null) {
            displayTeamList(TEAM_MORE, false);
        } else {
            try {
                teamItams = new ArrayList<>();
                JMF_network.getTeamList()
                        .flatMap(gigaPodPair -> {
                            return Observable.just(gigaPodPair);
                        })
                        .doOnError(throwable -> {
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(gigaPodPair -> {
                                    if (gigaPodPair.first) {
                                        Log.i("JMF getTeamList", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                        JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                        JsonArray rows = result.getAsJsonArray("rows");
                                        for (int i = 0; i < rows.size(); i++) {
                                            TeamItem item = new TeamItem();
                                            JsonObject row = rows.get(i).getAsJsonObject();
                                            item.setId(row.get("team_id").getAsString());
                                            item.setTeamRole(row.get("role").getAsString());
                                            item.setTeamLeader(row.get("role").getAsString().equals("leader") ? true : false);
                                            JsonObject team = row.getAsJsonObject("Team");
                                            item.setTeamName(team.get("name").getAsString());
                                            item.setVolume(team.get("volume").getAsLong());
                                            item.setVolumeUsage(team.get("volume_usage").getAsLong());
//                                            ArrayList<UserItem> userItems = new ArrayList<>();
//                                            UserItem user = new UserItem();
//                                            user.setMaster(true);
//                                            user.setName("김남오");
//                                            user.setProfile("https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/26056134_1512623652118681_8894216410971930025_n.jpg?oh=0c96ee55b7f3e4b414a0b3027002793e&oe=5B0E540A");
//                                            userItems.add(user);
//                                            UserItem user1 = new UserItem();
//                                            user1.setName("박호영");
//                                            user1.setProfile("https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/11986412_1004742329597609_7674336604028008920_n.jpg?oh=51fcb43822f1fe3fbd83d911519d4b3d&oe=5B061417");
//                                            userItems.add(user1);
//                                            item.setUserList(userItems);
                                            teamItams.add(item);
                                        }
                                        SortUtils.sortTeam(teamItams);
                                        //displayTeamList(true, false);
                                        getTeamInfo();
                                    } else {
                                        Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                        displayTeamList(true, false);
                                    }
                                },
                                throwable -> {
                                    throwable.printStackTrace();
                                    displayTeamList(true, false);
                                });

            } catch (Exception e) {
                e.printStackTrace();
                displayTeamList(true, false);
                Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void loginAction() {
        //teamItams = teamTestData();
        if (teamItams != null) {
            displayTeamList(TEAM_MORE, false);
        } else {
            try {
                teamItams = new ArrayList<>();
                JMF_network.getHomeData()
                        .flatMap(gigaPodPair -> {
                            return Observable.just(gigaPodPair);
                        })
                        .doOnError(throwable -> {
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(gigaPodPair -> {
                                    if (gigaPodPair.first) {
                                        Log.i("JMF getTeamList", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                        JsonArray result = ((JsonObject) gigaPodPair.second).getAsJsonArray("result");
                                        for (int i = 0; i < result.size(); i++) {
                                            TeamItem item = new TeamItem();
                                            JsonObject row = result.get(i).getAsJsonObject();
                                            item.setMyRole(row.get("my_role").getAsString());
                                            JsonObject team = row.getAsJsonObject("team");
                                            item.setId(team.get("team_id").getAsString());
                                            item.setTeamRole(team.get("role").getAsString());
                                            item.setTeamLeader(team.get("role").getAsString().equals("leader") ? true : false);
                                            item.setTeamName(team.get("name").getAsString());
                                            item.setVolume(team.get("volume").getAsLong());
                                            item.setVolumeUsage(team.get("volume_usage").getAsLong());
                                            item.setJoinRule(team.get("join_rule").getAsString());
                                            item.setJoinProtectDomain(team.get("join_protect_domain").getAsString());
                                            item.setFlagPush(team.get("flag_push").getAsString());
                                            item.setTrashOption(team.get("trash_option").getAsInt());
                                            item.setStatus(team.get("status").getAsInt());
                                            item.setRegdate(Utils.formatDate(team.get("regdate").getAsString()));
                                            item.setModified(Utils.formatDate(team.get("modified").getAsString()));
                                            item.setMemberCount(team.get("member_count").getAsInt());

                                            JsonArray team_member = row.getAsJsonArray("team_member");
                                            ArrayList<UserItem> userItems = new ArrayList<>();
                                            for (int j = 0; j < team_member.size(); j++) {
                                                JsonObject member = team_member.get(j).getAsJsonObject();
                                                UserItem userItem = new UserItem();
                                                userItem.setMemberID(member.get("team_member_id").getAsString());
                                                userItem.setLeader(member.get("role").getAsString().equals("leader") ? true : false);
                                                userItem.setEmail(member.get("email").getAsString());
                                                userItem.setName(member.get("name").getAsString());
                                                userItem.setProfile(member.get("profile").getAsString());
                                                userItem.setProfileThumb(member.get("profile_thumb").getAsString());
                                                //userItem.setGrade(member.get("grade").getAsString());
                                                userItem.setStatus(member.get("status").getAsInt());
                                                userItem.setRegdate(Utils.formatDate(member.get("regdate").getAsString()));
//                                                    userItem.setName("김남오");
//                                                    userItem.setProfile("https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/26056134_1512623652118681_8894216410971930025_n.jpg?oh=0c96ee55b7f3e4b414a0b3027002793e&oe=5B0E540A");
                                                userItems.add(userItem);
                                            }
                                            item.setUserList(userItems);
                                            teamItams.add(item);
                                        }
                                        SortUtils.sortTeam(teamItams);
                                    } else {
                                        Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                    }
                                    displayTeamList(true, false);
                                },
                                throwable -> {
                                    throwable.printStackTrace();
                                    displayTeamList(true, false);
                                });

            } catch (Exception e) {
                e.printStackTrace();
                displayTeamList(true, false);
                Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void getTeamInfo() {
        //teamItams = teamTestData();
        if (teamItams != null && teamItams.size() > 0) {
            try {
                for (TeamItem teamItem : teamItams) {
                    if (teamItem.getUserList() == null) {
                        JMF_network.getTeamInfo(teamItem.getId())
                                .flatMap(gigaPodPair -> {
                                    return Observable.just(gigaPodPair);
                                })
                                .doOnError(throwable -> {
                                })
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(gigaPodPair -> {
                                            if (gigaPodPair.first) {
                                                Log.i("JMF getTeamInfo", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                                JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                                JsonObject team = result.getAsJsonObject("team");
                                                teamItem.setMyRole(result.get("my_role").getAsString());
                                                teamItem.setId(team.get("team_id").getAsString());
                                                //teamItem.setUserId(team.get("user_id").getAsString());
                                                //teamItem.setDriveId(team.get("drive_id").getAsString());
                                                teamItem.setTeamName(team.get("name").getAsString());
                                                teamItem.setVolume(team.get("volume").getAsLong());
                                                teamItem.setVolumeUsage(team.get("volume_usage").getAsLong());
                                                teamItem.setJoinRule(team.get("join_rule").getAsString());
                                                teamItem.setJoinProtectDomain(team.get("join_protect_domain").getAsString());
                                                teamItem.setFlagPush(team.get("flag_push").getAsString());
                                                teamItem.setTrashOption(team.get("trash_option").getAsInt());
                                                teamItem.setStatus(team.get("status").getAsInt());
                                                teamItem.setRegdate(Utils.formatDate(team.get("regdate").getAsString()));
                                                teamItem.setModified(Utils.formatDate(team.get("modified").getAsString()));

                                                JsonArray wait_member = result.getAsJsonArray("wait_member");
                                                ArrayList<UserItem> waitItems = new ArrayList<>();
                                                for (int i = 0; i < wait_member.size(); i++) {
                                                    JsonObject waitMember = wait_member.get(i).getAsJsonObject();
                                                    UserItem userItem = new UserItem();
                                                    userItem.setMemberID(waitMember.get("team_member_id").getAsString());
                                                    userItem.setLeader(waitMember.get("role").getAsString().equals("leader") ? true : false);
                                                    userItem.setEmail(waitMember.get("email").getAsString());
                                                    userItem.setName(waitMember.get("name").getAsString());
                                                    userItem.setProfile(waitMember.get("profile").getAsString());
                                                    userItem.setProfileThumb(waitMember.get("profile_thumb").getAsString());
                                                    //userItem.setGrade(member.get("grade").getAsString());
                                                    userItem.setStatus(waitMember.get("status").getAsInt());
                                                    userItem.setRegdate(Utils.formatDate(waitMember.get("regdate").getAsString()));
//                                                    userItem.setName("김남오");
//                                                    userItem.setProfile("https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/26056134_1512623652118681_8894216410971930025_n.jpg?oh=0c96ee55b7f3e4b414a0b3027002793e&oe=5B0E540A");
                                                    waitItems.add(userItem);
                                                }
                                                teamItem.setWaitList(waitItems);


                                                JsonArray team_member = result.getAsJsonArray("team_member");
                                                ArrayList<UserItem> userItems = new ArrayList<>();
                                                for (int i = 0; i < team_member.size(); i++) {
                                                    JsonObject member = team_member.get(i).getAsJsonObject();
                                                    UserItem userItem = new UserItem();
                                                    userItem.setMemberID(member.get("team_member_id").getAsString());
                                                    userItem.setLeader(member.get("role").getAsString().equals("leader") ? true : false);
                                                    userItem.setEmail(member.get("email").getAsString());
                                                    userItem.setName(member.get("name").getAsString());
                                                    userItem.setProfile(member.get("profile").getAsString());
                                                    userItem.setProfileThumb(member.get("profile_thumb").getAsString());
                                                    //userItem.setGrade(member.get("grade").getAsString());
                                                    userItem.setStatus(member.get("status").getAsInt());
                                                    userItem.setRegdate(Utils.formatDate(member.get("regdate").getAsString()));
//                                                    userItem.setName("김남오");
//                                                    userItem.setProfile("https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/26056134_1512623652118681_8894216410971930025_n.jpg?oh=0c96ee55b7f3e4b414a0b3027002793e&oe=5B0E540A");
                                                    userItems.add(userItem);
                                                }
                                                teamItem.setUserList(userItems);
                                            } else {
                                                Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                                teamItem.setUserList(new ArrayList<>());
                                            }
                                            getTeamInfo();
                                        },
                                        throwable -> {
                                            throwable.printStackTrace();
                                            displayTeamList(true, false);
                                        });
                        return;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                displayTeamList(true, false);
                Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
            }
        }
        displayTeamList(true, false);
    }

//    public void moveToStargeRoot(int position) {
//        TeamItem selectItem = teamItams.get(position);
//        try {
//            ((MainActivity)mMainContext).showProgress();
//            JMF_network.getRootPath(selectItem.getId())
//                    .flatMap(gigaPodPair -> {
//                        return Observable.just(gigaPodPair);
//                    })
//                    .doOnError(throwable -> {
//                        ((MainActivity)mMainContext).dismissProgress();
//                    })
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(gigaPodPair -> {
//                                ((MainActivity)mMainContext).dismissProgress();
//                                if (gigaPodPair.first) {
//                                    Log.i("JMF getRootPath", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
//                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
//                                    JsonObject current = result.getAsJsonObject("current");
//                                    CurrentItem currentItem = new CurrentItem();
//                                    currentItem.setObjectId(current.get("object_id").getAsString());
//                                    currentItem.setNode(current.get("node").getAsString());
//                                    currentItem.setName(current.get("name").getAsString());
//                                    currentItem.setSize(current.get("size").getAsString());
//                                    currentItem.setRegdate(Utils.formatDate(current.get("regdate").getAsString()));
//                                    currentItem.setModified(Utils.formatDate(current.get("modified").getAsString()));
//
//                                    JsonObject lists = result.getAsJsonObject("lists");
//                                    JsonArray rows = lists.getAsJsonArray("rows");
//                                    ArrayList<FileItem> storageList = new ArrayList<FileItem>();
//                                    for(int i=0; i < rows.size(); i++) {
//                                        FileItem item = new FileItem();
//                                        JsonObject row = rows.get(i).getAsJsonObject();
//                                        item.setId(row.get("object_id").getAsString());
//                                        item.setSObjectType(row.get("object_type").getAsString());
//                                        item.setSNode(row.get("node").getAsString());
//                                        item.setName(row.get("name").getAsString());
//                                        item.setSCreator(row.get("creator").getAsString());
//                                        item.setSCreatorName(row.get("creator_name").getAsString());
//                                        item.setSMemo(row.get("memo").getAsString());
//                                        item.setSize(row.get("size").getAsString());
//                                        item.setSExtension(row.get("extension").getAsString());
//                                        item.setRegDate(Utils.formatDate(row.get("regdate").getAsString()));
//                                        item.setSModifedDate(Utils.formatDate(row.get("modified").getAsString()));
//
//                                        storageList.add(item);
//                                    }
//                                    StorageListItem storageListItem = new StorageListItem(currentItem, storageList);
//                                    getActivity().getSupportFragmentManager().beginTransaction()
//                                            .replace(R.id.content_frame, new StorageFragment(mMainContext, selectItem, storageListItem))
//                                            .commit();
//                                    //Toast.makeText(mMainContext, "object_id => " + object_id, Toast.LENGTH_SHORT).show();
//                                } else {
//                                    Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
//                                }
//                            },
//                            throwable -> {
//                                ((MainActivity)mMainContext).dismissProgress();
//                                throwable.printStackTrace();
//                            });
//        } catch (Exception e) {
//            ((MainActivity)mMainContext).dismissProgress();
//            e.printStackTrace();
//            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
//        }
//    }

//    private ArrayList<TeamItem> teamTestData(){
//        ArrayList<TeamItem> teamItams = new ArrayList<>();
//        TeamItem item1 = new TeamItem();
//        item1.setTeamName("김남오팀");
//        item1.setVolume(200000000);
//        item1.setVolumeUsage(100000);
//        ArrayList<UserItem> userItems = new ArrayList<>();
//        UserItem user = new UserItem();
//        user.setLeader(true);
//        user.setName("김남오");
//        user.setProfile("https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/26056134_1512623652118681_8894216410971930025_n.jpg?oh=0c96ee55b7f3e4b414a0b3027002793e&oe=5B0E540A");
//        userItems.add(user);
//        UserItem user1 = new UserItem();
//        user1.setName("박호영");
//        user1.setProfile("https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/11986412_1004742329597609_7674336604028008920_n.jpg?oh=51fcb43822f1fe3fbd83d911519d4b3d&oe=5B061417");
//        userItems.add(user1);
//        item1.setUserList(userItems);
//        teamItams.add(item1);
//
//        for(int i=1; i < 4; i++){
//            TeamItem item2 = new TeamItem();
//            item2.setTeamName("TEST팀"+i);
//            item2.setVolume(200000000);
//            item2.setVolumeUsage(500000);
//            ArrayList<UserItem> userItems1 = new ArrayList<>();
//
//            UserItem user2 = new UserItem();
//            user2.setLeader(true);
//            user2.setName("test");
//            user2.setProfile("https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/20246217_1552333218174131_2368295000402953678_n.jpg?oh=a3d44842a772d66c8e498e16e497debd&oe=5B497080");
//            userItems1.add(user2);
//
//            for(int j=0; j < 8; j++){
//                Random rand = new Random();
//                int  n = rand.nextInt(10);
//
//                UserItem user3 = new UserItem();
//                user3.setName("test1"+j);
//                if(n%6 == 0){
//                    user3.setProfile("https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/20621283_1622771937755748_5307532924490429287_n.jpg?oh=ee2c03ffb3053259a07be2044da56ac3&oe=5B4773A4");
//                }else if(n%6 == 1){
//                    user3.setProfile("https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/27072333_1682849905107872_7688435776971437974_n.jpg?oh=ba476c7e2cc2c1a5b928af5a36ff8edb&oe=5B163D2F");
//                }else if(n%6 == 2){
//                    user3.setProfile("https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/14725602_1425203887493297_2686733455472652214_n.jpg?oh=bc56f0f3a3fa3ae068714a442cc08e01&oe=5B11FEB5");
//                }else if(n%6 == 3){
//                    user3.setProfile("https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/1521628_637783526278992_1346716620_n.jpg?oh=ddd2b155dadcd2246979f3472ca9d277&oe=5AFFDF8B");
//                }else if(n%6 == 4){
//                    user3.setProfile("https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/14708115_1818210845083239_1455253238175493487_n.jpg?oh=423c27fc080544fef21de220bc155c37&oe=5B147212");
//                }else{
//                    user3.setProfile("https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/10557425_1538886816332465_810087931664631230_n.jpg?oh=236b9c18a889d875b4f9810421d7af9e&oe=5B4BECDD");
//                }
//                userItems1.add(user3);
//            }
//            item2.setUserList(userItems1);
//            teamItams.add(item2);
//        }
//
//        return teamItams;
//    }

    // TM Browser banner
    private void implementSwipeDismiss() {
        SwipeDismissBehavior swipeDismissBehavior = new SwipeDismissBehavior();
        swipeDismissBehavior.setSwipeDirection(SwipeDismissBehavior.SWIPE_DIRECTION_START_TO_END);//Swipe direction i.e any direction, here you can put any direction LEFT or RIGHT
        swipeDismissBehavior.setListener(new SwipeDismissBehavior.OnDismissListener() {
            @Override
            public void onDismiss(View view) {
                SharedPreferenceUtil.getInstance().setDelayShowAdTMBrowser(mMainContext, System.currentTimeMillis());
                bannerCardView.setVisibility(View.GONE);
            }

            @Override
            public void onDragStateChanged(int state) {

            }
        });
        CoordinatorLayout.LayoutParams layoutParams =
                (CoordinatorLayout.LayoutParams) bannerCardView.getLayoutParams();
        layoutParams.setBehavior(swipeDismissBehavior);//set swipe behaviour to Coordinator layout
    }

    private void bannerClose() {

        Animation anim = AnimationUtils.loadAnimation(mMainContext, R.anim.slide_out_right);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                SharedPreferenceUtil.getInstance().setDelayShowAdTMBrowser(mMainContext, System.currentTimeMillis());
                bannerCardView.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        bannerCardView.startAnimation(anim);
    }
}
