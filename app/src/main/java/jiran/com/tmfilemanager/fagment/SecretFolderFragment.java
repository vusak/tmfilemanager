package jiran.com.tmfilemanager.fagment;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.InitActivity;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.activity.viewer.ImageSlideViewerActivity;
import jiran.com.tmfilemanager.adapter.SecretFolderAdapter;
import jiran.com.tmfilemanager.common.AlertCallback;
import jiran.com.tmfilemanager.common.ClipBoard;
import jiran.com.tmfilemanager.common.FolderSelectTotalDialog;
import jiran.com.tmfilemanager.common.MimeTypes;
import jiran.com.tmfilemanager.common.SortUtils;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.data.SecretFileItem;
import jiran.com.tmfilemanager.settings.MyFileSettings;

/**
 * Created by user on 2016-08-03.
 */


public class SecretFolderFragment extends Fragment {

    public static boolean mActionMode = false;
    public static boolean mPasswordMode = true;
    public Context mMainContext;

    //public int mListType = 0;
    public String mCurrentPath;
    public String mStartPath;
    Dialog mDialog;
    HashMap<String, Integer> listPositionMap;
    TextView title_info;
    TextView comment;
    //    LinearLayout underbar_skyblue;
//    LinearLayout underbar_red;
    TextView password_1;
    TextView password_2;
    TextView password_3;
    TextView password_4;
    TextView forgot_password_btn;
    TextView cancel, ok;
    private String mPassword = "";
    private String mConfirmPassword = "";
    private AbsListView mListView;
    private GridView mGridView;
    private LinearLayout mFoler_path_layout, mHide_layout;
    private HorizontalScrollView foler_path_scroll_layout;
    private SecretFolderAdapter mSecretFolderAdapter;
    //private ImageView list_bottom_menu_paste;
    private LinearLayout list_bottom_menu;
    private ImageView list_bottom_menu_sort;
    private ImageView list_bottom_menu_check;

    public SecretFolderFragment() {
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public SecretFolderFragment(Context context, String path, boolean startBack) {
        mMainContext = context;
        mCurrentPath = path;
        if (startBack) {
            mStartPath = path;
        } else {
            mStartPath = null;
        }
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public SecretFolderFragment(Context context, String currentPath) {
        mMainContext = context;
        //mStartPath = currentPath;
        mCurrentPath = currentPath;
        setRetainInstance(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mMainContext == null) {
            Intent intent = new Intent(getActivity(), InitActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        }
        ((MainActivity) mMainContext).toolbar.setBackgroundResource(R.color.secret_top_bg);
        View rootView = inflater.inflate(R.layout.frag_secretfolder, container, false);
        initLayout(inflater, rootView);
        //String password = PreferenceHelper.getString(PreferenceHelper.password, "");
        String password = MainActivity.mMds.getSecretUserPassword();
        if (password != null && password.length() > 0) {
            checkPassword(false);
        } else {
            initSetting();
        }
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        ((MainActivity) mMainContext).finishSearchMode();
        ((MainActivity) mMainContext).toolbarMenu(R.menu.secret_mode);
        ((MainActivity) getActivity()).setTitle(getResources().getString(R.string.left_menu_secretfolder));
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {

        if (mPasswordMode && mDialog != null && !mDialog.isShowing()) {
            checkPassword(false);
        }
        if (!mPasswordMode) {
            mPasswordMode = true;
        }

        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void initLayout(LayoutInflater inflater, View rootView) {
        listPositionMap = new HashMap<String, Integer>();

        foler_path_scroll_layout = (HorizontalScrollView) rootView.findViewById(R.id.foler_path_scroll_layout);
        mFoler_path_layout = (LinearLayout) rootView.findViewById(R.id.foler_path_layout);
        mHide_layout = (LinearLayout) rootView.findViewById(R.id.hide_layout);

        mListView = (ListView) rootView.findViewById(R.id.secretfolder_listview);
        mListView.setEmptyView(rootView.findViewById(R.id.empty_layout));
        mGridView = (GridView) rootView.findViewById(R.id.secretfolder_gridview);
        mGridView.setEmptyView(rootView.findViewById(R.id.empty_layout));

        list_bottom_menu = (LinearLayout) rootView.findViewById(R.id.list_bottom_menu);

        list_bottom_menu_sort = (ImageView) rootView.findViewById(R.id.list_bottom_menu_sort);
        list_bottom_menu_sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSetSort();
            }
        });
        list_bottom_menu_check = (ImageView) rootView.findViewById(R.id.list_bottom_menu_check);
        list_bottom_menu_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mActionMode) {
                    finishActionMode();
                } else {
                    setActionMode();
                }
            }
        });

        changeListView();
        finishActionMode();
    }

    public void changeListGridView() {
        Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();

    }

    public void changeListView() {
        changeActionModeList();
        mSecretFolderAdapter = new SecretFolderAdapter(getActivity(), null);
        mListView.setVisibility(View.VISIBLE);
        mGridView.setVisibility(View.GONE);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mActionMode) {
                    mSecretFolderAdapter.setCheck(position);
                    mSecretFolderAdapter.notifyDataSetChanged();
                } else {
                    final File file = new File(((SecretFileItem) mListView.getAdapter()
                            .getItem(position)).getCurrentPath());

                    if (file.isDirectory()) {
                        if (mStartPath == null) {
                            mStartPath = file.getAbsolutePath();
                        }
                        //Scroll Position Set
                        listPositionMap.put(mCurrentPath, mListView.getFirstVisiblePosition());
                        folderListDisplay(file.getAbsolutePath());
                    } else {
                        SecretFolderFragment.mPasswordMode = false;
                        if (MimeTypes.isPicture(((SecretFileItem) mListView.getAdapter().getItem(position)).getName())) {
                            Intent intent = new Intent(mMainContext, ImageSlideViewerActivity.class);
                            ImageSlideViewerActivity.mDataSource = mSecretFolderAdapter.getImageList();
                            ImageSlideViewerActivity.mPosition = 0;
                            for (FileItem item : ImageSlideViewerActivity.mDataSource) {
                                if (item.getPath().equals(file.getPath())) {
                                    break;
                                } else {
                                    ImageSlideViewerActivity.mPosition++;
                                }
                            }
                            ((MainActivity) mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_SLIDE_VIEWER);
                        } else {
                            Utils.openFile(mMainContext, file);
                        }

                    }
                }
            }
        });

        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                mSecretFolderAdapter.setCheck(position);
                setActionMode();
                return true;
            }
        });
        mListView.setAdapter(mSecretFolderAdapter);
        folderListDisplay(mCurrentPath);

    }

    public void setScrollPosition(String path) {
        //Scroll Position Set
        int position;
        if (listPositionMap.containsKey(path)) {
            position = listPositionMap.get(path);
            listPositionMap.remove(path);
        } else {
            position = 0;
        }
        mListView.setSelection(position);
    }

    public void folderListDisplay(String path) {
        mCurrentPath = path;
        setFolderFathLayout(mCurrentPath);
        foler_path_scroll_layout.post(new Runnable() {
            public void run() {
                foler_path_scroll_layout.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        });
        mSecretFolderAdapter.addFiles();
        //Scroll Position Set
        setScrollPosition(path);
    }

    public void setFolderFathLayout(String path) {
        //setFolderFathLayout(path, false);
    }

    public void setFolderFathLayout(String path, boolean search) {
        mFoler_path_layout.removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        //Home Image add
        String HHOOMMEEPATH = "";
        boolean internal = true;
        if (path.contains(Utils.getInternalDirectoryPath())) {
            internal = true;
            String[] lastPath = Utils.getInternalDirectoryPath().split("/");
            path = File.separator + "HHOOMMEE" + File.separator + lastPath[lastPath.length - 1] + path.replaceFirst(Utils.getInternalDirectoryPath(), "");
            HHOOMMEEPATH = Utils.getInternalDirectoryPath().replaceFirst(File.separator + lastPath[lastPath.length - 1], "");
        } else if (Utils.isSDcardDirectory(true) && path.contains(Utils.getSDcardDirectoryPath())) {
            internal = false;
            String[] lastPath = Utils.getSDcardDirectoryPath().split("/");
            path = File.separator + "HHOOMMEE" + File.separator + lastPath[lastPath.length - 1] + path.replaceFirst(Utils.getSDcardDirectoryPath(), "");
            HHOOMMEEPATH = Utils.getSDcardDirectoryPath().replaceFirst(File.separator + lastPath[lastPath.length - 1], "");
        }
        String[] arrayPath = path.split("/");
        if (path.contains("/") && arrayPath.length > 1) {

            String makePath = "";
            int homeIndex = -1;
            for (int i = 1; i < arrayPath.length; i++) {
                String sPath = arrayPath[i];
                if (sPath.equals("HHOOMMEE")) {
                    homeIndex = i;
                    View home_view = inflater.inflate(R.layout.frag_folder_path_item_home, null, false);
                    makePath += HHOOMMEEPATH;
                    home_view.setTag(makePath);
                    home_view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((MainActivity) mMainContext).initFragment();
                        }
                    });
                    mFoler_path_layout.addView(home_view);
                } else {
                    View path_view = inflater.inflate(R.layout.frag_folder_path_item, null, false);
                    TextView foler_path = (TextView) path_view.findViewById(R.id.foler_path);
                    if (homeIndex == (i - 1)) {
                        if (internal) {
                            foler_path.setText("/" + getResources().getString(R.string.internaldirectory));
                        } else {
                            foler_path.setText("/" + getResources().getString(R.string.sdcard));
                        }
                    } else {
                        foler_path.setText("/" + sPath);
                    }
                    makePath += ("/" + sPath);
                    path_view.setTag(makePath);
                    path_view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            folderListDisplay((String) v.getTag());
                        }
                    });
                    mFoler_path_layout.addView(path_view);
                }
            }
        } else {
            View path_view = inflater.inflate(R.layout.frag_folder_path_item, null, false);
            TextView foler_path = (TextView) path_view.findViewById(R.id.foler_path);
            if (search) {
                foler_path.setText("\"" + path + "\" " + getResources().getString(R.string.action_search_result));
            } else {
                if (path.equals(getResources().getString(R.string.category_document)) ||
                        path.equals(getResources().getString(R.string.category_recentfile)) ||
                        path.equals(getResources().getString(R.string.category_largefile)) ||
                        path.equals(getResources().getString(R.string.category_image)) ||
                        path.equals(getResources().getString(R.string.category_video)) ||
                        path.equals(getResources().getString(R.string.category_favorite)) ||
                        path.equals(getResources().getString(R.string.category_download)) ||
                        path.equals(getResources().getString(R.string.category_music))) {
                    foler_path.setText(File.separator + path);
                    View home_view = inflater.inflate(R.layout.frag_folder_path_item_home, null, false);
                    home_view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((MainActivity) mMainContext).initFragment();
                        }
                    });
                    mFoler_path_layout.addView(home_view);
                } else {
                    foler_path.setText(path);
                }
            }
            mFoler_path_layout.addView(path_view);
        }
    }

    public void setActionMode() {
        mActionMode = true;
        ((MainActivity) mMainContext).toolbar.getMenu().clear();
        ((MainActivity) mMainContext).toolbar.inflateMenu(R.menu.action_secret_mode);
        ((MainActivity) mMainContext).finishSearchMode();
        mSecretFolderAdapter.notifyDataSetChanged();
    }

    public void finishActionMode() {
        mActionMode = false;
        ((MainActivity) mMainContext).toolbar.getMenu().clear();
        //((MainActivity) mMainContext).toolbar.inflateMenu(R.menu.secret_mode);
        ((MainActivity) mMainContext).toolbarMenu(R.menu.secret_mode);

        mSecretFolderAdapter.setCheckClear();
        mSecretFolderAdapter.notifyDataSetChanged();

//        if (mSecretFolderAdapter.getCount() > 0) {
//            list_bottom_menu.setVisibility(View.VISIBLE);
//        } else
        {
            list_bottom_menu.setVisibility(View.GONE);
        }
    }

    public void changeActionModeList() {
        ((MainActivity) mMainContext).toolbar.getMenu().clear();
        ((MainActivity) mMainContext).toolbar.inflateMenu(R.menu.secret_mode);

    }

    public boolean onBackPressed() {
        if (mActionMode) {
            finishActionMode();
            return true;
        } else if (mCurrentPath != null && mCurrentPath.equals("/")) {
            ((MainActivity) mMainContext).initFragment();
            return false;
        } else {
            try {
                if (mStartPath != null && mStartPath.equals(mCurrentPath)) {
                    ((MainActivity) mMainContext).initFragment();
                } else {
                    if (mCurrentPath.equals(Utils.getInternalDirectoryPath())) {
                        ((MainActivity) mMainContext).initFragment();
                    } else {
                        File file = new File(mCurrentPath);
                        folderListDisplay(file.getParent());
                    }
                }
            } catch (Exception e) {
                ((MainActivity) mMainContext).initFragment();
                return false;
            }

            return true;
        }
    }

    public void copyMethod(boolean oriRemove) {
        int count = mSecretFolderAdapter.getCheckCount();

        if (count > 0) {
            ArrayList<SecretFileItem> secretItems = mSecretFolderAdapter.getCheckList();
            ArrayList<FileItem> items = new ArrayList<>();
            for (SecretFileItem temp : secretItems) {
                FileItem item = new FileItem();
                item.setName(temp.getName());
                item.setPath(temp.getCurrentPath());
                item.setId("");
                item.setcheck(false);
                item.setSecret(true);

                items.add(item);
            }
            if (oriRemove) {
                ClipBoard.cutMove(items);
                Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_move), Toast.LENGTH_SHORT).show();
            } else {
                ClipBoard.cutCopy(items);
                Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_copy), Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        finishActionMode();
    }

    public void selectCopyFolder(boolean oriRemove) {
        int check_count = mSecretFolderAdapter.getCheckCount();

        if (check_count > 0) {
            FolderSelectTotalDialog.ORI_FILE_TYPE = FolderSelectTotalDialog.LOCAL_MEMORY;
            FolderSelectTotalDialog selectDialog = new FolderSelectTotalDialog(getActivity(), R.style.Theme_TransparentBackground, getActivity(), !oriRemove, false, new AlertCallback() {
                @Override
                public void ok(Object teamObj, Object obj) {
                    String path = (String) obj;
                    returnFile(check_count, path, oriRemove);
                }

                @Override
                public void cancle(Object obj) {
                }
            });
            selectDialog.show();
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
            finishActionMode();
        }
    }

    public void deleteMethod() {
        int check_count = mSecretFolderAdapter.getCheckCount();

        if (check_count > 0) {
            dialogDeleteFile(check_count);
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
            finishActionMode();
        }
    }

    public void addAccountMethod() {
        if (ActivityCompat.checkSelfPermission(mMainContext, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            //return false;
//                    View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
//                    Snackbar.make(viewPos, getResources().getString(R.string.secretfolder_not_permission), Snackbar.LENGTH_SHORT)
//                            .setAction("Action", null).show();
            Toast.makeText(mMainContext, getResources().getString(R.string.secretfolder_not_permission), Toast.LENGTH_SHORT).show();
        } else {
            Account[] accounts = AccountManager.get(mMainContext).getAccountsByType("com.google");
            if (accounts != null && accounts.length > 0) {
                dialogSelectAccount(true, accounts);
            } else {
                Toast.makeText(mMainContext, getResources().getString(R.string.secretfolder_do_not_have_a_google_account), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void dialogSetSort() {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.dialog_select_sort, null, false);
        LinearLayout sort_by_name = (LinearLayout) view.findViewById(R.id.sort_by_name);
        TextView sort_by_name_txt = (TextView) view.findViewById(R.id.sort_by_name_txt);
        if (MyFileSettings.getSortType() != SortUtils.SORT_ALPHA) {
            sort_by_name_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        } else {
            if (!MyFileSettings.getSortReverse()) {
                sort_by_name_txt.setText(sort_by_name_txt.getText().toString() + " ↑");
            } else {
                sort_by_name_txt.setText(sort_by_name_txt.getText().toString() + " ↓");
            }
        }
        sort_by_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                if (MyFileSettings.getSortType() == SortUtils.SORT_ALPHA) {
                    MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
                } else {
                    MyFileSettings.setSortType(SortUtils.SORT_ALPHA);
                }

                mSecretFolderAdapter.reSort();
            }
        });
        LinearLayout sort_by_extension = (LinearLayout) view.findViewById(R.id.sort_by_extension);
        TextView sort_by_extension_txt = (TextView) view.findViewById(R.id.sort_by_extension_txt);
        if (MyFileSettings.getSortType() != SortUtils.SORT_TYPE) {
            sort_by_extension_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        } else {
            if (!MyFileSettings.getSortReverse()) {
                sort_by_extension_txt.setText(sort_by_extension_txt.getText().toString() + " ↑");
            } else {
                sort_by_extension_txt.setText(sort_by_extension_txt.getText().toString() + " ↓");
            }
        }
        sort_by_extension.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                if (MyFileSettings.getSortType() == SortUtils.SORT_TYPE) {
                    MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
                } else {
                    MyFileSettings.setSortType(SortUtils.SORT_TYPE);
                }
                mSecretFolderAdapter.reSort();
            }
        });
        LinearLayout sort_by_date = (LinearLayout) view.findViewById(R.id.sort_by_date);
        TextView sort_by_date_txt = (TextView) view.findViewById(R.id.sort_by_date_txt);
        if (MyFileSettings.getSortType() != SortUtils.SORT_DATE) {
            sort_by_date_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        } else {
            if (!MyFileSettings.getSortReverse()) {
                sort_by_date_txt.setText(sort_by_date_txt.getText().toString() + " ↑");
            } else {
                sort_by_date_txt.setText(sort_by_date_txt.getText().toString() + " ↓");
            }
        }
        sort_by_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                if (MyFileSettings.getSortType() == SortUtils.SORT_DATE) {
                    MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
                } else {
                    MyFileSettings.setSortType(SortUtils.SORT_DATE);
                }
                mSecretFolderAdapter.reSort();
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void setSortName() {
        if (MyFileSettings.getSortType() == SortUtils.SORT_ALPHA) {
            MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
        } else {
            MyFileSettings.setSortType(SortUtils.SORT_ALPHA);
        }

        mSecretFolderAdapter.reSort();
    }

    public void setSortExtension() {
        if (MyFileSettings.getSortType() == SortUtils.SORT_TYPE) {
            MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
        } else {
            MyFileSettings.setSortType(SortUtils.SORT_TYPE);
        }
        mSecretFolderAdapter.reSort();
    }

    public void setSortDate() {
        if (MyFileSettings.getSortType() == SortUtils.SORT_DATE) {
            MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
        } else {
            MyFileSettings.setSortType(SortUtils.SORT_DATE);
        }
        mSecretFolderAdapter.reSort();
    }

    public void returnFile(final int check_count, String path, boolean oriRemove) {
        ArrayList<SecretFileItem> items = mSecretFolderAdapter.getCheckList();

        boolean success = true;

        for (SecretFileItem item : items) {
            File from = new File(item.getCurrentPath());
            File to;// = new File(item.getOriPath());
            if (path != null && path.length() > 0) {
                to = new File(path + File.separator + item.getName());
            } else {
                to = new File(item.getOriPath());
            }

            if (!to.getParentFile().exists()) {
                to.getParentFile().mkdirs();
            }

            if (!Utils.returnToPathDirectory(item.getId(), from, to, oriRemove, mMainContext)) {
                success = false;
                break;
            }
        }
        if (oriRemove) {
            if (success) {
                Toast.makeText(mMainContext, getResources().getString(R.string.secretfolder_move_to_path_location), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mMainContext, getResources().getString(R.string.secretfolder_move_to_path_location_fail), Toast.LENGTH_SHORT).show();
            }

        } else {
            if (success) {
                Toast.makeText(mMainContext, getResources().getString(R.string.secretfolder_copy_to_path_location), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mMainContext, getResources().getString(R.string.secretfolder_copy_to_path_location_fail), Toast.LENGTH_SHORT).show();
            }
        }
        mSecretFolderAdapter.refresh();
        finishActionMode();
    }

    public void dialogDeleteFile(final int check_count) {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        //View view = inflater.inflate(R.layout.dialog_secret_base, null, false);
        View view = inflater.inflate(R.layout.dialog_base, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.left_menu_secretfolder));
        TextView content = (TextView) view.findViewById(R.id.content);
        content.setText(getResources().getString(R.string.secretfolder_do_you_want_delete_files));
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<SecretFileItem> items = mSecretFolderAdapter.getCheckList();

                boolean success = true;

                for (SecretFileItem item : items) {
                    File select = new File(item.getCurrentPath());
                    if (select != null && select.exists()) {
                        if (select.isDirectory()) {
                            success = deleteFile(select);
                            if (!success) {
                                break;
                            }
                            select.delete();
                        } else {
                            if (select.delete()) {
                                MainActivity.mMds.deleteSecretFile(item.getId());
                            } else {
                                success = false;
                                break;
                            }
                        }
                    }
                }
                if (success) {
                    Toast.makeText(mMainContext, getResources().getString(R.string.secretfolder_delete_successfully), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_delete_error), Toast.LENGTH_SHORT).show();
                }
                mSecretFolderAdapter.refresh();
                finishActionMode();

                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public boolean deleteFile(File file) {
        File[] fileList = file.listFiles();
        boolean success = true;
        for (File temp : fileList) {
            if (temp.isDirectory()) {
                success = deleteFile(temp);
                if (!success) {
                    break;
                }
                temp.delete();
            } else {
                if (temp.delete()) {
                    ArrayList<FileItem> tempList = MainActivity.mMds.selectTagFiles(temp.getPath());
                    for (FileItem sub : tempList) {
                        MainActivity.mMds.deleteTag(sub.getId());
                    }
                } else {
                    success = false;
                    break;
                }
            }
        }
        return success;
    }

    public void listRefresh() {
        mSecretFolderAdapter.refresh();
    }

    public void initSetting() {
        checkPassword(true);
    }

    public void initPasswordSettingfinish() {
        mHide_layout.setVisibility(View.GONE);
    }

    public void initAccountSettingfinish() {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.dialog_secret_account_finish, null, false);
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                //checkPassword(false);
            }
        });
        mDialog.setContentView(view);
        mDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                //checkPassword(false);
            }
        });
        mDialog.show();
    }

    public void checkPassword(final boolean setting) {
        mHide_layout.setVisibility(View.VISIBLE);
        mConfirmPassword = "";
        mPassword = "";
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.dialog_secret_password, null, false);

        title_info = (TextView) view.findViewById(R.id.title_info);
        comment = (TextView) view.findViewById(R.id.comment);
        forgot_password_btn = (TextView) view.findViewById(R.id.forgot_password_btn);

        cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
                ((MainActivity) mMainContext).initFragment();
            }
        });
        ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (setting) {
                    if (mConfirmPassword.length() > 0) {
                        if (mConfirmPassword.equals(mPassword)) {
                            //저장 로직
                            //PreferenceHelper.putString(PreferenceHelper.password, mPassword);
                            MainActivity.mMds.updateSecretUser(MainActivity.mMds.getSecretUserAccount(), mPassword);
                            if (mDialog != null) {
                                mDialog.dismiss();
                            }
                            initPasswordSettingfinish();
                            //이후 로직
                        } else {
                            //comment.setText(getResources().getString(R.string.secretfolder_password_set));
                            comment.setText(getResources().getString(R.string.secretfolder_password_not_match));
                            //comment.setVisibility(View.VISIBLE);
                            mConfirmPassword = "";
                            mPassword = "";
                            password_1.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                            password_2.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                            password_3.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                            password_4.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                        }
                    } else {
                        comment.setText(getResources().getString(R.string.secretfolder_password_confirm));
                        mConfirmPassword = mPassword;
                        mPassword = "";
                        password_1.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                        password_2.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                        password_3.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                        password_4.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                    }

                } else {
                    //비번 확인
                    //String password = PreferenceHelper.getString(PreferenceHelper.password, "");
                    String password = MainActivity.mMds.getSecretUserPassword();
                    if (password.equals(mPassword)) {
                        //이후동작
                        if (mDialog != null) {
                            mDialog.dismiss();
                        }
                        mHide_layout.setVisibility(View.GONE);
                    } else {
                        comment.setText(getResources().getString(R.string.secretfolder_password_not_match));
                        //comment.setVisibility(View.VISIBLE);
//                        underbar_skyblue.setVisibility(View.GONE);
//                        underbar_red.setVisibility(View.VISIBLE);
                        mPassword = "";
                        password_1.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                        password_2.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                        password_3.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                        password_4.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                    }
                }
            }
        });

        if (setting) {
            title_info.setVisibility(View.VISIBLE);
            comment.setText(getResources().getString(R.string.secretfolder_password_set));
        } else {
            String account = MainActivity.mMds.getSecretUserAccount();
            if (account != null && account.length() > 0) {
                forgot_password_btn.setVisibility(View.VISIBLE);
            }
        }
//        forgot_password_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(mMainContext, GoogleLoginActivity.class);
//                ((MainActivity) mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_GOOGLE_LOGIN);
//            }
//        });

        password_1 = (TextView) view.findViewById(R.id.password_1);
        password_2 = (TextView) view.findViewById(R.id.password_2);
        password_3 = (TextView) view.findViewById(R.id.password_3);
        password_4 = (TextView) view.findViewById(R.id.password_4);

        TextView btn_1 = (TextView) view.findViewById(R.id.btn_1);
        btn_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPasswordUIUpdate(setting, "1");
            }
        });
        TextView btn_2 = (TextView) view.findViewById(R.id.btn_2);
        btn_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPasswordUIUpdate(setting, "2");
            }
        });
        TextView btn_3 = (TextView) view.findViewById(R.id.btn_3);
        btn_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPasswordUIUpdate(setting, "3");
            }
        });
        TextView btn_4 = (TextView) view.findViewById(R.id.btn_4);
        btn_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPasswordUIUpdate(setting, "4");
            }
        });
        TextView btn_5 = (TextView) view.findViewById(R.id.btn_5);
        btn_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPasswordUIUpdate(setting, "5");
            }
        });
        TextView btn_6 = (TextView) view.findViewById(R.id.btn_6);
        btn_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPasswordUIUpdate(setting, "6");
            }
        });
        TextView btn_7 = (TextView) view.findViewById(R.id.btn_7);
        btn_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPasswordUIUpdate(setting, "7");
            }
        });
        TextView btn_8 = (TextView) view.findViewById(R.id.btn_8);
        btn_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPasswordUIUpdate(setting, "8");
            }
        });
        TextView btn_9 = (TextView) view.findViewById(R.id.btn_9);
        btn_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPasswordUIUpdate(setting, "9");
            }
        });
        TextView btn_0 = (TextView) view.findViewById(R.id.btn_0);
        btn_0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPasswordUIUpdate(setting, "0");
            }
        });
        LinearLayout btn_delete = (LinearLayout) view.findViewById(R.id.btn_delete);
        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPassword.length() > 0) {
                    mPassword = mPassword.substring(0, mPassword.length() - 1);
                    if (mPassword.length() == 0) {
                        password_1.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                    }
                    if (mPassword.length() == 1) {
                        password_2.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                    }
                    if (mPassword.length() == 2) {
                        password_3.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                    }
                    if (mPassword.length() == 3) {
                        password_4.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                    }
                }
            }
        });

        mDialog.setContentView(view);
        mDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                ((MainActivity) mMainContext).initFragment();
            }
        });
        mDialog.show();
    }

    public void checkPasswordUIUpdate(final boolean setting, String str) {
        if (mPassword.length() < 4) {
            mPassword += str;
            if (mPassword.length() == 1) {
                password_1.setBackgroundResource(R.drawable.stroke_secret_circle_check);
            }
            if (mPassword.length() == 2) {
                password_2.setBackgroundResource(R.drawable.stroke_secret_circle_check);
            }
            if (mPassword.length() == 3) {
                password_3.setBackgroundResource(R.drawable.stroke_secret_circle_check);
            }
            if (mPassword.length() == 4) {
                password_4.setBackgroundResource(R.drawable.stroke_secret_circle_check);
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (setting) {
                            if (mConfirmPassword.length() > 0) {
                                if (mConfirmPassword.equals(mPassword)) {
                                    //저장 로직
                                    //PreferenceHelper.putString(PreferenceHelper.password, mPassword);
                                    MainActivity.mMds.updateSecretUser(MainActivity.mMds.getSecretUserAccount(), mPassword);
                                    if (mDialog != null) {
                                        mDialog.dismiss();
                                    }
                                    initPasswordSettingfinish();
                                    //이후 로직
                                } else {
                                    //comment.setText(getResources().getString(R.string.secretfolder_password_set));
                                    comment.setText(getResources().getString(R.string.secretfolder_password_not_match));
                                    //comment.setVisibility(View.VISIBLE);
                                    mConfirmPassword = "";
                                    mPassword = "";
                                    password_1.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                                    password_2.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                                    password_3.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                                    password_4.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                                }
                            } else {
                                comment.setText(getResources().getString(R.string.secretfolder_password_confirm));
                                mConfirmPassword = mPassword;
                                mPassword = "";
                                password_1.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                                password_2.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                                password_3.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                                password_4.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                            }

                        } else {
                            //비번 확인
                            //String password = PreferenceHelper.getString(PreferenceHelper.password, "");
                            String password = MainActivity.mMds.getSecretUserPassword();
                            if (password.equals(mPassword)) {
                                //이후동작
                                if (mDialog != null) {
                                    mDialog.dismiss();
                                }
                                mHide_layout.setVisibility(View.GONE);
                            } else {
                                comment.setText(getResources().getString(R.string.secretfolder_password_not_match));
                                //comment.setVisibility(View.VISIBLE);
//                        underbar_skyblue.setVisibility(View.GONE);
//                        underbar_red.setVisibility(View.VISIBLE);
                                mPassword = "";
                                password_1.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                                password_2.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                                password_3.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                                password_4.setBackgroundResource(R.drawable.stroke_secret_circle_none);
                            }
                        }
                    }
                }, 500);
            }
        }
    }


    public void dialogSelectAccount(final boolean backNoAction, Account[] accounts) {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        final View view = inflater.inflate(R.layout.dialog_secret_account_select, null, false);
        final RadioGroup dialog_account_list_layout = (RadioGroup) view.findViewById(R.id.dialog_account_list_layout);

        // device 계정 정보 가져오기
        //Account[] accounts =  AccountManager.get(mMainContext).getAccounts();
        for (final Account account : accounts) {
            RadioButton radioButtonAndroid = (RadioButton) inflater.inflate(R.layout.custom_radiobutton, null, false);
            radioButtonAndroid.setText(account.name);
            dialog_account_list_layout.addView(radioButtonAndroid);
        }

        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                if (backNoAction) {
                    //checkPassword(false);
                } else {
                    initPasswordSettingfinish();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedRadioButtonID = dialog_account_list_layout.getCheckedRadioButtonId();
                // If nothing is selected from Radio Group, then it return -1
                if (selectedRadioButtonID != -1) {

                    RadioButton selectedRadioButton = (RadioButton) view.findViewById(selectedRadioButtonID);
                    String selectedRadioButtonText = selectedRadioButton.getText().toString();
                    MainActivity.mMds.updateSecretUser(selectedRadioButtonText, MainActivity.mMds.getSecretUserPassword());
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                    initAccountSettingfinish();
                } else {
                    Toast.makeText(mMainContext, getResources().getString(R.string.secretfolder_please_select_account), Toast.LENGTH_SHORT).show();
                }
            }
        });
        mDialog.setContentView(view);
        mDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                if (backNoAction) {
                    //checkPassword(false);
                } else {
                    initPasswordSettingfinish();
                }
            }
        });
        mDialog.show();
    }

    public void resetPassword() {
        MainActivity.mMds.updateSecretUser("", "");
        if (mDialog != null) {
            mDialog.dismiss();
        }
        initSetting();
    }
}
