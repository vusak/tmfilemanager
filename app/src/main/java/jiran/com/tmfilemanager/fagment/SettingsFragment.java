package jiran.com.tmfilemanager.fagment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.InitActivity;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.service.FileObserverService;
import jiran.com.tmfilemanager.settings.MyFileSettings;

/**
 * Created by user on 2016-08-03.
 */


public class SettingsFragment extends Fragment {
    public Context mMainContext;

    LinearLayout new_file_layout;
    LinearLayout memory_space_layout;
    LinearLayout hide_layout;
    LinearLayout language_layout;

    CheckBox new_file_checkbox;
    CheckBox memory_space_checkbox;
    CheckBox hide_checkbox;

    TextView mVersion;

    private Dialog mDialog;

    public SettingsFragment() {
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public SettingsFragment(Context context) {
        mMainContext = context;
        setRetainInstance(true);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mMainContext == null) {
            Intent intent = new Intent(getActivity(), InitActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        }
        ((MainActivity) mMainContext).toolbar.setBackgroundResource(R.color.colorTitleBG);
        View rootView = inflater.inflate(R.layout.frag_settings, container, false);
        initLayout(inflater, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        ((MainActivity) mMainContext).finishSearchMode();
        ((MainActivity) mMainContext).toolbarMenu(R.menu.none);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        ((MainActivity) getActivity()).setTitle(getResources().getString(R.string.left_menu_setting));
        //Toast.makeText(getActivity(), mPath, Toast.LENGTH_SHORT).show();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onBackPressed() {
        //getActivity().finish();
        ((MainActivity) mMainContext).initFragment();
        return false;
    }

    public void initLayout(LayoutInflater inflater, View rootView) {
        //final MainActivity context = (MainActivity) getActivity();
        //Setting
        mVersion = (TextView) rootView.findViewById(R.id.version);
        PackageInfo pi = null;
        try {
            pi = mMainContext.getPackageManager().getPackageInfo(mMainContext.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String verSion = pi.versionName;
        mVersion.setText(verSion);

        new_file_layout = (LinearLayout) rootView.findViewById(R.id.new_file_layout);
        new_file_checkbox = (CheckBox) rootView.findViewById(R.id.new_file_checkbox);
        if (MyFileSettings.getNewFilesAlram()) {
            new_file_layout.setTag(true);
            new_file_checkbox.setChecked(true);
        } else {
            new_file_layout.setTag(false);
            new_file_checkbox.setChecked(false);
        }
        new_file_layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((boolean) v.getTag()) {
                    v.setTag(false);
                    new_file_checkbox.setChecked(false);
                    MyFileSettings.setNewFilesAlram(false);
                    mMainContext.stopService(new Intent(mMainContext, FileObserverService.class));
                } else {
                    v.setTag(true);
                    new_file_checkbox.setChecked(true);
                    MyFileSettings.setNewFilesAlram(true);
                    mMainContext.startService(new Intent(mMainContext, FileObserverService.class));

                }
            }
        });
        memory_space_layout = (LinearLayout) rootView.findViewById(R.id.memory_space_layout);
        memory_space_checkbox = (CheckBox) rootView.findViewById(R.id.memory_space_checkbox);
        if (MyFileSettings.getMemoryAlram()) {
            memory_space_layout.setTag(true);
            memory_space_checkbox.setChecked(true);
        } else {
            memory_space_layout.setTag(false);
            memory_space_checkbox.setChecked(false);
        }
        memory_space_layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((boolean) v.getTag()) {
                    v.setTag(false);
                    memory_space_checkbox.setChecked(false);
                    MyFileSettings.setMemoryAlram(false);
                } else {
                    v.setTag(true);
                    memory_space_checkbox.setChecked(true);
                    MyFileSettings.setMemoryAlram(true);
                }
            }
        });
        hide_layout = (LinearLayout) rootView.findViewById(R.id.hide_layout);
        hide_checkbox = (CheckBox) rootView.findViewById(R.id.hide_checkbox);
        if (MyFileSettings.getShowHiddenFiles()) {
            hide_layout.setTag(true);
            hide_checkbox.setChecked(true);
        } else {
            hide_layout.setTag(false);
            hide_checkbox.setChecked(false);
        }
        hide_layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((boolean) v.getTag()) {
                    v.setTag(false);
                    hide_checkbox.setChecked(false);
                    MyFileSettings.setShowHiddenFiles(false);
                } else {
                    v.setTag(true);
                    hide_checkbox.setChecked(true);
                    MyFileSettings.setShowHiddenFiles(true);
                }
            }
        });
        language_layout = (LinearLayout) rootView.findViewById(R.id.language_layout);
        language_layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSetLanguage();
            }
        });
    }

    public void dialogSetLanguage() {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.dialog_select_language, null, false);
        LinearLayout korean = (LinearLayout) view.findViewById(R.id.korean);
        TextView korean_txt = (TextView) view.findViewById(R.id.korean_txt);
        if (MyFileSettings.getLanguage() != MyFileSettings.LANGUAGE_KOREA) {
            korean_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        }
        korean.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                MyFileSettings.setLanguage(MyFileSettings.LANGUAGE_KOREA);
                applicationRestart();
            }
        });
        LinearLayout english = (LinearLayout) view.findViewById(R.id.english);
        TextView english_txt = (TextView) view.findViewById(R.id.english_txt);
        if (MyFileSettings.getLanguage() != MyFileSettings.LANGUAGE_ENGLISH) {
            english_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        }
        english.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                MyFileSettings.setLanguage(MyFileSettings.LANGUAGE_ENGLISH);
                applicationRestart();
            }
        });
        LinearLayout japan = (LinearLayout) view.findViewById(R.id.japan);
        TextView japan_txt = (TextView) view.findViewById(R.id.japan_txt);
        if (MyFileSettings.getLanguage() != MyFileSettings.LANGUAGE_JAPAN) {
            japan_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        }
        japan.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                MyFileSettings.setLanguage(MyFileSettings.LANGUAGE_JAPAN);
                applicationRestart();
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }


    public void applicationRestart() {
        Intent intent = getActivity().getBaseContext().getPackageManager().getLaunchIntentForPackage(getActivity().getPackageName());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        getActivity().startActivity(intent);
        getActivity().finish();
    }
}
