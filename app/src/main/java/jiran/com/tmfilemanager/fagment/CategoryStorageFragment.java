package jiran.com.tmfilemanager.fagment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import jiran.com.circleprogress.DonutProgress;
import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.InitActivity;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.activity.viewer.ImageSlideViewerActivity;
import jiran.com.tmfilemanager.adapter.CategoryAdapter;
import jiran.com.tmfilemanager.adapter.CategoryStorageAdapter;
import jiran.com.tmfilemanager.common.ClipBoard;
import jiran.com.tmfilemanager.common.FolderSelectTotalDialog;
import jiran.com.tmfilemanager.common.ListFunctionUtils;
import jiran.com.tmfilemanager.common.MimeTypes;
import jiran.com.tmfilemanager.common.SortUtils;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.CurrentItem;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.data.StorageListItem;
import jiran.com.tmfilemanager.data.TagItem;
import jiran.com.tmfilemanager.data.TeamItem;
import jiran.com.tmfilemanager.layout.FlowLayout;
import jiran.com.tmfilemanager.network.JMF_network;
import jiran.com.tmfilemanager.network.updown.DownloadProgressListener;
import jiran.com.tmfilemanager.service.FileObserverService;
import jiran.com.tmfilemanager.settings.MyFileSettings;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by user on 2016-08-03.
 */


public class CategoryStorageFragment extends Fragment {

    public static final int CATEGORY_TOTAL = 0, CATEGORY_APP = -1, CATEGORY_IMAGE = -2, CATEGORY_MOVIE = -3, CATEGORY_MUSIC = -4, CATEGORY_DOCUMENT = -5, CATEGORY_ZIP = -6;
    public static String LIST_LIMIT = "100";
    public static boolean isLastpage = false;
    public static boolean mActionMode = false;
    public Context mMainContext;
    public int CATEGORY_TYPE = CATEGORY_TOTAL;
    public TeamItem mTeam;
    public StorageListItem mCurrentArray;

    public String mCurrentPath;
    public TagItem mTagItem;
    public String mSearch;
    public ArrayList<FileItem> mSearchArray;
    boolean isConnecting = false;
    Dialog mDialog;
    HashMap<String, Integer> listPositionMap;
    LinearLayout top_tag_layout, top_category_layout;
    Dialog downloadProgressDialog;
    DonutProgress download_percent_dp;
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            setDownlaodProgress(msg.what);
        }
    };
    DownloadProgressListener listener = new DownloadProgressListener() {
        @SuppressLint("LongLogTag")
        @Override
        public void update(int percent, int done) {
            //start a new thread to process job
            new Thread(new Runnable() {
                @Override
                public void run() {
                    //heavy job here
                    //send message to main thread
                    handler.sendEmptyMessage(percent);
                }
            }).start();
            //setDownlaodProgress(percent);
            Log.i("DownloadProgressListener", "percent => " + percent + "%");
        }
    };
    private AbsListView mListView;
    private GridView mGridView;

    //add Category Menu
    private RecyclerView mRecyclerView;
    //private LinearLayout mFoler_path_layout;
    //private TextView mFoler_path;
    private HorizontalScrollView category_scroll_layout;
    private ImageView category_app_layout, category_image_layout, category_video_layout, category_music_layout, category_document_layout, category_zip_layout;
    private int mView_Type = 0;
    private CategoryStorageAdapter mCategoryStorageAdapter;
    private FlowLayout tag_content_layout;


    public CategoryStorageFragment() {
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public CategoryStorageFragment(Context context, String path, TeamItem team, StorageListItem list) {
        mMainContext = context;
        mCurrentPath = path;
        mTeam = team;
        mCurrentArray = list;
        mView_Type = MainActivity.mViewType;
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public CategoryStorageFragment(Context context, String path, StorageListItem list, int viewType) {
        mMainContext = context;
        mCurrentPath = path;
        mCurrentArray = list;
        mView_Type = viewType;
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public CategoryStorageFragment(Context context, String currentPath, String search) {
        mMainContext = context;
        //mStartPath = currentPath;
        mCurrentPath = currentPath;
        mSearch = search;
        mView_Type = MainActivity.mViewType;
        setRetainInstance(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        CategoryAdapter.sdcardUse = Utils.isSDcardDirectory(true);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mMainContext == null || ((MainActivity) mMainContext).toolbar == null) {
            Intent intent = new Intent(getActivity(), InitActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        }
        ((MainActivity) mMainContext).toolbar.setBackgroundResource(R.color.colorTitleBG);
        View rootView = inflater.inflate(R.layout.frag_category, container, false);
        initLayout(inflater, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        ((MainActivity) mMainContext).finishSearchMode();
        //((MainActivity)mMainContext).toolbarMenu(R.menu.main);

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        //getActivity().setTitle(getResources().getString(R.string.app_name));
        ((MainActivity) getActivity()).setTitle(mCurrentPath);
        if (mCurrentPath.equals(getResources().getString(R.string.category_app))) {
            CATEGORY_TYPE = CATEGORY_APP;
        } else if (mCurrentPath.equals(getResources().getString(R.string.category_image))) {
            CATEGORY_TYPE = CATEGORY_IMAGE;
        } else if (mCurrentPath.equals(getResources().getString(R.string.category_video))) {
            CATEGORY_TYPE = CATEGORY_MOVIE;
        } else if (mCurrentPath.equals(getResources().getString(R.string.category_music))) {
            CATEGORY_TYPE = CATEGORY_MUSIC;
        } else if (mCurrentPath.equals(getResources().getString(R.string.category_document))) {
            CATEGORY_TYPE = CATEGORY_DOCUMENT;
        } else if (mCurrentPath.equals(getResources().getString(R.string.category_zip))) {
            CATEGORY_TYPE = CATEGORY_ZIP;
        }
//        File file = new File(mCurrentPath);
//        if(mListType != 2 && file != null && file.isDirectory()){
//            if(ClipBoard.isEmpty()){
//                list_bottom_menu_paste.setVisibility(View.GONE);
//            }else{
//                list_bottom_menu_paste.setVisibility(View.VISIBLE);
//            }
//        }else{
//            list_bottom_menu_paste.setVisibility(View.GONE);
//        }
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void initLayout(LayoutInflater inflater, View rootView) {
        //final MainActivity context = (MainActivity) getActivity();
        top_tag_layout = (LinearLayout) rootView.findViewById(R.id.top_tag_layout);
        top_category_layout = (LinearLayout) rootView.findViewById(R.id.top_category_layout);
        tag_content_layout = (FlowLayout) rootView.findViewById(R.id.tag_content_layout);

        top_tag_layout.setVisibility(View.GONE);
        top_category_layout.setVisibility(View.VISIBLE);

        listPositionMap = new HashMap<String, Integer>();

        category_scroll_layout = (HorizontalScrollView) rootView.findViewById(R.id.category_scroll_layout);
        //mFoler_path_layout =  (LinearLayout) rootView.findViewById(R.id.foler_path_layout);
        category_app_layout = (ImageView) rootView.findViewById(R.id.category_app_layout);
        category_app_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CATEGORY_TYPE = CATEGORY_APP;
                mCurrentPath = getResources().getString(R.string.category_app);
                setCategoryIcon();
                getStorageList(false, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), "");
            }
        });
        category_image_layout = (ImageView) rootView.findViewById(R.id.category_image_layout);
        category_image_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CATEGORY_TYPE = CATEGORY_IMAGE;
                mCurrentPath = getResources().getString(R.string.category_image);
                setCategoryIcon();
                getStorageList(false, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), "");
            }
        });
        category_video_layout = (ImageView) rootView.findViewById(R.id.category_video_layout);
        category_video_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CATEGORY_TYPE = CATEGORY_MOVIE;
                mCurrentPath = getResources().getString(R.string.category_video);
                setCategoryIcon();
                getStorageList(false, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), "");
            }
        });
        category_music_layout = (ImageView) rootView.findViewById(R.id.category_music_layout);
        category_music_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CATEGORY_TYPE = CATEGORY_MUSIC;
                mCurrentPath = getResources().getString(R.string.category_music);
                setCategoryIcon();
                getStorageList(false, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), "");
            }
        });
        category_document_layout = (ImageView) rootView.findViewById(R.id.category_document_layout);
        category_document_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CATEGORY_TYPE = CATEGORY_DOCUMENT;
                mCurrentPath = getResources().getString(R.string.category_document);
                setCategoryIcon();
                getStorageList(false, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), "");
            }
        });
        category_zip_layout = (ImageView) rootView.findViewById(R.id.category_zip_layout);
        category_zip_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CATEGORY_TYPE = CATEGORY_ZIP;
                mCurrentPath = getResources().getString(R.string.category_zip);
                setCategoryIcon();
                getStorageList(false, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), "");
            }
        });

        Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner);

        //String[]paths = {getResources().getString(R.string.category_loacl_storage), getResources().getString(R.string.category_team_storage)};
        int i = 0;
        int select = 1;
        String[] paths = new String[MultiFragment.teamItams.size() + 1];
        paths[i++] = getResources().getString(R.string.category_loacl_storage);
        for (TeamItem team : MultiFragment.teamItams) {
            if (mTeam.getId().equals(team.getId())) select = i;
            paths[i++] = team.getTeamName();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mMainContext,
                R.layout.frag_folder_path_item_spinner_tv, paths) {
            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
//                tv.setBackgroundResource(R.color.main_my_storage_bg);
//                // Set the Text color
//                tv.setTextColor(Color.parseColor("#FF414141"));
//                tv.setTextSize(20);

                return view;
                //return super.getDropDownView(position, convertView, parent);
            }
        };
        adapter.setDropDownViewResource(R.layout.frag_folder_path_item_spinner_tv);
        spinner.setAdapter(adapter);
        SpinnerInteractionListener listener = new SpinnerInteractionListener();
        spinner.setOnTouchListener(listener);
        spinner.setOnItemSelectedListener(listener);

        spinner.setSelection(select);

        mListView = (ListView) rootView.findViewById(R.id.folder_listview);
        mListView.setEmptyView(rootView.findViewById(R.id.empty_layout));
        mGridView = (GridView) rootView.findViewById(R.id.folder_gridview);
        mGridView.setEmptyView(rootView.findViewById(R.id.empty_layout));

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.folder_recyclerview);

        if (mView_Type == 0) {
            changeListView(false);
        } else {
            changeGridView(false);
        }

        //bottom menu
        finishActionMode();
        setCategoryIcon();
    }

    public void setCategoryIcon() {
        category_image_layout.setImageResource(mCurrentPath.equals(getString(R.string.category_image)) ? R.drawable.ic_history_circle_peacock_32 : R.drawable.ic_history_off_circle_peacock_32);
        category_video_layout.setImageResource(mCurrentPath.equals(getString(R.string.category_video)) ? R.drawable.ic_video_circle_navy_32 : R.drawable.ic_video_off_circle_navy_32);
        category_music_layout.setImageResource(mCurrentPath.equals(getString(R.string.category_music)) ? R.drawable.ic_music_circle_pink_32 : R.drawable.ic_music_off_circle_pink_32);
        category_document_layout.setImageResource(mCurrentPath.equals(getString(R.string.category_document)) ? R.drawable.ic_document_circle_blue_32 : R.drawable.ic_document_off_circle_blue_32);
        category_app_layout.setImageResource(mCurrentPath.equals(getString(R.string.category_app)) ? R.drawable.ic_app_circle_green_32 : R.drawable.ic_app_off_circle_green_32);
        category_zip_layout.setImageResource(mCurrentPath.equals(getString(R.string.category_zip)) ? R.drawable.ic_zip_circle_red_32 : R.drawable.ic_zip_off_circle_red_32);
    }

    public void changeListGridView() {
        mCategoryStorageAdapter.listChange = true;
        if (mView_Type == 0) {
            MainActivity.mViewType = 1;
            listPositionMap.put(mCurrentPath, mListView.getFirstVisiblePosition());
            changeGridView(true);
        } else {
            MainActivity.mViewType = 0;
            listPositionMap.put(mCurrentPath, mGridView.getFirstVisiblePosition());
            changeListView(true);
        }
    }

    public void changeGridView() {
        if (mView_Type == 0) {
            mCategoryStorageAdapter.listChange = true;
            MainActivity.mViewType = 1;
            listPositionMap.put(mCurrentPath, mListView.getFirstVisiblePosition());
            changeGridView(true);
        }
    }

    public void changeListView() {
        if (mView_Type == 1) {
            mCategoryStorageAdapter.listChange = true;
            MainActivity.mViewType = 0;
            listPositionMap.put(mCurrentPath, mGridView.getFirstVisiblePosition());
            changeListView(true);
        }
    }

    private void changeGridView(boolean listChange) {
        //final MainActivity context = (MainActivity) getActivity();
        ((MainActivity) getActivity()).setSelectedTitle("");
        mView_Type = 1;
        changeActionModeList();
        if (listChange) {
            mCategoryStorageAdapter = new CategoryStorageAdapter(getActivity(), null, mView_Type, mCategoryStorageAdapter.getList());
        } else {
            mCategoryStorageAdapter = new CategoryStorageAdapter(getActivity(), null, mView_Type);
        }
        mGridView.setVisibility(View.VISIBLE);
        mListView.setVisibility(View.GONE);
        mGridView.setOnItemClickListener((parent, view, position, id) -> {
            FileItem selectFileItem = ((FileItem) mGridView.getAdapter().getItem(position));
            if (mActionMode) {
                mCategoryStorageAdapter.setCheck(position);
                mCategoryStorageAdapter.notifyDataSetChanged();
                toolbarMenuIconChange();
                int count = mCategoryStorageAdapter.getCheckCount();
                ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
            } else {
                if (selectFileItem.isDirectory()) {
                    mSearch = null;
                    //Scroll Position Set
                    listPositionMap.put(selectFileItem.getId(), mListView.getFirstVisiblePosition());
                    CATEGORY_TYPE = CATEGORY_TOTAL;
                    getStorageList(false, mTeam.getId(), selectFileItem.getId(), "");
                    // go to the top of the ListView
                    //mListView.setSelection(0);
                } else {
//                        //파일일 경우
//                        downloadfile(mTeam.getId(), selectFileItem.getId(), selectFileItem.getName());
                    if (!checkFile(selectFileItem, false)) {
                        downloadfile(mTeam.getId(), selectFileItem, false);
                    }
                }
            }
        });

        mGridView.setOnItemLongClickListener((parent, view, position, id) -> {
            mCategoryStorageAdapter.setCheck(position);
            int count = mCategoryStorageAdapter.getCheckCount();
            ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
            setActionMode();
            toolbarMenuIconChange();
            return true;
        });
        mGridView.setAdapter(mCategoryStorageAdapter);
        mGridView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub
                if (!isConnecting && !isLastpage && (mGridView.getLastVisiblePosition() == mGridView.getAdapter().getCount() - 1
                        && mGridView.getChildAt(mGridView.getChildCount() - 1).getBottom() <= mGridView.getHeight())) {
                    //scroll end reached
                    //Write your code here
                    getStorageList(true, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), mCurrentArray.getList().size() + "");
                    //Toast.makeText(mMainActivity, "LAST", Toast.LENGTH_SHORT).show();
                    Log.i("onScrollStateChanged", "mCurrentArray.getList().size() => " + mCurrentArray.getList().size() + " Add Item Action");

                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                // TODO Auto-generated method stub
            }
        });

        if (mSearch != null && mSearch.length() > 0) {
            folderSearchListDisplay(mCurrentPath, mSearch);
        } else {
            if (!listChange)
                storageListDisplay(mCurrentArray, false);
        }
    }

    private void changeListView(boolean listChange) {
        //final MainActivity context = (MainActivity) getActivity();
        ((MainActivity) getActivity()).setSelectedTitle("");
        mView_Type = 0;
        changeActionModeList();
        if (listChange) {
            mCategoryStorageAdapter = new CategoryStorageAdapter(getActivity(), null, mView_Type, mCategoryStorageAdapter.getList());
        } else {
            mCategoryStorageAdapter = new CategoryStorageAdapter(getActivity(), null, mView_Type);
        }
        mListView.setVisibility(View.VISIBLE);
        mGridView.setVisibility(View.GONE);
        mListView.setOnItemClickListener((parent, view, position, id) -> {
            FileItem selectFileItem = ((FileItem) mListView.getAdapter().getItem(position));
            if (mActionMode) {
                mCategoryStorageAdapter.setCheck(position);
                mCategoryStorageAdapter.notifyDataSetChanged();
                toolbarMenuIconChange();
                int count = mCategoryStorageAdapter.getCheckCount();
                ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
            } else {
                if (selectFileItem.isDirectory()) {
                    mSearch = null;
                    //Scroll Position Set
                    listPositionMap.put(selectFileItem.getId(), mListView.getFirstVisiblePosition());
                    CATEGORY_TYPE = CATEGORY_TOTAL;
                    getStorageList(false, mTeam.getId(), selectFileItem.getId(), "");
                    // go to the top of the ListView
                    //mListView.setSelection(0);
                } else {
//                        //파일일 경우
//                        downloadfile(mTeam.getId(), selectFileItem.getId(), selectFileItem.getName());
                    //파일일 경우
                    if (!checkFile(selectFileItem, false)) {
                        downloadfile(mTeam.getId(), selectFileItem, false);
                    }
                }
            }
        });

        mListView.setOnItemLongClickListener((parent, view, position, id) -> {
            mCategoryStorageAdapter.setCheck(position);
            int count = mCategoryStorageAdapter.getCheckCount();
            ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
            setActionMode();
            toolbarMenuIconChange();
            return true;
        });
        mListView.setAdapter(mCategoryStorageAdapter);
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub
                if (!isConnecting && !isLastpage && (mListView.getLastVisiblePosition() == mListView.getAdapter().getCount() - 1
                        && mListView.getChildAt(mListView.getChildCount() - 1).getBottom() <= mListView.getHeight())) {
                    //scroll end reached
                    //Write your code here
                    getStorageList(true, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), mCurrentArray.getList().size() + "");
                    //Toast.makeText(mMainActivity, "LAST", Toast.LENGTH_SHORT).show();
                    Log.i("onScrollStateChanged", "mCurrentArray.getList().size() => " + mCurrentArray.getList().size() + " Add Item Action");

                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                // TODO Auto-generated method stub
            }
        });

        if (mSearch != null && mSearch.length() > 0) {
            folderSearchListDisplay(mCurrentPath, mSearch);
        } else {
            if (!listChange)
                storageListDisplay(mCurrentArray, false);
        }
    }

    public void setScrollPosition(String path) {
        //Scroll Position Set
        int position;
        if (listPositionMap.containsKey(path)) {
            position = listPositionMap.get(path);
            listPositionMap.remove(path);
        } else {
            position = 0;
        }

        if (mView_Type == 0) {
            mListView.setSelection(position);
        } else {
            mGridView.setSelection(position);
        }
    }

    public void storageListDisplay(StorageListItem list, boolean listChange) {
        //mFoler_path.setText(mCurrentPath);
        mCurrentArray = list;

        mCategoryStorageAdapter.addContent(list.getList());
        //Scroll Position Set
        //setScrollPosition(mCurrentObjectId);
    }

    public void folderTagListDisplay(TagItem tag) {
        //mFoler_path.setText(tag.getName());
        mCurrentPath = "/";
        category_scroll_layout.post(new Runnable() {
            public void run() {
            }
        });
        mCategoryStorageAdapter.addTagFiles(tag);

        //Scroll Position Set
        setScrollPosition(tag.getName());
    }

    public void folderSearchListDisplay(String currentPath, String search) {
        //mFoler_path.setText(title);
        category_scroll_layout.post(new Runnable() {
            public void run() {
            }
        });
        if (mSearchArray != null) {
            mCategoryStorageAdapter.addContent(mSearchArray);
            mCategoryStorageAdapter.notifyDataSetChanged();
        } else {
            SearchTask mTask = new SearchTask(getActivity(), currentPath);
            mTask.execute(search);
        }
    }

    public void folderFavoriteListDisplay(String title) {
        //mFoler_path.setText(title);
        mCurrentPath = "/";
        category_scroll_layout.post(new Runnable() {
            public void run() {
            }
        });
        mCategoryStorageAdapter.addFavoriteFiles();

        //Scroll Position Set
        setScrollPosition(title);
    }

    public void toolbarMenuIconChange() {
        ArrayList<FileItem> items = mCategoryStorageAdapter.getCheckList();
        getActivity().setTitle(items.size() + " " + getResources().getString(R.string.selected));
        boolean allfiles = true;
        boolean allfavorite = true;
        if (items != null && items.size() > 0) {
            if (items.size() == 1) {
                //File file = new File(items.get(0).getPath());
                if (!items.get(0).isDirectory()) {
                    ((MainActivity) mMainContext).item_menu_another_app.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_rename.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_send_member.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_secretfolder.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_favorite.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_tag.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_e_album.setVisibility(View.VISIBLE);
                } else {
                    ((MainActivity) mMainContext).item_menu_another_app.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_rename.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_send_member.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_secretfolder.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_favorite.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_tag.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_e_album.setVisibility(View.GONE);
                }
            } else {
                for (FileItem item : items) {
                    if (allfiles) {
                        if (item.isDirectory()) {
                            allfiles = false;
                        }
                    }
                    if (allfavorite) {
                        if (!item.getFavorite()) {
                            allfavorite = false;
                        }
                    }
                }
//        if (items != null && items.size() > 0) {
//            for (FileItem item : items) {
//                if (allfiles) {
//                    File file = new File(item.getPath());
//                    if (file.isDirectory()) {
//                        allfiles = false;
//                    }
//                }
//                if (allfavorite) {
//                    if (!item.getFavorite()) {
//                        allfavorite = false;
//                    }
//                }
//            }
//            if(allhide){
//                ((MainActivity)mMainContext).toolbar.getMenu().getItem(0).setIcon(R.mipmap.ic_unhide);
//            }else{
//                ((MainActivity)mMainContext).toolbar.getMenu().getItem(0).setIcon(R.mipmap.ic_hide);
//            }

                if (items.size() == 1 && new File(items.get(0).getPath()).isFile()) {
                    ((MainActivity) mMainContext).toolbar.getMenu().getItem(0).setVisible(true);
                } else {
                    ((MainActivity) mMainContext).toolbar.getMenu().getItem(0).setVisible(false);
                }
                if (allfiles) {
                    ((MainActivity) mMainContext).toolbar.getMenu().getItem(1).setVisible(true);
                    ((MainActivity) mMainContext).toolbar.getMenu().getItem(4).setVisible(true);
                } else {
                    ((MainActivity) mMainContext).toolbar.getMenu().getItem(1).setVisible(false);
                    ((MainActivity) mMainContext).toolbar.getMenu().getItem(4).setVisible(false);
                }
                if (allfavorite) {
                    ((MainActivity) mMainContext).toolbar.getMenu().getItem(5).setIcon(R.mipmap.ic_unfavorite);
                } else {
                    ((MainActivity) mMainContext).toolbar.getMenu().getItem(5).setIcon(R.mipmap.ic_favorite);
                }
            }
        } else {
            ((MainActivity) mMainContext).toolbar.getMenu().getItem(0).setVisible(false);
            ((MainActivity) mMainContext).toolbar.getMenu().getItem(1).setVisible(false);
            ((MainActivity) mMainContext).toolbar.getMenu().getItem(5).setIcon(R.mipmap.ic_favorite);
        }
    }

    public void setActionMode() {
        mActionMode = true;
        getActivity().setTitle("0 " + getResources().getString(R.string.selected));
        ((MainActivity) mMainContext).toolbar.getMenu().clear();
        ((MainActivity) mMainContext).toolbar.inflateMenu(R.menu.action_mode);
        ((MainActivity) mMainContext).finishSearchMode();
        ((MainActivity) mMainContext).multiSelectMode();
        mCategoryStorageAdapter.notifyDataSetChanged();
    }

    public void finishActionMode() {
        ((MainActivity) getActivity()).setTitle(getResources().getString(R.string.app_name));
        mActionMode = false;
        changeActionModeList();
//        if(ClipBoard.isEmpty()){
//            list_bottom_menu_paste.setVisibility(View.GONE);
//        }else{
//            list_bottom_menu_paste.setVisibility(View.VISIBLE);
//        }
        ((MainActivity) mMainContext).finishMultiSelectMode();
        mCategoryStorageAdapter.setCheckClear();
        mCategoryStorageAdapter.notifyDataSetChanged();
    }

    public void changeActionModeList() {
        if (mMainContext != null && ((MainActivity) mMainContext).toolbar != null) {
            ((MainActivity) mMainContext).toolbar.getMenu().clear();
            ((MainActivity) mMainContext).toolbar.inflateMenu(R.menu.main_no_newfolder);
        }
    }

    public void listAllcheck() {
//        mCategoryStorageAdapter.setAllCheck();
//        mCategoryStorageAdapter.notifyDataSetChanged();
        if (mCategoryStorageAdapter.isAllChecked()) {
            mCategoryStorageAdapter.setCheckClear();
            mCategoryStorageAdapter.notifyDataSetChanged();
        } else {
            mCategoryStorageAdapter.setAllCheck();
            mCategoryStorageAdapter.notifyDataSetChanged();
        }
        int count = mCategoryStorageAdapter.getCheckCount();
        ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
    }

    public void listCheckClear() {
        mCategoryStorageAdapter.setCheckClear();
        mCategoryStorageAdapter.notifyDataSetChanged();
    }

    public boolean onBackPressed() {

        if (mActionMode) {
            finishActionMode();
            return true;
        } else if (mCurrentPath != null && mCurrentPath.equals("/")) {
            //getActivity().finish();
            ((MainActivity) mMainContext).initFragment();
            return false;
        } else {
            try {
                if (mSearch != null && mSearch.length() > 0) {
                    mSearch = null;
                    if (mCurrentPath.equals(Utils.getInternalDirectoryPath())) {
                        ((MainActivity) mMainContext).initFragment();
                    } else {
                        storageListDisplay(mCurrentArray, false);
                    }
                } else {
                    ((MainActivity) mMainContext).initFragment();
                }
            } catch (Exception e) {
                //getActivity().finish();
                ((MainActivity) mMainContext).initFragment();
                return false;
            }
            // get position of the previous folder in ListView
            //mListView.setSelection(mListAdapter.getPosition(file.getPath()));
            return true;
        }
    }

    public void shareMethod() {

        int check_count = mCategoryStorageAdapter.getCheckCount();
        ArrayList<FileItem> items = mCategoryStorageAdapter.getCheckList();
        if (items != null && items.size() > 0) {
            Utils.sendFiles(mMainContext, items);
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        finishActionMode();
    }

    public void secretMethod() {
        final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
        final int check_count = mCategoryStorageAdapter.getCheckCount();
        final ArrayList<FileItem> items = mCategoryStorageAdapter.getCheckList();
        if (items != null && items.size() > 0) {
            mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
            LayoutInflater inflater = LayoutInflater.from(mMainContext);
            View view = inflater.inflate(R.layout.dialog_base, null, false);
            TextView title = (TextView) view.findViewById(R.id.title);
            title.setText(getResources().getString(R.string.left_menu_secretfolder));
            TextView content = (TextView) view.findViewById(R.id.content);
            content.setText(getResources().getString(R.string.secretfolder_do_you_want_save_select_file_in_secretfolder_1) + check_count
                    + getResources().getString(R.string.secretfolder_do_you_want_save_select_file_in_secretfolder_2));
            TextView cancel = (TextView) view.findViewById(R.id.cancel);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                }
            });
            TextView ok = (TextView) view.findViewById(R.id.ok);
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (FileItem item : items) {
                        File from = new File(item.getPath());
//                        File directory = from.getParentFile();
//                        File to = new File(directory, item.getName().replaceFirst(".", ""));
//                        from.renameTo(to);
//                        item.setPath(directory.getPath() + File.separator + item.getName().replaceFirst(".", ""));
//                        item.setName(item.getName().replaceFirst(".", ""));
                        // 시스템으로부터 현재시간(ms) 가져오기
                        long now = System.currentTimeMillis();
                        Utils.moveToSecretDirectory(from, "." + Utils.stripFilenameExtension(from.getName()), mMainContext);
                    }
                    //Toast.makeText(mMainContext, check_count + getResources().getString(R.string.secretfolder_move_to_secretfolder), Toast.LENGTH_SHORT).show();
                    Toast.makeText(mMainContext, getResources().getString(R.string.secretfolder_move_to_secretfolder), Toast.LENGTH_SHORT).show();
                    listRefresh();
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                }
            });
            mDialog.setContentView(view);
            mDialog.show();
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        finishActionMode();
    }

    public void hideMethod() {
        final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
        int check_count = mCategoryStorageAdapter.getCheckCount();
        ArrayList<FileItem> items = mCategoryStorageAdapter.getCheckList();
        if (items != null && items.size() > 0) {
            boolean allHide = true;
            for (FileItem item : items) {
                if (!item.getName().startsWith(".")) {
                    allHide = false;
                    break;
                }
            }
            for (FileItem item : items) {
                if (allHide) {
                    File from = new File(item.getPath());
                    File directory = from.getParentFile();
                    File to = new File(directory, item.getName().replaceFirst(".", ""));
                    from.renameTo(to);
                    item.setPath(directory.getPath() + File.separator + item.getName().replaceFirst(".", ""));
                    item.setName(item.getName().replaceFirst(".", ""));
                } else {
                    if (!item.getName().startsWith(".")) {
                        File from = new File(item.getPath());
                        File directory = from.getParentFile();
                        File to = new File(directory, "." + item.getName());
                        from.renameTo(to);
                        item.setPath(directory.getPath() + File.separator + "." + item.getName());
                        item.setName("." + item.getName());
                    }
                }
            }
            if (allHide) {
                Toast.makeText(mMainContext, check_count + getResources().getString(R.string.snackbar_file_unhide), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mMainContext, check_count + getResources().getString(R.string.snackbar_file_hide), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        finishActionMode();
    }

    public void copyMethod(final boolean oriRemove) {
        int count = mCategoryStorageAdapter.getCheckCount();

        if (count > 0) {
            ArrayList<FileItem> items = mCategoryStorageAdapter.getCheckList();
//            boolean checkFolder = false;
//            for(FileItem item : items){
//                if(item.isDirectory()){
//                    checkFolder = true;
//                }
//            }
            if (oriRemove) {
                ClipBoard.cutMove(items);
//                Snackbar.make(viewPos, getResources().getString(R.string.snackbar_move), Snackbar.LENGTH_SHORT)
//                        .setAction("Action", null).show();
            } else {
                ClipBoard.cutCopy(items);
//                Snackbar.make(viewPos, getResources().getString(R.string.snackbar_copy), Snackbar.LENGTH_SHORT)
//                        .setAction("Action", null).show();
            }

            FolderSelectTotalDialog.ORI_FILE_TYPE = FolderSelectTotalDialog.LOCAL_MEMORY;
            FolderSelectTotalDialog selectDialog = new FolderSelectTotalDialog(getActivity(), R.style.Theme_TransparentBackground, getActivity(), !oriRemove, false);
            selectDialog.show();

        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        //finishActionMode();
    }

    public void favoriteMethod() {
        int check_count = mCategoryStorageAdapter.getCheckCount();
        ArrayList<FileItem> items = mCategoryStorageAdapter.getCheckList();
        if (items != null && items.size() > 0) {
            boolean allFavorite = true;
            for (FileItem item : items) {
                if (!item.getFavorite()) {
                    allFavorite = false;
                    break;
                }
            }
            for (FileItem item : items) {
                if (allFavorite) {
//                    if(item.getTagId() != null && item.getTagId().length() > 0){
//                        MainActivity.mMds.updateFileFavorite(item.getId(), "N");
//                    }else
                    {
                        MainActivity.mMds.deleteTag(item.getId());
                        mCategoryStorageAdapter.removeFavoriteFiles(item);
                    }
                    item.setId("");
                } else {
//                    if(item.getFavorite() != null && item.getFavorite().length() == 0){
//                        MainActivity.mMds.updateFileFavorite(item.getId(), "Y");
//                    }else{
//                        MainActivity.mMds.insertFavoriteItem(item.getPath());
//                    }
                    item.setId(MainActivity.mMds.insertFavoriteItem(item.getPath()) + "");
                }
            }

            if (allFavorite) {
                Toast.makeText(mMainContext, check_count + getResources().getString(R.string.snackbar_file_unfavorite), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mMainContext, check_count + getResources().getString(R.string.snackbar_file_favorite), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        mCategoryStorageAdapter.notifyDataSetChanged();
        finishActionMode();
    }

//    public void renameStorageFolder(FileItem item, String name){
//        try{
//            ((MainActivity)mMainContext).showProgress();
//            JMF_network.modifyFolderName(mTeam.getId(), item.getId(), name)
//                    .flatMap(gigaPodPair -> {
//                        return Observable.just(gigaPodPair);
//                    })
//                    .doOnError(throwable -> {
//                        ((MainActivity)mMainContext).dismissProgress();
//                    })
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(gigaPodPair -> {
//                                ((MainActivity)mMainContext).dismissProgress();
//                                if (gigaPodPair.first) {
//                                    Log.i("JMF deleteFolder", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
//                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
//                                    Toast.makeText(mMainContext, mMainContext.getString(R.string.snackbar_file_rename), Toast.LENGTH_SHORT).show();
//                                    item.setName(name);
//                                    finishActionMode();
//                                } else {
//                                    Toast.makeText(mMainContext, mMainContext.getString(R.string.snackbar_rename_error), Toast.LENGTH_SHORT).show();
//                                    //Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
//                                }
//                            },
//                            throwable -> {
//                                ((MainActivity)mMainContext).dismissProgress();
//                                throwable.printStackTrace();
//                            });
//        } catch (Exception e) {
//            ((MainActivity)mMainContext).dismissProgress();
//            e.printStackTrace();
//            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
//        }
//    }

    public void tagMethod(TagItem tag) {

        ArrayList<FileItem> items = mCategoryStorageAdapter.getCheckList();
        if (items != null && items.size() > 0) {
            for (FileItem item : items) {
                if (tag == null) {
                    MainActivity.mMds.deleteTag(item.getId());
                    item.setId("");
                } else {
                    item.setId(MainActivity.mMds.insertTagItem(tag.getName(), item.getPath()) + "");
                }
            }
//            Snackbar.make(viewPos, "태그", Snackbar.LENGTH_SHORT)
//                    .setAction("Action", null).show();
            mCategoryStorageAdapter.refresh();
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        finishActionMode();
    }

    public void deleteMethod() {
        int check_count = mCategoryStorageAdapter.getCheckCount();

        if (check_count > 0) {
            dialogDeleteFile(check_count);
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
            //finishActionMode();
        }
    }

    public void renameMethod() {
        int check_count = mCategoryStorageAdapter.getCheckCount();
        if (check_count > 0) {
            dialogRename();
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
            //finishActionMode();
        }
    }

    public void dialogRename() {
        ArrayList<FileItem> items = mCategoryStorageAdapter.getCheckList();
        FileItem item = items.get(0);
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        final View view = inflater.inflate(R.layout.dialog_new_folder, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.dialog_rename_file));
        final EditText edt = (EditText) view.findViewById(R.id.edt);
        String ext = Utils.getFilenameExtension(item.getName());
        edt.setText(Utils.stripFilenameExtension(item.getName()));
//        edt.setText(item.getName());
        edt.requestFocus();
        InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView create = (TextView) view.findViewById(R.id.create);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String folder_name = edt.getText().toString();
                if (folder_name != null && folder_name.length() > 0) {
                    String name = "";
                    if (item.getSObjectType().equals("D")) {
                        name = folder_name;
                    } else {
                        if (ext != null && ext.length() > 0) {
                            name = folder_name + "." + ext;
                        }
//                        name = Utils.stripFilenameExtension(folder_name);
                        else
                            name = folder_name;
                    }
                    renameStorageFile(item, name);

                    InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                } else {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_input_foldername), Toast.LENGTH_SHORT).show();
                }

            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void renameStorageFile(FileItem item, String name) {
        try {
            ((MainActivity) mMainContext).showProgress();
            JMF_network.modifyFileName(mTeam.getId(), item.getId(), name)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        ((MainActivity) mMainContext).dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF modifyFileName", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    Toast.makeText(mMainContext, mMainContext.getString(R.string.snackbar_file_rename), Toast.LENGTH_SHORT).show();
                                    item.setName(name);
                                    finishActionMode();
                                } else {
                                    try {
                                        String errorMsg = (String) gigaPodPair.second;
                                        Toast.makeText(mMainContext, errorMsg, Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        Toast.makeText(mMainContext, mMainContext.getString(R.string.snackbar_rename_error), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            throwable -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            ((MainActivity) mMainContext).dismissProgress();
            e.printStackTrace();
            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void sortDate() {
        if (MyFileSettings.getSortType() == SortUtils.SORT_DATE) {
            MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
        } else {
            MyFileSettings.setSortType(SortUtils.SORT_DATE);
        }
        mCategoryStorageAdapter.reSort();
    }

    public void sortName() {
        if (MyFileSettings.getSortType() == SortUtils.SORT_ALPHA) {
            MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
        } else {
            MyFileSettings.setSortType(SortUtils.SORT_ALPHA);
        }
        mCategoryStorageAdapter.reSort();
    }

    public void sortType() {
        if (MyFileSettings.getSortType() == SortUtils.SORT_TYPE) {
            MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
        } else {
            MyFileSettings.setSortType(SortUtils.SORT_TYPE);
        }
        mCategoryStorageAdapter.reSort();
    }

    public void sortSize() {
        if (MyFileSettings.getSortType() == SortUtils.SORT_SIZE) {
            MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
        } else {
            MyFileSettings.setSortType(SortUtils.SORT_SIZE);
        }
        mCategoryStorageAdapter.reSort();
    }

    public void dialogSetSort() {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.dialog_select_sort, null, false);
        LinearLayout sort_by_name = (LinearLayout) view.findViewById(R.id.sort_by_name);
        TextView sort_by_name_txt = (TextView) view.findViewById(R.id.sort_by_name_txt);
        if (MyFileSettings.getSortType() != SortUtils.SORT_ALPHA) {
            sort_by_name_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        } else {
            if (!MyFileSettings.getSortReverse()) {
                sort_by_name_txt.setText(sort_by_name_txt.getText().toString() + " ↑");
            } else {
                sort_by_name_txt.setText(sort_by_name_txt.getText().toString() + " ↓");
            }
        }
        sort_by_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                if (MyFileSettings.getSortType() == SortUtils.SORT_ALPHA) {
                    MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
                } else {
                    MyFileSettings.setSortType(SortUtils.SORT_ALPHA);
                }

                mCategoryStorageAdapter.reSort();
            }
        });
        LinearLayout sort_by_extension = (LinearLayout) view.findViewById(R.id.sort_by_extension);
        TextView sort_by_extension_txt = (TextView) view.findViewById(R.id.sort_by_extension_txt);
        if (MyFileSettings.getSortType() != SortUtils.SORT_TYPE) {
            sort_by_extension_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        } else {
            if (!MyFileSettings.getSortReverse()) {
                sort_by_extension_txt.setText(sort_by_extension_txt.getText().toString() + " ↑");
            } else {
                sort_by_extension_txt.setText(sort_by_extension_txt.getText().toString() + " ↓");
            }
        }
        sort_by_extension.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                if (MyFileSettings.getSortType() == SortUtils.SORT_TYPE) {
                    MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
                } else {
                    MyFileSettings.setSortType(SortUtils.SORT_TYPE);
                }
                mCategoryStorageAdapter.reSort();
            }
        });
        LinearLayout sort_by_date = (LinearLayout) view.findViewById(R.id.sort_by_date);
        TextView sort_by_date_txt = (TextView) view.findViewById(R.id.sort_by_date_txt);
        if (MyFileSettings.getSortType() != SortUtils.SORT_DATE) {
            sort_by_date_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        } else {
            if (!MyFileSettings.getSortReverse()) {
                sort_by_date_txt.setText(sort_by_date_txt.getText().toString() + " ↑");
            } else {
                sort_by_date_txt.setText(sort_by_date_txt.getText().toString() + " ↓");
            }
        }
        sort_by_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                if (MyFileSettings.getSortType() == SortUtils.SORT_DATE) {
                    MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
                } else {
                    MyFileSettings.setSortType(SortUtils.SORT_DATE);
                }
                mCategoryStorageAdapter.reSort();
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void dialogDeleteFile(final int check_count) {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.dialog_base, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.dialog_delete_file));
        TextView content = (TextView) view.findViewById(R.id.content);
        content.setText(getResources().getString(R.string.dialog_delete_file_info1) + check_count + getResources().getString(R.string.dialog_delete_file_info2));
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<FileItem> items = mCategoryStorageAdapter.getCheckList();

                boolean success = true;

                for (FileItem item : items) {
                    File select = new File(item.getPath());
                    if (select != null && select.exists()) {
                        if (select.isDirectory()) {
                            success = deleteFile(select);
                            if (!success) {
                                break;
                            }
                            select.delete();
                        } else {
                            if (select.delete()) {
//                                MainActivity.mMds.deleteTagPath(item.getPath());
//                                MainActivity.mMds.deleteFavorite(item.getFavoriteId());
                                MainActivity.mMds.deleteFile(item.getPath());
                            } else {
                                success = false;
                                break;
                            }
                        }
                    }
                }
                if (success) {
                    Toast.makeText(mMainContext, check_count + getResources().getString(R.string.snackbar_file_delete), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_delete_error), Toast.LENGTH_SHORT).show();
                }
                mCategoryStorageAdapter.refresh();
                finishActionMode();

                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public boolean deleteFile(File file) {
        File[] fileList = file.listFiles();
        boolean success = true;
        for (File temp : fileList) {
            if (temp.isDirectory()) {
                success = deleteFile(temp);
                if (!success) {
                    break;
                }
                temp.delete();
            } else {
                if (temp.delete()) {
//                    ArrayList<FileItem> tempList = MainActivity.mMds.selectTagFiles(temp.getPath());
//                    for(FileItem sub : tempList){
//                        MainActivity.mMds.deleteFile(sub.getPath());
//                    }
                    MainActivity.mMds.deleteFile(temp.getPath());
                } else {
                    success = false;
                    break;
                }
            }
        }
        return success;
    }

    public void listRefresh() {
        //mFolderAdapter.refresh();
        if (ImageSlideViewerActivity.mDataSource != null && ImageSlideViewerActivity.mPosition >= 0) {
            FileItem item = ImageSlideViewerActivity.mDataSource.get(ImageSlideViewerActivity.mPosition);
            ArrayList<FileItem> items = mCategoryStorageAdapter.getList();
            int pos = 0;
            for (FileItem temp : items) {
                if (item.getPath().equals(temp.getPath())) {
                    break;
                } else {
                    pos++;
                }
            }
            if (mView_Type == 0) {
                mListView.setSelection(pos);
            } else {
                mGridView.setSelection(pos);
            }
        }
        ImageSlideViewerActivity.mDataSource = mCategoryStorageAdapter.getImageList();
        ImageSlideViewerActivity.mPosition = -1;

        finishActionMode();
    }

    public ArrayList<FileItem> getCheckList() {
        return mCategoryStorageAdapter.getCheckList();
    }

    public void dialogNewFolder() {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        final View view = inflater.inflate(R.layout.dialog_new_folder, null, false);
        final EditText edt = (EditText) view.findViewById(R.id.edt);

        edt.requestFocus();
        InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView create = (TextView) view.findViewById(R.id.create);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String folder_name = edt.getText().toString();
                if (folder_name != null && folder_name.length() > 0) {
                    File file = new File(mCurrentPath);
                    Log.e("readable->", "" + file.canRead());
                    Log.e("writable->", "" + file.canWrite());
                    Log.e("executable->", "" + file.canExecute());

                    String createPath = file.getAbsolutePath() + File.separator + folder_name;

                    File create = new File(createPath);

                    if (create.exists()) {
                        Toast.makeText(mMainContext, getResources().getString(R.string.overwrite_title_folder), Toast.LENGTH_SHORT).show();
                    } else {
                        //create.mkdirs();
                        if (create.mkdirs()) {
                            //success
                        } else {
                            //fail
                            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_folder_create_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                    listRefresh();

                    InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                } else {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_input_foldername), Toast.LENGTH_SHORT).show();
                }

            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void initSecretFolder() {
        //String password = PreferenceHelper.getString(PreferenceHelper.password, "");
        String password = MainActivity.mMds.getSecretUserPassword();
        if (password != null && password.length() > 0) {
            secretMethod();
        } else {
            mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
            LayoutInflater inflater = LayoutInflater.from(mMainContext);
            View view = inflater.inflate(R.layout.dialog_base, null, false);
            TextView title = (TextView) view.findViewById(R.id.title);
            title.setText(getResources().getString(R.string.left_menu_secretfolder));
            TextView content = (TextView) view.findViewById(R.id.content);
            content.setText(getResources().getString(R.string.secretfolder_activate_your_secretfolder_now));
            TextView cancel = (TextView) view.findViewById(R.id.cancel);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                    finishActionMode();
                }
            });
            TextView ok = (TextView) view.findViewById(R.id.ok);
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_frame, new SecretFolderFragment(mMainContext, Utils.getInternalDirectoryPath(), true))
                            .commit();
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                    finishActionMode();
                }
            });
            mDialog.setContentView(view);
            mDialog.show();
        }
    }

    public void moveToStorageRoot(int position) {
        mTeam = MultiFragment.teamItams.get(position);
        try {
            ((MainActivity) mMainContext).showProgress();
            JMF_network.getRootPath(mTeam.getId())
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        ((MainActivity) mMainContext).dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                if (gigaPodPair.first) {
                                    Log.i("JMF getRootPath", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    JsonObject current = result.getAsJsonObject("current");

                                    getStorageList(false, mTeam.getId(), current.get("object_id").getAsString(), "");
                                    //Toast.makeText(mMainContext, "object_id => " + object_id, Toast.LENGTH_SHORT).show();
                                } else {
                                    ((MainActivity) mMainContext).dismissProgress();
                                    Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            ((MainActivity) mMainContext).dismissProgress();
            e.printStackTrace();
            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void getStorageList(boolean paging, String teamId, String objectId, String offset) {
        try {
            ((MainActivity) getActivity()).setTitle(mCurrentPath);
            ((MainActivity) mMainContext).showProgress();
            isConnecting = true;
            String sort = "";
            if (MyFileSettings.getSortReverse()) {
                sort = "+";
            } else {
                sort = "-";
            }
            switch (MyFileSettings.getSortType()) {
                case SortUtils.SORT_ALPHA:
                    sort += "name";
                    break;
                case SortUtils.SORT_TYPE:
                    sort += "extension";
                    break;
                case SortUtils.SORT_SIZE:
                    sort += "size";
                    break;
                case SortUtils.SORT_DATE:
                    sort += "regdate";
                    break;
                default:
                    sort += "name";
                    break;
            }
            String assort = "";
            switch (CATEGORY_TYPE) {
                case CATEGORY_APP:
                    assort = "application";
                    break;
                case CATEGORY_IMAGE:
                    assort = "picture";
                    break;
                case CATEGORY_MOVIE:
                    assort = "movie";
                    break;
                case CATEGORY_MUSIC:
                    assort = "music";
                    break;
                case CATEGORY_DOCUMENT:
                    assort = "document";
                    break;
                case CATEGORY_ZIP:
                    assort = "archive";
                    break;
                default:
                    assort = "";
                    break;
            }

            String finalAssort = assort;
            JMF_network.getFolderList(teamId, objectId, sort, LIST_LIMIT, offset, assort)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        ((MainActivity) mMainContext).dismissProgress();
                        isConnecting = false;
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                isConnecting = false;
                                if (gigaPodPair.first) {
                                    Log.i("JMF getStorageList", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");

                                    JsonObject current = result.getAsJsonObject("current");
                                    CurrentItem currentItem = new CurrentItem();
                                    currentItem.setObjectId(current.get("object_id").getAsString());
                                    currentItem.setNode(current.get("node").getAsString());
                                    if (CATEGORY_TYPE < 0) {
                                        currentItem.setName(finalAssort);
                                        currentItem.setCategoryId(CATEGORY_TYPE + "");
                                    } else {
                                        currentItem.setName(current.get("name").getAsString());
                                        currentItem.setCategoryId("");
                                    }
                                    currentItem.setSize(current.get("size").getAsString());
                                    currentItem.setRegdate(Utils.formatDate(current.get("regdate").getAsString()));
                                    currentItem.setModified(Utils.formatDate(current.get("modified").getAsString()));

                                    JsonObject lists = result.getAsJsonObject("lists");
                                    isLastpage = lists.get("lastpage").getAsBoolean();
                                    JsonArray rows = lists.getAsJsonArray("rows");
                                    ArrayList<FileItem> storageList = new ArrayList<FileItem>();
                                    for (int i = 0; i < rows.size(); i++) {
                                        FileItem item = new FileItem();
                                        JsonObject row = rows.get(i).getAsJsonObject();
                                        item.setTeamId(teamId);
                                        item.setId(row.get("object_id").getAsString());
                                        item.setSObjectType(row.get("object_type").getAsString());
                                        item.setSNode(row.get("node").getAsString());
                                        item.setName(row.get("name").getAsString());
                                        item.setSCreator(row.get("creator").getAsString());
                                        item.setSCreatorName(row.get("creator_name").getAsString());
                                        item.setSMemo(row.get("memo").getAsString());
                                        item.setSize(row.get("size").getAsString());
                                        item.setSExtension(row.get("extension").getAsString());
                                        item.setSThumbnail(row.get("thumbnail").getAsString());
                                        item.setSUrlPath(row.get("url_path").getAsString());
                                        item.setRegDate(Utils.formatDate(row.get("regdate").getAsString()));
                                        item.setSModifedDate(Utils.formatDate(row.get("modified").getAsString()));

                                        if (paging) {
                                            mCurrentArray.getList().add(item);
                                        } else {
                                            storageList.add(item);
                                        }
                                    }

                                    if (paging) {
                                        if (offset != null && offset.length() == 0) {
                                            mListView.setSelection(0);
                                            mGridView.setSelection(0);
                                        }
//                                        mStoageAdapter.addContent(mCurrentArray.getList());
//                                        mStoageAdapter.notifyDataSetChanged();
                                    } else {
                                        StorageListItem storageListItem = new StorageListItem(currentItem, storageList);

                                        storageListDisplay(storageListItem, false);
                                    }
                                } else {
                                    Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                isConnecting = false;
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            ((MainActivity) mMainContext).dismissProgress();
            isConnecting = false;
            e.printStackTrace();
            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean checkFile(FileItem item, boolean share) {
        try {

            String path = Utils.getStorageSaveFileFolderPath() + "/" + item.getName();
            File file = new File(path);

            if (file != null && file.exists()) {
                if (share) {
                    ArrayList<FileItem> items = mCategoryStorageAdapter.getCheckList();
                    items.get(0).setPath(path);
                    Utils.sendFiles(mMainContext, items);
                    finishActionMode();
                    return true;
                }
                //if (MimeTypes.isPicture(file)) {
                if (MimeTypes.isPicture(file) || MimeTypes.isPdf(file) || MimeTypes.isVideo(file)) {
                    Intent intent = new Intent(mMainContext, ImageSlideViewerActivity.class);
                    ArrayList<FileItem> tempList = new ArrayList<FileItem>();
                    item.setcheck(true);
                    item.setPath(path);
                    tempList.add(item);
                    ImageSlideViewerActivity.mDataSource = tempList;
                    ImageSlideViewerActivity.mTeam = mTeam;
                    ImageSlideViewerActivity.mPosition = 0;
                    ((MainActivity) mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_SLIDE_VIEWER);
                    return true;
                }
                Utils.openFile(mMainContext, file);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //public void downloadfile(String teamId, String objectId, String name) {
    public void downloadfile(String teamId, FileItem downItem, boolean share) {
        try {
            showDownlaodProgress();
            //JMF_network.filedownload(teamId, objectId, Utils.getStorageSaveFileFolderPath(), name, listener)
            JMF_network.filedownload(teamId, downItem.getId(), Utils.getStorageSaveFileFolderPath(), downItem.getName(), listener)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissDownlaodProgress();
                        isConnecting = false;
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissDownlaodProgress();
                                isConnecting = false;
                                if (gigaPodPair.first) {
                                    Log.i("JMF filedownload", "=>=>" + ((String) gigaPodPair.second));
                                    String path = ((String) gigaPodPair.second);
                                    File file = new File(path);

                                    if (share) {
                                        ArrayList<FileItem> items = mCategoryStorageAdapter.getCheckList();
                                        items.get(0).setPath(path);
                                        Utils.sendFiles(mMainContext, items);
                                        finishActionMode();
                                        return;
                                    }

                                    //if (MimeTypes.isPicture(file)) {
                                    if (MimeTypes.isPicture(file) || MimeTypes.isPdf(file) || MimeTypes.isVideo(file)) {
                                        Intent intent = new Intent(mMainContext, ImageSlideViewerActivity.class);
                                        ArrayList<FileItem> tempList = new ArrayList<FileItem>();
                                        downItem.setcheck(true);
                                        downItem.setPath(path);
                                        tempList.add(downItem);
                                        ImageSlideViewerActivity.mDataSource = tempList;
                                        ImageSlideViewerActivity.mTeam = mTeam;
                                        ImageSlideViewerActivity.mPosition = 0;
                                        ((MainActivity) mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_SLIDE_VIEWER);
                                        return;
                                    }

                                    Utils.openFile(mMainContext, file);
                                } else {
                                    Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissDownlaodProgress();
                                isConnecting = false;
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissDownlaodProgress();
            isConnecting = false;
            e.printStackTrace();
            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void showDownlaodProgress() {
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.progress_download_dialog_gray, null, false);

        download_percent_dp = (DonutProgress) view.findViewById(R.id.download_percent_dp);
        downloadProgressDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        downloadProgressDialog.setContentView(view);
        downloadProgressDialog.setCancelable(false);
        downloadProgressDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        downloadProgressDialog.show();
    }

    public void setDownlaodProgress(Integer percent) {
        download_percent_dp.setProgress(percent);
    }

    public void dismissDownlaodProgress() {
        if (downloadProgressDialog != null) {
            downloadProgressDialog.dismiss();
        }
    }

    //Search Method
    private class SearchTask extends AsyncTask<String, Void, ArrayList<FileItem>> {
        private final Context context;
        private ProgressDialog pr_dialog;
        private String mCurrentPath;

        private String mSearchStr;

        private SearchTask(Context c, String currentPath) {
            context = c;
            File file = new File(currentPath);
            if (file != null && file.isDirectory()) {
                mCurrentPath = currentPath;
            } else {
                mCurrentPath = Utils.getInternalDirectoryPath();
            }
        }

        @Override
        protected void onPreExecute() {
            pr_dialog = ProgressDialog.show(context, null,
                    context.getResources().getString(R.string.action_search));
            pr_dialog.setCanceledOnTouchOutside(true);
        }

        @Override
        protected ArrayList<FileItem> doInBackground(String... params) {
            //String location = BrowserTabsAdapter.getCurrentBrowserFragment().mCurrentPath;
            mSearchStr = params[0];
            return Utils.searchInDirectory(mCurrentPath, mSearchStr);
        }

        @Override
        protected void onPostExecute(final ArrayList<FileItem> files) {
            int len = files != null ? files.size() : 0;
            mSearchArray = files;
            pr_dialog.dismiss();
            if (len == 0) {
                try {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_search_result_empty), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_search_result_empty), Toast.LENGTH_SHORT).show();
                }
            } else {
                SortUtils.sortList(files, mCurrentPath, MyFileSettings.getSortType());
                mCategoryStorageAdapter.addContent(files);
                mCategoryStorageAdapter.notifyDataSetChanged();
            }
            setScrollPosition(mSearchStr);
        }
    }

    private class LoadingTask extends AsyncTask<String, Void, ArrayList<FileItem>> {
        private final Context context;
        private ProgressDialog pr_dialog;
        private String mCurrentPath;

        private LoadingTask(Context c, String currentPath) {
            context = c;
            mCurrentPath = currentPath;
        }

        @Override
        protected void onPreExecute() {
            pr_dialog = ProgressDialog.show(context, null,
                    context.getResources().getString(R.string.action_loading));
            pr_dialog.setCanceledOnTouchOutside(true);
        }

        @Override
        protected ArrayList<FileItem> doInBackground(String... params) {
            //String location = BrowserTabsAdapter.getCurrentBrowserFragment().mCurrentPath;

            if (mCurrentPath.equals(getResources().getString(R.string.category_document))) {
                return ListFunctionUtils.listAllDocumentFiles(context);
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_app))) {
                return ListFunctionUtils.listAllAPKFiles(context);
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_zip))) {
                return ListFunctionUtils.listAllZipFiles(context);
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_recentfile))) {
                return ListFunctionUtils.listDaysFiles(context, ListFunctionUtils.LIST_DAY);
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_largefile))) {
                return ListFunctionUtils.listHugeFiles(context, FileObserverService.NEW_FILE_SIZE_LIMIT_100MB);
            } else {
                return Utils.searchInDirectory(mCurrentPath, params[0]);
            }
        }

        @Override
        protected void onPostExecute(final ArrayList<FileItem> files) {
            int len = files != null ? files.size() : 0;
            if (pr_dialog != null && pr_dialog.isShowing()) {
                pr_dialog.dismiss();
            }
            if (len == 0) {
                Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_search_result_empty), Toast.LENGTH_SHORT).show();
            } else {
                SortUtils.sortList(files, mCurrentPath, MyFileSettings.getSortType());
                mCategoryStorageAdapter.addContent(files);
                mCategoryStorageAdapter.notifyDataSetChanged();
            }
        }
    }

    public class SpinnerInteractionListener implements AdapterView.OnItemSelectedListener, View.OnTouchListener {

        boolean userSelect = false;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            userSelect = true;
            return false;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            if (userSelect) {
                // Your selection handling code here
                if (pos == 0) {
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_frame, new CategoryFragment(mMainContext, mCurrentPath, true))
                            .commit();
                } else {
                    moveToStorageRoot(pos - 1);
                }

                userSelect = false;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }
}
