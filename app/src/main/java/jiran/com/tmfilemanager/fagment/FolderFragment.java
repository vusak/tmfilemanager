package jiran.com.tmfilemanager.fagment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.InitActivity;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.activity.viewer.ImageSlideViewerActivity;
import jiran.com.tmfilemanager.adapter.FolderAdapter;
import jiran.com.tmfilemanager.common.ClipBoard;
import jiran.com.tmfilemanager.common.FolderSelectTotalDialog;
import jiran.com.tmfilemanager.common.ListFunctionUtils;
import jiran.com.tmfilemanager.common.MimeTypes;
import jiran.com.tmfilemanager.common.SortUtils;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.data.TagItem;
import jiran.com.tmfilemanager.layout.FlowLayout;
import jiran.com.tmfilemanager.network.updown.ListRefreshListener;
import jiran.com.tmfilemanager.service.FileObserverService;
import jiran.com.tmfilemanager.settings.MyFileSettings;

/**
 * Created by user on 2016-08-03.
 */


public class FolderFragment extends Fragment {

    //add Category Menu
    public static final int CATEGORY_APP = 0, CATEGORY_IMAGE = 1, CATEGORY_MOVIE = 2, CATEGORY_MUSIC = 3, CATEGORY_DOCUMENT = 4, CATEGORY_ZIP = 5;
    public static boolean mActionMode = false;
    public Context mMainContext;
    public int mListType = 0;
    public String mCurrentPath;
    public String mStoragePath;
    public String mStartPath;
    public TagItem mTagItem;
    public String mSearch;
    public ArrayList<FileItem> mSearchArray;
    public int CATEGORY_TYPE;
    Dialog mDialog;
    HashMap<String, Integer> listPositionMap;
    LinearLayout path_dimd_layout, list_dimd_layout;
    LinearLayout foler_path_category, frag_folder_category_menu;
    ImageView foler_path_image, foler_path_arrow;
    private AbsListView mListView;
    private GridView mGridView;
    private RecyclerView mRecyclerView;
    private LinearLayout mFoler_path_layout;
    //private TextView mFoler_path;
    private HorizontalScrollView foler_path_scroll_layout;
    private LinearLayout foler_path_category_dimd;
    private int mView_Type = 0;
    private FolderAdapter mFolderAdapter;
    ListRefreshListener listener = new ListRefreshListener() {
        @Override
        public void refresh(ArrayList<FileItem> items) {
            if (items == null) {
                finishActionMode();
            } else {
                for (FileItem item : items) {
                    mFolderAdapter.removeFiles(item);
                }
                mFolderAdapter.notifyDataSetChanged();
            }
        }
    };
    private Animation showAni, goneAni;

    public FolderFragment() {
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public FolderFragment(Context context, String path, boolean startBack) {
        mMainContext = context;
        mCurrentPath = path;
        if (startBack) {
            mStartPath = path;
        } else {
            mStartPath = null;
        }
        mView_Type = MainActivity.mViewType;
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public FolderFragment(Context context, String path, boolean startBack, int viewType) {
        mMainContext = context;
        mCurrentPath = path;
        if (startBack) {
            mStartPath = path;
        } else {
            mStartPath = null;
        }
        mView_Type = viewType;
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public FolderFragment(Context context, int type, TagItem item) {
        mMainContext = context;
        mListType = type;
        mTagItem = item;
        mView_Type = MainActivity.mViewType;
        setRetainInstance(true);
    }


    @SuppressLint("ValidFragment")
    public FolderFragment(Context context, String currentPath, String start, String search) {
        mMainContext = context;
        mStartPath = start;
        mCurrentPath = currentPath;
        mSearch = search;
        mView_Type = MainActivity.mViewType;
        setRetainInstance(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mMainContext == null) {
            Intent intent = new Intent(getActivity(), InitActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        }
        ((MainActivity) mMainContext).toolbar.setBackgroundResource(R.color.colorTitleBG);
        View rootView = inflater.inflate(R.layout.frag_folder, container, false);
        initLayout(inflater, rootView);
        return rootView;
    }

    private void setTitle(String title) {
        if (Utils.isSDcardDirectory(true) || (MultiFragment.teamItams != null && MultiFragment.teamItams.size() > 0)) {
            ((MainActivity) getActivity()).setTitle(title, true);
        } else {
            ((MainActivity) getActivity()).setTitle(title);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        ((MainActivity) mMainContext).finishSearchMode();
        ((MainActivity) mMainContext).toolbarMenu(R.menu.main);
        if (Utils.isSDcardDirectory(true) && mCurrentPath.contains(Utils.getSDcardDirectoryPath())) {
            setTitle(getResources().getString(R.string.sdcard));
        } else {
            if (mCurrentPath.equals(getResources().getString(R.string.category_document)) ||
                    mCurrentPath.equals(getResources().getString(R.string.category_recentfile)) ||
                    mCurrentPath.equals(getResources().getString(R.string.category_largefile)) ||
                    mCurrentPath.equals(getResources().getString(R.string.category_image)) ||
                    mCurrentPath.equals(getResources().getString(R.string.category_video)) ||
                    mCurrentPath.equals(getResources().getString(R.string.category_favorite)) ||
                    mCurrentPath.equals(getResources().getString(R.string.category_download)) ||
                    mCurrentPath.equals(getResources().getString(R.string.category_music))) {
                ((MainActivity) getActivity()).setTitle(mCurrentPath);
            } else {
                setTitle(getResources().getString(R.string.internaldirectory));
            }
        }
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        //getActivity().setTitle(getResources().getString(R.string.app_name));
//        File file = new File(mCurrentPath);
//        if(mListType != 2 && file != null && file.isDirectory()){
//            if(ClipBoard.isEmpty()){
//                list_bottom_menu_paste.setVisibility(View.GONE);
//            }else{
//                list_bottom_menu_paste.setVisibility(View.VISIBLE);
//            }
//        }else{
//            list_bottom_menu_paste.setVisibility(View.GONE);
//        }
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private boolean categoryMenu(boolean check) {
        finishActionMode();
        if (frag_folder_category_menu.getVisibility() == View.VISIBLE) {
            goneAni = AnimationUtils.loadAnimation(mMainContext, R.anim.main_top_info_slide_up);
            frag_folder_category_menu.setAnimation(goneAni);
            frag_folder_category_menu.setVisibility(View.GONE);
            foler_path_arrow.setImageResource(R.mipmap.list_folder_dropdown);
            path_dimd_layout.setVisibility(View.GONE);
            list_dimd_layout.setVisibility(View.GONE);
            return true;
        } else {
            if (!check) {
                showAni = AnimationUtils.loadAnimation(mMainContext, R.anim.main_top_info_slide_down);
                frag_folder_category_menu.setVisibility(View.VISIBLE);
                frag_folder_category_menu.setAnimation(showAni);
                foler_path_arrow.setImageResource(R.mipmap.list_folder_dropup);
                path_dimd_layout.setVisibility(View.VISIBLE);
                list_dimd_layout.setVisibility(View.VISIBLE);
            }
            return false;
        }
    }

    public boolean categoryTeamList(int type) {
        switch (type) {
            case CATEGORY_APP:
                //foler_path_image.setImageResource(R.mipmap.list_app);
                Toast.makeText(mMainContext, "TEAM APP 파일 리스트 개발중...", Toast.LENGTH_SHORT).show();
                break;
            case CATEGORY_IMAGE:
                //foler_path_image.setImageResource(R.mipmap.list_picture);
                Toast.makeText(mMainContext, "TEAM IMAGE 파일 리스트 개발중...", Toast.LENGTH_SHORT).show();
                break;
            case CATEGORY_MOVIE:
                //foler_path_image.setImageResource(R.mipmap.list_movie);
                Toast.makeText(mMainContext, "TEAM MOVIE 파일 리스트 개발중...", Toast.LENGTH_SHORT).show();
                break;
            case CATEGORY_MUSIC:
                //foler_path_image.setImageResource(R.mipmap.list_music);
                Toast.makeText(mMainContext, "TEAM MUSIC 파일 리스트 개발중...", Toast.LENGTH_SHORT).show();
                break;
            case CATEGORY_DOCUMENT:
                //foler_path_image.setImageResource(R.mipmap.list_document);
                Toast.makeText(mMainContext, "TEAM DOCUMENT 파일 리스트 개발중...", Toast.LENGTH_SHORT).show();
                break;
            case CATEGORY_ZIP:
                //foler_path_image.setImageResource(R.mipmap.list_zip);
                Toast.makeText(mMainContext, "TEAM ZIP 파일 리스트 개발중...", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }

        return true;
    }

    public void initLayout(LayoutInflater inflater, View rootView) {
        //final MainActivity context = (MainActivity) getActivity();
        path_dimd_layout = (LinearLayout) rootView.findViewById(R.id.path_dimd_layout);
        list_dimd_layout = (LinearLayout) rootView.findViewById(R.id.list_dimd_layout);
        foler_path_category = (LinearLayout) rootView.findViewById(R.id.foler_path_category);
        foler_path_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categoryMenu(false);
            }
        });
        frag_folder_category_menu = (LinearLayout) rootView.findViewById(R.id.frag_folder_category_menu);
        foler_path_image = (ImageView) rootView.findViewById(R.id.foler_path_image);
        foler_path_arrow = (ImageView) rootView.findViewById(R.id.foler_path_arrow);

        listPositionMap = new HashMap<String, Integer>();

        foler_path_scroll_layout = (HorizontalScrollView) rootView.findViewById(R.id.foler_path_scroll_layout);
        foler_path_category_dimd = (LinearLayout) rootView.findViewById(R.id.foler_path_category_dimd);
        foler_path_category_dimd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        mFoler_path_layout = (LinearLayout) rootView.findViewById(R.id.foler_path_layout);

        mListView = (ListView) rootView.findViewById(R.id.folder_listview);
        mListView.setEmptyView(rootView.findViewById(R.id.empty_layout));
        mGridView = (GridView) rootView.findViewById(R.id.folder_gridview);
        mGridView.setEmptyView(rootView.findViewById(R.id.empty_layout));

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.folder_recyclerview);

        if (mView_Type == 0) {
            changeListView(false);
        } else {
            changeGridView(false);
        }

        finishActionMode();
    }

    public void changeListGridView() {
        mFolderAdapter.listChange = true;
        if (mView_Type == 0) {
            MainActivity.mViewType = 1;
            listPositionMap.put(mCurrentPath, mListView.getFirstVisiblePosition());
            changeGridView(true);
        } else {
            MainActivity.mViewType = 0;
            listPositionMap.put(mCurrentPath, mGridView.getFirstVisiblePosition());
            changeListView(true);
        }
    }

    public void changeGridView() {
        if (mView_Type == 0) {
            mFolderAdapter.listChange = true;
            MainActivity.mViewType = 1;
            listPositionMap.put(mCurrentPath, mListView.getFirstVisiblePosition());
            changeGridView(true);
        }
    }

    public void changeListView() {
        if (mView_Type == 1) {
            mFolderAdapter.listChange = true;
            MainActivity.mViewType = 0;
            listPositionMap.put(mCurrentPath, mGridView.getFirstVisiblePosition());
            changeListView(true);
        }
    }

    private void changeGridView(boolean listChange) {
        //final MainActivity context = (MainActivity) getActivity();
        ((MainActivity) getActivity()).setSelectedTitle("");
        mView_Type = 1;
        changeActionModeList();
        if (listChange) {
            mFolderAdapter = new FolderAdapter(getActivity(), null, mView_Type, mFolderAdapter.getList());
        } else {
            mFolderAdapter = new FolderAdapter(getActivity(), null, mView_Type);
        }
        mGridView.setVisibility(View.VISIBLE);
        mListView.setVisibility(View.GONE);
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mActionMode) {
                    mFolderAdapter.setCheck(position);
                    mFolderAdapter.notifyDataSetChanged();
                    toolbarMenuIconChange();
                    int count = mFolderAdapter.getCheckCount();
                    ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
                } else {
                    final File file = new File(((FileItem) mGridView.getAdapter()
                            .getItem(position)).getPath());

                    if (file.isDirectory()) {
                        mSearch = null;
                        if ((mListType == 1 || mListType == 2) && mStartPath == null) {
                            mStartPath = file.getAbsolutePath();
                        }
                        //Scroll Position Set
                        if (mListType == 1) {
                            listPositionMap.put(mTagItem.getName(), mGridView.getFirstVisiblePosition());
                        } else if (mListType == 2) {
                            listPositionMap.put(getResources().getString(R.string.category_favorite), mGridView.getFirstVisiblePosition());
                        } else {
                            listPositionMap.put(mCurrentPath, mGridView.getFirstVisiblePosition());
                        }
                        folderListDisplay(file.getAbsolutePath());
                        // go to the top of the ListView
                        //mGridView.setSelection(0);
                    } else {
                        //if ( MimeTypes.isPicture(file)) {
                        if (MimeTypes.isPicture(file) || MimeTypes.isPdf(file) || MimeTypes.isVideo(file)) {
                            Intent intent = new Intent(mMainContext, ImageSlideViewerActivity.class);
                            ImageSlideViewerActivity.mDataSource = mFolderAdapter.getImageList();
                            ImageSlideViewerActivity.mPosition = 0;
                            for (FileItem item : ImageSlideViewerActivity.mDataSource) {
                                if (item.getPath().equals(file.getPath())) {
                                    break;
                                } else {
                                    ImageSlideViewerActivity.mPosition++;
                                }
                            }
                            ((MainActivity) mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_SLIDE_VIEWER);
                        } else {
                            Utils.openFile(mMainContext, file);
                        }
                    }
                }
            }
        });

        mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                mFolderAdapter.setCheck(position);
                int count = mFolderAdapter.getCheckCount();
                ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
                setActionMode();
                toolbarMenuIconChange();
                return true;
            }
        });
        mGridView.setAdapter(mFolderAdapter);

        if (mSearch != null && mSearch.length() > 0) {
            folderSearchListDisplay(mCurrentPath, mSearch);
        } else if (mListType == 1) {
            folderTagListDisplay(mTagItem);
        } else if (mListType == 2) {
            folderFavoriteListDisplay(getResources().getString(R.string.category_favorite));
        } else {
            folderListDisplay(mCurrentPath);
        }
//        mFolderAdapter.addFiles(mCurrentPath);
//        mFolderAdapter.notifyDataSetChanged();
    }

    private void changeListView(boolean listChange) {
        //final MainActivity context = (MainActivity) getActivity();
        ((MainActivity) getActivity()).setSelectedTitle("");
        mView_Type = 0;
        changeActionModeList();
        if (listChange) {
            mFolderAdapter = new FolderAdapter(getActivity(), null, mView_Type, mFolderAdapter.getList());
        } else {
            mFolderAdapter = new FolderAdapter(getActivity(), null, mView_Type);
        }
        mListView.setVisibility(View.VISIBLE);
        mGridView.setVisibility(View.GONE);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mActionMode) {
                    mFolderAdapter.setCheck(position);
                    mFolderAdapter.notifyDataSetChanged();
                    toolbarMenuIconChange();
                    int count = mFolderAdapter.getCheckCount();
                    ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
                } else {
                    final File file = new File(((FileItem) mListView.getAdapter()
                            .getItem(position)).getPath());

                    if (file.isDirectory()) {
                        mSearch = null;
                        if ((mListType == 1 || mListType == 2) && mStartPath == null) {
                            mStartPath = file.getAbsolutePath();
                        }
                        //Scroll Position Set
                        if (mListType == 1) {
                            listPositionMap.put(mTagItem.getName(), mListView.getFirstVisiblePosition());
                        } else if (mListType == 2) {
                            listPositionMap.put(getResources().getString(R.string.category_favorite), mListView.getFirstVisiblePosition());
                        } else {
                            listPositionMap.put(mCurrentPath, mListView.getFirstVisiblePosition());
                        }
                        folderListDisplay(file.getAbsolutePath());
                        // go to the top of the ListView
                        //mListView.setSelection(0);
                    } else {
                        //if (MimeTypes.isPicture(file)) {
                        if (MimeTypes.isPicture(file) || MimeTypes.isPdf(file) || MimeTypes.isVideo(file)) {
                            Intent intent = new Intent(mMainContext, ImageSlideViewerActivity.class);
                            ImageSlideViewerActivity.mDataSource = mFolderAdapter.getImageList();
                            ImageSlideViewerActivity.mPosition = 0;
                            for (FileItem item : ImageSlideViewerActivity.mDataSource) {
                                if (item.getPath().equals(file.getPath())) {
                                    break;
                                } else {
                                    ImageSlideViewerActivity.mPosition++;
                                }
                            }
                            ((MainActivity) mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_SLIDE_VIEWER);
                        } else {
                            Utils.openFile(mMainContext, file);
                        }
                    }
                }
            }
        });

        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                mFolderAdapter.setCheck(position);
                int count = mFolderAdapter.getCheckCount();
                ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
                setActionMode();
                toolbarMenuIconChange();
                return true;
            }
        });
        mListView.setAdapter(mFolderAdapter);
        if (mSearch != null && mSearch.length() > 0) {
            folderSearchListDisplay(mCurrentPath, mSearch);
        } else if (mListType == 1) {
            folderTagListDisplay(mTagItem);
        } else if (mListType == 2) {
            folderFavoriteListDisplay(getResources().getString(R.string.category_favorite));
        } else {
            folderListDisplay(mCurrentPath);
        }
//        mFolderAdapter.addFiles(mCurrentPath);
//        mFolderAdapter.notifyDataSetChanged();
    }

    public void setScrollPosition(String path) {
        //Scroll Position Set
        int position;
        if (listPositionMap.containsKey(path)) {
            position = listPositionMap.get(path);
            listPositionMap.remove(path);
        } else {
            position = 0;
        }

        if (mView_Type == 0) {
            mListView.setSelection(position);
        } else {
            mGridView.setSelection(position);
        }
    }

    public void folderListDisplay(String path) {
        mCurrentPath = path;
        //mFoler_path.setText(mCurrentPath);
        setFolderFathLayout(mCurrentPath);
        foler_path_scroll_layout.post(new Runnable() {
            public void run() {
                foler_path_scroll_layout.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        });

        if (mCurrentPath.equals(getResources().getString(R.string.category_document)) ||
                mCurrentPath.equals(getResources().getString(R.string.category_app)) ||
                mCurrentPath.equals(getResources().getString(R.string.category_zip)) ||
                mCurrentPath.equals(getResources().getString(R.string.category_recentfile)) ||
                mCurrentPath.equals(getResources().getString(R.string.category_largefile))) {
            if (mFolderAdapter.listChange) {
                mFolderAdapter.listChange = false;
                mFolderAdapter.notifyDataSetChanged();
            } else {
                if (mCurrentPath.equals(getResources().getString(R.string.category_document)) && ListFunctionUtils.mTempDocuContent.size() > 0) {
                    SortUtils.sortList(ListFunctionUtils.mTempDocuContent, mCurrentPath, MyFileSettings.getSortType());
                    mFolderAdapter.addContent(ListFunctionUtils.mTempDocuContent);
                    mFolderAdapter.notifyDataSetChanged();
                } else if (mCurrentPath.equals(getResources().getString(R.string.category_app)) && ListFunctionUtils.mTempAppContent.size() > 0) {
                    SortUtils.sortList(ListFunctionUtils.mTempAppContent, mCurrentPath, MyFileSettings.getSortType());
                    mFolderAdapter.addContent(ListFunctionUtils.mTempAppContent);
                    mFolderAdapter.notifyDataSetChanged();
                } else if (mCurrentPath.equals(getResources().getString(R.string.category_zip)) && ListFunctionUtils.mTempZipContent.size() > 0) {
                    SortUtils.sortList(ListFunctionUtils.mTempZipContent, mCurrentPath, MyFileSettings.getSortType());
                    mFolderAdapter.addContent(ListFunctionUtils.mTempZipContent);
                    mFolderAdapter.notifyDataSetChanged();
                } else if (mCurrentPath.equals(getResources().getString(R.string.category_recentfile)) && ListFunctionUtils.mTempRecentContent.size() > 0) {
                    SortUtils.sortList(ListFunctionUtils.mTempRecentContent, mCurrentPath, MyFileSettings.getSortType());
                    ArrayList<FileItem> temp = new ArrayList<>();
                    temp = ListFunctionUtils.mTempRecentContent;
                    mFolderAdapter.addContent(temp);
                    mFolderAdapter.notifyDataSetChanged();
                } else if (mCurrentPath.equals(getResources().getString(R.string.category_largefile)) && ListFunctionUtils.mTempHugeContent.size() > 0) {
                    SortUtils.sortList(ListFunctionUtils.mTempHugeContent, mCurrentPath, MyFileSettings.getSortType());
                    mFolderAdapter.addContent(ListFunctionUtils.mTempHugeContent);
                    mFolderAdapter.notifyDataSetChanged();
                } else {
                    LoadingTask mTask = new LoadingTask(getActivity(), mCurrentPath);
                    mTask.execute("");
                }
            }
        } else {
            mFolderAdapter.addFiles(path, 0);
        }
        //Scroll Position Set
        setScrollPosition(path);
    }

    public void folderTagListDisplay(TagItem tag) {
        //mFoler_path.setText(tag.getName());
        setFolderFathLayout(tag.getName());
        mCurrentPath = "/";
        foler_path_scroll_layout.post(new Runnable() {
            public void run() {
                foler_path_scroll_layout.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        });
        mFolderAdapter.addTagFiles(tag);

        //Scroll Position Set
        setScrollPosition(tag.getName());
    }

    public void folderSearchListDisplay(String currentPath, String search) {
        //mFoler_path.setText(title);
        setFolderFathLayout(search, true);
        foler_path_scroll_layout.post(new Runnable() {
            public void run() {
                foler_path_scroll_layout.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        });
        if (mSearchArray != null) {
            mFolderAdapter.addContent(mSearchArray);
            mFolderAdapter.notifyDataSetChanged();
        } else {
            SearchTask mTask = new SearchTask(getActivity(), currentPath);
            mTask.execute(search);
        }
    }

    public void folderFavoriteListDisplay(String title) {
        //mFoler_path.setText(title);
        setFolderFathLayout(title);
        mCurrentPath = title;//"/";
        foler_path_scroll_layout.post(new Runnable() {
            public void run() {
                foler_path_scroll_layout.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        });
        mFolderAdapter.addFavoriteFiles();

        //Scroll Position Set
        setScrollPosition(title);
    }

    public void setFolderFathLayout(String path) {
        setFolderFathLayout(path, false);
    }

    public void setFolderFathLayout(String path, boolean search) {
        mFoler_path_layout.removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        //Home Image add
        String HHOOMMEEPATH = "";
        boolean internal = true;
        if (path.contains(Utils.getInternalDirectoryPath())) {
            internal = true;
            String[] lastPath = Utils.getInternalDirectoryPath().split("/");
            path = File.separator + "HHOOMMEE" + File.separator + lastPath[lastPath.length - 1] + path.replaceFirst(Utils.getInternalDirectoryPath(), "");
            HHOOMMEEPATH = Utils.getInternalDirectoryPath().replaceFirst(File.separator + lastPath[lastPath.length - 1], "");
        } else if (Utils.isSDcardDirectory(true) && path.contains(Utils.getSDcardDirectoryPath())) {
            internal = false;
            String[] lastPath = Utils.getSDcardDirectoryPath().split("/");
            path = File.separator + "HHOOMMEE" + File.separator + lastPath[lastPath.length - 1] + path.replaceFirst(Utils.getSDcardDirectoryPath(), "");
            HHOOMMEEPATH = Utils.getSDcardDirectoryPath().replaceFirst(File.separator + lastPath[lastPath.length - 1], "");
        }
        String[] arrayPath = path.split("/");
        if (path.contains("/") && arrayPath.length > 1) {

            String makePath = "";
            int homeIndex = -1;
            for (int i = 1; i < arrayPath.length; i++) {
                String sPath = arrayPath[i];
                if (sPath.equals("HHOOMMEE")) {
                    homeIndex = i;
                    View home_view = inflater.inflate(R.layout.frag_folder_path_item_home, null, false);
                    makePath += HHOOMMEEPATH;
                    home_view.setTag(makePath);
                    home_view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((MainActivity) mMainContext).initFragment();
                        }
                    });
                    mFoler_path_layout.addView(home_view);
                } else {
                    View path_view = inflater.inflate(R.layout.frag_folder_path_item, null, false);
                    TextView foler_path = (TextView) path_view.findViewById(R.id.foler_path);
                    if (homeIndex == (i - 1)) {
                        if (internal) {
                            //foler_path.setText("" + getResources().getString(R.string.internaldirectory));
                            foler_path.setBackgroundResource(R.mipmap.list_folder);
                        } else {
                            //foler_path.setText("" + getResources().getString(R.string.sdcard));
                            foler_path.setBackgroundResource(R.mipmap.list_folder);
                        }
                    } else {
                        foler_path.setText(" > " + sPath);
                    }
                    makePath += ("/" + sPath);
                    path_view.setTag(makePath);
                    path_view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            folderListDisplay((String) v.getTag());
                        }
                    });
                    mFoler_path_layout.addView(path_view);
                }
            }
        } else {
            View path_view = inflater.inflate(R.layout.frag_folder_path_item, null, false);
            TextView foler_path = (TextView) path_view.findViewById(R.id.foler_path);
            if (search) {
                foler_path.setText("\"" + path + "\" " + getResources().getString(R.string.action_search_result));
            } else {
                if (path.equals(getResources().getString(R.string.category_document)) ||
                        path.equals(getResources().getString(R.string.category_recentfile)) ||
                        path.equals(getResources().getString(R.string.category_largefile)) ||
                        path.equals(getResources().getString(R.string.category_image)) ||
                        path.equals(getResources().getString(R.string.category_video)) ||
                        path.equals(getResources().getString(R.string.category_favorite)) ||
                        path.equals(getResources().getString(R.string.category_download)) ||
                        path.equals(getResources().getString(R.string.category_music))) {
                    //foler_path.setText(File.separator + path);
                    foler_path.setText(path);
                    View home_view = inflater.inflate(R.layout.frag_folder_path_item_home, null, false);
                    home_view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ((MainActivity) mMainContext).initFragment();
                        }
                    });
                    mFoler_path_layout.addView(home_view);
                } else {
                    foler_path.setText(path);
                }
            }
            mFoler_path_layout.addView(path_view);
        }
    }

    public void toolbarMenuIconChange() {
        ArrayList<FileItem> items = mFolderAdapter.getCheckList();
        //getActivity().setTitle(items.size() + " " +  getResources().getString(R.string.selected));
        boolean allfiles = true;
        boolean allfavorite = true;
        if (items != null && items.size() > 0) {
            if (items.size() == 1) {
                File file = new File(items.get(0).getPath());
                if (file.isFile()) {
                    ((MainActivity) mMainContext).item_menu_another_app.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_rename.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_send_member.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_secretfolder.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_favorite.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_tag.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_e_album.setVisibility(View.VISIBLE);
                } else {
                    ((MainActivity) mMainContext).item_menu_another_app.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_rename.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_send_member.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_secretfolder.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_favorite.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_tag.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_e_album.setVisibility(View.GONE);
                }
            } else {
                for (FileItem item : items) {
                    if (allfiles) {
                        File file = new File(item.getPath());
                        if (file.isDirectory()) {
                            allfiles = false;
                        }
                    }
                    if (allfavorite) {
                        if (!item.getFavorite()) {
                            allfavorite = false;
                        }
                    }
                }
//            if(allhide){
//                ((MainActivity)mMainContext).toolbar.getMenu().getItem(0).setIcon(R.mipmap.ic_unhide);
//            }else{
//                ((MainActivity)mMainContext).toolbar.getMenu().getItem(0).setIcon(R.mipmap.ic_hide);
//            }


                if (allfiles) {
                    ((MainActivity) mMainContext).item_menu_another_app.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_rename.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_send_member.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_secretfolder.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_favorite.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_tag.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_e_album.setVisibility(View.GONE);
                } else {
                    ((MainActivity) mMainContext).item_menu_another_app.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_rename.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_send_member.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_secretfolder.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_favorite.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_tag.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_e_album.setVisibility(View.GONE);
                }
                if (allfavorite) {
                    ((MainActivity) mMainContext).toolbar.getMenu().getItem(5).setIcon(R.mipmap.ic_unfavorite);
                } else {
                    ((MainActivity) mMainContext).toolbar.getMenu().getItem(5).setIcon(R.mipmap.ic_favorite);
                }
            }
        } else {
            ((MainActivity) mMainContext).toolbar.getMenu().getItem(0).setVisible(false);
            ((MainActivity) mMainContext).toolbar.getMenu().getItem(1).setVisible(false);
            ((MainActivity) mMainContext).toolbar.getMenu().getItem(5).setIcon(R.mipmap.ic_favorite);
        }
    }

    public void setActionMode() {
        mActionMode = true;
        //getActivity().setTitle("0 " +  getResources().getString(R.string.selected));
        ((MainActivity) mMainContext).toolbar.getMenu().clear();
        ((MainActivity) mMainContext).toolbar.inflateMenu(R.menu.action_mode);
        ((MainActivity) mMainContext).finishSearchMode();
        ((MainActivity) mMainContext).multiSelectMode();
        foler_path_category_dimd.setVisibility(View.VISIBLE);
        mFolderAdapter.notifyDataSetChanged();
    }

    public void finishActionMode() {
        //getActivity().setTitle(getResources().getString(R.string.app_name));
        mActionMode = false;
        changeActionModeList();
//        if(ClipBoard.isEmpty()){
//            list_bottom_menu_paste.setVisibility(View.GONE);
//        }else{
//            list_bottom_menu_paste.setVisibility(View.VISIBLE);
//        }
        ((MainActivity) mMainContext).finishMultiSelectMode();
        foler_path_category_dimd.setVisibility(View.GONE);
        mFolderAdapter.setCheckClear();
        mFolderAdapter.notifyDataSetChanged();
    }

    public void listAllcheck() {
//        mFolderAdapter.setAllCheck();
//        mFolderAdapter.notifyDataSetChanged();
        if (mFolderAdapter.isAllChecked()) {
            mFolderAdapter.setCheckClear();
            mFolderAdapter.notifyDataSetChanged();
        } else {
            mFolderAdapter.setAllCheck();
            mFolderAdapter.notifyDataSetChanged();
        }
        int count = mFolderAdapter.getCheckCount();
        ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
    }

    public void listCheckClear() {
        mFolderAdapter.setCheckClear();
        mFolderAdapter.notifyDataSetChanged();
    }

    public void changeActionModeList() {
        if (mMainContext != null && ((MainActivity) mMainContext).toolbar != null) {
            ((MainActivity) mMainContext).toolbar.getMenu().clear();
            ((MainActivity) mMainContext).toolbar.inflateMenu(R.menu.main);
        }
    }

    public boolean onBackPressed() {

        if (mActionMode) {
            finishActionMode();
            return true;
        } else if (categoryMenu(true)) {
            return true;
        } else if (mCurrentPath != null && mCurrentPath.equals("/")) {
            //getActivity().finish();
            ((MainActivity) mMainContext).initFragment();
            return false;
        } else {
            try {
                if (mSearch != null && mSearch.length() > 0) {
                    mSearch = null;
                    if (mCurrentPath.equals(Utils.getInternalDirectoryPath())) {
                        ((MainActivity) mMainContext).initFragment();
                    } else {
                        folderListDisplay(mCurrentPath);
                    }
                } else {
                    if (mStartPath != null && mStartPath.equals(mCurrentPath)) {
                        if (mListType == 1) {
                            folderTagListDisplay(mTagItem);
                        } else if (mListType == 2) {
                            folderFavoriteListDisplay(getResources().getString(R.string.category_favorite));
                        } else {
                            ((MainActivity) mMainContext).initFragment();
                        }
                    } else {
                        if (mCurrentPath.equals(Utils.getInternalDirectoryPath())) {
                            ((MainActivity) mMainContext).initFragment();
                        } else {
                            File file = new File(mCurrentPath);
                            folderListDisplay(file.getParent());
                        }
                    }
                }
            } catch (Exception e) {
                //getActivity().finish();
                ((MainActivity) mMainContext).initFragment();
                return false;
            }
            // get position of the previous folder in ListView
            //mListView.setSelection(mListAdapter.getPosition(file.getPath()));
            return true;
        }
    }

    public void shareMethod() {

        int check_count = mFolderAdapter.getCheckCount();
        ArrayList<FileItem> items = mFolderAdapter.getCheckList();
        if (items != null && items.size() > 0) {
            Utils.sendFiles(mMainContext, items);
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        finishActionMode();
    }

    public void secretMethod() {
        final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
        final int check_count = mFolderAdapter.getCheckCount();
        final ArrayList<FileItem> items = mFolderAdapter.getCheckList();
        if (items != null && items.size() > 0) {
            mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
            LayoutInflater inflater = LayoutInflater.from(mMainContext);
            View view = inflater.inflate(R.layout.dialog_secret_copy_move, null, false);
            TextView title = (TextView) view.findViewById(R.id.title);
            title.setText(getResources().getString(R.string.left_menu_secretfolder));
//            TextView content = (TextView) view.findViewById(R.id.content);
//            content.setText(getResources().getString(R.string.secretfolder_do_you_want_save_select_file_in_secretfolder_1) + check_count
//                    + getResources().getString(R.string.secretfolder_do_you_want_save_select_file_in_secretfolder_2));
            final CheckBox secret_copy = (CheckBox) view.findViewById(R.id.secret_copy);
            final CheckBox secret_move = (CheckBox) view.findViewById(R.id.secret_move);
            secret_copy.setEnabled(false);
            secret_copy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        secret_copy.setEnabled(false);
                        secret_move.setEnabled(true);
                        secret_move.setChecked(false);
                    } else {

                    }
                }
            });
            secret_move.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        secret_copy.setEnabled(true);
                        secret_move.setEnabled(false);
                        secret_copy.setChecked(false);
                    } else {

                    }
                }
            });
            TextView cancel = (TextView) view.findViewById(R.id.cancel);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                }
            });
            TextView ok = (TextView) view.findViewById(R.id.ok);
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (FileItem item : items) {
                        File from = new File(item.getPath());
//                        File directory = from.getParentFile();
//                        File to = new File(directory, item.getName().replaceFirst(".", ""));
//                        from.renameTo(to);
//                        item.setPath(directory.getPath() + File.separator + item.getName().replaceFirst(".", ""));
//                        item.setName(item.getName().replaceFirst(".", ""));
                        // 시스템으로부터 현재시간(ms) 가져오기
                        long now = System.currentTimeMillis();
                        if (secret_move.isChecked()) {
                            Utils.moveToSecretDirectory(from, "." + Utils.stripFilenameExtension(from.getName()), mMainContext);
                        } else {
                            Utils.moveToSecretDirectory(from, "." + Utils.stripFilenameExtension(from.getName()), mMainContext, false);
                        }

                    }
                    //Toast.makeText(mMainContext, check_count + getResources().getString(R.string.secretfolder_move_to_secretfolder), Toast.LENGTH_SHORT).show();
                    Toast.makeText(mMainContext, getResources().getString(R.string.secretfolder_move_to_secretfolder), Toast.LENGTH_SHORT).show();
                    listRefresh();
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                }
            });
            mDialog.setContentView(view);
            mDialog.show();
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        finishActionMode();
    }

    public void hideMethod() {
        int check_count = mFolderAdapter.getCheckCount();
        ArrayList<FileItem> items = mFolderAdapter.getCheckList();
        if (items != null && items.size() > 0) {
            boolean allHide = true;
            for (FileItem item : items) {
                if (!item.getName().startsWith(".")) {
                    allHide = false;
                    break;
                }
            }
            for (FileItem item : items) {
                if (allHide) {
                    File from = new File(item.getPath());
                    File directory = from.getParentFile();
                    File to = new File(directory, item.getName().replaceFirst(".", ""));
                    from.renameTo(to);
                    item.setPath(directory.getPath() + File.separator + item.getName().replaceFirst(".", ""));
                    item.setName(item.getName().replaceFirst(".", ""));
                } else {
                    if (!item.getName().startsWith(".")) {
                        File from = new File(item.getPath());
                        File directory = from.getParentFile();
                        File to = new File(directory, "." + item.getName());
                        from.renameTo(to);
                        item.setPath(directory.getPath() + File.separator + "." + item.getName());
                        item.setName("." + item.getName());
                    }
                }
            }
            if (allHide) {
                Toast.makeText(mMainContext, check_count + getResources().getString(R.string.snackbar_file_unhide), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mMainContext, check_count + getResources().getString(R.string.snackbar_file_hide), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        finishActionMode();
    }

    public void copyMethod(final boolean oriRemove) {
        final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
        int count = mFolderAdapter.getCheckCount();

        if (count > 0) {
            ArrayList<FileItem> items = mFolderAdapter.getCheckList();
            boolean checkFolder = false;
            for (FileItem item : items) {
                File file = new File(item.getPath());
                if (file.isDirectory()) {
                    checkFolder = true;
                }
            }
            if (oriRemove) {
                ClipBoard.cutMove(items);
//                Snackbar.make(viewPos, getResources().getString(R.string.snackbar_move), Snackbar.LENGTH_SHORT)
//                        .setAction("Action", null).show();
            } else {
                ClipBoard.cutCopy(items);
//                Snackbar.make(viewPos, getResources().getString(R.string.snackbar_copy), Snackbar.LENGTH_SHORT)
//                        .setAction("Action", null).show();
            }
            FolderSelectTotalDialog.ORI_FILE_TYPE = FolderSelectTotalDialog.LOCAL_MEMORY;
            FolderSelectTotalDialog selectDialog = new FolderSelectTotalDialog(getActivity(), R.style.Theme_TransparentBackground, getActivity(), !oriRemove, checkFolder, listener);
            selectDialog.show();
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        //finishActionMode();
    }

    public void favoriteMethod() {

        final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
        int check_count = mFolderAdapter.getCheckCount();
        ArrayList<FileItem> items = mFolderAdapter.getCheckList();
        if (items != null && items.size() > 0) {
            boolean allFavorite = true;
            for (FileItem item : items) {
                if (!item.getFavorite()) {
                    allFavorite = false;
                    break;
                }
            }
            for (FileItem item : items) {
                if (allFavorite) {
//                    if(item.getTagId() != null && item.getTagId().length() > 0){
//                        MainActivity.mMds.updateFileFavorite(item.getId(), "N");
//                    }else
                    {
                        MainActivity.mMds.deleteTag(item.getId());
                        mFolderAdapter.removeFavoriteFiles(item);
                    }
                    item.setId("");
                } else {
//                    if(item.getFavorite() != null && item.getFavorite().length() == 0){
//                        MainActivity.mMds.updateFileFavorite(item.getId(), "Y");
//                    }else{
//                        MainActivity.mMds.insertFavoriteItem(item.getPath());
//                    }
                    item.setId(MainActivity.mMds.insertFavoriteItem(item.getPath()) + "");
                    item.setFavorite(true);
                }
            }

            if (allFavorite) {
                Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_file_unfavorite), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_file_favorite), Toast.LENGTH_SHORT).show();
            }
            mFolderAdapter.refresh(mCurrentPath);
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        finishActionMode();
    }

    public void tagMethod(TagItem tag) {
        ArrayList<FileItem> items = mFolderAdapter.getCheckList();
        if (items != null && items.size() > 0) {
            for (FileItem item : items) {
                if (tag == null) {
                    MainActivity.mMds.deleteTag(item.getId());
                    item.setId("");
                } else {
                    item.setId(MainActivity.mMds.insertTagItem(tag.getName(), item.getPath()) + "");
                }
            }
//            Snackbar.make(viewPos, "태그", Snackbar.LENGTH_SHORT)
//                    .setAction("Action", null).show();
            mFolderAdapter.refresh(mCurrentPath);
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        finishActionMode();
    }

    public void deleteMethod() {
        int check_count = mFolderAdapter.getCheckCount();

        if (check_count > 0) {
            dialogDeleteFile(check_count);
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
            //finishActionMode();
        }
    }

    public void renameMethod() {
        int check_count = mFolderAdapter.getCheckCount();
        if (check_count > 0) {
            dialogRename();
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
            //finishActionMode();
        }
    }

    public void dialogRename() {
        ArrayList<FileItem> items = mFolderAdapter.getCheckList();
        final FileItem item = items.get(0);
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        final View view = inflater.inflate(R.layout.dialog_new_folder, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.dialog_rename_file));
        final EditText edt = (EditText) view.findViewById(R.id.edt);

        edt.setText(Utils.stripFilenameExtension(item.getName()));
        edt.requestFocus();
        InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView create = (TextView) view.findViewById(R.id.create);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String folder_name = edt.getText().toString();
                if (folder_name != null && folder_name.length() > 0) {
                    File select = new File(item.getPath());
//                    File to = new File(item.getPath());
//                    select.renameTo(to);

                    if (select.isDirectory()) {
                        //renameStorageFolder(item, folder_name);

                    } else {
                        //renameStorageFile(item, folder_name);
                        folder_name = folder_name + "." + Utils.getFilenameExtension(item.getName());
                    }

                    File to = new File(select.getParent(), folder_name);
                    if (select.renameTo(to)) {
                        item.setPath(to.getAbsolutePath());
                        item.setName(to.getName());
                        mFolderAdapter.notifyDataSetChanged();
                    }

                    InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                } else {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_input_foldername), Toast.LENGTH_SHORT).show();
                }

            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    //    public void sendFileIDMethod() {
//        ArrayList<FileItem> tempList = mFolderAdapter.getCheckList();
//
//        if(tempList.size() > 0){
//            boolean isDirectory = false;
//            for(FileItem item : tempList){
//                File file = new File(item.getPath());
//                if(file.isDirectory()){
//                    isDirectory = true;
//                    break;
//                }
//            }
//            if(isDirectory){
//                View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
//                Snackbar.make(viewPos, getResources().getString(R.string.Dialog_Fileview_NoFolderRequest), Snackbar.LENGTH_SHORT)
//                        .setAction("Action", null).show();
//            }else{
//                FileIDActivity.selectSendFileIDList = mFolderAdapter.getCheckList();
//                Intent intent = new Intent(mMainContext, FileIDActivity.class);
//                ((MainActivity)mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_FILE_ID_SEND);
//            }
//        }else{
//            View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
//            Snackbar.make(viewPos, getResources().getString(R.string.snackbar_no_select), Snackbar.LENGTH_SHORT)
//                    .setAction("Action", null).show();
//        }
//    }
    public void sortDate() {
        if (MyFileSettings.getSortType() == SortUtils.SORT_DATE) {
            MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
        } else {
            MyFileSettings.setSortType(SortUtils.SORT_DATE);
        }
        mFolderAdapter.reSort();
    }

    public void sortName() {
        if (MyFileSettings.getSortType() == SortUtils.SORT_ALPHA) {
            MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
        } else {
            MyFileSettings.setSortType(SortUtils.SORT_ALPHA);
        }
        mFolderAdapter.reSort();
    }

    public void sortType() {
        if (MyFileSettings.getSortType() == SortUtils.SORT_TYPE) {
            MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
        } else {
            MyFileSettings.setSortType(SortUtils.SORT_TYPE);
        }
        mFolderAdapter.reSort();
    }

    public void sortSize() {
        if (MyFileSettings.getSortType() == SortUtils.SORT_SIZE) {
            MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
        } else {
            MyFileSettings.setSortType(SortUtils.SORT_SIZE);
        }
        mFolderAdapter.reSort();
    }

    public void dialogSetSort() {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.dialog_select_sort, null, false);
        LinearLayout sort_by_name = (LinearLayout) view.findViewById(R.id.sort_by_name);
        TextView sort_by_name_txt = (TextView) view.findViewById(R.id.sort_by_name_txt);
        if (MyFileSettings.getSortType() != SortUtils.SORT_ALPHA) {
            sort_by_name_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        } else {
            if (!MyFileSettings.getSortReverse()) {
                sort_by_name_txt.setText(sort_by_name_txt.getText().toString() + " ↑");
            } else {
                sort_by_name_txt.setText(sort_by_name_txt.getText().toString() + " ↓");
            }
        }
        sort_by_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                if (MyFileSettings.getSortType() == SortUtils.SORT_ALPHA) {
                    MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
                } else {
                    MyFileSettings.setSortType(SortUtils.SORT_ALPHA);
                }

                mFolderAdapter.reSort();
            }
        });
        LinearLayout sort_by_extension = (LinearLayout) view.findViewById(R.id.sort_by_extension);
        TextView sort_by_extension_txt = (TextView) view.findViewById(R.id.sort_by_extension_txt);
        if (MyFileSettings.getSortType() != SortUtils.SORT_TYPE) {
            sort_by_extension_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        } else {
            if (!MyFileSettings.getSortReverse()) {
                sort_by_extension_txt.setText(sort_by_extension_txt.getText().toString() + " ↑");
            } else {
                sort_by_extension_txt.setText(sort_by_extension_txt.getText().toString() + " ↓");
            }
        }
        sort_by_extension.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                if (MyFileSettings.getSortType() == SortUtils.SORT_TYPE) {
                    MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
                } else {
                    MyFileSettings.setSortType(SortUtils.SORT_TYPE);
                }
                mFolderAdapter.reSort();
            }
        });
        LinearLayout sort_by_date = (LinearLayout) view.findViewById(R.id.sort_by_date);
        TextView sort_by_date_txt = (TextView) view.findViewById(R.id.sort_by_date_txt);
        if (MyFileSettings.getSortType() != SortUtils.SORT_DATE) {
            sort_by_date_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        } else {
            if (!MyFileSettings.getSortReverse()) {
                sort_by_date_txt.setText(sort_by_date_txt.getText().toString() + " ↑");
            } else {
                sort_by_date_txt.setText(sort_by_date_txt.getText().toString() + " ↓");
            }
        }
        sort_by_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                if (MyFileSettings.getSortType() == SortUtils.SORT_DATE) {
                    MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
                } else {
                    MyFileSettings.setSortType(SortUtils.SORT_DATE);
                }
                mFolderAdapter.reSort();
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void dialogDeleteFile(final int check_count) {
        final Dialog dialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.dialog_base, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.dialog_delete_file));
        TextView content = (TextView) view.findViewById(R.id.content);
        content.setText(getResources().getString(R.string.dialog_delete_file_info1) + check_count + getResources().getString(R.string.dialog_delete_file_info2));
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<FileItem> items = mFolderAdapter.getCheckList();

                boolean success = true;

                for (FileItem item : items) {
                    File select = new File(item.getPath());
                    if (select != null && select.exists()) {
                        if (select.isDirectory()) {
                            success = deleteFile(select);
                            if (!success) {
                                break;
                            }
                            select.delete();
                        } else {
                            if (select.delete()) {
//                                MainActivity.mMds.deleteTagPath(item.getPath());
//                                MainActivity.mMds.deleteFavorite(item.getFavoriteId());
                                MainActivity.mMds.deleteFile(item.getPath());
                            } else {
                                success = false;
                                break;
                            }
                        }
                    }
                }
                if (success) {
                    Toast.makeText(mMainContext, check_count + getResources().getString(R.string.snackbar_file_delete), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_delete_error), Toast.LENGTH_SHORT).show();
                }

                for (FileItem item : items) {
                    mFolderAdapter.removeFiles(item);
                }
                //mFolderAdapter.refresh(mCurrentPath);
                finishActionMode();

                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        dialog.setContentView(view);
        dialog.show();
    }

    public boolean deleteFile(File file) {
        File[] fileList = file.listFiles();
        boolean success = true;
        for (File temp : fileList) {
            if (temp.isDirectory()) {
                success = deleteFile(temp);
                if (!success) {
                    break;
                }
                temp.delete();
            } else {
                if (temp.delete()) {
//                    ArrayList<FileItem> tempList = MainActivity.mMds.selectTagFiles(temp.getPath());
//                    for(FileItem sub : tempList){
//                        MainActivity.mMds.deleteFile(sub.getPath());
//                    }
                    MainActivity.mMds.deleteFile(temp.getPath());
                } else {
                    success = false;
                    break;
                }
            }
        }
        return success;
    }

    public void listRefresh() {
        mFolderAdapter.refresh(mCurrentPath);
        if (mDialog != null) {
            mDialog.dismiss();
        }
        if (ImageSlideViewerActivity.mDataSource != null && ImageSlideViewerActivity.mPosition >= 0) {
            FileItem item = ImageSlideViewerActivity.mDataSource.get(ImageSlideViewerActivity.mPosition);
            ArrayList<FileItem> items = mFolderAdapter.getList();
            int pos = 0;
            for (FileItem temp : items) {
                if (item.getPath().equals(temp.getPath())) {
                    break;
                } else {
                    pos++;
                }
            }
            if (mView_Type == 0) {
                mListView.setSelection(pos);
            } else {
                mGridView.setSelection(pos);
            }
        }


        ImageSlideViewerActivity.mDataSource = mFolderAdapter.getImageList();
        ;
        ImageSlideViewerActivity.mPosition = -1;

        finishActionMode();
    }

    public ArrayList<FileItem> getCheckList() {
        return mFolderAdapter.getCheckList();
    }

    public void dialogSelectTag() {
        ArrayList<FileItem> items = mFolderAdapter.getCheckList();
        if (items == null || items.size() == 0) {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
            return;
        }

        dialogNewTag(items.get(0).getPath());
    }

    public void dialogNewTag(String path) {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        final LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.dialog_new_tag, null, false);

        final FlowLayout dialog_tag_content_layout = (FlowLayout) view.findViewById(R.id.dialog_tag_content_layout);
        String tagName = "";
        final ArrayList<FileItem> tag = MainActivity.mMds.selectTagFiles(path);
        for (FileItem item : tag) {
            View tag_view = inflater.inflate(R.layout.dialog_tag_list_item, null, false);
            TextView tag_name = (TextView) tag_view.findViewById(R.id.tag_name);
            tag_name.setText(item.getTagName());
            tag_view.setTag(item.getTagName());
            tag_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog_tag_content_layout.removeView(v);
                }
            });
            dialog_tag_content_layout.addView(tag_view);
        }

        final EditText edt = (EditText) view.findViewById(R.id.edt);
        edt.requestFocus();
        InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView tag_add_bt = (TextView) view.findViewById(R.id.tag_add_bt);
        tag_add_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
                String edt_name = edt.getText().toString().trim();
                if (edt_name != null && edt_name.length() > 0) {
                    int count = dialog_tag_content_layout.getChildCount();
                    boolean duplicate = false;

                    if (count >= 10) {
                        Toast.makeText(mMainContext, "Max Count is 10...", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    for (int i = 0; i < count; i++) {
                        View childView = dialog_tag_content_layout.getChildAt(i);
                        if (((String) childView.getTag()).equals(edt_name)) {
                            Toast.makeText(mMainContext, "Duplicate Tag...", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }

                    View tag_view = inflater.inflate(R.layout.dialog_tag_list_item, null, false);
                    TextView tag_name = (TextView) tag_view.findViewById(R.id.tag_name);
                    tag_view.setTag(edt_name);
                    tag_name.setText(edt_name);
                    tag_view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog_tag_content_layout.removeView(v);
                        }
                    });
                    dialog_tag_content_layout.addView(tag_view);
                    edt.setText("");
                } else {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_input_tagname), Toast.LENGTH_SHORT).show();
                }
            }
        });


        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView create = (TextView) view.findViewById(R.id.create);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = dialog_tag_content_layout.getChildCount();
                if (count > 0) {
//                    int select = (int)edt.getTag();
//                    //Toast.makeText(mMainContext, tag_name + " => " + select, Toast.LENGTH_SHORT).show();
//                    MainActivity.mMds.insertTagItem(tag_name, select + "");
//                    ((MainActivity)mMainContext).displayTagList();
//
//                    ArrayList<TagItem> tagItams = MainActivity.mMds.getAllTag();
//                    for(TagItem item : tagItams){
//                        if(item.getName().equals(tag_name)){
//                            tagMethod(item);
//                            break;
//                        }
//                    }
                    final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);

                    ArrayList<FileItem> items = mFolderAdapter.getCheckList();
                    if (items != null && items.size() > 0) {
                        for (FileItem item : items) {
                            for (FileItem tagItem : tag) {
                                MainActivity.mMds.deleteTag(tagItem.getId());
                            }
                            for (int i = 0; i < count; i++) {
                                View childView = dialog_tag_content_layout.getChildAt(i);
                                item.setId(MainActivity.mMds.insertTagItem((String) childView.getTag(), item.getPath()) + "");
                            }
                            item.setTagName("tag");
                        }
                        if (mCurrentPath.equals(getString(R.string.category_recentfile))) {
                            if (mView_Type == 0) {
                                changeListView(false);
                            } else {
                                changeGridView(false);
                            }
                        } else {
                            mFolderAdapter.refresh(mCurrentPath);
                        }
                        Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_add_tag), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
                    }
                    finishActionMode();
                    InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                } else {
                    if (tag != null && tag.size() > 0) {
                        ArrayList<FileItem> items = mFolderAdapter.getCheckList();
                        if (items != null && items.size() > 0) {
                            for (FileItem item : items) {
                                for (FileItem tagItem : tag) {
                                    MainActivity.mMds.deleteTag(tagItem.getId());
                                }
                                item.setTagName("");
                            }
                            if (mCurrentPath.equals(getString(R.string.category_recentfile))) {
                                if (mView_Type == 0) {
                                    changeListView(false);
                                } else {
                                    changeGridView(false);
                                }
                            } else {
                                mFolderAdapter.refresh(mCurrentPath);
                            }
                            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_remove_tag), Toast.LENGTH_SHORT).show();
                        }
                        finishActionMode();
                        InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                        if (mDialog != null) {
                            mDialog.dismiss();
                        }
                    } else {
                        Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_input_tagname), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void dialogNewFolder() {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        final View view = inflater.inflate(R.layout.dialog_new_folder, null, false);
        final EditText edt = (EditText) view.findViewById(R.id.edt);

        edt.requestFocus();
        InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView create = (TextView) view.findViewById(R.id.create);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String folder_name = edt.getText().toString();
                if (folder_name != null && folder_name.length() > 0) {
                    File file = new File(mCurrentPath);
                    Log.e("readable->", "" + file.canRead());
                    Log.e("writable->", "" + file.canWrite());
                    Log.e("executable->", "" + file.canExecute());

                    String createPath = file.getAbsolutePath() + File.separator + folder_name;

                    File create = new File(createPath);

                    if (create.exists()) {
                        Toast.makeText(mMainContext, getResources().getString(R.string.overwrite_title_folder), Toast.LENGTH_SHORT).show();
                    } else {
                        //create.mkdirs();
                        if (create.mkdirs()) {
                            //success
                        } else {
                            //fail
                            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_folder_create_error), Toast.LENGTH_SHORT).show();
                        }
                    }

                    mFolderAdapter.refresh(mCurrentPath);
                    //listRefresh();

                    InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                } else {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_input_foldername), Toast.LENGTH_SHORT).show();
                }

            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void initSecretFolder() {
        //String password = PreferenceHelper.getString(PreferenceHelper.password, "");
        String password = MainActivity.mMds.getSecretUserPassword();
        if (password != null && password.length() > 0) {
            secretMethod();
        } else {
            mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
            LayoutInflater inflater = LayoutInflater.from(mMainContext);
            View view = inflater.inflate(R.layout.dialog_base, null, false);
            TextView title = (TextView) view.findViewById(R.id.title);
            title.setText(getResources().getString(R.string.left_menu_secretfolder));
            TextView content = (TextView) view.findViewById(R.id.content);
            content.setText(getResources().getString(R.string.secretfolder_activate_your_secretfolder_now));
            TextView cancel = (TextView) view.findViewById(R.id.cancel);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                    finishActionMode();
                }
            });
            TextView ok = (TextView) view.findViewById(R.id.ok);
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_frame, new SecretFolderFragment(mMainContext, Utils.getInternalDirectoryPath(), true))
                            .commit();
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                    finishActionMode();
                }
            });
            mDialog.setContentView(view);
            mDialog.show();
        }
    }

    //Search Method
    private class SearchTask extends AsyncTask<String, Void, ArrayList<FileItem>> {
        private final Context context;
        private ProgressDialog pr_dialog;
        private String mCurrentPath;

        private String mSearchStr;

        private SearchTask(Context c, String currentPath) {
            context = c;
            File file = new File(currentPath);
            if (file != null && file.isDirectory()) {
                mCurrentPath = currentPath;
            } else {
                mCurrentPath = Utils.getInternalDirectoryPath();
            }
        }

        @Override
        protected void onPreExecute() {
            pr_dialog = ProgressDialog.show(context, null,
                    context.getResources().getString(R.string.action_search));
            pr_dialog.setCanceledOnTouchOutside(true);
        }

        @Override
        protected ArrayList<FileItem> doInBackground(String... params) {
            //String location = BrowserTabsAdapter.getCurrentBrowserFragment().mCurrentPath;
            mSearchStr = params[0];
            return Utils.searchInDirectory(mCurrentPath, mSearchStr);
        }

        @Override
        protected void onPostExecute(final ArrayList<FileItem> files) {
            int len = files != null ? files.size() : 0;
            mSearchArray = files;
            pr_dialog.dismiss();
            if (len == 0) {
                try {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_search_result_empty), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_search_result_empty), Toast.LENGTH_SHORT).show();
                }
            } else {
                SortUtils.sortList(files, mCurrentPath, MyFileSettings.getSortType());
                mFolderAdapter.addContent(files);
                mFolderAdapter.notifyDataSetChanged();
            }
            setScrollPosition(mSearchStr);
        }
    }

    private class LoadingTask extends AsyncTask<String, Void, ArrayList<FileItem>> {
        private final Context context;
        private ProgressDialog pr_dialog;
        private String mCurrentPath;

        private LoadingTask(Context c, String currentPath) {
            context = c;
            mCurrentPath = currentPath;
        }

        @Override
        protected void onPreExecute() {
            pr_dialog = ProgressDialog.show(context, null,
                    context.getResources().getString(R.string.action_loading));
            pr_dialog.setCanceledOnTouchOutside(true);
        }

        @Override
        protected ArrayList<FileItem> doInBackground(String... params) {
            //String location = BrowserTabsAdapter.getCurrentBrowserFragment().mCurrentPath;

            if (mCurrentPath.equals(getResources().getString(R.string.category_document))) {
                return ListFunctionUtils.listAllDocumentFiles(context);
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_app))) {
                return ListFunctionUtils.listAllAPKFiles(context);
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_zip))) {
                return ListFunctionUtils.listAllZipFiles(context);
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_recentfile))) {
                return ListFunctionUtils.listDaysFiles(context, ListFunctionUtils.LIST_DAY);
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_largefile))) {
                return ListFunctionUtils.listHugeFiles(context, FileObserverService.NEW_FILE_SIZE_LIMIT_100MB);
            } else {
                return Utils.searchInDirectory(mCurrentPath, params[0]);
            }
        }

        @Override
        protected void onPostExecute(final ArrayList<FileItem> files) {
            int len = files != null ? files.size() : 0;
            if (pr_dialog != null && pr_dialog.isShowing()) {
                pr_dialog.dismiss();
            }
            if (len == 0) {
                Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_search_result_empty), Toast.LENGTH_SHORT).show();
            } else {
                SortUtils.sortList(files, mCurrentPath, MyFileSettings.getSortType());
                mFolderAdapter.addContent(files);
                mFolderAdapter.notifyDataSetChanged();
            }
        }
    }
}
