package jiran.com.tmfilemanager.fagment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jiransoft.officedisplay.controllers.downloads.DownloadService;
import com.jiransoft.officedisplay.controllers.events.DownloadFolderQueryEvent;
import com.jiransoft.officedisplay.controllers.sources.OFFICEBOX;
import com.jiransoft.officedisplay.controllers.wrappers.DirectFolderRequestWrapper;
import com.jiransoft.officedisplay.utils.DownloadUtils;
import com.jiransoft.officedisplay.utils.ImagePipelineConfigFactory;
import com.jiransoft.officedisplay.views.activities.Chapter.ChapterActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import de.greenrobot.event.EventBus;
import jiran.com.circleprogress.DonutProgress;
import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.FileSelectActivity;
import jiran.com.tmfilemanager.activity.InitActivity;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.activity.team.TeamManagentActivity;
import jiran.com.tmfilemanager.activity.viewer.ImageSlideViewerActivity;
import jiran.com.tmfilemanager.adapter.StorageAdapter;
import jiran.com.tmfilemanager.common.AlertCallback;
import jiran.com.tmfilemanager.common.FolderSelectTotalDialog;
import jiran.com.tmfilemanager.common.ListFunctionUtils;
import jiran.com.tmfilemanager.common.MimeTypes;
import jiran.com.tmfilemanager.common.SortUtils;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.CurrentItem;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.data.FileSelectItem;
import jiran.com.tmfilemanager.data.StorageListItem;
import jiran.com.tmfilemanager.data.TeamItem;
import jiran.com.tmfilemanager.layout.FlowLayout;
import jiran.com.tmfilemanager.network.JMF_RestfulAdapter;
import jiran.com.tmfilemanager.network.JMF_network;
import jiran.com.tmfilemanager.network.updown.DownloadProgressListener;
import jiran.com.tmfilemanager.network.updown.ListRefreshListener;
import jiran.com.tmfilemanager.service.FileObserverService;
import jiran.com.tmfilemanager.settings.MyFileSettings;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by user on 2016-08-03.
 */


public class StorageFragment extends Fragment {
    //add Category Menu
    public static final int CATEGORY_TOTAL = 0, CATEGORY_APP = -1, CATEGORY_IMAGE = -2, CATEGORY_MOVIE = -3, CATEGORY_MUSIC = -4, CATEGORY_DOCUMENT = -5, CATEGORY_ZIP = -6;
    public static TeamItem mTeam;
    public static boolean mActionMode = false;
    //    sort	String	N	디록토리 및 파일의 정렬값.(이름:+/-name, 날짜:+/-regdate, 용량:+/-size, 확장자: +/-extension)
//    limit	String	N	한번에 조회할 갯수.(기본값: 100)
//    offset	String	N	조회에서 제외할 갯수.(기본값: 0)
//    assort	String	N	모아보기 옵션(application, picture, movie, music, document, archive)
    public static String LIST_LIMIT = "100";
    public static boolean isLastpage = false;
    public Context mMainContext;
    public String mCurrentObjectId;
    public StorageListItem mCurrentArray;
    public String mSearch;
    public ArrayList<FileItem> mSearchArray;
    public int CATEGORY_TYPE = CATEGORY_TOTAL;
    DirectFolderRequestWrapper chapterRequest;
    Stack<StorageListItem> listStack;
    Dialog mDialog;
    HashMap<String, Integer> listPositionMap;
    boolean isConnecting = false;
    LinearLayout path_dimd_layout, list_dimd_layout;
    LinearLayout foler_path_category, frag_folder_category_menu;
    ImageView foler_path_image, foler_path_arrow;
    LinearLayout empty_1depth_layout;
    TextView empty_menu_new_folder, empty_menu_team_manage;
    Dialog downloadProgressDialog;
    DonutProgress download_percent_dp;
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            setDownlaodProgress(msg.what);
        }
    };
    DownloadProgressListener listener = new DownloadProgressListener() {
        @Override
        public void update(int percent, int done) {
            //start a new thread to process job
            new Thread(new Runnable() {
                @Override
                public void run() {
                    //heavy job here
                    //send message to main thread
                    handler.sendEmptyMessage(percent);
                }
            }).start();
            //setDownlaodProgress(percent);
            //Log.i("DownloadProgressListener", "percent => " + percent + "%");
        }
    };
    private AbsListView mListView;
    private GridView mGridView;
    private RecyclerView mRecyclerView;
    private LinearLayout mStorage_path_layout, member_count_layout;
    private TextView team_member_tv;
    //private TextView mFoler_path;
    private HorizontalScrollView foler_path_scroll_layout;
    private LinearLayout foler_path_category_dimd;
    private int mView_Type = 0;
    private StorageAdapter mStoageAdapter;
    ListRefreshListener dialoglistener = new ListRefreshListener() {
        @Override
        public void refresh(ArrayList<FileItem> items) {
            if (items == null) {
                finishActionMode();
            } else {
                for (FileItem item : items) {
                    mStoageAdapter.removeFileItem(item);
                }
                mStoageAdapter.notifyDataSetChanged();
            }
        }
    };
    private Animation showAni, goneAni;

    public StorageFragment() {
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public StorageFragment(Context context, TeamItem team, StorageListItem list) {
        mMainContext = context;
        mTeam = team;
        mCurrentObjectId = list.getCurrentItem().getObjectId();
        mCurrentArray = list;
        mView_Type = MainActivity.mViewType;
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public StorageFragment(Context context, TeamItem team, StorageListItem list, int viewType) {
        mMainContext = context;
        mTeam = team;
        mCurrentObjectId = list.getCurrentItem().getObjectId();
        mCurrentArray = list;
        mView_Type = viewType;
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public StorageFragment(Context context, String currentObjectId, String search) {
        mMainContext = context;
        //mStartPath = currentPath;
        mCurrentObjectId = currentObjectId;
        mSearch = search;
        mView_Type = MainActivity.mViewType;
        setRetainInstance(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mMainContext == null) {
            Intent intent = new Intent(getActivity(), InitActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        }
        ((MainActivity) mMainContext).toolbar.setBackgroundResource(R.color.colorTitleBG);
        View rootView = inflater.inflate(R.layout.frag_storage, container, false);
        chapterRequest = new DirectFolderRequestWrapper(OFFICEBOX.BASE_URL, "", "");
        initLayout(inflater, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        ((MainActivity) mMainContext).finishSearchMode();
        //((MainActivity)mMainContext).toolbarMenu(R.menu.main);

        if (Utils.isSDcardDirectory(true) || (MultiFragment.teamItams != null && MultiFragment.teamItams.size() > 0)) {
            ((MainActivity) getActivity()).setTitle(mTeam.getTeamName(), true);
        } else {
            ((MainActivity) getActivity()).setTitle(mTeam.getTeamName());
        }
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        //getActivity().setTitle(getResources().getString(R.string.app_name));
//        File file = new File(mCurrentPath);
//        if(mListType != 2 && file != null && file.isDirectory()){
//            if(ClipBoard.isEmpty()){
//                list_bottom_menu_paste.setVisibility(View.GONE);
//            }else{
//                list_bottom_menu_paste.setVisibility(View.VISIBLE);
//            }
//        }else{
//            list_bottom_menu_paste.setVisibility(View.GONE);
//        }
        team_member_tv.setText(mTeam.getUserList().size() + "");
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * Display action
     */
    public void actionDisplay() {
        Fresco.initialize(mMainContext, ImagePipelineConfigFactory.getNewOkHttpImagePipelineConfig(mMainContext, JMF_RestfulAdapter.getClient()));
        //Toast.makeText(mMainActivity, getResources().getString(R.string.display), Toast.LENGTH_SHORT).show();

        ArrayList<DirectFolderRequestWrapper> chaptersToDownload = new ArrayList<>();
        chaptersToDownload.add(chapterRequest);

        Intent startService = new Intent(mMainContext, DownloadService.class);
        startService.putExtra(DownloadService.INTENT_QUEUE_DOWNLOAD, chaptersToDownload);
        startService.putExtra("ScreenSaver", false);
        mMainContext.startService(startService);
    }

    public void onEventMainThread(DownloadFolderQueryEvent event) {
        if (event != null) {
            switch (event.getmEvent()) {
                case DownloadUtils.FLAG_PENDING:
//                    if (hud != null) {
//                        hud.dismiss();
//                        hud = null;
//                    }
                    if (!event.getmMSG().equals("ScreenSaver")) {
                        Intent chapterIntent;
                        chapterIntent = ChapterActivity.constructFolderChapterActivityIntent(mMainContext, chapterRequest, 0);
                        mMainContext.startActivity(chapterIntent);
                    }
                    break;
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private boolean categoryMenu(boolean check) {
        finishActionMode();
        if (frag_folder_category_menu.getVisibility() == View.VISIBLE) {
            goneAni = AnimationUtils.loadAnimation(mMainContext, R.anim.main_top_info_slide_up);
            frag_folder_category_menu.setAnimation(goneAni);
            frag_folder_category_menu.setVisibility(View.GONE);
            foler_path_arrow.setImageResource(R.mipmap.list_folder_dropdown);
            path_dimd_layout.setVisibility(View.GONE);
            list_dimd_layout.setVisibility(View.GONE);
            return true;
        } else {
            if (!check) {
                showAni = AnimationUtils.loadAnimation(mMainContext, R.anim.main_top_info_slide_down);
                frag_folder_category_menu.setVisibility(View.VISIBLE);
                frag_folder_category_menu.setAnimation(showAni);
                foler_path_arrow.setImageResource(R.mipmap.list_folder_dropup);
                path_dimd_layout.setVisibility(View.VISIBLE);
                list_dimd_layout.setVisibility(View.VISIBLE);
            }
            return false;
        }
    }

    public boolean categoryTeamList(int type) {
        switch (type) {
            case CATEGORY_APP:
                //foler_path_image.setImageResource(R.mipmap.list_app);
                //Toast.makeText(mMainContext, "TEAM APP 파일 리스트 개발중...", Toast.LENGTH_SHORT).show();
                //mCurrentArray.setList(new ArrayList<FileItem>());
                CATEGORY_TYPE = type;//"application";
                getStorageList(false, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), "");
                break;
            case CATEGORY_IMAGE:
                //foler_path_image.setImageResource(R.mipmap.list_picture);
                //Toast.makeText(mMainContext, "TEAM IMAGE 파일 리스트 개발중...", Toast.LENGTH_SHORT).show();
                //mCurrentArray.setList(new ArrayList<FileItem>());
                CATEGORY_TYPE = type;//"picture";
                getStorageList(false, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), "");
                break;
            case CATEGORY_MOVIE:
                //foler_path_image.setImageResource(R.mipmap.list_movie);
                //Toast.makeText(mMainContext, "TEAM MOVIE 파일 리스트 개발중...", Toast.LENGTH_SHORT).show();
                //mCurrentArray.setList(new ArrayList<FileItem>());
                CATEGORY_TYPE = type;//"movie";
                getStorageList(false, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), "");
                break;
            case CATEGORY_MUSIC:
                //foler_path_image.setImageResource(R.mipmap.list_music);
                //Toast.makeText(mMainContext, "TEAM MUSIC 파일 리스트 개발중...", Toast.LENGTH_SHORT).show();
                //mCurrentArray.setList(new ArrayList<FileItem>());
                CATEGORY_TYPE = type;//"music";
                getStorageList(false, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), "");
                break;
            case CATEGORY_DOCUMENT:
                //foler_path_image.setImageResource(R.mipmap.list_document);
                //Toast.makeText(mMainContext, "TEAM DOCUMENT 파일 리스트 개발중...", Toast.LENGTH_SHORT).show();
                //mCurrentArray.setList(new ArrayList<FileItem>());
                CATEGORY_TYPE = type;//"document";
                getStorageList(false, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), "");
                break;
            case CATEGORY_ZIP:
                //foler_path_image.setImageResource(R.mipmap.list_zip);
                //Toast.makeText(mMainContext, "TEAM ZIP 파일 리스트 개발중...", Toast.LENGTH_SHORT).show();
                //mCurrentArray.setList(new ArrayList<FileItem>());
                CATEGORY_TYPE = type;//"archive";
                getStorageList(false, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), "");
                break;
            default:
                break;
        }

        return true;
    }

    public void initLayout(LayoutInflater inflater, View rootView) {
        listStack = new Stack<StorageListItem>();
        //final MainActivity context = (MainActivity) getActivity();
        path_dimd_layout = (LinearLayout) rootView.findViewById(R.id.path_dimd_layout);
        list_dimd_layout = (LinearLayout) rootView.findViewById(R.id.list_dimd_layout);
        foler_path_category = (LinearLayout) rootView.findViewById(R.id.foler_path_category);
        foler_path_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categoryMenu(false);
            }
        });
        frag_folder_category_menu = (LinearLayout) rootView.findViewById(R.id.frag_folder_category_menu);
        foler_path_image = (ImageView) rootView.findViewById(R.id.foler_path_image);
        foler_path_arrow = (ImageView) rootView.findViewById(R.id.foler_path_arrow);

        listPositionMap = new HashMap<String, Integer>();

        foler_path_scroll_layout = (HorizontalScrollView) rootView.findViewById(R.id.foler_path_scroll_layout);
        foler_path_category_dimd = (LinearLayout) rootView.findViewById(R.id.foler_path_category_dimd);
        foler_path_category_dimd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        mStorage_path_layout = (LinearLayout) rootView.findViewById(R.id.foler_path_layout);
        mStorage_path_layout.removeAllViews();
        member_count_layout = (LinearLayout) rootView.findViewById(R.id.member_count_layout);
        member_count_layout.setVisibility(View.VISIBLE);
        member_count_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mMainContext, TeamManagentActivity.class);
                intent.putExtra("TeamItem", mTeam);
                ((MainActivity) mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_TEAM_MANAGEMENT_ACTIVITY);
            }
        });
        team_member_tv = (TextView) rootView.findViewById(R.id.team_member_tv);

        mListView = (ListView) rootView.findViewById(R.id.folder_listview);
        mListView.setEmptyView(rootView.findViewById(R.id.empty_layout));
        mGridView = (GridView) rootView.findViewById(R.id.folder_gridview);
        mGridView.setEmptyView(rootView.findViewById(R.id.empty_layout));

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.folder_recyclerview);

        empty_1depth_layout = (LinearLayout) rootView.findViewById(R.id.empty_1depth_layout);
        empty_menu_new_folder = (TextView) rootView.findViewById(R.id.empty_menu_new_folder);
        empty_menu_new_folder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogNewFolder();
            }
        });
        empty_menu_team_manage = (TextView) rootView.findViewById(R.id.empty_menu_team_manage);
        empty_menu_team_manage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mMainContext, TeamManagentActivity.class);
                intent.putExtra("TeamItem", mTeam);
                ((MainActivity) mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_TEAM_MANAGEMENT_ACTIVITY);
            }
        });

        if (mView_Type == 0) {
            changeListView(false, true);
        } else {
            changeGridView(false, true);
        }

        finishActionMode();
    }

    public void changeListGridView() {
        mStoageAdapter.listChange = true;
        if (mView_Type == 0) {
            MainActivity.mViewType = 1;
            listPositionMap.put(mCurrentObjectId, mListView.getFirstVisiblePosition());
            changeGridView(true);
        } else {
            MainActivity.mViewType = 0;
            listPositionMap.put(mCurrentObjectId, mGridView.getFirstVisiblePosition());
            changeListView(true);
        }
    }

    public void changeGridView() {
        if (mView_Type == 0) {
            mStoageAdapter.listChange = true;
            MainActivity.mViewType = 1;
            listPositionMap.put(mCurrentObjectId, mListView.getFirstVisiblePosition());
            changeGridView(true);
        }
    }

    public void changeListView() {
        if (mView_Type == 1) {
            mStoageAdapter.listChange = true;
            MainActivity.mViewType = 0;
            listPositionMap.put(mCurrentObjectId, mGridView.getFirstVisiblePosition());
            changeListView(true);
        }
    }

    private void changeGridView(boolean listChange) {
        changeGridView(listChange, false);
    }

    private void changeGridView(boolean listChange, boolean mainlist) {
        //final MainActivity context = (MainActivity) getActivity();
        ((MainActivity) getActivity()).setSelectedTitle("");
        mView_Type = 1;
        changeActionModeList();
        if (listChange) {
            mStoageAdapter = new StorageAdapter(getActivity(), null, mView_Type, mCurrentArray.getList());
        } else {
            mStoageAdapter = new StorageAdapter(getActivity(), null, mView_Type);
        }
        mGridView.setVisibility(View.VISIBLE);
        mListView.setVisibility(View.GONE);
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FileItem selectFileItem = ((FileItem) mGridView.getAdapter().getItem(position));
                if (mActionMode) {
                    mStoageAdapter.setCheck(position);
                    mStoageAdapter.notifyDataSetChanged();
                    toolbarMenuIconChange();
                    int count = mStoageAdapter.getCheckCount();
                    ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
                } else {
                    if (selectFileItem.isDirectory()) {
                        mSearch = null;
                        //Scroll Position Set
                        CATEGORY_TYPE = CATEGORY_TOTAL;
                        getStorageList(false, mTeam.getId(), selectFileItem.getId(), "");
                        // go to the top of the ListView
                        //mGridView.setSelection(0);
                    } else {
//                        //파일일 경우
//                        if(!checkFile(selectFileItem, false)){
//                            downloadfile(mTeam.getId(), selectFileItem, false);
//                        }

                        //if (MimeTypes.isPicture(selectFileItem.getName()) || MimeTypes.isPdf(selectFileItem.getName()) || MimeTypes.isVideo(selectFileItem.getName())) {
                        if (MimeTypes.isPicture(selectFileItem.getName())) {
                            Intent intent = new Intent(mMainContext, ImageSlideViewerActivity.class);
                            ImageSlideViewerActivity.mDataSource = mStoageAdapter.getImageList();
                            ImageSlideViewerActivity.mTeam = mTeam;
                            ImageSlideViewerActivity.mPosition = 0;
                            for (FileItem item : ImageSlideViewerActivity.mDataSource) {
                                if (item.getId().equals(selectFileItem.getId())) {
                                    break;
                                } else {
                                    ImageSlideViewerActivity.mPosition++;
                                }
                            }
                            ((MainActivity) mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_SLIDE_VIEWER);
                        } else {
                            if (!checkFile(selectFileItem, false)) {
                                downloadfile(mTeam.getId(), selectFileItem, false);
                            }
                        }
                    }
                }
            }
        });

        mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (!mActionMode) {
                    mStoageAdapter.setCheckClear();
                }
                mStoageAdapter.setCheck(position);
                int count = mStoageAdapter.getCheckCount();
                ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
                setActionMode();
                toolbarMenuIconChange();
                return true;
            }
        });
        mGridView.setAdapter(mStoageAdapter);
        mGridView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub
                if (!isConnecting && !isLastpage && (mGridView.getLastVisiblePosition() == mGridView.getAdapter().getCount() - 1
                        && mGridView.getChildAt(mGridView.getChildCount() - 1).getBottom() <= mGridView.getHeight())) {
                    //scroll end reached
                    //Write your code here
                    getStorageList(true, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), mCurrentArray.getList().size() + "");
                    //Toast.makeText(mMainActivity, "LAST", Toast.LENGTH_SHORT).show();
                    Log.i("onScrollStateChanged", "mCurrentArray.getList().size() => " + mCurrentArray.getList().size() + " Add Item Action");

                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                // TODO Auto-generated method stub
            }
        });
        if (!listChange) {
            listStack.add(mCurrentArray);
        }
        if (mSearch != null && mSearch.length() > 0) {
            folderSearchListDisplay(mCurrentObjectId, mSearch);
        } else {
            storageListDisplay(mCurrentArray, listChange, mainlist);
        }
//        mFolderAdapter.addFiles(mCurrentPath);
//        mFolderAdapter.notifyDataSetChanged();
    }

    private void changeListView(boolean listChange) {
        changeListView(listChange, false);
    }

    private void changeListView(boolean listChange, boolean mainlist) {
        //final MainActivity context = (MainActivity) getActivity();
        ((MainActivity) getActivity()).setSelectedTitle("");
        mView_Type = 0;
        changeActionModeList();
        if (listChange) {
            mStoageAdapter = new StorageAdapter(getActivity(), null, mView_Type, mCurrentArray.getList());
        } else {
            mStoageAdapter = new StorageAdapter(getActivity(), null, mView_Type);
        }
        mListView.setVisibility(View.VISIBLE);
        mGridView.setVisibility(View.GONE);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FileItem selectFileItem = ((FileItem) mListView.getAdapter().getItem(position));
                if (mActionMode) {
                    mStoageAdapter.setCheck(position);
                    mStoageAdapter.notifyDataSetChanged();
                    toolbarMenuIconChange();
                    int count = mStoageAdapter.getCheckCount();
                    ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
                } else {
                    if (selectFileItem.isDirectory()) {
                        mSearch = null;
                        //Scroll Position Set
                        listPositionMap.put(mCurrentObjectId, mListView.getFirstVisiblePosition());
                        CATEGORY_TYPE = CATEGORY_TOTAL;
                        getStorageList(false, mTeam.getId(), selectFileItem.getId(), "");
                        // go to the top of the ListView
                        //mListView.setSelection(0);
                    } else {
//                        //파일일 경우
//                        if(!checkFile(selectFileItem, false)){
//                            downloadfile(mTeam.getId(), selectFileItem, false);
//                        }

                        //if (MimeTypes.isPicture(selectFileItem.getName()) || MimeTypes.isPdf(selectFileItem.getName()) || MimeTypes.isVideo(selectFileItem.getName())) {
                        if (MimeTypes.isPicture(selectFileItem.getName())) {
                            Intent intent = new Intent(mMainContext, ImageSlideViewerActivity.class);
                            ImageSlideViewerActivity.mDataSource = mStoageAdapter.getImageList();
                            ImageSlideViewerActivity.mTeam = mTeam;
                            ImageSlideViewerActivity.mPosition = 0;
                            for (FileItem item : ImageSlideViewerActivity.mDataSource) {
                                if (item.getId().equals(selectFileItem.getId())) {
                                    break;
                                } else {
                                    ImageSlideViewerActivity.mPosition++;
                                }
                            }
                            ((MainActivity) mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_SLIDE_VIEWER);
                        } else {
                            if (!checkFile(selectFileItem, false)) {
                                downloadfile(mTeam.getId(), selectFileItem, false);
                            }
                        }
                    }
                }
            }
        });

        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (!mActionMode) {
                    mStoageAdapter.setCheckClear();
                }
                mStoageAdapter.setCheck(position);
                int count = mStoageAdapter.getCheckCount();
                ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
                setActionMode();
                toolbarMenuIconChange();
                return true;
            }
        });
        mListView.setAdapter(mStoageAdapter);
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub
                if (!isConnecting && !isLastpage && (mListView.getLastVisiblePosition() == mListView.getAdapter().getCount() - 1
                        && mListView.getChildAt(mListView.getChildCount() - 1).getBottom() <= mListView.getHeight())) {
                    //scroll end reached
                    //Write your code here
                    getStorageList(true, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), mCurrentArray.getList().size() + "");
                    //Toast.makeText(mMainActivity, "LAST", Toast.LENGTH_SHORT).show();
                    Log.i("onScrollStateChanged", "mCurrentArray.getList().size() => " + mCurrentArray.getList().size() + " Add Item Action");

                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                // TODO Auto-generated method stub
            }
        });

        if (!listChange) {
            listStack.add(mCurrentArray);
        }
        if (mSearch != null && mSearch.length() > 0) {
            folderSearchListDisplay(mCurrentObjectId, mSearch);
        } else {
            storageListDisplay(mCurrentArray, listChange, mainlist);
        }
//        mFolderAdapter.addFiles(mCurrentPath);
//        mFolderAdapter.notifyDataSetChanged();
    }

    public void setScrollPosition(String path) {
        //Scroll Position Set
        int position;
        if (listPositionMap.containsKey(path)) {
            position = listPositionMap.get(path);
            listPositionMap.remove(path);
        } else {
            position = 0;
        }

        if (mView_Type == 0) {
            mListView.setSelection(position);
        } else {
            mGridView.setSelection(position);
        }
    }

    public void storageListDisplay(StorageListItem list, boolean listChange) {
        storageListDisplay(list, listChange, false);
    }

    public void storageListDisplay(StorageListItem list, boolean listChange, boolean mainlist) {
        //mFoler_path.setText(mCurrentPath);
        if (listStack.size() <= 1) {
            empty_1depth_layout.setVisibility(View.VISIBLE);
        } else {
            empty_1depth_layout.setVisibility(View.GONE);
        }

        mCurrentArray = list;
        if (mainlist) {
//            if(MultiFragment.teamItams != null && MultiFragment.teamItams.size() > 0){
//                ((MainActivity) getActivity()).setTitle(mTeam.getTeamName(), true);
//            }else {
//                ((MainActivity) getActivity()).setTitle(mTeam.getTeamName());
//            }
        }
        if (!listChange) {
            setStorageFathLayout(list.getCurrentItem());
        }
        foler_path_scroll_layout.post(new Runnable() {
            public void run() {
                foler_path_scroll_layout.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        });

        mStoageAdapter.addContent(list.getList());

        //chapterRequest = new DirectFolderRequestWrapper(OFFICEBOX.BASE_URL, list.getCurrentItem().getObjectId(), list.getCurrentItem().getName());
        chapterRequest.setUrl(list.getCurrentItem().getObjectId());
        chapterRequest.setName(list.getCurrentItem().getName());

        if (mCurrentArray.getCurrentItem().getName().contains(getString(R.string.action_search_result))) {
            ((MainActivity) mMainContext).toolbarMenu(R.menu.main_storage_sub);
        } else {
            if (Utils.checkIsTablet(mMainContext)) {
                ((MainActivity) mMainContext).toolbarMenu(R.menu.main_storage_tablet);
            } else {
                ((MainActivity) mMainContext).toolbarMenu(R.menu.main_storage);
            }
        }

        //Scroll Position Set
        setScrollPosition(mCurrentObjectId);
    }

    public void storageListDisplayBack(StorageListItem list) {
        //mFoler_path.setText(mCurrentPath);
        mCurrentArray = list;
        mCurrentObjectId = list.getCurrentItem().getObjectId();
        isLastpage = false;
        setStorageFathPosition(list.getCurrentItem());
        foler_path_scroll_layout.post(new Runnable() {
            public void run() {
                foler_path_scroll_layout.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        });
        isLastpage = false;
        mStoageAdapter.addContent(list.getList());
        //chapterRequest = new DirectFolderRequestWrapper(OFFICEBOX.BASE_URL, mCurrentObjectId, list.getCurrentItem().getName());
        chapterRequest.setUrl(mCurrentObjectId);
        chapterRequest.setName(list.getCurrentItem().getName());

        if (mCurrentArray.getCurrentItem().getName().contains(getString(R.string.action_search_result))) {
            ((MainActivity) mMainContext).toolbarMenu(R.menu.main_storage_sub);
        } else {
            if (Utils.checkIsTablet(mMainContext)) {
                ((MainActivity) mMainContext).toolbarMenu(R.menu.main_storage_tablet);
            } else {
                ((MainActivity) mMainContext).toolbarMenu(R.menu.main_storage);
            }

        }
        //Scroll Position Set
        setScrollPosition(mCurrentObjectId);
    }

    public void folderSearchListDisplay(String currentPath, String search) {
        //mFoler_path.setText(title);
        //setFolderFathLayout(search, true);
        foler_path_scroll_layout.post(new Runnable() {
            public void run() {
                foler_path_scroll_layout.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        });
        if (mSearchArray != null) {
            mStoageAdapter.addContent(mSearchArray);
            mStoageAdapter.notifyDataSetChanged();
        } else {
            SearchTask mTask = new SearchTask(getActivity(), currentPath);
            mTask.execute(search);
        }
    }

    public void setStorageFathLayout(CurrentItem currentItem) {

        team_member_tv.setText(mTeam.getUserList().size() + "");
        LayoutInflater inflater = LayoutInflater.from(mMainContext);

        View path_view = inflater.inflate(R.layout.frag_folder_path_item, null, false);
        TextView foler_path = (TextView) path_view.findViewById(R.id.foler_path);
        if (mStorage_path_layout.getChildCount() > 0) {
            foler_path.setText("  >  " + currentItem.getName());
        } else {
            //foler_path.setText(currentItem.getName());
            foler_path.setBackgroundResource(R.mipmap.list_folder);
        }

        path_view.setTag(currentItem);
        path_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishActionMode();
                while (listStack.size() > 0) {
                    String currentId = ((CurrentItem) v.getTag()).getObjectId();
                    String categoryId = ((CurrentItem) v.getTag()).getCategoryId();
                    String stackId = listStack.get(listStack.size() - 1).getCurrentItem().getObjectId();
                    String stackcategoryId = listStack.get(listStack.size() - 1).getCurrentItem().getCategoryId();
                    if (categoryId.length() > 0 || stackcategoryId.length() > 0) {
                        if (categoryId.equals(stackcategoryId)) {
                            storageListDisplayBack(listStack.get(listStack.size() - 1));
                            break;
                        } else {
                            listStack.pop();
                        }
                    } else if (currentId.equals(stackId)) {
                        //storageListDisplay(listStack.get(listStack.size() - 1));
                        storageListDisplayBack(listStack.get(listStack.size() - 1));
                        break;
                    } else {
                        listStack.pop();
                    }
                }
            }
        });
        mStorage_path_layout.addView(path_view);
    }

    public void setStorageFathPosition(CurrentItem currentItem) {
        int totalCount = mStorage_path_layout.getChildCount();
        int stackCount = listStack.size();
        for (int i = totalCount - 1; i >= stackCount; i--) {
            View view = mStorage_path_layout.getChildAt(i);
            mStorage_path_layout.removeView(view);
//            CurrentItem selectItem = (CurrentItem)view.getTag();
//
//            if(currentItem.getCategoryId() != null && currentItem.getCategoryId().length() > 0) {
//                mStorage_path_layout.removeView(view);
//                break;
//            }else if(currentItem.getObjectId().equals(selectItem.getObjectId())){
//                break;
//            }else{
//                mStorage_path_layout.removeView(view);
//            }
        }
    }

    public void toolbarMenuIconChange() {
        ArrayList<FileItem> items = mStoageAdapter.getCheckList();
        //getActivity().setTitle(items.size() + " " +  getResources().getString(R.string.selected));
        boolean allfiles = true;
        boolean allfavorite = true;
        if (items != null && items.size() > 0) {
            if (items.size() == 1) {
                //File file = new File(items.get(0).getPath());
                if (!items.get(0).isDirectory()) {
                    ((MainActivity) mMainContext).item_menu_another_app.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_rename.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_send_member.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_secretfolder.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_favorite.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_tag.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_e_album.setVisibility(View.VISIBLE);
                } else {
                    ((MainActivity) mMainContext).item_menu_another_app.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_rename.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_send_member.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_secretfolder.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_favorite.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_tag.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_e_album.setVisibility(View.GONE);
                }
            } else {
                for (FileItem item : items) {
                    if (allfiles) {
                        if (item.isDirectory()) {
                            allfiles = false;
                        }
                    }
                    if (allfavorite) {
                        if (!item.getFavorite()) {
                            allfavorite = false;
                        }
                    }
                }
//            if(allhide){
//                ((MainActivity)mMainContext).toolbar.getMenu().getItem(0).setIcon(R.mipmap.ic_unhide);
//            }else{
//                ((MainActivity)mMainContext).toolbar.getMenu().getItem(0).setIcon(R.mipmap.ic_hide);
//            }


                if (allfiles) {
                    ((MainActivity) mMainContext).item_menu_another_app.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_rename.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_send_member.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_secretfolder.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_favorite.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_tag.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_e_album.setVisibility(View.GONE);
                } else {
                    ((MainActivity) mMainContext).item_menu_another_app.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_rename.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_send_member.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_secretfolder.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_favorite.setVisibility(View.VISIBLE);
                    ((MainActivity) mMainContext).item_menu_tag.setVisibility(View.GONE);
                    ((MainActivity) mMainContext).item_menu_e_album.setVisibility(View.GONE);
                }
                if (allfavorite) {
                    ((MainActivity) mMainContext).toolbar.getMenu().getItem(5).setIcon(R.mipmap.ic_unfavorite);
                } else {
                    ((MainActivity) mMainContext).toolbar.getMenu().getItem(5).setIcon(R.mipmap.ic_favorite);
                }
            }
        } else {
            ((MainActivity) mMainContext).toolbar.getMenu().getItem(0).setVisible(false);
            ((MainActivity) mMainContext).toolbar.getMenu().getItem(1).setVisible(false);
            ((MainActivity) mMainContext).toolbar.getMenu().getItem(5).setIcon(R.mipmap.ic_favorite);
        }
    }

    public void setActionMode() {
        mActionMode = true;
        //getActivity().setTitle("0 " +  getResources().getString(R.string.selected));
        ((MainActivity) mMainContext).toolbar.getMenu().clear();
        ((MainActivity) mMainContext).toolbar.inflateMenu(R.menu.action_mode);
        ((MainActivity) mMainContext).finishSearchMode();
        ((MainActivity) mMainContext).multiSelectMode();
        foler_path_category_dimd.setVisibility(View.VISIBLE);
        mStoageAdapter.notifyDataSetChanged();
    }

    public void finishActionMode() {
        //getActivity().setTitle(getResources().getString(R.string.app_name));
        //((MainActivity)getActivity()).setTitle(mTeam.getTeamName());
        mActionMode = false;
        changeActionModeList();
//        if(ClipBoard.isEmpty()){
//            list_bottom_menu_paste.setVisibility(View.GONE);
//        }else{
//            list_bottom_menu_paste.setVisibility(View.VISIBLE);
//        }
        ((MainActivity) mMainContext).finishMultiSelectMode();
        if (listStack.size() <= 1) {
            empty_1depth_layout.setVisibility(View.VISIBLE);
        } else {
            empty_1depth_layout.setVisibility(View.GONE);
        }
        foler_path_category_dimd.setVisibility(View.GONE);
        mStoageAdapter.setCheckClear();
        mStoageAdapter.notifyDataSetChanged();
    }

    public void listAllcheck() {
        if (mStoageAdapter.isAllChecked()) {
            mStoageAdapter.setCheckClear();
            mStoageAdapter.notifyDataSetChanged();
        } else {
            mStoageAdapter.setAllCheck();
            mStoageAdapter.notifyDataSetChanged();
        }
        int count = mStoageAdapter.getCheckCount();
        ((MainActivity) getActivity()).setSelectedTitle(count == 0 ? "" : count + " Selected");
    }

    public void listCheckClear() {
        mStoageAdapter.setCheckClear();
        mStoageAdapter.notifyDataSetChanged();
    }

    public void changeActionModeList() {
        if (mMainContext != null && ((MainActivity) mMainContext).toolbar != null) {
            if (mCurrentArray.getCurrentItem().getName().contains(getString(R.string.action_search_result))) {
                ((MainActivity) mMainContext).toolbarMenu(R.menu.main_storage_sub);
            } else {
                if (Utils.checkIsTablet(mMainContext)) {
                    ((MainActivity) mMainContext).toolbarMenu(R.menu.main_storage_tablet);
                } else {
                    ((MainActivity) mMainContext).toolbarMenu(R.menu.main_storage);
                }
            }
        }
    }

    public boolean onBackPressed() {

        if (mActionMode) {
            finishActionMode();
            return true;
        } else if (categoryMenu(true)) {
            return true;
        } else if (listStack != null && listStack.size() == 1) {
            //getActivity().finish();
            ((MainActivity) mMainContext).initFragment();
            return false;
        } else {
            try {
                if (mSearch != null && mSearch.length() > 0) {
                    mSearch = null;
                    listStack.pop();
                    storageListDisplayBack(listStack.get(listStack.size() - 1));
                } else {
                    listStack.pop();
                    storageListDisplayBack(listStack.get(listStack.size() - 1));
                }
            } catch (Exception e) {
                //getActivity().finish();
                ((MainActivity) mMainContext).initFragment();
                return false;
            }
            // get position of the previous folder in ListView
            //mListView.setSelection(mListAdapter.getPosition(file.getPath()));
            return true;
        }
    }

    public void shareMethod() {
        //int check_count = mStoageAdapter.getCheckCount();
        ArrayList<FileItem> items = mStoageAdapter.getCheckList();
        if (items != null && items.size() > 0) {
            if (!checkFile(items.get(0), true)) {
                downloadfile(mTeam.getId(), items.get(0), true);
            }
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        //finishActionMode();
    }

    public void secretMethod() {
        final int check_count = mStoageAdapter.getCheckCount();
        final ArrayList<FileItem> items = mStoageAdapter.getCheckList();
        if (items != null && items.size() > 0) {
            mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
            LayoutInflater inflater = LayoutInflater.from(mMainContext);
            View view = inflater.inflate(R.layout.dialog_secret_copy_move, null, false);
            TextView title = (TextView) view.findViewById(R.id.title);
            title.setText(getResources().getString(R.string.left_menu_secretfolder));
//            TextView content = (TextView) view.findViewById(R.id.content);
//            content.setText(getResources().getString(R.string.secretfolder_do_you_want_save_select_file_in_secretfolder_1) + check_count
//                    + getResources().getString(R.string.secretfolder_do_you_want_save_select_file_in_secretfolder_2));
            final CheckBox secret_copy = (CheckBox) view.findViewById(R.id.secret_copy);
            final CheckBox secret_move = (CheckBox) view.findViewById(R.id.secret_move);
            secret_copy.setEnabled(false);
            secret_copy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        secret_copy.setEnabled(false);
                        secret_move.setEnabled(true);
                        secret_move.setChecked(false);
                    } else {

                    }
                }
            });
            secret_move.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        secret_copy.setEnabled(true);
                        secret_move.setEnabled(false);
                        secret_copy.setChecked(false);
                    } else {

                    }
                }
            });
            TextView cancel = (TextView) view.findViewById(R.id.cancel);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                }
            });
            TextView ok = (TextView) view.findViewById(R.id.ok);
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (FileItem item : items) {
                        File from = new File(item.getPath());
//                        File directory = from.getParentFile();
//                        File to = new File(directory, item.getName().replaceFirst(".", ""));
//                        from.renameTo(to);
//                        item.setPath(directory.getPath() + File.separator + item.getName().replaceFirst(".", ""));
//                        item.setName(item.getName().replaceFirst(".", ""));
                        // 시스템으로부터 현재시간(ms) 가져오기
                        long now = System.currentTimeMillis();
                        if (secret_move.isChecked()) {
                            Utils.moveToSecretDirectory(from, "." + Utils.stripFilenameExtension(from.getName()), mMainContext);
                        } else {
                            Utils.moveToSecretDirectory(from, "." + Utils.stripFilenameExtension(from.getName()), mMainContext, false);
                        }

                    }
                    //Toast.makeText(mMainContext, check_count + getResources().getString(R.string.secretfolder_move_to_secretfolder), Toast.LENGTH_SHORT).show();
                    Toast.makeText(mMainContext, getResources().getString(R.string.secretfolder_move_to_secretfolder), Toast.LENGTH_SHORT).show();
                    listRefresh();
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                }
            });
            mDialog.setContentView(view);
            mDialog.show();
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        finishActionMode();
    }

    public void hideMethod() {
        int check_count = mStoageAdapter.getCheckCount();
        ArrayList<FileItem> items = mStoageAdapter.getCheckList();
        if (items != null && items.size() > 0) {
            boolean allHide = true;
            for (FileItem item : items) {
                if (!item.getName().startsWith(".")) {
                    allHide = false;
                    break;
                }
            }
            for (FileItem item : items) {
                if (allHide) {
                    File from = new File(item.getPath());
                    File directory = from.getParentFile();
                    File to = new File(directory, item.getName().replaceFirst(".", ""));
                    from.renameTo(to);
                    item.setPath(directory.getPath() + File.separator + item.getName().replaceFirst(".", ""));
                    item.setName(item.getName().replaceFirst(".", ""));
                } else {
                    if (!item.getName().startsWith(".")) {
                        File from = new File(item.getPath());
                        File directory = from.getParentFile();
                        File to = new File(directory, "." + item.getName());
                        from.renameTo(to);
                        item.setPath(directory.getPath() + File.separator + "." + item.getName());
                        item.setName("." + item.getName());
                    }
                }
            }
            if (allHide) {
                Toast.makeText(mMainContext, check_count + getResources().getString(R.string.snackbar_file_unhide), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mMainContext, check_count + getResources().getString(R.string.snackbar_file_hide), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        finishActionMode();
    }

    public void copyMethod(final boolean oriRemove) {
        final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
        int count = mStoageAdapter.getCheckCount();

        if (count > 0) {
            ArrayList<FileItem> items = mStoageAdapter.getCheckList();
            boolean checkFolder = false;
            String object_id = "";
            for (FileItem item : items) {
                if (item.getSObjectType() != null && item.getSObjectType().equals("D")) {
                    checkFolder = true;
                }
                object_id += ("," + item.getId());
            }
//            FolderSelectTotalDialog.ORI_FILE_TYPE = FolderSelectTotalDialog.STORAGE_MEMORY;
//            FolderSelectTotalDialog selectDialog = new FolderSelectTotalDialog(getActivity(), R.style.Theme_TransparentBackground, getActivity(), !oriRemove, checkFolder, dialoglistener);
//            selectDialog.show();
            FolderSelectTotalDialog.ORI_FILE_TYPE = FolderSelectTotalDialog.STORAGE_MEMORY;
            FolderSelectTotalDialog.SELECT_TEAM = mTeam;
            String finalObject_id = object_id.replaceFirst(",", "");
            ;
            FolderSelectTotalDialog selectDialog = new FolderSelectTotalDialog(getActivity(), R.style.Theme_TransparentBackground, getActivity(), !oriRemove, checkFolder, new AlertCallback() {
                @Override
                public void ok(Object teamObj, Object obj) {
                    String target_id = (String) obj;
                    TeamItem target_team = (TeamItem) teamObj;
                    if (oriRemove) {
                        if (target_team.getId().equals(mTeam.getId())) {
                            moveStorage(finalObject_id, target_id);
                        } else {
                            moveOtherStorage(finalObject_id, target_team.getId(), target_id);
                        }
                    } else {
                        if (target_team.getId().equals(mTeam.getId())) {
                            copyStorage(finalObject_id, target_id);
                        } else {
                            copyOtherStorage(finalObject_id, target_team.getId(), target_id);
                        }
                    }
                    FolderSelectTotalDialog.SELECT_TEAM = null;
                }

                @Override
                public void cancle(Object obj) {
                    FolderSelectTotalDialog.SELECT_TEAM = null;
                }
            });
            selectDialog.show();
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        //finishActionMode();
    }

    public void copyStorage(String object_id, String target_id) {
        try {
            ((MainActivity) mMainContext).showProgress();
            JMF_network.copyTeamFile(mTeam.getId(), object_id, target_id)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        ((MainActivity) mMainContext).dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF copyTeamFile", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    Toast.makeText(mMainContext, mMainContext.getString(R.string.copysuccsess), Toast.LENGTH_SHORT).show();
                                    finishActionMode();
                                    for (StorageListItem sList : listStack) {
                                        if (sList.getCurrentItem().getObjectId().equals(target_id)) {
                                            upDateList(target_id);
                                        }
                                    }
                                } else {
                                    try {
                                        String errorMsg = (String) gigaPodPair.second;
                                        Toast.makeText(mMainContext, errorMsg, Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        Toast.makeText(mMainContext, mMainContext.getString(R.string.copyfail), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            throwable -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            ((MainActivity) mMainContext).dismissProgress();
            e.printStackTrace();
            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void moveStorage(String object_id, String target_id) {
        try {
            ((MainActivity) mMainContext).showProgress();
            JMF_network.moveTeamFile(mTeam.getId(), object_id, target_id)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        ((MainActivity) mMainContext).dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF moveTeamFile", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    ArrayList<FileItem> items = mStoageAdapter.getCheckList();
                                    for (FileItem item : items) {
                                        mStoageAdapter.removeFileItem(item);
                                    }
                                    finishActionMode();
                                    Toast.makeText(mMainContext, mMainContext.getString(R.string.movesuccsess), Toast.LENGTH_SHORT).show();
                                    for (StorageListItem sList : listStack) {
                                        if (sList.getCurrentItem().getObjectId().equals(target_id)) {
                                            upDateList(target_id);
                                        }
                                    }
                                } else {
                                    try {
                                        String errorMsg = (String) gigaPodPair.second;
                                        Toast.makeText(mMainContext, errorMsg, Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        Toast.makeText(mMainContext, mMainContext.getString(R.string.movefail), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            throwable -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            ((MainActivity) mMainContext).dismissProgress();
            e.printStackTrace();
            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void copyOtherStorage(String object_id, String target_team_id, String target_id) {
        try {
            ((MainActivity) mMainContext).showProgress();
            JMF_network.copyOtherTeamFile(mTeam.getId(), object_id, target_team_id, target_id)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        ((MainActivity) mMainContext).dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF copyTeamFile", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    Toast.makeText(mMainContext, mMainContext.getString(R.string.copysuccsess), Toast.LENGTH_SHORT).show();
                                    finishActionMode();
                                    for (StorageListItem sList : listStack) {
                                        if (sList.getCurrentItem().getObjectId().equals(target_id)) {
                                            upDateList(target_id);
                                        }
                                    }
                                } else {
                                    try {
                                        String errorMsg = (String) gigaPodPair.second;
                                        Toast.makeText(mMainContext, errorMsg, Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        Toast.makeText(mMainContext, mMainContext.getString(R.string.copyfail), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            throwable -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            ((MainActivity) mMainContext).dismissProgress();
            e.printStackTrace();
            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void moveOtherStorage(String object_id, String target_team_id, String target_id) {
        try {
            ((MainActivity) mMainContext).showProgress();
            JMF_network.moveOtherTeamFile(mTeam.getId(), object_id, target_team_id, target_id)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        ((MainActivity) mMainContext).dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF moveTeamFile", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    ArrayList<FileItem> items = mStoageAdapter.getCheckList();
                                    for (FileItem item : items) {
                                        mStoageAdapter.removeFileItem(item);
                                    }
                                    finishActionMode();
                                    Toast.makeText(mMainContext, mMainContext.getString(R.string.movesuccsess), Toast.LENGTH_SHORT).show();
                                    for (StorageListItem sList : listStack) {
                                        if (sList.getCurrentItem().getObjectId().equals(target_id)) {
                                            upDateList(target_id);
                                        }
                                    }
                                } else {
                                    try {
                                        String errorMsg = (String) gigaPodPair.second;
                                        Toast.makeText(mMainContext, errorMsg, Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        Toast.makeText(mMainContext, mMainContext.getString(R.string.movefail), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            throwable -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            ((MainActivity) mMainContext).dismissProgress();
            e.printStackTrace();
            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void favoriteMethod() {

        final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
        int check_count = mStoageAdapter.getCheckCount();
        ArrayList<FileItem> items = mStoageAdapter.getCheckList();
        if (items != null && items.size() > 0) {
            boolean allFavorite = true;
            for (FileItem item : items) {
                if (!item.getFavorite()) {
                    allFavorite = false;
                    break;
                }
            }
            for (FileItem item : items) {
                if (allFavorite) {
//                    if(item.getTagId() != null && item.getTagId().length() > 0){
//                        MainActivity.mMds.updateFileFavorite(item.getId(), "N");
//                    }else
                    {
                        MainActivity.mMds.deleteTag(item.getId());
                        //mStoageAdapter.removeFavoriteFiles(item);
                    }
                    item.setId("");
                } else {
//                    if(item.getFavorite() != null && item.getFavorite().length() == 0){
//                        MainActivity.mMds.updateFileFavorite(item.getId(), "Y");
//                    }else{
//                        MainActivity.mMds.insertFavoriteItem(item.getPath());
//                    }
                    item.setId(MainActivity.mMds.insertFavoriteItem(item.getPath()) + "");
                }
            }

            if (allFavorite) {
                Toast.makeText(mMainContext, check_count + getResources().getString(R.string.snackbar_file_unfavorite), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mMainContext, check_count + getResources().getString(R.string.snackbar_file_favorite), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        mStoageAdapter.notifyDataSetChanged();
        finishActionMode();
    }

    public void deleteMethod() {
        int check_count = mStoageAdapter.getCheckCount();

        if (check_count > 0) {
            dialogDeleteFile(check_count);
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
            //finishActionMode();
        }
    }

    public void renameMethod() {
        int check_count = mStoageAdapter.getCheckCount();
        if (check_count > 0) {
            dialogRename();
        } else {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
            //finishActionMode();
        }
    }

    //    public void sendFileIDMethod() {
//        ArrayList<FileItem> tempList = mFolderAdapter.getCheckList();
//
//        if(tempList.size() > 0){
//            boolean isDirectory = false;
//            for(FileItem item : tempList){
//                File file = new File(item.getPath());
//                if(file.isDirectory()){
//                    isDirectory = true;
//                    break;
//                }
//            }
//            if(isDirectory){
//                View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
//                Snackbar.make(viewPos, getResources().getString(R.string.Dialog_Fileview_NoFolderRequest), Snackbar.LENGTH_SHORT)
//                        .setAction("Action", null).show();
//            }else{
//                FileIDActivity.selectSendFileIDList = mFolderAdapter.getCheckList();
//                Intent intent = new Intent(mMainContext, FileIDActivity.class);
//                ((MainActivity)mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_FILE_ID_SEND);
//            }
//        }else{
//            View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
//            Snackbar.make(viewPos, getResources().getString(R.string.snackbar_no_select), Snackbar.LENGTH_SHORT)
//                    .setAction("Action", null).show();
//        }
//    }
    public void sortDate() {
        if (MyFileSettings.getSortType() == SortUtils.SORT_DATE) {
            MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
        } else {
            MyFileSettings.setSortType(SortUtils.SORT_DATE);
        }
        mCurrentArray.setList(new ArrayList<FileItem>());
        getStorageList(true, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), "");
    }

//    public void deleteStorageFolder(FileItem item){
//        try{
//            JMF_network.deleteFolder(mTeam.getId(), item.getId())
//                    .flatMap(gigaPodPair -> {
//                        return Observable.just(gigaPodPair);
//                    })
//                    .doOnError(throwable -> {
//                        ((MainActivity)mMainContext).dismissProgress();
//                        isConnecting = false;
//                    })
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(gigaPodPair -> {
//                                isConnecting = false;
//                                if (gigaPodPair.first) {
//                                    Log.i("JMF deleteFolder", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
//                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
//                                    listRefresh();
//
//                                    Toast.makeText(mMainContext, mMainContext.getString(R.string.snackbar_file_delete), Toast.LENGTH_SHORT).show();
//                                    mStoageAdapter.removeFileItem(item);
//                                    finishActionMode();
//                                } else {
//                                    Toast.makeText(mMainContext, mMainContext.getString(R.string.snackbar_delete_error), Toast.LENGTH_SHORT).show();
//                                    //Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
//                                }
//                            },
//                            throwable -> {
//                                ((MainActivity)mMainContext).dismissProgress();
//                                isConnecting = false;
//                                throwable.printStackTrace();
//                            });
//        } catch (Exception e) {
//            ((MainActivity)mMainContext).dismissProgress();
//            isConnecting = false;
//            e.printStackTrace();
//            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
//        }
//    }

    public void sortName() {
        if (MyFileSettings.getSortType() == SortUtils.SORT_ALPHA) {
            MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
        } else {
            MyFileSettings.setSortType(SortUtils.SORT_ALPHA);
        }
        mCurrentArray.setList(new ArrayList<FileItem>());
        getStorageList(true, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), "");
    }

    public void sortType() {
        if (MyFileSettings.getSortType() == SortUtils.SORT_TYPE) {
            MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
        } else {
            MyFileSettings.setSortType(SortUtils.SORT_TYPE);
        }
        mCurrentArray.setList(new ArrayList<FileItem>());
        getStorageList(true, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), "");
    }

//    public void renameStorageFolder(FileItem item, String name){
//        try{
//            ((MainActivity)mMainContext).showProgress();
//            JMF_network.modifyFolderName(mTeam.getId(), item.getId(), name)
//                    .flatMap(gigaPodPair -> {
//                        return Observable.just(gigaPodPair);
//                    })
//                    .doOnError(throwable -> {
//                        ((MainActivity)mMainContext).dismissProgress();
//                    })
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(gigaPodPair -> {
//                                ((MainActivity)mMainContext).dismissProgress();
//                                if (gigaPodPair.first) {
//                                    Log.i("JMF deleteFolder", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
//                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
//                                    Toast.makeText(mMainContext, mMainContext.getString(R.string.snackbar_file_rename), Toast.LENGTH_SHORT).show();
//                                    item.setName(name);
//                                    finishActionMode();
//                                } else {
//                                    Toast.makeText(mMainContext, mMainContext.getString(R.string.snackbar_rename_error), Toast.LENGTH_SHORT).show();
//                                    //Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
//                                }
//                            },
//                            throwable -> {
//                                ((MainActivity)mMainContext).dismissProgress();
//                                throwable.printStackTrace();
//                            });
//        } catch (Exception e) {
//            ((MainActivity)mMainContext).dismissProgress();
//            e.printStackTrace();
//            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
//        }
//    }

    public void sortSize() {
        if (MyFileSettings.getSortType() == SortUtils.SORT_SIZE) {
            MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
        } else {
            MyFileSettings.setSortType(SortUtils.SORT_SIZE);
        }
        mCurrentArray.setList(new ArrayList<FileItem>());
        getStorageList(true, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), "");
    }

    public void dialogSetSort() {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.dialog_select_sort, null, false);
        LinearLayout sort_by_name = (LinearLayout) view.findViewById(R.id.sort_by_name);
        TextView sort_by_name_txt = (TextView) view.findViewById(R.id.sort_by_name_txt);
        if (MyFileSettings.getSortType() != SortUtils.SORT_ALPHA) {
            sort_by_name_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        } else {
            if (!MyFileSettings.getSortReverse()) {
                sort_by_name_txt.setText(sort_by_name_txt.getText().toString() + " ↑");
            } else {
                sort_by_name_txt.setText(sort_by_name_txt.getText().toString() + " ↓");
            }
        }
        sort_by_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                if (MyFileSettings.getSortType() == SortUtils.SORT_ALPHA) {
                    MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
                } else {
                    MyFileSettings.setSortType(SortUtils.SORT_ALPHA);
                }

                mStoageAdapter.reSort();
            }
        });
        LinearLayout sort_by_extension = (LinearLayout) view.findViewById(R.id.sort_by_extension);
        TextView sort_by_extension_txt = (TextView) view.findViewById(R.id.sort_by_extension_txt);
        if (MyFileSettings.getSortType() != SortUtils.SORT_TYPE) {
            sort_by_extension_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        } else {
            if (!MyFileSettings.getSortReverse()) {
                sort_by_extension_txt.setText(sort_by_extension_txt.getText().toString() + " ↑");
            } else {
                sort_by_extension_txt.setText(sort_by_extension_txt.getText().toString() + " ↓");
            }
        }
        sort_by_extension.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                if (MyFileSettings.getSortType() == SortUtils.SORT_TYPE) {
                    MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
                } else {
                    MyFileSettings.setSortType(SortUtils.SORT_TYPE);
                }
                mStoageAdapter.reSort();
            }
        });
        LinearLayout sort_by_date = (LinearLayout) view.findViewById(R.id.sort_by_date);
        TextView sort_by_date_txt = (TextView) view.findViewById(R.id.sort_by_date_txt);
        if (MyFileSettings.getSortType() != SortUtils.SORT_DATE) {
            sort_by_date_txt.setTextColor(getResources().getColorStateList(R.color.gray3));
        } else {
            if (!MyFileSettings.getSortReverse()) {
                sort_by_date_txt.setText(sort_by_date_txt.getText().toString() + " ↑");
            } else {
                sort_by_date_txt.setText(sort_by_date_txt.getText().toString() + " ↓");
            }
        }
        sort_by_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                if (MyFileSettings.getSortType() == SortUtils.SORT_DATE) {
                    MyFileSettings.setSortReverse(!MyFileSettings.getSortReverse());
                } else {
                    MyFileSettings.setSortType(SortUtils.SORT_DATE);
                }
                mStoageAdapter.reSort();
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void dialogDeleteFile(final int check_count) {
        final Dialog dialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.dialog_base, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.dialog_delete_file));
        TextView content = (TextView) view.findViewById(R.id.content);
        content.setText(getResources().getString(R.string.dialog_delete_file_info1) + check_count + getResources().getString(R.string.dialog_delete_file_info2));
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
                ArrayList<FileItem> items = mStoageAdapter.getCheckList();
//                FileItem item = items.get(0);
//                if(item.getSObjectType().equals("D")){
//                    deleteStorageFolder(item);
//                }else
                {
                    deleteStorageFile(items);
                }

                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        dialog.setContentView(view);
        dialog.show();
    }

    public void deleteStorageFile(ArrayList<FileItem> checkList) {
        try {
            String delete = "";
            for (FileItem check : checkList) {
                delete += ("," + check.getId());
            }
            delete = delete.replaceFirst(",", "");
            Log.i("JMF delete", "id =>=> " + delete);
            JMF_network.deleteFile(mTeam.getId(), delete)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        ((MainActivity) mMainContext).dismissProgress();
                        isConnecting = false;
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                isConnecting = false;
                                if (gigaPodPair.first) {
                                    Log.i("JMF deleteFolder", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    listRefresh();

                                    for (FileItem check : checkList) {
                                        mStoageAdapter.removeFileItem(check);
                                    }
                                    Toast.makeText(mMainContext, checkList.size() + mMainContext.getString(R.string.snackbar_file_delete), Toast.LENGTH_SHORT).show();
                                    finishActionMode();
                                } else {
                                    try {
                                        String errorMsg = (String) gigaPodPair.second;
                                        Toast.makeText(mMainContext, errorMsg, Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        Toast.makeText(mMainContext, mMainContext.getString(R.string.snackbar_delete_error), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            throwable -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                isConnecting = false;
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            ((MainActivity) mMainContext).dismissProgress();
            isConnecting = false;
            e.printStackTrace();
            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void dialogRename() {
        ArrayList<FileItem> items = mStoageAdapter.getCheckList();
        FileItem item = items.get(0);
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        final View view = inflater.inflate(R.layout.dialog_new_folder, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.dialog_rename_file));
        final EditText edt = (EditText) view.findViewById(R.id.edt);
        String ext = Utils.getFilenameExtension(item.getName());
        edt.setText(Utils.stripFilenameExtension(item.getName()));

        edt.requestFocus();
        InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView create = (TextView) view.findViewById(R.id.create);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String folder_name = edt.getText().toString();
                if (folder_name != null && folder_name.length() > 0) {
                    String name = "";
                    if (item.getSObjectType().equals("D")) {
                        name = folder_name;
                    } else {
                        if (ext != null && ext.length() > 0) {
                            name = folder_name + "." + ext;//Utils.stripFilenameExtension(folder_name);
                        } else {
                            name = folder_name;
                        }
                    }
                    renameStorageFile(item, name);

                    InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                } else {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_input_foldername), Toast.LENGTH_SHORT).show();
                }

            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void renameStorageFile(FileItem item, String name) {
        try {
            ((MainActivity) mMainContext).showProgress();
            JMF_network.modifyFileName(mTeam.getId(), item.getId(), name)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        ((MainActivity) mMainContext).dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF modifyFileName", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    Toast.makeText(mMainContext, mMainContext.getString(R.string.snackbar_file_rename), Toast.LENGTH_SHORT).show();
                                    item.setName(name);
                                    finishActionMode();
                                } else {
                                    try {
                                        String errorMsg = (String) gigaPodPair.second;
                                        Toast.makeText(mMainContext, errorMsg, Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        Toast.makeText(mMainContext, mMainContext.getString(R.string.snackbar_rename_error), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            throwable -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            ((MainActivity) mMainContext).dismissProgress();
            e.printStackTrace();
            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void listRefresh() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
        mCurrentArray.setList(new ArrayList<FileItem>());
        getStorageList(true, mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), "");
        finishActionMode();
    }

    public void returnViewerlistRefresh(boolean move, String target_id) {
        if (move) {
            ArrayList<FileItem> items = mStoageAdapter.getCheckList();
            for (FileItem item : items) {
                mStoageAdapter.removeFileItem(item);
            }
        }
        finishActionMode();
        if (target_id != null && target_id.length() > 0) {
            for (StorageListItem sList : listStack) {
                if (sList.getCurrentItem().getObjectId().equals(target_id)) {
                    upDateList(target_id);
                }
            }
        }
    }

    public ArrayList<FileItem> getCheckList() {
        return mStoageAdapter.getCheckList();
    }

    public void dialogSelectTag() {
        ArrayList<FileItem> items = mStoageAdapter.getCheckList();
        if (items == null || items.size() == 0) {
            Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
            return;
        }

        dialogNewTag(items.get(0).getPath());
    }

    public void dialogNewTag(String path) {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        final LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.dialog_new_tag, null, false);

        final FlowLayout dialog_tag_content_layout = (FlowLayout) view.findViewById(R.id.dialog_tag_content_layout);
        String tagName = "";
        final ArrayList<FileItem> tag = MainActivity.mMds.selectTagFiles(path);
        for (FileItem item : tag) {
            View tag_view = inflater.inflate(R.layout.dialog_tag_list_item, null, false);
            TextView tag_name = (TextView) tag_view.findViewById(R.id.tag_name);
            tag_name.setText(item.getTagName());
            tag_view.setTag(item.getTagName());
            tag_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog_tag_content_layout.removeView(v);
                }
            });
            dialog_tag_content_layout.addView(tag_view);
        }

        final EditText edt = (EditText) view.findViewById(R.id.edt);
        edt.requestFocus();
        InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView tag_add_bt = (TextView) view.findViewById(R.id.tag_add_bt);
        tag_add_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final View viewPos = getActivity().findViewById(R.id.myCoordinatorLayout);
                String edt_name = edt.getText().toString().trim();
                if (edt_name != null && edt_name.length() > 0) {
                    int count = dialog_tag_content_layout.getChildCount();
                    boolean duplicate = false;

                    if (count >= 10) {
                        Toast.makeText(mMainContext, "Max Count is 10...", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    for (int i = 0; i < count; i++) {
                        View childView = dialog_tag_content_layout.getChildAt(i);
                        if (((String) childView.getTag()).equals(edt_name)) {
                            Toast.makeText(mMainContext, "Duplicate Tag...", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }

                    View tag_view = inflater.inflate(R.layout.dialog_tag_list_item, null, false);
                    TextView tag_name = (TextView) tag_view.findViewById(R.id.tag_name);
                    tag_view.setTag(edt_name);
                    tag_name.setText(edt_name);
                    tag_view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog_tag_content_layout.removeView(v);
                        }
                    });
                    dialog_tag_content_layout.addView(tag_view);
                    edt.setText("");
                } else {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_input_tagname), Toast.LENGTH_SHORT).show();
                }
            }
        });


        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
            }
        });
        TextView create = (TextView) view.findViewById(R.id.create);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = dialog_tag_content_layout.getChildCount();
                if (count > 0) {
//                    int select = (int)edt.getTag();
//                    //Toast.makeText(mMainContext, tag_name + " => " + select, Toast.LENGTH_SHORT).show();
//                    MainActivity.mMds.insertTagItem(tag_name, select + "");
//                    ((MainActivity)mMainContext).displayTagList();
//
//                    ArrayList<TagItem> tagItams = MainActivity.mMds.getAllTag();
//                    for(TagItem item : tagItams){
//                        if(item.getName().equals(tag_name)){
//                            tagMethod(item);
//                            break;
//                        }
//                    }

                    ArrayList<FileItem> items = mStoageAdapter.getCheckList();
                    if (items != null && items.size() > 0) {
                        for (FileItem item : items) {
                            for (FileItem tagItem : tag) {
                                MainActivity.mMds.deleteTag(tagItem.getId());
                            }
                            for (int i = 0; i < count; i++) {
                                View childView = dialog_tag_content_layout.getChildAt(i);
                                item.setId(MainActivity.mMds.insertTagItem((String) childView.getTag(), item.getPath()) + "");
                            }
                        }
                        mStoageAdapter.refresh(new ArrayList<FileItem>());
                        Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_add_tag), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
                    }
                    finishActionMode();
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                } else {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_input_tagname), Toast.LENGTH_SHORT).show();
                }
                InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void dialogNewFolder() {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        final View view = inflater.inflate(R.layout.dialog_new_folder, null, false);
        final EditText edt = (EditText) view.findViewById(R.id.edt);

        edt.requestFocus();
        InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView create = (TextView) view.findViewById(R.id.create);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String folder_name = edt.getText().toString();
                if (folder_name != null && folder_name.length() > 0) {
                    addFolder(mTeam.getId(), mCurrentArray.getCurrentItem().getObjectId(), folder_name);
                    InputMethodManager imm = (InputMethodManager) mMainContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                } else {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_input_foldername), Toast.LENGTH_SHORT).show();
                }

            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void dialogUpload() {
        mDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.dialog_file_select, null, false);
        RelativeLayout select_internaldirectory = (RelativeLayout) view.findViewById(R.id.select_internaldirectory);
        select_internaldirectory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (mDialog != null) {
//                    mDialog.dismiss();
//                }
                moveToFileSelectActivity(Utils.getInternalDirectoryPath());
            }
        });
        RelativeLayout select_sdcard = (RelativeLayout) view.findViewById(R.id.select_sdcard);
        if (Utils.isSDcardDirectory(true)) {
            select_sdcard.setVisibility(View.VISIBLE);
            select_sdcard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if (mDialog != null) {
//                        mDialog.dismiss();
//                    }
                    moveToFileSelectActivity(Utils.getSDcardDirectoryPath());
                }
            });
        } else {
            select_sdcard.setVisibility(View.GONE);
        }

        RelativeLayout select_album = (RelativeLayout) view.findViewById(R.id.select_album);
        select_album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (mDialog != null) {
//                    mDialog.dismiss();
//                }
                moveToFileSelectActivity("album");
            }
        });
        RelativeLayout select_recentfile = (RelativeLayout) view.findViewById(R.id.select_recentfile);
        select_recentfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (mDialog != null) {
//                    mDialog.dismiss();
//                }
                moveToFileSelectActivity(getString(R.string.category_recentfile));
            }
        });
        RelativeLayout select_image = (RelativeLayout) view.findViewById(R.id.select_image);
        select_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (mDialog != null) {
//                    mDialog.dismiss();
//                }
                moveToFileSelectActivity(getString(R.string.category_image));
            }
        });
        RelativeLayout select_video = (RelativeLayout) view.findViewById(R.id.select_video);
        select_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (mDialog != null) {
//                    mDialog.dismiss();
//                }
                moveToFileSelectActivity(getString(R.string.category_video));
            }
        });
        RelativeLayout select_music = (RelativeLayout) view.findViewById(R.id.select_music);
        select_music.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (mDialog != null) {
//                    mDialog.dismiss();
//                }
                moveToFileSelectActivity(getString(R.string.category_music));
            }
        });

        RelativeLayout select_document = (RelativeLayout) view.findViewById(R.id.select_document);
        select_document.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (mDialog != null) {
//                    mDialog.dismiss();
//                }
                moveToFileSelectActivity(getString(R.string.category_document));
            }
        });
        RelativeLayout select_zip = (RelativeLayout) view.findViewById(R.id.select_zip);
        select_zip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (mDialog != null) {
//                    mDialog.dismiss();
//                }
                moveToFileSelectActivity(getString(R.string.category_zip));
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void moveToFileSelectActivity(String path) {
        Intent intent = new Intent(mMainContext, FileSelectActivity.class);
        FileSelectItem item = new FileSelectItem();
        item.setTeamItem(mTeam);
        item.setTeamId(mTeam.getId());
        item.setObjectId(mCurrentObjectId);
        item.setPath(path);
        intent.putExtra("FileSelectItem", item);
        ((MainActivity) mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_FILE_SELECT_ACTIVITY);
    }

    public void getSearchStorageList(String keyword) {
        mSearch = keyword;
        getStorageList(false, mTeam.getId(), mCurrentObjectId, "", keyword);
    }

    public void getStorageList(boolean paging, String teamId, String objectId, String offset) {
        getStorageList(paging, teamId, objectId, offset, "");
    }

    public void getStorageList(boolean paging, String teamId, String objectId, String offset, String keyword) {
        try {
            ((MainActivity) mMainContext).showProgress();
            isConnecting = true;
            String sort = "";
            if (MyFileSettings.getSortReverse()) {
                sort = "+";
            } else {
                sort = "-";
            }
            switch (MyFileSettings.getSortType()) {
                case SortUtils.SORT_ALPHA:
                    sort += "name";
                    break;
                case SortUtils.SORT_TYPE:
                    sort += "extension";
                    break;
                case SortUtils.SORT_SIZE:
                    sort += "size";
                    break;
                case SortUtils.SORT_DATE:
                    sort += "regdate";
                    break;
                default:
                    sort += "name";
                    break;
            }
            String assort = "";
            switch (CATEGORY_TYPE) {
                case CATEGORY_APP:
                    assort = "application";
                    break;
                case CATEGORY_IMAGE:
                    assort = "picture";
                    break;
                case CATEGORY_MOVIE:
                    assort = "movie";
                    break;
                case CATEGORY_MUSIC:
                    assort = "music";
                    break;
                case CATEGORY_DOCUMENT:
                    assort = "document";
                    break;
                case CATEGORY_ZIP:
                    assort = "archive";
                    break;
                default:
                    assort = "";
                    break;
            }

            String finalAssort = assort;
            JMF_network.getFolderList(teamId, objectId, sort, LIST_LIMIT, offset, assort, keyword)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        ((MainActivity) mMainContext).dismissProgress();
                        isConnecting = false;
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                isConnecting = false;
                                if (gigaPodPair.first) {
                                    Log.i("JMF getStorageList", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");

                                    JsonObject current = result.getAsJsonObject("current");
                                    CurrentItem currentItem = new CurrentItem();
                                    currentItem.setObjectId(current.get("object_id").getAsString());
                                    mCurrentObjectId = currentItem.getObjectId();
                                    currentItem.setNode(current.get("node").getAsString());
                                    if (CATEGORY_TYPE < 0) {
                                        currentItem.setName(finalAssort);
                                        currentItem.setCategoryId(CATEGORY_TYPE + "");
                                    } else {
                                        if (keyword != null && keyword.length() > 0) {
                                            currentItem.setName(keyword + getResources().getString(R.string.action_search_result));
                                        } else {
                                            currentItem.setName(current.get("name").getAsString());
                                        }
                                        currentItem.setCategoryId("");
                                    }
                                    currentItem.setSize(current.get("size").getAsString());
                                    currentItem.setRegdate(Utils.formatDate(current.get("regdate").getAsString()));
                                    currentItem.setModified(Utils.formatDate(current.get("modified").getAsString()));

                                    JsonObject lists = result.getAsJsonObject("lists");
                                    isLastpage = lists.get("lastpage").getAsBoolean();
                                    JsonArray rows = lists.getAsJsonArray("rows");
                                    ArrayList<FileItem> storageList = new ArrayList<FileItem>();
                                    for (int i = 0; i < rows.size(); i++) {
                                        FileItem item = new FileItem();
                                        JsonObject row = rows.get(i).getAsJsonObject();
                                        item.setTeamId(mTeam.getId());
                                        item.setId(row.get("object_id").getAsString());
                                        item.setSObjectType(row.get("object_type").getAsString());
                                        item.setSNode(row.get("node").getAsString());
                                        item.setName(row.get("name").getAsString());
                                        item.setSCreator(row.get("creator").getAsString());
                                        item.setSCreatorName(row.get("creator_name").getAsString());
                                        item.setSMemo(row.get("memo").getAsString());
                                        item.setSize(row.get("size").getAsString());
                                        item.setSExtension(row.get("extension").getAsString());
                                        item.setSThumbnail(row.get("thumbnail").getAsString());
                                        item.setSUrlPath(row.get("url_path").getAsString());
                                        item.setRegDate(Utils.formatDate(row.get("regdate").getAsString()));
                                        item.setSModifedDate(Utils.formatDate(row.get("modified").getAsString()));

                                        if (paging) {
                                            mCurrentArray.getList().add(item);
                                        } else {
                                            storageList.add(item);
                                        }
                                    }

                                    if (paging) {
                                        if (offset != null && offset.length() == 0) {
                                            mListView.setSelection(0);
                                            mGridView.setSelection(0);
                                        }
                                        mStoageAdapter.addContent(mCurrentArray.getList());
                                        mStoageAdapter.notifyDataSetChanged();
                                    } else {
                                        if (mSearch != null && mSearch.length() > 0) {
                                            while (listStack.size() > 1) {
                                                listStack.pop();
                                            }
                                            storageListDisplayBack(listStack.get(listStack.size() - 1));
                                            mSearch = "";
                                        }
                                        StorageListItem storageListItem = new StorageListItem(currentItem, storageList);
                                        if (CATEGORY_TYPE == CATEGORY_TOTAL) {
                                            listStack.add(storageListItem);
                                        }
                                        storageListDisplay(storageListItem, false);
                                    }
                                } else {
                                    Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                isConnecting = false;
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            ((MainActivity) mMainContext).dismissProgress();
            isConnecting = false;
            e.printStackTrace();
            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void upDateList(String objectId) {
        try {
            ((MainActivity) mMainContext).showProgress();
            isConnecting = true;
            String sort = "";
            if (MyFileSettings.getSortReverse()) {
                sort = "+";
            } else {
                sort = "-";
            }
            switch (MyFileSettings.getSortType()) {
                case SortUtils.SORT_ALPHA:
                    sort += "name";
                    break;
                case SortUtils.SORT_TYPE:
                    sort += "extension";
                    break;
                case SortUtils.SORT_SIZE:
                    sort += "size";
                    break;
                case SortUtils.SORT_DATE:
                    sort += "regdate";
                    break;
                default:
                    sort += "name";
                    break;
            }
            String assort = "";
            switch (CATEGORY_TYPE) {
                case CATEGORY_APP:
                    assort = "application";
                    break;
                case CATEGORY_IMAGE:
                    assort = "picture";
                    break;
                case CATEGORY_MOVIE:
                    assort = "movie";
                    break;
                case CATEGORY_MUSIC:
                    assort = "music";
                    break;
                case CATEGORY_DOCUMENT:
                    assort = "document";
                    break;
                case CATEGORY_ZIP:
                    assort = "archive";
                    break;
                default:
                    assort = "";
                    break;
            }

            String finalAssort = assort;
            JMF_network.getFolderList(mTeam.getId(), objectId, sort, LIST_LIMIT, "", assort, "")
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        ((MainActivity) mMainContext).dismissProgress();
                        isConnecting = false;
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                isConnecting = false;
                                if (gigaPodPair.first) {
                                    Log.i("JMF getStorageList", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");

                                    JsonObject current = result.getAsJsonObject("current");
                                    CurrentItem currentItem = new CurrentItem();
                                    currentItem.setObjectId(current.get("object_id").getAsString());
                                    mCurrentObjectId = currentItem.getObjectId();
                                    currentItem.setNode(current.get("node").getAsString());
                                    currentItem.setName(current.get("name").getAsString());
                                    currentItem.setCategoryId("");
                                    currentItem.setSize(current.get("size").getAsString());
                                    currentItem.setRegdate(Utils.formatDate(current.get("regdate").getAsString()));
                                    currentItem.setModified(Utils.formatDate(current.get("modified").getAsString()));

                                    JsonObject lists = result.getAsJsonObject("lists");
                                    isLastpage = lists.get("lastpage").getAsBoolean();
                                    JsonArray rows = lists.getAsJsonArray("rows");
                                    ArrayList<FileItem> storageList = new ArrayList<FileItem>();
                                    for (int i = 0; i < rows.size(); i++) {
                                        FileItem item = new FileItem();
                                        JsonObject row = rows.get(i).getAsJsonObject();
                                        item.setTeamId(mTeam.getId());
                                        item.setId(row.get("object_id").getAsString());
                                        item.setSObjectType(row.get("object_type").getAsString());
                                        item.setSNode(row.get("node").getAsString());
                                        item.setName(row.get("name").getAsString());
                                        item.setSCreator(row.get("creator").getAsString());
                                        item.setSCreatorName(row.get("creator_name").getAsString());
                                        item.setSMemo(row.get("memo").getAsString());
                                        item.setSize(row.get("size").getAsString());
                                        item.setSExtension(row.get("extension").getAsString());
                                        item.setSThumbnail(row.get("thumbnail").getAsString());
                                        item.setSUrlPath(row.get("url_path").getAsString());
                                        item.setRegDate(Utils.formatDate(row.get("regdate").getAsString()));
                                        item.setSModifedDate(Utils.formatDate(row.get("modified").getAsString()));

                                        storageList.add(item);
                                    }

                                    for (StorageListItem sList : listStack) {
                                        if (sList.getCurrentItem().getObjectId().equals(objectId)) {
                                            sList.setList(storageList);
                                            break;
                                        }
                                    }
                                } else {
                                    Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                isConnecting = false;
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            ((MainActivity) mMainContext).dismissProgress();
            isConnecting = false;
            e.printStackTrace();
            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void addFolder(String teamId, String objectId, String name) {
        try {
            ((MainActivity) mMainContext).showProgress();
            JMF_network.createFolder(teamId, objectId, name)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        ((MainActivity) mMainContext).dismissProgress();
                        isConnecting = false;
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                isConnecting = false;
                                if (gigaPodPair.first) {
                                    Log.i("JMF createFolder", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    listRefresh();
                                } else {
                                    Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                ((MainActivity) mMainContext).dismissProgress();
                                isConnecting = false;
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            ((MainActivity) mMainContext).dismissProgress();
            isConnecting = false;
            e.printStackTrace();
            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean checkFile(FileItem item, boolean share) {
        try {

            String path = Utils.getStorageSaveFileFolderPath() + "/" + item.getName();
            File file = new File(path);

            if (file != null && file.exists()) {
                if (share) {
                    ArrayList<FileItem> items = mStoageAdapter.getCheckList();
                    items.get(0).setPath(path);
                    Utils.sendFiles(mMainContext, items);
                    finishActionMode();
                    return true;
                }
                //if (MimeTypes.isPicture(file)) {
                if (MimeTypes.isPicture(file) || MimeTypes.isPdf(file) || MimeTypes.isVideo(file)) {
                    Intent intent = new Intent(mMainContext, ImageSlideViewerActivity.class);
                    ArrayList<FileItem> tempList = new ArrayList<FileItem>();
                    item.setcheck(true);
                    item.setPath(path);
                    tempList.add(item);
                    ImageSlideViewerActivity.mDataSource = tempList;
                    ImageSlideViewerActivity.mTeam = mTeam;
                    ImageSlideViewerActivity.mPosition = 0;
                    ((MainActivity) mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_SLIDE_VIEWER);
                    return true;
                }
                Utils.openFile(mMainContext, file);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void downloadfile(String teamId, FileItem downItem, boolean share) {
        try {
            showDownlaodProgress();
            JMF_network.filedownload(teamId, downItem.getId(), Utils.getStorageSaveFileFolderPath(), downItem.getName(), listener)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissDownlaodProgress();
                        isConnecting = false;
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissDownlaodProgress();
                                isConnecting = false;
                                if (gigaPodPair.first) {
                                    Log.i("JMF filedownload", "=>=>" + ((String) gigaPodPair.second));
                                    String path = ((String) gigaPodPair.second);
                                    File file = new File(path);

                                    if (share) {
                                        ArrayList<FileItem> items = mStoageAdapter.getCheckList();
                                        items.get(0).setPath(path);
                                        Utils.sendFiles(mMainContext, items);
                                        finishActionMode();
                                        return;
                                    }

                                    //if (MimeTypes.isPicture(file)) {
                                    if (MimeTypes.isPicture(file) || MimeTypes.isPdf(file) || MimeTypes.isVideo(file)) {
                                        Intent intent = new Intent(mMainContext, ImageSlideViewerActivity.class);
                                        ArrayList<FileItem> tempList = new ArrayList<FileItem>();
                                        downItem.setcheck(true);
                                        downItem.setPath(path);
                                        tempList.add(downItem);
                                        ImageSlideViewerActivity.mDataSource = tempList;
                                        ImageSlideViewerActivity.mTeam = mTeam;
                                        ImageSlideViewerActivity.mPosition = 0;
                                        ((MainActivity) mMainContext).startActivityForResult(intent, MainActivity.REQUEST_CODE_SLIDE_VIEWER);
                                        return;
                                    }
                                    Utils.openFile(mMainContext, file);
                                } else {
                                    Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissDownlaodProgress();
                                isConnecting = false;
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissDownlaodProgress();
            isConnecting = false;
            e.printStackTrace();
            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void showDownlaodProgress() {
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.progress_download_dialog_gray, null, false);

        download_percent_dp = (DonutProgress) view.findViewById(R.id.download_percent_dp);
        downloadProgressDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        downloadProgressDialog.setContentView(view);
        downloadProgressDialog.setCancelable(false);
        downloadProgressDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        downloadProgressDialog.show();
    }

    public void setDownlaodProgress(Integer percent) {
        download_percent_dp.setProgress(percent);
    }

    public void dismissDownlaodProgress() {
        if (downloadProgressDialog != null) {
            downloadProgressDialog.dismiss();
        }
    }

    //Search Method
    private class SearchTask extends AsyncTask<String, Void, ArrayList<FileItem>> {
        private final Context context;
        private ProgressDialog pr_dialog;
        private String mCurrentPath;

        private String mSearchStr;

        private SearchTask(Context c, String currentPath) {
            context = c;
            File file = new File(currentPath);
            if (file != null && file.isDirectory()) {
                mCurrentPath = currentPath;
            } else {
                mCurrentPath = Utils.getInternalDirectoryPath();
            }
        }

        @Override
        protected void onPreExecute() {
            pr_dialog = ProgressDialog.show(context, null,
                    context.getResources().getString(R.string.action_search));
            pr_dialog.setCanceledOnTouchOutside(true);
        }

        @Override
        protected ArrayList<FileItem> doInBackground(String... params) {
            //String location = BrowserTabsAdapter.getCurrentBrowserFragment().mCurrentPath;
            mSearchStr = params[0];
            return Utils.searchInDirectory(mCurrentPath, mSearchStr);
        }

        @Override
        protected void onPostExecute(final ArrayList<FileItem> files) {
            int len = files != null ? files.size() : 0;
            mSearchArray = files;
            pr_dialog.dismiss();
            if (len == 0) {
                try {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_search_result_empty), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_search_result_empty), Toast.LENGTH_SHORT).show();
                }
            } else {
                SortUtils.sortList(files, mCurrentPath, MyFileSettings.getSortType());
                mStoageAdapter.addContent(files);
                mStoageAdapter.notifyDataSetChanged();
            }
            setScrollPosition(mSearchStr);
        }
    }

    private class LoadingTask extends AsyncTask<String, Void, ArrayList<FileItem>> {
        private final Context context;
        private ProgressDialog pr_dialog;
        private String mCurrentPath;

        private LoadingTask(Context c, String currentPath) {
            context = c;
            mCurrentPath = currentPath;
        }

        @Override
        protected void onPreExecute() {
            pr_dialog = ProgressDialog.show(context, null,
                    context.getResources().getString(R.string.action_loading));
            pr_dialog.setCanceledOnTouchOutside(true);
        }

        @Override
        protected ArrayList<FileItem> doInBackground(String... params) {
            //String location = BrowserTabsAdapter.getCurrentBrowserFragment().mCurrentPath;

            if (mCurrentPath.equals(getResources().getString(R.string.category_document))) {
                return ListFunctionUtils.listAllDocumentFiles(context);
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_app))) {
                return ListFunctionUtils.listAllAPKFiles(context);
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_zip))) {
                return ListFunctionUtils.listAllZipFiles(context);
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_recentfile))) {
                return ListFunctionUtils.listDaysFiles(context, ListFunctionUtils.LIST_DAY);
            } else if (mCurrentPath.equals(getResources().getString(R.string.category_largefile))) {
                return ListFunctionUtils.listHugeFiles(context, FileObserverService.NEW_FILE_SIZE_LIMIT_100MB);
            } else {
                return Utils.searchInDirectory(mCurrentPath, params[0]);
            }
        }

        @Override
        protected void onPostExecute(final ArrayList<FileItem> files) {
            int len = files != null ? files.size() : 0;
            if (pr_dialog != null && pr_dialog.isShowing()) {
                pr_dialog.dismiss();
            }
            if (len == 0) {
                Toast.makeText(mMainContext, getResources().getString(R.string.snackbar_search_result_empty), Toast.LENGTH_SHORT).show();
            } else {
                SortUtils.sortList(files, mCurrentPath, MyFileSettings.getSortType());
                mStoageAdapter.addContent(files);
                mStoageAdapter.notifyDataSetChanged();
            }
        }
    }
}
