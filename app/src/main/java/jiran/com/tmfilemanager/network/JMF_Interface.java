package jiran.com.tmfilemanager.network;

import com.google.gson.JsonObject;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;
import rx.Observable;

/**
 * Created by user on 2015-04-30.
 */
public interface JMF_Interface {

	/*
		User API
	 */

    /**
     * 회원가입 이메일 코드발송
     *
     * @param email
     * @param email_code
     * @return
     */
    @FormUrlEncoded
    @POST("user/join")
    Observable<JsonObject> JoinAndSendEmailCode(@Field("email") String email, @Field("email_code") String email_code);

    /**
     * 회원가입
     *
     * @param email_code
     * @param email
     * @param password
     * @param repassword
     * @param name
     * @return
     */
    @FormUrlEncoded
    @PUT("user/join")
    Observable<JsonObject> Join(@Field("email_code") String email_code, @Field("email") String email, @Field("password") String password, @Field("repassword") String repassword, @Field("name") String name);

    /**
     * 인증메일 재발송
     *
     * @param email
     * @return
     */
    @FormUrlEncoded
    @POST("user/join/resend")
    Observable<JsonObject> ResendEmail(@Field("email") String email);


    /**
     * 회원정보 조회
     *
     * @return
     */
    @GET("user")
    Observable<JsonObject> GetUserInfo();

    /**
     * 회원정보 수정
     *
     * @param name
     * @return
     */
    @FormUrlEncoded
    @PUT("user")
    Observable<JsonObject> ModifyUserInfo(@Field("name") String name);

    /**
     * 회원 탈퇴
     *
     * @return
     */
    @DELETE("user")
    Observable<JsonObject> LeaveMember();

    /**
     * 비밀번호 찾기
     *
     * @param email
     * @param name
     * @return
     */
    @POST("user/repassword")
    Observable<JsonObject> FindPassword(@Field("email") String email, @Field("name") String name);

    /**
     * 비밀번호 변경
     *
     * @param current_password
     * @param new_password
     * @param new_password_match
     * @return
     */
    @FormUrlEncoded
    @PUT("user/password")
    Observable<JsonObject> ChangePassword(@Field("current_password") String current_password, @Field("new_password") String new_password, @Field("new_password_match") String new_password_match);

    /**
     * 프로필수정
     *
     * @param filedata
     * @return
     */
    @Multipart
    @PUT("user/profile")
    Observable<JsonObject> UploadProfile(@Part List<MultipartBody.Part> filedata);


	/*
		Team API
	 */

    /**
     * 나의 팀 목록
     *
     * @return
     */
    @GET("team")
    Observable<JsonObject> GetTeamList();

    /**
     * 팀 생성
     *
     * @param name
     * @return
     */
    @FormUrlEncoded
    @POST("team")
    Observable<JsonObject> CreateTeam(@Field("name") String name);

    /**
     * 팀 정보 확인
     *
     * @param team_id
     * @return
     */
    @GET("team/{team_id}")
    Observable<JsonObject> GetTeamInfo(@Path("team_id") String team_id);

    /**
     * 팀 설정 변경
     *
     * @param team_id
     * @param options1
     * @param options2
     * @return
     */
    @FormUrlEncoded
    @PUT("team/{team_id}")
    Observable<JsonObject> ModifyTeamInfo(@Path("team_id") String team_id, @FieldMap Map<String, Integer> options1, @FieldMap Map<String, String> options2);
    //Observable<JsonObject> ModifyTeamInfo(@Path("team_id") String team_id, @Field("volume") String volume, @Field("join_rule") String join_rule, @Field("join_protect_domain") String join_protect_domain, @Field("flag_push") String flag_push, @Field("trash_option") String trash_option);

    /**
     * 팀 삭제
     *
     * @param team_id
     * @return
     */
    @DELETE("team/{team_id}")
    Observable<JsonObject> DeleteTeam(@Path("team_id") String team_id);

    /**
     * 가입할 팀 찾기
     *
     * @param name
     * @return
     */
    @FormUrlEncoded
    @POST("team/join")
    Observable<JsonObject> FindTeam(@Field("name") String name);

    /**
     * 팀 가입
     *
     * @param team_id
     * @return
     */
    @POST("team/join/{team_id}")
    Observable<JsonObject> JoinTeam(@Path("team_id") String team_id);

    /**
     * 팀 가입신청 메일발송
     *
     * @param team_id
     * @param memo
     * @return
     */
    @FormUrlEncoded
    @POST("team/join/{team_id}/request")
    Observable<JsonObject> JoinTeamSendEmail(@Path("team_id") String team_id, @Field("memo") String memo);

    /**
     * 팀원 목록 조회
     *
     * @param team_id
     * @return
     */
    @GET("team/member/{team_id}")
    Observable<JsonObject> GetTeamMemberList(@Path("team_id") String team_id);

    /**
     * 팀원 추가
     *
     * @param team_id
     * @param email
     * @return
     */
    @FormUrlEncoded
    @POST("team/member/{team_id}")
    Observable<JsonObject> AddTeamMember(@Path("team_id") String team_id, @Field("email") String email);

//	/**
//	 * 팀원 탈퇴
//	 *
//	 * @param team_id
//	 * @param team_member_id
//	 * @return
//	 */
//
//	@DELETE("team/member/{team_id}")
//	Observable<JsonObject> LeaveTeamMember(@Path("team_id") String team_id, @Field("team_member_id") String team_member_id);

    /**
     * 팀원 탈퇴
     *
     * @param team_id
     * @param team_member_id
     * @return
     */
    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "team/member/{team_id}", hasBody = true)
    Observable<JsonObject> LeaveTeamMember(@Path("team_id") String team_id, @Field("team_member_id") String team_member_id);

    /**
     * 팀원정보 확인
     *
     * @param team_id
     * @param team_member_id
     * @return
     */
    @GET("team/member/{team_id}/{team_member_id}")
    Observable<JsonObject> GetTeamMemberList(@Path("team_id") String team_id, @Path("team_member_id") String team_member_id);

    /**
     * 팀원 강제 탈퇴
     *
     * @param team_id
     * @param team_member_id
     * @return
     */

    @DELETE("team/member/{team_id}/{team_member_id}")
    Observable<JsonObject> DeleteTeamMember(@Path("team_id") String team_id, @Path("team_member_id") String team_member_id);

    /**
     * 팀 초대메일 재전송
     *
     * @param team_id
     * @param team_member_id
     * @return
     */
    @POST("team/member/{team_id}/{team_member_id}")
    Observable<JsonObject> ReSendJoinMail(@Path("team_id") String team_id, @Path("team_member_id") String team_member_id);

    /**
     * 팀원 가입 승인
     *
     * @param team_id
     * @param team_member_id
     * @return
     */
    @FormUrlEncoded
    @PUT("team/member/{team_id}")
    Observable<JsonObject> WaitMemberConfirm(@Path("team_id") String team_id, @Field("team_member_id") String team_member_id);


	/*
		Object API
	 */

    /**
     * 루트 디렉토리 조회
     *
     * @param team_id
     * @return
     */
    @GET("object/{team_id}")
    Observable<JsonObject> GetRootPath(@Path("team_id") String team_id);

    /**
     * 디렉토리 조회
     *
     * @param team_id
     * @param object_id
     * @param options
     * @return
     */
    @GET("object/{team_id}/{object_id}")
    Observable<JsonObject> GetFolderList(@Path("team_id") String team_id, @Path("object_id") String object_id, @QueryMap Map<String, String> options);

    /**
     * 하위 디렉토리 생성
     *
     * @param team_id
     * @param object_id
     * @param name
     * @return
     */
    @FormUrlEncoded
    @POST("object/{team_id}/{object_id}")
    Observable<JsonObject> CreateFolder(@Path("team_id") String team_id, @Path("object_id") String object_id, @Field("name") String name);

    /**
     * 디렉토리 정보 확인
     *
     * @param team_id
     * @param object_id
     * @return
     */
    @GET("object/dir/{team_id}/{object_id}")
    Observable<JsonObject> GetFolderInfo(@Path("team_id") String team_id, @Path("object_id") String object_id);

    /**
     * 디렉토리 이름 변경
     *
     * @param team_id
     * @param object_id
     * @param name
     * @return
     */
    @FormUrlEncoded
    @PUT("object/dir/{team_id}/{object_id}")
    Observable<JsonObject> ModifyFolderName(@Path("team_id") String team_id, @Path("object_id") String object_id, @Field("name") String name);

    /**
     * 디렉토리 삭제
     *
     * @param team_id
     * @param object_id
     * @return
     */
    @DELETE("object/dir/{team_id}/{object_id}")
    Observable<JsonObject> DeleteFolder(@Path("team_id") String team_id, @Path("object_id") String object_id);

    /**
     * 파일 정보 확인
     *
     * @param team_id
     * @param object_id
     * @return
     */
    @GET("object_detail/{team_id}/{object_id}")
    Observable<JsonObject> GetFileInfo(@Path("team_id") String team_id, @Path("object_id") String object_id);

    /**
     * 파일 이름 변경
     *
     * @param team_id
     * @param object_id
     * @param name
     * @return
     */
    @FormUrlEncoded
    @PUT("object_detail/{team_id}/{object_id}")
    Observable<JsonObject> ModifyFileName(@Path("team_id") String team_id, @Path("object_id") String object_id, @Field("name") String name);

//	/**
//	 * 파일 삭제
//	 *
//	 * @param team_id
//	 * @param object_id
//	 * @return
//	 */
//	@DELETE("object/file/{team_id}/{object_id}")
//	Observable<JsonObject> DeleteFile(@Path("team_id") String team_id, @Path("object_id") String object_id);

    /**
     * 파일 삭제
     *
     * @param team_id
     * @param object_id
     * @return
     */
    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "object_task/{team_id}", hasBody = true)
    Observable<JsonObject> DeleteFile(@Path("team_id") String team_id, @Field("object_id") String object_id);

    /**
     * 파일 다운로드
     *
     * @param team_id
     * @param object_id
     * @return
     */
    @GET("object_detail/{team_id}/{object_id}/download")
    @Streaming
    Observable<ResponseBody> Filedownload(@Path("team_id") String team_id, @Path("object_id") String object_id);

    /**
     * 파일업로드
     *
     * @param team_id
     * @param object_id
     * @param filedata  //* @param filedata
     * @return
     */
    @Multipart
    @POST("object/{team_id}/{object_id}/upload")
    Observable<JsonObject> FileUpload(@Path("team_id") String team_id, @Path("object_id") String object_id, @Part List<MultipartBody.Part> filedata);
    //Observable<JsonObject> FileUpload(@Path("team_id") String team_id, @Path("object_id") String object_id, @Part MultipartBody.Part file);//, @Part("filedata") RequestBody filedata);

    /**
     * 팀 내 복사
     *
     * @param team_id
     * @param object_id
     * @param target_id
     * @return
     */
    @FormUrlEncoded
    @POST("object_task/{team_id}")
    Observable<JsonObject> CopyTeamFile(@Path("team_id") String team_id, @Field("object_id") String object_id, @Field("target_id") String target_id);

    /**
     * 팀 내 이동
     *
     * @param team_id
     * @param object_id
     * @param target_id
     * @return
     */
    @FormUrlEncoded
    @PUT("object_task/{team_id}")
    Observable<JsonObject> MoveTeamFile(@Path("team_id") String team_id, @Field("object_id") String object_id, @Field("target_id") String target_id);


    /**
     * 팀 간 복사
     *
     * @param source_team_id
     * @param source_id
     * @param target_team_id
     * @param target_id
     * @return
     */
    @FormUrlEncoded
    @POST("object_task")
    Observable<JsonObject> CopyOtherTeamFile(@Field("source_team_id") String source_team_id, @Field("source_id") String source_id, @Field("target_team_id") String target_team_id, @Field("target_id") String target_id);

    /**
     * 팀 간 이동
     *
     * @param source_team_id
     * @param source_id
     * @param target_team_id
     * @param target_id
     * @return
     */
    @FormUrlEncoded
    @PUT("object_task")
    Observable<JsonObject> MoveOtherTeamFile(@Field("source_team_id") String source_team_id, @Field("source_id") String source_id, @Field("target_team_id") String target_team_id, @Field("target_id") String target_id);

    /**
     * 휴지통 조회
     *
     * @param team_id
     * @param options
     * @return
     */
    @GET("trash/{team_id}")
    Observable<JsonObject> GetTrashList(@Path("team_id") String team_id, @QueryMap Map<String, String> options);

    /**
     * 휴지통 복구
     *
     * @param team_id
     * @param objects
     * @return
     */
    @FormUrlEncoded
    @PUT("trash/{team_id}")
    Observable<JsonObject> TrashRecovery(@Path("team_id") String team_id, @Field("objects") String objects);

    /**
     * 휴지통 비우기
     *
     * @param team_id
     * @param objects
     * @return
     */
    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "trash/{team_id}", hasBody = true)
    Observable<JsonObject> TrashDelete(@Path("team_id") String team_id, @Field("objects") String objects);

	/*
		Login API
	 */

    /**
     * Token 발급
     *
     * @param email
     * @param password
     * @return
     */
    @FormUrlEncoded
    @POST("auth/token")
    Observable<JsonObject> TokenLogin(@Field("email") String email, @Field("password") String password);

    /**
     * Token 확인
     *
     * @param Authorization
     * @return
     */
    @GET("auth/resource")
    Observable<JsonObject> CheckToken(@Header("Authorization") String Authorization);

    /**
     * Token 확인
     *
     * @return
     */
    @GET("auth/resource")
    Observable<JsonObject> CheckToken();

    /**
     * Token 재발급
     *
     * @return
     */
    @GET("auth/refresh")
    Observable<JsonObject> RefreshToken();


    /**
     * 로그인
     *
     * @param email
     * @param password
     * @return
     */
    @FormUrlEncoded
    @POST("auth/login")
    Observable<JsonObject> Login(@Field("email") String email, @Field("password") String password);

    /**
     * 로그아웃
     *
     * @return
     */
    @GET("auth/logout")
    Observable<JsonObject> Logout();


    /**
     * 회원 대쉬보드
     *
     * @return
     */
    @GET("./")
    Observable<JsonObject> GetHomeData();

//
//	/**
//	 * 폴더 리스트를 불러 온다,
//	 *
//	 * @param folder_id
//	 * @param sort
//	 * @return
//	 */
//	@FormUrlEncoded
//	@POST("app/item/lists/{folder_id}")
//	Observable<JsonObject> GetFolderList(@Path("folder_id") String folder_id, @Field("sort") String sort);
//
//	/**
//	 * 특정 폴더의 이미지 리스트를 불러온다.
//	 *
//	 * @param folder_id
//	 * @param limit
//	 * @return
//	 */
//	@GET("app/image/lists/{folder_id}")
//	Observable<JsonObject> GetBoxImageList(@Path("folder_id") String folder_id, @Query("limit") String limit);
//
//	/**
//	 * 파일 업로드
//	 *
//	 * @return
//	 */
//	@Multipart
//	@POST("app/item/upload")
//	Observable<JsonObject> FileUpload(@PartMap Map<String, RequestBody> params);
////    Observable<JsonObject> FileUpload(@Part("node") String node, @Part("uploadkey") String uploadkey, @Part("Filedata") RequestBody typedFile);
//
//	/**
//	 * 파일 다운로드
//	 *
//	 * @param file_id
//	 * @return
//	 */
//	@GET("app/item/download/{file_id}")
//	@Streaming
//	Observable<ResponseBody> downloadfile(@Path("file_id") String file_id);
//
//	/**
//	 * 파일 다운로드
//	 *
//	 * @param file_id
//	 * @return
//	 */
//	@GET("app/item/image/{file_id}/1000")
//	@Streaming
//	Observable<ResponseBody> downloadimagefile(@Path("file_id") String file_id);
//
//
//	/**
//	 * 특정 폴더 및 파일 복사
//	 *
//	 * @param folder_id
//	 * @param method
//	 * @param node
//	 * @param nodes
//	 * @param fids
//	 * @param replace
//	 * @return
//	 */
//	@FormUrlEncoded
//	@POST("app/item/copy/{folder_id}")
//	Observable<JsonObject> CopyAndMove(@Path("folder_id") String folder_id, @Field("method") String method, @Field("node") String node, @Field("nodes") String nodes, @Field("fids") String fids, @Field("replace") String replace);
//
//
//	/**
//	 * 특정 폴더 및 파일 삭제
//	 *
//	 * @param folder_id
//	 * @param nodes
//	 * @param fids
//	 * @return
//	 */
//	@FormUrlEncoded
//	@POST("app/item/delete/{folder_id}")
//	Observable<JsonObject> delete(@Path("folder_id") String folder_id, @Field("nodes") String nodes, @Field("fids") String fids);
//
//	/**
//	 * 폴더 생성
//	 *
//	 * @param node
//	 * @param auto
//	 * @param name
//	 * @return
//	 */
//	@FormUrlEncoded
//	@POST("app/item/folder/add")
//	Observable<JsonObject> addFolder(@Field("node") String node, @Field("auto") String auto, @Field("name") String name);
//
//	/**
//	 * Link 설정값
//	 *
//	 * @return
//	 */
//	@GET("app/link/setting/")
//	Observable<JsonObject> linkSettingValue();
//
//	/**
//	 * Link 생성
//	 *
//	 * @return
//	 */
//	@FormUrlEncoded
//	@POST("app/link/edit/")
//	Observable<JsonObject> makeLink(@Field("linkid") String linkid, @Field("type") String type, @Field("fid") String fid,
//                                    @Field("node") String node, @Field("password") String password, @Field("expdate_unlimited") String expdate_unlimited,
//                                    @Field("expdate") String expdate, @Field("count") String count);
//
//
//	/**
//	 * 비밀번호 변경
//	 *
//	 * @param confirm_password
//	 * @param new_password
//	 * @param verify_password
//	 * @return
//	 */
//	@FormUrlEncoded
//	@POST("app/user/editpassword")
//	Observable<JsonObject> editPassword(@Field("confirm_password") String confirm_password, @Field("new_password") String new_password, @Field("verify_password") String verify_password);
//
//
//
//	/**
//	 * Storage List
//	 *
//	 * @return
//	 */
//	@GET("app/storage/getlist")
//	Observable<JsonObject> storageList();
//
//
//	/**
//	 * Storage List
//	 * @param path
//	 * @return
//	 */
//	@GET("app/storage/getlist")
//	Observable<JsonObject> storageList(@Query("reqtype") int reqtype, @Query("path") String path);
//
//
//	/**
//	 * Storage List
//	 * @param path
//	 * @return
//	 */
//	@GET("app/storage/download")
//	Observable<JsonObject> storageDownload(@Query("reqtype") int reqtype, @Query("path") String path);
//
//	/**
//	 * Storage Auth List
//	 *
//	 * @return
//	 */
//	@GET("app/storage/lists")
//	Observable<JsonObject> storageAuthLis();
//
//	/**
//	 * Storage Login info Reg
//	 * String str = id + "{GS}" + pw;
//	 * String base64 = Base64.encodeToString(str.getBytes(), Base64.DEFAULT);
//	 *
//	 * @param str
//	 * @param path
//	 * @return
//	 */
//	@FormUrlEncoded
//	@POST("app/storage/auth/reg")
//	Observable<JsonObject> storageLoginInfoReg(@Field("str") String str, @Field("path") String path);
//
//	/**
//	 * Storage Login info Del
//	 *
//	 * @param path
//	 * @return
//	 */
//	@FormUrlEncoded
//	@POST("app/storage/auth/del")
//	Observable<JsonObject> storageLoginInfoDel(@Field("path") String path);
//
//
//	/**
//	 * 데스크탑 최상위 드라이브 호출
//	 *
//	 * @return
//	 */
//	@GET("app/dir/getlist")
//	Observable<JsonObject> GetDesktopFirstList();
//
//	/**
//	 * 데스크탑 드라이브 하위 파일 리스트 호출
//     * @param reqtype
//     * @param socketid
//	 * @param path
//	 * @return
//	 */
//	@GET("app/dir/getlist")
//	Observable<JsonObject> GetDesktopList(@Query("reqtype") int reqtype, @Query("socketid") String socketid, @Query("path") String path);
//
//	/**
//	 * 데스크탑 다운로드
//	 * @param path
//	 * @return
//	 */
//	@GET("app/dir/download")
//	Observable<JsonObject> desktopDownload(@Query("reqtype") int reqtype, @Query("socketid") String socketid, @Query("path") String path);
//
//
//	/**
//	 * 검색 리스트를 불러 온다,
//	 *
//	 * @param path
//	 * @param keyword
//	 * @param sort
//	 * @param listcount
//	 * @return
//	 */
//	@FormUrlEncoded
//	@POST("app/item/search/{path}")
//	Observable<JsonObject> getSearchList(@Path("path") String path, @Field("keyword") String keyword, @Field("sort") String sort, @Field("listcount") String listcount);
//
//    /**
//     * 검색 리스트를 불러 온다,
//     *
//     * @param path
//     * @param keyword
//     * @param sort
//     * @param listcount
//     * @return
//     */
//    @FormUrlEncoded
//    @POST("app/item/search/{path}")
//    Observable<JsonObject> getSearchMyList(@Path("path") String path, @Field("keyword") String keyword, @Field("sort") String sort, @Field("listcount") String listcount);
//
//
}
