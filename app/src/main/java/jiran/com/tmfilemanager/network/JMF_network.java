package jiran.com.tmfilemanager.network;


import android.support.v4.util.Pair;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jiran.com.tmfilemanager.network.updown.DiskUtils;
import jiran.com.tmfilemanager.network.updown.DownloadProgressListener;
import jiran.com.tmfilemanager.utils.rx.RxUtils;
import okhttp3.MultipartBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by user on 2016-10-06.
 */

public class JMF_network {

    /*
		User API
	 */

    public static JsonObject getErrorJsonObject(Throwable throwable) {
        JsonObject errorObj = null;
        if (throwable instanceof HttpException) {
            HttpException exception = (HttpException) throwable;
            try {
                String json = exception.response().errorBody().string();
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                JsonElement jelem = gson.fromJson(json, JsonElement.class);
                errorObj = jelem.getAsJsonObject();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return errorObj;
    }

    public static String getErrorMsg(Throwable throwable) {
        String errorMsg = "Network Error";
        if (throwable instanceof HttpException) {
            HttpException exception = (HttpException) throwable;
            try {
                String json = exception.response().errorBody().string();
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                JsonElement jelem = gson.fromJson(json, JsonElement.class);
                JsonObject jobj = jelem.getAsJsonObject();
                errorMsg = jobj.get("all").getAsString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return errorMsg;
//        if (throwable instanceof HttpException) {
//            HttpException exception = (HttpException) throwable;
//            String errMsg;
//            try {
//                errMsg = exception.response().errorBody().string();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            switch (exception.code()) {
//                case 400:
//                    // Handle code 400
//                    errMsg = MyApplication.getInstance().getResources().getString(R.string.dialog_sign_up_exist_email);
//                    break;
//                case 500:
//                    // Handle code 500
//                    break;
//                default:
//                    break;
//            }
//        }
    }

    //회원가입 이메일 코드발송
    public static Observable<Pair<Boolean, Object>> joinAndSendEmailCode(String email, String email_code) throws Exception {
        if (email.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getNewInterface().JoinAndSendEmailCode(email, email_code)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //회원가입
    public static Observable<Pair<Boolean, Object>> join(String email_code, String email, String password, String repassword, String name) throws Exception {
        if (email_code.equals("") || email.equals("") || password.equals("") || repassword.equals("") || name.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().Join(email_code, email, password, repassword, name)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //인증메일 재발송
    public static Observable<Pair<Boolean, Object>> resendEmail(String email) throws Exception {
        if (email.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().ResendEmail(email)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //회원정보 조회
    public static Observable<Pair<Boolean, Object>> getUserInfo() throws Exception {
        return JMF_RestfulAdapter.getInterface().GetUserInfo()
                .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                .map(jsonObject -> {
                    if (jsonObject.get("success").getAsBoolean()) {
                        return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                    } else {
                        return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                    }
                })
                .onErrorResumeNext(throwable -> {
                    throwable.printStackTrace();
                    Log.i("TESTTEST", throwable.toString());
                    return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                })
                .subscribeOn(Schedulers.io());
    }

    //회원정보 수정
    public static Observable<Pair<Boolean, Object>> modifyUserInfo(String name) throws Exception {
        if (name.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().ModifyUserInfo(name)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //회원탈퇴
    public static Observable<Pair<Boolean, Object>> leaveMember() throws Exception {
        return JMF_RestfulAdapter.getInterface().LeaveMember()
                .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                .map(jsonObject -> {
                    if (jsonObject.get("success").getAsBoolean()) {
                        return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                    } else {
                        return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                    }
                })
                .onErrorResumeNext(throwable -> {
                    throwable.printStackTrace();
                    Log.i("TESTTEST", throwable.toString());
                    return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                })
                .subscribeOn(Schedulers.io());
    }

    //비밀번호 찾기
    public static Observable<Pair<Boolean, Object>> findPassword(String email, String name) throws Exception {
        if (email.equals("") || name.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().FindPassword(email, name)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //비밀번호 변경
    public static Observable<Pair<Boolean, Object>> changePassword(String current_password, String new_password, String new_password_match) throws Exception {
        if (current_password.equals("") || new_password.equals("") || new_password_match.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().ChangePassword(current_password, new_password, new_password_match)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //프로필 사진 업로드
    public static Observable<Pair<Boolean, Object>> uploadProfile(List<MultipartBody.Part> body) throws Exception {
        if (body == null) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().UploadProfile(body)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

   	/*
		Team API
	 */


    //나의 팀 목록
    public static Observable<Pair<Boolean, Object>> getTeamList() throws Exception {
        return JMF_RestfulAdapter.getInterface().GetTeamList()
                .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                .map(jsonObject -> {
                    if (jsonObject.get("success").getAsBoolean()) {
                        return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                    } else {
                        return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                    }
                })
                .onErrorResumeNext(throwable -> {
                    throwable.printStackTrace();
                    Log.i("TESTTEST", throwable.toString());
                    return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                })
                .subscribeOn(Schedulers.io());
    }

    //팀 생성
    public static Observable<Pair<Boolean, Object>> createTeam(String name) throws Exception {
        if (name.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().CreateTeam(name)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //팀 정보 확인
    public static Observable<Pair<Boolean, Object>> getTeamInfo(String team_id) throws Exception {
        if (team_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().GetTeamInfo(team_id)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //팀 설정 변경
    public static Observable<Pair<Boolean, Object>> ModifyTeamInfo(String team_id, int volume, String join_rule, String join_protect_domain, String flag_push, int trash_option) throws Exception {
        if (team_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            Map<String, Integer> params1 = new HashMap<String, Integer>();
            Map<String, String> params2 = new HashMap<String, String>();

            if (volume != -1) {
                params1.put("volume", volume);
            }
            if (join_rule != null & !join_rule.equals("0")) {
                params2.put("join_rule", join_rule);
            }
            if (join_protect_domain != null && !join_protect_domain.equals("0")) {
                params2.put("join_protect_domain", join_protect_domain);
            }
            if (flag_push != null && !flag_push.equals("0")) {
                params2.put("flag_push", flag_push);
            }
            if (trash_option != 0) {
                params2.put("trash_option", trash_option + "");
            }
            return JMF_RestfulAdapter.getInterface().ModifyTeamInfo(team_id, params1, params2)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //팀 삭제
    public static Observable<Pair<Boolean, Object>> deleteTeam(String team_id) throws Exception {
        if (team_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().DeleteTeam(team_id)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //가입할 팀 찾기
    public static Observable<Pair<Boolean, Object>> findTeam(String team_name) throws Exception {
        if (team_name.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().FindTeam(team_name)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        String errMsg = "Network Error";
                        if (throwable instanceof HttpException) {
                            HttpException exception = (HttpException) throwable;
                            switch (exception.code()) {
                                case 403:
                                    // Handle code 400
                                    errMsg = "403";
                                    break;
                                case 404:
                                    // Handle code 400
                                    errMsg = "404";
                                    break;
                                case 500:
                                    // Handle code 500
                                    break;
                                default:
                                    break;
                            }
                        }
                        return Observable.just(new Pair<>(false, errMsg));//getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //팀 가입
    public static Observable<Pair<Boolean, Object>> joinTeam(String team_id) throws Exception {
        if (team_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().JoinTeam(team_id)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //팀 가입신청 메일발송
    public static Observable<Pair<Boolean, Object>> joinTeamSendEmail(String team_id, String memo) throws Exception {
        if (team_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().JoinTeamSendEmail(team_id, memo)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //팀원 목록 조회
    public static Observable<Pair<Boolean, Object>> getTeamMemberList(String team_id) throws Exception {
        if (team_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().GetTeamMemberList(team_id)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //팀원 추가
    public static Observable<Pair<Boolean, Object>> addTeamMember(String team_id, String email) throws Exception {
        if (team_id.equals("") || email.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().AddTeamMember(team_id, email)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //팀원 탈퇴
    public static Observable<Pair<Boolean, Object>> leaveTeamMember(String team_id, String team_member_id) throws Exception {
        if (team_id.equals("") || team_member_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().LeaveTeamMember(team_id, team_member_id)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //팀원정보 확인
    public static Observable<Pair<Boolean, Object>> getTeamMemberList(String team_id, String team_member_id) throws Exception {
        if (team_id.equals("") || team_member_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().GetTeamMemberList(team_id, team_member_id)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //팀원 강제 탈퇴
    public static Observable<Pair<Boolean, Object>> deleteTeamMember(String team_id, String team_member_id) throws Exception {
        if (team_id.equals("") || team_member_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().DeleteTeamMember(team_id, team_member_id)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //팀 초대메일 재전송
    public static Observable<Pair<Boolean, Object>> reSendJoinMail(String team_id, String team_member_id) throws Exception {
        if (team_id.equals("") || team_member_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().ReSendJoinMail(team_id, team_member_id)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //팀원 가입 승인
    public static Observable<Pair<Boolean, Object>> waitMemberConfirm(String team_id, String team_member_id) throws Exception {
        if (team_id.equals("") || team_member_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().WaitMemberConfirm(team_id, team_member_id)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    /*
		Object API
	 */

    //루트 디렉토리 조회
    public static Observable<Pair<Boolean, Object>> getRootPath(String team_id) throws Exception {
        if (team_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().GetRootPath(team_id)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //디렉토리 조회
//    sort	String	N	디록토리 및 파일의 정렬값.(이름:+/-name, 날짜:+/-regdate, 용량:+/-size, 확장자: +/-extension)
//    limit	String	N	한번에 조회할 갯수.(기본값: 100)
//    offset	String	N	조회에서 제외할 갯수.(기본값: 0)
//    assort	String	N	모아보기 옵션(application, picture, movie, music, document, archive)
    public static Observable<Pair<Boolean, Object>> getFolderList(String team_id, String object_id, String sort, String limit, String offset, String assort) throws Exception {
        return getFolderList(team_id, object_id, sort, limit, offset, assort, "");
    }

    //디렉토리 조회
//    sort	String	N	디록토리 및 파일의 정렬값.(이름:+/-name, 날짜:+/-regdate, 용량:+/-size, 확장자: +/-extension)
//    limit	String	N	한번에 조회할 갯수.(기본값: 100)
//    offset	String	N	조회에서 제외할 갯수.(기본값: 0)
//    assort	String	N	모아보기 옵션(application, picture, movie, music, document, archive)
    public static Observable<Pair<Boolean, Object>> getFolderList(String team_id, String object_id, String sort, String limit, String offset, String assort, String keyword) throws Exception {
        if (team_id.equals("") || object_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            Map<String, String> params = new HashMap<String, String>();
            if (sort != null && sort.length() > 0) {
                params.put("sort", sort);
            } else {
                params.put("sort", "");
            }
            if (limit != null && limit.length() > 0) {
                params.put("limit", limit);
            } else {
                params.put("limit", "");
            }
            if (offset != null && offset.length() > 0) {
                params.put("offset", offset);
            } else {
                params.put("offset", "");
            }
            if (assort != null && assort.length() > 0) {
                params.put("assort", assort);
            } else {
                params.put("assort", "");
            }

            if (keyword != null && keyword.length() > 0) {
                params.put("keyword", keyword);
            } else {
                params.put("keyword", "");
            }

            return JMF_RestfulAdapter.getInterface().GetFolderList(team_id, object_id, params)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //하위 디렉토리 생성
    public static Observable<Pair<Boolean, Object>> createFolder(String team_id, String object_id, String name) throws Exception {
        if (team_id.equals("") || object_id.equals("") || name.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().CreateFolder(team_id, object_id, name)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

//    //디렉토리 정보 확인
//    public static Observable<Pair<Boolean, Object>> getFolderInfo(String team_id, String object_id) throws  Exception{
//        if (team_id.equals("") || object_id.equals("")) {
//            return Observable.just(new Pair<>(false, new JsonObject()));
//        } else {
//            return JMF_RestfulAdapter.getInterface().GetFolderInfo(team_id, object_id)
//                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
//                    .map(jsonObject -> {
//                        if (jsonObject.get("success").getAsBoolean()) {
//                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
//                        } else {
//                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
//                        }
//                    })
//                    .onErrorResumeNext(throwable -> {
//                        throwable.printStackTrace();
//                        Log.i("TESTTEST", throwable.toString());
//                        return Observable.just(new Pair<>(false, "Network Error"));
//                    } )
//                    .subscribeOn(Schedulers.io());
//        }
//    }
//
//    //디렉토리 이름 변경
//    public static Observable<Pair<Boolean, Object>> modifyFolderName(String team_id, String object_id, String name) throws  Exception{
//        if (team_id.equals("") || object_id.equals("") || name.equals("")) {
//            return Observable.just(new Pair<>(false, new JsonObject()));
//        } else {
//            return JMF_RestfulAdapter.getInterface().ModifyFolderName(team_id, object_id, name)
//                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
//                    .map(jsonObject -> {
//                        if (jsonObject.get("success").getAsBoolean()) {
//                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
//                        } else {
//                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
//                        }
//                    })
//                    .onErrorResumeNext(throwable -> {
//                        throwable.printStackTrace();
//                        Log.i("TESTTEST", throwable.toString());
//                        return Observable.just(new Pair<>(false, "Network Error"));
//                    } )
//                    .subscribeOn(Schedulers.io());
//        }
//    }
//
//    //디렉토리 삭제
//    public static Observable<Pair<Boolean, Object>> deleteFolder(String team_id, String object_id) throws  Exception{
//        if (team_id.equals("") || object_id.equals("")) {
//            return Observable.just(new Pair<>(false, new JsonObject()));
//        } else {
//            return JMF_RestfulAdapter.getInterface().DeleteFolder(team_id, object_id)
//                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
//                    .map(jsonObject -> {
//                        if (jsonObject.get("success").getAsBoolean()) {
//                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
//                        } else {
//                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
//                        }
//                    })
//                    .onErrorResumeNext(throwable -> {
//                        throwable.printStackTrace();
//                        Log.i("TESTTEST", throwable.toString());
//                        return Observable.just(new Pair<>(false, "Network Error"));
//                    } )
//                    .subscribeOn(Schedulers.io());
//        }
//    }

    //파일 정보 확인
    public static Observable<Pair<Boolean, Object>> getFileInfo(String team_id, String object_id) throws Exception {
        if (team_id.equals("") || object_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().GetFileInfo(team_id, object_id)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //파일 이름 변경
    public static Observable<Pair<Boolean, Object>> modifyFileName(String team_id, String object_id, String name) throws Exception {
        if (team_id.equals("") || object_id.equals("") || name.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().ModifyFileName(team_id, object_id, name)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //파일 삭제
    public static Observable<Pair<Boolean, Object>> deleteFile(String team_id, String object_id) throws Exception {
        if (team_id.equals("") || object_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().DeleteFile(team_id, object_id)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //파일 다운로드
    public static Observable<Pair<Boolean, Object>> filedownload(String team_id, String object_id, String path, String name, DownloadProgressListener listener) throws Exception {
        if (team_id.equals("") || object_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().Filedownload(team_id, object_id)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(response -> {
                        String savePath = "";
                        try {
                            savePath = DiskUtils.saveInputStreamToDirectory(response, path, name, listener);
                        } catch (IOException e) {
                            e.printStackTrace();
                            return (Pair<Boolean, Object>) new Pair(false, "Disk Save Error...");
                        }
                        return (Pair<Boolean, Object>) new Pair(true, savePath);
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());

//            return JMF_RestfulAdapter.getInterface().Filedownload(team_id, object_id)
//                    .flatMap(response -> {
//                        return DirectDisplayManager.saveInputStreamToDirectory(response, downloadPage);
//                    })
//                    .doOnCompleted(() -> {
//                        //Log.d("파일 다운로드 오나료?? doOnCompleted");
//                        //EventBus.getDefault().post(new DownloadChapterQueryEvent(DownloadUtils.FLAG_RUNNING, ""));
//                    })
//                    .doOnError(throwable -> {
//                        //Dlog.d("downloadChapterFromNetwork_officebox: 여기 들어오나?");
//                        throwable.printStackTrace();
//                    });
        }
    }

    //FileUpload
    //public static Observable<Pair<Boolean, Object>> fileUpload(String team_id, String object_id, Map<String, File> fileData) throws  Exception{
    public static Observable<Pair<Boolean, Object>> fileUpload(String team_id, String object_id, List<MultipartBody.Part> body) throws Exception {
        if (team_id.equals("") || object_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().FileUpload(team_id, object_id, body)//, fileData)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }


    //팀 내 복사
    public static Observable<Pair<Boolean, Object>> copyTeamFile(String team_id, String object_id, String target_id) throws Exception {
        if (team_id.equals("") || object_id.equals("") || target_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().CopyTeamFile(team_id, object_id, target_id)//, fileData)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //팀 내 이동
    public static Observable<Pair<Boolean, Object>> moveTeamFile(String team_id, String object_id, String target_id) throws Exception {
        if (team_id.equals("") || object_id.equals("") || target_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().MoveTeamFile(team_id, object_id, target_id)//, fileData)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //팀 간 복사
    public static Observable<Pair<Boolean, Object>> copyOtherTeamFile(String source_team_id, String source_id, String target_team_id, String target_id) throws Exception {
        if (source_team_id.equals("") || source_id.equals("") || target_team_id.equals("") || target_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().CopyOtherTeamFile(source_team_id, source_id, target_team_id, target_id)//, fileData)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //팀 간 이동
    public static Observable<Pair<Boolean, Object>> moveOtherTeamFile(String source_team_id, String source_id, String target_team_id, String target_id) throws Exception {
        if (source_team_id.equals("") || source_id.equals("") || target_team_id.equals("") || target_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().MoveOtherTeamFile(source_team_id, source_id, target_team_id, target_id)//, fileData)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }


    //휴지통 조회
//    sort	String	N	디록토리 및 파일의 정렬값.(이름:+/-name, 날짜:+/-regdate, 용량:+/-size, 확장자: +/-extension)
//    limit	String	N	한번에 조회할 갯수.(기본값: 100)
//    offset	String	N	조회에서 제외할 갯수.(기본값: 0)
    public static Observable<Pair<Boolean, Object>> getTrashList(String team_id, String sort, String limit, String offset) throws Exception {
        if (team_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            Map<String, String> params = new HashMap<String, String>();
            if (sort != null && sort.length() > 0) {
                params.put("sort", sort);
            } else {
                params.put("sort", "");
            }
            if (limit != null && limit.length() > 0) {
                params.put("limit", limit);
            } else {
                params.put("limit", "");
            }
            if (offset != null && offset.length() > 0) {
                params.put("offset", offset);
            } else {
                params.put("offset", "");
            }

            return JMF_RestfulAdapter.getInterface().GetTrashList(team_id, params)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //휴지통 복구
    public static Observable<Pair<Boolean, Object>> trashRecovery(String team_id, String objects) throws Exception {
        if (team_id.equals("") || objects.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().TrashRecovery(team_id, objects)//, fileData)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //휴지통 비우기
    public static Observable<Pair<Boolean, Object>> trashDelete(String team_id, String objects) throws Exception {
        if (team_id.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getInterface().TrashDelete(team_id, objects)//, fileData)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    /*
		Login API
	 */


    //Token 발급
    public static Observable<Pair<Boolean, Object>> tokenLogin(String email, String pw) throws Exception {
        if (email.equals("") || pw.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getNewInterface().TokenLogin(email, pw)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    //Token 확인
    public static Observable<Pair<Boolean, Object>> checkToken() throws Exception {
        return JMF_RestfulAdapter.getNewInterface().CheckToken()
                .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                .map(jsonObject -> {
                    if (jsonObject.get("success").getAsBoolean()) {
                        return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                    } else {
                        return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                    }
                })
                .onErrorResumeNext(throwable -> {
                    throwable.printStackTrace();
                    Log.i("TESTTEST", throwable.toString());
                    return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                })
                .subscribeOn(Schedulers.io());
    }

    //Token 재발급
    public static Observable<Pair<Boolean, Object>> refreshToken() throws Exception {
        return JMF_RestfulAdapter.getNewInterface().RefreshToken()
                .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                .map(jsonObject -> {
                    if (jsonObject.get("success").getAsBoolean()) {
                        return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                    } else {
                        return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                    }
                })
                .onErrorResumeNext(throwable -> {
                    throwable.printStackTrace();
                    Log.i("TESTTEST", throwable.toString());
                    return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                })
                .subscribeOn(Schedulers.io());
    }


    public static Observable<Pair<Boolean, Object>> login(String email, String pw) throws Exception {
        if (email.equals("") || pw.equals("")) {
            return Observable.just(new Pair<>(false, new JsonObject()));
        } else {
            return JMF_RestfulAdapter.getNewInterface().Login(email, pw)
                    .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                    .map(jsonObject -> {
                        if (jsonObject.get("success").getAsBoolean()) {
                            return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                        } else {
                            return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                        }
                    })
                    .onErrorResumeNext(throwable -> {
                        throwable.printStackTrace();
                        Log.i("TESTTEST", throwable.toString());
                        return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                    })
                    .subscribeOn(Schedulers.io());
        }
    }

    public static Observable<Pair<Boolean, Object>> logout() throws Exception {
        return JMF_RestfulAdapter.getInterface().Logout()
                .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                .map(jsonObject -> {
                    if (jsonObject.get("success").getAsBoolean()) {
                        return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                    } else {
                        return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                    }
                })
                .onErrorResumeNext(throwable -> {
                    throwable.printStackTrace();
                    Log.i("TESTTEST", throwable.toString());
                    return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                })
                .subscribeOn(Schedulers.io());
    }

    public static Observable<Pair<Boolean, Object>> getHomeData() throws Exception {
        return JMF_RestfulAdapter.getInterface().GetHomeData()
                .retryWhen(new RxUtils.RetryWithDelay(2, 1000))
                .map(jsonObject -> {
                    if (jsonObject.get("success").getAsBoolean()) {
                        return (Pair<Boolean, Object>) new Pair(true, jsonObject);
                    } else {
                        return (Pair<Boolean, Object>) new Pair(false, jsonObject.get("all").getAsString());
                    }
                })
                .onErrorResumeNext(throwable -> {
                    throwable.printStackTrace();
                    Log.i("TESTTEST", throwable.toString());
                    return Observable.just(new Pair<>(false, getErrorMsg(throwable)));
                })
                .subscribeOn(Schedulers.io());
    }


    private static String getExtension(String fileStr) {
        return fileStr.substring(fileStr.lastIndexOf(".") + 1, fileStr.length()).toLowerCase();
    }

    private static String sizeConverter(String size) {
        Float orgsize = Float.parseFloat(size);
        String fileSize = size;

        if (orgsize < 1024 * 1024) {
            fileSize = (orgsize / 1024) + "KB";
            fileSize = String.format("%.0fKB", (orgsize / 1024.0f));

        } else if (orgsize < 1024 * 1024 * 1024) {
            fileSize = (orgsize / 1024 / 1024) + "MB";
            fileSize = String.format("%.2fMB", (orgsize / 1024.0f / 1024.0f));
        } else {
            fileSize = (orgsize / 1024 / 1024 / 1024) + "GB";
            fileSize = String.format("%.2fGB", (orgsize / 1024.0f / 1024.0f / 1024.0f));
        }
        return fileSize;
    }
}
