package jiran.com.tmfilemanager.network.updown;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;

import com.google.gson.JsonObject;

import java.util.ArrayList;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.common.ClipBoard;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.network.JMF_network;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Namo on 2018-03-15.
 */

public class DownloadTask extends AsyncTask<String, Void, String> {

    DownloadProgressListener listener = new DownloadProgressListener() {
        @Override
        public void update(int percent, int done) {
            //start a new thread to process job
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    //heavy job here
//                    //send message to main thread
//                    handler.sendEmptyMessage(percent);
//                }
//            }).start();
            //setDownlaodProgress(percent);
            Log.i("DownloadProgressListener", "percent => " + percent + "%");
        }
    };
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            //setDownlaodProgress(msg.what);
        }
    };
    private Context mMainContext;
    private String mCurrentPath;
    private ArrayList<FileItem> arrayItems;
    private NotificationManager mNotifyManager;
    private Builder mBuilder;

    public DownloadTask(Context context, String path) {
        mMainContext = context;
        mCurrentPath = path;
    }

    @Override
    protected String doInBackground(String... params) {
        // ListFunctionUtils.listAllDocumrntFilesInit();

//        mNotifyManager = (NotificationManager) MyApplication.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);
//        mBuilder = new NotificationCompat.Builder(MyApplication.getInstance());
//        mBuilder.setContentTitle("Upload")
//                .setContentText("Uploading...")
//                .setSmallIcon(R.mipmap.ic_launcher);
//        mNotifyManager.notify(0, mBuilder.build());

        sendNotification("Downloading...", 0);

        try {
            String team_id = params[0];
            String object_id = params[1];
            String path = params[2];
            Log.i("JMF UploadTask", "team_id => " + team_id + " object_id => " + object_id);
            arrayItems = ClipBoard.getClipBoardContents();

            for (FileItem fileItem : arrayItems) {
                JMF_network.filedownload(team_id, object_id, mCurrentPath, fileItem.getName(), listener)
                        .flatMap(gigaPodPair -> {
                            return Observable.just(gigaPodPair);
                        })
                        .doOnError(throwable -> {

                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(gigaPodPair -> {
                                    if (gigaPodPair.first) {
                                        ClipBoard.clear();
                                        Log.i("JMF filedownload", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
//                                    mBuilder.setContentText("Upload complete");
//                                    // Removes the progress bar
//                                    mBuilder.setProgress(0, 0, false);
//                                    mNotifyManager.notify(0, mBuilder.build());
                                        sendNotification("Downloading complete.", 100);
                                    } else {
                                        //Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
//                                    mBuilder.setContentText("Upload Fail");
//                                    // Removes the progress bar
//                                    mBuilder.setProgress(0, 0, false);
//                                    mNotifyManager.notify(0, mBuilder.build());

                                        sendNotification("Downloading Fail.", 0);
                                    }
                                },
                                throwable -> {
//                                mBuilder.setContentText("Upload Fail");
//                                // Removes the progress bar
//                                mBuilder.setProgress(0, 0, false);
//                                mNotifyManager.notify(0, mBuilder.build());
                                    sendNotification("Downloading Fail.", 0);
                                    throwable.printStackTrace();
                                });
            }
        } catch (Exception e) {
//            mBuilder.setContentText("Upload Fail");
//            // Removes the progress bar
//            mBuilder.setProgress(0, 0, false);
//            mNotifyManager.notify(0, mBuilder.build());
            sendNotification("Downloading Fail.", 0);
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }

    public void sendNotification(String content, int percent) {
        Builder mBuilder =
                new Builder(mMainContext.getApplicationContext());
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.naver.com/"));
        PendingIntent pendingIntent = PendingIntent.getActivity(mMainContext, 0, intent, 0);

//        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
//        bigText.setBigContentTitle(mMainContext.getString(R.string.app_name));
//        bigText.setSummaryText(content);

        mBuilder.setContentIntent(pendingIntent);
        //mBuilder.setLargeIcon(BitmapFactory.decodeResource(mMainContext.getResources(), R.mipmap.ic_launcher));
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle(mMainContext.getString(R.string.app_name));
        mBuilder.setContentText(content);
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        //mBuilder.setStyle(bigText);

        NotificationManager mNotificationManager =
                (NotificationManager) mMainContext.getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("notify_001",
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);
        }

        mNotificationManager.notify(0, mBuilder.build());


//        NotificationCompat.Builder builder = new NotificationCompat.Builder(mMainContext);
//        builder.setSmallIcon(android.R.drawable.ic_dialog_alert);
//        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.naver.com/"));
//        PendingIntent pendingIntent = PendingIntent.getActivity(mMainContext, 0, intent, 0);
//        builder.setContentIntent(pendingIntent);
//        builder.setLargeIcon(BitmapFactory.decodeResource(mMainContext.getResources(), R.mipmap.ic_launcher));
//        builder.setContentTitle(mMainContext.getString(R.string.app_name));
//        builder.setContentText(content);
//        //builder.setSubText("Tap to view the website.");
//
//        NotificationManager notificationManager = (NotificationManager) mMainContext.getSystemService(NOTIFICATION_SERVICE);
//
//        // Will display the notification in the notification bar
//        notificationManager.notify(1, builder.build());


//        //Get an instance of NotificationManager//
//
//        NotificationCompat.Builder mBuilder =
//                new NotificationCompat.Builder(mMainContext)
//                        .setSmallIcon(R.mipmap.ic_launcher)
//                        .setContentTitle(title)
//                        .setContentText(content)
//                        .setProgress(100, percent, false);
//
//
//        // Gets an instance of the NotificationManager service//
//
//        NotificationManager mNotificationManager =
//
//                (NotificationManager) mMainContext.getSystemService(NOTIFICATION_SERVICE);
//
//        // When you issue multiple notifications about the same type of event,
//        // it’s best practice for your app to try to update an existing notification
//        // with this new information, rather than immediately creating a new notification.
//        // If you want to update this notification at a later date, you need to assign it an ID.
//        // You can then use this ID whenever you issue a subsequent notification.
//        // If the previous notification is still visible, the system will update this existing notification,
//        // rather than create a new one. In this example, the notification’s ID is 001//
//
//
//        mNotificationManager.notify(001, mBuilder.build());
    }
}
