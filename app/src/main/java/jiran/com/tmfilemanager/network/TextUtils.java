package jiran.com.tmfilemanager.network;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by dhawal sodha parmar on 4/24/2015.
 */
public class
TextUtils {

    /**
     * TEXT 된 파일 사이즈 변환
     *
     * @param size
     * @return
     */
    public static String sizeConverter(String size) {
        Float orgsize = Float.parseFloat(size);
        String fileSize = size;

        if (orgsize < 1024 * 1024) {

            fileSize = String.format("%.0fKB", (orgsize / 1024.0f));

        } else if (orgsize < 1024 * 1024 * 1024) {

            fileSize = String.format("%.2fMB", (orgsize / 1024.0f / 1024.0f));
        } else {

            fileSize = String.format("%.2fGB",
                    (orgsize / 1024.0f / 1024.0f / 1024.0f));
        }
        return fileSize;
    }

    public static String formatTimeString(String date_string) {
        //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat format = new SimpleDateFormat("yyyy. MM. dd. HH:mm");
        try {
            java.util.Date date;
            date = format.parse(date_string);
            return formatTimeString(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

    }

    public static String formatTimeString(java.util.Date tempDate) {

        long curTime = System.currentTimeMillis();
        long regTime = tempDate.getTime();
        long diffTime = (curTime - regTime) / 1000;

        String msg = null;
        if (diffTime < TIME_MAXIMUM.SEC) {
            // sec
            msg = "방금 전";
        } else if ((diffTime /= TIME_MAXIMUM.SEC) < TIME_MAXIMUM.MIN) {
            // min
            msg = diffTime + "분 전";
        } else if ((diffTime /= TIME_MAXIMUM.MIN) < TIME_MAXIMUM.HOUR) {
            // hour
            msg = (diffTime) + "시간 전";
        } else if ((diffTime /= TIME_MAXIMUM.HOUR) < TIME_MAXIMUM.DAY) {
            // day
            msg = (diffTime) + "일 전";
        } else if ((diffTime /= TIME_MAXIMUM.DAY) < TIME_MAXIMUM.MONTH) {
            // day

            msg = (diffTime) + "달 전";
        } else {
//            msg = (diffTime) + "년 전";

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

            msg = simpleDateFormat.format(tempDate);
        }

        return msg;
    }

    /**
     * 키보드 숨기기
     *
     * @param paramContext
     * @param paramView
     */
    public static void HideKeyboard(Context paramContext, View paramView) {
        InputMethodManager imm = (InputMethodManager) paramContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(paramView.getWindowToken(), 0);
    }

    /**
     * 키보드 보이기
     *
     * @param paramContext
     * @param paramView
     */
    public static void ShowKeyboard(Context paramContext, View paramView) {
        InputMethodManager imm = (InputMethodManager) paramContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(paramView, 0);
    }

    public static boolean isIPType(String Text) {
        if (Text.contains(".")) {
            return true;
        } else {
            return false;
        }
    }

    public static String getLanguage(Context ctx) {
        String LANGUAGE = "eng";
        if (ctx.getResources().getConfiguration().locale.getCountry().equals("KR"))
            LANGUAGE = "kor";
        else if (ctx.getResources().getConfiguration().locale.getCountry().equals("JP"))
            LANGUAGE = "jpn";
        else if (ctx.getResources().getConfiguration().locale.getCountry().equals("CH"))
            LANGUAGE = "chi";
        else
            LANGUAGE = "eng";

        return LANGUAGE;
    }

    private static class TIME_MAXIMUM {
        public static final int SEC = 60;
        public static final int MIN = 60;
        public static final int HOUR = 24;
        public static final int DAY = 30;
        public static final int MONTH = 2;
    }
}
