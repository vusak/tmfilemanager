package jiran.com.tmfilemanager.network.updown;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;

import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jiran.com.tmfilemanager.MyApplication;
import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.common.ClipBoard;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.network.JMF_network;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Namo on 2018-03-15.
 */

public class UploadTask extends AsyncTask<String, Void, String> {

    private Context mMainContext;
    private ListRefreshListener mListener;

    private NotificationManager mNotifyManager;
    private Builder mBuilder;

    public UploadTask() {
    }

    public UploadTask(Context context, ListRefreshListener listener) {
        mMainContext = context;
        mListener = listener;
    }

    @Override
    protected String doInBackground(String... params) {
        // ListFunctionUtils.listAllDocumrntFilesInit();

//        mNotifyManager = (NotificationManager) MyApplication.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);
//        mBuilder = new NotificationCompat.Builder(MyApplication.getInstance());
//        mBuilder.setContentTitle("Upload")
//                .setContentText("Uploading...")
//                .setSmallIcon(R.mipmap.ic_launcher);
//        mNotifyManager.notify(0, mBuilder.build());

        sendNotification("Uploading...", 0);


        try {
            String team_id = params[0];
            String object_id = params[1];
            Log.i("JMF UploadTask", "team_id => " + team_id + " object_id => " + object_id);
            ArrayList<FileItem> fileItems = ClipBoard.getClipBoardContents();

////            Map<String, File> fileMap = new HashMap<>();
////
//////            for(FileItem fileItem : fileItems)
//////            {
////                File file = new File(fileItems.get(0).getPath());
////                String mime = MimeTypes.getMimeType(file);
//////                Uri apkURI = null;
//////                try {
//////                    apkURI = FileProvider.getUriForFile(
//////                            context,
//////                            context.getApplicationContext()
//////                                    .getPackageName() + ".provider", target);
//////                }catch(Exception e){
//////                    mime = null;
//////                }
//////                if (mime != null) {
//////                    i.setDataAndType(apkURI, mime);
//////                } else {
//////                    i.setDataAndType(Uri.fromFile(target), "*/*");
//////                }
////
////                //RequestBody fileBody = RequestBody.create(MediaType.parse("image/*"), file);
////                RequestBody fileBody = RequestBody.create(MediaType.parse(mime), file);
////                fileMap.put("filedata", file);
//////            }
//
//
//            // Map is used to multipart the file using okhttp3.RequestBody
//            File file = new File(fileItems.get(0).getPath());
//
//            // Parsing any Media type file
//            RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
//            MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("filedata", file.getName(), requestBody);
//            RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());

            List<MultipartBody.Part> parts = new ArrayList();
            for (FileItem fileItem : fileItems) {
                File file = new File(fileItem.getPath());

                RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
                MultipartBody.Part part = MultipartBody.Part.createFormData("filedata", file.getName(), requestBody);
                parts.add(part);
            }

            JMF_network.fileUpload(team_id, object_id, parts)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {

                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                if (gigaPodPair.first) {
                                    Log.i("JMF fileUpload", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
//                                    mBuilder.setContentText("Upload complete");
//                                    // Removes the progress bar
//                                    mBuilder.setProgress(0, 0, false);
//                                    mNotifyManager.notify(0, mBuilder.build());
                                    sendNotification("Upload complete.", 100);

                                    if (ClipBoard.isMove() && mListener != null) {
                                        boolean success = true;
                                        ArrayList<FileItem> deleteItems = ClipBoard.getClipBoardContents();
                                        for (FileItem item : deleteItems) {
                                            File select = new File(item.getPath());
                                            if (select != null && select.exists()) {
                                                if (select.isDirectory()) {
                                                    success = deleteFile(select);
                                                    if (!success) {
                                                        break;
                                                    }
                                                    select.delete();
                                                } else {
                                                    if (select.delete()) {
                                                        MainActivity.mMds.deleteFile(item.getPath());
                                                    } else {
                                                        success = false;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        if (success) {
                                            mListener.refresh(fileItems);
                                        }
                                    }
                                    ClipBoard.clear();
                                } else {
                                    //Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
//                                    mBuilder.setContentText("Upload Fail");
//                                    // Removes the progress bar
//                                    mBuilder.setProgress(0, 0, false);
//                                    mNotifyManager.notify(0, mBuilder.build());

                                    sendNotification("Upload Fail.", 0);
                                }
                            },
                            throwable -> {
//                                mBuilder.setContentText("Upload Fail");
//                                // Removes the progress bar
//                                mBuilder.setProgress(0, 0, false);
//                                mNotifyManager.notify(0, mBuilder.build());
                                sendNotification("Upload Fail.", 0);
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
//            mBuilder.setContentText("Upload Fail");
//            // Removes the progress bar
//            mBuilder.setProgress(0, 0, false);
//            mNotifyManager.notify(0, mBuilder.build());
            sendNotification("Upload Fail.", 0);
            e.printStackTrace();
        }
        return null;
    }

    public boolean deleteFile(File file) {
        File[] fileList = file.listFiles();
        boolean success = true;
        for (File temp : fileList) {
            if (temp.isDirectory()) {
                success = deleteFile(temp);
                if (!success) {
                    break;
                }
                temp.delete();
            } else {
                if (temp.delete()) {
//                    ArrayList<FileItem> tempList = MainActivity.mMds.selectTagFiles(temp.getPath());
//                    for(FileItem sub : tempList){
//                        MainActivity.mMds.deleteFile(sub.getPath());
//                    }
                    MainActivity.mMds.deleteFile(temp.getPath());
                } else {
                    success = false;
                    break;
                }
            }
        }
        return success;
    }

    @Override
    protected void onPostExecute(String result) {
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }

    public void sendNotification(String content, int percent) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(mMainContext.getApplicationContext());
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.naver.com/"));
        PendingIntent pendingIntent = PendingIntent.getActivity(mMainContext, 0, intent, 0);

//        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
//        bigText.setBigContentTitle(mMainContext.getString(R.string.app_name));
//        bigText.setSummaryText(content);

        mBuilder.setContentIntent(pendingIntent);
        //mBuilder.setLargeIcon(BitmapFactory.decodeResource(mMainContext.getResources(), R.mipmap.ic_launcher));
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle(mMainContext.getString(R.string.app_name));
        mBuilder.setContentText(content);
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        //mBuilder.setStyle(bigText);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            mBuilder.setChannelId(MyApplication.NOTIFICATION_CHANNEL_ID);

        NotificationManager mNotificationManager =
                (NotificationManager) mMainContext.getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("notify_001",
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);
        }

        mNotificationManager.notify(0, mBuilder.build());


//        NotificationCompat.Builder builder = new NotificationCompat.Builder(mMainContext);
//        builder.setSmallIcon(android.R.drawable.ic_dialog_alert);
//        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.naver.com/"));
//        PendingIntent pendingIntent = PendingIntent.getActivity(mMainContext, 0, intent, 0);
//        builder.setContentIntent(pendingIntent);
//        builder.setLargeIcon(BitmapFactory.decodeResource(mMainContext.getResources(), R.mipmap.ic_launcher));
//        builder.setContentTitle(mMainContext.getString(R.string.app_name));
//        builder.setContentText(content);
//        //builder.setSubText("Tap to view the website.");
//
//        NotificationManager notificationManager = (NotificationManager) mMainContext.getSystemService(NOTIFICATION_SERVICE);
//
//        // Will display the notification in the notification bar
//        notificationManager.notify(1, builder.build());


//        //Get an instance of NotificationManager//
//
//        NotificationCompat.Builder mBuilder =
//                new NotificationCompat.Builder(mMainContext)
//                        .setSmallIcon(R.mipmap.ic_launcher)
//                        .setContentTitle(title)
//                        .setContentText(content)
//                        .setProgress(100, percent, false);
//
//
//        // Gets an instance of the NotificationManager service//
//
//        NotificationManager mNotificationManager =
//
//                (NotificationManager) mMainContext.getSystemService(NOTIFICATION_SERVICE);
//
//        // When you issue multiple notifications about the same type of event,
//        // it’s best practice for your app to try to update an existing notification
//        // with this new information, rather than immediately creating a new notification.
//        // If you want to update this notification at a later date, you need to assign it an ID.
//        // You can then use this ID whenever you issue a subsequent notification.
//        // If the previous notification is still visible, the system will update this existing notification,
//        // rather than create a new one. In this example, the notification’s ID is 001//
//
//
//        mNotificationManager.notify(001, mBuilder.build());
    }
}
