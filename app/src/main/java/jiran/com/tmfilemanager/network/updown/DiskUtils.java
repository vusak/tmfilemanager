package jiran.com.tmfilemanager.network.updown;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import jiran.com.tmfilemanager.network.TextUtils;
import okhttp3.ResponseBody;

/**
 * Created by Namo on 2018-03-14.
 */

public class DiskUtils {
    public static String saveInputStreamToDirectory(ResponseBody responseBody, String directory, String name, DownloadProgressListener listener) throws IOException {
        File fileDirectory = new File(directory);
        if (!fileDirectory.exists()) {
            if (!fileDirectory.mkdirs()) {
                throw new IOException("Failed Creating  Directory");
            }
        }

        File writeFile = new File(fileDirectory, name);
        if (writeFile.exists()) {
            if (writeFile.delete()) {
                writeFile = new File(fileDirectory, name);
            } else {
                throw new IOException("Failed Deleting Existing File for Overwrite");
            }
        }


        try {
            String totalFileSize;
            int count;
            //byte data[] = new byte[1024 * 4];
            byte data[] = new byte[1024];
            long fileSize = responseBody.contentLength();
            InputStream bis = new BufferedInputStream(responseBody.byteStream(), 1024 * 8);
            OutputStream output = new FileOutputStream(writeFile);
            long total = 0;
            long startTime = System.currentTimeMillis();
            int timeCount = 1;
            while ((count = bis.read(data)) != -1) {

                total += count;
                totalFileSize = TextUtils.sizeConverter(String.valueOf(fileSize));
                String current = TextUtils.sizeConverter(String.valueOf(total));

                int progress = (int) ((total * 100) / fileSize);
//                long currentTime = System.currentTimeMillis();
//
//                if (currentTime-startTime > 1000 * timeCount) {
//                    //EventBus.getDefault().post(new com.jiransoft.officedisplay.controllers.events.FileDownloadEvent(totalFileSize + "/ " + current, String.valueOf(progress)));
//                    if(listener != null){
//                        listener.update(progress, DownloadProgressListener.DOWNLOADING);
//                    }
//                    startTime = currentTime;
//                }
                if (listener != null) {
                    listener.update(progress, DownloadProgressListener.DOWNLOADING);
                }
                output.write(data, 0, count);
            }
            output.flush();
            output.close();
            bis.close();
            if (listener != null) {
                listener.update(100, DownloadProgressListener.SUCCESS);
            }

        } catch (Exception e) {
            if (listener != null) {
                listener.update(0, DownloadProgressListener.FAIL);
            }
            responseBody.close();
            throw e;
        }

        return writeFile.getAbsolutePath();
    }
}
