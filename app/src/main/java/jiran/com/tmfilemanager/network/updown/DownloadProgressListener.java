package jiran.com.tmfilemanager.network.updown;

/**
 * Created by Namo on 2018-03-15.
 */

public interface DownloadProgressListener {
    public static final int DOWNLOADING = 0, SUCCESS = -1, FAIL = -2;

    void update(int percent, int done);
}