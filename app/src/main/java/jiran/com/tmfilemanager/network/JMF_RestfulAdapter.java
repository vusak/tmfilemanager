package jiran.com.tmfilemanager.network;


import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import jiran.com.tmfilemanager.MyApplication;
import jiran.com.tmfilemanager.activity.LoginActivity;
import okhttp3.Interceptor;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by user on 2015-06-12.
 */
public class JMF_RestfulAdapter {
    public static final int TIMEOUT = 30;

    public static final int CONNECT_TIMEOUT = TIMEOUT;
    public static final int WRITE_TIMEOUT = TIMEOUT;
    public static final int READ_TIMEOUT = TIMEOUT;

    public static String JMF_URL = "";
    //public static String JMF_SERVER = "http://159.89.208.77";
    public static String JMF_SERVER = "https://api.teammobile.io";
    public static String JMF_VERSION = "v08";

    private static OkHttpClient client;
    private static JMF_Interface Interface;

//	public static CookieManager cookieManager;

    public synchronized static OkHttpClient getClient() {
        if (client == null) {
            client = makeOkHttpClient();
        }
        return client;
    }

    public synchronized static JMF_Interface getInterface() {
        if (Interface == null) {

            JMF_URL = JMF_SERVER + "/" + JMF_VERSION + "/";

            client = getClient();

            Interface = new Retrofit.Builder()
                    .baseUrl(JMF_URL)
                    .client(client)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build().create(JMF_Interface.class);
        }
        return Interface;
    }

    public synchronized static JMF_Interface getNewInterface() throws Exception {
        JMF_URL = JMF_SERVER + "/" + JMF_VERSION + "/";

        client = makeOkHttpClient();

        Interface = new Retrofit.Builder()
                .baseUrl(JMF_URL)
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(JMF_Interface.class);

        return Interface;
    }

    static synchronized OkHttpClient makeOkHttpClient() {
        PersistentCookieStore cookieStore = new PersistentCookieStore(MyApplication.getInstance());
        CookieManager cookieManager = new CookieManager(cookieStore, CookiePolicy.ACCEPT_ALL);

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        //httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC).setLevel
                (HttpLoggingInterceptor.Level.BODY).setLevel(HttpLoggingInterceptor.Level.HEADERS);

        OkHttpClient.Builder client = new OkHttpClient.Builder();

        //return configureClient(new OkHttpClient().newBuilder())
        return configureClient(client)
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .cookieJar(new JavaNetCookieJar(cookieManager))
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(new AddHeaderInterceptor())
                .build();
    }

    /**
     * UnCertificated 허용
     */
    public static OkHttpClient.Builder configureClient(final OkHttpClient.Builder builder) {
        final TrustManager[] certs = new TrustManager[]{new X509TrustManager() {

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                X509Certificate[] x509Certificates = new X509Certificate[0];
                return x509Certificates;
            }

            @Override
            public void checkServerTrusted(final X509Certificate[] chain,
                                           final String authType) {
            }

            @Override
            public void checkClientTrusted(final X509Certificate[] chain,
                                           final String authType) {
            }
        }};

        SSLContext ctx = null;
        try {
            ctx = SSLContext.getInstance("TLS");
            ctx.init(null, certs, new SecureRandom());
        } catch (final java.security.GeneralSecurityException ex) {
            ex.printStackTrace();
        }

        try {
            final HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                @Override
                public boolean verify(final String hostname, final SSLSession session) {
//                    Dlog.d( "hostname : " + hostname);
                    return true;
                }
            };

            builder.sslSocketFactory(ctx.getSocketFactory()).hostnameVerifier(hostnameVerifier);
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return builder;
    }

    public static String getThumbnailUrl(String id) {
        StringBuilder url = new StringBuilder();
        url.append(JMF_URL);
        url.append("/app/item/image/");
        url.append(id);
        url.append("/300");
        return url.toString();
    }

    public static String getOriginalUrl(String id) {
        StringBuilder url = new StringBuilder();
        url.append(JMF_URL);
        url.append("/app/item/download/");
        url.append(id);
        return url.toString();
    }

    private static final class AddHeaderInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException, java.io.IOException {
//			Request request = chain.request();
//
//			Headers.Builder hBuilder = new Headers.Builder();
//			hBuilder.add("Content-Type", "application/x-www-form-urlencoded");
//			hBuilder.add("Authorization", LoginActivity.mLoginToken);
//			Headers h = hBuilder.build();
//
//			request.newBuilder().headers(h).build();
//			return chain.proceed(request);

            Request request = chain.request();
            Request newRequest;

            newRequest = request.newBuilder()
                    //.addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .addHeader("Authorization", LoginActivity.mLoginToken)
                    .build();
            return chain.proceed(newRequest);
        }
    }
}