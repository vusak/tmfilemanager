package jiran.com.tmfilemanager.network.updown;

import java.util.ArrayList;

import jiran.com.tmfilemanager.data.FileItem;

/**
 * Created by Namo on 2018-03-15.
 */

public interface ListRefreshListener {
    void refresh(ArrayList<FileItem> items);
}