package jiran.com.tmfilemanager.common;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Message;
import android.os.StatFs;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v4.provider.DocumentFile;
import android.text.Spannable;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.activity.viewer.ImageSlideViewerActivity;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.fagment.SecretFolderFragment;
import jiran.com.tmfilemanager.settings.MyFileSettings;

import static android.os.Environment.getStorageState;

/**
 * Created by user on 2016-08-03.
 */
public class Utils {

    private static final int BUFFER = 16384;
    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String TEAM_NAME_PATTERN = "^[A-Za-z0-9]*$";

    public static boolean isSdMounted() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    /**
     * 전체 내장 메모리 크기를 가져온다
     */
    public static long getTotalSDCaredMemorySize() {
        //File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(getSDcardDirectoryPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();

        return totalBlocks * blockSize;
    }

    /**
     * 사용가능한 내장 메모리 크기를 가져온다
     */
    public static long getSDCardMemorySize() {
        //File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(getSDcardDirectoryPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();

        return availableBlocks * blockSize;
    }

    /**
     * 전체 외장 메모리 크기를 가져온다
     */
    public static long getTotalExternalMemorySize() {
        if (isStorage(true) == true) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long totalBlocks = stat.getBlockCount();

            return totalBlocks * blockSize;
        } else {
            return -1;
        }
    }

    /**
     * 사용가능한 외장 메모리 크기를 가져온다
     */
    public static long getExternalMemorySize() {
        if (isStorage(true) == true) {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long availableBlocks = stat.getAvailableBlocks();
            return availableBlocks * blockSize;
        } else {
            return -1;
        }
    }

    /**
     * 서버 시간을 일반 시간으로 변환
     */
    public static String formatDate2(String oriDate) {
        String DATE_FORMAT_I = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        String DATE_FORMAT_O = "yyyy. MM. dd. ";

        SimpleDateFormat formatInput = new SimpleDateFormat(DATE_FORMAT_I);
        SimpleDateFormat formatOutput = new SimpleDateFormat(DATE_FORMAT_O);

        DateFormat df = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault());

        Date date = null;
        try {
            date = formatInput.parse(oriDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

//        TimeZone timeZone = TimeZone.getDefault();
//        String name = timeZone.getID();  // "Asia/Seoul"
//        TimeZone tz = TimeZone.getTimeZone(name);
//        TimeZone tz = TimeZone.getTimeZone("Asia/Seoul");
//        formatOutput.setTimeZone(tz);
//        df.setTimeZone(tz);

        String dateString = formatOutput.format(date) + df.format(date);

        return dateString;
    }

    public static String formatDate(String oriDate) {
        return formatDate(oriDate, "yyyy. MM. dd. ", true);
    }

    public static String formatDate(String oriDate, String outputDateFormat, boolean subStr) {
        String DATE_FORMAT_I = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        String DATE_FORMAT_O = outputDateFormat;//"yyyy. MM. dd. ";

        DateFormat df = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault());
        try {

            SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_I);
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(oriDate);

            SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT_O); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());

            oriDate = dateFormatter.format(value);
            if (subStr) {
                oriDate += df.format(value);
            }

            //Log.d("OurDate", OurDate);
        } catch (Exception e) {
            oriDate = "00-00-0000 00:00";
        }
        return oriDate;
    }

    public static void notiAlert(Context context, String message) {
        notiAlert(context, message, null);
    }

    public static void notiAlert(Context context, String message, AlertCallback callback) {
        Dialog mDialog = new Dialog(context, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_base_one_button, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(context.getResources().getString(R.string.dialog_notice));
        TextView content = (TextView) view.findViewById(R.id.content);
        content.setText(message);
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.ok(null, v);
                }
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    /**
     * 보기 좋게 MB,KB 단위로 축소시킨다
     */
    public static String formatSize(long size) {
        String suffix = null;

        if (size == 0) {
            suffix = "KB";
        }

        if (size >= 1024) {
            suffix = "KB";
            size /= 1024;
            if (size >= 1024) {
                suffix = "MB";
                size /= 1024;
                if (size >= 1024) {
                    suffix = "GB";
                    size /= 1024;
                }
            }
        }
        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }

        if (suffix != null) {
            resultBuffer.append(suffix);
        }

        return resultBuffer.toString();
    }

    /**
     * 외장메모리 sdcard 사용가능한지에 대한 여부 판단
     */
    public static boolean isStorage(boolean requireWriteAccess) {
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        } else if (!requireWriteAccess &&
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    /**
     * 외장메모리 sdcard 사용가능한지에 대한 여부 판단
     */
    public static boolean isSDcardDirectory(boolean requireWriteAccess) {
        String sdFileName = getSDcardDirectoryPath();
        Log.i("isSDcardDirectory", "sdFileName => " + sdFileName);
        if (sdFileName != null && sdFileName.length() > 0) {
            final File file = new File(sdFileName);
            String state;
            try {
                state = getStorageState(file);
            } catch (NoSuchMethodError e) {
                state = Environment.getExternalStorageState();
            }

            if (Environment.MEDIA_MOUNTED.equals(state)) {
                return true;
            } else if (!requireWriteAccess &&
                    Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
                return true;
            }

        }
        return false;
//        if(file.exists() || file.isDirectory()){
//            return true;
//        }
//        return false;
    }

    /**
     * Returns the path to internal storage ex:- /storage/emulated/0
     *
     * @return
     */
    public static String getInternalDirectoryPath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath();
    }

    /**
     * Returns the path to SecretFolder ex:- /storage/emulated/0
     *
     * @return
     */
    public static String getSecretFolderPath() {
        String[] secret = {".Application", ".manager+", ".folder"};
        String secretPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        for (String path : secret) {
            secretPath += File.separator + path;
            File file = new File(secretPath);
            if (!file.exists()) {
                file.mkdir();
            }
        }
        return secretPath;
    }

    /**
     * Returns the path to SecretFolder ex:- /storage/emulated/0
     *
     * @return
     */
    public static String getStorageSaveFileFolderPath() {
        String[] secret = {"Application", "TMFileManager", "ViewFileFolder"};
        String secretPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        for (String path : secret) {
            secretPath += File.separator + path;
            File file = new File(secretPath);
            if (!file.exists()) {
                file.mkdir();
            }
        }
        return secretPath;
    }

    /**
     * Returns the SDcard storage path for samsung ex:- /storage/extSdCard
     *
     * @return
     */
    public static String getSDcardDirectoryPath() {
        String strSDCardPath = System.getenv("SECONDARY_STORAGE");

        if (strSDCardPath != null && strSDCardPath.length() > 0) {
            File file = new File(strSDCardPath);
            if (file.isDirectory()) {
                Log.i("getSDcardDirectoryPath", "SECONDARY_STORAGE => " + strSDCardPath);
                return strSDCardPath;
            }
        }

        strSDCardPath = System.getenv("EXTERNAL_SDCARD_STORAGE");

        if (strSDCardPath != null && strSDCardPath.length() > 0) {
            File file = new File(strSDCardPath);
            if (file.isDirectory()) {
                Log.i("getSDcardDirectoryPath", "EXTERNAL_SDCARD_STORAGE => " + strSDCardPath);
                return strSDCardPath;
            }
        }

        HashSet<String> path = getExternalMounts();

        for (String extSDCardPath : path) {
            if (extSDCardPath != null && extSDCardPath.length() > 0) {
                String[] array = extSDCardPath.split("/");
                if (array.length > 0) {
                    strSDCardPath = "/storage/" + array[array.length - 1];
                    Log.i("getSDcardDirectoryPath", "getExternalMounts => " + strSDCardPath);
                    return strSDCardPath;
                }
            }
        }

//        strSDCardPath = Environment.getExternalStorageDirectory().getAbsolutePath();
//        if (android.os.Build.DEVICE.contains("samsung") || android.os.Build.MANUFACTURER.contains("samsung")) {
//            File f = new File(Environment.getExternalStorageDirectory().getParent() + "/extSdCard" + "/myDirectory");
//            if (f.exists() && f.isDirectory()) {
//                strSDCardPath = Environment.getExternalStorageDirectory().getParent() + "/extSdCard";
//            } else {
//                f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/external_sd" + "/myDirectory");
//                if (f.exists() && f.isDirectory()) {
//                    strSDCardPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/external_sd";
//                }
//            }
//        }
        strSDCardPath = getExternalStoragePath();
        return strSDCardPath;
    }

    public static String getExternalStoragePath() {

        String internalPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        String[] paths = internalPath.split("/");
        String parentPath = "/";
        for (String s : paths) {
            if (s.trim().length() > 0) {
                parentPath = parentPath.concat(s);
                break;
            }
        }
        File parent = new File(parentPath);
        if (parent.exists()) {
            File[] files = parent.listFiles();
            for (File file : files) {
                String filePath = file.getAbsolutePath();
                Log.d("getExternalStoragePath", filePath);
                if (filePath.equals(internalPath)) {
                    continue;
                } else if (filePath.toLowerCase().contains("sdcard")) {
                    return filePath;
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    try {
                        if (Environment.isExternalStorageRemovable(file)) {
                            return filePath;
                        }
                    } catch (RuntimeException e) {
                        Log.e("getExternalStoragePath", "RuntimeException: " + e);
                    }
                }
            }

        }
        return null;
    }

    public static HashSet<String> getExternalMounts() {
        final HashSet<String> out = new HashSet<String>();
        String reg = "(?i).*vold.*(vfat|ntfs|exfat|fat32|ext3|ext4).*rw.*";
        String s = "";
        try {
            final Process process = new ProcessBuilder().command("mount")
                    .redirectErrorStream(true).start();
            process.waitFor();
            final InputStream is = process.getInputStream();
            final byte[] buffer = new byte[1024];
            while (is.read(buffer) != -1) {
                s = s + new String(buffer);
            }
            is.close();
        } catch (final Exception e) {
            e.printStackTrace();
        }

        // parse output
        final String[] lines = s.split("\n");
        for (String line : lines) {
            if (!line.toLowerCase(Locale.US).contains("asec")) {
                if (line.matches(reg)) {
                    String[] parts = line.split(" ");
                    for (String part : parts) {
                        if (part.startsWith("/"))
                            if (!part.toLowerCase(Locale.US).contains("vold"))
                                out.add(part);
                    }
                }
            }
        }
        return out;
    }

    public static String getImageDirectoryPath() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath();
    }

    public static String getVideoDirectoryPath() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).getAbsolutePath();
    }

    public static String getMusicDirectoryPath() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getAbsolutePath();
    }

    public static String getFavoriteDirectoryPath() {
        return System.getenv("SECONDARY_STORAGE");
    }

    public static String getDownloadDirectoryPath() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
    }

    public static String getHDVDirectoryPath() {
        return getInternalDirectoryPath() + File.separator + "HDV Downloader";
        //return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
    }

    /**
     * Tag Color에 따라서 Image Resource value retun
     */
//    public static int getTagColorResourceSmall(String color) {
//        int resource = R.mipmap.tag_red_s;
//        if (color.equals("0")) {
//            resource = R.mipmap.tag_black_s;
//        } else if (color.equals("1")) {
//            resource = R.mipmap.tag_red_s;
//        } else if (color.equals("2")) {
//            resource = R.mipmap.tag_orange_s;
//        } else if (color.equals("3")) {
//            resource = R.mipmap.tag_yellow_s;
//        } else if (color.equals("4")) {
//            resource = R.mipmap.tag_green_s;
//        } else if (color.equals("5")) {
//            resource = R.mipmap.tag_sky_s;
//        } else if (color.equals("6")) {
//            resource = R.mipmap.tag_indigo_s;
//        } else if (color.equals("7")) {
//            resource = R.mipmap.tag_pupple_s;
//        } else if (color.equals("8")) {
//            resource = R.mipmap.tag_black_s;
//        } else if (color.equals("9")) {
//            resource = R.mipmap.tag_grey_s;
//        } else if (color.equals("10")) {
//            resource = R.mipmap.tag_lightgrey_s;
//        }
//        return resource;
//    }

//    public static int getTagColorResourceLb(String color) {
//        int resource = R.mipmap.tag_red_lb;
//        if (color.equals("0")) {
//            resource = R.mipmap.tag_black_lb;
//        } else if (color.equals("1")) {
//            resource = R.mipmap.tag_red_lb;
//        } else if (color.equals("2")) {
//            resource = R.mipmap.tag_orange_lb;
//        } else if (color.equals("3")) {
//            resource = R.mipmap.tag_yellow_lb;
//        } else if (color.equals("4")) {
//            resource = R.mipmap.tag_green_lb;
//        } else if (color.equals("5")) {
//            resource = R.mipmap.tag_sky_lb;
//        } else if (color.equals("6")) {
//            resource = R.mipmap.tag_indigo_lb;
//        } else if (color.equals("7")) {
//            resource = R.mipmap.tag_pupple_lb;
//        } else if (color.equals("8")) {
//            resource = R.mipmap.tag_black_lb;
//        } else if (color.equals("9")) {
//            resource = R.mipmap.tag_grey_lb;
//        } else if (color.equals("10")) {
//            resource = R.mipmap.tag_lightgrey_lb;
//        }
//        return resource;
//    }

    //Search Method
    private static void search_file(String dir, String fileName, ArrayList<FileItem> n) {
        File rootDir = new File(dir);
        String[] list = rootDir.list();
        //boolean root = MyFileSettings.rootAccess();

        if (list != null && rootDir.canRead()) {
            for (String aList : list) {
                File check = new File(dir + "/" + aList);
                String name = check.getName();

                if (check.isFile() && name.toLowerCase().contains(fileName.toLowerCase())) {
                    FileItem item = new FileItem();
                    item.setName(check.getName());
                    item.setPath(check.getPath());
                    item.setId("");
                    n.add(item);
                } else if (check.isDirectory()) {
                    if (name.toLowerCase().contains(fileName.toLowerCase())) {
                        FileItem item = new FileItem();
                        item.setName(check.getName());
                        item.setPath(check.getPath());
                        item.setId("");
                        n.add(item);
                        // change this!
                    } else if (check.canRead() && !dir.equals("/")) {
                        search_file(check.getAbsolutePath(), fileName, n);
                    }
//                    else if (!check.canRead() && root) {
//                        ArrayList<String> al = RootCommands.findFiles(check.getAbsolutePath(), fileName);
//
//                        for (String items : al) {
//                            File file = new File(items);
//                            FileItem item = new FileItem();
//                            item.setName(file.getName());
//                            item.setPath(file.getPath());
//                            item.setTagId("");
//                            n.add(item);
//                        }
//                    }
                }
            }
        }
//        else {
//            if (root) {
//                ArrayList<String> items = RootCommands.findFiles(dir, fileName);
//                ArrayList<FileItem> fileitems = new ArrayList<FileItem>();
//                for(String path : items){
//                    File file = new File(path);
//                    FileItem item = new FileItem();
//                    item.setName(file.getName());
//                    item.setPath(file.getPath());
//                    item.setTagId("");
//                    fileitems.add(item);
//                }
//                n.addAll(fileitems);
//            }
//        }
    }

    public static ArrayList<FileItem> searchInDirectory(String dir, String fileName) {
        ArrayList<FileItem> names = new ArrayList<FileItem>();
        search_file(dir, fileName, names);
        return names;
    }

    public static boolean moveToDirectory(File oldFile, File target, Context c) {
        ArrayList<FileItem> tempList = MainActivity.mMds.selectLikeTagPathFiles(oldFile.getPath());

        if (oldFile.renameTo(target)) {
        } else if (copyFile(oldFile, target, c)) {
            deleteTarget(oldFile.getAbsolutePath());
        } else {
            return false;
        }

        for (FileItem sub : tempList) {
            //Log.i("moveToDirectory", "sub.getPath() => " + sub.getPath());
            String path = sub.getPath().replaceAll(oldFile.getPath(), target.getPath());
            //Log.i("moveToDirectory", "modify path => " + path);
            MainActivity.mMds.updateTagItem(sub, path);
        }
        return true;
    }

    public static void moveToSecretDirectory(File oldFile, String targetName, Context c) {
        moveToSecretDirectory(oldFile, targetName, c, true);
    }

    public static void moveToSecretDirectory(File oldFile, String targetName, Context c, boolean move) {
        if (move) {
            ArrayList<FileItem> tempList = MainActivity.mMds.selectLikeTagPathFiles(oldFile.getPath());
            for (FileItem sub : tempList) {
                MainActivity.mMds.deleteTagPath(sub.getPath());
            }
        }
        String secretFilePath = Utils.getSecretFolderPath() + File.separator + targetName;
        File target = new File(secretFilePath);
        MainActivity.mMds.insertSecretFileItem(oldFile.getName(), oldFile.getPath(), target.getPath(), "", "1984.02.04");
        //if (!oldFile.renameTo(target) && copyFile(oldFile, target, c)) {
        if (copyFile(oldFile, target, c)) {
            if (move) {
                deleteTarget(oldFile.getAbsolutePath());
            }
        }
    }

    public static boolean returnToPathDirectory(String secretId, File oldFile, File target, boolean oriRemove, Context c) {
        if (oriRemove) {
            MainActivity.mMds.deleteSecretFile(secretId);
            if (oldFile.renameTo(target)) {
                return true;
            }

            if (!copyFile(oldFile, target, c)) {
                return false;
            }

            if (oriRemove) {
                deleteTarget(oldFile.getAbsolutePath());
            }
            return true;
        } else {
            if (!copyFile(oldFile, target, c)) {
                return false;
            }
            return true;
        }
    }

    public static boolean deleteSecretFile(String secretId, File target, Context c) {
        MainActivity.mMds.deleteSecretFile(secretId);
        deleteTarget(target.getAbsolutePath());
        return true;
    }

    public static boolean copyFile(final File source, final File target, Context context) {
        FileInputStream inStream = null;
        OutputStream outStream = null;
        FileChannel inChannel = null;
        FileChannel outChannel = null;

        try {
            File tempDir = target.getParentFile();

            if (source.isFile())
                inStream = new FileInputStream(source);

            if (source.canRead() && tempDir.isDirectory()) {
                if (source.isFile()) {
                    outStream = new FileOutputStream(target);
                    inChannel = inStream.getChannel();
                    outChannel = ((FileOutputStream) outStream).getChannel();
                    inChannel.transferTo(0, inChannel.size(), outChannel);
                } else if (source.isDirectory()) {
                    File[] files = source.listFiles();

                    if (createDir(target)) {
                        for (File file : files) {
                            copyFile(new File(source, file.getName()), new File(target, file.getName()), context);
                        }
                    }
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    DocumentFile targetDocument = DocumentFile.fromFile(tempDir);
                    outStream = context.getContentResolver().openOutputStream(targetDocument.getUri());
                } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
                    Uri uri = MediaStoreUtils.getUriFromFile(target.getAbsolutePath(), context);
                    outStream = context.getContentResolver().openOutputStream(uri);
                } else {
                    return false;
                }

                if (outStream != null && inStream != null) {
                    byte[] buffer = new byte[BUFFER];
                    int bytesRead;
                    while ((bytesRead = inStream.read(buffer)) != -1) {
                        outStream.write(buffer, 0, bytesRead);
                    }
                } else {
                    RootCommands.moveCopyRoot(source.getAbsolutePath(), target.getAbsolutePath());
                }
            }
        } catch (Exception e) {
            //Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
            Message msg = Message.obtain();
            msg.what = 0;
            msg.obj = e;
            if (context instanceof MainActivity) {
                ((MainActivity) context).errorHandler.sendMessage(msg);
            } else if (context instanceof ImageSlideViewerActivity) {
                ((ImageSlideViewerActivity) context).errorHandler.sendMessage(msg);
            }
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (inStream != null && outStream != null && inChannel != null && outChannel != null) {
                    inStream.close();
                    outStream.close();
                    inChannel.close();
                    outChannel.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    public static boolean renameTarget(String filePath, String newName) {
        File src = new File(filePath);

        String temp = filePath.substring(0, filePath.lastIndexOf("/"));
        File dest = new File(temp + "/" + newName);

        if (src.renameTo(dest)) {
            return true;
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                DocumentFile document = DocumentFile.fromFile(src);

                if (document.renameTo(dest.getAbsolutePath())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean createFile(File file) {
        if (file.exists()) {
            return !file.isDirectory();
        }

        try {
            if (file.createNewFile()) {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            DocumentFile document = DocumentFile.fromFile(file.getParentFile());

            try {
                return document.createFile(MimeTypes.getMimeType(file), file.getName()) != null;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    public static boolean createDir(File folder) {
        if (folder.exists())
            //return false;
            return true;

        if (folder.mkdir())
            return true;
        else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                DocumentFile document = DocumentFile.fromFile(folder.getParentFile());
                if (document.exists())
                    return true;
            }

            if (MyFileSettings.rootAccess()) {
                return RootCommands.createRootdir(folder);
            }
        }

        return false;
    }

    public static boolean deleteTarget(String path) {
        boolean success = true;

        File target = new File(path);
        if (target.isFile() && target.canWrite()) {
            target.delete();
        } else if (target.isDirectory() && target.canRead() && target.canWrite()) {
            String[] fileList = target.list();

            if (fileList != null && fileList.length == 0) {
                target.delete();
                return success;
            } else if (fileList != null && fileList.length > 0) {
                for (String aFile_list : fileList) {
                    String deltePath = target.getAbsolutePath() + "/" + aFile_list;
                    File tempF = new File(deltePath);

                    if (tempF.isDirectory())
                        success = deleteTarget(tempF.getAbsolutePath());
                    else if (tempF.isFile()) {
                        if (tempF.delete()) {
                            MainActivity.mMds.deleteFile(deltePath);
                        } else {
                            success = false;
                            return success;
                        }
                    }

                    if (!success) {
                        return success;
                    }
                }
            }

            if (target.exists())
                target.delete();
        } else if (!target.delete() && MyFileSettings.rootAccess()) {
            RootCommands.deleteRootFileOrDir(target);
        }
        return true;
    }

    public static void openFile(Context context, File target) {
        String mime = MimeTypes.getMimeType(target);
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri apkURI = null;
        try {
            apkURI = FileProvider.getUriForFile(
                    context,
                    context.getApplicationContext()
                            .getPackageName() + ".provider", target);
        } catch (Exception e) {
            mime = null;
        }
        if (mime != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                i.setDataAndType(apkURI, mime);
            } else {
                i.setDataAndType(Uri.fromFile(target), mime);
//                i.setData(Uri.fromFile(target));
//                i.setType(mime);
            }
        } else {
            i.setDataAndType(Uri.fromFile(target), "*/*");
        }
        i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        if (context.getPackageManager().queryIntentActivities(i, 0).isEmpty()) {
            Toast.makeText(context, R.string.cantopenfile, Toast.LENGTH_SHORT).show();
            SecretFolderFragment.mPasswordMode = true;
            return;
        }

        try {
            context.startActivity(i);
        } catch (Exception e) {
            Toast.makeText(context, context.getString(R.string.cantopenfile) + e.getMessage(),
                    Toast.LENGTH_SHORT).show();
            SecretFolderFragment.mPasswordMode = true;
        }
    }

    public static void sendFiles(Context context, ArrayList<FileItem> items) {

        if (items != null && items.size() > 0) {
            if (items.size() == 1) {
                File target = new File(items.get(0).getPath());

                String mime = MimeTypes.getMimeType(target);
                Intent i = new Intent(Intent.ACTION_SEND);

                if (mime != null) {
                    //i.setDataAndType(Uri.fromFile(target), mime);
                    i.setType(mime);
                } else {
                    //i.setDataAndType(Uri.fromFile(target), "*/*");
                    i.setType("*/*");
                }
                i.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(target));

                if (context.getPackageManager().queryIntentActivities(i, 0).isEmpty()) {
                    Toast.makeText(context, R.string.cantopenfile, Toast.LENGTH_SHORT).show();
                    return;
                }

                try {
                    context.startActivity(i);
                } catch (Exception e) {
                    Toast.makeText(context, context.getString(R.string.cantopenfile) + e.getMessage(),
                            Toast.LENGTH_SHORT).show();
                }
            } else {
                Intent i = new Intent(Intent.ACTION_SEND_MULTIPLE);
                i.setType("*/*");

                ArrayList<Uri> files = new ArrayList<Uri>();
                for (FileItem item : items /* List of the files you want to send */) {
                    File file = new File(item.getPath());
                    Uri uri = Uri.fromFile(file);
                    files.add(uri);
                }
                i.putParcelableArrayListExtra(Intent.EXTRA_STREAM, files);

                if (context.getPackageManager().queryIntentActivities(i, 0).isEmpty()) {
                    Toast.makeText(context, R.string.cantopenfile, Toast.LENGTH_SHORT).show();
                    return;
                }

                try {
                    context.startActivity(i);
                } catch (Exception e) {
                    Toast.makeText(context, context.getString(R.string.cantopenfile) + e.getMessage(),
                            Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    /**
     * Comment  : 정상적인 이메일 인지 검증.
     */
    public static boolean isValidEmail(String email) {
        boolean err = false;
        String regex = "^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(email);
        if (m.matches()) {
            err = true;
        }
        return err;
    }

    /**
     * 지정한 경로에서 파일의 확장자를 삭제한다.
     * e.g. "mypath/myfile.txt" -> "mypath/myfile".
     *
     * @param path 파일 경로
     * @return 확장자가 삭제된 경로. 파일 경로가 <tt>null</tt>인 경우 <tt>null</tt>
     */
    public static String stripFilenameExtension(String path) {
        if (path == null) {
            return null;
        }
        int sepIndex = path.lastIndexOf('.');
        return (sepIndex != -1 ? path.substring(0, sepIndex) : path);
    }

    /**
     * 지정한 경로에서 파일의 확장자를 추출한다.
     * e.g. "mypath/myfile.txt" -> "txt".
     *
     * @param path 파일 경로
     * @return 확장자. 파일 경로가 <tt>null</tt>인 경우 <tt>null</tt>
     */
    public static String getFilenameExtension(String path) {
        if (path == null) {
            return null;
        }
        int sepIndex = path.lastIndexOf('.');
        return (sepIndex != -1 ? path.substring(sepIndex + 1) : null);
    }

    /**
     * Removes URL underlines in a string by replacing URLSpan occurrences by
     * URLSpanNoUnderline objects.
     *
     * @param p_Text A Spannable object. For example, a TextView casted as
     *               Spannable.
     */

    public static Spannable removeUnderlines(Spannable p_Text) {
        URLSpan[] spans = p_Text.getSpans(0, p_Text.length(), URLSpan.class);
        for (URLSpan span : spans) {
            int start = p_Text.getSpanStart(span);
            int end = p_Text.getSpanEnd(span);
            p_Text.removeSpan(span);
            span = new URLSpanNoUnderline(span.getURL());
            p_Text.setSpan(span, start, end, 0);
        }
        return p_Text;
    }

    public static Dialog Progress(Context ctx, String title, String contents, Boolean cancel) {
        return Progress(ctx, title, contents, cancel, 1);
    }

    public static Dialog Progress(Context ctx, String title, String contents, Boolean cancel, int style) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View view;
        if (style == 0) {
            view = inflater.inflate(R.layout.progress_dialog, null, false);
        } else {
            view = inflater.inflate(R.layout.progress_dialog_gray, null, false);
            TextView progress_tv = (TextView) view.findViewById(R.id.progress_tv);
        }

        Dialog dialog = new Dialog(ctx, R.style.Theme_TransparentBackground);
        dialog.setContentView(view);
        dialog.setCancelable(cancel);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        return dialog;

//        ProgressDialog dialog = new ProgressDialog(ctx);
//        // dialog.setTitle(title);
//        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        // dialog.setProgressStyle(R.drawable.my_progress_indeterminate);
//        // dialog.setProgressDrawable(ctx.getResources().getDrawable(R.drawable.progressbar_custom));
//        dialog.setMessage(contents);
//        dialog.setIndeterminate(true);
//        dialog.setCancelable(cancel);
//        return dialog;
    }

    public static Dialog ProgressText(Context ctx, String contents) {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View view = inflater.inflate(R.layout.progress_dialog_gray, null, false);
        TextView progress_tv = (TextView) view.findViewById(R.id.progress_tv);
        if (contents != null && contents.length() > 0) {
            progress_tv.setVisibility(View.VISIBLE);
            progress_tv.setText(contents);
        }

        Dialog dialog = new Dialog(ctx, R.style.Theme_TransparentBackground);
        dialog.setContentView(view);
        dialog.setCancelable(false);
        //dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        return dialog;
    }

    /**
     * Validate hex with regular expression
     *
     * @param hex hex for validation
     * @return true valid hex, false invalid hex
     */
    public static boolean emailvalidate(final String hex) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(hex);
        return matcher.matches();

    }

    public static boolean teamnamevalidate(final CharSequence hex) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(TEAM_NAME_PATTERN);
        matcher = pattern.matcher(hex);
        return matcher.matches();

    }

    public static boolean checkIsTablet(Context context) {
        boolean isTablet = false;
        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        float widthInches = metrics.widthPixels / metrics.xdpi;
        float heightInches = metrics.heightPixels / metrics.ydpi;
        double diagonalInches = Math.sqrt(Math.pow(widthInches, 2) + Math.pow(heightInches, 2));
        //if (diagonalInches >= 7.0) {
        if (diagonalInches >= 6.5) {
            isTablet = true;
            //((Activity)context).setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        }

        if (isTablet) {
            //((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        }
//        else{
//            ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
//        }

        return isTablet;
    }
}
