package jiran.com.tmfilemanager.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.data.SecretFileItem;

public final class PasteTask extends AsyncTask<String, Void, List<String>> {

    private final WeakReference<Activity> activity;
    private final File location;
    private ProgressDialog dialog;
    private boolean success = false;

    private TextView TextView_CurrentFileName;

    public PasteTask(final Activity activity, File currentDir) {
        this.activity = new WeakReference<>(activity);
        this.location = currentDir;
    }

    @Override
    protected void onPreExecute() {
        final Activity activity = this.activity.get();

        if (activity != null) {
            this.dialog = new ProgressDialog(activity);
//            LayoutInflater li = LayoutInflater.from(activity);
//            RelativeLayout layout = (RelativeLayout)li.inflate(R.layout.filetransfer_dialog, null);
//            TextView_CurrentFileName = (TextView) layout.findViewById(R.id.TextView_CurrentFileName);
//            dialog.setView(layout);

            if (ClipBoard.isMove())
                this.dialog.setMessage(activity.getString(R.string.moving));
                //TextView_CurrentFileName.setText(activity.getString(R.string.moving));
            else
                this.dialog.setMessage(activity.getString(R.string.copying));
            //TextView_CurrentFileName.setText(activity.getString(R.string.copying));

            this.dialog.setCancelable(true);
            this.dialog
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            cancel(false);
                        }
                    });
            if (!activity.isFinishing()) {
                this.dialog.show();
            }
        }
    }

    @Override
    protected List<String> doInBackground(String... content) {
        final List<String> failed = new ArrayList<>();
        final Activity activity = this.activity.get();
        ClipBoard.lock();

        for (String s : content) {
            String fileName = s.substring(s.lastIndexOf("/"), s.length());
            ArrayList<SecretFileItem> list = MainActivity.mMds.selectSecretFiles(s);
            if (list.size() > 0) {
                SecretFileItem item = list.get(0);
                fileName = item.getName();
            }
            if (ClipBoard.isMove()) {
                success = Utils.moveToDirectory(new File(s), new File(location, fileName), activity);
            } else {
                success = Utils.copyFile(new File(s), new File(location, fileName), activity);
                //success = true;
            }
        }

        if (location.canRead()) {
            for (File file : location.listFiles())
                MediaStoreUtils.addFileToMediaStore(file.getPath(), activity);
        }
        return failed;
    }

    @Override
    protected void onPostExecute(final List<String> failed) {
        super.onPostExecute(failed);
        this.finish(failed);
    }

    @Override
    protected void onCancelled(final List<String> failed) {
        super.onCancelled(failed);
        this.finish(failed);
    }

    private void finish(final List<String> failed) {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }

        final Activity activity = this.activity.get();

        if (ClipBoard.isMove()) {
            if (success)
                Toast.makeText(activity,
                        activity.getString(R.string.movesuccsess),
                        Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(activity, activity.getString(R.string.movefail),
                        Toast.LENGTH_SHORT).show();
        } else {
            if (success)
                Toast.makeText(activity,
                        activity.getString(R.string.copysuccsess),
                        Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(activity, activity.getString(R.string.copyfail),
                        Toast.LENGTH_SHORT).show();
        }

        ClipBoard.unlock();
        ClipBoard.clear();
        activity.invalidateOptionsMenu();
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).pasteFinish();
        }

        if (!failed.isEmpty()) {
            Toast.makeText(activity, activity.getString(R.string.cantopenfile),
                    Toast.LENGTH_SHORT).show();
            if (!activity.isFinishing()) {
                dialog.show();
            }
        }
    }
}
