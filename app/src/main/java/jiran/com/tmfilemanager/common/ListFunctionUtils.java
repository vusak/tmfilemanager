package jiran.com.tmfilemanager.common;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.provider.UserDictionary;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.data.SecretFileItem;
import jiran.com.tmfilemanager.settings.MyFileSettings;

/**
 * Created by user on 2016-08-03.
 */
public class ListFunctionUtils {

    public static final int LIST_DAY = 3;
    private static final int BUFFER = 16384;
    private static final long ONE_KB = 1024;
    private static final long L_KB_BI = ONE_KB;
    private static final long L_MB_BI = L_KB_BI * L_KB_BI;
    private static final long L_GB_BI = L_MB_BI * L_KB_BI;
    private static final long L_TB_BI = L_GB_BI * L_KB_BI;
    private static final BigDecimal KB_BI = BigDecimal.valueOf(ONE_KB);
    ;
    private static final BigDecimal MB_BI = KB_BI.multiply(KB_BI);
    private static final BigDecimal GB_BI = KB_BI.multiply(MB_BI);
    private static final BigDecimal TB_BI = KB_BI.multiply(GB_BI);
    public static ArrayList<FileItem> mTempAppContent = new ArrayList<FileItem>();
    public static ArrayList<FileItem> mTempZipContent = new ArrayList<FileItem>();
    public static ArrayList<FileItem> mTempDocuContent = new ArrayList<FileItem>();
    public static ArrayList<FileItem> mTempRecentContent = new ArrayList<FileItem>();
    public static ArrayList<FileItem> mTempHugeContent = new ArrayList<FileItem>();

    private ListFunctionUtils() {
    }

//        // TODO: fix search with root
//        private static void search_file(String dir, String fileName, ArrayList<String> n) {
//            File rootDir = new File(dir);
//            String[] list = rootDir.list();
//            boolean root = false;//MyFileSettings.rootAccess();
//
//            if (list != null && rootDir.canRead()) {
//                for (String aList : list) {
//                    File check = new File(dir + "/" + aList);
//                    String name = check.getName();
//
//                    if (check.isFile() && name.toLowerCase().contains(fileName.toLowerCase())) {
//                        n.add(check.getPath());
//                    } else if (check.isDirectory()) {
//                        if (name.toLowerCase().contains(fileName.toLowerCase())) {
//                            n.add(check.getPath());
//
//                            // change this!
//                        } else if (check.canRead() && !dir.equals("/")) {
//                            search_file(check.getAbsolutePath(), fileName, n);
//                        } else if (!check.canRead() && root) {
//                            ArrayList<String> al = RootCommands.findFiles(check.getAbsolutePath(), fileName);
//
//                            for (String items : al) {
//                                n.add(items);
//                            }
//                        }
//                    }
//                }
//            } else {
//                if (root)
//                    n.addAll(RootCommands.findFiles(dir, fileName));
//            }
//        }

    public static ArrayList<FileItem> listFolders(String path, Context c) throws Exception {
        ArrayList<FileItem> mDirContent = new ArrayList<>();
        boolean showhidden = MyFileSettings.getShowHiddenFiles();//MyFileSettings.showHiddenFiles();

        if (!mDirContent.isEmpty())
            mDirContent.clear();

        final File file = new File(path);

        if (file.exists() && file.canRead()) {
            String[] list = file.list();

            // add files/folder to ArrayList depending on hidden status
            for (String aList : list) {
                if (!showhidden) {
                    if (aList.charAt(0) != '.') {
                        FileItem item = new FileItem();
                        item.setName(aList);
                        item.setPath(path + "/" + aList);
                        item.setId("");

                        //item.setFavoriteId(MainActivity.mMds.checkFavoriteFile(path + "/" + aList));
                        item.setcheck(false);
                        mDirContent.add(item);
                    }
                } else {
                    FileItem item = new FileItem();
                    item.setName(aList);
                    item.setPath(path + "/" + aList);
                    item.setId("");

                    //item.setFavoriteId(MainActivity.mMds.checkFavoriteFile(path + "/" + aList));
                    item.setcheck(false);
                    mDirContent.add(item);
                }
            }
        } else if (MyFileSettings.rootAccess()) {
            mDirContent = RootCommands.listFiles(file.getAbsolutePath(), showhidden);
        } else {
            Toast.makeText(c, c.getString(R.string.error), Toast.LENGTH_SHORT).show();
            //Toast.makeText(c, "test", Toast.LENGTH_SHORT).show();
        }
        return mDirContent;
    }

    public static ArrayList<FileItem> listFiles(String path, Context c) throws Exception {
        ArrayList<FileItem> mDirContent = new ArrayList<>();
        boolean showhidden = MyFileSettings.getShowHiddenFiles();//MyFileSettings.showHiddenFiles();

        if (!mDirContent.isEmpty())
            mDirContent.clear();

        final File file = new File(path);

        if (file.exists() && file.canRead()) {
            String[] list = file.list();

            // add files/folder to ArrayList depending on hidden status
            for (String aList : list) {
                if (!showhidden) {
                    if (aList.charAt(0) != '.') {
                        FileItem item = MainActivity.mMds.checkFile(path + "/" + aList);
                        if (item == null) {
                            item = new FileItem();
                            item.setName(aList);
                            item.setPath(path + "/" + aList);
                            item.setId("");
                        } else {
                            item.setName(aList);
                        }
                        //item.setFavoriteId(MainActivity.mMds.checkFavoriteFile(path + "/" + aList));
                        item.setcheck(false);
                        mDirContent.add(item);
                    }
                } else {
                    FileItem item = MainActivity.mMds.checkFile(path + "/" + aList);
                    if (item == null) {
                        item = new FileItem();
                        item.setName(aList);
                        item.setPath(path + "/" + aList);
                        item.setId("");
                    } else {
                        item.setName(aList);
                    }
                    //item.setFavoriteId(MainActivity.mMds.checkFavoriteFile(path + "/" + aList));
                    item.setcheck(false);
                    mDirContent.add(item);
                }
            }
        } else if (MyFileSettings.rootAccess()) {
            mDirContent = RootCommands.listFiles(file.getAbsolutePath(), showhidden);
        } else {
            Toast.makeText(c, c.getString(R.string.error), Toast.LENGTH_SHORT).show();
            //Toast.makeText(c, "test", Toast.LENGTH_SHORT).show();
        }
        return mDirContent;
    }

    public static ArrayList<FileItem> listAllDocumentQueryFiles(Context context) throws Exception {
        ArrayList<FileItem> mDirContent = new ArrayList<>();
        boolean showhidden = MyFileSettings.getShowHiddenFiles();//MyFileSettings.showHiddenFiles();

        if (!mDirContent.isEmpty())
            mDirContent.clear();

        final String[] columns = {UserDictionary.Words._ID, UserDictionary.Words.WORD, UserDictionary.Words.LOCALE};
        final String orderBy = UserDictionary.Words.DEFAULT_SORT_ORDER;
        //Stores all the images from the gallery in Cursor
        Cursor cursor = context.getContentResolver().query(
                UserDictionary.Words.CONTENT_URI, columns, null,
                null, orderBy);
        //Total number of images
        int count = cursor.getCount();

        if (count == 0) {
            return mDirContent;
        }

        //Create an array to store path to all the images
        String[] arrPath = new String[count];
        cursor.moveToLast();
        for (int i = count - 1; i >= 0; i--) {
            cursor.moveToPosition(i);
            int dataColumnIndex = cursor.getColumnIndex(UserDictionary.Words._ID);
            //Store the path of the image
            String path = cursor.getString(dataColumnIndex);
            Log.i("PATH", path);
            final File file = new File(path);
            if (file.exists() && file.canRead()) {
                if (!showhidden) {
                    if (file.getName().charAt(0) != '.') {
                        FileItem item = MainActivity.mMds.checkFile(path);
                        if (item == null) {
                            item = new FileItem();
                            item.setName(file.getName());
                            item.setPath(path);
                            item.setId("");
                        } else {

                        }
                        item.setcheck(false);
                        mDirContent.add(item);
                    }
                } else {
                    FileItem item = MainActivity.mMds.checkFile(path);
                    if (item == null) {
                        item = new FileItem();
                        item.setName(file.getName());
                        item.setPath(path);
                        item.setId("");
                    } else {

                    }
                    item.setcheck(false);
                    mDirContent.add(item);
                }

            }

        }
        return mDirContent;
    }

    public static ArrayList<FileItem> listAllImageFolders(Context context) throws Exception {
        ArrayList<String> FolderArray = new ArrayList<String>();
        ArrayList<FileItem> mDirContent = new ArrayList<>();
        boolean showhidden = MyFileSettings.getShowHiddenFiles();//MyFileSettings.showHiddenFiles();

        final String orderBy = MediaStore.Images.Media.BUCKET_DISPLAY_NAME;
        String[] projection = {MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
                MediaStore.Images.Media.DATA,
                MediaStore.Images.Media.ORIENTATION,
                MediaStore.Images.Media.MIME_TYPE};

        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, orderBy);

        //Total number of images
        int count = cursor.getCount();

        if (count == 0) {
            return mDirContent;
        }

        //Create an array to store path to all the images
        String[] arrPath = new String[count];
        cursor.moveToLast();
        for (int i = count - 1; i >= 0; i--) {
            cursor.moveToPosition(i);
            int dataNameIndex = cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
            int dataDataIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            //Store the path of the image
            String display_name = cursor.getString(dataNameIndex);
            String path = cursor.getString(dataDataIndex);
            Log.i("PATH", path);

            if (!FolderArray.contains(display_name)) {
                FolderArray.add(display_name);
                FileItem item = new FileItem();
                item.setSize("1");
                item.setSObjectType("D");
                item.setName(display_name);
                item.setPath(path);
                item.setId("");
                item.setcheck(false);
                mDirContent.add(item);
            } else {
                for (FileItem item : mDirContent) {
                    if (item.getName().equals(display_name)) {
                        item.setSize(String.valueOf((Integer.valueOf(item.getSize()) + 1)));
                    }
                }
            }
        }

        return mDirContent;
    }

    public static ArrayList<FileItem> listAllImageFolderFiles(Context context, String folderName) throws Exception {
        ArrayList<String> FolderArray = new ArrayList<String>();
        ArrayList<FileItem> mDirContent = new ArrayList<>();
        boolean showhidden = MyFileSettings.getShowHiddenFiles();//MyFileSettings.showHiddenFiles();

        final String orderBy = MediaStore.Images.Media.DATE_MODIFIED;
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, "bucket_display_name=" + DatabaseUtils.sqlEscapeString(folderName), null, orderBy);

        //Total number of images
        int count = cursor.getCount();

        if (count == 0) {
            return mDirContent;
        }

        //Create an array to store path to all the images
        String[] arrPath = new String[count];
        cursor.moveToLast();
        for (int i = count - 1; i >= 0; i--) {
            cursor.moveToPosition(i);
            String mime_type = cursor.getString(cursor
                    .getColumnIndex(MediaStore.MediaColumns.MIME_TYPE));
            String id = cursor.getString(cursor
                    .getColumnIndex(BaseColumns._ID));
            String display_name = cursor
                    .getString(cursor
                            .getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME));
            String data = cursor.getString(cursor
                    .getColumnIndex(MediaStore.MediaColumns.DATA));
            String size = cursor.getString(cursor
                    .getColumnIndex(MediaStore.MediaColumns.SIZE));
            String date_modified = cursor
                    .getString(cursor
                            .getColumnIndex(MediaStore.MediaColumns.DATE_MODIFIED));
            String orientation = cursor
                    .getString(cursor
                            .getColumnIndex(MediaStore.Images.ImageColumns.ORIENTATION));
            Log.i("displayName", display_name);


            FileItem item = new FileItem();
            item.setName(display_name);
            item.setPath(data);
            item.setId("");
            item.setcheck(false);
            item.setSize(size);
            mDirContent.add(item);
        }
        return mDirContent;
    }

    public static ArrayList<FileItem> listAllImageFiles(Context context) throws Exception {
        ArrayList<FileItem> mDirContent = new ArrayList<>();
        boolean showhidden = MyFileSettings.getShowHiddenFiles();//MyFileSettings.showHiddenFiles();

        if (!mDirContent.isEmpty())
            mDirContent.clear();

        final String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID};
        final String orderBy = MediaStore.Images.Media.DATE_MODIFIED;
        //Stores all the images from the gallery in Cursor
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null,
                //MediaStore.Images.Media.EXTERNAL_CONTENT_URI.buildUpon().encodedQuery("limit=1,10").build(), columns, null,
                null, orderBy);
        //Total number of images
        int count = cursor.getCount();

        if (count == 0) {
            return mDirContent;
        }

        //Create an array to store path to all the images
        String[] arrPath = new String[count];
        cursor.moveToLast();
        for (int i = count - 1; i >= 0; i--) {
            cursor.moveToPosition(i);
            int dataColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            //Store the path of the image
            String path = cursor.getString(dataColumnIndex);
            Log.i("PATH", path);
            final File file = new File(path);
            if (file.exists() && file.canRead()) {
                if (!showhidden) {
                    if (file.getName().charAt(0) != '.') {
                        FileItem item = MainActivity.mMds.checkFile(path);
                        if (item == null) {
                            item = new FileItem();
                            item.setName(file.getName());
                            item.setPath(path);
                            item.setId("");
                        } else {

                        }
                        item.setcheck(false);
                        mDirContent.add(item);
                    }
                } else {
                    FileItem item = MainActivity.mMds.checkFile(path);
                    if (item == null) {
                        item = new FileItem();
                        item.setName(file.getName());
                        item.setPath(path);
                        item.setId("");
                    } else {

                    }
                    item.setcheck(false);
                    mDirContent.add(item);
                }

            }

        }
        return mDirContent;
    }

    public static ArrayList<FileItem> listAllVideoFiles(Context context) throws Exception {
        ArrayList<FileItem> mDirContent = new ArrayList<>();
        boolean showhidden = MyFileSettings.getShowHiddenFiles();//MyFileSettings.showHiddenFiles();

        if (!mDirContent.isEmpty())
            mDirContent.clear();

        final String[] columns = {MediaStore.Video.Media.DATA, MediaStore.Video.Media._ID};
        final String orderBy = MediaStore.Video.Media.DATE_MODIFIED;
        //Stores all the images from the gallery in Cursor
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI, columns, null,
                null, orderBy);
        //Total number of images
        int count = cursor.getCount();

        if (count == 0) {
            return mDirContent;
        }
        //Create an array to store path to all the images
        String[] arrPath = new String[count];

        cursor.moveToLast();
        for (int i = count - 1; i >= 0; i--) {
            cursor.moveToPosition(i);
            int dataColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            //Store the path of the image
            String path = cursor.getString(dataColumnIndex);
            Log.i("PATH", path);
            final File file = new File(path);
            if (file.exists() && file.canRead()) {
                if (!showhidden) {
                    if (file.getName().charAt(0) != '.') {
                        FileItem item = MainActivity.mMds.checkFile(path);
                        if (item == null) {
                            item = new FileItem();
                            item.setName(file.getName());
                            item.setPath(path);
                            item.setId("");
                        } else {

                        }
                        item.setcheck(false);
                        mDirContent.add(item);
                    }
                } else {
                    FileItem item = MainActivity.mMds.checkFile(path);
                    if (item == null) {
                        item = new FileItem();
                        item.setName(file.getName());
                        item.setPath(path);
                        item.setId("");
                    } else {

                    }
                    item.setcheck(false);
                    mDirContent.add(item);
                }

            }

        }
        return mDirContent;
    }

    public static ArrayList<FileItem> listAllAudioFiles(Context context) throws Exception {
        ArrayList<FileItem> mDirContent = new ArrayList<>();
        boolean showhidden = MyFileSettings.getShowHiddenFiles();//MyFileSettings.showHiddenFiles();

        if (!mDirContent.isEmpty())
            mDirContent.clear();

        final String[] columns = {MediaStore.Audio.Media.DATA, MediaStore.Audio.Media._ID};
        final String orderBy = MediaStore.Audio.Media.DATE_MODIFIED;
        //Stores all the images from the gallery in Cursor
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, columns, null,
                null, orderBy);
        //Total number of images
        int count = cursor.getCount();

        if (count == 0) {
            return mDirContent;
        }

        //Create an array to store path to all the images
        String[] arrPath = new String[count];

        cursor.moveToLast();
        for (int i = count - 1; i >= 0; i--) {
            cursor.moveToPosition(i);
            int dataColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            //Store the path of the image
            String path = cursor.getString(dataColumnIndex);
            Log.i("PATH", path);
            final File file = new File(path);
            if (file.exists() && file.canRead()) {
                if (!showhidden) {
                    if (file.getName().charAt(0) != '.') {
                        FileItem item = MainActivity.mMds.checkFile(path);
                        if (item == null) {
                            item = new FileItem();
                            item.setName(file.getName());
                            item.setPath(path);
                            item.setId("");
                        } else {

                        }
                        item.setcheck(false);
                        mDirContent.add(item);
                    }
                } else {
                    FileItem item = MainActivity.mMds.checkFile(path);
                    if (item == null) {
                        item = new FileItem();
                        item.setName(file.getName());
                        item.setPath(path);
                        item.setId("");
                    } else {

                    }
                    item.setcheck(false);
                    mDirContent.add(item);
                }

            }

        }
        return mDirContent;
    }

    public static void listDaysFilesInit(int day) {
        ArrayList<FileItem> mTempDirContent = new ArrayList<FileItem>();
        if (Utils.isSDcardDirectory(true)) {
            searchDays(Utils.getSDcardDirectoryPath(), day, mTempDirContent);
        }
        searchDays(Utils.getInternalDirectoryPath(), day, mTempDirContent);
        if (mTempRecentContent.size() != mTempDirContent.size()) {
            mTempRecentContent = mTempDirContent;
        }
    }

    public static ArrayList<FileItem> listDaysFiles(Context context, int day) {
//        if (!mTempDirContent.isEmpty())
//            mTempDirContent.clear();
//        ArrayList<FileItem> mTempDirContent = new ArrayList<FileItem>();
        if (mTempRecentContent != null && mTempRecentContent.size() > 0) {
            return mTempRecentContent;
        }

        if (Utils.isSDcardDirectory(true)) {
            searchDays(Utils.getSDcardDirectoryPath(), day, mTempRecentContent);
        }
        searchDays(Utils.getInternalDirectoryPath(), day, mTempRecentContent);
        return mTempRecentContent;
    }

    public static void searchDays(String path, int day, ArrayList<FileItem> dirContent) {
        boolean showhidden = MyFileSettings.getShowHiddenFiles();//MyFileSettings.showHiddenFiles();
        File dir = new File(path);
        File[] list = dir.listFiles();
        if (list != null) {
            for (File file : list) {
                if (file.isDirectory()) {
                    //if (checkDays(file, day))
                    {
                        searchDays(file.getPath(), day, dirContent);
                    }
                } else {
                    if (file.exists() && file.canRead() && checkDays(file, day)) {
                        if (!showhidden) {
                            if (file.getName().charAt(0) != '.') {
                                FileItem item = MainActivity.mMds.checkFile(file.getPath());
                                if (item == null) {
                                    item = new FileItem();
                                    item.setName(file.getName());
                                    item.setPath(file.getPath());
                                    item.setId("");
                                } else {

                                }
                                item.setcheck(false);
                                Log.i("PATH", item.getPath());
                                dirContent.add(item);
                            }
                        } else {
                            FileItem item = MainActivity.mMds.checkFile(file.getPath());
                            if (item == null) {
                                item = new FileItem();
                                item.setName(file.getName());
                                item.setPath(file.getPath());
                                item.setId("");
                            } else {

                            }
                            item.setcheck(false);
                            Log.i("PATH", item.getPath());
                            dirContent.add(item);
                        }
                    }
                }
            }
        }
    }

    public static void listHugeFilesInit(long size) {
        ArrayList<FileItem> mTempDirContent = new ArrayList<FileItem>();
        if (Utils.isSDcardDirectory(true)) {
            searchHuge(Utils.getSDcardDirectoryPath(), size, mTempDirContent);
        }
        searchHuge(Utils.getInternalDirectoryPath(), size, mTempDirContent);
        if (mTempHugeContent.size() != mTempDirContent.size()) {
            mTempHugeContent = mTempDirContent;
        }
    }

    public static ArrayList<FileItem> listHugeFiles(Context context, long size) {
        if (mTempHugeContent != null && mTempHugeContent.size() > 0) {
            return mTempHugeContent;
        }

        if (Utils.isSDcardDirectory(true)) {
            searchHuge(Utils.getSDcardDirectoryPath(), size, mTempHugeContent);
        }
        searchHuge(Utils.getInternalDirectoryPath(), size, mTempHugeContent);
        return mTempHugeContent;
    }

    public static void searchHuge(String path, long size, ArrayList<FileItem> dirContent) {
        boolean showhidden = MyFileSettings.getShowHiddenFiles();//MyFileSettings.showHiddenFiles();
        File dir = new File(path);
        File[] list = dir.listFiles();
        if (list != null) {
            for (File file : list) {
                if (file.isDirectory()) {
                    searchHuge(file.getPath(), size, dirContent);
                } else {
                    if (file.exists() && file.canRead() && checkSize(file, size)) {
                        if (!showhidden) {
                            if (file.getName().charAt(0) != '.') {
                                FileItem item = MainActivity.mMds.checkFile(file.getPath());
                                if (item == null) {
                                    item = new FileItem();
                                    item.setName(file.getName());
                                    item.setPath(file.getPath());
                                    item.setId("");
                                } else {

                                }
                                item.setcheck(false);
                                Log.i("PATH", item.getPath());
                                dirContent.add(item);
                            }
                        } else {
                            FileItem item = MainActivity.mMds.checkFile(file.getPath());
                            if (item == null) {
                                item = new FileItem();
                                item.setName(file.getName());
                                item.setPath(file.getPath());
                                item.setId("");
                            } else {

                            }
                            item.setcheck(false);
                            Log.i("PATH", item.getPath());
                            dirContent.add(item);
                        }
                    }
                }
            }
        }
    }

    public static void listAllDocumentFilesInit() {
        ArrayList<FileItem> mTempDirContent = new ArrayList<FileItem>();
        if (Utils.isSDcardDirectory(true)) {
            searchDocument(Utils.getSDcardDirectoryPath(), mTempDirContent);
        }
        searchDocument(Utils.getInternalDirectoryPath(), mTempDirContent);
        if (mTempDocuContent.size() != mTempDirContent.size()) {
            mTempDocuContent = mTempDirContent;
        }
    }

    public static ArrayList<FileItem> listAllDocumentFiles(Context context) {
        return listAllDocumentFiles(context, false);
    }

    public static ArrayList<FileItem> listAllDocumentFiles(Context context, boolean reset) {
        if (reset) {
            mTempDocuContent = new ArrayList<FileItem>();
        }
        if (mTempDocuContent != null && mTempDocuContent.size() > 0) {
            return mTempDocuContent;
        }

        if (Utils.isSDcardDirectory(true)) {
            searchDocument(Utils.getSDcardDirectoryPath(), mTempDocuContent);
        }
        searchDocument(Utils.getInternalDirectoryPath(), mTempDocuContent);
        return mTempDocuContent;
    }

    public static void searchDocument(String path, ArrayList<FileItem> dirContent) {
        boolean showhidden = MyFileSettings.getShowHiddenFiles();//MyFileSettings.showHiddenFiles();
        File dir = new File(path);
        File[] list = dir.listFiles();

        if (list != null) {
            for (File file : list) {
                if (file.isDirectory()) {
                    searchDocument(file.getPath(), dirContent);
                } else {
                    if (file.exists() && file.canRead() && isDocument(getExtension(file.getName()))) {
                        if (!showhidden) {
                            if (file.getName().charAt(0) != '.') {
                                FileItem item = MainActivity.mMds.checkFile(file.getPath());
                                if (item == null) {
                                    item = new FileItem();
                                    item.setName(file.getName());
                                    item.setPath(file.getPath());
                                    item.setId("");
                                } else {

                                }
                                item.setcheck(false);
                                Log.i("PATH", item.getPath());
                                dirContent.add(item);
                            }
                        } else {
                            FileItem item = MainActivity.mMds.checkFile(file.getPath());
                            if (item == null) {
                                item = new FileItem();
                                item.setName(file.getName());
                                item.setPath(file.getPath());
                                item.setId("");
                            } else {

                            }
                            item.setcheck(false);
                            Log.i("PATH", item.getPath());
                            dirContent.add(item);
                        }
                    }
                }
            }
        }
    }


    public static ArrayList<FileItem> listAllAPKFiles(Context context) {
//        if (!mTempDirContent.isEmpty())
//            mTempDirContent.clear();
//        ArrayList<FileItem> mTempDirContent = new ArrayList<FileItem>();

//        mTempDirContent = new ArrayList<FileItem>();
        if (mTempAppContent != null && mTempAppContent.size() > 0) {
            return mTempAppContent;
        }

        if (Utils.isSDcardDirectory(true)) {
            searchAPK(Utils.getSDcardDirectoryPath(), mTempAppContent);
        }
        searchAPK(Utils.getInternalDirectoryPath(), mTempAppContent);
        return mTempAppContent;
    }

    public static void searchAPK(String path, ArrayList<FileItem> dirContent) {
        boolean showhidden = MyFileSettings.getShowHiddenFiles();//MyFileSettings.showHiddenFiles();
        File dir = new File(path);
        File[] list = dir.listFiles();

        if (list != null) {
            for (File file : list) {
                if (file.isDirectory()) {
                    searchAPK(file.getPath(), dirContent);
                } else {
                    if (file.exists() && file.canRead() && isAPK(getExtension(file.getName()))) {
                        if (!showhidden) {
                            if (file.getName().charAt(0) != '.') {
                                FileItem item = MainActivity.mMds.checkFile(file.getPath());
                                if (item == null) {
                                    item = new FileItem();
                                    item.setName(file.getName());
                                    item.setPath(file.getPath());
                                    item.setId("");
                                } else {

                                }
                                item.setcheck(false);
                                Log.i("PATH", item.getPath());
                                dirContent.add(item);
                            }
                        } else {
                            FileItem item = MainActivity.mMds.checkFile(file.getPath());
                            if (item == null) {
                                item = new FileItem();
                                item.setName(file.getName());
                                item.setPath(file.getPath());
                                item.setId("");
                            } else {

                            }
                            item.setcheck(false);
                            Log.i("PATH", item.getPath());
                            dirContent.add(item);
                        }
                    }
                }
            }
        }
    }

    public static ArrayList<FileItem> listAllZipFiles(Context context) {
//        if (!mTempDirContent.isEmpty())
//            mTempDirContent.clear();
//        ArrayList<FileItem> mTempDirContent = new ArrayList<FileItem>();

//        mTempDirContent = new ArrayList<FileItem>();
        if (mTempZipContent != null && mTempZipContent.size() > 0) {
            return mTempZipContent;
        }

        if (Utils.isSDcardDirectory(true)) {
            searchZip(Utils.getSDcardDirectoryPath(), mTempZipContent);
        }
        searchZip(Utils.getInternalDirectoryPath(), mTempZipContent);
        return mTempZipContent;
    }

    public static void searchZip(String path, ArrayList<FileItem> dirContent) {
        boolean showhidden = MyFileSettings.getShowHiddenFiles();//MyFileSettings.showHiddenFiles();
        File dir = new File(path);
        File[] list = dir.listFiles();

        if (list != null) {
            for (File file : list) {
                if (file.isDirectory()) {
                    searchZip(file.getPath(), dirContent);
                } else {
                    if (file.exists() && file.canRead() && isZip(getExtension(file.getName()))) {
                        if (!showhidden) {
                            if (file.getName().charAt(0) != '.') {
                                FileItem item = MainActivity.mMds.checkFile(file.getPath());
                                if (item == null) {
                                    item = new FileItem();
                                    item.setName(file.getName());
                                    item.setPath(file.getPath());
                                    item.setId("");
                                } else {

                                }
                                item.setcheck(false);
                                Log.i("PATH", item.getPath());
                                dirContent.add(item);
                            }
                        } else {
                            FileItem item = MainActivity.mMds.checkFile(file.getPath());
                            if (item == null) {
                                item = new FileItem();
                                item.setName(file.getName());
                                item.setPath(file.getPath());
                                item.setId("");
                            } else {

                            }
                            item.setcheck(false);
                            Log.i("PATH", item.getPath());
                            dirContent.add(item);
                        }
                    }
                }
            }
        }
    }


    public static ArrayList<FileItem> listTagFiles(String name, Context c) {
        ArrayList<FileItem> mDirContent = new ArrayList<>();
        mDirContent = MainActivity.mMds.selectTagNameFiles(name);

        return mDirContent;
    }

    public static ArrayList<SecretFileItem> listSecretFiles(Context c) {
        ArrayList<SecretFileItem> mDirContent = new ArrayList<>();
        mDirContent = MainActivity.mMds.getAllSecretFile();

        return mDirContent;
    }

    public static ArrayList<FileItem> listFavoriteFiles(Context c) {
        ArrayList<FileItem> mDirContent = new ArrayList<>();
        mDirContent = MainActivity.mMds.getAllFavorite();

        return mDirContent;
    }

//
//        public static void moveToDirectory(File oldFile, File target, Context c) {
//            if (!oldFile.renameTo(target) && copyFile(oldFile, target, c)) {
//                deleteTarget(oldFile.getAbsolutePath());
//            }
//        }
//
//        // TODO: fix copy to sdcard root
//        public static boolean copyFile(final File source, final File target, Context context) {
//            FileInputStream inStream = null;
//            OutputStream outStream = null;
//            FileChannel inChannel = null;
//            FileChannel outChannel = null;
//
//            try {
//                File tempDir = target.getParentFile();
//
//                if (source.isFile())
//                    inStream = new FileInputStream(source);
//
//                if (source.canRead() && tempDir.isDirectory()) {
//                    if (source.isFile()) {
//                        outStream = new FileOutputStream(target);
//                        inChannel = inStream.getChannel();
//                        outChannel = ((FileOutputStream) outStream).getChannel();
//                        inChannel.transferTo(0, inChannel.size(), outChannel);
//                    } else if (source.isDirectory()){
//                        File[] files = source.listFiles();
//
//                        if (createDir(target)) {
//                            for (File file : files) {
//                                copyFile(new File(source, file.getName()), new File(target, file.getName()), context);
//                            }
//                        }
//                    }
//                } else {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        DocumentFile targetDocument = DocumentFile.fromFile(tempDir);
//                        outStream = context.getContentResolver().openOutputStream(targetDocument.getUri());
//                    } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
//                        Uri uri = MediaStoreUtils.getUriFromFile(target.getAbsolutePath(), context);
//                        outStream = context.getContentResolver().openOutputStream(uri);
//                    } else {
//                        return false;
//                    }
//
//                    if (outStream != null && inStream !=null) {
//                        byte[] buffer = new byte[BUFFER];
//                        int bytesRead;
//                        while ((bytesRead = inStream.read(buffer)) != -1) {
//                            outStream.write(buffer, 0, bytesRead);
//                        }
//                    } else {
//                        RootCommands.moveCopyRoot(source.getAbsolutePath(), target.getAbsolutePath());
//                    }
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                return false;
//            } finally {
//                try {
//                    if (inStream != null && outStream != null && inChannel != null && outChannel != null) {
//                        inStream.close();
//                        outStream.close();
//                        inChannel.close();
//                        outChannel.close();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//            return true;
//        }
//
//        // filePath = currentDir + "/" + item
//        // newName = new name
//        public static boolean renameTarget(String filePath, String newName) {
//            File src = new File(filePath);
//
//            String temp = filePath.substring(0, filePath.lastIndexOf("/"));
//            File dest = new File(temp + "/" + newName);
//
//            if (src.renameTo(dest)) {
//                return true;
//            } else {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    DocumentFile document = DocumentFile.fromFile(src);
//
//                    if (document.renameTo(dest.getAbsolutePath())) {
//                        return true;
//                    }
//                }
//            }
//            return false;
//        }
//
//        public static boolean createFile(File file) {
//            if (file.exists()) {
//                return !file.isDirectory();
//            }
//
//            try {
//                if (file.createNewFile()) {
//                    return true;
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                DocumentFile document = DocumentFile.fromFile(file.getParentFile());
//
//                try {
//                    return document.createFile(MimeTypes.getMimeType(file), file.getName()) != null;
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    return false;
//                }
//            }
//            return false;
//        }
//
//        public static boolean createDir(File folder) {
//            if (folder.exists())
//                return false;
//
//            if (folder.mkdir())
//                return true;
//            else {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    DocumentFile document = DocumentFile.fromFile(folder.getParentFile());
//                    if (document.exists())
//                        return true;
//                }
//
////                if (MyFileSettings.rootAccess()) {
////                    return RootCommands.createRootdir(folder);
////                }
//            }
//
//            return false;
//        }
//
//        public static void deleteTarget(String path) {
//            File target = new File(path);
//
//            if (target.isFile() && target.canWrite()) {
//                target.delete();
//            } else if (target.isDirectory() && target.canRead() && target.canWrite()) {
//                String[] fileList = target.list();
//
//                if (fileList != null && fileList.length == 0) {
//                    target.delete();
//                    return;
//                } else if (fileList != null && fileList.length > 0) {
//                    for (String aFile_list : fileList) {
//                        File tempF = new File(target.getAbsolutePath() + "/"
//                                + aFile_list);
//
//                        if (tempF.isDirectory())
//                            deleteTarget(tempF.getAbsolutePath());
//                        else if (tempF.isFile()) {
//                            tempF.delete();
//                        }
//                    }
//                }
//
//                if (target.exists())
//                    target.delete();
////            } else if (!target.delete() && MyFileSettings.rootAccess()) {
////                RootCommands.deleteRootFileOrDir(target);
//            }
//        }
//
//        public static ArrayList<String> searchInDirectory(String dir, String fileName) {
//            ArrayList<String> names = new ArrayList<>();
//            search_file(dir, fileName, names);
//            return names;
//        }
//
//        public static void openFile(final Context context, final File target) {
//            final String mime = MimeTypes.getMimeType(target);
//            final Intent i = new Intent(Intent.ACTION_VIEW);
//
//            if (mime != null) {
//                i.setDataAndType(Uri.fromFile(target), mime);
//            } else {
//                i.setDataAndType(Uri.fromFile(target), "*/*");
//            }
//
//            if (context.getPackageManager().queryIntentActivities(i, 0).isEmpty()) {
//                Toast.makeText(context, R.string.cantopenfile, Toast.LENGTH_SHORT).show();
//                return;
//            }
//
//            try {
//                context.startActivity(i);
//            } catch (Exception e) {
//                Toast.makeText(context, context.getString(R.string.cantopenfile) + e.getMessage(),
//                        Toast.LENGTH_SHORT).show();
//            }
//        }
//
//        // get MD5 or SHA1 checksum from a file
//        public static String getChecksum(File file, String algorithm) {
//            InputStream fis = null;
//            try {
//                fis = new FileInputStream(file);
//                MessageDigest digester = MessageDigest.getInstance(algorithm);
//                byte[] bytes = new byte[2 * BUFFER];
//                int byteCount;
//                String result = "";
//
//                while ((byteCount = fis.read(bytes)) > 0) {
//                    digester.update(bytes, 0, byteCount);
//                }
//
//                for (byte aB : digester.digest()) {
//                    result += Integer.toString((aB & 0xff) + 0x100, 16).substring(1);
//                }
//                return result;
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                if (fis != null) {
//                    try {
//                        fis.close();
//                    } catch (IOException e) {
//                        // ignore
//                    }
//                }
//            }
//            return null;
//        }
//
//        // save current string in ClipBoard
//        public static void savetoClipBoard(final Context co, String dir1) {
//            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) co
//                    .getSystemService(Context.CLIPBOARD_SERVICE);
//            android.content.ClipData clip = android.content.ClipData.newPlainText(
//                    "Copied Text", dir1);
//            clipboard.setPrimaryClip(clip);
//            Toast.makeText(co,
//                    "'" + dir1 + "' " + co.getString(R.string.copiedtoclipboard),
//                    Toast.LENGTH_SHORT).show();
//        }
//
//        public static void createShortcut(Activity main, String path) {
//            File file = new File(path);
//            Intent shortcutIntent;
//
//            try {
//                // Create the intent that will handle the shortcut
//                if (file.isFile()) {
//                    shortcutIntent = new Intent(Intent.ACTION_VIEW);
//                    shortcutIntent.setDataAndType(Uri.fromFile(file), MimeTypes.getMimeType(file));
//                    shortcutIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                } else {
//                    shortcutIntent = new Intent(main, BrowserActivity.class);
//                    shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    shortcutIntent.putExtra(BrowserActivity.EXTRA_SHORTCUT, path);
//                }
//
//                // The intent to send to broadcast for register the shortcut intent
//                Intent intent = new Intent();
//                intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
//                intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, file.getName());
//
//                if (file.isFile()) {
//                    BitmapDrawable bd = (BitmapDrawable) IconPreview.getBitmapDrawableFromFile(file);
//
//                    if (bd != null) {
//                        intent.putExtra(Intent.EXTRA_SHORTCUT_ICON, bd.getBitmap());
//                    } else {
//                        intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
//                                Intent.ShortcutIconResource.fromContext(main, R.drawable.type_unknown));
//                    }
//                } else {
//                    intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
//                            Intent.ShortcutIconResource.fromContext(main, R.drawable.ic_launcher));
//                }
//
//                intent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
//                main.sendBroadcast(intent);
//
//                Toast.makeText(main, main.getString(R.string.shortcutcreated),
//                        Toast.LENGTH_SHORT).show();
//            } catch (Exception e) {
//                e.printStackTrace();
//                Toast.makeText(main, main.getString(R.string.error), Toast.LENGTH_SHORT).show();
//            }
//        }

    public static String formatCalculatedSize(long ls) {
        return formatCalculatedSize(ls, 2);
    }

    public static String formatCalculatedSize(long ls, int pos) {
        BigDecimal size = BigDecimal.valueOf(ls);

        String displaySize;

        if (ls / L_TB_BI > 0) {
            displaySize = String.valueOf(size.divide(TB_BI, pos, BigDecimal.ROUND_HALF_UP)) + " TB";
        } else if (ls / L_GB_BI > 0) {
            displaySize = String.valueOf(size.divide(GB_BI, pos, BigDecimal.ROUND_HALF_UP)) + " GB";
        } else if (ls / L_MB_BI > 0) {
            displaySize = String.valueOf(size.divide(MB_BI, pos, BigDecimal.ROUND_HALF_UP)) + " MB";
        } else if (ls / L_KB_BI > 0) {
            displaySize = String.valueOf(size.divide(KB_BI, pos, BigDecimal.ROUND_HALF_UP)) + " KB";
        } else {
            displaySize = String.valueOf(size) + " bytes";
        }
        return displaySize;
    }

//        public static String formatCalculatedSize(long ls) {
//            BigDecimal size = BigDecimal.valueOf(ls);
//
//            String displaySize;
//
//            if (size.divide(TB_BI).compareTo(BigDecimal.ZERO) > 0) {
//                displaySize = String.valueOf(size.divide(TB_BI, 2, BigDecimal.ROUND_DOWN)) + " TB";
//            } else if (size.divide(GB_BI).compareTo(BigDecimal.ZERO) > 0) {
//                displaySize = String.valueOf(size.divide(GB_BI, 2, BigDecimal.ROUND_DOWN)) + " GB";
//            } else if (size.divide(MB_BI).compareTo(BigDecimal.ZERO) > 0) {
//                displaySize = String.valueOf(size.divide(MB_BI, 2, BigDecimal.ROUND_DOWN)) + " MB";
//            } else if (size.divide(KB_BI).compareTo(BigDecimal.ZERO) > 0) {
//                displaySize = String.valueOf(size.divide(KB_BI, 2, BigDecimal.ROUND_DOWN)) + " KB";
//            } else {
//                displaySize = String.valueOf(size) + " bytes";
//            }
//            return displaySize;
//        }

    public static long getDirectorySize(File directory) {
        final File[] files = directory.listFiles();
        long size = 0;

        if (files == null) {
            return 0L;
        }

        for (final File file : files) {
            try {
                if (!isSymlink(file)) {
                    size += sizeOf(file);
                    if (size < 0) {
                        break;
                    }
                }
            } catch (IOException ioe) {
                // ignore exception when asking for symlink
            }
        }

        return size;
    }

    private static boolean isSymlink(File file) throws IOException {
        File fileInCanonicalDir;

        if (file.getParent() == null) {
            fileInCanonicalDir = file;
        } else {
            File canonicalDir = file.getParentFile().getCanonicalFile();
            fileInCanonicalDir = new File(canonicalDir, file.getName());
        }

        return !fileInCanonicalDir.getCanonicalFile().equals(fileInCanonicalDir.getAbsoluteFile());
    }

    private static long sizeOf(File file) {
        if (file.isDirectory()) {
            return getDirectorySize(file);
        } else {
            return file.length();
        }
    }

    public static String getExtension(String name) {
        String ext;

        if (name.lastIndexOf(".") == -1) {
            ext = "";

        } else {
            int index = name.lastIndexOf(".");
            ext = name.substring(index + 1, name.length());
        }
        return ext;
    }

    public static boolean isSupportedArchive(File file) {
        String ext = getExtension(file.getName());
        return ext.equalsIgnoreCase("zip");
    }

    public static boolean isDocument(String ext) {
        boolean value = false;
        if (ext.equals("pdf") || ext.equals("xls") || ext.equals("xlsx")
                || ext.equals("doc") || ext.equals("docx") || ext.equals("hwp")) {//|| ext.equals("txt") || ext.equals("xml") || ext.equals("html")){
            value = true;
        }
        return value;
    }

    public static boolean isAPK(String ext) {
        boolean value = false;
        if (ext.equals("apk")) {//|| ext.equals("txt") || ext.equals("xml") || ext.equals("html")){
            value = true;
        }
        return value;
    }

    public static boolean isZip(String ext) {
        boolean value = false;
        if (ext.equals("zip") || ext.equals("alz") || ext.equals("gz") || ext.equals("rar") || ext.equals("tar") || ext.equals("tgz")) {//|| ext.equals("txt") || ext.equals("xml") || ext.equals("html")){
            value = true;
        }
        return value;
    }

    public static boolean isUseFile(String ext) {
        boolean value = false;
        if (ext.equals("pdf") || ext.equals("xls") || ext.equals("xlsx")
                || ext.equals("doc") || ext.equals("docx") || ext.equals("hwp")// || ext.equals("xml") || ext.equals("html")
                || ext.equals("jpg") || ext.equals("jpeg") || ext.equals("png") || ext.equals("bmp") || ext.equals("avi") || ext.equals("mp4") || ext.equals("mp3")) {//ext.equals("txt") ||
            value = true;
        }
        return value;
    }

    public static boolean checkDays(File file, int day) {
        boolean value = false;
        if (isUseFile(getExtension(file.getName()))) {
            long curTime = System.currentTimeMillis();
            long regTime = file.lastModified();
            long diffTime = (curTime - regTime) / 1000;
            diffTime /= (60 * 60 * 24);
            if (diffTime <= day) {
                value = true;
            }
        }
        return value;
    }

    public static boolean checkSize(File file, long checksize) {
        boolean value = false;
        if (isUseFile(getExtension(file.getName()))) {
            long size = file.length();
            if (size >= checksize) {
                value = true;
            }
        }
        return value;
    }
}