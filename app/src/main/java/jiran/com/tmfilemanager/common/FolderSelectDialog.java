package jiran.com.tmfilemanager.common;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Stack;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.data.CurrentItem;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.data.StorageListItem;
import jiran.com.tmfilemanager.data.TeamItem;
import jiran.com.tmfilemanager.fagment.MultiFragment;
import jiran.com.tmfilemanager.network.JMF_network;
import jiran.com.tmfilemanager.network.updown.ListRefreshListener;
import jiran.com.tmfilemanager.service.FileDownLoadService;
import jiran.com.tmfilemanager.service.FileUploadService;
import jiran.com.tmfilemanager.settings.MyFileSettings;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

public class FolderSelectDialog extends Dialog {

    public static final int LOCAL_MEMORY = 0, STORAGE_MEMORY = 1;
    public static int ORI_FILE_TYPE;
    public static String ORI_TEAM_ID;
    public static String ORI_OBJECT_ID;
    public int MEMORY_TYPE = LOCAL_MEMORY;

    ListRefreshListener mListener;

    boolean checkFolder;
    Spinner spinner;

    Stack<StorageListItem> listStack;
    TeamItem selectTeamItem;
    FileItem crrentFileItem;

    Activity mActivity;
    ListView lv;
    RelativeLayout layoutUpper;

    String mCurrentPath = Utils.getInternalDirectoryPath();

    TextView mCancel, mOk;

    //AlertDialog alertDialog;
    boolean COPY_MODE = true;
    boolean MOVE_MODE = true;
    boolean isFIRST = true;
    boolean isCATEGORY = false;
    ArrayList<FileItem> top3lists;
    Dialog progressDialog;
    private ImageView dialog_copy_info_icon;
    private LinearLayout mFoler_path_layout, dialog_copy_info_layout;
    private HorizontalScrollView foler_path_scroll_layout;

    public FolderSelectDialog(Activity a) {
        super(a);
        listStack = new Stack<>();
        mActivity = a;
        init();
        //disyplayTop();
        firstList();
    }

    public FolderSelectDialog(Activity a, boolean copy) {
        super(a);
        listStack = new Stack<>();
        mActivity = a;
        COPY_MODE = copy;
        MOVE_MODE = !copy;
        init();
        //disyplayTop();
        firstList();
    }

//	public void setAlertDialog(AlertDialog dialog){
//		alertDialog = dialog;
//	}

    public FolderSelectDialog(@NonNull Context context, @StyleRes int themeResId, Activity a, boolean copy, boolean check) {
        super(context, themeResId);
        mActivity = a;
        COPY_MODE = copy;
        MOVE_MODE = !copy;
        checkFolder = check;
        MEMORY_TYPE = ORI_FILE_TYPE;
        init();
        //disyplayTop();
        firstList();
    }

    public FolderSelectDialog(@NonNull Context context, @StyleRes int themeResId, Activity a, boolean copy, boolean check, ListRefreshListener listener) {
        super(context, themeResId);
        mActivity = a;
        COPY_MODE = copy;
        MOVE_MODE = !copy;
        checkFolder = check;
        MEMORY_TYPE = ORI_FILE_TYPE;
        mListener = listener;
        init();
        //disyplayTop();
        firstList();
    }

    void init() {
        LayoutInflater li = LayoutInflater.from(mActivity);
        RelativeLayout layout = (RelativeLayout) li.inflate(R.layout.dialog_folder_select, null);

        dialog_copy_info_icon = (ImageView) layout.findViewById(R.id.dialog_copy_info_icon);
        dialog_copy_info_layout = (LinearLayout) layout.findViewById(R.id.dialog_copy_info_layout);
        dialog_copy_info_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog_copy_info_layout.isShown()) {
                    dialog_copy_info_layout.setVisibility(View.GONE);
                    dialog_copy_info_icon.setImageResource(R.mipmap.icon_point);
                } else {
                    dialog_copy_info_layout.setVisibility(View.VISIBLE);
                    dialog_copy_info_icon.setImageResource(R.mipmap.icon_close);
                }
            }
        });

        spinner = (Spinner) layout.findViewById(R.id.spinner);

        String[] paths = {mActivity.getResources().getString(R.string.category_loacl_storage), mActivity.getResources().getString(R.string.category_team_storage)};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,
                R.layout.frag_folder_path_item_spinner_tv, paths) {
            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                tv.setBackgroundResource(R.color.main_my_storage_bg);
                // Set the Text color
                tv.setTextColor(Color.parseColor("#FF414141"));
                tv.setTextSize(20);

                return view;
                //return super.getDropDownView(position, convertView, parent);
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        FolderSelectDialog.SpinnerInteractionListener listener = new FolderSelectDialog.SpinnerInteractionListener();
        spinner.setOnTouchListener(listener);
        spinner.setOnItemSelectedListener(listener);

        spinner.setSelection(MEMORY_TYPE);


        lv = (ListView) layout.findViewById(R.id.lv);
        layoutUpper = (RelativeLayout) layout.findViewById(R.id.layout_upper);

        foler_path_scroll_layout = (HorizontalScrollView) layout.findViewById(R.id.foler_path_scroll_layout);
        mFoler_path_layout = (LinearLayout) layout.findViewById(R.id.foler_path_layout);

        mCancel = (TextView) layout.findViewById(R.id.cancel);
        mOk = (TextView) layout.findViewById(R.id.ok);

        TextView function_title_tv = (TextView) layout.findViewById(R.id.function_title_tv);
        if (COPY_MODE) {
            function_title_tv.setText(mActivity.getResources().getString(R.string.snackbar_copy));
            mOk.setText(mActivity.getResources().getString(R.string.menu_copy));
        } else {
            function_title_tv.setText(mActivity.getResources().getString(R.string.snackbar_move));
            mOk.setText(mActivity.getResources().getString(R.string.menu_move));
        }

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ORI_FILE_TYPE == LOCAL_MEMORY) {
                    if (MEMORY_TYPE == LOCAL_MEMORY) {
                        PasteTaskExecutor ptc = new PasteTaskExecutor(mActivity, mCurrentPath);
                        ptc.start();
                    } else {
                        //Server Upload
                        if (FileDownLoadService.mCurrentPath != null && FileDownLoadService.mCurrentPath.length() > 0) {
                            Toast.makeText(mActivity, mActivity.getString(R.string.dialog_download_service_used), Toast.LENGTH_SHORT).show();
                        } else if (FileUploadService.mTeam_id == null || FileUploadService.mTeam_id.length() == 0) {
                            FileUploadService.mTeam_id = selectTeamItem.getId();
                            FileUploadService.mObject_id = crrentFileItem.getId();
                            FileUploadService.uploadFileList = ClipBoard.getClipBoardContents();
                            Intent intent = new Intent(mActivity, FileUploadService.class);
                            mActivity.startService(intent);
                            if (mListener != null) {
                                mListener.refresh(null);
                            }
                        } else {
                            Toast.makeText(mActivity, mActivity.getString(R.string.dialog_upload_service_used), Toast.LENGTH_SHORT).show();
                        }
                        //new UploadTask(mActivity, mListener).execute(selectTeamItem.getId(), crrentFileItem.getId());
                    }
                } else {
                    if (MEMORY_TYPE == LOCAL_MEMORY) {
                        //Local Download
                        //mCurrentPath;
                        if (FileUploadService.mTeam_id != null && FileUploadService.mTeam_id.length() > 0) {
                            Toast.makeText(mActivity, mActivity.getString(R.string.dialog_upload_service_used), Toast.LENGTH_SHORT).show();
                        } else if (FileDownLoadService.mCurrentPath == null || FileDownLoadService.mCurrentPath.length() == 0) {
                            FileDownLoadService.mCurrentPath = mCurrentPath;
                            FileDownLoadService.downloadFileList = ClipBoard.getClipBoardContents();
                            Intent intent = new Intent(mActivity, FileDownLoadService.class);
                            mActivity.startService(intent);
                            if (mListener != null) {
                                mListener.refresh(null);
                            }
                        } else {
                            Toast.makeText(mActivity, mActivity.getString(R.string.dialog_download_service_used), Toast.LENGTH_SHORT).show();
                        }
                        //new DownloadTask(mActivity, mCurrentPath).execute(selectTeamItem.getId(), crrentFileItem.getId());
                    } else {
                        //Server to Server
                    }
                }

                dismiss();
            }
        });
        setContentView(layout);

//		if(COPY_MODE){
//			setNeutralButton(mActivity.getResources().getString(R.string.snackbar_copy), new OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
////					if(isFIRST){
////							Toast.makeText(mActivity, mActivity.getResources().getString(R.string.selectfolder), Toast.LENGTH_SHORT).show();
////					}else{
////						requestCopy("copy", filelist, folderlist);
////					}
//					PasteTaskExecutor ptc = new PasteTaskExecutor(mActivity, mCurrentPath);
//					ptc.start();
//					//Toast.makeText(mActivity, mActivity.getResources().getString(R.string.copysuccsess), Toast.LENGTH_SHORT).show();
//				}
//			});
//		}
//		if(MOVE_MODE){
//			setNegativeButton(mActivity.getResources().getString(R.string.snackbar_move), new OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
////					if(isFIRST){
////							Toast.makeText(mActivity, mActivity.getResources().getString(R.string.selectfolder), Toast.LENGTH_SHORT).show();
////					}else{
////						requestCopy("move", filelist, folderlist);
////					}
//					PasteTaskExecutor ptc = new PasteTaskExecutor(mActivity, mCurrentPath);
//					ptc.start();
//					//Toast.makeText(mActivity, mActivity.getResources().getString(R.string.movesuccsess), Toast.LENGTH_SHORT).show();
//				}
//			});
//		}
    }


//	public void disyplayTop(){
//		isFIRST = true;
//		layoutUpper.setVisibility(View.GONE);
//		mCurrentPath = Utils.getInternalDirectoryPath();
//		FolderSelectAdapter adapter = new FolderSelectAdapter(mActivity);
//		lv.setAdapter(adapter);
//		lv.setOnItemClickListener(new OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
//				final File file = new File(((FileItem) lv.getAdapter()
//						.getItem(position)).getPath());
//
//				if (file.isDirectory()) {
//					displayList(file.getAbsolutePath());
//				}
//			}
//		});
//		adapter.addFiles(mCurrentPath);
//
//		foler_path_scroll_layout.post(new Runnable() {
//			public void run() {
//				foler_path_scroll_layout.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
//			}
//		});
//		setFolderFathLayout(mCurrentPath, false);
//	}

    public void firstList() {
        top3lists = new ArrayList<FileItem>();

        if (MEMORY_TYPE == LOCAL_MEMORY) {
            isCATEGORY = false;

            FileItem item = new FileItem();
            item.setName(mActivity.getResources().getString(R.string.category_image));
            item.setPath(Utils.getImageDirectoryPath());
            item.setIconResoruce(R.mipmap.list_picture);
            top3lists.add(item);

            FileItem item2 = new FileItem();
            item2.setName(mActivity.getResources().getString(R.string.category_download));
            item2.setPath(Utils.getDownloadDirectoryPath());
            item2.setIconResoruce(R.mipmap.list_recentfile);
            top3lists.add(item2);

            FileItem item5 = new FileItem();
            item5.setName(mActivity.getResources().getString(R.string.category_music));
            item5.setPath(Utils.getMusicDirectoryPath());
            item5.setIconResoruce(R.mipmap.list_music);
            top3lists.add(item5);

            FileItem item6 = new FileItem();
            item6.setName(mActivity.getResources().getString(R.string.category_video));
            item6.setPath(Utils.getVideoDirectoryPath());
            item6.setIconResoruce(R.mipmap.list_movie);
            top3lists.add(item6);

            FileItem item3 = new FileItem();
            item3.setName(mActivity.getResources().getString(R.string.internaldirectory));
            item3.setPath(Utils.getInternalDirectoryPath());
            top3lists.add(item3);

            if (Utils.isSDcardDirectory(true)) {
                FileItem item4 = new FileItem();
                item4.setName(mActivity.getResources().getString(R.string.sdcard));
                item4.setPath(Utils.getSDcardDirectoryPath());
                top3lists.add(item4);
            }
        } else {
            for (TeamItem team : MultiFragment.teamItams) {
                FileItem item = new FileItem();
                item.setName(team.getTeamName());
                item.setPath(team.getId());
                item.setIconResoruce(R.mipmap.list_teamstorage);
                top3lists.add(item);
            }
        }
        disyplayTop(top3lists);

    }

    public void disyplayTop(ArrayList<FileItem> list) {
        isFIRST = true;
        layoutUpper.setVisibility(View.GONE);
        mOk.setVisibility(View.INVISIBLE);
        mCurrentPath = Utils.getInternalDirectoryPath();
        FolderSelectAdapter adapter = new FolderSelectAdapter(mActivity);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                if (MEMORY_TYPE == LOCAL_MEMORY) {
                    final File file = new File(((FileItem) lv.getAdapter()
                            .getItem(position)).getPath());

                    if (((FileItem) lv.getAdapter().getItem(position)).getIconResoruce() != -1) {
                        isCATEGORY = true;
                    }

                    if (file.isDirectory()) {
                        displayList(file.getAbsolutePath());
                    }
                } else {
                    getStorageRoot(position);
                }
            }
        });
        adapter.display(list);
        listStack = new Stack<>();
        StorageListItem storageListItem = new StorageListItem(null, list);
        listStack.add(storageListItem);

        foler_path_scroll_layout.post(new Runnable() {
            public void run() {
                foler_path_scroll_layout.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        });
        setFolderFathLayout(mCurrentPath, true);
    }

    public void displayList(String path) {
        isFIRST = false;
        layoutUpper.setVisibility(View.VISIBLE);
        mOk.setVisibility(View.VISIBLE);
        mCurrentPath = path;

        FolderSelectAdapter adapter = new FolderSelectAdapter(mActivity);
        adapter.addFiles(mCurrentPath);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                //getList(list.get(position).getNode());
                if (MEMORY_TYPE == LOCAL_MEMORY) {
                    final File file = new File(((FileItem) lv.getAdapter()
                            .getItem(position)).getPath());

                    if (file.isDirectory()) {
                        displayList(file.getAbsolutePath());
                    }
                } else {

                }
            }
        });

        layoutUpper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//				listStack.pop();
//				nodeStack.pop();
//				currentNode = nodeStack.get(nodeStack.size() - 1);
//				DLog.d("jiran list", listStack.size() +"");
                if (MEMORY_TYPE == LOCAL_MEMORY) {
                    File file = new File(mCurrentPath);
                    //if(file.getParent().equals(Utils.getInternalDirectoryPath()))
                    if ((file.getParent().equals(Utils.getInternalDirectoryPath()) && isCATEGORY) || (file.getParent().equals(Utils.getSDcardDirectoryPath()) && isCATEGORY)) {
                        firstList();
                    } else {
                        if (file.getAbsolutePath().equals(Utils.getInternalDirectoryPath()) || file.getAbsolutePath().equals(Utils.getSDcardDirectoryPath())) {
                            firstList();
                        } else {
                            displayList(file.getParent());
                        }
                    }
                } else {

                }
            }
        });

        foler_path_scroll_layout.post(new Runnable() {
            public void run() {
                foler_path_scroll_layout.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        });
        setFolderFathLayout(mCurrentPath, false);
    }

    public void displayStorageList(StorageListItem list, boolean back) {
        isFIRST = false;
        layoutUpper.setVisibility(View.VISIBLE);
        mOk.setVisibility(View.VISIBLE);

        FolderSelectAdapter adapter = new FolderSelectAdapter(mActivity);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                //getList(list.get(position).getNode());
                if (MEMORY_TYPE == LOCAL_MEMORY) {

                } else {
                    crrentFileItem = list.getList().get(position);
                    getStorageList(selectTeamItem.getId(), crrentFileItem.getId());
                }
            }
        });

        layoutUpper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//				listStack.pop();
//				nodeStack.pop();
//				currentNode = nodeStack.get(nodeStack.size() - 1);
//				DLog.d("jiran list", listStack.size() +"");
                if (MEMORY_TYPE == LOCAL_MEMORY) {

                } else {
                    if (listStack.size() <= 2) {
                        firstList();
                    } else {
                        listStack.pop();
                        displayStorageList(listStack.get(listStack.size() - 1), true);
                    }
                }
            }
        });
        if (!back) {
            listStack.add(list);
        }
        adapter.display(list.getList());
        foler_path_scroll_layout.post(new Runnable() {
            public void run() {
                foler_path_scroll_layout.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
            }
        });
        setFolderFathLayout(mCurrentPath, false);
    }

    public void setFolderFathLayout(String path, boolean onlyhome) {
        mFoler_path_layout.removeAllViews();
        LayoutInflater inflater = LayoutInflater.from(mActivity);
        //Home Image add
        String HHOOMMEEPATH = "";
        boolean internal = true;
        if (onlyhome) {
            path = File.separator + "HHOOMMEE";
        } else if (path.contains(Utils.getInternalDirectoryPath())) {
            internal = true;
            String[] lastPath = Utils.getInternalDirectoryPath().split("/");
            path = File.separator + "HHOOMMEE" + File.separator + lastPath[lastPath.length - 1] + path.replaceFirst(Utils.getInternalDirectoryPath(), "");
            HHOOMMEEPATH = Utils.getInternalDirectoryPath().replaceFirst(File.separator + lastPath[lastPath.length - 1], "");
        } else if (Utils.isSDcardDirectory(true) && path.contains(Utils.getSDcardDirectoryPath())) {
            internal = false;
            String[] lastPath = Utils.getSDcardDirectoryPath().split("/");
            path = File.separator + "HHOOMMEE" + File.separator + lastPath[lastPath.length - 1] + path.replaceFirst(Utils.getSDcardDirectoryPath(), "");
            HHOOMMEEPATH = Utils.getSDcardDirectoryPath().replaceFirst(File.separator + lastPath[lastPath.length - 1], "");
        }
        String[] arrayPath = path.split("/");
        if (path.contains("/") && arrayPath.length > 1) {

            String makePath = "";
            int homeIndex = -1;
            for (int i = 1; i < arrayPath.length; i++) {
                String sPath = arrayPath[i];
                if (sPath.equals("HHOOMMEE")) {
                    homeIndex = i;
                    View home_view = inflater.inflate(R.layout.frag_folder_path_item_home, null, false);
                    makePath += HHOOMMEEPATH;
                    home_view.setTag(makePath);
                    home_view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //disyplayTop();
                            firstList();
                        }
                    });
                    mFoler_path_layout.addView(home_view);
                } else {
                    View path_view = inflater.inflate(R.layout.frag_folder_path_item, null, false);
                    TextView foler_path = (TextView) path_view.findViewById(R.id.foler_path);
                    if (homeIndex == (i - 1)) {
                        if (internal) {
                            foler_path.setText("/" + mActivity.getString(R.string.internaldirectory));
                        } else {
                            foler_path.setText("/" + mActivity.getString(R.string.sdcard));
                        }
                    } else {
                        foler_path.setText("/" + sPath);
                    }
                    makePath += ("/" + sPath);
                    path_view.setTag(makePath);
                    path_view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            displayList((String) v.getTag());
                        }
                    });
                    mFoler_path_layout.addView(path_view);
                }
            }
        } else {
            View path_view = inflater.inflate(R.layout.frag_folder_path_item, null, false);
            TextView foler_path = (TextView) path_view.findViewById(R.id.foler_path);

            if (path.equals(mActivity.getString(R.string.category_document)) ||
                    path.equals(mActivity.getString(R.string.category_recentfile)) ||
                    path.equals(mActivity.getString(R.string.category_largefile)) ||
                    path.equals(mActivity.getString(R.string.category_image)) ||
                    path.equals(mActivity.getString(R.string.category_video)) ||
                    path.equals(mActivity.getString(R.string.category_favorite)) ||
                    path.equals(mActivity.getString(R.string.category_download)) ||
                    path.equals(mActivity.getString(R.string.category_music))) {
                foler_path.setText(File.separator + path);
                View home_view = inflater.inflate(R.layout.frag_folder_path_item_home, null, false);
                home_view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //disyplayTop();
                        firstList();
                    }
                });
                mFoler_path_layout.addView(home_view);
            } else {
                foler_path.setText(path);
            }

            mFoler_path_layout.addView(path_view);
        }
    }

    public void getStorageRoot(int position) {
        selectTeamItem = MultiFragment.teamItams.get(position);
        try {
            showProgress();
            JMF_network.getRootPath(selectTeamItem.getId())
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF getRootPath", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    JsonObject current = result.getAsJsonObject("current");
                                    CurrentItem currentItem = new CurrentItem();
                                    currentItem.setObjectId(current.get("object_id").getAsString());
                                    currentItem.setNode(current.get("node").getAsString());
                                    currentItem.setName(current.get("name").getAsString());
                                    currentItem.setSize(current.get("size").getAsString());
                                    currentItem.setRegdate(Utils.formatDate(current.get("regdate").getAsString()));
                                    currentItem.setModified(Utils.formatDate(current.get("modified").getAsString()));

                                    JsonObject lists = result.getAsJsonObject("lists");
                                    JsonArray rows = lists.getAsJsonArray("rows");

                                    crrentFileItem = new FileItem();
                                    crrentFileItem.setId(currentItem.getObjectId());
                                    crrentFileItem.setName(currentItem.getName());

                                    ArrayList<FileItem> storageList = new ArrayList<FileItem>();
                                    for (int i = 0; i < rows.size(); i++) {
                                        FileItem item = new FileItem();
                                        JsonObject row = rows.get(i).getAsJsonObject();
                                        item.setId(row.get("object_id").getAsString());
                                        item.setSObjectType(row.get("object_type").getAsString());
                                        item.setSNode(row.get("node").getAsString());
                                        item.setName(row.get("name").getAsString());
                                        item.setSCreator(row.get("creator").getAsString());
                                        item.setSCreatorName(row.get("creator_name").getAsString());
                                        item.setSMemo(row.get("memo").getAsString());
                                        item.setSize(row.get("size").getAsString());
                                        item.setSExtension(row.get("extension").getAsString());
                                        item.setRegDate(Utils.formatDate(row.get("regdate").getAsString()));
                                        item.setSModifedDate(Utils.formatDate(row.get("modified").getAsString()));

                                        if (item.getSObjectType().equals("D")) {
                                            storageList.add(item);
                                        }
                                    }
                                    StorageListItem storageListItem = new StorageListItem(currentItem, storageList);
                                    displayStorageList(storageListItem, false);
                                    //Toast.makeText(mMainContext, "object_id => " + object_id, Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(mActivity, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
            Toast.makeText(mActivity, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void getStorageList(String teamId, String objectId) {
        try {
            showProgress();

            String sort = "";
            if (MyFileSettings.getSortReverse()) {
                sort = "+";
            } else {
                sort = "-";
            }
            switch (MyFileSettings.getSortType()) {
                case SortUtils.SORT_ALPHA:
                    sort += "name";
                    break;
                case SortUtils.SORT_TYPE:
                    sort += "extension";
                    break;
                case SortUtils.SORT_SIZE:
                    sort += "size";
                    break;
                case SortUtils.SORT_DATE:
                    sort += "regdate";
                    break;
                default:
                    sort += "name";
                    break;
            }

            JMF_network.getFolderList(teamId, objectId, sort, "10000", "", "")
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF getStorageList", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");

                                    JsonObject current = result.getAsJsonObject("current");
                                    CurrentItem currentItem = new CurrentItem();
                                    currentItem.setObjectId(current.get("object_id").getAsString());
                                    currentItem.setNode(current.get("node").getAsString());
                                    currentItem.setName(current.get("name").getAsString());
                                    currentItem.setCategoryId("");
                                    currentItem.setSize(current.get("size").getAsString());
                                    currentItem.setRegdate(Utils.formatDate(current.get("regdate").getAsString()));
                                    currentItem.setModified(Utils.formatDate(current.get("modified").getAsString()));

                                    JsonObject lists = result.getAsJsonObject("lists");
                                    JsonArray rows = lists.getAsJsonArray("rows");
                                    ArrayList<FileItem> storageList = new ArrayList<FileItem>();
                                    for (int i = 0; i < rows.size(); i++) {
                                        FileItem item = new FileItem();
                                        JsonObject row = rows.get(i).getAsJsonObject();
                                        item.setId(row.get("object_id").getAsString());
                                        item.setSObjectType(row.get("object_type").getAsString());
                                        item.setSNode(row.get("node").getAsString());
                                        item.setName(row.get("name").getAsString());
                                        item.setSCreator(row.get("creator").getAsString());
                                        item.setSCreatorName(row.get("creator_name").getAsString());
                                        item.setSMemo(row.get("memo").getAsString());
                                        item.setSize(row.get("size").getAsString());
                                        item.setSExtension(row.get("extension").getAsString());
                                        item.setRegDate(Utils.formatDate(row.get("regdate").getAsString()));
                                        item.setSModifedDate(Utils.formatDate(row.get("modified").getAsString()));

                                        if (item.getSObjectType().equals("D")) {
                                            storageList.add(item);
                                        }
                                    }

                                    StorageListItem storageListItem = new StorageListItem(currentItem, storageList);
                                    displayStorageList(storageListItem, false);

                                } else {
                                    Toast.makeText(mActivity, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
            Toast.makeText(mActivity, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void showProgress() {
        progressDialog = Utils.Progress(mActivity, "", "Login...", false, 1);
        progressDialog.show();
        ;
    }

    public void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    class FolderSelectAdapter extends BaseAdapter {
        public String mCurrentPath;
        public ArrayList<Bitmap> iconsSmall;
        ArrayList<FileItem> items;
        Context mContext;

        public FolderSelectAdapter(Context a) {
            super();
            mContext = a;
            items = new ArrayList<FileItem>();
            //items = list;
        }

        public int getCount() {
            return items.size();
        }

        public Object getItem(int position) {
            return items.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

//		public boolean getItemSelected(int position) {
//			return items.get(position).getSelected();
//		}

        public int getViewTypeCount() {
            return 2;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            View view = convertView;
            final FileItem item = (FileItem) getItem(position);
            if (view != null) {
                holder = (ViewHolder) view.getTag();
            } else {
                final LayoutInflater inflater = LayoutInflater.from(mContext);
                view = inflater.inflate(R.layout.list_folder_select, parent, false);
                holder = new ViewHolder();
                holder.filename = ((TextView) view.findViewById(R.id.filename));
                holder.filedate = ((TextView) view.findViewById(R.id.filedate));
                holder.fileimage = ((ImageView) view.findViewById(R.id.icon));
                view.setTag(holder);
            }

            int mimeIcon = 0;
            if (MEMORY_TYPE == LOCAL_MEMORY) {
                final File file = new File(item.getPath());
                if (file != null && file.isDirectory()) {
                    String[] files = file.list();
                    if (file.canRead() && files != null && files.length > 0) {
                        mimeIcon = R.drawable.folder;
                        //mimeIcon = mResources.getDrawable(R.drawable.fd_contain_l);
                    } else {
                        mimeIcon = R.drawable.folder;
                        //mimeIcon = mResources.getDrawable(R.drawable.fd_l);
                    }
                } else if (file != null && file.isFile()) {
                    final String fileExt = ListFunctionUtils.getExtension(file.getName());
                    mimeIcon = MimeTypes.getIconForExt(fileExt);
                }
            } else {
                mimeIcon = R.drawable.folder;
            }


            if (item.getIconResoruce() != -1) {
                holder.fileimage.setImageResource(item.getIconResoruce());
            } else if (mimeIcon != 0) {
                //icon.setImageDrawable(mimeIcon);
                holder.fileimage.setImageResource(mimeIcon);
            } else {
                // default icon
                holder.fileimage.setImageResource(R.drawable.file_etc);
            }
            holder.filename.setText(item.getName());
//			DateFormat df = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault());
//			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy. MM. dd. ");
//			String date = simpleDateFormat.format(file.lastModified());
//
//			//holder.filedate.setText(date + df.format(file.lastModified()));
//			holder.filedate.setText(date);

            holder.filedate.setVisibility(View.GONE);
            return view;
        }

        public void addFiles(String path) {
            if (!items.isEmpty())
                items.clear();

            mCurrentPath = path;
            getFiles();
        }

        public void display(ArrayList<FileItem> list) {
            items = list;
            notifyDataSetChanged();
        }

        public void getFiles() {
            ArrayList<FileItem> temp = null;
            try {
                if (mCurrentPath.equals(mContext.getResources().getString(R.string.category_image))) {
                    temp = ListFunctionUtils.listAllImageFiles(mContext);
                } else if (mCurrentPath.equals(mContext.getResources().getString(R.string.category_video))) {
                    temp = ListFunctionUtils.listAllVideoFiles(mContext);
                } else if (mCurrentPath.equals(mContext.getResources().getString(R.string.category_music))) {
                    temp = ListFunctionUtils.listAllAudioFiles(mContext);
                } else {
                    temp = ListFunctionUtils.listFiles(mCurrentPath, mContext);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            for (FileItem a : temp) {
                if (new File(mCurrentPath + "/" + a.getName()).isDirectory())
                    items.add(a);
            }

            // sort files with a comparator if not empty
            if (!items.isEmpty()) {
                SortUtils.sortList(items, mCurrentPath, MyFileSettings.getSortType());
            }

            notifyDataSetChanged();
        }

        private class ViewHolder {
            TextView filename;
            TextView filedate;
            ImageView fileimage;
        }
    }

    public class SpinnerInteractionListener implements AdapterView.OnItemSelectedListener, View.OnTouchListener {

        boolean userSelect = false;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            userSelect = true;
            return false;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            if (userSelect) {
                // Your selection handling code here
                switch (pos) {
                    case 0:
                        // Whatever you want to happen when the first item gets selected
                        //folderListDisplay(Utils.getInternalDirectoryPath());
                        if (checkFolder && ORI_FILE_TYPE == STORAGE_MEMORY) {
                            Toast.makeText(mActivity, mActivity.getString(R.string.dialog_selectfolder_checkfolder), Toast.LENGTH_SHORT).show();
                            spinner.setSelection(STORAGE_MEMORY);
                        } else {
                            Toast.makeText(mActivity, "" + pos, Toast.LENGTH_SHORT).show();
                            MEMORY_TYPE = LOCAL_MEMORY;
                            firstList();
                        }
                        break;
                    case 1:
                        // Whatever you want to happen when the second item gets selected
                        //folderListDisplay(Utils.getSDcardDirectoryPath());
                        if (checkFolder && ORI_FILE_TYPE == LOCAL_MEMORY) {
                            Toast.makeText(mActivity, mActivity.getString(R.string.dialog_selectfolder_checkfolder), Toast.LENGTH_SHORT).show();
                            spinner.setSelection(LOCAL_MEMORY);
                        } else {
                            Toast.makeText(mActivity, "" + pos, Toast.LENGTH_SHORT).show();
                            MEMORY_TYPE = STORAGE_MEMORY;
                            firstList();
                        }
                        break;
                }
                userSelect = false;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }
}
