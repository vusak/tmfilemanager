package jiran.com.tmfilemanager.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.barteksc.pdfviewer.PDFView;

import java.io.File;
import java.util.ArrayList;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.common.MimeTypes;
import jiran.com.tmfilemanager.data.FileItem;

/**
 * Created by user on 2017-02-06.
 */

public class SlideImageAdapter extends PagerAdapter {

    private Activity _activity;
    private ArrayList<FileItem> _fileArray;
    private LayoutInflater inflater;

    // constructor
    public SlideImageAdapter(Activity activity,
                             ArrayList<FileItem> items) {
        this._activity = activity;
        this._fileArray = items;
    }

    @Override
    public int getCount() {
        return this._fileArray.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        final ProgressBar progress;

        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout;// = inflater.inflate(R.layout.activity_slide_view_layout, container, false);

        FileItem item = _fileArray.get(position);
        File file = new File(item.getPath());

        if (MimeTypes.isPdf(file)) {
            viewLayout = inflater.inflate(R.layout.activity_slide_view_pdf_layout, container, false);
            PDFView pdfView = (PDFView) viewLayout.findViewById(R.id.pdfView);
            pdfView.fromFile(file).load();
        } else if (MimeTypes.isVideo(file)) {
            viewLayout = inflater.inflate(R.layout.activity_slide_view_movie_layout, container, false);
            FrameLayout content_frame_sub = (FrameLayout) viewLayout.findViewById(R.id.content_frame_sub);

        } else {
            viewLayout = inflater.inflate(R.layout.activity_slide_view_layout, container, false);

            ImageView imgDisplay = (ImageView) viewLayout.findViewById(R.id.imgDisplay);
            progress = (ProgressBar) viewLayout.findViewById(R.id.progress);

            Glide.with(_activity)
                    .load(new File(item.getPath()))
                    .placeholder(R.color.black)
                    .listener(new RequestListener<File, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, File model, Target<GlideDrawable> target, boolean isFirstResource) {
                            progress.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, File model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            progress.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    //.bitmapTransform(new CenterCrop(this), new RoundedCornersTransformation(this, 10, 0))
                    .into(imgDisplay);
        }

        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);

    }
}