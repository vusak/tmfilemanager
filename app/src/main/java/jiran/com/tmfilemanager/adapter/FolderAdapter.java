package jiran.com.tmfilemanager.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.common.ListFunctionUtils;
import jiran.com.tmfilemanager.common.MimeTypes;
import jiran.com.tmfilemanager.common.SortUtils;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.data.TagItem;
import jiran.com.tmfilemanager.fagment.FolderFragment;
import jiran.com.tmfilemanager.settings.MyFileSettings;
import jiran.com.tmfilemanager.utils.LogUtil;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

/**
 * Created by user on 2016-08-03.
 */
public class FolderAdapter extends BaseAdapter {

    public static boolean listChange = false;

    private final LayoutInflater mInflater;
    private final Resources mResources;
    private final Context mContext;
    private final int LIST = 0;
    private final int TAG = 1;
    private final int FAVORITE = 2;
    ThumbLoader mThumbLoader;
    private ArrayList<FileItem> mDataSource;
    private String mCurrentPath;
    private int mType;
    private int refreshType = LIST;
    private TagItem refreshTagItem;


    public FolderAdapter(Context context, LayoutInflater inflater, int type) {
        super();
        mInflater = inflater;
        mContext = context;
        mType = type;
        mDataSource = new ArrayList<>();
        mResources = context.getResources();

        mThumbLoader = new ThumbLoader(context);
    }

    public FolderAdapter(Context context, LayoutInflater inflater, int type, ArrayList<FileItem> dataSource) {
        super();
        mInflater = inflater;
        mContext = context;
        mType = type;
        mDataSource = dataSource;
        mResources = context.getResources();

        mThumbLoader = new ThumbLoader(context);
    }

    @Override
    public int getCount() {
        return mDataSource.size();
    }

    @Override
    public FileItem getItem(int position) {
        return mDataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder mViewHolder;
//        DateFormat df = DateFormat.getDateTimeInstance(DateFormat.SHORT,
//                DateFormat.SHORT, Locale.getDefault());

        DateFormat df = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy. MM. dd. ");

        if (convertView != null) {
            mViewHolder = (ViewHolder) convertView.getTag();
            //holder.fileimage.setImageResource(R.drawable.stub);
        } else {
            final LayoutInflater inflater = LayoutInflater.from(mContext);
            if (mType == 0) {
                convertView = inflater.inflate(R.layout.list_folder, parent, false);
            } else {
                convertView = inflater.inflate(R.layout.grid_folder, parent, false);
            }
            mViewHolder = new ViewHolder(convertView);
            convertView.setTag(mViewHolder);
        }
        final FileItem item = getItem(position);
        final File file = new File(item.getPath());

        if (MimeTypes.isPicture(file)) {
            //if ( MimeTypes.isPicture(file) || MimeTypes.isVideo(file)) {
//            mThumbLoader.DisplayImage(mContext, item.getPath(), item.getRegDate(), "0", mViewHolder.icon, false, false);

            Glide.with(mContext)
                    .load(file)
                    .placeholder(R.drawable.file_picture)
                    .bitmapTransform(new CenterCrop(mContext), new RoundedCornersTransformation(mContext, mType == 0 ? 10 : 0, 0))
                    .error(MimeTypes.isPicture(file) ? R.drawable.file_picture : R.drawable.file_movie)
                    .into(mViewHolder.icon);

        } else if (MimeTypes.isVideo(file)) {
            Handler han = new Handler();
            han.post(() -> {
                try {
                    Bitmap bitmap2 = ThumbnailUtils.createVideoThumbnail(file.getPath(), MediaStore.Images.Thumbnails.MINI_KIND);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap2.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    Glide.with(mContext)
                            .load(stream.toByteArray())
                            .placeholder(R.drawable.file_movie)
                            .bitmapTransform(new CenterCrop(mContext), new RoundedCornersTransformation(mContext, mType == 0 ? 10 : 0, 0))
                            .error(R.drawable.file_movie)
                            .into(mViewHolder.icon);

                } catch (Exception e) {
                    LogUtil.log(e.getMessage());
                }
            });
        } else {
            int mimeIcon = 0;

            if (file != null && file.isDirectory()) {
                String[] files = file.list();
                if (file.canRead() && files != null && files.length > 0) {
                    mimeIcon = R.drawable.folder;
                    //mimeIcon = mResources.getDrawable(R.drawable.fd_contain_l);
                } else {
                    mimeIcon = R.drawable.folder;
                    //mimeIcon = mResources.getDrawable(R.drawable.fd_l);
                }
            } else if (file != null && file.isFile()) {
                final String fileExt = ListFunctionUtils.getExtension(file.getName());
                mimeIcon = MimeTypes.getIconForExt(fileExt);
            }

            if (mimeIcon != 0) {
                //icon.setImageDrawable(mimeIcon);
                mViewHolder.icon.setImageResource(mimeIcon);
            } else {
                // default icon
                mViewHolder.icon.setImageResource(R.drawable.file_etc);
            }
        }
        //IconPreview.getFileIcon(file, mViewHolder.icon);

        if (FolderFragment.mActionMode) {
            mViewHolder.list_checker_layout.setVisibility(View.VISIBLE);
            mViewHolder.list_item_menu.setVisibility(View.GONE);
        } else {
            mViewHolder.list_checker_layout.setVisibility(View.GONE);
            mViewHolder.list_item_menu.setVisibility(View.VISIBLE);
        }

//        mViewHolder.list_checker.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                if(mViewHolder.list_checker.isChecked()){
//                    setCheck(position, true);
//                }else{
//                    setCheck(position, false);
//                }
//            }
//        });
        mViewHolder.list_checker.setChecked(item.getcheck());
        if (mViewHolder.titleLayout != null) {
            mViewHolder.titleLayout.setBackgroundResource(item.getcheck() ? R.color.navy_a100 : R.color.black_a50);
        }

        mViewHolder.filename.setText(file.getName());
        String date = simpleDateFormat.format(file.lastModified());

        mViewHolder.filedate.setText(date + df.format(file.lastModified()));
        if (mType == 0) {
            mViewHolder.filedate.setVisibility(View.VISIBLE);
        } else {
            mViewHolder.filedate.setVisibility(View.GONE);
        }

        if (item.getFavorite()) {
            mViewHolder.favorite_icon.setVisibility(View.VISIBLE);
        } else {
            mViewHolder.favorite_icon.setVisibility(View.GONE);
        }
        if (item.getTagName() != null && item.getTagName().length() > 0) {
//            mViewHolder.tag_icon.setImageResource(Utils.getTagColorResourceSmall(item.getTagColor()));
//            if(mType == 0) {
//                mViewHolder.tag_icon.setImageResource(Utils.getTagColorResourceSmall(item.getTagColor()));
//            }else{
//                mViewHolder.tag_icon.setImageResource(Utils.getTagColorResourceLb(item.getTagColor()));
//            }
            mViewHolder.tag_icon.setVisibility(View.VISIBLE);
//            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//            lp.setMargins(10, 0, 0, 0);
//            mViewHolder.tag_icon.setLayoutParams(lp);
        } else {
            mViewHolder.tag_icon.setVisibility(View.GONE);
        }

        if (mType == 0) {
            if (!file.isDirectory()) {
                mViewHolder.filesize.setText(ListFunctionUtils.formatCalculatedSize(file.length()));
            } else {
                mViewHolder.filesize.setText("");
            }
        }

        if (item.getName().startsWith(".")) {
            mViewHolder.showhidden.setVisibility(View.VISIBLE);
        } else {
            mViewHolder.showhidden.setVisibility(View.GONE);
        }

        mViewHolder.list_item_menu.setOnClickListener(view -> {
            if (!FolderFragment.mActionMode) {
                setCheckClear();
                item.setcheck(true);
                ((MainActivity) mContext).listMenuClick(file.isDirectory(), false, item.getFavorite());
            }
        });

        return convertView;
    }

    public void setCheck(int position) {
        mDataSource.get(position).setcheck(!mDataSource.get(position).getcheck());
    }

    public ArrayList<FileItem> getCheckList() {
        ArrayList<FileItem> checkList = new ArrayList<FileItem>();
        for (FileItem item : mDataSource) {
            if (item.getcheck()) {
                checkList.add(item);
            }
        }
        return checkList;
    }

    public ArrayList<FileItem> getImageList() {
        ArrayList<FileItem> imageList = new ArrayList<FileItem>();
        for (FileItem item : mDataSource) {
            File file = new File(item.getPath());
            if (MimeTypes.isPicture(file) || MimeTypes.isPdf(file) || MimeTypes.isVideo(file)) {
                //if (MimeTypes.isPicture(file)) {
                imageList.add(item);
            }
        }
        return imageList;
    }

    public int getCheckCount() {
        int check_count = 0;
        for (FileItem item : mDataSource) {
            if (item.getcheck()) {
                check_count++;
            }
        }
        return check_count;
    }

    public ArrayList<FileItem> getList() {
        return mDataSource;
    }

    public void setCheckClear() {
        for (FileItem item : mDataSource) {
            item.setcheck(false);
        }
    }

    public void setAllCheck() {
        for (FileItem item : mDataSource) {
            item.setcheck(true);
        }
    }

    public boolean isAllChecked() {
        for (FileItem item : mDataSource) {
            if (!item.getcheck())
                return false;
        }
        return true;
    }

    public void reSort() {
        // sort files with a comparator if not empty
        if (!mDataSource.isEmpty())
            SortUtils.sortList(mDataSource, mCurrentPath, MyFileSettings.getSortType());

        notifyDataSetChanged();
    }

    public void refresh(String path) {
        mCurrentPath = path;
        if (!mDataSource.isEmpty())
            mDataSource.clear();

        if (refreshType == LIST) {
            getFiles();
        } else if (refreshType == TAG) {
            addTagFiles(refreshTagItem);
        } else if (refreshType == FAVORITE) {
            addFavoriteFiles();
        }

        // sort files with a comparator if not empty
        if (!mDataSource.isEmpty())
            SortUtils.sortList(mDataSource, mCurrentPath, MyFileSettings.getSortType());

        notifyDataSetChanged();
    }

    public void addFiles(String path, int sort_type) {
        refreshType = LIST;
        if (!mDataSource.isEmpty()) {
            if (listChange) {
                listChange = false;
                notifyDataSetChanged();
                return;
            } else {
                mDataSource.clear();
            }
        }
        mCurrentPath = path;
        getFiles();
        listChange = false;
    }

    public void getFiles() {
        try {
            if (mCurrentPath.equals(mContext.getResources().getString(R.string.category_image))) {
                mDataSource = ListFunctionUtils.listAllImageFiles(mContext);
                // sort files with a comparator if not empty
                if (mDataSource.isEmpty() || (MyFileSettings.getSortType() == SortUtils.SORT_DATE && MyFileSettings.getSortReverse())) {
                    //정렬안함!!
                } else {
                    SortUtils.sortList(mDataSource, mCurrentPath, MyFileSettings.getSortType());
                    //Toast.makeText(mContext, "SortUtils" , Toast.LENGTH_SHORT).show();
                }
                //new LongOperation().execute("");
                //return;
            } else if (mCurrentPath.equals(mContext.getResources().getString(R.string.category_video))) {
                mDataSource = ListFunctionUtils.listAllVideoFiles(mContext);
                // sort files with a comparator if not empty
                if (!mDataSource.isEmpty())
                    SortUtils.sortList(mDataSource, mCurrentPath, MyFileSettings.getSortType());
            } else if (mCurrentPath.equals(mContext.getResources().getString(R.string.category_music))) {
                mDataSource = ListFunctionUtils.listAllAudioFiles(mContext);
                // sort files with a comparator if not empty
                if (!mDataSource.isEmpty())
                    SortUtils.sortList(mDataSource, mCurrentPath, MyFileSettings.getSortType());
            } else if (mCurrentPath.equals(mContext.getString(R.string.category_recentfile))) {
                mDataSource = ListFunctionUtils.listDaysFiles(mContext, ListFunctionUtils.LIST_DAY);

                // sort files with a comparator if not empty
                if (!mDataSource.isEmpty())
                    SortUtils.sortList(mDataSource, mCurrentPath, MyFileSettings.getSortType());
            } else {
                mDataSource = ListFunctionUtils.listFiles(mCurrentPath, mContext);
                // sort files with a comparator if not empty
                if (!mDataSource.isEmpty())
                    SortUtils.sortList(mDataSource, mCurrentPath, MyFileSettings.getSortType());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        notifyDataSetChanged();
    }

    public void addTagFiles(TagItem tag) {
        refreshType = TAG;
        refreshTagItem = tag;
        if (!mDataSource.isEmpty())
            mDataSource.clear();

        mDataSource = ListFunctionUtils.listTagFiles(tag.getName(), mContext);

//        // sort files with a comparator if not empty
//        if (!mDataSource.isEmpty())
//            SortUtils.sortList(mDataSource, path);

        notifyDataSetChanged();
    }

    public void addFavoriteFiles() {
        refreshType = FAVORITE;
        if (!mDataSource.isEmpty())
            mDataSource.clear();

        mDataSource = ListFunctionUtils.listFavoriteFiles(mContext);

//        // sort files with a comparator if not empty
//        if (!mDataSource.isEmpty())
//            SortUtils.sortList(mDataSource, path);

        notifyDataSetChanged();
    }

    public void removeFavoriteFiles(FileItem item) {
        if (!mDataSource.isEmpty()) {
            mDataSource.remove(item);
        }
    }

    public void removeFiles(FileItem item) {
        if (!mDataSource.isEmpty()) {
            mDataSource.remove(item);
        }
    }

    public void addContent(ArrayList<FileItem> files) {
        if (!mDataSource.isEmpty())
            mDataSource.clear();

        mDataSource = files;

        notifyDataSetChanged();
    }

    public int getPosition(String path) {
        return mDataSource.indexOf(path);
    }

    public ArrayList<FileItem> getContent() {
        return mDataSource;
    }

    private class ViewHolder {
        ImageView icon;
        TextView filename;
        TextView filedate;
        TextView filesize;
        RelativeLayout list_checker_layout;
        CheckBox list_checker;

        ImageView sdcard_icon;
        ImageView favorite_icon;
        ImageView tag_icon;

        LinearLayout showhidden, titleLayout;
        ImageButton list_item_menu;

        ViewHolder(View view) {
            filename = (TextView) view.findViewById(R.id.filename);
            filedate = (TextView) view.findViewById(R.id.filedate);
            icon = (ImageView) view.findViewById(R.id.icon);

            titleLayout = (LinearLayout) view.findViewById(R.id.layout_title_bg);
            list_checker_layout = (RelativeLayout) view.findViewById(R.id.list_checker_layout);
            list_checker = (CheckBox) view.findViewById(R.id.list_checker);

            sdcard_icon = (ImageView) view.findViewById(R.id.sdcard_icon);
            favorite_icon = (ImageView) view.findViewById(R.id.favorite_icon);
            tag_icon = (ImageView) view.findViewById(R.id.tag_icon);

            list_item_menu = (ImageButton) view.findViewById(R.id.list_item_menu);
            showhidden = (LinearLayout) view.findViewById(R.id.showhidden);

            if (mType == 0) {
                filesize = (TextView) view.findViewById(R.id.filesize);
            }
        }
    }

    private class LongOperation extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            boolean showhidden = MyFileSettings.getShowHiddenFiles();//MyFileSettings.showHiddenFiles();

            final String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID};
            final String orderBy = MediaStore.Images.Media.DATE_MODIFIED;
            //Stores all the images from the gallery in Cursor
            Cursor cursor = mContext.getContentResolver().query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null,
                    null, orderBy);
            //Total number of images
            int count = cursor.getCount();

            //Create an array to store path to all the images
            String[] arrPath = new String[count];

            cursor.moveToLast();
            for (int i = count - 1; i >= 0; i--) {
                cursor.moveToPosition(i);
                int dataColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
                //Store the path of the image
                String path = cursor.getString(dataColumnIndex);
                Log.i("PATH", path);
                final File file = new File(path);
                if (file.exists() && file.canRead()) {
                    if (!showhidden) {
                        if (file.getName().charAt(0) != '.') {
                            FileItem item = MainActivity.mMds.checkFile(path);
                            if (item == null) {
                                item = new FileItem();
                                item.setName(file.getName());
                                item.setPath(path);
                                item.setId("");
                            } else {

                            }
                            //item.setFavoriteId(MainActivity.mMds.checkFavoriteFile(path));
                            item.setcheck(false);
                            mDataSource.add(item);
                        }
                    } else {
                        FileItem item = MainActivity.mMds.checkFile(path);
                        if (item == null) {
                            item = new FileItem();
                            item.setName(file.getName());
                            item.setPath(path);
                            item.setId("");
                        } else {

                        }
                        //item.setFavoriteId(MainActivity.mMds.checkFavoriteFile(path));
                        item.setcheck(false);
                        mDataSource.add(item);
                    }

                }
                if (i % 100 == 10) {
                    publishProgress();
                }
            }
            //return mDirContent;
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            notifyDataSetChanged();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            notifyDataSetChanged();
        }
    }
}
