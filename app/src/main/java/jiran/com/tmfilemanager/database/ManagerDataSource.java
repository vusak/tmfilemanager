package jiran.com.tmfilemanager.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.io.File;
import java.util.ArrayList;

import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.data.SecretFileItem;
import jiran.com.tmfilemanager.data.TagItem;

public class ManagerDataSource {

    public static String FAVORITE_STR = "FFAAVVOORRIITTEE_171003";
    public String stringArray[];
    private MyDatabaseHelper dbHelper;
    private SQLiteDatabase database;


//    public String itemName;
//	public String itemNumber;
//	public String itemGroupName;
//	
//	public String itemStartDate; 
//	public String itemEndDate;
//
//	public String itemWeek; 
//
//	public boolean itemCall; 
//	public boolean itemMessage;
//	public boolean itemPush;
//	
//	public String itemSendMessage; 

//    
//    public final static String FLAG = "flag"; 
//    public final static String LOCATION = "location";

    /**
     * @param context
     */
    public ManagerDataSource(Context context) {
        dbHelper = new MyDatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
    }


    public long insertSecretFileItem(String name, String oriPath, String currentPath, String size, String date) {
        ContentValues values = new ContentValues();
        //values.put(MyDatabaseHelper.SKEEPER_ID, id);
        values.put(MyDatabaseHelper.SECRET_FILE_NAME, name);
        values.put(MyDatabaseHelper.SECRET_FILE_ORI_PATH, oriPath);
        values.put(MyDatabaseHelper.SECRET_FILE_CURRENT_PATH, currentPath);
        values.put(MyDatabaseHelper.SECRET_FILE_SIZE, size);
        values.put(MyDatabaseHelper.SECRET_FILE_DATE, date);

        return database.insert(MyDatabaseHelper.SECRET_TABLE, null, values);
    }

//    public long insertSecretUser(String account, String password) {
//        ContentValues values = new ContentValues();
//        //values.put(MyDatabaseHelper.SKEEPER_ID, id);
//        values.put(MyDatabaseHelper.SECRET_USER_ACCOUNT, account);
//        values.put(MyDatabaseHelper.SECRET_USER_PASSWORD, password);
//
//        return database.insert(MyDatabaseHelper.SECRET_TABLE, null, values);
//    }

    // TAG

    //Inset Tag
    public long insertTagItem(String name, String path) {
        ContentValues values = new ContentValues();
        //values.put(MyDatabaseHelper.SKEEPER_ID, id);
        values.put(MyDatabaseHelper.TAG_NAME, name);
        values.put(MyDatabaseHelper.TAG_PATH, path);
        return database.insert(MyDatabaseHelper.TAG_TABLE, null, values);
    }

    public long insertFavoriteItem(String path) {
        ContentValues values = new ContentValues();
        //values.put(MyDatabaseHelper.SKEEPER_ID, id);
        values.put(MyDatabaseHelper.TAG_NAME, FAVORITE_STR);
        values.put(MyDatabaseHelper.TAG_PATH, path);
        return database.insert(MyDatabaseHelper.TAG_TABLE, null, values);
    }

    public FileItem checkFile(String path) {
        String[] cols = new String[]{MyDatabaseHelper.TAG_ID, MyDatabaseHelper.TAG_NAME, MyDatabaseHelper.TAG_PATH};
        Cursor mCursor = database.query(true, MyDatabaseHelper.TAG_TABLE, cols, MyDatabaseHelper.TAG_PATH + "=?", new String[]{path}, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }

        //ArrayList<FileItem> fileList = new ArrayList<FileItem>();
        FileItem item = null;
        if (mCursor != null) {
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()) {
                if (item == null) item = new FileItem();

                item.setId(mCursor.getString(0));
                if (mCursor.getString(1).equals(FAVORITE_STR)) {
                    item.setFavorite(true);
                } else {
                    item.setTagName(item.getName() + mCursor.getString(1) + ";");
                }
                item.setPath(mCursor.getString(2));
                mCursor.moveToNext();
            }
        }
        mCursor.close();
        return item; // iterate to get each value.
    }

    public FileItem checkFavoriteFile(String path) {
        String[] cols = new String[]{MyDatabaseHelper.TAG_ID, MyDatabaseHelper.TAG_NAME, MyDatabaseHelper.TAG_PATH};
        Cursor mCursor = database.query(true, MyDatabaseHelper.TAG_TABLE, cols, MyDatabaseHelper.TAG_PATH + "=?", new String[]{path}, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }

        //ArrayList<FileItem> fileList = new ArrayList<FileItem>();
        FileItem item = new FileItem();

        if (mCursor != null) {
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()) {
                if (mCursor.getString(1).equals(FAVORITE_STR)) {
                    item.setId(mCursor.getString(0));
                    item.setFavorite(true);
                    item.setPath(mCursor.getString(2));
                    break;
                }
                mCursor.moveToNext();
            }
        }
        mCursor.close();
        return item; // iterate to get each value.
    }

    //Tag List
    public ArrayList<TagItem> getAllTag() {
        String[] cols = new String[]{MyDatabaseHelper.TAG_ID, MyDatabaseHelper.TAG_NAME, MyDatabaseHelper.TAG_PATH, "COUNT(*)"};
        //Cursor mCursor = database.query(true, MyDatabaseHelper.TAG_TABLE, cols, null, null, null, null, null, null);
        Cursor mCursor = database.query(true, MyDatabaseHelper.TAG_TABLE, cols, null, null, MyDatabaseHelper.TAG_NAME, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        ArrayList<TagItem> tagItems = new ArrayList<TagItem>();

        if (mCursor != null) {
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()) {
                if (!mCursor.getString(1).equals(FAVORITE_STR)) {
                    TagItem item = new TagItem();
                    item.setId(mCursor.getString(0));
                    item.setName(mCursor.getString(1));
                    item.setCount(mCursor.getString(3));
                    tagItems.add(item);
                }
                mCursor.moveToNext();
            }
        }
        mCursor.close();
        return tagItems; // iterate to get each value.
    }

    public ArrayList<FileItem> getAllFavorite() {
        String[] cols = new String[]{MyDatabaseHelper.TAG_ID, MyDatabaseHelper.TAG_NAME, MyDatabaseHelper.TAG_PATH};
        Cursor mCursor = database.query(true, MyDatabaseHelper.TAG_TABLE, cols, MyDatabaseHelper.TAG_NAME + "=?", new String[]{FAVORITE_STR}, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        ArrayList<FileItem> fileItems = new ArrayList<FileItem>();

        if (mCursor != null) {
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()) {
                FileItem item = new FileItem();
                item.setId(mCursor.getString(0));
                item.setPath(mCursor.getString(2));
                item.setName(new File(item.getPath()).getName());
                item.setFavorite(true);

                //item.setColor(mCursor.getString(2));
                fileItems.add(item);
                mCursor.moveToNext();
            }
        }
        mCursor.close();
        return fileItems; // iterate to get each value.
    }

    //Tag Name List
    public ArrayList<FileItem> selectTagNameFiles(String name) {
        String[] cols = new String[]{MyDatabaseHelper.TAG_ID, MyDatabaseHelper.TAG_NAME, MyDatabaseHelper.TAG_PATH};
        Cursor mCursor = database.query(true, MyDatabaseHelper.TAG_TABLE, cols, MyDatabaseHelper.TAG_NAME + "=?", new String[]{name}, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }

        ArrayList<FileItem> tagList = new ArrayList<FileItem>();

        if (mCursor != null) {
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()) {
                FileItem item = new FileItem();
                item.setId(mCursor.getString(0));
                item.setTagName(mCursor.getString(1));
                item.setPath(mCursor.getString(2));

                tagList.add(item);
                mCursor.moveToNext();
            }
        }
        mCursor.close();
        return tagList; // iterate to get each value.
    }

    public ArrayList<FileItem> selectTagFiles(String path) {
        String[] cols = new String[]{MyDatabaseHelper.TAG_ID, MyDatabaseHelper.TAG_NAME, MyDatabaseHelper.TAG_PATH};
        Cursor mCursor = database.query(true, MyDatabaseHelper.TAG_TABLE, cols, MyDatabaseHelper.TAG_PATH + "=?", new String[]{path}, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }

        ArrayList<FileItem> fileList = new ArrayList<FileItem>();

        if (mCursor != null) {
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()) {
                if (!mCursor.getString(1).equals(FAVORITE_STR)) {
                    FileItem item = new FileItem();
                    item.setId(mCursor.getString(0));
                    item.setTagName(mCursor.getString(1));
                    item.setPath(mCursor.getString(2));

                    fileList.add(item);
                }
                mCursor.moveToNext();
            }
        }
        mCursor.close();
        return fileList; // iterate to get each value.
    }

    public void deleteTag(String id) {
        System.out.println("Comment deleted with Tag id: " + id);
        database.delete(MyDatabaseHelper.TAG_TABLE, MyDatabaseHelper.TAG_ID + "=?", new String[]{id});
    }

    public void deleteTagName(String name) {
        System.out.println("Comment deleted with Tag name: " + name);
        database.delete(MyDatabaseHelper.TAG_TABLE, MyDatabaseHelper.TAG_NAME + "=?", new String[]{name});
    }

    public void deleteTagPath(String path) {
        System.out.println("Comment deleted with Tag path: " + path);
        database.delete(MyDatabaseHelper.TAG_TABLE, MyDatabaseHelper.TAG_PATH + "=?", new String[]{path});
    }

    public void deleteFile(String path) {
        deleteTagPath(path);
    }

    //FAVORITE

    //Inser Favortie

//    public long insertFavoriteItem(String path) {
//        ContentValues values = new ContentValues();
//        //values.put(MyDatabaseHelper.SKEEPER_ID, id);
//        //values.put(MyDatabaseHelper.FAVORITE_NAME, name);
//        values.put(MyDatabaseHelper.FAVORITE_PATH, path);
//
//        return database.insert(MyDatabaseHelper.FAVORITE_TABLE, null, values);
//    }

//    public long insertFavoriteItem(String path) {
//        ContentValues values = new ContentValues();
//        //values.put(MyDatabaseHelper.SKEEPER_ID, id);
//        values.put(MyDatabaseHelper.TAG_NAME, FAVORITE_STR);
//        values.put(MyDatabaseHelper.TAG_PATH, path);
//        return database.insert(MyDatabaseHelper.TAG_TABLE, null, values);
//    }
//
//    public ArrayList<FileItem> getAllFavorite() {
//        String[] cols = new String[] { MyDatabaseHelper.FAVORITE_ID, MyDatabaseHelper.FAVORITE_PATH};
//        Cursor mCursor = database.query(true, MyDatabaseHelper.FAVORITE_TABLE, cols, null, null, null, null, null, null);
//
//        if (mCursor != null) {
//            mCursor.moveToFirst();
//        }
//
//
//        ArrayList<FileItem> favoriteList = new ArrayList<FileItem>();
//
//        if (mCursor != null) {
//            mCursor.moveToFirst();
//            while (!mCursor.isAfterLast()) {
//                FileItem item = new FileItem();
//                item.setPath(mCursor.getString(1));
//                item.setFavoriteId(mCursor.getString(0));
//
//                favoriteList.add(item);
//                mCursor.moveToNext();
//            }
//        }
//        mCursor.close();
//        return favoriteList; // iterate to get each value.
//    }
//
//    public void deleteFavorite(String id) {
//        System.out.println("Comment deleted with Favorite id: " + id);
//        database.delete(MyDatabaseHelper.FAVORITE_TABLE, MyDatabaseHelper.FAVORITE_ID+"=?", new String[]{id});
//    }
//
//    public void deleteFavoritePath(String path) {
//        System.out.println("Comment deleted with Favorite path: " + path);
//        database.delete(MyDatabaseHelper.FAVORITE_TABLE, MyDatabaseHelper.FAVORITE_PATH+"=?", new String[]{path});
//    }
//
//
//


//    public Cursor getAllSecretFile() {
//        String[] cols = new String[] { MyDatabaseHelper.SECRET_ID, MyDatabaseHelper.SECRET_FILE_NAME, MyDatabaseHelper.SECRET_FILE_ORI_PATH,
//                MyDatabaseHelper.SECRET_FILE_CURRENT_PATH, MyDatabaseHelper.SECRET_FILE_SIZE, MyDatabaseHelper.SECRET_FILE_DATE};
//        Cursor mCursor = database.query(true, MyDatabaseHelper.SECRET_TABLE, cols, null, null, null, null, null, null);
//
//        if (mCursor != null) {
//            mCursor.moveToFirst();
//        }
//        return mCursor; // iterate to get each value.
//    }

    public ArrayList<SecretFileItem> selectSecretFiles(String path) {
        String[] cols = new String[]{MyDatabaseHelper.SECRET_ID, MyDatabaseHelper.SECRET_FILE_NAME, MyDatabaseHelper.SECRET_FILE_ORI_PATH,
                MyDatabaseHelper.SECRET_FILE_CURRENT_PATH, MyDatabaseHelper.SECRET_FILE_SIZE, MyDatabaseHelper.SECRET_FILE_DATE};
        Cursor mCursor = database.query(true, MyDatabaseHelper.SECRET_TABLE, cols, MyDatabaseHelper.SECRET_FILE_CURRENT_PATH + "=?", new String[]{path}, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }

        ArrayList<SecretFileItem> fileList = new ArrayList<SecretFileItem>();

        if (mCursor != null) {
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()) {
                SecretFileItem item = new SecretFileItem();
                item.setId(mCursor.getString(0));
                item.setName(mCursor.getString(1));
                item.setOriName(mCursor.getString(2));
                item.setCurrentPath(mCursor.getString(3));
                item.setSize(mCursor.getString(4));
                item.setDate(mCursor.getString(5));

                fileList.add(item);
                mCursor.moveToNext();
            }
        }
        mCursor.close();
        return fileList; // iterate to get each value.
    }

    public ArrayList<SecretFileItem> getAllSecretFile() {
        String[] cols = new String[]{MyDatabaseHelper.SECRET_ID, MyDatabaseHelper.SECRET_FILE_NAME, MyDatabaseHelper.SECRET_FILE_ORI_PATH,
                MyDatabaseHelper.SECRET_FILE_CURRENT_PATH, MyDatabaseHelper.SECRET_FILE_SIZE, MyDatabaseHelper.SECRET_FILE_DATE};
        Cursor mCursor = database.query(true, MyDatabaseHelper.SECRET_TABLE, cols, null, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }

        ArrayList<SecretFileItem> fileList = new ArrayList<SecretFileItem>();

        if (mCursor != null) {
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()) {
                SecretFileItem item = new SecretFileItem();
                item.setId(mCursor.getString(0));
                item.setName(mCursor.getString(1));
                item.setOriPath(mCursor.getString(2));
                item.setCurrentPath(mCursor.getString(3));
                item.setSize(mCursor.getString(4));
                item.setDate(mCursor.getString(5));

                fileList.add(item);
                mCursor.moveToNext();
            }
        }
        mCursor.close();
        return fileList; // iterate to get each value.
    }

    public String getSecretUserAccount() {
        String[] cols = new String[]{MyDatabaseHelper.SECRET_USER_ID, MyDatabaseHelper.SECRET_USER_ACCOUNT, MyDatabaseHelper.SECRET_USER_PASSWORD};
        Cursor mCursor = database.query(true, MyDatabaseHelper.SECRET_USER_TABLE, cols, null, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }

        String account = "";
        if (mCursor != null) {
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()) {
                account = mCursor.getString(1);
                mCursor.moveToNext();
            }
        }
        mCursor.close();
        return account; // iterate to get each value.
    }

    public String getSecretUserPassword() {
        String[] cols = new String[]{MyDatabaseHelper.SECRET_USER_ID, MyDatabaseHelper.SECRET_USER_ACCOUNT, MyDatabaseHelper.SECRET_USER_PASSWORD};
        Cursor mCursor = database.query(true, MyDatabaseHelper.SECRET_USER_TABLE, cols, null, null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }

        String password = "";
        if (mCursor != null) {
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()) {
                password = mCursor.getString(2);
                mCursor.moveToNext();
            }
        }
        mCursor.close();
        return password; // iterate to get each value.
    }

//    public ArrayList<FileItem> selectLikePathFiles(String path) {
//        String[] cols = new String[] { MyDatabaseHelper.FILE_ID, MyDatabaseHelper.FILE_NAME, MyDatabaseHelper.FILE_PATH,
//                MyDatabaseHelper.FILE_TAG_ID, MyDatabaseHelper.FILE_TAG_COLOR, MyDatabaseHelper.FILE_FAVORITE};
//        Cursor mCursor = database.query(true, MyDatabaseHelper.FILE_TABLE, cols, MyDatabaseHelper.FILE_PATH + " LIKE ?", new String[]{path + "%"}, null, null, null, null);
//        if (mCursor != null) {
//            mCursor.moveToFirst();
//        }
//
//        ArrayList<FileItem> fileList = new ArrayList<FileItem>();
//
//        if (mCursor != null) {
//            mCursor.moveToFirst();
//            while (!mCursor.isAfterLast()) {
//                FileItem item = new FileItem();
//                item.setId(mCursor.getString(0));
//                item.setName(mCursor.getString(1));
//                item.setPath(mCursor.getString(2));
//                item.setTagId(mCursor.getString(3));
//                item.setTagColor(mCursor.getString(4));
//                item.setFavorite(mCursor.getString(5));
//
//                fileList.add(item);
//                mCursor.moveToNext();
//            }
//        }
//        mCursor.close();
//        return fileList; // iterate to get each value.
//    }


    public void deleteSecretFile(String id) {
        System.out.println("Comment deleted with secret id: " + id);
        database.delete(MyDatabaseHelper.SECRET_TABLE, MyDatabaseHelper.SECRET_ID + "=?", new String[]{id});
    }

    public int updateTagItem(String id, String name, String path) {
        ContentValues values = new ContentValues();
        values.put(MyDatabaseHelper.TAG_NAME, name);
        values.put(MyDatabaseHelper.TAG_PATH, path);
        return database.update(MyDatabaseHelper.TAG_TABLE, values, MyDatabaseHelper.TAG_ID + "=?", new String[]{id});
    }

    public int updateTagItem(FileItem item, String path) {
        ContentValues values = new ContentValues();
        if (item.getFavorite()) {
            values.put(MyDatabaseHelper.TAG_NAME, FAVORITE_STR);
        } else {
            values.put(MyDatabaseHelper.TAG_NAME, item.getTagName());
        }
        values.put(MyDatabaseHelper.TAG_PATH, path);
        return database.update(MyDatabaseHelper.TAG_TABLE, values, MyDatabaseHelper.TAG_ID + "=?", new String[]{item.getId()});
    }

//
//    public int updateFileTag(String id, String tagId, String tagColor){
//        ContentValues values=new ContentValues();
//        values.put(MyDatabaseHelper.FILE_TAG_ID, tagId);
//        values.put(MyDatabaseHelper.FILE_TAG_COLOR, tagColor);
//        return database.update(MyDatabaseHelper.FILE_TABLE, values, MyDatabaseHelper.FILE_ID+"=?", new String[]{id});
//    }
//
//    public int updateFilePath(String id, String path){
//        ContentValues values=new ContentValues();
//        values.put(MyDatabaseHelper.FILE_PATH, path);
//        return database.update(MyDatabaseHelper.FILE_TABLE, values, MyDatabaseHelper.FILE_ID+"=?", new String[]{id});
//    }

    public long updateSecretUser(String account, String password) {
        ContentValues values = new ContentValues();
        values.put(MyDatabaseHelper.SECRET_USER_ACCOUNT, account);
        values.put(MyDatabaseHelper.SECRET_USER_PASSWORD, password);
        return database.update(MyDatabaseHelper.SECRET_USER_TABLE, values, null, null);
    }


    public ArrayList<FileItem> selectLikeTagPathFiles(String path) {
        String[] cols = new String[]{MyDatabaseHelper.TAG_ID, MyDatabaseHelper.TAG_NAME, MyDatabaseHelper.TAG_PATH};
        Cursor mCursor = database.query(true, MyDatabaseHelper.TAG_TABLE, cols, MyDatabaseHelper.TAG_PATH + " LIKE ?", new String[]{path + "%"}, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }

        ArrayList<FileItem> fileList = new ArrayList<FileItem>();

        if (mCursor != null) {
            mCursor.moveToFirst();
            while (!mCursor.isAfterLast()) {
                FileItem item = new FileItem();
                item.setId(mCursor.getString(0));
                if (mCursor.getString(1).equals(FAVORITE_STR)) {
                    item.setFavorite(true);
                } else {
                    item.setTagName(mCursor.getString(1));
                }
                item.setPath(mCursor.getString(2));

                String sub = item.getPath().toLowerCase().replace(path.toLowerCase(), "");
                if (sub.startsWith("/")) {
                    fileList.add(item);
                }
                mCursor.moveToNext();
            }
        }
        mCursor.close();
        return fileList; // iterate to get each value.
    }

//    public Cursor selectAllSilent(String week) {
//        Cursor mCursor = database.rawQuery("SELECT " + MyDatabaseHelper.SKEEPER_ID + "," +
//                MyDatabaseHelper.SKEEPER_NAME + "," +
//                MyDatabaseHelper.SKEEPER_GROUPNAME + "," +
//                MyDatabaseHelper.SKEEPER_NUMBER + "," +
//                MyDatabaseHelper.SKEEPER_START_DATE + "," +
//                MyDatabaseHelper.SKEEPER_END_DATE + "," +
//                MyDatabaseHelper.SKEEPER_SAVE_WEEK + "," +
//                MyDatabaseHelper.SKEEPER_WEEK + "," +
//                MyDatabaseHelper.SKEEPER_CALL + "," +
//                MyDatabaseHelper.SKEEPER_MESSAGE + "," +
//                MyDatabaseHelper.SKEEPER_PUSH + "," +
//                MyDatabaseHelper.SKEEPER_SEND_MESSAGE + "," +
//                MyDatabaseHelper.SKEEPER_SKEEP +
//                " FROM " + MyDatabaseHelper.SKEEPER_TABLE +
//                " WHERE (" + MyDatabaseHelper.SKEEPER_SKEEP + " LIKE '4') " +
//                " AND (" + MyDatabaseHelper.SKEEPER_WEEK + " LIKE '%" + week +"%')", null);
////        if (mCursor != null) {
////            mCursor.moveToFirst();
////        }
//        return mCursor; // iterate to get each value.
//    }


//
//    public int UpdateSkeeperItem(String id, String name,String groupName, String number, String startDate, String endDate, String saveWeek, String week, String call, String message, String push, String sendMessage, String skeep){
//       ContentValues values=new ContentValues();
//       values.put(MyDatabaseHelper.SKEEPER_NAME, name);
//       values.put(MyDatabaseHelper.SKEEPER_GROUPNAME, groupName);
//       values.put(MyDatabaseHelper.SKEEPER_NUMBER, number);
//       values.put(MyDatabaseHelper.SKEEPER_START_DATE, startDate);
//       values.put(MyDatabaseHelper.SKEEPER_END_DATE, endDate);
//       values.put(MyDatabaseHelper.SKEEPER_SAVE_WEEK, saveWeek);
//       values.put(MyDatabaseHelper.SKEEPER_WEEK, week);
//       values.put(MyDatabaseHelper.SKEEPER_CALL, call);
//       values.put(MyDatabaseHelper.SKEEPER_MESSAGE, message);
//       values.put(MyDatabaseHelper.SKEEPER_PUSH, push);
//       values.put(MyDatabaseHelper.SKEEPER_SEND_MESSAGE, sendMessage);
//       values.put(MyDatabaseHelper.SKEEPER_SKEEP, skeep);
//       return database.update(MyDatabaseHelper.SKEEPER_TABLE, values, MyDatabaseHelper.SKEEPER_ID+"=?", new String[]{id});
//      }
//
//    public int UpdateSkeeperSkeep(String id, String skeep){
//        ContentValues values=new ContentValues();
//        values.put(MyDatabaseHelper.SKEEPER_SKEEP, skeep);
//        return database.update(MyDatabaseHelper.SKEEPER_TABLE, values, MyDatabaseHelper.SKEEPER_ID+"=?", new String[]{id});
//    }
//
//    public int UpdateSkeeperWeek(String id, String week){
//        ContentValues values=new ContentValues();
//        values.put(MyDatabaseHelper.SKEEPER_WEEK, week);
//        return database.update(MyDatabaseHelper.SKEEPER_TABLE, values, MyDatabaseHelper.SKEEPER_ID+"=?", new String[]{id});
//    }
//
//    public void deleteSkeeper(String id) {
//        System.out.println("Comment deleted with id: " + id);
//        database.delete(MyDatabaseHelper.SKEEPER_TABLE, MyDatabaseHelper.SKEEPER_ID+"=?", new String[]{id});
//    }

}