package jiran.com.tmfilemanager.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MyDatabaseHelper extends SQLiteOpenHelper {

    public final static String TAG_TABLE = "TagList"; // name of table
    public final static String FAVORITE_TABLE = "FavoriteList"; // name of table
    public final static String SECRET_TABLE = "SecretItem"; // name of table
    public final static String SECRET_USER_TABLE = "SecretUser"; // name of table
    public final static String TAG_ID = "tag_id";
    public final static String TAG_NAME = "tag_name";
    public final static String TAG_PATH = "tag_path";
    public final static String FAVORITE_ID = "Favorite_id";
    //public final static String FAVORITE_NAME = "Favorite_name";
    public final static String FAVORITE_PATH = "Favorite_path";
    public final static String SECRET_ID = "secret_id";
    public final static String SECRET_FILE_NAME = "secret_file_name";
    public final static String SECRET_FILE_ORI_PATH = "secret_file_ori_path";
    public final static String SECRET_FILE_CURRENT_PATH = "secret_file_current_path";
    public final static String SECRET_FILE_SIZE = "secret_file_size";
    public final static String SECRET_FILE_DATE = "secret_file_date";
    public final static String SECRET_USER_ID = "secret_user_id";
    public final static String SECRET_USER_ACCOUNT = "secret_user_account";
    public final static String SECRET_USER_PASSWORD = "secret_user_password";
    private static final String DATABASE_NAME = "FileManager.db";
    private static final int DATABASE_VERSION = 2;
    // Database creation sql statement
    private static final String DATABASE_CREATE_TAG_ITEM =
            "create table " + TAG_TABLE + "(" + TAG_ID + " integer primary key autoincrement," +
                    TAG_NAME + " text not null," +
                    TAG_PATH + " text not null);";

//    private static final String DATABASE_INIT1_TAG_ITEM =
//            "insert into " + TAG_TABLE + "(" + TAG_NAME + "," + TAG_COLOR + ") values ('" + MyApplication.getInstance().getResources().getString(R.string.tag_temp_1) + "','1');";
//    private static final String DATABASE_INIT2_TAG_ITEM =
//            "insert into " + TAG_TABLE + "(" + TAG_NAME + "," + TAG_COLOR + ") values ('" + MyApplication.getInstance().getResources().getString(R.string.tag_temp_2) + "','2');";
//    private static final String DATABASE_INIT3_TAG_ITEM =
//            "insert into " + TAG_TABLE + "(" + TAG_NAME + "," + TAG_COLOR + ") values ('" + MyApplication.getInstance().getResources().getString(R.string.tag_temp_3) + "','3');";

//    private static final String DATABASE_CREATE_FAVORITE_ITEM =
//            "create table " + FAVORITE_TABLE + "(" + FAVORITE_ID + " integer primary key autoincrement," +
//                    //FAVORITE_NAME + " text not null," +
//                    FAVORITE_PATH + " text not null);";

    private static final String DATABASE_CREATE_SECRET_ITEM =
            "create table " + SECRET_TABLE + "(" + SECRET_ID + " integer primary key autoincrement," +
                    SECRET_FILE_NAME + " text not null," +
                    SECRET_FILE_ORI_PATH + " text not null," +
                    SECRET_FILE_CURRENT_PATH + " text not null," +
                    SECRET_FILE_SIZE + " text not null," +
                    SECRET_FILE_DATE + " text not null);";

    private static final String DATABASE_CREATE_SECRET_USER =
            "create table " + SECRET_USER_TABLE + "(" + SECRET_USER_ID + " integer primary key autoincrement," +
                    SECRET_USER_ACCOUNT + " text not null," +
                    SECRET_USER_PASSWORD + " text not null);";

    private static final String DATABASE_INSERT_SECRET_USER =
            "insert into " + SECRET_USER_TABLE + "(" + SECRET_USER_ACCOUNT + "," + SECRET_USER_PASSWORD + ") values ('','');";


    public MyDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Method is called during creation of the database
    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE_TAG_ITEM);
        //database.execSQL(DATABASE_CREATE_FAVORITE_ITEM);
        database.execSQL(DATABASE_CREATE_SECRET_ITEM);
        database.execSQL(DATABASE_CREATE_SECRET_USER);
        database.execSQL(DATABASE_INSERT_SECRET_USER);
        // MyApplication.getInstance().getResources().getString(R.string.secretfolder_add_account);

//        database.execSQL(DATABASE_INIT1_TAG_ITEM);
//        database.execSQL(DATABASE_INIT2_TAG_ITEM);
//        database.execSQL(DATABASE_INIT3_TAG_ITEM);
    }

    // Method is called during an upgrade of the database,
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(MyDatabaseHelper.class.getName(), "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
//        database.execSQL("DROP TABLE IF EXISTS " + TAG_TABLE);
//        database.execSQL("DROP TABLE IF EXISTS " + FILE_TABLE);
//        database.execSQL("DROP TABLE IF EXISTS " + SECRET_TABLE);
//        onCreate(database);
        database.execSQL(DATABASE_CREATE_SECRET_ITEM);
        database.execSQL(DATABASE_CREATE_SECRET_USER);
        database.execSQL(DATABASE_INSERT_SECRET_USER);
    }
}