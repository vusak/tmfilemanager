package jiran.com.tmfilemanager.data;

import java.io.Serializable;

/**
 * Created by user on 2016-08-04.
 */
public class UserItem implements Serializable {

    public static final long serialVersionUID = 1L;
    private String team_member_id;
    private boolean leader;
    private String email;
    private String name;
    private String profile;
    private String profile_thumb;
    private String grade;
    private int status;
    private String regdate;

    public UserItem() {
    }

    public String getMemberID() {
        return team_member_id;
    }

    public void setMemberID(String data) {
        this.team_member_id = data;
    }

    public boolean getLeader() {
        return leader;
    }

    public void setLeader(boolean data) {
        this.leader = data;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String data) {
        this.email = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String data) {
        this.name = data;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String data) {
        this.profile = data;
    }

    public String getProfileThumb() {
        return profile_thumb;
    }

    public void setProfileThumb(String data) {
        this.profile_thumb = data;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String data) {
        this.grade = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int data) {
        this.status = data;
    }

    public String getRegdate() {
        return regdate;
    }

    public void setRegdate(String data) {
        this.regdate = data;
    }
}
