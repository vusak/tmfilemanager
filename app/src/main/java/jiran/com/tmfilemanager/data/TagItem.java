package jiran.com.tmfilemanager.data;

import java.io.Serializable;

/**
 * Created by user on 2016-08-04.
 */
public class TagItem implements Serializable {

    public static final long serialVersionUID = 1L;
    private String id;
    private String name;
    private String count;

    public TagItem() {
    }

    public String getId() {
        return id;
    }

    public void setId(String data) {
        this.id = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String data) {
        this.name = data;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String data) {
        this.count = data;
    }
}
