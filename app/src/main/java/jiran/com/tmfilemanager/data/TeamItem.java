package jiran.com.tmfilemanager.data;

import java.io.Serializable;
import java.util.ArrayList;

import jiran.com.tmfilemanager.common.SortUtils;

/**
 * Created by user on 2016-08-04.
 */
public class TeamItem implements Serializable {

    public static final long serialVersionUID = 1L;
    private String team_id;
    private String user_id;
    private String drive_id;
    private String name;
    private String my_role;
    private String team_role;
    private boolean leader;
    //private int percent;
    private long volume = 100;
    private long volume_usage = 0;
    private String join_rule;
    private String join_protect_domain;
    private String flag_push;
    private int trash_option;
    private int status;
    private String regdate;
    private String modified;
    private int member_count;
    private ArrayList<UserItem> user;
    private ArrayList<UserItem> wait;

    public TeamItem() {
    }

    public String getId() {
        return team_id;
    }

    public void setId(String data) {
        this.team_id = data;
    }

    public String getUserId() {
        return user_id;
    }

    public void setUserId(String data) {
        this.user_id = data;
    }

    public String getDriveId() {
        return drive_id;
    }

    public void setDriveId(String data) {
        this.drive_id = data;
    }

    public String getTeamName() {
        return name;
    }

    public void setTeamName(String data) {
        this.name = data;
    }

    public String getMyRole() {
        return my_role;
    }

    public void setMyRole(String data) {
        this.my_role = data;
    }

    public String getTeamRole() {
        return team_role;
    }

    public void setTeamRole(String data) {
        this.team_role = data;
    }

    public boolean getTeamLeader() {
        return leader;
    }

    public void setTeamLeader(boolean data) {
        this.leader = data;
    }

    //    public void setPercent(int data) { this.percent = data; }
    public int getPercent() {
        long percent = (volume_usage * 100) / volume;
        return (int) percent;
    }

    public long getVolume() {
        return volume;
    }

    public void setVolume(long data) {
        this.volume = data;
    }

    public long getVolumeUsage() {
        return volume_usage;
    }

    public void setVolumeUsage(long data) {
        this.volume_usage = data;
    }

    public String getJoinRule() {
        return join_rule;
    }

    public void setJoinRule(String data) {
        this.join_rule = data;
    }

    public String getJoinProtectDomain() {
        return join_protect_domain;
    }

    public void setJoinProtectDomain(String data) {
        this.join_protect_domain = data;
    }

    public String getFlagPush() {
        return flag_push;
    }

    public void setFlagPush(String data) {
        this.flag_push = data;
    }

    public int getTrashOption() {
        return trash_option;
    }

    public void setTrashOption(int data) {
        this.trash_option = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int data) {
        this.status = data;
    }

    public String getRegdate() {
        return regdate;
    }

    public void setRegdate(String data) {
        this.regdate = data;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String data) {
        this.modified = data;
    }

    public int getMemberCount() {
        return member_count;
    }

    public void setMemberCount(int data) {
        this.member_count = data;
    }

    public ArrayList<UserItem> getUserList() {
        return user;
    }

    public void setUserList(ArrayList<UserItem> data) {
        SortUtils.sortUser(data);
        this.user = data;
    }

    public ArrayList<UserItem> getWaitList() {
        return wait;
    }

    public void setWaitList(ArrayList<UserItem> data) {
        this.wait = data;
    }
}
