package jiran.com.tmfilemanager.data;

import java.io.Serializable;

/**
 * Created by user on 2016-08-04.
 */
public class SecretFileItem implements Serializable {

    public static final long serialVersionUID = 1L;
    private String id;
    private String name;
    private String oriName;
    private String oriPath;
    private String currentPath;
    private String size;
    private String date;
    private boolean check;


    public SecretFileItem() {
    }

    public String getId() {
        return id;
    }

    public void setId(String data) {
        this.id = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String data) {
        this.name = data;
    }

    public String getOriName() {
        return oriName;
    }

    public void setOriName(String data) {
        this.oriName = data;
    }

    public String getOriPath() {
        return oriPath;
    }

    public void setOriPath(String data) {
        this.oriPath = data;
    }

    public String getCurrentPath() {
        return currentPath;
    }

    public void setCurrentPath(String data) {
        this.currentPath = data;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String data) {
        this.size = data;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String data) {
        this.date = data;
    }

    public void setcheck(boolean data) {
        this.check = data;
    }

    public boolean getcheck() {
        return check;
    }
}
