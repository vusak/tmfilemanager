package jiran.com.tmfilemanager.data;

import java.io.Serializable;

/**
 * Created by user on 2016-08-04.
 */
public class CurrentItem implements Serializable {

    public static final long serialVersionUID = 1L;
    private String object_id;
    private String node;
    private String name;
    private String size;
    private String regdate;
    private String modified;
    private String category_id = "";

    public CurrentItem() {
    }

    public String getObjectId() {
        return object_id;
    }

    public void setObjectId(String data) {
        this.object_id = data;
    }

    public String getNode() {
        return node;
    }

    public void setNode(String data) {
        this.node = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String data) {
        this.name = data;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String data) {
        this.size = data;
    }

    public String getRegdate() {
        return regdate;
    }

    public void setRegdate(String data) {
        this.regdate = data;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String data) {
        this.modified = data;
    }

    public String getCategoryId() {
        return category_id;
    }

    public void setCategoryId(String data) {
        this.category_id = data;
    }
}
