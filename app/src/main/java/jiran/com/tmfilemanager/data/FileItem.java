package jiran.com.tmfilemanager.data;

import java.io.Serializable;

/**
 * Created by user on 2016-08-04.
 */
public class FileItem implements Serializable {

    public static final long serialVersionUID = 1L;
    private String team_id = "";
    private String id = "";
    private String name = "";
    private String path;
    private String tag_name;
    private String size;
    private String reg_date;
    private boolean favorite = false;
    private String sModifed_date;
    private String sExtension;
    private String sMemo;
    private String sCreator;
    private String sCreator_name;
    private String sObject_type;
    private String sNode;
    private String sThumbnail;
    private String sUrl_path;
    private boolean check;
    private boolean secret = false;
    private String ori_path;
    private int iconResource = -1;

    public FileItem() {
    }

    public String getTeamId() {
        return team_id;
    }

    public void setTeamId(String data) {
        this.team_id = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String data) {
        this.id = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String data) {
        this.name = data;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String data) {
        this.path = data;
    }

    public String getTagName() {
        return tag_name;
    }

    public void setTagName(String data) {
        this.tag_name = data;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String data) {
        this.size = data;
    }

    public String getRegDate() {
        return reg_date;
    }

    public void setRegDate(String data) {
        this.reg_date = data;
    }

    public boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(boolean data) {
        this.favorite = data;
    }

    public void setcheck(boolean data) {
        this.check = data;
    }

    public boolean getcheck() {
        return check;
    }

    public boolean getSecret() {
        return secret;
    }

    public void setSecret(boolean data) {
        this.secret = data;
    }

    public String getOriPath() {
        return ori_path;
    }

    public void setOriPath(String data) {
        this.ori_path = data;
    }

    public int getIconResoruce() {
        return iconResource;
    }

    public void setIconResoruce(int data) {
        this.iconResource = data;
    }

    public String getSModifedDate() {
        return sModifed_date;
    }

    public void setSModifedDate(String data) {
        this.sModifed_date = data;
    }

    public String getSExtension() {
        return sExtension;
    }

    public void setSExtension(String data) {
        this.sExtension = data;
    }

    public String getSMemo() {
        return sMemo;
    }

    public void setSMemo(String data) {
        this.sMemo = data;
    }

    public String getSCreator() {
        return sCreator;
    }

    public void setSCreator(String data) {
        this.sCreator = data;
    }

    public String getSCreatorName() {
        return sCreator_name;
    }

    public void setSCreatorName(String data) {
        this.sCreator_name = data;
    }

    public String getSObjectType() {
        return sObject_type;
    }

    public void setSObjectType(String data) {
        this.sObject_type = data;
    }

    public boolean isDirectory() {
        return sObject_type.equals("D") ? true : false;
    }

    public String getSNode() {
        return sNode;
    }

    public void setSNode(String data) {
        this.sNode = data;
    }

    public String getSThumbnail() {
        return sThumbnail;
    }

    public void setSThumbnail(String data) {
        this.sThumbnail = data;
    }

    public String getSUrlPath() {
        return sUrl_path;
    }

    public void setSUrlPath(String data) {
        this.sUrl_path = data;
    }
}
