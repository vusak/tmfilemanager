package jiran.com.tmfilemanager.data;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by dhawal sodha parmar on 5/4/2015.
 */
public class ImageModel implements Parcelable {
    public static final Creator<ImageModel> CREATOR = new Creator<ImageModel>() {
        public ImageModel createFromParcel(Parcel source) {
            return new ImageModel(source);
        }

        public ImageModel[] newArray(int size) {
            return new ImageModel[size];
        }
    };
    String project_id;
    String comment;
    String email;
    String ext;
    String file_id;
    String modified;
    String name;
    String nick_name;
    long size;
    String thumb;
    int flag;
    String url;
    FileItem item;
    Context context;

    public ImageModel(String project_id, String comment, String email, String ext, String file_id, String modified, String name, String nick_name, long size, String thumb, int flag, String url) {
        this.project_id = project_id;
        this.comment = comment;
        this.email = email;
        this.ext = ext;
        this.file_id = file_id;
        this.modified = modified;
        this.name = name;
        this.nick_name = nick_name;
        this.size = size;
        this.thumb = thumb;
        this.flag = flag;
        this.url = url;
    }

    public ImageModel() {
    }

    protected ImageModel(Parcel in) {
        this.project_id = in.readString();
        this.comment = in.readString();
        this.email = in.readString();
        this.ext = in.readString();
        this.file_id = in.readString();
        this.modified = in.readString();
        this.name = in.readString();
        this.nick_name = in.readString();
        this.size = in.readLong();
        this.thumb = in.readString();
        this.flag = in.readInt();
        this.url = in.readString();
    }

    public FileItem getFileItem() {
        return item;
    }

    public void setFileItem(FileItem data) {
        this.item = data;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context data) {
        this.context = data;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getFile_id() {
        return file_id;
    }

    public void setFile_id(String file_id) {
        this.file_id = file_id;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.project_id);
        dest.writeString(this.comment);
        dest.writeString(this.email);
        dest.writeString(this.ext);
        dest.writeString(this.file_id);
        dest.writeString(this.modified);
        dest.writeString(this.name);
        dest.writeString(this.nick_name);
        dest.writeLong(this.size);
        dest.writeString(this.thumb);
        dest.writeInt(this.flag);
        dest.writeString(this.url);
    }
}
