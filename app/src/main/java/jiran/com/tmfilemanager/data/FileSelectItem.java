package jiran.com.tmfilemanager.data;

import java.io.Serializable;

/**
 * Created by user on 2016-08-04.
 */
public class FileSelectItem implements Serializable {

    public static final long serialVersionUID = 1L;
    private TeamItem team;
    private String team_id;
    private String object_id;
    private String path;
    private String name;
    private String size;
    private String regdate;
    private String modified;
    private String category_id = "";

    public FileSelectItem() {
    }

    public TeamItem getTeamItem() {
        return team;
    }

    public void setTeamItem(TeamItem data) {
        this.team = data;
    }

    public String getTeamId() {
        return team_id;
    }

    public void setTeamId(String data) {
        this.team_id = data;
    }

    public String getObjectId() {
        return object_id;
    }

    public void setObjectId(String data) {
        this.object_id = data;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String data) {
        this.path = data;
    }

//    public void setName(String data) { this.name = data; }
//    public String getName() { return name; }
//
//    public void setSize(String data) { this.size = data; }
//    public String getSize() { return size; }
//
//    public void setRegdate(String data) { this.regdate = data; }
//    public String getRegdate() { return regdate; }
//
//    public void setModified(String data) { this.modified = data; }
//    public String getModified() { return modified; }
//
//    public void setCategoryId(String data) { this.category_id = data; }
//    public String getCategoryId() { return category_id; }
}
