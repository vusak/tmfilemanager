package jiran.com.tmfilemanager.data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by user on 2016-08-04.
 */
public class StorageListItem implements Serializable {

    public static final long serialVersionUID = 1L;
    private CurrentItem mCurrent;
    private ArrayList<FileItem> mList;

    public StorageListItem(CurrentItem current, ArrayList<FileItem> list) {
        mCurrent = current;
        mList = list;
    }

    public StorageListItem() {
    }

    public CurrentItem getCurrentItem() {
        return mCurrent;
    }

    public void setCurrentItem(CurrentItem data) {
        this.mCurrent = data;
    }

    public ArrayList<FileItem> getList() {
        return mList;
    }

    public void setList(ArrayList<FileItem> data) {
        this.mList = data;
    }
}
