package jiran.com.tmfilemanager.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.fagment.MultiFragment;
import jiran.com.tmfilemanager.service.FileObserverService;
import jiran.com.tmfilemanager.settings.MyFileSettings;

/**
 * Created by user on 2016-08-09.
 */
public class InitActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_init);
        MyFileSettings.updatePreferences(this);

        //MobileAds.initialize(getApplicationContext(), "ca-app-pub-8870534780213740~4609619319");

        MainActivity.path = getIntent().getStringExtra("path");
        MultiFragment.teamItams = null;

        if (MyFileSettings.getNewFilesAlram()) {
            startService(new Intent(this, FileObserverService.class));
        }

        Handler han = new Handler();
        han.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(InitActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        }, 500);
        return;
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
