package jiran.com.tmfilemanager.activity.team;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.JsonObject;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.LoginActivity;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.common.AlertCallback;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.TeamItem;
import jiran.com.tmfilemanager.data.UserItem;
import jiran.com.tmfilemanager.fagment.MultiFragment;
import jiran.com.tmfilemanager.network.JMF_network;
import jiran.com.tmfilemanager.utils.PreferenceHelper;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Namo on 2018-02-28.
 */

public class MemberManageActivity extends AppCompatActivity {
    UserItem mUser;
    TeamItem mTeam;
    boolean leader = false;

    LinearLayout member_remove_btn, resend_join_email_btn, team_leader_layout;// log_btn;
    TextView user_email_tv, user_name_tv, user_level_tv;
    TextView member_name_tv, member_regdate_tv, member_remove_tv;
    ImageView rename_arrow;

    Dialog mDialog;
    Dialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_manage_info);

        if (getIntent().getSerializableExtra("UserItem") != null) {
            mUser = (UserItem) getIntent().getSerializableExtra("UserItem");
        }
        if (getIntent().getSerializableExtra("TeamItem") != null) {
            mTeam = (TeamItem) getIntent().getSerializableExtra("TeamItem");
        }

        leader = getIntent().getBooleanExtra("Leader", false);

        LinearLayout title_left_btn = (LinearLayout) findViewById(R.id.title_left_btn);
        title_left_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        final ImageView profile_iv = (ImageView) findViewById(R.id.profile_iv);
        if (mUser == null || LoginActivity.mUserEmail.equals(mUser.getEmail())) {
            profile_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }

        if (mUser != null) {
            if (mUser.getProfileThumb() != null && mUser.getProfileThumb().length() > 0) {
                Glide.with(MemberManageActivity.this).load(mUser.getProfileThumb()).asBitmap().centerCrop().placeholder(R.mipmap.user_none).into(new BitmapImageViewTarget(profile_iv) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        profile_iv.setImageDrawable(circularBitmapDrawable);
                    }
                });
            }
        }


        user_email_tv = (TextView) findViewById(R.id.user_email_tv);
        if (mUser != null) {
            user_email_tv.setText(mUser.getEmail());
        } else {
            user_email_tv.setText(LoginActivity.mUserEmail);
        }

        member_name_tv = (TextView) findViewById(R.id.member_name_tv);
        if (mUser != null && mUser.getStatus() == 0) {
            member_name_tv.setText(getString(R.string.team_info_not_sign_up));
            member_name_tv.setTextColor(getResources().getColorStateList(R.color.main_font_color_red));
        } else {
            member_name_tv.setText(mUser.getName());
        }
        member_regdate_tv = (TextView) findViewById(R.id.member_regdate_tv);
        //member_regdate_tv.setText(Utils.formatDate(mUser.getRegdate(),"yyyy-MM-dd", false));
        member_regdate_tv.setText(mUser.getRegdate());
        member_remove_tv = (TextView) findViewById(R.id.member_remove_tv);

        resend_join_email_btn = (LinearLayout) findViewById(R.id.resend_join_email_btn);
        team_leader_layout = (LinearLayout) findViewById(R.id.team_leader_layout);
        member_remove_btn = (LinearLayout) findViewById(R.id.member_remove_btn);
        //log_btn = (LinearLayout) findViewById(R.id.log_btn);

        if (mUser != null) {
            if (mTeam.getTeamLeader()) {
                if (LoginActivity.mUserEmail.equals(mUser.getEmail())) {
//                    log_btn.setVisibility(View.VISIBLE);
//                    log_btn.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            Toast.makeText(MemberManageActivity.this, getString(R.string.member_manage_log), Toast.LENGTH_SHORT).show();
//                        }
//                    });
                    resend_join_email_btn.setVisibility(View.GONE);
                    member_remove_tv.setText(getString(R.string.member_manage_team_remove));
                    member_remove_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            Intent intent = new Intent(MemberManageActivity.this, MemberLeaveActivity.class);
//                            startActivityForResult(intent, MainActivity.REQUEST_CODE_LEAVE_MEMBER_ACTIVITY);
                            removeTeamDialog();
                        }
                    });
                } else {
//                    log_btn.setVisibility(View.GONE);
                    if (mUser.getStatus() == 0) {
                        resend_join_email_btn.setVisibility(View.VISIBLE);
                        resend_join_email_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                requestResendEmail(mTeam.getId(), mUser.getMemberID());
                                //Toast.makeText(MemberManageActivity.this, getString(R.string.member_manage_resend_join_email), Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        resend_join_email_btn.setVisibility(View.GONE);
                    }
                    member_remove_tv.setText(getString(R.string.member_manage_member_remove));
                    member_remove_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            removeMemberDialog();
                        }
                    });
                }
            } else {
//                log_btn.setVisibility(View.VISIBLE);
//                log_btn.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Toast.makeText(MemberManageActivity.this, getString(R.string.member_manage_log), Toast.LENGTH_SHORT).show();
//                    }
//                });
                resend_join_email_btn.setVisibility(View.GONE);
                if (LoginActivity.mUserEmail.equals(mUser.getEmail())) {
                    member_remove_tv.setText(getString(R.string.member_manage_member_leave));
                    member_remove_btn.setVisibility(View.VISIBLE);
                    member_remove_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            leaveMemberDialog();
                        }
                    });
                } else {
                    member_remove_btn.setVisibility(View.GONE);
                }

                if (mUser.getLeader()) {
                    team_leader_layout.setVisibility(View.VISIBLE);
                } else {
                    team_leader_layout.setVisibility(View.GONE);
                }
            }
        } else {
            getUserInfo();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case MainActivity.REQUEST_CODE_LEAVE_MEMBER_ACTIVITY:
                    setResult(RESULT_OK);
                    finish();
                    break;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void getUserInfo() {
        showProgress();
        try {
            JMF_network.getUserInfo()
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF_getUserInfo", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    user_name_tv.setText(result.get("name").getAsString());
                                    user_level_tv.setText(result.get("grade").getAsString());
                                } else {
                                    Toast.makeText(MemberManageActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgress();
            Toast.makeText(MemberManageActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void setUserInfo(String name) {
        showProgress();
        try {
            JMF_network.modifyUserInfo(name)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF_modifyUserInfo", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    user_name_tv.setText(name);
                                    LoginActivity.mUserName = name;
                                    PreferenceHelper.putString("user_name", name);
                                    setResult(RESULT_OK);
                                } else {
                                    Toast.makeText(MemberManageActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgress();
            Toast.makeText(MemberManageActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void removeTeamDialog() {
        mDialog = new Dialog(this, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.dialog_base, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.member_manage_team_remove));
        TextView content = (TextView) view.findViewById(R.id.content);
        content.setText(getResources().getString(R.string.member_manage_team_remove_message));
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                requestTeamRemove(mTeam.getId());
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void requestTeamRemove(String team_id) {
        showProgress();
        try {
            JMF_network.deleteTeam(team_id)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF deleteTeam", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    Utils.notiAlert(MemberManageActivity.this, getString(R.string.member_manage_team_remove_complete), new AlertCallback() {
                                        @Override
                                        public void ok(Object teamObj, Object obj) {
                                            for (int i = 0; i < MultiFragment.teamItams.size(); i++) {
                                                if (team_id.equals(MultiFragment.teamItams.get(i).getId())) {
                                                    MultiFragment.teamItams.remove(i);
                                                    break;
                                                }
                                            }
                                            setResult(RESULT_OK);
                                            finish();
                                        }

                                        @Override
                                        public void cancle(Object obj) {
                                        }
                                    });
                                } else {
                                    Toast.makeText(MemberManageActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgress();
            Toast.makeText(MemberManageActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void removeMemberDialog() {
        mDialog = new Dialog(this, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.dialog_base, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.member_manage_member_remove));
        TextView content = (TextView) view.findViewById(R.id.content);
        content.setText(getResources().getString(R.string.member_manage_member_remove_message));
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                requestMemeberRemove(mTeam.getId(), mUser.getMemberID());
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void requestMemeberRemove(String team_id, String team_member_id) {
        showProgress();
        try {
            JMF_network.deleteTeamMember(team_id, team_member_id)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF deleteTeamMember", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    Utils.notiAlert(MemberManageActivity.this, getString(R.string.member_manage_member_remove_complete), new AlertCallback() {
                                        @Override
                                        public void ok(Object teamObj, Object obj) {
                                            for (int i = 0; i < MultiFragment.teamItams.size(); i++) {
                                                if (mTeam.getId().equals(MultiFragment.teamItams.get(i).getId())) {
                                                    for (UserItem item : MultiFragment.teamItams.get(i).getUserList()) {
                                                        if (item.getMemberID().equals(team_member_id)) {
                                                            MultiFragment.teamItams.get(i).getUserList().remove(item);
                                                            mTeam = MultiFragment.teamItams.get(i);
                                                            break;
                                                        }
                                                    }
                                                    break;
                                                }
                                            }
                                            setResult(RESULT_OK);
                                            finish();
                                        }

                                        @Override
                                        public void cancle(Object obj) {
                                        }
                                    });
                                } else {
                                    Toast.makeText(MemberManageActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgress();
            Toast.makeText(MemberManageActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void leaveMemberDialog() {
        mDialog = new Dialog(this, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.dialog_base, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.member_manage_member_leave));
        TextView content = (TextView) view.findViewById(R.id.content);
        content.setText(getResources().getString(R.string.member_manage_member_leave_info));
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                requestMemeberLeave(mTeam.getId(), mUser.getMemberID());
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void requestMemeberLeave(String team_id, String team_member_id) {
        showProgress();
        try {
            JMF_network.leaveTeamMember(team_id, team_member_id)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF requestMemeberLeave", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    Utils.notiAlert(MemberManageActivity.this, getString(R.string.member_manage_member_leave_complete), new AlertCallback() {
                                        @Override
                                        public void ok(Object teamObj, Object obj) {
                                            for (int i = 0; i < MultiFragment.teamItams.size(); i++) {
                                                if (mTeam.getId().equals(MultiFragment.teamItams.get(i).getId())) {
                                                    MultiFragment.teamItams.remove(i);
                                                    break;
                                                }
                                            }
                                            setResult(RESULT_OK);
                                            finish();
                                        }

                                        @Override
                                        public void cancle(Object obj) {
                                        }
                                    });
                                } else {
                                    Toast.makeText(MemberManageActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgress();
            Toast.makeText(MemberManageActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void requestResendEmail(String team_id, String email) {
        showProgress();
        try {
            JMF_network.reSendJoinMail(team_id, email)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF reSendJoinMail", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    Utils.notiAlert(MemberManageActivity.this, getString(R.string.team_info_resend_joinmail_complete));
                                    //MultiFragment.teamItams = null;
                                } else {
                                    Toast.makeText(MemberManageActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgress();
            Toast.makeText(MemberManageActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void showProgress() {
        progressDialog = Utils.Progress(MemberManageActivity.this, "", "Login...", false);
        progressDialog.show();
        ;
//        mLoginProgressBar.setVisibility(View.VISIBLE);
//        mLoginProgressBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//            }
//        });
    }

    public void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
//        if(mLoginProgressBar != null){
//            mLoginProgressBar.setVisibility(View.GONE);
//        }
    }
}
