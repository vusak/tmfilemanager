package jiran.com.tmfilemanager.activity.team;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.LoginActivity;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.network.JMF_network;
import jiran.com.tmfilemanager.utils.PreferenceHelper;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Namo on 2018-02-28.
 */

public class MemberLeaveActivity extends AppCompatActivity {

    CheckBox member_leave_check;
    RelativeLayout member_leave_btn;

    Dialog mDialog;
    Dialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_leave);

        LinearLayout title_left_btn = (LinearLayout) findViewById(R.id.title_left_btn);
        title_left_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        member_leave_check = (CheckBox) findViewById(R.id.member_leave_check);
        member_leave_btn = (RelativeLayout) findViewById(R.id.member_leave_btn);
        member_leave_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (member_leave_check.isChecked()) {
                    try {
                        showProgress();
                        JMF_network.leaveMember()
                                .flatMap(gigaPodPair -> {
                                    return Observable.just(gigaPodPair);
                                })
                                .doOnError(throwable -> {
                                    dismissProgress();
                                    PreferenceHelper.putString("access_token", "");
                                })
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(gigaPodPair -> {
                                            dismissProgress();
                                            if (gigaPodPair.first) {
                                                Log.i("JMF_leaveMember", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                                JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                                PreferenceHelper.putString("access_token", "");
                                                LoginActivity.mLoginToken = "";
                                                //LoginActivity.mUserId = "";
                                                LoginActivity.mUserEmail = "";
                                                LoginActivity.mUserName = "";

                                                mDialog = new Dialog(MemberLeaveActivity.this, R.style.Theme_TransparentBackground);
                                                LayoutInflater inflater = LayoutInflater.from(MemberLeaveActivity.this);
                                                View view = inflater.inflate(R.layout.dialog_base_one_button, null, false);
                                                TextView title = (TextView) view.findViewById(R.id.title);
                                                title.setText(getResources().getString(R.string.dialog_notice));
                                                TextView content = (TextView) view.findViewById(R.id.content);
                                                content.setText(getResources().getString(R.string.leave_member_confirm));
                                                TextView ok = (TextView) view.findViewById(R.id.ok);
                                                ok.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        if (mDialog != null) {
                                                            mDialog.dismiss();
                                                        }
                                                        setResult(RESULT_OK);
                                                        finish();
                                                    }
                                                });
                                                mDialog.setContentView(view);
                                                mDialog.show();
                                            } else {
                                                Toast.makeText(MemberLeaveActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                            }
                                        },
                                        throwable -> {
                                            dismissProgress();
                                            PreferenceHelper.putString("access_token", "");
                                            throwable.printStackTrace();
                                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                        dismissProgress();
                        Toast.makeText(MemberLeaveActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(MemberLeaveActivity.this, getString(R.string.leave_member_checker_info), Toast.LENGTH_SHORT).show();
                    ;
                }
            }
        });


//        TextView email_add = (TextView) findViewById(R.id.email_add);
//        email_add.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mDialog = new Dialog(MemberLeaveActivity.this, R.style.Theme_TransparentBackground);
//                LayoutInflater inflater = LayoutInflater.from(MemberLeaveActivity.this);
//                View view = inflater.inflate(R.layout.dialog_reqeust_join_team, null, false);
//
//                TextView cancel = (TextView) view.findViewById(R.id.cancel);
//                cancel.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (mDialog != null) {
//                            mDialog.dismiss();
//                        }
//                    }
//                });
//                TextView ok = (TextView) view.findViewById(R.id.ok);
//                ok.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (mDialog != null) {
//                            mDialog.dismiss();
//                        }
//                    }
//                });
//
//                mDialog.setContentView(view);
//                mDialog.show();
//            }
//        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    public void showProgress() {
        progressDialog = Utils.Progress(MemberLeaveActivity.this, "", "Login...", false);
        progressDialog.show();
        ;
//        mLoginProgressBar.setVisibility(View.VISIBLE);
//        mLoginProgressBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//            }
//        });
    }

    public void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
//        if(mLoginProgressBar != null){
//            mLoginProgressBar.setVisibility(View.GONE);
//        }
    }
}
