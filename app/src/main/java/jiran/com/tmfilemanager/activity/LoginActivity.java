package jiran.com.tmfilemanager.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.UserItem;
import jiran.com.tmfilemanager.network.JMF_network;
import jiran.com.tmfilemanager.utils.PreferenceHelper;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Namo on 2018-02-28.
 */

public class LoginActivity extends AppCompatActivity {

    //public static boolean mLoginFlag = false;

    public static String mLoginToken = "";

    //public static String mUserId = "";
    public static UserItem mUserItem;

    public static String mUserEmail = "";
    public static String mUserName = "";

    EditText edt_email;
    EditText edt_password;
    CheckBox auto_login_cb;
    Button login_btn;
    TextView sign_up_btn;

    Dialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ImageView title_left_btn = (ImageView) findViewById(R.id.title_left_btn);
        title_left_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_email.setText(PreferenceHelper.getString("access_email", ""));
        edt_password = (EditText) findViewById(R.id.edt_password);
        auto_login_cb = (CheckBox) findViewById(R.id.auto_login_cb);

        if (PreferenceHelper.contains("auto_login")) {
            if (PreferenceHelper.getBoolean("auto_login", false)) {
                auto_login_cb.setChecked(true);
            } else {
                auto_login_cb.setChecked(false);
            }
        } else {
            auto_login_cb.setChecked(true);
        }

        login_btn = (Button) findViewById(R.id.login_btn);
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = edt_email.getText().toString().trim();
                String password = edt_password.getText().toString().trim();

                if (email == null || email.length() == 0) {
                    Toast.makeText(LoginActivity.this, getString(R.string.login_input_email), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_password == null || edt_password.length() == 0) {
                    Toast.makeText(LoginActivity.this, getString(R.string.login_input_password), Toast.LENGTH_SHORT).show();
                    return;
                }

                showProgress();
                try {
                    JMF_network.tokenLogin(email, password)
                            .flatMap(gigaPodPair -> {
                                return Observable.just(gigaPodPair);
                            })
                            .doOnError(throwable -> {
                                dismissProgress();
                            })
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(gigaPodPair -> {
                                        if (gigaPodPair.first) {
                                            Log.i("JMF_tokenLogin", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                            JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                            mLoginToken = "Bearer " + result.get("access_token").getAsString();
                                            PreferenceHelper.putString("access_email", email);
                                            //checkToken();
                                            getUserInfo();
                                        } else {
                                            dismissProgress();
                                            Toast.makeText(LoginActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                        }
                                    },
                                    throwable -> {
                                        dismissProgress();
                                        throwable.printStackTrace();
                                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    dismissProgress();
                    Toast.makeText(LoginActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
                }
            }
        });

        sign_up_btn = (TextView) findViewById(R.id.sign_up_btn);
        sign_up_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                intent.putExtra("LoginActivity", true);
                startActivityForResult(intent, MainActivity.REQUEST_CODE_SIGN_UP_ACTIVITY);
            }
        });

        if (getIntent().getBooleanExtra("SignUpActivity", false)) {
            LinearLayout sign_up_btn_layout = (LinearLayout) findViewById(R.id.sign_up_btn_layout);
            sign_up_btn_layout.setVisibility(View.INVISIBLE);
        }
    }

    public void getUserInfo() {
        try {
            JMF_network.getUserInfo()
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                        PreferenceHelper.putString("access_token", "");
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF_getUserInfo", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    PreferenceHelper.putString("access_token", mLoginToken);
                                    PreferenceHelper.putBoolean("auto_login", auto_login_cb.isChecked());
                                    mUserEmail = edt_email.getText().toString().trim();
                                    mUserName = result.get("name").getAsString();
                                    PreferenceHelper.putString("user_name", mUserName);

                                    UserItem userItem = new UserItem();
                                    userItem.setEmail(result.get("email").getAsString());
                                    userItem.setName(result.get("name").getAsString());
                                    userItem.setProfile(result.get("profile").getAsString());
                                    userItem.setProfileThumb(result.get("profile_thumb").getAsString());
                                    userItem.setGrade(result.get("grade").getAsString());
                                    userItem.setStatus(result.get("status").getAsInt());
                                    userItem.setRegdate(Utils.formatDate(result.get("regdate").getAsString()));
                                    mUserItem = userItem;
                                    setResult(RESULT_OK);
                                    finish();
                                } else {
                                    mLoginToken = "";
                                    Toast.makeText(LoginActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                PreferenceHelper.putString("access_token", "");
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgress();
            Toast.makeText(LoginActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    private void checkToken() {
        try {
            JMF_network.checkToken()
                    .flatMap(gigaPodPair -> {
                        dismissProgress();
                        PreferenceHelper.putString("access_token", "");
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                        PreferenceHelper.putString("access_token", "");
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF_checkToken", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    PreferenceHelper.putString("access_token", mLoginToken);
                                    //mUserId = result.get("user_id").getAsString();
                                    mUserEmail = result.get("email").getAsString();
                                    //mUserExpire = result.get("expire").getAsString();
                                    setResult(RESULT_OK);
                                    finish();
                                    //Toast.makeText(LoginActivity.this, "Login SUCCESE", Toast.LENGTH_SHORT).show();
                                } else {
                                    mLoginToken = "";
                                    Toast.makeText(LoginActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                PreferenceHelper.putString("access_token", "");
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgress();
            Toast.makeText(LoginActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    public void showProgress() {
        progressDialog = Utils.Progress(LoginActivity.this, "", "Login...", false);
        progressDialog.show();
        ;
//        mLoginProgressBar.setVisibility(View.VISIBLE);
//        mLoginProgressBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//            }
//        });
    }

    public void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
//        if(mLoginProgressBar != null){
//            mLoginProgressBar.setVisibility(View.GONE);
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case MainActivity.REQUEST_CODE_SIGN_UP_ACTIVITY:
                    setResult(RESULT_OK);
                    finish();
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
