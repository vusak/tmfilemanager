package jiran.com.tmfilemanager.activity.trash;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Stack;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.InitActivity;
import jiran.com.tmfilemanager.common.SortUtils;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.CurrentItem;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.data.StorageListItem;
import jiran.com.tmfilemanager.data.TeamItem;
import jiran.com.tmfilemanager.network.JMF_network;
import jiran.com.tmfilemanager.settings.MyFileSettings;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by user on 2016-08-03.
 */


public class TrashFragment extends Fragment {

    public static boolean mActionMode = false;
    //    sort	String	N	디록토리 및 파일의 정렬값.(이름:+/-name, 날짜:+/-regdate, 용량:+/-size, 확장자: +/-extension)
//    limit	String	N	한번에 조회할 갯수.(기본값: 100)
//    offset	String	N	조회에서 제외할 갯수.(기본값: 0)
//    assort	String	N	모아보기 옵션(application, picture, movie, music, document, archive)
    public static String LIST_LIMIT = "100";
    public static boolean isLastpage = false;
    public Context mTrashContext;
    public TeamItem mTeam;
    public StorageListItem mCurrentArray;
    TextView trash_empty_btn;
    Stack<StorageListItem> listStack;
    Dialog mDialog;
    boolean isConnecting = false;
    LinearLayout list_dimd_layout;
    private AbsListView mListView;
    private TrashListAdapter mTrashListAdapter;
    private Animation showAni, goneAni;


    public TrashFragment() {
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public TrashFragment(Context context, TeamItem team, StorageListItem list) {
        mTrashContext = context;
        mTeam = team;
        mCurrentArray = list;
        setRetainInstance(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mTrashContext == null) {
            Intent intent = new Intent(getActivity(), InitActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        }
        View rootView = inflater.inflate(R.layout.frag_trash, container, false);
        initLayout(inflater, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void initLayout(LayoutInflater inflater, View rootView) {
        listStack = new Stack<StorageListItem>();
        //final MainActivity context = (MainActivity) getActivity();
        trash_empty_btn = (TextView) rootView.findViewById(R.id.trash_empty_btn);
        trash_empty_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emptyTrash();
            }
        });
        list_dimd_layout = (LinearLayout) rootView.findViewById(R.id.list_dimd_layout);

        mListView = (ListView) rootView.findViewById(R.id.folder_listview);
        mListView.setEmptyView(rootView.findViewById(R.id.empty_layout));

        displayListView();
        finishActionMode();
    }

    private void emptyTrash() {
        mDialog = new Dialog(mTrashContext, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(mTrashContext);
        View view = inflater.inflate(R.layout.dialog_base, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.team_manage_trash_info2));
        TextView content = (TextView) view.findViewById(R.id.content);
        content.setText(getResources().getString(R.string.team_manage_trash_dialog_info));
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                deleteTrashFile(null);
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    private void displayListView() {
        ((TeamTrashListActivity) mTrashContext).setSelectedTitle("");
        //final MainActivity context = (MainActivity) getActivity();
        mTrashListAdapter = new TrashListAdapter(getActivity(), null, 0);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FileItem selectFileItem = ((FileItem) mListView.getAdapter().getItem(position));
                if (mActionMode) {
                    mTrashListAdapter.setCheck(position);
                    mTrashListAdapter.notifyDataSetChanged();
                    int count = mTrashListAdapter.getCheckCount();
                    ((TeamTrashListActivity) mTrashContext).setSelectedTitle(count == 0 ? "" : count + " Selected");
                } else {
//                    if (selectFileItem.isDirectory()) {
//                        getTrashList(false, mTeam.getId(), "");
//                        // go to the top of the ListView
//                        //mListView.setSelection(0);
//                    } else {
//                        //파일일 경우
//                        if(!checkFile(selectFileItem.getName())){
//
//                        }
//                    }
                }
            }
        });

        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (!mActionMode)
                    mTrashListAdapter.setCheckClear();
                mTrashListAdapter.setCheck(position);
                int count = mTrashListAdapter.getCheckCount();
                ((TeamTrashListActivity) mTrashContext).setSelectedTitle(count == 0 ? "" : count + " Selected");
                setActionMode();
                return true;
            }
        });
        mListView.setAdapter(mTrashListAdapter);
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub
                if (!isConnecting && !isLastpage && (mListView.getLastVisiblePosition() == mListView.getAdapter().getCount() - 1
                        && mListView.getChildAt(mListView.getChildCount() - 1).getBottom() <= mListView.getHeight())) {
                    //scroll end reached
                    //Write your code here
                    getTrashList(true, mTeam.getId(), mCurrentArray.getList().size() + "");
                    //Toast.makeText(mMainActivity, "LAST", Toast.LENGTH_SHORT).show();
                    Log.i("onScrollStateChanged", "mCurrentArray.getList().size() => " + mCurrentArray.getList().size() + " Add Item Action");
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                // TODO Auto-generated method stub
            }
        });


        getTrashList(false, mTeam.getId(), "");
        //storageListDisplay(mCurrentArray);
//        mFolderAdapter.addFiles(mCurrentPath);
//        mFolderAdapter.notifyDataSetChanged();
    }

    public void storageListDisplay(StorageListItem list) {
        //mFoler_path.setText(mCurrentPath);
        mCurrentArray = list;
        mTrashListAdapter.addContent(list.getList());
    }

    public void storageListDisplayBack(StorageListItem list) {
        //mFoler_path.setText(mCurrentPath);
        mCurrentArray = list;
        isLastpage = false;
        mTrashListAdapter.addContent(list.getList());

        //((TeamTrashListActivity)mTrashContext).toolbarMenu(R.menu.main_storage);
    }

    public void setActionMode() {
        mActionMode = true;
        //getActivity().setTitle("0 " +  getResources().getString(R.string.selected));
        ((TeamTrashListActivity) mTrashContext).multiSelectMode();
        mTrashListAdapter.notifyDataSetChanged();
    }

    public void finishActionMode() {
        //getActivity().setTitle(getResources().getString(R.string.app_name));
        //((MainActivity)getActivity()).setTitle(mTeam.getTeamName());
        mActionMode = false;
        ((TeamTrashListActivity) mTrashContext).finishMultiSelectMode();
        mTrashListAdapter.setCheckClear();
        mTrashListAdapter.notifyDataSetChanged();
    }

    public void listAllcheck() {
        if (mTrashListAdapter.isAllChecked()) {
            mTrashListAdapter.setCheckClear();
            mTrashListAdapter.notifyDataSetChanged();
        }
        else {
            mTrashListAdapter.setAllCheck();
            mTrashListAdapter.notifyDataSetChanged();
        }

        int count = mTrashListAdapter.getCheckCount();
        ((TeamTrashListActivity) mTrashContext).setSelectedTitle(count == 0 ? "" : count + " Selected");
    }

    public void listCheckClear() {
        mTrashListAdapter.setCheckClear();
        mTrashListAdapter.notifyDataSetChanged();
    }

    public boolean onBackPressed() {

        if (mActionMode) {
            finishActionMode();
            return false;
        } else {
            return true;
        }
    }

    public void deleteMethod() {
        int check_count = mTrashListAdapter.getCheckCount();

        if (check_count > 0) {
            deleteTrashFile(mTrashListAdapter.getCheckList());
        } else {
            Toast.makeText(mTrashContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
            //finishActionMode();
        }
    }

    public void recoveryMethod() {
        int check_count = mTrashListAdapter.getCheckCount();
        if (check_count > 0) {
            recoveryTrashFile(mTrashListAdapter.getCheckList());
        } else {
            Toast.makeText(mTrashContext, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
            //finishActionMode();
        }
    }

    public void deleteTrashFile(ArrayList<FileItem> checkList) {
        try {
            isConnecting = true;
            String delete = "";
            if (checkList != null) {
                for (FileItem check : checkList) {
                    delete += ("," + check.getId());
                }
                delete = delete.replaceFirst(",", "");
            }
            Log.i("JMF delete", "id =>=> " + delete);
            JMF_network.trashDelete(mTeam.getId(), delete)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        ((TeamTrashListActivity) mTrashContext).dismissProgress();
                        isConnecting = false;
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                isConnecting = false;
                                if (gigaPodPair.first) {
                                    Log.i("JMF trashDelete", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    if (checkList != null) {
                                        for (FileItem check : checkList) {
                                            mTrashListAdapter.removeFileItem(check);
                                        }
                                    } else {
                                        mTrashListAdapter.addContent(new ArrayList<FileItem>());
                                    }
                                    Toast.makeText(mTrashContext, mTrashContext.getString(R.string.snackbar_trash_delete), Toast.LENGTH_SHORT).show();
                                    finishActionMode();
                                } else {
                                    //Toast.makeText(mTrashContext, mTrashContext.getString(R.string.snackbar_trash_delete_fail), Toast.LENGTH_SHORT).show();
                                    Toast.makeText(mTrashContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                ((TeamTrashListActivity) mTrashContext).dismissProgress();
                                isConnecting = false;
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            ((TeamTrashListActivity) mTrashContext).dismissProgress();
            isConnecting = false;
            e.printStackTrace();
            Toast.makeText(mTrashContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void recoveryTrashFile(ArrayList<FileItem> checkList) {
        try {
            isConnecting = true;
            String recovery = "";
            for (FileItem check : checkList) {
                recovery += ("," + check.getId());
            }
            recovery = recovery.replaceFirst(",", "");
            Log.i("JMF recovery", "id =>=> " + recovery);
            ((TeamTrashListActivity) mTrashContext).showProgress();
            JMF_network.trashRecovery(mTeam.getId(), recovery)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        ((TeamTrashListActivity) mTrashContext).dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                ((TeamTrashListActivity) mTrashContext).dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF recoveryTrashFile", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    for (FileItem check : checkList) {
                                        mTrashListAdapter.removeFileItem(check);
                                    }
                                    Toast.makeText(mTrashContext, mTrashContext.getString(R.string.snackbar_trash_recovery), Toast.LENGTH_SHORT).show();
                                    finishActionMode();
                                } else {
                                    //Toast.makeText(mTrashContext, mTrashContext.getString(R.string.snackbar_trash_recovery_fail), Toast.LENGTH_SHORT).show();
                                    Toast.makeText(mTrashContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                ((TeamTrashListActivity) mTrashContext).dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            ((TeamTrashListActivity) mTrashContext).dismissProgress();
            e.printStackTrace();
            Toast.makeText(mTrashContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }


    public void listRefresh() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
        mCurrentArray.setList(new ArrayList<FileItem>());
        getTrashList(true, mTeam.getId(), "");
        finishActionMode();
    }

    public ArrayList<FileItem> getCheckList() {
        return mTrashListAdapter.getCheckList();
    }

    public void getTrashList(boolean paging, String teamId, String offset) {
        try {
            ((TeamTrashListActivity) mTrashContext).showProgress();
            isConnecting = true;
            String sort = "";
            if (MyFileSettings.getSortReverse()) {
                sort = "+";
            } else {
                sort = "-";
            }
            switch (MyFileSettings.getSortType()) {
                case SortUtils.SORT_ALPHA:
                    sort += "name";
                    break;
                case SortUtils.SORT_TYPE:
                    sort += "extension";
                    break;
                case SortUtils.SORT_SIZE:
                    sort += "size";
                    break;
                case SortUtils.SORT_DATE:
                    sort += "regdate";
                    break;
                default:
                    sort += "name";
                    break;
            }
            JMF_network.getTrashList(teamId, sort, LIST_LIMIT, offset)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        ((TeamTrashListActivity) mTrashContext).dismissProgress();
                        isConnecting = false;
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                ((TeamTrashListActivity) mTrashContext).dismissProgress();
                                isConnecting = false;
                                if (gigaPodPair.first) {
                                    Log.i("JMF getTrashList", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");

                                    JsonObject current = result.getAsJsonObject("current");
                                    CurrentItem currentItem = new CurrentItem();
//                                    currentItem.setObjectId(current.get("object_id").getAsString());
//                                    currentItem.setNode(current.get("node").getAsString());
//
//                                    currentItem.setName(current.get("name").getAsString());
//                                    currentItem.setCategoryId("");
//
//                                    currentItem.setSize(current.get("size").getAsString());
//                                    currentItem.setRegdate(Utils.formatDate(current.get("regdate").getAsString()));
//                                    currentItem.setModified(Utils.formatDate(current.get("modified").getAsString()));

                                    JsonObject lists = result.getAsJsonObject("lists");
                                    isLastpage = lists.get("lastpage").getAsBoolean();
                                    JsonArray rows = lists.getAsJsonArray("rows");
                                    ArrayList<FileItem> storageList = new ArrayList<FileItem>();
                                    for (int i = 0; i < rows.size(); i++) {
                                        FileItem item = new FileItem();
                                        JsonObject row = rows.get(i).getAsJsonObject();
                                        item.setTeamId(mTeam.getId());
                                        item.setId(row.get("object_id").getAsString());
                                        item.setSObjectType(row.get("object_type").getAsString());
                                        item.setSNode(row.get("node").getAsString());
                                        item.setName(row.get("name").getAsString());
                                        item.setSCreator(row.get("creator").getAsString());
                                        item.setSCreatorName(row.get("creator_name").getAsString());
                                        item.setSMemo(row.get("memo").getAsString());
                                        item.setSize(row.get("size").getAsString());
                                        item.setSExtension(row.get("extension").getAsString());
                                        //item.setSThumbnail(row.get("thumbnail").getAsString());
                                        //item.setSUrlPath(row.get("url_path").getAsString());
                                        item.setRegDate(Utils.formatDate(row.get("regdate").getAsString()));
                                        item.setSModifedDate(Utils.formatDate(row.get("modified").getAsString()));

                                        if (paging) {
                                            mCurrentArray.getList().add(item);
                                        } else {
                                            storageList.add(item);
                                        }
                                    }

                                    if (paging) {
                                        if (offset != null && offset.length() == 0) {
                                            mListView.setSelection(0);

                                        }
                                        mTrashListAdapter.addContent(mCurrentArray.getList());
                                        mTrashListAdapter.notifyDataSetChanged();
                                    } else {
                                        StorageListItem storageListItem = new StorageListItem(currentItem, storageList);
                                        storageListDisplay(storageListItem);
                                    }
                                } else {
                                    Toast.makeText(mTrashContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                ((TeamTrashListActivity) mTrashContext).dismissProgress();
                                isConnecting = false;
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            ((TeamTrashListActivity) mTrashContext).dismissProgress();
            isConnecting = false;
            e.printStackTrace();
            Toast.makeText(mTrashContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean checkFile(String name) {
        try {

            String path = Utils.getStorageSaveFileFolderPath() + "/" + name;
            File file = new File(path);

            if (file != null && file.exists()) {
                Utils.openFile(mTrashContext, file);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
