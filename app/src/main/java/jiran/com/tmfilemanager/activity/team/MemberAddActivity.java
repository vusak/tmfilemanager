package jiran.com.tmfilemanager.activity.team;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Stack;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.TeamItem;
import jiran.com.tmfilemanager.data.UserItem;
import jiran.com.tmfilemanager.network.JMF_network;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Namo on 2018-02-28.
 */

public class MemberAddActivity extends AppCompatActivity {
    TeamItem mTeam;

    RelativeLayout send_email_btn;
    EditText email_edt;
    TextView member_add_btn;

    LinearLayout add_member_contents_list;
    Stack<String> addUserStack;

    Dialog mDialog;
    Dialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_add);

        mTeam = (TeamItem) getIntent().getSerializableExtra("TeamItem");

        LinearLayout title_left_btn = (LinearLayout) findViewById(R.id.title_left_btn);
        title_left_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        email_edt = (EditText) findViewById(R.id.email_edt);
        member_add_btn = (TextView) findViewById(R.id.member_add_btn);
        member_add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String editStr = email_edt.getText().toString();
                if (editStr.length() == 0) {
                    Utils.notiAlert(MemberAddActivity.this, getString(R.string.login_input_email));
                    return;
                }
                if (!Utils.emailvalidate(editStr)) {
                    Utils.notiAlert(MemberAddActivity.this, getString(R.string.team_add_member_err_email_pattern));
                    return;
                }
                ArrayList<UserItem> userList = mTeam.getUserList();
                for (UserItem user : userList) {
                    if (user.getEmail().equals(editStr)) {
                        Utils.notiAlert(MemberAddActivity.this, getString(R.string.team_add_member_email_duplicate_noti));
                        return;
                    }
                }
                int totalCount = add_member_contents_list.getChildCount();
                for (int i = 0; i < totalCount; i++) {
                    String email = (String) add_member_contents_list.getChildAt(i).getTag();
                    if (editStr.equals(email)) {
                        Utils.notiAlert(MemberAddActivity.this, getString(R.string.team_add_member_email_duplicate_noti2));
                        return;
                    }
                }

                LayoutInflater inflater = LayoutInflater.from(MemberAddActivity.this);
                View user_view = inflater.inflate(R.layout.list_team_member_add_item, null, false);

                TextView email_tv = (TextView) user_view.findViewById(R.id.email_tv);
                ImageView delete_iv = (ImageView) user_view.findViewById(R.id.delete_iv);
                email_tv.setText(editStr);
                delete_iv.setTag(editStr);
                user_view.setTag(editStr);
                delete_iv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int totalCount = add_member_contents_list.getChildCount();
                        String clickEmail = (String) view.getTag();
                        for (int i = 0; i < totalCount; i++) {
                            String email = (String) add_member_contents_list.getChildAt(i).getTag();
                            if (email.equals(clickEmail)) {
                                add_member_contents_list.removeViewAt(i);
                                return;
                            }
                        }
                    }
                });
                email_edt.setText("");

                ArrayList<View> views = new ArrayList<View>();
                for (int x = add_member_contents_list.getChildCount() - 1; x >= 0; x--) {
                    views.add(add_member_contents_list.getChildAt(x));
                }
                views.add(user_view);
                add_member_contents_list.removeAllViews();
                for (int x = views.size() - 1; x >= 0; x--) {
                    add_member_contents_list.addView(views.get(x));
                }
                //add_member_contents_list.addView(user_view);
            }
        });

        add_member_contents_list = (LinearLayout) findViewById(R.id.add_member_contents_list);
        add_member_contents_list.removeAllViews();
        send_email_btn = (RelativeLayout) findViewById(R.id.send_email_btn);
        send_email_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addUserStack = new Stack<String>();
                int totalCount = add_member_contents_list.getChildCount();
                if (totalCount > 0) {
                    for (int i = 0; i < totalCount; i++) {
                        String email = (String) add_member_contents_list.getChildAt(i).getTag();
                        addUserStack.add(email);
                    }
                    addMember();
                } else {
                    Toast.makeText(MemberAddActivity.this, getString(R.string.team_add_member_add_email), Toast.LENGTH_SHORT).show();
                }
            }
        });


//        TextView email_add = (TextView) findViewById(R.id.email_add);
//        email_add.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mDialog = new Dialog(MemberLeaveActivity.this, R.style.Theme_TransparentBackground);
//                LayoutInflater inflater = LayoutInflater.from(MemberLeaveActivity.this);
//                View view = inflater.inflate(R.layout.dialog_reqeust_join_team, null, false);
//
//                TextView cancel = (TextView) view.findViewById(R.id.cancel);
//                cancel.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (mDialog != null) {
//                            mDialog.dismiss();
//                        }
//                    }
//                });
//                TextView ok = (TextView) view.findViewById(R.id.ok);
//                ok.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if (mDialog != null) {
//                            mDialog.dismiss();
//                        }
//                    }
//                });
//
//                mDialog.setContentView(view);
//                mDialog.show();
//            }
//        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = Utils.Progress(MemberAddActivity.this, "", "Login...", false);
            progressDialog.show();
            ;
        }

//        mLoginProgressBar.setVisibility(View.VISIBLE);
//        mLoginProgressBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//            }
//        });
    }

    public void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
//        if(mLoginProgressBar != null){
//            mLoginProgressBar.setVisibility(View.GONE);
//        }
    }

    public void addMember() {
        try {
            if (addUserStack.size() > 0) {
                showProgress();
                //(String team_id, String volume, String join_rule, String join_protect_domain, String flag_push, String trash_option)
                String email = addUserStack.get(addUserStack.size() - 1);
                addUserStack.pop();
                JMF_network.addTeamMember(mTeam.getId(), email)
                        .flatMap(gigaPodPair -> {
                            return Observable.just(gigaPodPair);
                        })
                        .doOnError(throwable -> {
                            dismissProgress();
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(gigaPodPair -> {
                                    if (gigaPodPair.first) {
                                        setResult(RESULT_OK);
                                        Log.i("JMF addTeamMember", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                        JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    } else {
                                        //dismissProgress();
                                        //Toast.makeText(MemberAddActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                    }
                                    addMember();
                                },
                                throwable -> {
                                    dismissProgress();
                                    throwable.printStackTrace();
                                });
            } else {
                dismissProgress();
                //추가완료
                mDialog = new Dialog(MemberAddActivity.this, R.style.Theme_TransparentBackground);
                LayoutInflater inflater = LayoutInflater.from(MemberAddActivity.this);
                View view = inflater.inflate(R.layout.dialog_base_one_button, null, false);
                TextView title = (TextView) view.findViewById(R.id.title);
                title.setText(getResources().getString(R.string.dialog_notice));
                TextView content = (TextView) view.findViewById(R.id.content);
                content.setText(getString(R.string.team_add_member_complete));
                TextView ok = (TextView) view.findViewById(R.id.ok);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mDialog != null) {
                            mDialog.dismiss();
                        }
                        finish();
                    }
                });
                mDialog.setCancelable(false);
                mDialog.setContentView(view);
                mDialog.show();
//                Intent intent = new Intent();
//                intent.putExtra("TeamItem", mTeam);
//                setResult(RESULT_OK, intent);
            }
        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
            Toast.makeText(MemberAddActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }
}
