package jiran.com.tmfilemanager.activity.viewer;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.File;

import jiran.com.circleprogress.DonutProgress;
import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.InitActivity;
import jiran.com.tmfilemanager.common.MimeTypes;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.network.JMF_network;
import jiran.com.tmfilemanager.network.updown.DownloadProgressListener;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by user on 2016-08-03.
 */


public class ImagePageFragment extends Fragment {
    public Context mMainContext;
    boolean isConnecting = false;
    Dialog downloadProgressDialog;
    DonutProgress download_percent_dp;
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            setDownlaodProgress(msg.what);
        }
    };
    DownloadProgressListener listener = new DownloadProgressListener() {
        @Override
        public void update(int percent, int done) {
            //start a new thread to process job
            new Thread(new Runnable() {
                @Override
                public void run() {
                    //heavy job here
                    //send message to main thread
                    handler.sendEmptyMessage(percent);
                }
            }).start();
            //setDownlaodProgress(percent);
            //Log.i("DownloadProgressListener", "percent => " + percent + "%");
        }
    };
    private FileItem mItem;
    private Dialog mDialog;
    private ImageView imgDisplay;
    private ProgressBar progress;
    private boolean imageDisplay = false;

    public ImagePageFragment() {
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public ImagePageFragment(Context context, FileItem item) {
        mMainContext = context;
        mItem = item;
        setRetainInstance(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mMainContext == null) {
            Intent intent = new Intent(getActivity(), InitActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        }
        View rootView = inflater.inflate(R.layout.fragment_imagepage, container, false);
        initLayout(inflater, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void initLayout(LayoutInflater inflater, View rootView) {
        //final MainActivity context = (MainActivity) getActivity();
        //Setting

        imgDisplay = (ImageView) rootView.findViewById(R.id.imgDisplay);
        progress = (ProgressBar) rootView.findViewById(R.id.progress);

        imageDisplay();
    }

    public void applicationRestart() {
        Intent intent = getActivity().getBaseContext().getPackageManager().getLaunchIntentForPackage(getActivity().getPackageName());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        getActivity().startActivity(intent);
        getActivity().finish();
    }

    public void imageDisplay() {
        if (mItem.getPath() != null || checkFile(mItem)) {
            Glide.with(mMainContext)
                    .load(new File(mItem.getPath()))
                    .placeholder(R.color.black)
                    .listener(new RequestListener<File, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, File model, Target<GlideDrawable> target, boolean isFirstResource) {
                            progress.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, File model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            progress.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    //.bitmapTransform(new CenterCrop(this), new RoundedCornersTransformation(this, 10, 0))
                    .into(imgDisplay);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (mItem.getSObjectType() != null && mItem.getSObjectType().length() > 0) {
            if (isVisibleToUser) {
                //화면에 실제로 보일때
                if (!checkFile(mItem)) {
                    downloadfile(mItem.getTeamId(), mItem);
                }
                //Toast.makeText(mMainContext, "Image onResume => " + mItem.getName(), Toast.LENGTH_SHORT).show();
            } else {
                //preload 될때(전페이지에 있을때)
            }
        }
    }

    public boolean checkFile(FileItem item) {
        try {
            String path = Utils.getStorageSaveFileFolderPath() + "/" + item.getName();
            File file = new File(path);

            if (file != null && file.exists()) {
                if (MimeTypes.isPicture(file)) {
                    item.setcheck(true);
                    item.setPath(path);
                }
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void downloadfile(String teamId, FileItem downItem) {
        try {
            showDownlaodProgress();
            JMF_network.filedownload(teamId, downItem.getId(), Utils.getStorageSaveFileFolderPath(), downItem.getName(), listener)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissDownlaodProgress();
                        isConnecting = false;
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissDownlaodProgress();
                                isConnecting = false;
                                if (gigaPodPair.first) {
                                    Log.i("JMF filedownload", "=>=>" + ((String) gigaPodPair.second));
                                    String path = ((String) gigaPodPair.second);
                                    File file = new File(path);
                                    if (MimeTypes.isPicture(file)) {
                                        mItem.setcheck(true);
                                        mItem.setPath(path);
                                        imageDisplay();
                                        return;
                                    }
                                } else {
                                    Toast.makeText(mMainContext, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissDownlaodProgress();
                                isConnecting = false;
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissDownlaodProgress();
            isConnecting = false;
            e.printStackTrace();
            Toast.makeText(mMainContext, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void showDownlaodProgress() {
        LayoutInflater inflater = LayoutInflater.from(mMainContext);
        View view = inflater.inflate(R.layout.progress_download_dialog_gray, null, false);

        download_percent_dp = (DonutProgress) view.findViewById(R.id.download_percent_dp);
        downloadProgressDialog = new Dialog(mMainContext, R.style.Theme_TransparentBackground);
        downloadProgressDialog.setContentView(view);
        downloadProgressDialog.setCancelable(false);
        downloadProgressDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        downloadProgressDialog.show();
    }

    public void setDownlaodProgress(Integer percent) {
        download_percent_dp.setProgress(percent);
    }

    public void dismissDownlaodProgress() {
        if (downloadProgressDialog != null) {
            downloadProgressDialog.dismiss();
        }
    }
}
