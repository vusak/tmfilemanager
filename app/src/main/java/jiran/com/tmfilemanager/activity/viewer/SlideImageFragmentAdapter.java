package jiran.com.tmfilemanager.activity.viewer;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.io.File;
import java.util.ArrayList;

import jiran.com.tmfilemanager.common.MimeTypes;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.data.ImageModel;

/**
 * Created by user on 2017-02-06.
 */

public class SlideImageFragmentAdapter extends FragmentPagerAdapter {

    private final int IMAGE = 0;
    private final int MOVIE = 1;
    private final int PDF = 2;
    private Activity _activity;
    private ArrayList<FileItem> _fileArray;

    public SlideImageFragmentAdapter(FragmentManager fm, Activity activity,
                                     ArrayList<FileItem> items) {
        super(fm);
        this._activity = activity;
        this._fileArray = items;
    }

    @Override
    public Fragment getItem(int position) {

        FileItem item = _fileArray.get(position);
        File file;
        int type = IMAGE;
        if (item.getPath() != null && item.getPath().length() > 0) {
            file = new File(item.getPath());
            if (MimeTypes.isPdf(file)) {
                type = PDF;
            } else if (MimeTypes.isVideo(file)) {
                type = MOVIE;
            } else {
                type = IMAGE;
            }
        } else {
            if (MimeTypes.isPdf(item.getName())) {
                type = PDF;
            } else if (MimeTypes.isVideo(item.getName())) {
                type = MOVIE;
            } else {
                type = IMAGE;
            }
        }

        switch (type) {
            case PDF:
                PdfPageFragment pdfFragment = new PdfPageFragment(_activity, item);
                return pdfFragment;
            case MOVIE:
                ImageModel model = new ImageModel();
                if (item.getPath() == null || item.getPath().length() == 0) {
                    model.setFile_id("");
                } else {
                    model.setFile_id("file://" + new File(item.getPath()).getPath());
                }
                model.setFileItem(item);
                model.setContext(_activity);
                MoviePageFragment movieFragment = MoviePageFragment.newInstance(null, model, false);

                return movieFragment;
            default:
                ImagePageFragment imageFragment = new ImagePageFragment(_activity, item);
                return imageFragment;
        }


//        if (MimeTypes.isPdf(item.getName()) || MimeTypes.isPdf(file)) {
//            PdfPageFragment pdfFragment = new PdfPageFragment(_activity, item);
//            return pdfFragment;
//        }else if(MimeTypes.isVideo(item.getName()) || MimeTypes.isVideo(file)) {
//            ImageModel model = new ImageModel();
//            if(item.getPath() == null || item.getPath().length() == 0){
//                model.setFile_id("");
//            }else{
//                model.setFile_id("file://" + new File(item.getPath()).getPath());
//            }
//            model.setFileItem(item);
//            model.setContext(_activity);
//            MoviePageFragment movieFragment = MoviePageFragment.newInstance(null, model, false);
//
//            return movieFragment;
//        }else {
//            ImagePageFragment imageFragment = new ImagePageFragment(_activity, item);
//            return imageFragment;
//        }
    }

    @Override
    public int getCount() {
        return this._fileArray.size();
    }
}