package jiran.com.tmfilemanager.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import jiran.com.inappbilling.InAppBillingActivity;
import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.team.MemberLeaveActivity;
import jiran.com.tmfilemanager.common.MimeTypes;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.UserItem;
import jiran.com.tmfilemanager.network.JMF_network;
import jiran.com.tmfilemanager.utils.PreferenceHelper;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Namo on 2018-02-28.
 */

public class AccountManageActivity extends AppCompatActivity {
    final int REQUESTCODE_UPLOAD_TAKEPHOTO = 0;
    final int REQUESTCODE_UPLOAD_TAKEMOVIE = 1;


    UserItem mUser;
    boolean leader = false;

    LinearLayout rename, password_change, cache_delete, inappbilling, member_leave_btn, rule_info_layout, account_manage_use_rule_layout, account_manage_user_info_layout;
    TextView user_email_tv, user_name_tv, user_level_tv;
    ImageView rename_arrow;
    ImageView profile_iv;

    Dialog mDialog;
    Dialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_manage_info);

        if (getIntent().getSerializableExtra("UserItem") != null) {
            mUser = (UserItem) getIntent().getSerializableExtra("UserItem");
        }

        leader = getIntent().getBooleanExtra("Leader", false);

        LinearLayout title_left_btn = (LinearLayout) findViewById(R.id.title_left_btn);
        title_left_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        user_email_tv = (TextView) findViewById(R.id.user_email_tv);
        user_name_tv = (TextView) findViewById(R.id.user_name_tv);
        user_level_tv = (TextView) findViewById(R.id.user_level_tv);
        if (mUser != null) {
            user_email_tv.setText(mUser.getEmail());
            user_name_tv.setText(mUser.getName());
        } else {
            user_email_tv.setText(LoginActivity.mUserItem.getEmail());
            user_name_tv.setText(LoginActivity.mUserItem.getName());
        }
        user_level_tv.setText("준비중");


        rename = (LinearLayout) findViewById(R.id.rename);
        rename_arrow = (ImageView) findViewById(R.id.rename_arrow);
        password_change = (LinearLayout) findViewById(R.id.password_change);
        cache_delete = (LinearLayout) findViewById(R.id.cache_delete);
        inappbilling = (LinearLayout) findViewById(R.id.inappbilling);
        rule_info_layout = (LinearLayout) findViewById(R.id.rule_info_layout);


        profile_iv = (ImageView) findViewById(R.id.profile_iv);
        if (mUser == null || LoginActivity.mUserEmail.equals(mUser.getEmail())) {
            profile_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showPictureDialog();
                }
            });
        }

        String profileThumb = "";
        if (mUser != null) {
            if (mUser.getProfileThumb() != null && mUser.getProfileThumb().length() > 0) {
                profileThumb = mUser.getProfileThumb();
            }
        } else {
            if (LoginActivity.mUserItem.getProfileThumb() != null && LoginActivity.mUserItem.getProfileThumb().length() > 0) {
                profileThumb = LoginActivity.mUserItem.getProfileThumb();
            }
        }
        if (profileThumb.length() > 0) {
            Glide.with(AccountManageActivity.this).load(profileThumb).asBitmap().centerCrop().placeholder(R.mipmap.user_none).into(new BitmapImageViewTarget(profile_iv) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    profile_iv.setImageDrawable(circularBitmapDrawable);
                }
            });
        }

        rename = (LinearLayout) findViewById(R.id.rename);
        rename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUser == null || LoginActivity.mUserEmail.equals(mUser.getEmail())) {
                    mDialog = new Dialog(AccountManageActivity.this, R.style.Theme_TransparentBackground);
                    LayoutInflater inflater = LayoutInflater.from(AccountManageActivity.this);
                    View view = inflater.inflate(R.layout.dialog_new_folder, null, false);

                    TextView title = (TextView) view.findViewById(R.id.title);
                    title.setText(getResources().getString(R.string.menu_rename));

                    EditText edt = (EditText) view.findViewById(R.id.edt);
                    edt.setText(user_name_tv.getText().toString());
                    int maxLength = 10;
                    edt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});

                    edt.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

                    TextView cancel = (TextView) view.findViewById(R.id.cancel);
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                            if (mDialog != null) {
                                mDialog.dismiss();
                            }
                        }
                    });
                    TextView create = (TextView) view.findViewById(R.id.create);
                    create.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                            if (mDialog != null) {
                                mDialog.dismiss();
                            }
                            setUserInfo(edt.getText().toString());
                        }
                    });

                    mDialog.setContentView(view);
                    mDialog.show();

                }
            }
        });

        //password_change = (LinearLayout) findViewById(R.id.password_change);
        password_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(AccountManageActivity.this, "준비중...", Toast.LENGTH_SHORT).show();

                mDialog = new Dialog(AccountManageActivity.this, R.style.Theme_TransparentBackground);
                LayoutInflater inflater = LayoutInflater.from(AccountManageActivity.this);
                View view = inflater.inflate(R.layout.dialog_change_password, null, false);

                final LinearLayout email_confirm_layout = (LinearLayout) view.findViewById(R.id.email_confirm_layout);

                EditText edt_current_password = (EditText) view.findViewById(R.id.edt_current_password);
                EditText edt_new_password = (EditText) view.findViewById(R.id.edt_new_password);
                EditText edt_check_new_password = (EditText) view.findViewById(R.id.edt_check_new_password);

                TextView cancel = (TextView) view.findViewById(R.id.cancel);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mDialog != null) {
                            mDialog.dismiss();
                        }
                    }
                });
                TextView ok = (TextView) view.findViewById(R.id.ok);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String currentPw = edt_current_password.getText().toString();
                        String newPw = edt_new_password.getText().toString();
                        String newCheckPw = edt_check_new_password.getText().toString();

                        if (currentPw == null || currentPw.length() == 0) {
                            Toast.makeText(AccountManageActivity.this, getString(R.string.dialog_change_input_password), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (newPw == null || newPw.length() == 0) {
                            Toast.makeText(AccountManageActivity.this, getString(R.string.dialog_change_input_new_password), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (newCheckPw == null || newCheckPw.length() == 0) {
                            Toast.makeText(AccountManageActivity.this, getString(R.string.dialog_change_input_new_repassword), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (!newPw.equals(newCheckPw)) {
                            Toast.makeText(AccountManageActivity.this, getString(R.string.dialog_change_new_password_not_match), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        showProgress();
                        try {
                            JMF_network.changePassword(currentPw, newPw, newCheckPw)
                                    .flatMap(gigaPodPair -> {
                                        return Observable.just(gigaPodPair);
                                    })
                                    .doOnError(throwable -> {
                                        dismissProgress();
                                    })
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(gigaPodPair -> {
                                                dismissProgress();
                                                if (gigaPodPair.first) {
                                                    if (mDialog != null) {
                                                        mDialog.dismiss();
                                                    }
                                                    Toast.makeText(AccountManageActivity.this, getString(R.string.dialog_change_complete), Toast.LENGTH_SHORT).show();
                                                } else {
                                                    Toast.makeText(AccountManageActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                                }
                                            },
                                            throwable -> {
                                                dismissProgress();
                                                throwable.printStackTrace();
                                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                            dismissProgress();
                            Toast.makeText(AccountManageActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                TextView confirm_ok = (TextView) view.findViewById(R.id.confirm_ok);
                confirm_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        getActivity().getSupportFragmentManager().beginTransaction()
//                                .replace(R.id.content_frame, new SecretFolderFragment(mMainContext, Utils.getInternalDirectoryPath(), true))
//                                .commit();
                        if (mDialog != null) {
                            mDialog.dismiss();
                        }
                    }
                });

                mDialog.setContentView(view);
                mDialog.show();
            }
        });

        cache_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(AccountManageActivity.this, "준비중...", Toast.LENGTH_SHORT).show();
                Dialog mDialog = new Dialog(AccountManageActivity.this, R.style.Theme_TransparentBackground);
                LayoutInflater inflater = LayoutInflater.from(AccountManageActivity.this);
                View view = inflater.inflate(R.layout.dialog_base, null, false);
                TextView title = (TextView) view.findViewById(R.id.title);
                title.setText(getResources().getString(R.string.dialog_notice));
                TextView content = (TextView) view.findViewById(R.id.content);
                content.setText(getString(R.string.account_manage_msg_cache_delete));
                TextView ok = (TextView) view.findViewById(R.id.ok);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean success = true;
                        success = Utils.deleteTarget(Utils.getStorageSaveFileFolderPath());

                        if (success) {
                            Toast.makeText(AccountManageActivity.this, getResources().getString(R.string.account_manage_msg_cache_deleted), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(AccountManageActivity.this, getResources().getString(R.string.snackbar_delete_error), Toast.LENGTH_SHORT).show();
                        }
                        if (mDialog != null) {
                            mDialog.dismiss();
                        }
                    }
                });
                TextView cancel = (TextView) view.findViewById(R.id.cancel);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mDialog != null) {
                            mDialog.dismiss();
                        }
                    }
                });
                mDialog.setContentView(view);
                mDialog.show();
            }
        });

        inappbilling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(AccountManageActivity.this, "준비중...", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(AccountManageActivity.this, InAppBillingActivity.class);
                startActivityForResult(intent, MainActivity.REQUEST_CODE_INAPP_BILLING_ACTIVITY);
            }
        });

        account_manage_use_rule_layout = (LinearLayout) findViewById(R.id.account_manage_use_rule_layout);
        account_manage_use_rule_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(AccountManageActivity.this, "준비중...", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://jmobile.io/privacy/"));
                startActivity(intent);

            }
        });
        account_manage_user_info_layout = (LinearLayout) findViewById(R.id.account_manage_user_info_layout);
        account_manage_user_info_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(AccountManageActivity.this, "준비중...", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://jmobile.io/terms/"));
                startActivity(intent);
            }
        });

        member_leave_btn = (LinearLayout) findViewById(R.id.member_leave_btn);
        member_leave_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(AccountManageActivity.this, "준비중...", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(AccountManageActivity.this, MemberLeaveActivity.class);
                startActivityForResult(intent, MainActivity.REQUEST_CODE_LEAVE_MEMBER_ACTIVITY);
            }
        });

//        if(mUser != null){
//            user_email_tv.setText(mUser.getEmail());
//            user_name_tv.setText(mUser.getName());
//            user_level_tv.setText(mUser.getGrade());
//        }else{
//            getUserInfo();
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void getUserInfo() {
        showProgress();
        try {
            JMF_network.getUserInfo()
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    setResult(RESULT_OK);
                                    Log.i("JMF_getUserInfo", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    user_name_tv.setText(result.get("name").getAsString());
                                    user_level_tv.setText(result.get("grade").getAsString());

                                    LoginActivity.mUserEmail = result.get("email").getAsString();
                                    LoginActivity.mUserName = result.get("name").getAsString();
                                    PreferenceHelper.putString("user_name", LoginActivity.mUserName);

                                    UserItem userItem = new UserItem();
                                    userItem.setEmail(result.get("email").getAsString());
                                    userItem.setName(result.get("name").getAsString());
                                    userItem.setProfile(result.get("profile").getAsString());
                                    userItem.setProfileThumb(result.get("profile_thumb").getAsString());
                                    userItem.setGrade(result.get("grade").getAsString());
                                    userItem.setStatus(result.get("status").getAsInt());
                                    userItem.setRegdate(Utils.formatDate(result.get("regdate").getAsString()));
                                    LoginActivity.mUserItem = userItem;

                                    if (userItem.getProfileThumb().length() > 0) {
                                        Glide.with(AccountManageActivity.this).load(userItem.getProfileThumb()).asBitmap().centerCrop().placeholder(R.mipmap.user_none).into(new BitmapImageViewTarget(profile_iv) {
                                            @Override
                                            protected void setResource(Bitmap resource) {
                                                RoundedBitmapDrawable circularBitmapDrawable =
                                                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                                                circularBitmapDrawable.setCircular(true);
                                                profile_iv.setImageDrawable(circularBitmapDrawable);
                                            }
                                        });
                                    }
                                } else {
                                    Toast.makeText(AccountManageActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgress();
            Toast.makeText(AccountManageActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void setUserInfo(String name) {
        showProgress();
        try {
            JMF_network.modifyUserInfo(name)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF_modifyUserInfo", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    user_name_tv.setText(name);
                                    LoginActivity.mUserName = name;
                                    PreferenceHelper.putString("user_name", name);
                                    setResult(RESULT_OK);
                                } else {
                                    Toast.makeText(AccountManageActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgress();
            Toast.makeText(AccountManageActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = Utils.Progress(AccountManageActivity.this, "", "Login...", false);
            progressDialog.show();
            ;
        }
//        mLoginProgressBar.setVisibility(View.VISIBLE);
//        mLoginProgressBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//            }
//        });
    }

    public void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
//        if(mLoginProgressBar != null){
//            mLoginProgressBar.setVisibility(View.GONE);
//        }
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle(getString(R.string.account_manage_profile_modify));
        String[] pictureDialogItems = {
                getString(R.string.account_manage_profile_album),
                getString(R.string.account_manage_profile_camera)};
//                "Select photo from gallery",
//                "Capture photo from camera" };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, 0);
    }


    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == 0) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    if (contentURI != null) {
                        File file = new File(Utils.getRealPathFromURI(AccountManageActivity.this, contentURI));
                        if (file.exists()) {
                            uploadProfile(file.getPath());
                        }
                    }
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    //Toast.makeText(AccountManageActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    uploadProfile(path);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(AccountManageActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == 1) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            String path = saveImage(thumbnail);
            //Toast.makeText(AccountManageActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
            uploadProfile(path);
        }

    }

    public void uploadProfile(String path) {
        showProgress();
        try {
            List<MultipartBody.Part> parts = new ArrayList();
            File file = new File(path);

            String mime = MimeTypes.getMimeType(file);
            RequestBody requestBody;
            if (mime != null) {
                requestBody = RequestBody.create(MediaType.parse(mime), file);
            } else {
                requestBody = RequestBody.create(MediaType.parse("*/*"), file);
            }

            MultipartBody.Part part = MultipartBody.Part.createFormData("filedata", file.getName(), requestBody);
            parts.add(part);

            JMF_network.uploadProfile(parts)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    //Toast.makeText(AccountManageActivity.this, "Uplaod Success!", Toast.LENGTH_SHORT).show();
                                    getUserInfo();
                                    file.delete();
                                } else {
                                    Toast.makeText(AccountManageActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgress();
            Toast.makeText(AccountManageActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
//        File wallpaperDirectory = new File(
//                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        File wallpaperDirectory = new File(Utils.getStorageSaveFileFolderPath());

        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }
}
