package jiran.com.tmfilemanager.activity.viewer;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.InitActivity;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.common.AlertCallback;
import jiran.com.tmfilemanager.common.ClipBoard;
import jiran.com.tmfilemanager.common.FolderSelectTotalDialog;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.data.TagItem;
import jiran.com.tmfilemanager.data.TeamItem;
import jiran.com.tmfilemanager.layout.FlowLayout;
import jiran.com.tmfilemanager.network.JMF_network;
import jiran.com.tmfilemanager.network.updown.ListRefreshListener;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by user on 2017-02-06.
 */

public class ImageSlideViewerActivity extends AppCompatActivity {

    static public ArrayList<FileItem> mDataSource;
    static public int mPosition;
    static public TeamItem mTeam;
    public Toolbar toolbar;
    public Handler errorHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.obj instanceof Exception) {
                Exception e = (Exception) msg.obj;
                //Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                String message = e.getMessage();
                if (message.contains(Utils.getSDcardDirectoryPath()) && message.contains("EACCES")) {
                    Utils.notiAlert(ImageSlideViewerActivity.this, getString(R.string.error_sdcard_write_permission_denied));
                }
            }
        }
    };
    ProgressBar progress;
    //SlideImageAdapter mSlideImageAdapter;
    SlideImageFragmentAdapter mSlideImageAdapter;
    Dialog mDialog;
    boolean fileSender = false;
    Dialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager_layout);

        fileSender = getIntent().getBooleanExtra("FileSender", false);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ViewPager pager = (ViewPager) findViewById(R.id.pager);

        if (mDataSource == null) {
            Intent intent = new Intent(this, InitActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }

        mSlideImageAdapter = new SlideImageFragmentAdapter(getSupportFragmentManager(), this, mDataSource);
        pager.setAdapter(mSlideImageAdapter);
        pager.setCurrentItem(mPosition);

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mPosition = position;
                toolbar.setTitle(mDataSource.get(mPosition).getName());
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

//        TouchImageView imgDisplay = (TouchImageView) findViewById(R.id.imgDisplay);
//        progress = (ProgressBar) findViewById(R.id.progress);
//
//        String path = getIntent().getStringExtra("path");
//
//        Glide.with(this)
//                .load(new File(path))
//                .placeholder(R.color.black)
//                .listener(new RequestListener<File, GlideDrawable>() {
//                    @Override
//                    public boolean onException(Exception e, File model, Target<GlideDrawable> target, boolean isFirstResource) {
//                        progress.setVisibility(View.GONE);
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(GlideDrawable resource, File model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                        progress.setVisibility(View.GONE);
//                        return false;
//                    }
//                })
//                //.bitmapTransform(new CenterCrop(this), new RoundedCornersTransformation(this, 10, 0))
//                .into(imgDisplay);
        return;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (fileSender) {
            getMenuInflater().inflate(R.menu.none, menu);
        } else if (mDataSource.get(mPosition).getSecret()) {
            getMenuInflater().inflate(R.menu.viewer_secretmode, menu);
        } else if (mDataSource.get(mPosition).getSObjectType() != null) {
            getMenuInflater().inflate(R.menu.action_storage_mode, menu);
        } else {
            getMenuInflater().inflate(R.menu.action_viewer_mode, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_share) {
            //Toast.makeText(this, "action_vaction_shareiew_change => " + mDataSource.get(mPosition).getName(), Toast.LENGTH_SHORT).show();
            shareMethod();
            return true;
        } else if (id == R.id.action_secret) {
            //Toast.makeText(this, "action_secret => " + mDataSource.get(mPosition).getName(), Toast.LENGTH_SHORT).show();
            initSecretFolder();
            return true;
        } else if (id == R.id.action_move) {
            //Toast.makeText(this, "action_move => " + mDataSource.get(mPosition).getName(), Toast.LENGTH_SHORT).show();
            copyMethod(true);
            return true;
        } else if (id == R.id.action_copy) {
            //Toast.makeText(this, "action_copy => " + mDataSource.get(mPosition).getName(), Toast.LENGTH_SHORT).show();
            copyMethod(false);
            return true;
        } else if (id == R.id.action_favorite) {
            //Toast.makeText(this, "action_favorite => " + mDataSource.get(mPosition).getName(), Toast.LENGTH_SHORT).show();
            favoriteMethod();
            return true;
        } else if (id == R.id.action_tag) {
            //Toast.makeText(this, "action_tag => " + mDataSource.get(mPosition).getName(), Toast.LENGTH_SHORT).show();
            dialogNewTag();
            return true;
        } else if (id == R.id.action_delete) {
            //Toast.makeText(this, "action_delete => " + mDataSource.get(mPosition).getName(), Toast.LENGTH_SHORT).show();
            deleteMethod();
            return true;
        } else if (id == R.id.action_storage_share) {
            //Toast.makeText(this, "action_copy => " + mDataSource.get(mPosition).getName(), Toast.LENGTH_SHORT).show();
            shareMethod();
            return true;
        } else if (id == R.id.action_storage_copy) {
            //Toast.makeText(this, "action_favorite => " + mDataSource.get(mPosition).getName(), Toast.LENGTH_SHORT).show();
            storageCopyMethod(false);
            return true;
        } else if (id == R.id.action_storage_move) {
            //Toast.makeText(this, "action_tag => " + mDataSource.get(mPosition).getName(), Toast.LENGTH_SHORT).show();
            storageCopyMethod(true);
            return true;
        } else if (id == R.id.action_storage_delete) {
            //Toast.makeText(this, "action_delete => " + mDataSource.get(mPosition).getName(), Toast.LENGTH_SHORT).show();
            deleteMethod();
            return true;
        }

        // SecretViewer Menu Action
        else if (id == R.id.actionsecret_copy) {
            //copyMethod(false);
            selectCopyFolder(false);
        } else if (id == R.id.actionsecret_move) {
            //copyMethod(true);
            selectCopyFolder(true);
//        }else if (id == R.id.actionsecret_secret) {
//            returnToOriLocation();
//            return true;
        } else if (id == R.id.actionsecret_delete) {
            dialogDeleteSecretFile(1);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        toolbar.setTitle(mDataSource.get(mPosition).getName());
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (mDataSource.get(mPosition).getSObjectType() != null) {
            setResult(RESULT_OK);
        }
        super.onBackPressed();
    }

    public void shareMethod() {
        ArrayList<FileItem> imageList = new ArrayList<FileItem>();
        imageList.add(mDataSource.get(mPosition));
        Utils.sendFiles(this, imageList);
    }

//    public void copyMethod(boolean oriRemove) {
//
//        ArrayList<FileItem> imageList = new ArrayList<FileItem>();
//        imageList.add(mDataSource.get(mPosition));
//
//        if(oriRemove){
//            ClipBoard.cutMove(imageList);
//            Toast.makeText(ImageSlideViewerActivity.this, getResources().getString(R.string.snackbar_move), Toast.LENGTH_SHORT).show();
//        }else{
//            ClipBoard.cutCopy(imageList);
//            Toast.makeText(ImageSlideViewerActivity.this, getResources().getString(R.string.snackbar_copy), Toast.LENGTH_SHORT).show();
//        }
//    }

    public void initSecretFolder() {
        //String password = PreferenceHelper.getString(PreferenceHelper.password, "");
        String password = MainActivity.mMds.getSecretUserPassword();
        if (password != null && password.length() > 0) {
            secretMethod();
        } else {
            mDialog = new Dialog(this, R.style.Theme_TransparentBackground);
            LayoutInflater inflater = LayoutInflater.from(this);
            View view = inflater.inflate(R.layout.dialog_base, null, false);
            TextView title = (TextView) view.findViewById(R.id.title);
            title.setText(getResources().getString(R.string.left_menu_secretfolder));
            TextView content = (TextView) view.findViewById(R.id.content);
            content.setText(getResources().getString(R.string.secretfolder_activate_your_secretfolder_now));
            TextView cancel = (TextView) view.findViewById(R.id.cancel);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                }
            });
            TextView ok = (TextView) view.findViewById(R.id.ok);
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }

                    Intent intent = new Intent();
                    intent.putExtra("action", "secret");
                    setResult(RESULT_OK, intent);
                    finish();

                }
            });
            mDialog.setContentView(view);
            mDialog.show();
        }
    }

    public void secretMethod() {
        mDialog = new Dialog(this, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.dialog_secret_copy_move, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.left_menu_secretfolder));
//        TextView content = (TextView) view.findViewById(R.id.content);
//        content.setText(getResources().getString(R.string.secretfolder_do_you_want_save_select_file_in_secretfolder_1) + "1"
//                + getResources().getString(R.string.secretfolder_do_you_want_save_select_file_in_secretfolder_2));
        final CheckBox secret_copy = (CheckBox) view.findViewById(R.id.secret_copy);
        final CheckBox secret_move = (CheckBox) view.findViewById(R.id.secret_move);
        secret_copy.setEnabled(false);
        secret_copy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    secret_copy.setEnabled(false);
                    secret_move.setEnabled(true);
                    secret_move.setChecked(false);
                } else {

                }
            }
        });
        secret_move.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    secret_copy.setEnabled(true);
                    secret_move.setEnabled(false);
                    secret_copy.setChecked(false);
                } else {

                }
            }
        });

        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                File from = new File(mDataSource.get(mPosition).getPath());
//                        File directory = from.getParentFile();
//                        File to = new File(directory, item.getName().replaceFirst(".", ""));
//                        from.renameTo(to);
//                        item.setPath(directory.getPath() + File.separator + item.getName().replaceFirst(".", ""));
//                        item.setName(item.getName().replaceFirst(".", ""));
                // 시스템으로부터 현재시간(ms) 가져오기
                long now = System.currentTimeMillis();
                if (secret_move.isChecked()) {
                    Utils.moveToSecretDirectory(from, "." + now, ImageSlideViewerActivity.this);
                } else {
                    Utils.moveToSecretDirectory(from, "." + now, ImageSlideViewerActivity.this, false);
                }

                //Toast.makeText(ImageSlideViewerActivity.this, "1" + getResources().getString(R.string.secretfolder_move_to_secretfolder), Toast.LENGTH_SHORT).show();
                Toast.makeText(ImageSlideViewerActivity.this, getResources().getString(R.string.secretfolder_move_to_secretfolder), Toast.LENGTH_SHORT).show();

                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setResult(RESULT_OK);
                        finish();
                    }
                }, 1000);
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void copyMethod(final boolean oriRemove) {
        ArrayList<FileItem> imageList = new ArrayList<FileItem>();
        imageList.add(mDataSource.get(mPosition));

        int count = imageList.size();

        if (count > 0) {
            boolean checkFolder = false;
            for (FileItem item : imageList) {
                File file = new File(item.getPath());
                if (file.isDirectory()) {
                    checkFolder = true;
                }
            }
            if (oriRemove) {
                ClipBoard.cutMove(imageList);
//                Snackbar.make(viewPos, getResources().getString(R.string.snackbar_move), Snackbar.LENGTH_SHORT)
//                        .setAction("Action", null).show();
            } else {
                ClipBoard.cutCopy(imageList);
//                Snackbar.make(viewPos, getResources().getString(R.string.snackbar_copy), Snackbar.LENGTH_SHORT)
//                        .setAction("Action", null).show();
            }
            FolderSelectTotalDialog.ORI_FILE_TYPE = FolderSelectTotalDialog.LOCAL_MEMORY;
            FolderSelectTotalDialog selectDialog = new FolderSelectTotalDialog(ImageSlideViewerActivity.this, R.style.Theme_TransparentBackground, ImageSlideViewerActivity.this, !oriRemove, checkFolder, new ListRefreshListener() {
                @Override
                public void refresh(ArrayList<FileItem> items) {
//                    if(oriRemove){
//                        setResult(RESULT_OK);
//                        finish();
//                    }
                }
            });
            selectDialog.show();
        } else {
            Toast.makeText(ImageSlideViewerActivity.this, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
        }
        //finishActionMode();
    }

    public void favoriteMethod() {

        int check_count = 1;
        ArrayList<FileItem> imageList = new ArrayList<FileItem>();
        imageList.add(mDataSource.get(mPosition));

        boolean allFavorite = true;
        for (FileItem item : imageList) {
            if (!item.getFavorite()) {
                allFavorite = false;
                break;
            }
        }
        for (FileItem item : imageList) {
            if (allFavorite) {
//                if(item.getTagId() != null && item.getTagId().length() > 0){
//                    MainActivity.mMds.updateFileFavorite(item.getId(), "N");
//                }else
                {
                    MainActivity.mMds.deleteTag(item.getId());
                }
                item.setId("");
            } else {
//                if(item.getTagId() != null && item.getTagId().length() > 0){
//                    MainActivity.mMds.updateFileFavorite(item.getId(), "Y");
//                }else
                {

                }
                item.setId(MainActivity.mMds.insertFavoriteItem(item.getPath()) + "");
            }
        }
        if (allFavorite) {
            Toast.makeText(ImageSlideViewerActivity.this, check_count + getResources().getString(R.string.snackbar_file_unfavorite), Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(ImageSlideViewerActivity.this, getResources().getString(R.string.snackbar_file_favorite), Toast.LENGTH_SHORT).show();
        }
        setResult(RESULT_OK);
    }

    public void deleteMethod() {
        dialogDeleteFile(1);
    }

    public void dialogDeleteSecretFile(final int check_count) {
        mDialog = new Dialog(this, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.dialog_base, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.dialog_delete_file));
        TextView content = (TextView) view.findViewById(R.id.content);
        content.setText(getResources().getString(R.string.dialog_delete_file_info1) + check_count + getResources().getString(R.string.dialog_delete_file_info2));
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<FileItem> imageList = new ArrayList<FileItem>();
                imageList.add(mDataSource.get(mPosition));

                boolean success = true;

                for (FileItem item : imageList) {
                    File from = new File(item.getPath());

                    if (!Utils.deleteSecretFile(item.getId(), from, ImageSlideViewerActivity.this)) {
                        success = false;
                        break;
                    }
                }

                if (success) {
                    Toast.makeText(ImageSlideViewerActivity.this, check_count + getResources().getString(R.string.snackbar_file_delete), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ImageSlideViewerActivity.this, getResources().getString(R.string.snackbar_delete_error), Toast.LENGTH_SHORT).show();
                }

                if (mDialog != null) {
                    mDialog.dismiss();
                }
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setResult(RESULT_OK);
                        finish();
                    }
                }, 1000);
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void dialogDeleteFile(final int check_count) {
        mDialog = new Dialog(this, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.dialog_base, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.dialog_delete_file));
        TextView content = (TextView) view.findViewById(R.id.content);
        content.setText(getResources().getString(R.string.dialog_delete_file_info1) + check_count + getResources().getString(R.string.dialog_delete_file_info2));
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<FileItem> imageList = new ArrayList<FileItem>();
                imageList.add(mDataSource.get(mPosition));

                if (mDataSource.get(mPosition).getSObjectType() != null) {
                    deleteStorageFile(imageList);
                    return;
                }

                boolean success = true;

                for (FileItem item : imageList) {
                    File select = new File(item.getPath());
                    if (select != null && select.exists()) {
                        if (select.isDirectory()) {
                            success = deleteFile(select);
                            if (!success) {
                                break;
                            }
                            select.delete();
                        } else {
                            if (select.delete()) {
                                MainActivity.mMds.deleteFile(item.getPath());
                            } else {
                                success = false;
                                break;
                            }
                        }
                    }
                }
                if (success) {
                    Toast.makeText(ImageSlideViewerActivity.this, check_count + getResources().getString(R.string.snackbar_file_delete), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ImageSlideViewerActivity.this, getResources().getString(R.string.snackbar_delete_error), Toast.LENGTH_SHORT).show();
                }

                if (mDialog != null) {
                    mDialog.dismiss();
                }
                Handler han = new Handler();
                han.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setResult(RESULT_OK);
                        finish();
                    }
                }, 1000);
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public boolean deleteFile(File file) {
        File[] fileList = file.listFiles();
        boolean success = true;
        for (File temp : fileList) {
            if (temp.isDirectory()) {
                success = deleteFile(temp);
                if (!success) {
                    break;
                }
                temp.delete();
            } else {
                if (temp.delete()) {
                    ArrayList<FileItem> tempList = MainActivity.mMds.selectTagFiles(temp.getPath());
                    for (FileItem sub : tempList) {
                        MainActivity.mMds.deleteFile(sub.getPath());
                    }
                } else {
                    success = false;
                    break;
                }
            }
        }
        return success;
    }

//    public void dialogNewTag(){
//        mDialog = new Dialog(this, R.style.Theme_TransparentBackground);
//        LayoutInflater inflater = LayoutInflater.from(this);
//        View view = inflater.inflate(R.layout.dialog_new_tag, null, false);
//
//        final EditText edt = (EditText)view.findViewById(R.id.edt);
//        edt.requestFocus();
//        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
//
//        TextView cancel = (TextView)view.findViewById(R.id.cancel);
//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(mDialog != null){
//                    mDialog.dismiss();
//                }
//            }
//        });
//        TextView create = (TextView)view.findViewById(R.id.create);
//        create.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String tag_name = edt.getText().toString();
//                if(tag_name != null && tag_name.length() > 0){
//                    int select = (int)edt.getTag();
//                    //Toast.makeText(mMainContext, tag_name + " => " + select, Toast.LENGTH_SHORT).show();
//                    MainActivity.mMds.insertTagItem(tag_name, select + "");
//                    Intent intent = new Intent();
//                    intent.putExtra("action", "tag");
//                    setIntent(intent);
//                    setResult(RESULT_OK);
//
//                    ArrayList<TagItem> tagItams = MainActivity.mMds.getAllTag();
//                    for(TagItem item : tagItams){
//                        if(item.getName().equals(tag_name)){
//                            tagMethod(item);
//                            break;
//                        }
//                    }
//                    if(mDialog != null){
//                        mDialog.dismiss();
//                    }
//                }else{
//                    Toast.makeText(ImageSlideViewerActivity.this, getResources().getString(R.string.snackbar_input_tagname), Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//        mDialog.setContentView(view);
//        mDialog.show();
//    }

    public void dialogNewTag() {
        FileItem crrentItem = mDataSource.get(mPosition);

        mDialog = new Dialog(ImageSlideViewerActivity.this, R.style.Theme_TransparentBackground);
        final LayoutInflater inflater = LayoutInflater.from(ImageSlideViewerActivity.this);
        View view = inflater.inflate(R.layout.dialog_new_tag, null, false);

        final FlowLayout dialog_tag_content_layout = (FlowLayout) view.findViewById(R.id.dialog_tag_content_layout);
        String tagName = "";
        final ArrayList<FileItem> tag = MainActivity.mMds.selectTagFiles(crrentItem.getPath());
        for (FileItem item : tag) {
            View tag_view = inflater.inflate(R.layout.dialog_tag_list_item, null, false);
            TextView tag_name = (TextView) tag_view.findViewById(R.id.tag_name);
            tag_name.setText(item.getTagName());
            tag_view.setTag(item.getTagName());
            tag_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog_tag_content_layout.removeView(v);
                }
            });
            dialog_tag_content_layout.addView(tag_view);
        }

        final EditText edt = (EditText) view.findViewById(R.id.edt);
        edt.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        TextView tag_add_bt = (TextView) view.findViewById(R.id.tag_add_bt);
        tag_add_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String edt_name = edt.getText().toString().trim();
                if (edt_name != null && edt_name.length() > 0) {
                    int count = dialog_tag_content_layout.getChildCount();
                    boolean duplicate = false;

                    if (count >= 10) {
                        Toast.makeText(ImageSlideViewerActivity.this, "Max Count is 10...", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    for (int i = 0; i < count; i++) {
                        View childView = dialog_tag_content_layout.getChildAt(i);
                        if (((String) childView.getTag()).equals(edt_name)) {
                            Toast.makeText(ImageSlideViewerActivity.this, "Duplicate Tag...", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }

                    View tag_view = inflater.inflate(R.layout.dialog_tag_list_item, null, false);
                    TextView tag_name = (TextView) tag_view.findViewById(R.id.tag_name);
                    tag_view.setTag(edt_name);
                    tag_name.setText(edt_name);
                    tag_view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog_tag_content_layout.removeView(v);
                        }
                    });
                    dialog_tag_content_layout.addView(tag_view);
                    edt.setText("");
                } else {
                    Toast.makeText(ImageSlideViewerActivity.this, getResources().getString(R.string.snackbar_input_tagname), Toast.LENGTH_SHORT).show();
                }
            }
        });


        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView create = (TextView) view.findViewById(R.id.create);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = dialog_tag_content_layout.getChildCount();
                if (count > 0) {
//                    int select = (int)edt.getTag();
//                    //Toast.makeText(mMainContext, tag_name + " => " + select, Toast.LENGTH_SHORT).show();
//                    MainActivity.mMds.insertTagItem(tag_name, select + "");
//                    ((MainActivity)mMainContext).displayTagList();
//
//                    ArrayList<TagItem> tagItams = MainActivity.mMds.getAllTag();
//                    for(TagItem item : tagItams){
//                        if(item.getName().equals(tag_name)){
//                            tagMethod(item);
//                            break;
//                        }
//                    }

//                    ArrayList<FileItem> items = new ArrayList<FileItem>();
//                    items.add(mDataSource.get(mPosition));

                    if (crrentItem != null && crrentItem.getPath().length() > 0) {

                        for (FileItem tagItem : tag) {
                            MainActivity.mMds.deleteTag(tagItem.getId());
                        }
                        for (int i = 0; i < count; i++) {
                            View childView = dialog_tag_content_layout.getChildAt(i);
                            crrentItem.setId(MainActivity.mMds.insertTagItem((String) childView.getTag(), crrentItem.getPath()) + "");
                        }
                        crrentItem.setTagName("tag");

                        setResult(RESULT_OK);
                        Toast.makeText(ImageSlideViewerActivity.this, getResources().getString(R.string.snackbar_add_tag), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ImageSlideViewerActivity.this, getResources().getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
                    }

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                    if (mDialog != null) {
                        mDialog.dismiss();
                    }
                } else {
                    if (tag != null && tag.size() > 0) {
                        ArrayList<FileItem> items = new ArrayList<FileItem>();
                        items.add(mDataSource.get(mPosition));
                        if (crrentItem != null && crrentItem.getPath().length() > 0) {

                            for (FileItem tagItem : tag) {
                                MainActivity.mMds.deleteTag(tagItem.getId());
                            }
                            crrentItem.setTagName("");

                            setResult(RESULT_OK);
                            Toast.makeText(ImageSlideViewerActivity.this, getResources().getString(R.string.snackbar_remove_tag), Toast.LENGTH_SHORT).show();
                        }
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edt.getWindowToken(), 0);
                        if (mDialog != null) {
                            mDialog.dismiss();
                        }
                    } else {
                        Toast.makeText(ImageSlideViewerActivity.this, getResources().getString(R.string.snackbar_input_tagname), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void tagMethod(TagItem tag) {

        final View viewPos = findViewById(R.id.myCoordinatorLayout);

        ArrayList<FileItem> imageList = new ArrayList<FileItem>();
        imageList.add(mDataSource.get(mPosition));


        for (FileItem item : imageList) {
            if (tag == null) {
                MainActivity.mMds.deleteTag(item.getId());
                item.setId("");
            } else {
                MainActivity.mMds.insertTagItem(item.getName(), item.getPath());
                item.setId(tag.getId());
            }
        }
//            Snackbar.make(viewPos, "태그", Snackbar.LENGTH_SHORT)
//                    .setAction("Action", null).show();
        setResult(RESULT_OK);
    }

    public void selectCopyFolder(boolean oriRemove) {
        FolderSelectTotalDialog.ORI_FILE_TYPE = FolderSelectTotalDialog.LOCAL_MEMORY;
        FolderSelectTotalDialog selectDialog = new FolderSelectTotalDialog(ImageSlideViewerActivity.this, R.style.Theme_TransparentBackground, ImageSlideViewerActivity.this, !oriRemove, false, new AlertCallback() {
            @Override
            public void ok(Object teamObj, Object obj) {
                String path = (String) obj;
                boolean success = true;
                ArrayList<FileItem> imageList = new ArrayList<FileItem>();
                imageList.add(mDataSource.get(mPosition));

                for (FileItem item : imageList) {
                    File from = new File(item.getPath());
                    File to;// = new File(item.getOriPath());
                    if (path != null && path.length() > 0) {
                        to = new File(path + File.separator + item.getName());
                    } else {
                        to = new File(item.getOriPath());
                    }

                    if (!Utils.returnToPathDirectory(item.getId(), from, to, oriRemove, ImageSlideViewerActivity.this)) {
                        success = false;
                        break;
                    }
                }

                if (oriRemove) {
                    if (success) {
                        Toast.makeText(ImageSlideViewerActivity.this, getResources().getString(R.string.secretfolder_move_to_path_location), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(ImageSlideViewerActivity.this, getResources().getString(R.string.secretfolder_move_to_path_location_fail), Toast.LENGTH_SHORT).show();
                    }
                    Handler han = new Handler();
                    han.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setResult(RESULT_OK);
                            finish();
                        }
                    }, 1000);

                } else {
                    if (success) {
                        Toast.makeText(ImageSlideViewerActivity.this, getResources().getString(R.string.secretfolder_copy_to_path_location), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ImageSlideViewerActivity.this, getResources().getString(R.string.secretfolder_copy_to_path_location_fail), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void cancle(Object obj) {
            }
        });
        selectDialog.show();
    }

    public void storageCopyMethod(final boolean oriRemove) {

        ArrayList<FileItem> items = new ArrayList<FileItem>();
        items.add(mDataSource.get(mPosition));

        boolean checkFolder = false;
        String object_id = "";
        for (FileItem item : items) {
            if (item.getSObjectType() != null && item.getSObjectType().equals("D")) {
                checkFolder = true;
            }
            object_id += ("," + item.getId());
        }
//            FolderSelectTotalDialog.ORI_FILE_TYPE = FolderSelectTotalDialog.STORAGE_MEMORY;
//            FolderSelectTotalDialog selectDialog = new FolderSelectTotalDialog(getActivity(), R.style.Theme_TransparentBackground, getActivity(), !oriRemove, checkFolder, dialoglistener);
//            selectDialog.show();
        FolderSelectTotalDialog.ORI_FILE_TYPE = FolderSelectTotalDialog.STORAGE_MEMORY;
        FolderSelectTotalDialog.SELECT_TEAM = mTeam;
        String finalObject_id = object_id.replaceFirst(",", "");
        ;
        FolderSelectTotalDialog selectDialog = new FolderSelectTotalDialog(ImageSlideViewerActivity.this, R.style.Theme_TransparentBackground, ImageSlideViewerActivity.this, !oriRemove, checkFolder, new AlertCallback() {
            @Override
            public void ok(Object teamObj, Object obj) {
                String target_id = (String) obj;
                TeamItem target_team = (TeamItem) teamObj;
                if (oriRemove) {
                    if (target_team.getId().equals(mTeam.getId())) {
                        moveStorage(finalObject_id, target_id);
                    } else {
                        moveOtherStorage(finalObject_id, target_team.getId(), target_id);
                    }
                } else {
                    if (target_team.getId().equals(mTeam.getId())) {
                        copyStorage(finalObject_id, target_id);
                    } else {
                        copyOtherStorage(finalObject_id, target_team.getId(), target_id);
                    }
                }
                FolderSelectTotalDialog.SELECT_TEAM = null;
            }

            @Override
            public void cancle(Object obj) {
                FolderSelectTotalDialog.SELECT_TEAM = null;
            }
        });
        selectDialog.show();

        //finishActionMode();
    }

    public void copyStorage(String object_id, String target_id) {
        try {
            showProgress();
            JMF_network.copyTeamFile(mTeam.getId(), object_id, target_id)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF copyTeamFile", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    Toast.makeText(ImageSlideViewerActivity.this, getString(R.string.copysuccsess), Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent();
                                    intent.putExtra("delete", false);
                                    intent.putExtra("target_id", target_id);
                                    setResult(RESULT_OK, intent);
                                } else {
                                    try {
                                        String errorMsg = (String) gigaPodPair.second;
                                        Toast.makeText(ImageSlideViewerActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        Toast.makeText(ImageSlideViewerActivity.this, getString(R.string.copyfail), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
            Toast.makeText(ImageSlideViewerActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void moveStorage(String object_id, String target_id) {
        try {
            showProgress();
            JMF_network.moveTeamFile(mTeam.getId(), object_id, target_id)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF moveTeamFile", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    Toast.makeText(ImageSlideViewerActivity.this, getString(R.string.movesuccsess), Toast.LENGTH_SHORT).show();
                                    Handler han = new Handler();
                                    han.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            Intent intent = new Intent();
                                            intent.putExtra("delete", true);
                                            intent.putExtra("target_id", target_id);
                                            setResult(RESULT_OK, intent);
                                            finish();
                                        }
                                    }, 1000);
                                } else {
                                    try {
                                        String errorMsg = (String) gigaPodPair.second;
                                        Toast.makeText(ImageSlideViewerActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        Toast.makeText(ImageSlideViewerActivity.this, getString(R.string.movefail), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
            Toast.makeText(ImageSlideViewerActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void copyOtherStorage(String object_id, String target_team_id, String target_id) {
        try {
            showProgress();
            JMF_network.copyOtherTeamFile(mTeam.getId(), object_id, target_team_id, target_id)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF copyTeamFile", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    Toast.makeText(ImageSlideViewerActivity.this, getString(R.string.copysuccsess), Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent();
                                    intent.putExtra("delete", false);
                                    intent.putExtra("target_id", target_id);
                                    setResult(RESULT_OK, intent);
                                } else {
                                    try {
                                        String errorMsg = (String) gigaPodPair.second;
                                        Toast.makeText(ImageSlideViewerActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        Toast.makeText(ImageSlideViewerActivity.this, getString(R.string.copyfail), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
            Toast.makeText(ImageSlideViewerActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void moveOtherStorage(String object_id, String target_team_id, String target_id) {
        try {
            showProgress();
            JMF_network.moveOtherTeamFile(mTeam.getId(), object_id, target_team_id, target_id)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF moveTeamFile", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    Toast.makeText(ImageSlideViewerActivity.this, getString(R.string.movesuccsess), Toast.LENGTH_SHORT).show();
                                    Handler han = new Handler();
                                    han.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            Intent intent = new Intent();
                                            intent.putExtra("delete", true);
                                            intent.putExtra("target_id", target_id);
                                            setResult(RESULT_OK, intent);
                                            finish();
                                        }
                                    }, 1000);
                                } else {
                                    try {
                                        String errorMsg = (String) gigaPodPair.second;
                                        Toast.makeText(ImageSlideViewerActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        Toast.makeText(ImageSlideViewerActivity.this, getString(R.string.movefail), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
            Toast.makeText(ImageSlideViewerActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void deleteStorageFile(ArrayList<FileItem> checkList) {
        try {
            String delete = "";
            for (FileItem check : checkList) {
                delete += ("," + check.getId());
            }
            delete = delete.replaceFirst(",", "");
            Log.i("JMF delete", "id =>=> " + delete);
            JMF_network.deleteFile(mTeam.getId(), delete)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                if (gigaPodPair.first) {
                                    Log.i("JMF deleteFolder", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    Toast.makeText(ImageSlideViewerActivity.this, checkList.size() + getString(R.string.snackbar_file_delete), Toast.LENGTH_SHORT).show();
                                    if (mDialog != null) {
                                        mDialog.dismiss();
                                    }
                                    Handler han = new Handler();
                                    han.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            Intent intent = new Intent();
                                            intent.putExtra("delete", true);
                                            intent.putExtra("target_id", "");
                                            setResult(RESULT_OK, intent);
                                            finish();
                                        }
                                    }, 1000);
                                } else {
                                    try {
                                        String errorMsg = (String) gigaPodPair.second;
                                        Toast.makeText(ImageSlideViewerActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        Toast.makeText(ImageSlideViewerActivity.this, getString(R.string.snackbar_delete_error), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
            Toast.makeText(ImageSlideViewerActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = Utils.Progress(ImageSlideViewerActivity.this, "", "Login...", false);
            progressDialog.show();
            ;
        }
//        mLoginProgressBar.setVisibility(View.VISIBLE);
//        mLoginProgressBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//            }
//        });
    }

    public void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
//        if(mLoginProgressBar != null){
//            mLoginProgressBar.setVisibility(View.GONE);
//        }
    }
}
