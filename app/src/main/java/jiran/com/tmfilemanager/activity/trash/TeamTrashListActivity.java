package jiran.com.tmfilemanager.activity.trash;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.TeamItem;

/**
 * Created by Namo on 2018-02-28.
 */

public class TeamTrashListActivity extends AppCompatActivity {
    public static LinearLayout item_menu_recovery, item_menu_delete;
    TeamItem mTeam;
    FrameLayout mainFrame;
    //Item Menu
    LinearLayout trash_dimd_layout, top_multi_item_menu_layout, bottom_item_menu_layout, bottom_multi_item_menu_layout;
    ImageButton top_multi_item_menu_select_all, top_multi_item_menu_cancle; //, top_multi_item_menu_cancle;
    LinearLayout bottom_multi_item_menu_recovery, bottom_multi_item_menu_delete;
    Dialog mDialog;
    Dialog progressDialog;
    TextView top_multi_item_title;
    private Animation showAniBottom, goneAniBottom;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trash_list);

        if (getIntent().getSerializableExtra("TeamItem") != null) {
            mTeam = (TeamItem) getIntent().getSerializableExtra("TeamItem");
        }

        ImageButton title_left_btn = findViewById(R.id.title_left_btn);
        title_left_btn.setOnClickListener(view -> onBackPressed());

        trash_dimd_layout = (LinearLayout) findViewById(R.id.trash_dimd_layout);
        top_multi_item_menu_layout = (LinearLayout) findViewById(R.id.top_multi_item_menu_layout);
        bottom_item_menu_layout = (LinearLayout) findViewById(R.id.bottom_item_menu_layout);
        bottom_multi_item_menu_layout = (LinearLayout) findViewById(R.id.bottom_multi_item_menu_layout);

        top_multi_item_menu_select_all = findViewById(R.id.top_multi_item_menu_select_all);
//        top_multi_item_menu_cancle_all = (LinearLayout) findViewById(R.id.top_multi_item_menu_cancle_all);
        top_multi_item_menu_cancle = findViewById(R.id.top_multi_item_menu_cancle);

        top_multi_item_title = findViewById(R.id.top_multi_item_title);

        top_multi_item_menu_select_all = findViewById(R.id.top_multi_item_menu_select_all);
        top_multi_item_menu_select_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof TrashFragment) {
                    ((TrashFragment) fm).listAllcheck();
                }
            }
        });
//        top_multi_item_menu_cancle_all = (LinearLayout) findViewById(R.id.top_multi_item_menu_cancle_all);
//        top_multi_item_menu_cancle_all.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
//                if (fm instanceof TrashFragment) {
//                    ((TrashFragment) fm).listCheckClear();
//                }
//            }
//        });
        top_multi_item_menu_cancle = findViewById(R.id.top_multi_item_menu_cancle);
        top_multi_item_menu_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof TrashFragment) {
                    ((TrashFragment) fm).finishActionMode();
                }
            }
        });

        item_menu_recovery = (LinearLayout) findViewById(R.id.item_menu_recovery);
        item_menu_delete = (LinearLayout) findViewById(R.id.item_menu_delete);
        item_menu_recovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomMenu(true);
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof TrashFragment) {
                    ((TrashFragment) fm).recoveryMethod();
                }
            }
        });
        item_menu_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomMenu(true);
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof TrashFragment) {
                    ((TrashFragment) fm).deleteMethod();
                }
            }
        });

        bottom_multi_item_menu_recovery = (LinearLayout) findViewById(R.id.bottom_multi_item_menu_recovery);
        bottom_multi_item_menu_delete = (LinearLayout) findViewById(R.id.bottom_multi_item_menu_delete);
        bottom_multi_item_menu_recovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomMenu(true);
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof TrashFragment) {
                    ((TrashFragment) fm).recoveryMethod();
                }
            }
        });
        bottom_multi_item_menu_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomMenu(true);
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof TrashFragment) {
                    ((TrashFragment) fm).deleteMethod();
                }
            }
        });

        mainFrame = (FrameLayout) findViewById(R.id.content_frame);

        initFragment();
    }

    public void initFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, new TrashFragment(this, mTeam, null))
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case MainActivity.REQUEST_CODE_LEAVE_MEMBER_ACTIVITY:
                    setResult(RESULT_OK);
                    finish();
                    break;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void multiSelectMode() {
        top_multi_item_menu_layout.setVisibility(View.VISIBLE);
        bottom_multi_item_menu_layout.setVisibility(View.VISIBLE);
    }

    public void setSelectedTitle(String selected) {
        top_multi_item_title.setText(selected);
    }

    public void finishMultiSelectMode() {
        top_multi_item_title.setText("");
        top_multi_item_menu_layout.setVisibility(View.GONE);
        bottom_multi_item_menu_layout.setVisibility(View.GONE);
    }

    public void dimd_back(View v) {
        requestCurrentFragmentBackPressed();
    }

    public boolean bottomMenu(boolean check) {
        if (bottom_item_menu_layout.isShown()) {
            goneAniBottom = AnimationUtils.loadAnimation(this, R.anim.main_bottom_menu_slide_bottom);
            bottom_item_menu_layout.setAnimation(goneAniBottom);
            bottom_item_menu_layout.setVisibility(View.GONE);
            trash_dimd_layout.setVisibility(View.GONE);
            return true;
        } else {
            if (!check) {
                showAniBottom = AnimationUtils.loadAnimation(this, R.anim.main_bottom_menu_slide_top);
                bottom_item_menu_layout.setVisibility(View.VISIBLE);
                trash_dimd_layout.setVisibility(View.VISIBLE);
                bottom_item_menu_layout.setAnimation(showAniBottom);
            }
            return false;
        }
    }

    public boolean requestCurrentFragmentBackPressed() {
        if (bottomMenu(true)) {
            if (bottom_multi_item_menu_layout.isShown()) {
                return false;
            }
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof TrashFragment) {
                ((TrashFragment) fm).finishActionMode();
            }
            return false;
        }
        Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);

        if (fm instanceof TrashFragment) {
            return ((TrashFragment) fm).onBackPressed();
        } else {
            return ((TrashFragment) fm).onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        if (requestCurrentFragmentBackPressed()) {
            super.onBackPressed();
        }
    }

    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = Utils.Progress(TeamTrashListActivity.this, "", "Login...", false);
            progressDialog.show();
        }
//        mLoginProgressBar.setVisibility(View.VISIBLE);
//        mLoginProgressBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//            }
//        });
    }

    public void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
//        if(mLoginProgressBar != null){
//            mLoginProgressBar.setVisibility(View.GONE);
//        }
    }
}
