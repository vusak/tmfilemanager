package jiran.com.tmfilemanager.activity;

import android.accounts.Account;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;

import jiran.com.flyingfile.wifidirect.activity.WifiDirectActivity;
import jiran.com.inappbilling.InAppBillingActivity;
import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.common.ClipBoard;
import jiran.com.tmfilemanager.common.ListFunctionUtils;
import jiran.com.tmfilemanager.common.SortUtils;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.CurrentItem;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.data.StorageListItem;
import jiran.com.tmfilemanager.data.TagItem;
import jiran.com.tmfilemanager.data.TeamItem;
import jiran.com.tmfilemanager.data.UserItem;
import jiran.com.tmfilemanager.database.ManagerDataSource;
import jiran.com.tmfilemanager.fagment.CategoryFragment;
import jiran.com.tmfilemanager.fagment.CategoryStorageFragment;
import jiran.com.tmfilemanager.fagment.FeedbackFragment;
import jiran.com.tmfilemanager.fagment.FolderFragment;
import jiran.com.tmfilemanager.fagment.MultiFragment;
import jiran.com.tmfilemanager.fagment.SecretFolderFragment;
import jiran.com.tmfilemanager.fagment.SettingsFragment;
import jiran.com.tmfilemanager.fagment.StorageFragment;
import jiran.com.tmfilemanager.network.JMF_network;
import jiran.com.tmfilemanager.preview.IconPreview;
import jiran.com.tmfilemanager.settings.MyFileSettings;
import jiran.com.tmfilemanager.utils.PreferenceHelper;
import jiran.com.tmfilemanager.utils.Util;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String TAG = MainActivity.class.getSimpleName();
    public static final String POSITION_ARGUMENT_KEY = TAG + ":" + "PositionArgumentKey";
    public final static int REQUEST_CODE_SLIDE_VIEWER = 0;
    public final static int REQUEST_CODE_FILE_ID_SEND = 2;
    public final static int REQUEST_CODE_GOOGLE_LOGIN = 1;
    public final static int REQUEST_CODE_LOGIN_ACTIVITY = 1000;
    public final static int REQUEST_CODE_ACCOUNT_MANAGE_ACTIVITY = 1001;
    public final static int REQUEST_CODE_SIGN_UP_ACTIVITY = 1002;
    public final static int REQUEST_CODE_SEARCH_TEAM_ACTIVITY = 1003;
    public final static int REQUEST_CODE_LEAVE_MEMBER_ACTIVITY = 1004;
    public final static int REQUEST_CODE_TEAM_MANAGEMENT_ACTIVITY = 1005;
    public final static int REQUEST_CODE_FILE_SELECT_ACTIVITY = 1006;
    public final static int REQUEST_CODE_INAPP_BILLING_ACTIVITY = 1007;

    //----------------------------------------------------------------------------------------------
    // Side Appbar
    //----------------------------------------------------------------------------------------------
    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.9f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.7f;
    private static final int ALPHA_ANIMATIONS_DURATION = 200;
    public static LinearLayout sd_card;
    public static LinearLayout exit_layout;
    public static boolean mSearchMode = false;
    public static int mViewType = 0;
    public static FragmentManager mFragMenager;
    public static ManagerDataSource mMds;
    public static String path;
    public static int homeScrollX = 0;
    public static int homeScrollY = -1;
    public static LinearLayout item_menu_another_app, item_menu_copy, item_menu_move, item_menu_rename, item_menu_send_member, item_menu_secretfolder, item_menu_favorite, item_menu_e_album, item_menu_tag, item_menu_delete;
    public Toolbar toolbar;
    public ImageView toolbar_title_arrow;
    public int homeScrollPosition = 0;
    public Handler errorHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.obj instanceof Exception) {
                Exception e = (Exception) msg.obj;
                //Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                String message = e.getMessage();
                if (message.contains(Utils.getSDcardDirectoryPath()) && message.contains("EACCES")) {
                    Utils.notiAlert(MainActivity.this, getString(R.string.error_sdcard_write_permission_denied));
                }
            }
        }
    };
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    NavigationView navigationView;
    TextView toolbar_title;
    LinearLayout toolbar_title_layout;
    FrameLayout mainFrame;
    FrameLayout search_layout;
    EditText search_edit;
    Button search_edit_clear_btn;
    LinearLayout user_info_layout;
    ImageView main_profile_iv;
    TextView main_user_email_tv, main_user_name_tv, left_menu_login_str, left_menu_account_manage_str;
    TextView left_menu_inappbilling;
    LinearLayout local_memory_content_layout, team_content_layout;
    LinearLayout team_layout;

    //Item Menu
    DrawerLayout drawerLayout;
    LinearLayout main_title_dimd_layout, main_dimd_layout, top_multi_item_menu_layout, top_Storage_item_menu_layout, bottom_item_menu_layout, bottom_multi_item_menu_layout;
    //    LinearLayout top_multi_item_menu_select_all, top_multi_item_menu_cancle_all, top_multi_item_menu_cancle;
    ImageButton top_multi_item_menu_select_all, top_multi_item_menu_cancle_all, top_multi_item_menu_cancle;
    TextView top_multi_item_title;
    ImageView item_menu_favorite_iv;
    LinearLayout bottom_multi_item_menu_copy, bottom_multi_item_menu_move, bottom_multi_item_menu_delete, bottom_multi_item_menu_etc;

    // Appbar
    AppBarLayout appBarLayout;
    CollapsingToolbarLayout collapsing;
    LinearLayout userInfoLayout, drawerInfoLayout;
    Toolbar drawerToolbar;
    ImageView drawerProfileImage;
    TextView drawerNameText;

    // Banner
    FrameLayout bannerLayout;

    Dialog mDialog;

    BroadcastReceiver ejectReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (action.equals(Intent.ACTION_MEDIA_MOUNTED) || action.equals(Intent.ACTION_MEDIA_EJECT) || action.equals(Intent.ACTION_MEDIA_UNMOUNTED)) {
                Log.i("msg", "SDCard Mount Change   action => " + action);
                if (Utils.isSDcardDirectory(true)) {
                    sd_card.setVisibility(View.VISIBLE);
                    if (fm instanceof MultiFragment) {
                        ((MultiFragment) fm).setSDcardMemoryLayout(true);
                    }
//                    sd_card_memory_layout.setVisibility(View.VISIBLE);
//                    sd_card_memory_layout.setVisibility(View.VISIBLE);
//                    long sdTotal = Utils.getTotalSDCaredMemorySize();
//                    long sdFree = Utils.getSDCardMemorySize();
//                    sdcard_total_size.setText(Utils.formatSize(sdTotal));
//                    sdcard_use_size.setText(Utils.formatSize(sdTotal - sdFree));
//                    int sdcard_size = (int)((sdFree * 100) / sdTotal);
//                    sd_card_memory.setProgress(sdcard_size);
                } else {
                    sd_card.setVisibility(View.GONE);
                    if (fm instanceof MultiFragment) {
                        ((MultiFragment) fm).setSDcardMemoryLayout(false);
                    }
//                    sd_card_memory_layout.setVisibility(View.GONE);

                }
            }
        }
    };
    Dialog progressDialog;

    private FirebaseAnalytics mFirebaseAnalytics;
    private Animation showAni, goneAni;
    private Animation showAniBottom, goneAniBottom;
    private boolean titleVisible = false;
    private boolean containerVisible = true;

    public static void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        sp = PreferenceManager.getDefaultSharedPreferences(this);
        ed = sp.edit();

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorTitleBG));
        }
//        if (Build.VERSION.SDK_INT >= 21) {
//            // Set the status bar to dark-semi-transparentish
//            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
//                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        }

        setContentView(R.layout.activity_main);
        mMds = new ManagerDataSource(this);

        initLayout();
        initFunction();

        if (MainActivity.path != null && MainActivity.path.length() > 0) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, new FolderFragment(MainActivity.this, MainActivity.path, true))
                    .commit();
            MainActivity.path = null;
        } else {
            initFragment();
        }

        com.jiransoft.officedisplay.utils.PreferenceHelper.putInt(com.jiransoft.officedisplay.utils.PreferenceHelper.slide_cnt, 100);
    }

    @Override
    protected void onResume() {
        super.onResume();

        boolean exist = Util.getPackageList(this, Util.PACKAGE_TM_BROWSER);
        bannerLayout.setVisibility(exist ? View.GONE : View.VISIBLE);

        Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fm instanceof MultiFragment) {
            ((MultiFragment) fm).setUserVisibleHint(true);
        }

        IntentFilter ejectFilter = new IntentFilter(Intent.ACTION_MEDIA_MOUNTED);
        ejectFilter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        ejectFilter.addAction(Intent.ACTION_MEDIA_EJECT);
        ejectFilter.addDataScheme("file");
        registerReceiver(ejectReceiver, ejectFilter);

        new LongOperationDocument().execute("");
        new LongOperationDays().execute("");
        new LongOperationAPK().execute("");
        new LongOperationZip().execute("");
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(ejectReceiver);
    }

    @Override
    protected void onDestroy() {
        if (!PreferenceHelper.getBoolean("auto_login", false)) {
            PreferenceHelper.putString("access_token", "");
            PreferenceHelper.putString("user_name", "");
            LoginActivity.mLoginToken = "";
            LoginActivity.mUserEmail = "";
            LoginActivity.mUserName = "";
        }
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keycode, @NonNull KeyEvent event) {
        if (keycode != KeyEvent.KEYCODE_BACK)
            return false;

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
            return true;
        }
        return requestCurrentFragmentBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            setSearchMode();
            return true;
        } else if (id == R.id.action_share) {
            //Toast.makeText(this, "숨김", Toast.LENGTH_SHORT).show();
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof FolderFragment) {
                ((FolderFragment) fm).shareMethod();
            } else if (fm instanceof CategoryFragment) {
                ((CategoryFragment) fm).shareMethod();
            }
            return true;
        } else if (id == R.id.action_secret) {
            //Toast.makeText(this, "숨김", Toast.LENGTH_SHORT).show();
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof FolderFragment) {
                ((FolderFragment) fm).initSecretFolder();
            }
            return true;
        } else if (id == R.id.action_move) {
            //Toast.makeText(this, "복사", Toast.LENGTH_SHORT).show();
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof FolderFragment) {
                ((FolderFragment) fm).copyMethod(true);
            } else if (fm instanceof CategoryFragment) {
                ((CategoryFragment) fm).copyMethod(true);
            }
            return true;
        } else if (id == R.id.action_copy) {
            //Toast.makeText(this, "복사", Toast.LENGTH_SHORT).show();
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof FolderFragment) {
                ((FolderFragment) fm).copyMethod(false);
            } else if (fm instanceof CategoryFragment) {
                ((CategoryFragment) fm).copyMethod(false);
            }
            return true;
        } else if (id == R.id.action_favorite) {
            //Toast.makeText(this, "즐겨찾기", Toast.LENGTH_SHORT).show();
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof FolderFragment) {
                ((FolderFragment) fm).favoriteMethod();
            } else if (fm instanceof CategoryFragment) {
                ((CategoryFragment) fm).favoriteMethod();
            }
            return true;
        } else if (id == R.id.action_tag) {
            //Toast.makeText(this, "태그", Toast.LENGTH_SHORT).show();
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof FolderFragment) {
                ((FolderFragment) fm).dialogSelectTag();
            }
            return true;
        } else if (id == R.id.action_delete) {
            //Toast.makeText(this, "삭제", Toast.LENGTH_SHORT).show();
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof FolderFragment) {
                ((FolderFragment) fm).deleteMethod();
            } else if (fm instanceof CategoryFragment) {
                ((CategoryFragment) fm).deleteMethod();
            }
            return true;
        }
        // SecretFolder Menu Action
        else if (id == R.id.actionsecret_copy) {
            //Toast.makeText(this, "Secret Copy...", Toast.LENGTH_SHORT).show();
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof SecretFolderFragment) {
                ((SecretFolderFragment) fm).selectCopyFolder(false);
            }
            return true;
        } else if (id == R.id.actionsecret_move) {
            //Toast.makeText(this, "Secret Secret...", Toast.LENGTH_SHORT).show();
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof SecretFolderFragment) {
                ((SecretFolderFragment) fm).selectCopyFolder(true);
            }
            return true;
        } else if (id == R.id.actionsecret_delete) {
            //Toast.makeText(this, "Secret Delete...", Toast.LENGTH_SHORT).show();
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof SecretFolderFragment) {
                ((SecretFolderFragment) fm).deleteMethod();
            }
            return true;
        } else if (id == R.id.actionsecret_sort_name) {
            //Toast.makeText(this, "Secret Delete...", Toast.LENGTH_SHORT).show();
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof SecretFolderFragment) {
                ((SecretFolderFragment) fm).setSortName();
            }
            return true;
        } else if (id == R.id.actionsecret_sort_extension) {
            //Toast.makeText(this, "Secret Delete...", Toast.LENGTH_SHORT).show();
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof SecretFolderFragment) {
                ((SecretFolderFragment) fm).setSortExtension();
            }
            return true;
        } else if (id == R.id.actionsecret_sort_date) {
            //Toast.makeText(this, "Secret Delete...", Toast.LENGTH_SHORT).show();
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof SecretFolderFragment) {
                ((SecretFolderFragment) fm).setSortDate();
            }
            return true;
        } else if (id == R.id.menu_add_folder) {
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof FolderFragment) {
                ((FolderFragment) fm).dialogNewFolder();
            } else if (fm instanceof StorageFragment) {
                ((StorageFragment) fm).dialogNewFolder();
            } else if (fm instanceof CategoryFragment) {
                ((CategoryFragment) fm).dialogNewFolder();
            }
            return true;
        } else if (id == R.id.menu_upload) {
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof StorageFragment) {
                ((StorageFragment) fm).dialogUpload();
            }
            return true;
        } else if (id == R.id.menu_gridview) {
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof FolderFragment) {
                ((FolderFragment) fm).changeGridView();
            } else if (fm instanceof CategoryFragment) {
                ((CategoryFragment) fm).changeGridView();
            } else if (fm instanceof StorageFragment) {
                ((StorageFragment) fm).changeGridView();
            } else if (fm instanceof CategoryStorageFragment) {
                ((CategoryStorageFragment) fm).changeGridView();
            }
            return true;
        } else if (id == R.id.menu_listview) {
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof FolderFragment) {
                ((FolderFragment) fm).changeListView();
            } else if (fm instanceof CategoryFragment) {
                ((CategoryFragment) fm).changeListView();
            } else if (fm instanceof StorageFragment) {
                ((StorageFragment) fm).changeListView();
            } else if (fm instanceof CategoryStorageFragment) {
                ((CategoryStorageFragment) fm).changeListView();
            }
            return true;
        } else if (id == R.id.menu_sort_date) {
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof FolderFragment) {
                ((FolderFragment) fm).sortDate();
            } else if (fm instanceof CategoryFragment) {
                ((CategoryFragment) fm).sortDate();
            } else if (fm instanceof StorageFragment) {
                ((StorageFragment) fm).sortDate();
            } else if (fm instanceof CategoryStorageFragment) {
                ((CategoryStorageFragment) fm).sortDate();
            }
            return true;
        } else if (id == R.id.menu_sort_name) {
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof FolderFragment) {
                ((FolderFragment) fm).sortName();
            } else if (fm instanceof CategoryFragment) {
                ((CategoryFragment) fm).sortName();
            } else if (fm instanceof StorageFragment) {
                ((StorageFragment) fm).sortName();
            } else if (fm instanceof CategoryStorageFragment) {
                ((CategoryStorageFragment) fm).sortName();
            }
            return true;
        } else if (id == R.id.menu_sort_type) {
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof FolderFragment) {
                ((FolderFragment) fm).sortType();
            } else if (fm instanceof CategoryFragment) {
                ((CategoryFragment) fm).sortType();
            } else if (fm instanceof StorageFragment) {
                ((StorageFragment) fm).sortType();
            } else if (fm instanceof CategoryStorageFragment) {
                ((CategoryStorageFragment) fm).sortType();
            }
            return true;
        } else if (id == R.id.menu_sort_size) {
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof FolderFragment) {
                ((FolderFragment) fm).sortSize();
            } else if (fm instanceof CategoryFragment) {
                ((CategoryFragment) fm).sortSize();
            } else if (fm instanceof StorageFragment) {
                ((StorageFragment) fm).sortSize();
            } else if (fm instanceof CategoryStorageFragment) {
                ((CategoryStorageFragment) fm).sortSize();
            }
            return true;
        } else if (id == R.id.menu_display) {
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof StorageFragment) {
                ((StorageFragment) fm).actionDisplay();
            }
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawers();
//        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_FILE_ID_SEND) {
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof FolderFragment) {
                ((FolderFragment) fm).finishActionMode();
            }
        }
        if (resultCode == RESULT_OK) {
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            switch (requestCode) {
                case REQUEST_CODE_SLIDE_VIEWER:
                    if (data != null) {
                        if (fm instanceof StorageFragment) {
                            boolean delete = data.getBooleanExtra("delete", false);
                            String target_id = data.getStringExtra("target_id");
                            ((StorageFragment) fm).returnViewerlistRefresh(delete, target_id);
                            return;
                        }
                        String action = data.getStringExtra("action");
                        if (action != null && action.equals("secret")) {
                            if (fm instanceof FolderFragment) {
                                //((FolderFragment) fm).initSecretFolder();
                                Handler han = new Handler();
                                han.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        getSupportFragmentManager().beginTransaction()
                                                .replace(R.id.content_frame, new SecretFolderFragment(MainActivity.this, Utils.getSecretFolderPath(), true))
                                                .commit();
                                    }
                                }, 0);
                            }
                        } else if (action != null && action.equals("tag")) {
                            //displayTagList();
                        }
                    } else {
                        if (fm instanceof FolderFragment) {
                            ((FolderFragment) fm).listRefresh();
                        } else if (fm instanceof SecretFolderFragment) {
                            ((SecretFolderFragment) fm).listRefresh();
                        } else if (fm instanceof StorageFragment) {
                            ((StorageFragment) fm).finishActionMode();
                        }
                    }
                    break;
                case REQUEST_CODE_SIGN_UP_ACTIVITY:
                case REQUEST_CODE_ACCOUNT_MANAGE_ACTIVITY:
                case REQUEST_CODE_LOGIN_ACTIVITY:
                    if (requestCode == REQUEST_CODE_LOGIN_ACTIVITY) {
                        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                            drawerLayout.closeDrawers();
                        }
                    }
                    Handler han = new Handler();
                    han.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (LoginActivity.mLoginToken != null && LoginActivity.mLoginToken.length() > 0) {
                                MultiFragment.teamItams = null;
                                loginAction();
                            } else {
                                logoutAction();
                            }
                        }
                    }, 100);

                    break;
                case REQUEST_CODE_TEAM_MANAGEMENT_ACTIVITY:
                    if (fm instanceof MultiFragment) {
                        ((MultiFragment) fm).displayTeamList();
                    } else if (fm instanceof StorageFragment) {
                        String teamId = (((StorageFragment) fm).mTeam).getId();
                        for (int i = 0; i < MultiFragment.teamItams.size(); i++) {
                            if (teamId.equals(MultiFragment.teamItams.get(i).getId())) {
                                ((StorageFragment) fm).mTeam = MultiFragment.teamItams.get(i);
                                return;
                            }
                        }
                        Handler han1 = new Handler();
                        han1.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                initFragment();
                            }
                        }, 100);
                    }
                    break;
                case REQUEST_CODE_SEARCH_TEAM_ACTIVITY:
                    Handler han1 = new Handler();
                    han1.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            MultiFragment.teamItams = null;
                            initFragment();
                        }
                    }, 100);
                    break;
                case REQUEST_CODE_FILE_SELECT_ACTIVITY:
                    if (fm instanceof FolderFragment) {
                        ((FolderFragment) fm).listRefresh();
                    } else if (fm instanceof StorageFragment) {
                        ((StorageFragment) fm).listRefresh();
                    }
                    break;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void recentFolder(View v) {
        Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);

        if (fm instanceof MultiFragment) {
            ((MultiFragment) fm).recentFolder();
        }
    }

    public void downloadFolder(View v) {
        Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fm instanceof MultiFragment) {
            ((MultiFragment) fm).downloadFolder();
        }
    }

    public void appFolder(View v) {
        Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fm instanceof FolderFragment) {
            ((FolderFragment) fm).categoryTeamList(FolderFragment.CATEGORY_APP);
            dimd_back(null);
        } else if (fm instanceof StorageFragment) {
            ((StorageFragment) fm).categoryTeamList(StorageFragment.CATEGORY_APP);
            dimd_back(null);
        } else if (fm instanceof MultiFragment) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, new CategoryFragment(MainActivity.this, getResources().getString(R.string.category_app), true))
                    .commit();
        } else if (fm instanceof CategoryFragment) {
            if (!((CategoryFragment) fm).mCurrentPath.equals(getResources().getString(R.string.category_app))) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, new CategoryFragment(MainActivity.this, getResources().getString(R.string.category_app), true))
                        .commit();
            }
        }
    }

    public void pictureFolder(View v) {
        Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fm instanceof FolderFragment) {
            ((FolderFragment) fm).categoryTeamList(FolderFragment.CATEGORY_IMAGE);
            dimd_back(null);
        } else if (fm instanceof StorageFragment) {
            ((StorageFragment) fm).categoryTeamList(StorageFragment.CATEGORY_IMAGE);
            dimd_back(null);
        } else if (fm instanceof MultiFragment) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, new CategoryFragment(MainActivity.this, getResources().getString(R.string.category_image), true))
                    .commit();
        } else if (fm instanceof CategoryFragment) {
            if (!((CategoryFragment) fm).mCurrentPath.equals(getResources().getString(R.string.category_image))) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, new CategoryFragment(MainActivity.this, getResources().getString(R.string.category_image), true))
                        .commit();
            }
        }

    }

    public void movieFolder(View v) {
        Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fm instanceof FolderFragment) {
            ((FolderFragment) fm).categoryTeamList(FolderFragment.CATEGORY_MOVIE);
            dimd_back(null);
        } else if (fm instanceof StorageFragment) {
            ((StorageFragment) fm).categoryTeamList(StorageFragment.CATEGORY_MOVIE);
            dimd_back(null);
        } else if (fm instanceof MultiFragment) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, new CategoryFragment(MainActivity.this, getResources().getString(R.string.category_video), true))
                    .commit();
        } else if (fm instanceof CategoryFragment) {
            if (!((CategoryFragment) fm).mCurrentPath.equals(getResources().getString(R.string.category_video))) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, new CategoryFragment(MainActivity.this, getResources().getString(R.string.category_video), true))
                        .commit();
            }
        }
    }

    public void musicFolder(View v) {
        Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fm instanceof FolderFragment) {
            ((FolderFragment) fm).categoryTeamList(FolderFragment.CATEGORY_MUSIC);
            dimd_back(null);
        } else if (fm instanceof StorageFragment) {
            ((StorageFragment) fm).categoryTeamList(StorageFragment.CATEGORY_MUSIC);
            dimd_back(null);
        } else if (fm instanceof MultiFragment) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, new CategoryFragment(MainActivity.this, getResources().getString(R.string.category_music), true))
                    .commit();
        } else if (fm instanceof CategoryFragment) {
            if (!((CategoryFragment) fm).mCurrentPath.equals(getResources().getString(R.string.category_music))) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, new CategoryFragment(MainActivity.this, getResources().getString(R.string.category_music), true))
                        .commit();
            }
        }
    }

    public void documentFolder(View v) {
        Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fm instanceof FolderFragment) {
            ((FolderFragment) fm).categoryTeamList(FolderFragment.CATEGORY_DOCUMENT);
            dimd_back(null);
        } else if (fm instanceof StorageFragment) {
            ((StorageFragment) fm).categoryTeamList(StorageFragment.CATEGORY_DOCUMENT);
            dimd_back(null);
        } else if (fm instanceof MultiFragment) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, new CategoryFragment(MainActivity.this, getResources().getString(R.string.category_document), true))
                    .commit();
        } else if (fm instanceof CategoryFragment) {
            if (!((CategoryFragment) fm).mCurrentPath.equals(getResources().getString(R.string.category_document))) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, new CategoryFragment(MainActivity.this, getResources().getString(R.string.category_document), true))
                        .commit();
            }
        }
    }

    public void zipFolder(View v) {
        Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fm instanceof FolderFragment) {
            ((FolderFragment) fm).categoryTeamList(FolderFragment.CATEGORY_ZIP);
            dimd_back(null);
        } else if (fm instanceof StorageFragment) {
            ((StorageFragment) fm).categoryTeamList(StorageFragment.CATEGORY_ZIP);
            dimd_back(null);
        } else if (fm instanceof MultiFragment) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, new CategoryFragment(MainActivity.this, getResources().getString(R.string.category_zip), true))
                    .commit();
        } else if (fm instanceof CategoryFragment) {
            if (!((CategoryFragment) fm).mCurrentPath.equals(getResources().getString(R.string.category_zip))) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, new CategoryFragment(MainActivity.this, getResources().getString(R.string.category_zip), true))
                        .commit();
            }
        }
    }

    public void dimd_back(View v) {
        requestCurrentFragmentBackPressed();
    }

    public void listMenuClick(boolean folder, boolean storage, boolean favorite) {
        if (bottom_item_menu_layout.getVisibility() == View.VISIBLE) {
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof FolderFragment) {
                ((FolderFragment) fm).listCheckClear();
            } else if (fm instanceof CategoryFragment) {
                ((CategoryFragment) fm).listCheckClear();
            } else if (fm instanceof CategoryStorageFragment) {
                ((CategoryStorageFragment) fm).listCheckClear();
            } else if (fm instanceof StorageFragment) {
                ((StorageFragment) fm).listCheckClear();
            }
        }
        bottomMenuPermission(folder, storage, false, favorite);
    }

    //public Method
    public void initLayout() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        search_layout = (FrameLayout) findViewById(R.id.search_layout);
        search_edit = (EditText) findViewById(R.id.search_edit);
        search_edit_clear_btn = (Button) findViewById(R.id.search_edit_clear_btn);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        mainFrame = (FrameLayout) findViewById(R.id.content_frame);

        main_title_dimd_layout = (LinearLayout) findViewById(R.id.main_title_dimd_layout);
        main_title_dimd_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dimd_back(null);
            }
        });
        main_dimd_layout = (LinearLayout) findViewById(R.id.main_dimd_layout);
        top_multi_item_menu_layout = (LinearLayout) findViewById(R.id.top_multi_item_menu_layout);
        top_Storage_item_menu_layout = (LinearLayout) findViewById(R.id.top_Storage_item_menu_layout);
        bottom_item_menu_layout = (LinearLayout) findViewById(R.id.bottom_item_menu_layout);
        bottom_multi_item_menu_layout = (LinearLayout) findViewById(R.id.bottom_multi_item_menu_layout);

        top_multi_item_title = (TextView) findViewById(R.id.top_multi_item_title);

        top_multi_item_menu_select_all = (ImageButton) findViewById(R.id.top_multi_item_menu_select_all);
        top_multi_item_menu_select_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof FolderFragment) {
                    ((FolderFragment) fm).listAllcheck();
                } else if (fm instanceof CategoryFragment) {
                    ((CategoryFragment) fm).listAllcheck();
                } else if (fm instanceof StorageFragment) {
                    ((StorageFragment) fm).listAllcheck();
                } else if (fm instanceof CategoryStorageFragment) {
                    ((CategoryStorageFragment) fm).listAllcheck();
                }
            }
        });
//        top_multi_item_menu_cancle_all = (LinearLayout) findViewById(R.id.top_multi_item_menu_cancle_all);
//        top_multi_item_menu_cancle_all.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
//                if (fm instanceof FolderFragment) {
//                    ((FolderFragment) fm).listCheckClear();
//                } else if (fm instanceof CategoryFragment) {
//                    ((CategoryFragment) fm).listCheckClear();
//                } else if (fm instanceof StorageFragment) {
//                    ((StorageFragment) fm).listCheckClear();
//                }
//            }
//        });
        top_multi_item_menu_cancle = (ImageButton) findViewById(R.id.top_multi_item_menu_cancle);
        top_multi_item_menu_cancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof FolderFragment) {
                    ((FolderFragment) fm).finishActionMode();
                } else if (fm instanceof CategoryFragment) {
                    ((CategoryFragment) fm).finishActionMode();
                } else if (fm instanceof StorageFragment) {
                    ((StorageFragment) fm).finishActionMode();
                } else if (fm instanceof CategoryStorageFragment) {
                    ((CategoryStorageFragment) fm).finishActionMode();
                }
            }
        });

        bottom_multi_item_menu_copy = (LinearLayout) findViewById(R.id.bottom_multi_item_menu_copy);
        bottom_multi_item_menu_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof FolderFragment) {
                    ((FolderFragment) fm).copyMethod(false);
                } else if (fm instanceof CategoryFragment) {
                    ((CategoryFragment) fm).copyMethod(false);
                } else if (fm instanceof StorageFragment) {
                    ((StorageFragment) fm).copyMethod(false);
                } else if (fm instanceof CategoryStorageFragment) {
                    ((CategoryStorageFragment) fm).copyMethod(false);
                }
            }
        });
        bottom_multi_item_menu_move = (LinearLayout) findViewById(R.id.bottom_multi_item_menu_move);
        bottom_multi_item_menu_move.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof FolderFragment) {
                    ((FolderFragment) fm).copyMethod(true);
                } else if (fm instanceof CategoryFragment) {
                    ((CategoryFragment) fm).copyMethod(true);
                } else if (fm instanceof StorageFragment) {
                    ((StorageFragment) fm).copyMethod(true);
                } else if (fm instanceof CategoryStorageFragment) {
                    ((CategoryStorageFragment) fm).copyMethod(true);
                }
            }
        });
        bottom_multi_item_menu_delete = (LinearLayout) findViewById(R.id.bottom_multi_item_menu_delete);
        bottom_multi_item_menu_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof FolderFragment) {
                    ((FolderFragment) fm).deleteMethod();
                } else if (fm instanceof CategoryFragment) {
                    ((CategoryFragment) fm).deleteMethod();
                } else if (fm instanceof StorageFragment) {
                    ((StorageFragment) fm).deleteMethod();
                } else if (fm instanceof CategoryStorageFragment) {
                    ((CategoryStorageFragment) fm).deleteMethod();
                }
            }
        });
        bottom_multi_item_menu_etc = (LinearLayout) findViewById(R.id.bottom_multi_item_menu_etc);
        bottom_multi_item_menu_etc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<FileItem> checkList = null;
                boolean folder = false;
                boolean storage = false;
                boolean multi = false;

                boolean allFavorite = true;


                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof FolderFragment) {
                    checkList = ((FolderFragment) fm).getCheckList();
                    for (FileItem item : checkList) {
                        File select = new File(item.getPath());
                        if (select != null && select.exists()) {
                            if (select.isDirectory()) {
                                folder = true;
                            }
                            if (!item.getFavorite()) {
                                allFavorite = false;
                            }
                        }
                    }
                } else if (fm instanceof CategoryFragment) {
                    checkList = ((CategoryFragment) fm).getCheckList();
                    for (FileItem item : checkList) {
                        File select = new File(item.getPath());
                        if (select != null && select.exists()) {
                            if (select.isDirectory()) {
                                folder = true;
                            }
                            if (!item.getFavorite()) {
                                allFavorite = false;
                            }
                        }
                    }
                } else if (fm instanceof StorageFragment) {
                    checkList = ((StorageFragment) fm).getCheckList();
                    for (FileItem item : checkList) {
                        if (item.getSObjectType() != null && item.getSObjectType().equals("D")) {
                            folder = true;
                            break;
                        }
                    }
                    storage = true;
                }
                multi = checkList.size() > 1 ? true : false;
                bottomMenuPermission(folder, storage, multi, allFavorite);
            }
        });


        item_menu_another_app = (LinearLayout) findViewById(R.id.item_menu_another_app);
        item_menu_another_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomMenu(true);
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof FolderFragment) {
                    ((FolderFragment) fm).shareMethod();
                } else if (fm instanceof CategoryFragment) {
                    ((CategoryFragment) fm).shareMethod();
                } else if (fm instanceof StorageFragment) {
                    ((StorageFragment) fm).shareMethod();
                } else if (fm instanceof CategoryStorageFragment) {
                    ((CategoryStorageFragment) fm).shareMethod();
                }
            }
        });
        item_menu_copy = (LinearLayout) findViewById(R.id.item_menu_copy);
        item_menu_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomMenu(true);
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof FolderFragment) {
                    ((FolderFragment) fm).copyMethod(false);
                } else if (fm instanceof CategoryFragment) {
                    ((CategoryFragment) fm).copyMethod(false);
                } else if (fm instanceof StorageFragment) {
                    ((StorageFragment) fm).copyMethod(false);
                } else if (fm instanceof CategoryStorageFragment) {
                    ((CategoryStorageFragment) fm).copyMethod(false);
                }
            }
        });
        item_menu_move = (LinearLayout) findViewById(R.id.item_menu_move);
        item_menu_move.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomMenu(true);
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof FolderFragment) {
                    ((FolderFragment) fm).copyMethod(true);
                } else if (fm instanceof CategoryFragment) {
                    ((CategoryFragment) fm).copyMethod(true);
                } else if (fm instanceof StorageFragment) {
                    ((StorageFragment) fm).copyMethod(true);
                } else if (fm instanceof CategoryStorageFragment) {
                    ((CategoryStorageFragment) fm).copyMethod(true);
                }
            }
        });
        item_menu_tag = (LinearLayout) findViewById(R.id.item_menu_tag);
        item_menu_tag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomMenu(true);
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof FolderFragment) {
                    ((FolderFragment) fm).dialogSelectTag();
                } else if (fm instanceof CategoryFragment) {
                    ((CategoryFragment) fm).dialogSelectTag();
                } else if (fm instanceof StorageFragment) {
                    ((StorageFragment) fm).dialogSelectTag();
                }
            }
        });
        item_menu_favorite = (LinearLayout) findViewById(R.id.item_menu_favorite);
        item_menu_favorite_iv = (ImageView) findViewById(R.id.item_menu_favorite_iv);
        item_menu_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomMenu(true);
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof FolderFragment) {
                    ((FolderFragment) fm).favoriteMethod();
                } else if (fm instanceof CategoryFragment) {
                    ((CategoryFragment) fm).favoriteMethod();
                } else if (fm instanceof StorageFragment) {
                    ((StorageFragment) fm).favoriteMethod();
                } else if (fm instanceof CategoryStorageFragment) {
                    ((CategoryStorageFragment) fm).favoriteMethod();
                }
            }
        });
        item_menu_rename = (LinearLayout) findViewById(R.id.item_menu_rename);
        item_menu_rename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomMenu(true);
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof FolderFragment) {
                    ((FolderFragment) fm).renameMethod();
                } else if (fm instanceof CategoryFragment) {
                    ((CategoryFragment) fm).renameMethod();
                } else if (fm instanceof StorageFragment) {
                    ((StorageFragment) fm).renameMethod();
                } else if (fm instanceof CategoryStorageFragment) {
                    ((CategoryStorageFragment) fm).renameMethod();
                }
            }
        });
        item_menu_delete = (LinearLayout) findViewById(R.id.item_menu_delete);
        item_menu_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomMenu(true);
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof FolderFragment) {
                    ((FolderFragment) fm).deleteMethod();
                } else if (fm instanceof CategoryFragment) {
                    ((CategoryFragment) fm).deleteMethod();
                } else if (fm instanceof StorageFragment) {
                    ((StorageFragment) fm).deleteMethod();
                } else if (fm instanceof CategoryStorageFragment) {
                    ((CategoryStorageFragment) fm).deleteMethod();
                }
            }
        });
        item_menu_send_member = (LinearLayout) findViewById(R.id.item_menu_send_member);
        item_menu_send_member.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomMenu(true);
                Toast.makeText(MainActivity.this, "준비중...", Toast.LENGTH_SHORT).show();
            }
        });
        item_menu_secretfolder = (LinearLayout) findViewById(R.id.item_menu_secretfolder);
        item_menu_secretfolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomMenu(true);
//                Toast.makeText(MainActivity.this, "준비중...", Toast.LENGTH_SHORT).show();
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof FolderFragment) {
                    ((FolderFragment) fm).initSecretFolder();
                } else if (fm instanceof CategoryFragment) {
                    ((CategoryFragment) fm).initSecretFolder();
                }
            }
        });
        item_menu_e_album = (LinearLayout) findViewById(R.id.item_menu_e_album);
        item_menu_e_album.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomMenu(true);
                Toast.makeText(MainActivity.this, "준비중...", Toast.LENGTH_SHORT).show();
            }
        });

        mFragMenager = getSupportFragmentManager();

        exit_layout = (LinearLayout) findViewById(R.id.exit_layout);

        exit_layout.setOnClickListener(null);
        ImageButton exit_btn = (ImageButton) findViewById(R.id.exit_btn);
        exit_btn.setOnClickListener(v -> {
            ClipBoard.clear();
            finish();
        });
        ImageButton cancel_btn = (ImageButton) findViewById(R.id.cancel_btn);
        cancel_btn.setOnClickListener(v -> {
            exit_layout.setVisibility(View.GONE);
            navigationViewAction(true);
        });
    }

    public void navigationViewAction(boolean flag) {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (flag) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        } else {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    public void setTitle(String title) {
        setTitle(title, false);
    }

    public void setTitle(String title, boolean top_menu) {
        toolbar_title.setText(title);
        if (top_menu) {
            toolbar_title_arrow.setVisibility(View.VISIBLE);
        } else {
            toolbar_title_arrow.setVisibility(View.GONE);
        }
    }

    public void setSelectedTitle(String selected) {
        top_multi_item_title.setText(selected);
    }

    public void initFunction() {
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar_title_layout = (LinearLayout) toolbar.findViewById(R.id.toolbar_title_layout);
        toolbar_title = (TextView) toolbar.findViewById(R.id.toolbar_title);
        toolbar_title_arrow = (ImageView) toolbar.findViewById(R.id.toolbar_title_arrow);
        toolbar_title_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = toolbar_title.getText().toString();
                if (toolbar_title_arrow.isShown()) {
                    topMenu(false);
                }
            }
        });
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);


        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();


        navigationView.setNavigationItemSelectedListener(this);
        navigationView.addView(setCustomNavigationView(R.layout.custom_navigation_view));
        new IconPreview(this);

        search_edit.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                // Your piece of code on keyboard search click
                String search = search_edit.getText().toString();
                if (search != null && search.length() > 0) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(search_edit.getWindowToken(), 0);

                    Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);

                    if (fm instanceof FolderFragment) {
                        String path = Utils.getInternalDirectoryPath();
                        String startPath = Utils.getInternalDirectoryPath();

                        if (((FolderFragment) fm).mListType != 1 && ((FolderFragment) fm).mListType != 2) {
                            path = ((FolderFragment) fm).mCurrentPath;
                            startPath = ((FolderFragment) fm).mStartPath;
                        }
                        search_edit.setText("");
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.content_frame, new FolderFragment(MainActivity.this, path, startPath, search))
                                .commit();
                    } else if (fm instanceof StorageFragment) {
                        search_edit.setText("");
                        ((StorageFragment) fm).getSearchStorageList(search);
                    }
                } else {
                }
                return true;
            }
            return false;
        });

        search_edit_clear_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (search_edit.getText().toString().length() > 0) {
                    search_edit.setText("");
                } else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(search_edit.getWindowToken(), 0);
                    finishSearchMode();
                }
            }
        });
    }

    public void initFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, new MultiFragment(this))
                .commit();

        // premium inapp
//        dialogSubscriptionFinish();
    }

    public void dialogSubscriptionFinish() {
        mDialog = new Dialog(MainActivity.this, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
        final View view = inflater.inflate(R.layout.dialog_subscription_finish, null, false);
        TextView dialog_finish_tv = (TextView) view.findViewById(R.id.dialog_finish_tv);
        dialog_finish_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView delete_day_tv = (TextView) view.findViewById(R.id.delete_day_tv);
        TextView delete_team_tv = (TextView) view.findViewById(R.id.delete_team_tv);
        TextView subscription_retry_tv = (TextView) view.findViewById(R.id.subscription_retry_tv);
        delete_day_tv.setText("2018년 07월 15일 24:00");
        delete_team_tv.setText("jiran\n" + "Hungree\n" + "323443");
        subscription_retry_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                Intent intent = new Intent(MainActivity.this, InAppBillingActivity.class);
                startActivityForResult(intent, MainActivity.REQUEST_CODE_INAPP_BILLING_ACTIVITY);
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void dialogSelectAccount(final boolean backNoAction, Account[] accounts) {
        mDialog = new Dialog(this, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(this);
        final View view = inflater.inflate(R.layout.dialog_secret_account_select, null, false);
        final RadioGroup dialog_account_list_layout = (RadioGroup) view.findViewById(R.id.dialog_account_list_layout);

        // device 계정 정보 가져오기
        //Account[] accounts =  AccountManager.get(mMainContext).getAccounts();
        for (final Account account : accounts) {
            RadioButton radioButtonAndroid = (RadioButton) inflater.inflate(R.layout.custom_radiobutton, null, false);
            radioButtonAndroid.setText(account.name);
            dialog_account_list_layout.addView(radioButtonAndroid);
        }

        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        mDialog.setContentView(view);
        mDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        mDialog.show();
    }

    public View setCustomNavigationView(int resourceId) {
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(resourceId, null, false);

        appBarLayout = view.findViewById(R.id.appbar_layout);
        appBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            int max = appBarLayout.getTotalScrollRange();
            float percentage = (float) Math.abs(verticalOffset) / (float) max;

            handleAlphaOnTitle(percentage);
            handleToolbarTitleVisibility(percentage);
        });

        collapsing = (CollapsingToolbarLayout) view.findViewById(R.id.collapsing_toolbar_android_layout);
        drawerToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        userInfoLayout = (LinearLayout) view.findViewById(R.id.layout_user_info);
        startAlphaAnimation(drawerToolbar, 0, View.INVISIBLE);
        drawerInfoLayout = (LinearLayout) view.findViewById(R.id.layout_user_info_semi);
        drawerProfileImage = (ImageView) view.findViewById(R.id.image_user_profile);
        drawerProfileImage.setOnClickListener(v -> {
//            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers();

                Intent intent = new Intent(MainActivity.this, AccountManageActivity.class);
                startActivityForResult(intent, MainActivity.REQUEST_CODE_ACCOUNT_MANAGE_ACTIVITY);
            }
        });
        drawerNameText = (TextView) view.findViewById(R.id.text_user_name);

        user_info_layout = (LinearLayout) view.findViewById(R.id.user_info_layout);
        main_profile_iv = (ImageView) view.findViewById(R.id.main_profile_iv);
        main_profile_iv.setOnClickListener(v -> {
//            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawers();

                Intent intent = new Intent(MainActivity.this, AccountManageActivity.class);
                startActivityForResult(intent, MainActivity.REQUEST_CODE_ACCOUNT_MANAGE_ACTIVITY);
            }
        });
        main_user_email_tv = (TextView) view.findViewById(R.id.main_user_email_tv);
        main_user_name_tv = (TextView) view.findViewById(R.id.main_user_name_tv);

        bannerLayout = (FrameLayout) view.findViewById(R.id.layout_banner_tmbr);
        bannerLayout.setOnClickListener(v -> Util.launchApp(MainActivity.this, Util.PACKAGE_TM_BROWSER));

        LinearLayout left_menu_account_manage_btn = (LinearLayout) view.findViewById(R.id.left_menu_account_manage_btn);
        left_menu_account_manage_str = (TextView) view.findViewById(R.id.left_menu_account_manage_str);
        left_menu_account_manage_btn.setOnClickListener(view1 -> {
            if (LoginActivity.mLoginToken != null && LoginActivity.mLoginToken.length() > 0) {
//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();

                    Intent intent = new Intent(MainActivity.this, AccountManageActivity.class);
                    startActivityForResult(intent, MainActivity.REQUEST_CODE_ACCOUNT_MANAGE_ACTIVITY);
                }
            } else {
                Intent intent = new Intent(MainActivity.this, SignUpActivity.class);
                startActivityForResult(intent, MainActivity.REQUEST_CODE_LOGIN_ACTIVITY);
            }
        });
        LinearLayout left_menu_login_btn = (LinearLayout) view.findViewById(R.id.left_menu_login_btn);
        left_menu_login_str = (TextView) view.findViewById(R.id.left_menu_login_str);

        //left_menu_login_str.setText(getResources().getString(R.string.left_menu_top_logout));
        left_menu_login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LoginActivity.mLoginToken != null && LoginActivity.mLoginToken.length() > 0) {
                    mDialog = new Dialog(MainActivity.this, R.style.Theme_TransparentBackground);
                    LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                    View view = inflater.inflate(R.layout.dialog_base, null, false);
                    TextView title = (TextView) view.findViewById(R.id.title);
                    title.setText(getResources().getString(R.string.left_menu_top_logout));
                    TextView content = (TextView) view.findViewById(R.id.content);
                    content.setText(getResources().getString(R.string.login_logout_action));
                    TextView cancel = (TextView) view.findViewById(R.id.cancel);
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mDialog != null) {
                                mDialog.dismiss();
                            }
                        }
                    });
                    TextView ok = (TextView) view.findViewById(R.id.ok);
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mDialog != null) {
                                mDialog.dismiss();
                            }
                            logoutAction();
                        }
                    });
                    mDialog.setContentView(view);
                    mDialog.show();
                } else {
//                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//                    if (drawer.isDrawerOpen(GravityCompat.START)) {
//                        drawer.closeDrawer(GravityCompat.START);
//
//                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
//                        startActivityForResult(intent, MainActivity.REQUEST_CODE_LOGIN_ACTIVITY);
//                    }
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivityForResult(intent, MainActivity.REQUEST_CODE_LOGIN_ACTIVITY);
                }
            }
        });

        left_menu_inappbilling = (TextView) view.findViewById(R.id.left_menu_inappbilling);
        left_menu_inappbilling.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, InAppBillingActivity.class);
            startActivityForResult(intent, MainActivity.REQUEST_CODE_INAPP_BILLING_ACTIVITY);
        });

        LinearLayout home = (LinearLayout) view.findViewById(R.id.home);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();

                    Handler han = new Handler();
                    han.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, new MultiFragment(MainActivity.this))
                                    .commit();
                        }
                    }, 300);
                }
            }
        });

        LinearLayout storage = (LinearLayout) view.findViewById(R.id.storage);
        storage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();

                    Handler han = new Handler();
                    han.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                            if (fm instanceof FolderFragment && ((FolderFragment) fm).mCurrentPath.contains(Utils.getInternalDirectoryPath())) {
                                //동작하지않음.
                            } else {
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.content_frame, new FolderFragment(MainActivity.this, Utils.getInternalDirectoryPath(), true))
                                        .commit();
                            }
                        }
                    }, 300);
                }
            }
        });

        sd_card = (LinearLayout) view.findViewById(R.id.sd_card);
        if (Utils.isSDcardDirectory(true)) {
            sd_card.setVisibility(View.VISIBLE);
            sd_card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                        drawerLayout.closeDrawers();

                        Handler han = new Handler();
                        han.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                                if (fm instanceof FolderFragment && ((FolderFragment) fm).mCurrentPath.contains(Utils.getSDcardDirectoryPath())) {
                                    //동작하지않음.
                                } else {
                                    getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.content_frame, new FolderFragment(MainActivity.this, Utils.getSDcardDirectoryPath(), true))
                                            .commit();
                                }
                            }
                        }, 300);

                    }
                }
            });
        } else {
            sd_card.setVisibility(View.GONE);
        }

//        local_memory_content_layout = (LinearLayout) view.findViewById(R.id.local_memory_content_layout);
//        RelativeLayout local_memory_layout = (RelativeLayout) view.findViewById(R.id.local_memory_layout);
//        final ImageView local_memory_arrow = (ImageView) view.findViewById(R.id.local_memory_arrow);
//        local_memory_layout.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                if(local_memory_content_layout.getVisibility() == View.VISIBLE){
//                    local_memory_arrow.setImageResource(R.mipmap.list_dropdown);
//
//                    ExpandCollapseAnimation animation = null;
//                    animation = new ExpandCollapseAnimation(local_memory_content_layout, 200, 1, MainActivity.this);
//                    local_memory_content_layout.startAnimation(animation);
//                }else{
//                    local_memory_arrow.setImageResource(R.mipmap.list_dropup);
//                    ExpandCollapseAnimation animation = null;
//                    animation = new ExpandCollapseAnimation(local_memory_content_layout, 200, 0, MainActivity.this);
//                    local_memory_content_layout.startAnimation(animation);
//                }
//            }
//        });

        //displayTagList();
        team_content_layout = (LinearLayout) view.findViewById(R.id.team_content_layout);
        team_layout = (LinearLayout) view.findViewById(R.id.team_layout);

//        final ImageView team_arrow = (ImageView) view.findViewById(R.id.team_arrow);
//        team_layout.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                if(team_content_layout.getVisibility() == View.VISIBLE){
//                    team_arrow.setImageResource(R.mipmap.list_dropdown);
//                    ExpandCollapseAnimation animation = null;
//                    animation = new ExpandCollapseAnimation(team_content_layout, 200, 1, MainActivity.this);
//                    team_content_layout.startAnimation(animation);
//                }else{
//                    team_arrow.setImageResource(R.mipmap.list_dropup);
//                    ExpandCollapseAnimation animation = null;
//                    animation = new ExpandCollapseAnimation(team_content_layout, 200, 0, MainActivity.this);
//                    team_content_layout.startAnimation(animation);
//                }
//            }
//        });


        ImageView left_menu_app_category_btn = (ImageView) view.findViewById(R.id.left_menu_app_category_btn);
        left_menu_app_category_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                    Handler han = new Handler();
                    han.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, new CategoryFragment(MainActivity.this, getResources().getString(R.string.category_app), true))
                                    .commit();
                        }
                    }, 300);
                }
            }
        });
        ImageView left_menu_image_category_btn = (ImageView) view.findViewById(R.id.left_menu_image_category_btn);
        left_menu_image_category_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                    Handler han = new Handler();
                    han.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, new CategoryFragment(MainActivity.this, getResources().getString(R.string.category_image), true))
                                    .commit();
                        }
                    }, 300);
                }
            }
        });
        ImageView left_menu_movie_category_btn = (ImageView) view.findViewById(R.id.left_menu_movie_category_btn);
        left_menu_movie_category_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                    Handler han = new Handler();
                    han.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, new CategoryFragment(MainActivity.this, getResources().getString(R.string.category_video), true))
                                    .commit();
                        }
                    }, 300);
                }
            }
        });
        ImageView left_menu_music_category_btn = (ImageView) view.findViewById(R.id.left_menu_music_category_btn);
        left_menu_music_category_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                    Handler han = new Handler();
                    han.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, new CategoryFragment(MainActivity.this, getResources().getString(R.string.category_music), true))
                                    .commit();
                        }
                    }, 300);
                }
            }
        });
        ImageView left_menu_document_category_btn = (ImageView) view.findViewById(R.id.left_menu_document_category_btn);
        left_menu_document_category_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();

                    Handler han = new Handler();
                    han.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, new CategoryFragment(MainActivity.this, getResources().getString(R.string.category_document), true))
                                    .commit();
                        }
                    }, 300);
                }
            }
        });
        ImageView left_menu_zip_category_btn = (ImageView) view.findViewById(R.id.left_menu_zip_category_btn);
        left_menu_zip_category_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();

                    Handler han = new Handler();
                    han.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, new CategoryFragment(MainActivity.this, getResources().getString(R.string.category_zip), true))
                                    .commit();
                        }
                    }, 300);
                }
            }
        });

        LinearLayout file_receive_layout = (LinearLayout) view.findViewById(R.id.file_receive_layout);
        file_receive_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                    Toast.makeText(MainActivity.this, "개발중...", Toast.LENGTH_SHORT).show();

//                    Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
//                    if(fm instanceof FeedbackFragment) {
//                        //동작하지않음.
//                    }else{
//                        getSupportFragmentManager().beginTransaction()
//                                .replace(R.id.content_frame, new FeedbackFragment(MainActivity.this))
//                                .commit();
//                    }
                }
            }
        });
        LinearLayout secretfolder_layout = (LinearLayout) view.findViewById(R.id.secretfolder_layout);
        secretfolder_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();

//                    Toast.makeText(MainActivity.this, "준비중...", Toast.LENGTH_SHORT).show();
                    Handler han = new Handler();
                    han.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                            if (fm instanceof SecretFolderFragment) {
                                //동작하지않음.
                            } else {
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.content_frame, new SecretFolderFragment(MainActivity.this, Utils.getSecretFolderPath(), true))
                                        .commit();
                            }
                        }
                    }, 300);
                }
            }
        });
        LinearLayout e_album_layout = (LinearLayout) view.findViewById(R.id.e_album_layout);
        e_album_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                    Toast.makeText(MainActivity.this, "개발중...", Toast.LENGTH_SHORT).show();
//                    Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
//                    if(fm instanceof SettingsFragment) {
//                        //동작하지않음.
//                    }else{
//                        getSupportFragmentManager().beginTransaction()
//                                .replace(R.id.content_frame, new SettingsFragment(MainActivity.this))
//                                .commit();
//                    }
                }
            }
        });

        LinearLayout favorite_layout = (LinearLayout) view.findViewById(R.id.favorite_layout);
        favorite_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();

                    Handler han = new Handler();
                    han.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, new FolderFragment(MainActivity.this, 2, null))
                                    .commit();
                        }
                    }, 300);
                }
            }
        });
        LinearLayout wifi_direct_layout = (LinearLayout) view.findViewById(R.id.wifi_direct_layout);
        wifi_direct_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();
                    //와이파이 다이렉트 기능 시작.
                    Intent intent = new Intent(MainActivity.this, WifiDirectActivity.class);
                    startActivity(intent);
                }
            }
        });
        LinearLayout tag_layout = (LinearLayout) view.findViewById(R.id.tag_layout);
        tag_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();

                    Handler han = new Handler();
                    han.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                            if (fm instanceof CategoryFragment && ((CategoryFragment) fm).mListType == 1) {
                                //동작하지않음.
                            } else {
                                ArrayList<TagItem> tagItams = MainActivity.mMds.getAllTag();
                                if (tagItams != null && tagItams.size() > 0) {
                                    getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.content_frame, new CategoryFragment(MainActivity.this, 1, null))
                                            .commit();
                                } else {
                                    Toast.makeText(MainActivity.this, getString(R.string.tag_empty_title), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }, 300);
                }
            }
        });
        LinearLayout setting_layout = (LinearLayout) view.findViewById(R.id.setting_layout);
        setting_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                //TEST
//                // 1. get Shared Preference
//                SharedPreferences sharedPreference
//                        = getSharedPreferences("MobilePOD.Shared", Context.MODE_MULTI_PROCESS | Context.MODE_WORLD_READABLE);
//                // 2. get Editor
//                SharedPreferences.Editor editor = sharedPreference.edit();
//                // 3. set Key values
//                editor.putString("loginEmail", "jiran");
//                // 4. commit the values
//                editor.commit();
//
//                ComponentName cn = new ComponentName("jiran.com.gatepod","jiran.com.gatepod.activity.IntroActivity");
//                //ex) ("com.android.music" ,"com.android.music.MusicBrowserActivity")
//                Intent intent = new Intent(Intent.ACTION_MAIN);
//                intent.addCategory(Intent.CATEGORY_LAUNCHER);
//                intent.setComponent(cn);
//                startActivity(intent);
//                //TEST
//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawers();

                    Handler han = new Handler();
                    han.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                            if (fm instanceof SettingsFragment) {
                                //동작하지않음.
                            } else {
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.content_frame, new SettingsFragment(MainActivity.this))
                                        .commit();
                            }
                        }
                    }, 300);
                }
            }
        });

        setLoginStatus();
        return view;
    }

    public void displayTeamList() {
        LayoutInflater inflater = LayoutInflater.from(this);
        team_content_layout.removeAllViews();
        top_Storage_item_menu_layout.removeAllViews();

        View top_menu_view0 = inflater.inflate(R.layout.list_top_menu_item, null, false);
        TextView team_name_tv0 = (TextView) top_menu_view0.findViewById(R.id.team_name_tv);
        team_name_tv0.setText(getString(R.string.internaldirectory));
        top_menu_view0.setTag(getString(R.string.internaldirectory));
        top_menu_view0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                topMenu(true);
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof FolderFragment && ((FolderFragment) fm).mCurrentPath.contains(Utils.getInternalDirectoryPath())) {
                    //동작하지않음.
                } else {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_frame, new FolderFragment(MainActivity.this, Utils.getInternalDirectoryPath(), true))
                            .commit();
                }
            }
        });
        top_Storage_item_menu_layout.addView(top_menu_view0);

        if (Utils.isSDcardDirectory(true)) {
            View top_menu_view1 = inflater.inflate(R.layout.list_top_menu_item, null, false);
            TextView team_name_tv1 = (TextView) top_menu_view1.findViewById(R.id.team_name_tv);
            team_name_tv1.setText(getString(R.string.sdcard));
            top_menu_view1.setTag(getString(R.string.sdcard));
            top_menu_view1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    topMenu(true);
                    Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                    if (fm instanceof FolderFragment && ((FolderFragment) fm).mCurrentPath.contains(Utils.getSDcardDirectoryPath())) {
                        //동작하지않음.
                    } else {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.content_frame, new FolderFragment(MainActivity.this, Utils.getSDcardDirectoryPath(), true))
                                .commit();
                    }
                }
            });
            top_Storage_item_menu_layout.addView(top_menu_view1);
        }


        if (MultiFragment.teamItams != null && MultiFragment.teamItams.size() > 0) {
            team_layout.setVisibility(View.VISIBLE);
            int i = 0;
            for (final TeamItem item : MultiFragment.teamItams) {
                final int pos = i;
                View team_view = inflater.inflate(R.layout.custom_navigation_tagitem_layout, null, false);
                TextView team_name = (TextView) team_view.findViewById(R.id.team_name);
                team_name.setText(item.getTeamName());
                team_view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                            drawerLayout.closeDrawers();

                            moveToStorageRoot(pos);
                        }
                    }
                });
                team_content_layout.addView(team_view);


                View top_menu_view = inflater.inflate(R.layout.list_top_menu_item, null, false);
                TextView team_name_tv = (TextView) top_menu_view.findViewById(R.id.team_name_tv);
                team_name_tv.setText(item.getTeamName());
                top_menu_view.setTag(item.getTeamName());
                top_menu_view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        topMenu(true);
                        moveToStorageRoot(pos);
                    }
                });
                top_Storage_item_menu_layout.addView(top_menu_view);
                i++;
            }
//            ExpandCollapseAnimation animation = null;
//            animation = new ExpandCollapseAnimation(team_content_layout, 50, 0, MainActivity.this);
//            team_content_layout.startAnimation(animation);
        } else {
            team_layout.setVisibility(View.GONE);
        }
    }

    public void getTeamInfo(int position) {
        try {
            showProgress();
            TeamItem selectItem = MultiFragment.teamItams.get(position);
            JMF_network.getTeamInfo(selectItem.getId())
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                if (gigaPodPair.first) {
                                    Log.i("JMF getTeamInfo", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    JsonObject team = result.getAsJsonObject("team");
                                    selectItem.setMyRole(result.get("my_role").getAsString());
                                    selectItem.setId(team.get("team_id").getAsString());
                                    //teamItem.setUserId(team.get("user_id").getAsString());
                                    //teamItem.setDriveId(team.get("drive_id").getAsString());
                                    selectItem.setTeamName(team.get("name").getAsString());
                                    selectItem.setVolume(team.get("volume").getAsLong());
                                    selectItem.setVolumeUsage(team.get("volume_usage").getAsLong());
                                    selectItem.setJoinRule(team.get("join_rule").getAsString());
                                    selectItem.setJoinProtectDomain(team.get("join_protect_domain").getAsString());
                                    selectItem.setFlagPush(team.get("flag_push").getAsString());
                                    selectItem.setTrashOption(team.get("trash_option").getAsInt());
                                    selectItem.setStatus(team.get("status").getAsInt());
                                    selectItem.setRegdate(Utils.formatDate(team.get("regdate").getAsString()));
                                    selectItem.setModified(Utils.formatDate(team.get("modified").getAsString()));

                                    JsonArray wait_member = result.getAsJsonArray("wait_member");
                                    ArrayList<UserItem> waitItems = new ArrayList<>();
                                    for (int i = 0; i < wait_member.size(); i++) {
                                        JsonObject waitMember = wait_member.get(i).getAsJsonObject();
                                        UserItem userItem = new UserItem();
                                        userItem.setMemberID(waitMember.get("team_member_id").getAsString());
                                        userItem.setLeader(waitMember.get("role").getAsString().equals("leader") ? true : false);
                                        userItem.setEmail(waitMember.get("email").getAsString());
                                        userItem.setName(waitMember.get("name").getAsString());
                                        userItem.setProfile(waitMember.get("profile").getAsString());
                                        userItem.setProfileThumb(waitMember.get("profile_thumb").getAsString());
                                        //userItem.setGrade(member.get("grade").getAsString());
                                        userItem.setStatus(waitMember.get("status").getAsInt());
                                        userItem.setRegdate(Utils.formatDate(waitMember.get("regdate").getAsString()));
//                                                    userItem.setName("김남오");
//                                                    userItem.setProfile("https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/26056134_1512623652118681_8894216410971930025_n.jpg?oh=0c96ee55b7f3e4b414a0b3027002793e&oe=5B0E540A");
                                        waitItems.add(userItem);
                                    }
                                    selectItem.setWaitList(waitItems);


                                    JsonArray team_member = result.getAsJsonArray("team_member");
                                    ArrayList<UserItem> userItems = new ArrayList<>();
                                    for (int i = 0; i < team_member.size(); i++) {
                                        JsonObject member = team_member.get(i).getAsJsonObject();
                                        UserItem userItem = new UserItem();
                                        userItem.setMemberID(member.get("team_member_id").getAsString());
                                        userItem.setLeader(member.get("role").getAsString().equals("leader") ? true : false);
                                        userItem.setEmail(member.get("email").getAsString());
                                        userItem.setName(member.get("name").getAsString());
                                        userItem.setProfile(member.get("profile").getAsString());
                                        userItem.setProfileThumb(member.get("profile_thumb").getAsString());
                                        //userItem.setGrade(member.get("grade").getAsString());
                                        userItem.setStatus(member.get("status").getAsInt());
                                        userItem.setRegdate(Utils.formatDate(member.get("regdate").getAsString()));
//                                                    userItem.setName("김남오");
//                                                    userItem.setProfile("https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/26056134_1512623652118681_8894216410971930025_n.jpg?oh=0c96ee55b7f3e4b414a0b3027002793e&oe=5B0E540A");
                                        userItems.add(userItem);
                                    }
                                    selectItem.setUserList(userItems);

                                    moveToStorageRoot(position);
                                } else {
                                    dismissProgress();
                                    Toast.makeText(MainActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                    selectItem.setUserList(new ArrayList<>());
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
            return;
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgress();
            Toast.makeText(MainActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void moveToStorageRoot(int position) {
        TeamItem selectItem = MultiFragment.teamItams.get(position);
        try {
            showProgress();
            JMF_network.getRootPath(selectItem.getId())
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                if (gigaPodPair.first) {
                                    Log.i("JMF getRootPath", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    JsonObject current = result.getAsJsonObject("current");
//                                    CurrentItem currentItem = new CurrentItem();
//                                    currentItem.setObjectId(current.get("object_id").getAsString());
//                                    currentItem.setNode(current.get("node").getAsString());
//                                    currentItem.setName(current.get("name").getAsString());
//                                    currentItem.setSize(current.get("size").getAsString());
//                                    currentItem.setRegdate(Utils.formatDate(current.get("regdate").getAsString()));
//                                    currentItem.setModified(Utils.formatDate(current.get("modified").getAsString()));
//
//                                    JsonObject lists = result.getAsJsonObject("lists");
//                                    JsonArray rows = lists.getAsJsonArray("rows");
//                                    ArrayList<FileItem> storageList = new ArrayList<FileItem>();
//                                    for(int i=0; i < rows.size(); i++) {
//                                        FileItem item = new FileItem();
//                                        JsonObject row = rows.get(i).getAsJsonObject();
//                                        item.setId(row.get("object_id").getAsString());
//                                        item.setSObjectType(row.get("object_type").getAsString());
//                                        item.setSNode(row.get("node").getAsString());
//                                        item.setName(row.get("name").getAsString());
//                                        item.setSCreator(row.get("creator").getAsString());
//                                        item.setSCreatorName(row.get("creator_name").getAsString());
//                                        item.setSMemo(row.get("memo").getAsString());
//                                        item.setSize(row.get("size").getAsString());
//                                        item.setSExtension(row.get("extension").getAsString());
//                                        item.setRegDate(Utils.formatDate(row.get("regdate").getAsString()));
//                                        item.setSModifedDate(Utils.formatDate(row.get("modified").getAsString()));
//
//                                        storageList.add(item);
//                                    }
//                                    StorageListItem storageListItem = new StorageListItem(currentItem, storageList);
//                                    getSupportFragmentManager().beginTransaction()
//                                            .replace(R.id.content_frame, new StorageFragment(MainActivity.this, selectItem, storageListItem))
//                                            .commit();

                                    getStorageList(selectItem, current.get("object_id").getAsString());
                                    //Toast.makeText(mMainContext, "object_id => " + object_id, Toast.LENGTH_SHORT).show();
                                } else {
                                    dismissProgress();
                                    Toast.makeText(MainActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
            Toast.makeText(MainActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void getStorageList(TeamItem team, String objectId) {
        try {
            String sort = "";
            if (MyFileSettings.getSortReverse()) {
                sort = "+";
            } else {
                sort = "-";
            }
            switch (MyFileSettings.getSortType()) {
                case SortUtils.SORT_ALPHA:
                    sort += "name";
                    break;
                case SortUtils.SORT_TYPE:
                    sort += "extension";
                    break;
                case SortUtils.SORT_SIZE:
                    sort += "size";
                    break;
                case SortUtils.SORT_DATE:
                    sort += "regdate";
                    break;
                default:
                    sort += "name";
                    break;
            }

            JMF_network.getFolderList(team.getId(), objectId, sort, StorageFragment.LIST_LIMIT, "", "")
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF getStorageList", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");

                                    JsonObject current = result.getAsJsonObject("current");
                                    CurrentItem currentItem = new CurrentItem();
                                    currentItem.setObjectId(current.get("object_id").getAsString());
                                    currentItem.setNode(current.get("node").getAsString());
                                    currentItem.setName(current.get("name").getAsString());
                                    currentItem.setSize(current.get("size").getAsString());
                                    currentItem.setRegdate(Utils.formatDate(current.get("regdate").getAsString()));
                                    currentItem.setModified(Utils.formatDate(current.get("modified").getAsString()));

                                    JsonObject lists = result.getAsJsonObject("lists");
                                    StorageFragment.isLastpage = lists.get("lastpage").getAsBoolean();
                                    JsonArray rows = lists.getAsJsonArray("rows");
                                    ArrayList<FileItem> storageList = new ArrayList<FileItem>();
                                    for (int i = 0; i < rows.size(); i++) {
                                        FileItem item = new FileItem();
                                        JsonObject row = rows.get(i).getAsJsonObject();
                                        item.setId(row.get("object_id").getAsString());
                                        item.setTeamId(team.getId());
                                        item.setSObjectType(row.get("object_type").getAsString());
                                        item.setSNode(row.get("node").getAsString());
                                        item.setName(row.get("name").getAsString());
                                        item.setSCreator(row.get("creator").getAsString());
                                        item.setSCreatorName(row.get("creator_name").getAsString());
                                        item.setSMemo(row.get("memo").getAsString());
                                        item.setSize(row.get("size").getAsString());
                                        item.setSExtension(row.get("extension").getAsString());
                                        item.setSThumbnail(row.get("thumbnail").getAsString());
                                        item.setSUrlPath(row.get("url_path").getAsString());
                                        item.setRegDate(Utils.formatDate(row.get("regdate").getAsString()));
                                        item.setSModifedDate(Utils.formatDate(row.get("modified").getAsString()));

                                        storageList.add(item);
                                    }

                                    StorageListItem storageListItem = new StorageListItem(currentItem, storageList);
                                    getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.content_frame, new StorageFragment(MainActivity.this, team, storageListItem))
                                            .commit();
                                } else {
                                    Toast.makeText(MainActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
            Toast.makeText(MainActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean requestCurrentFragmentBackPressed() {
        if (bottomMenu(true) || topMenu(true)) {
            if (bottom_multi_item_menu_layout.isShown()) {
                return false;
            }
            Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
            if (fm instanceof FolderFragment) {
                ((FolderFragment) fm).finishActionMode();
            } else if (fm instanceof CategoryFragment) {
                ((CategoryFragment) fm).finishActionMode();
            } else if (fm instanceof StorageFragment) {
                ((StorageFragment) fm).finishActionMode();
            } else if (fm instanceof CategoryStorageFragment) {
                ((CategoryStorageFragment) fm).finishActionMode();
            }
            return false;
        }

        if (mSearchMode) {
            finishSearchMode();
            return false;
        }

        Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);

        if (fm instanceof MultiFragment) {
            return ((MultiFragment) fm).onBackPressed();
        } else if (fm instanceof SettingsFragment) {
            return ((SettingsFragment) fm).onBackPressed();
        } else if (fm instanceof FeedbackFragment) {
            return ((FeedbackFragment) fm).onBackPressed();
        } else if (fm instanceof SecretFolderFragment) {
            return ((SecretFolderFragment) fm).onBackPressed();
        } else if (fm instanceof CategoryFragment) {
            return ((CategoryFragment) fm).onBackPressed();
        } else if (fm instanceof StorageFragment) {
            return ((StorageFragment) fm).onBackPressed();
        } else if (fm instanceof CategoryStorageFragment) {
            return ((CategoryStorageFragment) fm).onBackPressed();
        } else {
            return ((FolderFragment) fm).onBackPressed();
        }
    }

    public void toolbarMenu(int res) {
        toolbar.getMenu().clear();
        toolbar.inflateMenu(res);
    }

    public void setSearchMode() {
        mSearchMode = true;
        search_layout.setVisibility(View.VISIBLE);
        search_edit.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(search_edit, InputMethodManager.SHOW_IMPLICIT);
    }

    public void finishSearchMode() {
        mSearchMode = false;
        search_edit.setText("");
        search_layout.setVisibility(View.GONE);
    }

    public void multiSelectMode() {
        item_menu_another_app.setVisibility(View.GONE);
        item_menu_copy.setVisibility(View.GONE);
        item_menu_move.setVisibility(View.GONE);
        item_menu_rename.setVisibility(View.VISIBLE);
        item_menu_send_member.setVisibility(View.GONE);
        item_menu_secretfolder.setVisibility(View.VISIBLE);
        item_menu_favorite.setVisibility(View.VISIBLE);
        item_menu_e_album.setVisibility(View.GONE);
        item_menu_tag.setVisibility(View.VISIBLE);
        item_menu_delete.setVisibility(View.GONE);

        top_multi_item_menu_layout.setVisibility(View.VISIBLE);
        bottom_multi_item_menu_layout.setVisibility(View.VISIBLE);

        Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fm instanceof FolderFragment) {
            bottom_multi_item_menu_etc.setVisibility(View.VISIBLE);
        } else if (fm instanceof CategoryFragment) {
            bottom_multi_item_menu_etc.setVisibility(View.VISIBLE);
        } else if (fm instanceof StorageFragment) {
            bottom_multi_item_menu_etc.setVisibility(View.GONE);
        } else if (fm instanceof CategoryStorageFragment) {
            bottom_multi_item_menu_etc.setVisibility(View.GONE);
        }
    }

    public void finishMultiSelectMode() {
        top_multi_item_title.setText("");
        top_multi_item_menu_layout.setVisibility(View.GONE);
        bottom_multi_item_menu_layout.setVisibility(View.GONE);
    }

    public void pasteFinish() {
        Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fm instanceof FolderFragment) {
            ((FolderFragment) fm).listRefresh();
        }
    }

    public void bottomMenuPermission(boolean folder, boolean storage, boolean multi, boolean allFavorite) {
        if (folder) {
            item_menu_another_app.setVisibility(View.GONE);

            if (bottom_multi_item_menu_layout.isShown()) {
                item_menu_copy.setVisibility(View.GONE);
                item_menu_move.setVisibility(View.GONE);
                if (multi) {
                    item_menu_rename.setVisibility(View.GONE);
                } else {
                    item_menu_rename.setVisibility(View.VISIBLE);
                }
                item_menu_delete.setVisibility(View.GONE);
            } else {
                item_menu_copy.setVisibility(View.VISIBLE);
                item_menu_move.setVisibility(View.VISIBLE);
                item_menu_rename.setVisibility(View.VISIBLE);
                item_menu_delete.setVisibility(View.VISIBLE);
            }
            item_menu_send_member.setVisibility(View.GONE);
            item_menu_secretfolder.setVisibility(View.GONE);
            if (storage) {
                item_menu_favorite.setVisibility(View.GONE);
                item_menu_tag.setVisibility(View.GONE);
            } else {
                item_menu_favorite.setVisibility(View.VISIBLE);
                item_menu_tag.setVisibility(View.VISIBLE);
            }
            item_menu_e_album.setVisibility(View.GONE);

        } else {
            if (bottom_multi_item_menu_layout.isShown()) {
                item_menu_copy.setVisibility(View.GONE);
                item_menu_move.setVisibility(View.GONE);
                if (multi) {
                    item_menu_another_app.setVisibility(View.GONE);
                    item_menu_rename.setVisibility(View.GONE);
                } else {
                    item_menu_another_app.setVisibility(View.VISIBLE);
                    item_menu_rename.setVisibility(View.VISIBLE);
                }
                item_menu_delete.setVisibility(View.GONE);
            } else {
                item_menu_copy.setVisibility(View.VISIBLE);
                item_menu_move.setVisibility(View.VISIBLE);
                item_menu_another_app.setVisibility(View.VISIBLE);
                item_menu_rename.setVisibility(View.VISIBLE);
                item_menu_delete.setVisibility(View.VISIBLE);
            }

            item_menu_send_member.setVisibility(View.GONE);

            if (storage) {
                item_menu_secretfolder.setVisibility(View.GONE);
                item_menu_favorite.setVisibility(View.GONE);
                item_menu_tag.setVisibility(View.GONE);
            } else {
                item_menu_secretfolder.setVisibility(View.VISIBLE);
                item_menu_favorite.setVisibility(View.VISIBLE);
                item_menu_tag.setVisibility(View.VISIBLE);
            }
            item_menu_e_album.setVisibility(View.GONE);

        }

        if (allFavorite) {
            item_menu_favorite_iv.setImageResource(R.drawable.ic_unstar_navy_24);
        } else {
            item_menu_favorite_iv.setImageResource(R.drawable.ic_star_navy_24);
        }
        bottomMenu(false);
    }

    public boolean topMenu(boolean check) {
        if (top_Storage_item_menu_layout.getVisibility() == View.VISIBLE) {
            goneAniBottom = AnimationUtils.loadAnimation(this, R.anim.main_top_info_slide_up);
            top_Storage_item_menu_layout.setAnimation(goneAniBottom);
            top_Storage_item_menu_layout.setVisibility(View.GONE);
            main_title_dimd_layout.setVisibility(View.GONE);
            main_dimd_layout.setVisibility(View.GONE);
            return true;
        } else {
            if (!check) {
                String position = "";
                Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fm instanceof FolderFragment) {
                    if (Utils.isSDcardDirectory(true) && ((FolderFragment) fm).mCurrentPath.contains(Utils.getSDcardDirectoryPath())) {
                        position = getResources().getString(R.string.sdcard);
                    } else {
                        position = getResources().getString(R.string.internaldirectory);
                    }
                } else if (fm instanceof StorageFragment) {
                    position = ((StorageFragment) fm).mTeam.getTeamName();
                }
                int count = top_Storage_item_menu_layout.getChildCount();
                if (position != null && position.length() > 0) {
                    for (int i = 0; i < count; i++) {
                        View view = top_Storage_item_menu_layout.getChildAt(i);
                        String name = (String) view.getTag();
                        ImageView team_check_iv = (ImageView) view.findViewById(R.id.team_check_iv);
                        if (position.equals(name)) {
                            team_check_iv.setVisibility(View.VISIBLE);
                        } else {
                            team_check_iv.setVisibility(View.GONE);
                        }
                    }
                }
                showAniBottom = AnimationUtils.loadAnimation(this, R.anim.main_top_info_slide_down);
                top_Storage_item_menu_layout.setVisibility(View.VISIBLE);
                main_title_dimd_layout.setVisibility(View.VISIBLE);
                main_dimd_layout.setVisibility(View.VISIBLE);
                top_Storage_item_menu_layout.setAnimation(showAniBottom);
            }
            return false;
        }
    }

    public boolean bottomMenu(boolean check) {
        if (bottom_item_menu_layout.getVisibility() == View.VISIBLE) {
            goneAniBottom = AnimationUtils.loadAnimation(this, R.anim.main_bottom_menu_slide_bottom);
            bottom_item_menu_layout.setAnimation(goneAniBottom);
            bottom_item_menu_layout.setVisibility(View.GONE);
            main_title_dimd_layout.setVisibility(View.GONE);
            main_dimd_layout.setVisibility(View.GONE);
            return true;
        } else {
            if (!check) {
                showAniBottom = AnimationUtils.loadAnimation(this, R.anim.main_bottom_menu_slide_top);
                bottom_item_menu_layout.setVisibility(View.VISIBLE);
                main_dimd_layout.setVisibility(View.VISIBLE);
                main_title_dimd_layout.setVisibility(View.VISIBLE);
                bottom_item_menu_layout.setAnimation(showAniBottom);
            }
            return false;
        }
    }

    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = Utils.Progress(MainActivity.this, "", "Login...", false);
            progressDialog.show();
        }
    }

    public void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private void handleToolbarTitleVisibility(float percentage) {
        if (percentage >= PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR) {
            if (!titleVisible) {
                startAlphaAnimation(drawerToolbar, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                titleVisible = true;
            }
        } else {
            if (titleVisible) {
                startAlphaAnimation(drawerToolbar, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                titleVisible = false;
            }
        }
    }

    private void handleAlphaOnTitle(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if (containerVisible) {
                startAlphaAnimation(userInfoLayout, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                containerVisible = false;
            }

        } else {

            if (!containerVisible) {
                startAlphaAnimation(userInfoLayout, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                containerVisible = true;
            }
        }
    }

    public void loginAction() {
        setLoginStatus();
//        user_info_layout.setVisibility(View.VISIBLE);
//        left_menu_login_str.setText(getString(R.string.left_menu_top_logout));
//        left_menu_account_manage_str.setText(getString(R.string.left_menu_top_account_manage));
//        main_user_email_tv.setText(LoginActivity.mUserEmail);
//        main_user_name_tv.setText(LoginActivity.mUserName);
//        drawerNameText.setText(LoginActivity.mUserName);
//        left_menu_inappbilling.setVisibility(View.VISIBLE);
//
//        if (LoginActivity.mUserItem.getProfileThumb() != null && LoginActivity.mUserItem.getProfileThumb().length() > 0) {
//            Glide.with(MainActivity.this).load(LoginActivity.mUserItem.getProfileThumb()).asBitmap().centerCrop().placeholder(R.mipmap.user_none).into(new BitmapImageViewTarget(main_profile_iv) {
//                @Override
//                protected void setResource(Bitmap resource) {
//                    RoundedBitmapDrawable circularBitmapDrawable =
//                            RoundedBitmapDrawableFactory.create(getResources(), resource);
//                    circularBitmapDrawable.setCircular(true);
//                    main_profile_iv.setImageDrawable(circularBitmapDrawable);
//                }
//            });
//            Glide.with(MainActivity.this).load(LoginActivity.mUserItem.getProfileThumb()).asBitmap().centerCrop().placeholder(R.mipmap.user_none).into(new BitmapImageViewTarget(drawerProfileImage) {
//                @Override
//                protected void setResource(Bitmap resource) {
//                    RoundedBitmapDrawable circularBitmapDrawable =
//                            RoundedBitmapDrawableFactory.create(getResources(), resource);
//                    circularBitmapDrawable.setCircular(true);
//                    drawerProfileImage.setImageDrawable(circularBitmapDrawable);
//                }
//            });
//        }
//        main_profile_iv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//                if (drawer.isDrawerOpen(GravityCompat.START)) {
//                    drawer.closeDrawer(GravityCompat.START);
//
//                    Intent intent = new Intent(MainActivity.this, AccountManageActivity.class);
//                    startActivityForResult(intent, MainActivity.REQUEST_CODE_ACCOUNT_MANAGE_ACTIVITY);
//                }
//            }
//        });

        //MultiFragment 일경우 초기화
        Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fm instanceof MultiFragment) {
            ((MultiFragment) fm).loginAction();
        } else {
            initFragment();
            displayTeamList();
        }
    }

    public void logoutAction() {
        PreferenceHelper.putString("access_token", "");
        PreferenceHelper.putString("user_name", "");
        LoginActivity.mLoginToken = "";
        LoginActivity.mUserEmail = "";
        LoginActivity.mUserName = "";

        setLoginStatus();
//        user_info_layout.setVisibility(View.GONE);
//        left_menu_login_str.setText(getString(R.string.left_menu_top_login));
//        left_menu_account_manage_str.setText(getString(R.string.member_join));
//        main_profile_iv.setImageResource(R.drawable.ic_profile_gray_48);
//        main_user_email_tv.setText("");
//        main_user_name_tv.setText("");
//        drawerNameText.setText("");
//        drawerInfoLayout.setVisibility(View.GONE);
//        team_layout.setVisibility(View.GONE);
//        left_menu_inappbilling.setVisibility(View.VISIBLE);

        //MultiFragment 일경우 초기화
        MultiFragment.teamItams = null;
        Fragment fm = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fm instanceof MultiFragment) {
            ((MultiFragment) fm).logoutAction();
        } else if (fm instanceof StorageFragment || fm instanceof CategoryStorageFragment) {
            initFragment();
        }
    }

    private void setLoginStatus() {
        boolean isLogined = LoginActivity.mLoginToken != null && LoginActivity.mLoginToken.length() > 0;
        left_menu_account_manage_str.setText(getString(isLogined ? R.string.left_menu_top_account_manage : R.string.member_join));
        left_menu_login_str.setText(getString(isLogined ? R.string.left_menu_top_logout : R.string.left_menu_top_login));
        user_info_layout.setVisibility(isLogined ? View.VISIBLE : View.GONE);
        main_user_email_tv.setText(isLogined ? LoginActivity.mUserEmail : "");
        main_user_name_tv.setText(isLogined ? LoginActivity.mUserName : "");
        drawerNameText.setText(LoginActivity.mUserName);
        drawerInfoLayout.setVisibility(isLogined ? View.VISIBLE : View.GONE);
        team_layout.setVisibility(isLogined ? View.VISIBLE : View.GONE);
        // premium inapp
        left_menu_inappbilling.setVisibility(View.GONE);
//        left_menu_inappbilling.setVisibility(isLogined ? View.VISIBLE : View.GONE);

        if (isLogined && LoginActivity.mUserItem.getProfileThumb() != null && LoginActivity.mUserItem.getProfileThumb().length() > 0) {
            Glide.with(MainActivity.this).load(LoginActivity.mUserItem.getProfileThumb()).asBitmap().centerCrop().placeholder(R.drawable.ic_profile_gray_48).into(new BitmapImageViewTarget(main_profile_iv) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    main_profile_iv.setImageDrawable(circularBitmapDrawable);
                    drawerProfileImage.setImageDrawable(circularBitmapDrawable);
                }

                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    main_profile_iv.setImageResource(R.drawable.ic_profile_gray_48);
                    drawerProfileImage.setImageResource(R.drawable.ic_profile_gray_48);
                }
            });
        } else {
            main_profile_iv.setImageResource(R.drawable.ic_profile_gray_48);
            drawerProfileImage.setImageResource(R.drawable.ic_profile_gray_48);
        }
    }

    private class LongOperationDocument extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            // ListFunctionUtils.listAllDocumrntFilesInit();

            try {
                ListFunctionUtils.listAllDocumentFiles(MainActivity.this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    private class LongOperationDays extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            ListFunctionUtils.listDaysFilesInit(ListFunctionUtils.LIST_DAY);
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    private class LongOperationAPK extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            ListFunctionUtils.listAllAPKFiles(MainActivity.this);
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    private class LongOperationZip extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            ListFunctionUtils.listAllZipFiles(MainActivity.this);
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }
}



