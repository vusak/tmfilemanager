package jiran.com.tmfilemanager.activity.viewer;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.devbrackets.android.exomedia.EMVideoView;
import com.devbrackets.android.exomedia.event.EMMediaProgressEvent;
import com.devbrackets.android.exomedia.listener.EMProgressCallback;

import java.util.concurrent.TimeUnit;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.data.ImageModel;
import jiran.com.tmfilemanager.utils.DisplayUtils;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

//import de.greenrobot.event.EventBus;

public class MoviePageFragment extends Fragment implements View.OnClickListener, EMProgressCallback,
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener {
    public static final String TAG = MoviePageFragment.class.getSimpleName();

    public static final String PROJECTID_ARGUMENT_KEY = TAG + ":" + "ProjectDArgumentKey";
    public static final String IMAGE_ARGUMENT_KEY = TAG + ":" + "ImagesArgumentKey";
    public static final String OFFLINE_ARGUMENT_KEY = TAG + ":" + "OfflineArgumentKey";
    private static boolean mControlsFrameIsShow = true;
    //Add
    public Context mMainContext;
    FileItem mItem;
    ProgressBar progress, progressbar;
    boolean isConnecting = false;
    boolean LOCALFILE = false;
    private String mProjectId;
    private ImageModel mImageModel;
    private boolean mIsOffline;
    private TextView mPosition;
    private SeekBar mPositionSeek;
    private TextView mDuration;
    private ImageButton mPlayPause;
    private View mRetry;
    private View mControlsFrame;
    private EMVideoView mStreamer;
    private String mOutputUri;
    private boolean mWasPlaying;
    private boolean mFinishedPlaying;

    public static MoviePageFragment newInstance(String mProjectId, ImageModel imageModel, boolean isOffline) {
        MoviePageFragment newInstance = new MoviePageFragment();

        Bundle arguments = new Bundle();
        arguments.putString(PROJECTID_ARGUMENT_KEY, mProjectId);
        arguments.putParcelable(IMAGE_ARGUMENT_KEY, imageModel);
        arguments.putBoolean(OFFLINE_ARGUMENT_KEY, isOffline);
        newInstance.setArguments(arguments);

        return newInstance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //EventBus.getDefault().register(this);

        Bundle arguments = getArguments();
        if (arguments != null) {
            if (arguments.containsKey(PROJECTID_ARGUMENT_KEY)) {
                mProjectId = arguments.getString(PROJECTID_ARGUMENT_KEY);
            }
            if (arguments.containsKey(IMAGE_ARGUMENT_KEY)) {
                mImageModel = arguments.getParcelable(IMAGE_ARGUMENT_KEY);
                mItem = mImageModel.getFileItem();
                mMainContext = mImageModel.getContext();
            }

            if (arguments.containsKey(OFFLINE_ARGUMENT_KEY)) {
                mIsOffline = arguments.getBoolean(OFFLINE_ARGUMENT_KEY, false);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_moviepage, container, false);
//        Dlog.d( "onCreateView: " + mImageModel.getFile_id());

        mPosition = (TextView) view.findViewById(R.id.position);
        mPositionSeek = (SeekBar) view.findViewById(R.id.positionSeek);
        mDuration = (TextView) view.findViewById(R.id.duration);
        mPlayPause = (ImageButton) view.findViewById(R.id.playPause);
        mRetry = view.findViewById(R.id.retry);
        mControlsFrame = view.findViewById(R.id.controlsFrame);
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) mControlsFrame.getLayoutParams();
        params.setMargins(0, 0, 0, DisplayUtils.getNavBarHeight(getContext())); //substitute parameters for left, top, right, bottom
        mControlsFrame.setLayoutParams(params);

        mStreamer = (EMVideoView) view.findViewById(R.id.video_play_activity_video_view);

        view.findViewById(R.id.playbackFrame).setOnClickListener(this);
        mRetry.setOnClickListener(this);
        mPlayPause.setOnClickListener(this);
        mPositionSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    if (progress < seekBar.getMax())
                        mFinishedPlaying = false;
                    else if (progress >= seekBar.getMax())
                        mFinishedPlaying = true;
                    mStreamer.seekTo(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mWasPlaying = mStreamer.isPlaying();
                mStreamer.pause();
                mStreamer.stopProgressPoll();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (mWasPlaying) {
                    mStreamer.start();
                    mStreamer.startProgressPoll(MoviePageFragment.this);
                }
            }
        });
        mOutputUri = mImageModel.getFile_id();
        //Log.d("onCreateView: mOutputUri :" + mOutputUri);

        mStreamer.setDefaultControlsEnabled(false);
        mStreamer.setProgressCallback(this);
        mStreamer.setOnPreparedListener(this);
        mStreamer.setOnErrorListener(this);
        mStreamer.setOnCompletionListener(this);

        if (mStreamer.isPlaying())
            mPlayPause.setImageResource(R.drawable.pause);
        else
            mPlayPause.setImageResource(R.drawable.aa_feed_icon_sent_unopened_blue);

        progress = (ProgressBar) view.findViewById(R.id.progress);

        mStreamer.setVideoURI(Uri.parse(mOutputUri));
        LOCALFILE = true;

//		//HideContralFrame();
//		Observable.timer(1, TimeUnit.SECONDS)
//				.subscribeOn(Schedulers.computation())
//				.observeOn(AndroidSchedulers.mainThread())
//				.subscribe(aLong -> {
//					playmovie();
//					HideContralFrame();
//				});
        pausemovie();
        return view;
    }

    @Override
    public void onResume() {
        //Toast.makeText(getActivity(), mPath, Toast.LENGTH_SHORT).show();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            if (mStreamer != null) {
                if (mStreamer.isPlaying()) {
                    pausemovie();
                }
            }
        }
//		if (isVisibleToUser)
//		{
//			//화면에 실제로 보일때
//			Toast.makeText(mMainContext, "Movie onResume => " + mImageModel.getFileItem().getName(), Toast.LENGTH_SHORT).show();
//		}
//		else
//		{
//			//preload 될때(전페이지에 있을때)
//		}
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //EventBus.getDefault().unregister(this);
        mPosition = null;
        mPositionSeek = null;
        mDuration = null;
        mPlayPause = null;
        mRetry = null;
        mControlsFrame = null;
        mStreamer = null;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.playbackFrame) {
            if (mControlsFrameIsShow)
                HideContralFrame();
            else
                ShowControlFrame();

            //EventBus.getDefault().post(new PhotoOnTabEvent(PhotoOnTabEvent.Direction.Center));
        } else if (v.getId() == R.id.playPause) {
            PlayPauseEvent();
        } else if (v.getId() == R.id.retry) {
//            mInterface.onRetry(mOutputUri);
            mStreamer.setVideoURI(Uri.parse(mOutputUri));
            mPositionSeek.setProgress((int) mStreamer.getDuration());
            mPosition.setText(getDurationString(mStreamer.getDuration()));

            Observable.timer(1, TimeUnit.SECONDS)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(aLong -> {
                        playmovie();
                        HideContralFrame();
                    });
        }
    }


    private void HideContralFrame() {
        mControlsFrame.setVisibility(View.INVISIBLE);
        mControlsFrameIsShow = false;
    }

    private void ShowControlFrame() {
        mControlsFrame.setVisibility(View.VISIBLE);
        mControlsFrameIsShow = true;
    }

    private void PlayPauseEvent() {
        if (mStreamer != null) {

            if (mStreamer.isPlaying()) {
                pausemovie();
            } else {
                playmovie();
            }
        }
    }

    public void playmovie() {
        if (mFinishedPlaying)
            mStreamer.seekTo(0);
        mFinishedPlaying = false;
        mPlayPause.setImageResource(R.drawable.pause);

        mStreamer.start();
        mStreamer.startProgressPoll(MoviePageFragment.this);
    }


    public void pausemovie() {
        if (mStreamer != null) {
            mPlayPause.setImageResource(R.drawable.aa_feed_icon_sent_unopened_blue);
            mStreamer.pause();
            mStreamer.stopProgressPoll();
        }
    }


    @Override
    public void onPrepared(MediaPlayer mp) {
        mStreamer.pause();
        final int durationMs = (int) mStreamer.getDuration();
        mPositionSeek.setMax(durationMs);
        mDuration.setText(String.format("-%s", getDurationString(durationMs)));
        mPlayPause.setEnabled(true);
        mRetry.setEnabled(true);
        if (!mFinishedPlaying) ;
        //EventBus.getDefault().post(new MovieLoadingEvent(MovieLoadingEvent.FLAG_PREPARED, this.getTag()));
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mFinishedPlaying = true;
        if (mPlayPause != null)
            mPlayPause.setImageResource(R.drawable.aa_feed_icon_sent_unopened_blue);
        if (mPositionSeek != null) {
            mPositionSeek.setProgress((int) mStreamer.getDuration());
            mPosition.setText(getDurationString(mStreamer.getDuration()));
        }
        mStreamer.setVideoURI(Uri.parse(mOutputUri));
        mStreamer.pause();
        //EventBus.getDefault().post(new MovieLoadingEvent(MovieLoadingEvent.FLAG_COMPLETED, this.getTag()));

    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        if (what == -38) {

            return false;
        }
        String errorMsg = "Preparation/playback error: ";
        switch (what) {
            case MediaPlayer.MEDIA_ERROR_IO:
                errorMsg += "I/O error";
                break;
            case MediaPlayer.MEDIA_ERROR_MALFORMED:
                errorMsg += "Malformed";
                break;
            case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                errorMsg += "Not valid for progressive playback";
                break;
            case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                errorMsg += "Server died";
                break;
            case MediaPlayer.MEDIA_ERROR_TIMED_OUT:
                errorMsg += "Timed out";
                break;
            case MediaPlayer.MEDIA_ERROR_UNSUPPORTED:
                errorMsg += "Unsupported";
                break;
        }

        Toast.makeText(getActivity(), "Playback Error :" + errorMsg, Toast.LENGTH_SHORT).show();

        return false;
    }


    public String getDurationString(long durationMs) {
        return String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(durationMs),
                TimeUnit.MILLISECONDS.toSeconds(durationMs) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(durationMs))
        );
    }

    @Override
    public boolean onProgressUpdated(EMMediaProgressEvent progressEvent) {
        if (progressEvent.getBufferPercent() < 100) {
            if (mPositionSeek != null)
                mPositionSeek.setSecondaryProgress(progressEvent.getBufferPercent());

        }
        if (mPositionSeek != null)
            mPositionSeek.setSecondaryProgress(0);

        try {
            final int duration = (int) progressEvent.getDuration();
            int currentPosition = (int) progressEvent.getPosition();
            if (currentPosition > duration)
                currentPosition = duration;
            if (mPosition != null)
                mPosition.setText(getDurationString(currentPosition));
            if (mPositionSeek != null)
                mPositionSeek.setProgress(currentPosition);
            if (mDuration != null)
                mDuration.setText(String.format("-%s", getDurationString(duration - currentPosition)));
        } catch (Throwable t) {
            if (mPosition != null)
                mPosition.setText(getDurationString(0));
            if (mPositionSeek != null)
                mPositionSeek.setProgress(mPositionSeek.getMax());
        }
        return true;
    }

}

