package jiran.com.tmfilemanager.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.adapter.FileSelectAdapter;
import jiran.com.tmfilemanager.common.ListFunctionUtils;
import jiran.com.tmfilemanager.common.SortUtils;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.data.FileSelectItem;
import jiran.com.tmfilemanager.network.JMF_network;
import jiran.com.tmfilemanager.settings.MyFileSettings;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Namo on 2018-02-28.
 */

public class FileSelectActivity extends AppCompatActivity {
    public static final int LIST = 0, GRID = 1;
    public static ArrayList<FileItem> uploadFileList;
    public static String mTeam_id;
    public static String mObject_id;
    public static int totalCount = 0;
    private static Stack<FileItem> listStack;
    public String mStartPath;
    public String mCurrentPath;
    FileSelectItem mFileSelectItem;
    HashMap<String, Integer> listPositionMap;
    Dialog mDialog;
    Dialog progressDialog;
    ImageView title_right_iv;
    private int mView_Type = LIST;
    private AbsListView mListView;
    private GridView mGridView;
    private FileSelectAdapter mFileSelectAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.main_bottom_menu_slide_top, R.anim.hold);
        setContentView(R.layout.activity_file_select);
        mFileSelectItem = (FileSelectItem) getIntent().getSerializableExtra("FileSelectItem");
        mCurrentPath = mFileSelectItem.getPath();
        mStartPath = mFileSelectItem.getPath();
        if (mCurrentPath.equals(getString(R.string.category_image))) {
            mView_Type = GRID;
        }
        mTeam_id = mFileSelectItem.getTeamId();
        mObject_id = mFileSelectItem.getObjectId();

        if (mCurrentPath.equals("album")) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, 0);
            return;
        }

        LinearLayout title_left_btn = (LinearLayout) findViewById(R.id.title_left_btn);
        title_left_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //onBackPressed();
                finish();
            }
        });

        LinearLayout title_right_btn = (LinearLayout) findViewById(R.id.title_right_btn);
        title_right_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeListGridView();
            }
        });
        title_right_iv = (ImageView) findViewById(R.id.title_right_iv);

        LinearLayout bottom_layout = (LinearLayout) findViewById(R.id.bottom_layout);
        bottom_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadFileList = mFileSelectAdapter.getCheckList();
                if (uploadFileList != null && uploadFileList.size() > 0) {
                    totalCount = uploadFileList.size();
                    listStack = new Stack<>();
                    for (int i = uploadFileList.size() - 1; i >= 0; i--) {
                        listStack.add(uploadFileList.get(i));
                    }
                    showProgress();
                    requestUpload(false);
                } else {
                    Toast.makeText(FileSelectActivity.this, getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
                }
            }
        });

        initLayout();
    }

    public void initLayout() {
        listPositionMap = new HashMap<String, Integer>();

        mListView = (ListView) findViewById(R.id.folder_listview);
        mListView.setEmptyView(findViewById(R.id.empty_layout));
        mGridView = (GridView) findViewById(R.id.folder_gridview);
        mGridView.setEmptyView(findViewById(R.id.empty_layout));

        if (mView_Type == LIST) {
            changeListView(false, true);
        } else {
            changeGridView(false, true);
        }
    }

    public void changeListGridView() {
        mFileSelectAdapter.listChange = true;
        if (mView_Type == LIST) {
            listPositionMap.put(mCurrentPath, mListView.getFirstVisiblePosition());
            changeGridView(true);
        } else {
            listPositionMap.put(mCurrentPath, mGridView.getFirstVisiblePosition());
            changeListView(true);
        }
    }

    public void changeGridView() {
        if (mView_Type == 0) {
            mFileSelectAdapter.listChange = true;
            MainActivity.mViewType = 1;
            listPositionMap.put(mCurrentPath, mListView.getFirstVisiblePosition());
            changeGridView(true);
        }
    }

    public void changeListView() {
        if (mView_Type == 1) {
            mFileSelectAdapter.listChange = true;
            MainActivity.mViewType = 0;
            listPositionMap.put(mCurrentPath, mGridView.getFirstVisiblePosition());
            changeListView(true);
        }
    }

    private void changeGridView(boolean listChange) {
        changeGridView(listChange, false);
    }

    private void changeGridView(boolean listChange, boolean mainlist) {
        //final MainActivity context = (MainActivity) getActivity();
        title_right_iv.setImageResource(R.mipmap.ic_lsview);
        mView_Type = GRID;
        if (listChange) {
            mFileSelectAdapter = new FileSelectAdapter(FileSelectActivity.this, null, mView_Type, mFileSelectAdapter.getList());
        } else {
            mFileSelectAdapter = new FileSelectAdapter(FileSelectActivity.this, null, mView_Type);
        }
        mGridView.setVisibility(View.VISIBLE);
        mListView.setVisibility(View.GONE);
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                FileItem item = ((FileItem) mGridView.getAdapter().getItem(position));
                final File file = new File(item.getPath());

                if (item.getSObjectType() != null && item.isDirectory()) {
                    //Scroll Position Set
                    listPositionMap.put(mCurrentPath, mListView.getFirstVisiblePosition());

                    imageFolderListDisplay(item.getName());
                    // go to the top of the ListView
                    //mListView.setSelection(0);
                } else if (file.isDirectory()) {

                    //Scroll Position Set
                    listPositionMap.put(mCurrentPath, mGridView.getFirstVisiblePosition());
                    folderListDisplay(file.getAbsolutePath());
                    // go to the top of the ListView
                    //mGridView.setSelection(0);
                } else {
                    //File Check
                    mFileSelectAdapter.setCheck(position);
                    mFileSelectAdapter.notifyDataSetChanged();
                }

            }
        });

        mGridView.setAdapter(mFileSelectAdapter);
        folderListDisplay(mCurrentPath);
//        mFolderAdapter.addFiles(mCurrentPath);
//        mFolderAdapter.notifyDataSetChanged();
    }

    private void changeListView(boolean listChange) {
        changeListView(listChange, false);
    }

    private void changeListView(boolean listChange, boolean mainlist) {
        //final MainActivity context = (MainActivity) getActivity();
        title_right_iv.setImageResource(R.mipmap.ic_tmview);
        mView_Type = LIST;

        if (listChange) {
            mFileSelectAdapter = new FileSelectAdapter(FileSelectActivity.this, null, mView_Type, mFileSelectAdapter.getList());
        } else {
            mFileSelectAdapter = new FileSelectAdapter(FileSelectActivity.this, null, mView_Type);
        }
        mListView.setVisibility(View.VISIBLE);
        mGridView.setVisibility(View.GONE);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                FileItem item = ((FileItem) mListView.getAdapter().getItem(position));
                final File file = new File(item.getPath());

                if (item.getSObjectType() != null && item.isDirectory()) {
                    //Scroll Position Set
                    listPositionMap.put(mCurrentPath, mListView.getFirstVisiblePosition());

                    imageFolderListDisplay(item.getName());
                    // go to the top of the ListView
                    //mListView.setSelection(0);
                } else if (file.isDirectory()) {
                    //Scroll Position Set
                    listPositionMap.put(mCurrentPath, mListView.getFirstVisiblePosition());

                    folderListDisplay(file.getAbsolutePath());
                    // go to the top of the ListView
                    //mListView.setSelection(0);
                } else {
                    mFileSelectAdapter.setCheck(position);
                    mFileSelectAdapter.notifyDataSetChanged();
                }

            }
        });
        mListView.setAdapter(mFileSelectAdapter);
        folderListDisplay(mCurrentPath);
    }

    public void folderListDisplay(String path) {
        mCurrentPath = path;

        if (mCurrentPath.equals(getResources().getString(R.string.category_document)) ||
                mCurrentPath.equals(getResources().getString(R.string.category_app)) ||
                mCurrentPath.equals(getResources().getString(R.string.category_zip)) ||
                mCurrentPath.equals(getResources().getString(R.string.category_recentfile)) ||
                mCurrentPath.equals(getResources().getString(R.string.category_largefile))) {
            if (mFileSelectAdapter.listChange) {
                mFileSelectAdapter.listChange = false;
                mFileSelectAdapter.notifyDataSetChanged();
            } else {
                if (mCurrentPath.equals(getResources().getString(R.string.category_document)) && ListFunctionUtils.mTempDocuContent.size() > 0) {
                    SortUtils.sortList(ListFunctionUtils.mTempDocuContent, mCurrentPath, MyFileSettings.getSortType());
                    mFileSelectAdapter.addContent(ListFunctionUtils.mTempDocuContent);
                    mFileSelectAdapter.notifyDataSetChanged();
                } else if (mCurrentPath.equals(getResources().getString(R.string.category_app)) && ListFunctionUtils.mTempAppContent.size() > 0) {
                    SortUtils.sortList(ListFunctionUtils.mTempAppContent, mCurrentPath, MyFileSettings.getSortType());
                    mFileSelectAdapter.addContent(ListFunctionUtils.mTempAppContent);
                    mFileSelectAdapter.notifyDataSetChanged();
                } else if (mCurrentPath.equals(getResources().getString(R.string.category_zip)) && ListFunctionUtils.mTempZipContent.size() > 0) {
                    SortUtils.sortList(ListFunctionUtils.mTempZipContent, mCurrentPath, MyFileSettings.getSortType());
                    mFileSelectAdapter.addContent(ListFunctionUtils.mTempZipContent);
                    mFileSelectAdapter.notifyDataSetChanged();
                } else if (mCurrentPath.equals(getResources().getString(R.string.category_recentfile)) && ListFunctionUtils.mTempRecentContent.size() > 0) {
                    SortUtils.sortList(ListFunctionUtils.mTempRecentContent, mCurrentPath, MyFileSettings.getSortType());
                    mFileSelectAdapter.addContent(ListFunctionUtils.mTempRecentContent);
                    mFileSelectAdapter.notifyDataSetChanged();
                } else if (mCurrentPath.equals(getResources().getString(R.string.category_largefile)) && ListFunctionUtils.mTempHugeContent.size() > 0) {
                    SortUtils.sortList(ListFunctionUtils.mTempHugeContent, mCurrentPath, MyFileSettings.getSortType());
                    mFileSelectAdapter.addContent(ListFunctionUtils.mTempHugeContent);
                    mFileSelectAdapter.notifyDataSetChanged();
                } else {
//                    FolderFragment.LoadingTask mTask = new FolderFragment.LoadingTask(getActivity(), mCurrentPath);
//                    mTask.execute("");
                }
            }
        } else {
            mFileSelectAdapter.addFiles(path, 0);
        }
        //Scroll Position Set
        setScrollPosition(path);
    }

    public void imageFolderListDisplay(String displayName) {
        mCurrentPath = displayName;
        mFileSelectAdapter.addImageFiles(displayName);
        //Scroll Position Set
        setScrollPosition(displayName);
    }

    public void setScrollPosition(String path) {
        //Scroll Position Set
        int position;
        if (listPositionMap.containsKey(path)) {
            position = listPositionMap.get(path);
            listPositionMap.remove(path);
        } else {
            position = 0;
        }

        if (mView_Type == LIST) {
            mListView.setSelection(position);
        } else {
            mGridView.setSelection(position);
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.hold, R.anim.main_bottom_menu_slide_bottom);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        try {
            if (mStartPath != null && mStartPath.equals(getString(R.string.category_image))) {
                if (mCurrentPath.equals(getString(R.string.category_image))) {
                    finish();
                } else {
                    folderListDisplay(getString(R.string.category_image));
                }
            } else if (mStartPath != null && mStartPath.equals(mCurrentPath)) {
                finish();
            } else {
                if (mCurrentPath.equals(Utils.getInternalDirectoryPath())) {
                    finish();
                } else {
                    File file = new File(mCurrentPath);
                    folderListDisplay(file.getParent());
                }
            }
        } catch (Exception e) {

        }
        //super.onBackPressed();
    }

    public void showProgress() {
        progressDialog = Utils.ProgressText(FileSelectActivity.this, "");
        progressDialog.show();
        ;
//        mLoginProgressBar.setVisibility(View.VISIBLE);
//        mLoginProgressBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//            }
//        });
    }

    public void setTextProgress(String contents) {
        if (progressDialog != null) {
            View view = progressDialog.getWindow().getDecorView();
            TextView progress_tv = (TextView) view.findViewById(R.id.progress_tv);
            progress_tv.setVisibility(View.VISIBLE);
            progress_tv.setText(contents);
        }
    }


    public void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
//        if(mLoginProgressBar != null){
//            mLoginProgressBar.setVisibility(View.GONE);
//        }
    }

    public void requestUpload(boolean remove) {
        setTextProgress("(" + (uploadFileList.size() - listStack.size()) + "/" + uploadFileList.size() + ")");
        if (listStack != null && listStack.size() > 0) {
            FileItem currentItem = listStack.get(listStack.size() - 1);
            listStack.pop();
            try {
                Log.i("JMF requestUpload", "team_id => " + mTeam_id + " object_id => " + mObject_id);

//                List<MultipartBody.Part> parts= new ArrayList();
//                for(FileItem fileItem : fileItems){
//                    File file = new File(currentItem.getPath());
//
//                    RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
//                    MultipartBody.Part part = MultipartBody.Part.createFormData("filedata", file.getName(), requestBody);
//                    parts.add(part);
//                }

                List<MultipartBody.Part> parts = new ArrayList();
                File file = new File(currentItem.getPath());

                String mime = jiran.com.tmfilemanager.common.MimeTypes.getMimeType(file);
                RequestBody requestBody;
                if (mime != null) {
                    requestBody = RequestBody.create(MediaType.parse(mime), file);
                } else {
                    requestBody = RequestBody.create(MediaType.parse("*/*"), file);
                }

                MultipartBody.Part part = MultipartBody.Part.createFormData("filedata", file.getName(), requestBody);
                parts.add(part);

                JMF_network.fileUpload(mTeam_id, mObject_id, parts)
                        .flatMap(gigaPodPair -> {
                            return Observable.just(gigaPodPair);
                        })
                        .doOnError(throwable -> {

                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(gigaPodPair -> {
                                    if (gigaPodPair.first) {
                                        Log.i("JMF fileUpload", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
//                                    mBuilder.setContentText("Upload complete");
//                                    // Removes the progress bar
//                                    mBuilder.setProgress(0, 0, false);
//                                    mNotifyManager.notify(0, mBuilder.build());

                                        int current = uploadFileList.size() - listStack.size();
                                        setTextProgress("(" + current + "/" + uploadFileList.size() + ")");
                                        if (remove) {
                                            file.delete();
                                        }
                                    }
//                                    else {
//                                        sendNotification("Upload Fail.=> " + currentItem.getName(), "", "");
//                                    }
                                    requestUpload(false);
                                },
                                throwable -> {
                                    //sendNotification("Upload Fail.=> " + currentItem.getName(), "", "");
                                    requestUpload(false);
                                    throwable.printStackTrace();
                                });
            } catch (Exception e) {

                e.printStackTrace();
            }
        } else {
            setTextProgress("(" + totalCount + "/" + totalCount + ")");
            mTeam_id = "";
            mObject_id = "";
            setResult(RESULT_OK);
            finish();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == this.RESULT_CANCELED) {
            finish();
            return;
        }
        if (requestCode == 0) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    if (contentURI != null) {
                        File file = new File(Utils.getRealPathFromURI(FileSelectActivity.this, contentURI));
                        if (file.exists()) {
                            FileItem currentItem = new FileItem();
                            currentItem.setPath(file.getPath());
                            ArrayList<FileItem> checkList = new ArrayList<FileItem>();
                            checkList.add(currentItem);
                            uploadFileList = checkList;
                            if (uploadFileList != null && uploadFileList.size() > 0) {
                                totalCount = uploadFileList.size();
                                listStack = new Stack<>();
                                for (int i = uploadFileList.size() - 1; i >= 0; i--) {
                                    listStack.add(uploadFileList.get(i));
                                }
                                showProgress();
                                requestUpload(false);
                            } else {
                                Toast.makeText(FileSelectActivity.this, getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
                            }
                            return;
                        }
                    }
                } catch (Exception e) {

                }

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    //Toast.makeText(AccountManageActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    FileItem currentItem = new FileItem();
                    currentItem.setPath(path);
                    ArrayList<FileItem> checkList = new ArrayList<FileItem>();
                    checkList.add(currentItem);
                    uploadFileList = checkList;
                    if (uploadFileList != null && uploadFileList.size() > 0) {
                        totalCount = uploadFileList.size();
                        listStack = new Stack<>();
                        for (int i = uploadFileList.size() - 1; i >= 0; i--) {
                            listStack.add(uploadFileList.get(i));
                        }
                        showProgress();
                        requestUpload(true);
                    } else {
                        Toast.makeText(FileSelectActivity.this, getString(R.string.snackbar_no_select), Toast.LENGTH_SHORT).show();
                    }
                    //uploadProfile(path);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(FileSelectActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
//        File wallpaperDirectory = new File(
//                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        File wallpaperDirectory = new File(Utils.getStorageSaveFileFolderPath());

        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }
}
