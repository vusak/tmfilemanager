package jiran.com.tmfilemanager.activity.viewer;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.barteksc.pdfviewer.PDFView;

import java.io.File;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.InitActivity;
import jiran.com.tmfilemanager.data.FileItem;

/**
 * Created by user on 2016-08-03.
 */


public class PdfPageFragment extends Fragment {
    public Context mMainContext;

    private FileItem mItem;

    private Dialog mDialog;

    public PdfPageFragment() {
        setRetainInstance(true);
    }

    @SuppressLint("ValidFragment")
    public PdfPageFragment(Context context, FileItem item) {
        mMainContext = context;
        mItem = item;
        setRetainInstance(true);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mMainContext == null) {
            Intent intent = new Intent(getActivity(), InitActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        }
        View rootView = inflater.inflate(R.layout.fragment_pdfpage, container, false);
        initLayout(inflater, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void initLayout(LayoutInflater inflater, View rootView) {
        //final MainActivity context = (MainActivity) getActivity();
        //Setting

        PDFView pdfView = (PDFView) rootView.findViewById(R.id.pdfView);
        pdfView.fromFile(new File(mItem.getPath())).load();
    }

    public void applicationRestart() {
        Intent intent = getActivity().getBaseContext().getPackageManager().getLaunchIntentForPackage(getActivity().getPackageName());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        getActivity().startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser)
//        {
//            //화면에 실제로 보일때
//            Toast.makeText(mMainContext, "Pdf onResume => " + mItem.getName(), Toast.LENGTH_SHORT).show();
//        }
//        else
//        {
//            //preload 될때(전페이지에 있을때)
//        }
    }
}
