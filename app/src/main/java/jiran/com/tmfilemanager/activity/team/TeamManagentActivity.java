package jiran.com.tmfilemanager.activity.team;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.activity.trash.TeamTrashListActivity;
import jiran.com.tmfilemanager.common.AlertCallback;
import jiran.com.tmfilemanager.common.ListFunctionUtils;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.TeamItem;
import jiran.com.tmfilemanager.data.UserItem;
import jiran.com.tmfilemanager.fagment.MultiFragment;
import jiran.com.tmfilemanager.network.JMF_network;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Namo on 2018-02-28.
 */

public class TeamManagentActivity extends AppCompatActivity {

    public final static int REQUEST_CODE_ADD_MEMBER = 10000;
    public final static int REQUEST_CODE_TEAM_FOLDER_OPTION = 10001;
    public final static int REQUEST_CODE_TEAM_MEMBER = 10002;
    public final static int REQUEST_CODE_TRASH_LIST = 10003;

    //add Category Menu
    public static final int SET_EMPTY = 0, SET_VOLUME = -1, SET_JOIN_RULE = -2, SET_JOIN_PROTECT_DOMAIN = -3, SET_FLAG_PUSH = -4, SET_TRASH_OPTION = -5;
    public int CATEGORY_TYPE = SET_EMPTY;
    //(String team_id, String volume, String join_rule, String join_protect_domain, String flag_push, String trash_option)
    TeamItem mTeam;

    LinearLayout wait_layout, wait_content_list_layout;
    LinearLayout member_content_list_layout, add_member_btn;
    TextView team_use_memory_size_tv, team_total_memory_size_tv;
    LinearLayout team_leader_layout;
    LinearLayout push_message_option_btn;
    RelativeLayout find_folder_option_btn;
    TextView push_message_option_tv, find_folder_option_tv;
    RelativeLayout trash_auto_option_btn, trash_size_option_btn;
    TextView trash_auto_option_tv, trash_size_option_tv;

    Dialog mDialog;
    Dialog progressDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_management);

        if (getIntent().getSerializableExtra("TeamItem") != null) {
            mTeam = (TeamItem) getIntent().getSerializableExtra("TeamItem");
        }

        ImageButton title_left_btn = (ImageButton) findViewById(R.id.title_left_btn);
        title_left_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        wait_layout = (LinearLayout) findViewById(R.id.wait_layout);
        wait_content_list_layout = (LinearLayout) findViewById(R.id.wait_content_list_layout);

        member_content_list_layout = (LinearLayout) findViewById(R.id.member_content_list_layout);
        add_member_btn = (LinearLayout) findViewById(R.id.add_member_btn);
        team_leader_layout = (LinearLayout) findViewById(R.id.team_leader_layout);
        if (mTeam.getTeamLeader()) {
            add_member_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(TeamManagentActivity.this, MemberAddActivity.class);
                    intent.putExtra("TeamItem", mTeam);
                    startActivityForResult(intent, REQUEST_CODE_ADD_MEMBER);
                }
            });

            team_leader_layout.setVisibility(View.VISIBLE);
            push_message_option_btn = (LinearLayout) findViewById(R.id.push_message_option_btn);
            push_message_option_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(TeamManagentActivity.this, "준비중...", Toast.LENGTH_SHORT).show();
                    //setPushOption();
                }
            });
            push_message_option_tv = (TextView) findViewById(R.id.push_message_option_tv);
            find_folder_option_btn = (RelativeLayout) findViewById(R.id.find_folder_option_btn);
            find_folder_option_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(TeamManagentActivity.this, TeamFolderOptionActivity.class);
                    intent.putExtra("TeamItem", mTeam);
                    startActivityForResult(intent, REQUEST_CODE_TEAM_FOLDER_OPTION);

                }
            });
            find_folder_option_tv = (TextView) findViewById(R.id.find_folder_option_tv);
            trash_auto_option_btn = (RelativeLayout) findViewById(R.id.trash_auto_option_btn);
            trash_auto_option_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setTrashAutoOption();
                }
            });
            trash_auto_option_tv = (TextView) findViewById(R.id.trash_auto_option_tv);

            trash_size_option_btn = (RelativeLayout) findViewById(R.id.trash_size_option_btn);
            trash_size_option_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(TeamManagentActivity.this, TeamTrashListActivity.class);
                    intent.putExtra("TeamItem", mTeam);
                    startActivityForResult(intent, REQUEST_CODE_TRASH_LIST);
                }
            });
            trash_size_option_tv = (TextView) findViewById(R.id.trash_size_option_tv);
        } else {
            add_member_btn.setVisibility(View.GONE);
            team_leader_layout.setVisibility(View.GONE);
        }

        team_use_memory_size_tv = (TextView) findViewById(R.id.team_use_memory_size_tv);
        team_total_memory_size_tv = (TextView) findViewById(R.id.team_total_memory_size_tv);


        uiUpdate();

//        final ImageView profile_iv = (ImageView) findViewById(R.id.profile_iv);
//        Glide.with(TeamManagentActivity.this).load("https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/26056134_1512623652118681_8894216410971930025_n.jpg?oh=0c96ee55b7f3e4b414a0b3027002793e&oe=5B0E540A").asBitmap().centerCrop().into(new BitmapImageViewTarget(profile_iv) {
//            @Override
//            protected void setResource(Bitmap resource) {
//                RoundedBitmapDrawable circularBitmapDrawable =
//                        RoundedBitmapDrawableFactory.create(getResources(), resource);
//                circularBitmapDrawable.setCircular(true);
//                profile_iv.setImageDrawable(circularBitmapDrawable);
//            }
//        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case MainActivity.REQUEST_CODE_LEAVE_MEMBER_ACTIVITY:
                    setResult(RESULT_OK);
                    finish();
                    break;

                case REQUEST_CODE_TEAM_FOLDER_OPTION:
                    if (data.getSerializableExtra("TeamItem") != null) {
                        mTeam = (TeamItem) data.getSerializableExtra("TeamItem");

                        for (int i = 0; i < MultiFragment.teamItams.size(); i++) {
                            if (mTeam.getId().equals(MultiFragment.teamItams.get(i).getId())) {
                                MultiFragment.teamItams.get(i).setJoinRule(mTeam.getJoinRule());
                                MultiFragment.teamItams.get(i).setJoinProtectDomain(mTeam.getJoinProtectDomain());
                                break;
                            }
                        }
                        uiUpdate();
                    }
                    break;

                case REQUEST_CODE_ADD_MEMBER:
                    getTeamInfo();
                    break;
                case REQUEST_CODE_TEAM_MEMBER:
                    for (int i = 0; i < MultiFragment.teamItams.size(); i++) {
                        if (mTeam.getId().equals(MultiFragment.teamItams.get(i).getId())) {
                            mTeam = MultiFragment.teamItams.get(i);
                            uiUpdate();
                            return;
                        }
                    }
                    setResult(RESULT_OK);
                    finish();
                    break;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void teamMemberListDispaly() {
        member_content_list_layout.removeAllViews();
        wait_content_list_layout.removeAllViews();

        ArrayList<UserItem> waitList = mTeam.getWaitList();

        if (mTeam.getTeamLeader() && waitList != null && waitList.size() > 0) {
            wait_layout.setVisibility(View.VISIBLE);
            for (UserItem wait : waitList) {
                LayoutInflater inflater = LayoutInflater.from(TeamManagentActivity.this);
                View wait_view = inflater.inflate(R.layout.list_wait_member_item, null, false);

                ImageView member_profile_iv = (ImageView) wait_view.findViewById(R.id.member_profile_iv);
                if (wait.getProfileThumb() != null && wait.getProfileThumb().length() > 0) {
                    Glide.with(TeamManagentActivity.this).load(wait.getProfileThumb()).asBitmap().centerCrop().placeholder(R.mipmap.log_user).into(new BitmapImageViewTarget(member_profile_iv) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            member_profile_iv.setImageDrawable(circularBitmapDrawable);
                        }
                    });
                }
                TextView member_name_tv = (TextView) wait_view.findViewById(R.id.member_name_tv);
                member_name_tv.setText(wait.getName());
//                wait_view.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(TeamManagentActivity.this, MemberManageActivity.class);
//                        intent.putExtra("TeamItem", mTeam);
//                        intent.putExtra("UserItem", wait);
//                        startActivity(intent);
//                    }
//                });
                TextView wait_member_confirm = (TextView) wait_view.findViewById(R.id.wait_member_confirm);
                wait_member_confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        waitingMemberConfirm(wait);
                    }
                });

                wait_content_list_layout.addView(wait_view);
            }
        } else {
            wait_layout.setVisibility(View.GONE);
        }

        ArrayList<UserItem> userList = mTeam.getUserList();
//        if(!mTeam.getTeamLeader() && waitList != null && waitList.size() > 0){
//            for(UserItem wait : waitList){
//                userList.add(wait);
//            }
//        }

        for (UserItem user : userList) {
            LayoutInflater inflater = LayoutInflater.from(TeamManagentActivity.this);
            View user_view = inflater.inflate(R.layout.list_team_member_item, null, false);

            ImageView member_profile_iv = (ImageView) user_view.findViewById(R.id.member_profile_iv);
            if (user.getProfileThumb() != null && user.getProfileThumb().length() > 0) {
                Glide.with(TeamManagentActivity.this).load(user.getProfileThumb()).asBitmap().centerCrop().placeholder(R.mipmap.log_user).into(new BitmapImageViewTarget(member_profile_iv) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        member_profile_iv.setImageDrawable(circularBitmapDrawable);
                    }
                });
            }
            TextView member_name_tv = (TextView) user_view.findViewById(R.id.member_name_tv);
            member_name_tv.setText(user.getName());
            user_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(TeamManagentActivity.this, MemberManageActivity.class);
                    intent.putExtra("TeamItem", mTeam);
                    intent.putExtra("UserItem", user);
                    startActivityForResult(intent, REQUEST_CODE_TEAM_MEMBER);
                }
            });
            member_content_list_layout.addView(user_view);
        }
    }

    public void showProgress() {
        progressDialog = Utils.Progress(TeamManagentActivity.this, "", "Login...", false);
        progressDialog.show();
        ;
//        mLoginProgressBar.setVisibility(View.VISIBLE);
//        mLoginProgressBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//            }
//        });
    }

    public void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
//        if(mLoginProgressBar != null){
//            mLoginProgressBar.setVisibility(View.GONE);
//        }
    }

    public void uiUpdate() {
        CATEGORY_TYPE = SET_EMPTY;
        team_use_memory_size_tv.setText(ListFunctionUtils.formatCalculatedSize(mTeam.getVolumeUsage(), 0));
        team_total_memory_size_tv.setText(ListFunctionUtils.formatCalculatedSize(mTeam.getVolume(), 0));

        teamMemberListDispaly();

        if (mTeam.getTeamLeader()) {
            push_message_option_tv.setText(mTeam.getFlagPush().equals("Y") ? getString(R.string.team_manage_setting_message_option1) : getString(R.string.team_manage_setting_message_option2));
            if (mTeam.getJoinRule().equals("private")) {
                find_folder_option_tv.setText(getString(R.string.team_manage_setting_folder_option1));
            } else if (mTeam.getJoinRule().equals("public")) {
                find_folder_option_tv.setText(getString(R.string.team_manage_setting_folder_option2));
            } else {
                find_folder_option_tv.setText(getString(R.string.team_manage_setting_folder_option3));
            }
            if (mTeam.getTrashOption() > 0) {
                trash_auto_option_tv.setText(getString(R.string.team_manage_trash_auto_option1) + mTeam.getTrashOption() + getString(R.string.team_manage_trash_auto_option2));
            } else {
                trash_auto_option_tv.setText(getString(R.string.team_manage_trash_auto_notuse));
            }
            trash_size_option_tv.setText("");
        }

        for (int i = 0; i < MultiFragment.teamItams.size(); i++) {
            if (mTeam.getId().equals(MultiFragment.teamItams.get(i).getId())) {
                MultiFragment.teamItams.get(i).setJoinRule(mTeam.getJoinRule());
                MultiFragment.teamItams.get(i).setJoinProtectDomain(mTeam.getJoinProtectDomain());
                MultiFragment.teamItams.get(i).setFlagPush(mTeam.getFlagPush());
                MultiFragment.teamItams.get(i).setTrashOption(mTeam.getTrashOption());
                break;
            }
        }
    }

    public void setPushOption() {
        mDialog = new Dialog(TeamManagentActivity.this, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(TeamManagentActivity.this);
        View view = inflater.inflate(R.layout.dialog_team_management_push_option, null, false);

        final CheckBox push_receive_cb = (CheckBox) view.findViewById(R.id.push_receive_cb);
        final CheckBox push_missing_cb = (CheckBox) view.findViewById(R.id.push_missing_cb);
        push_receive_cb.setEnabled(false);
        push_receive_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    push_receive_cb.setEnabled(false);
                    push_missing_cb.setEnabled(true);
                    push_missing_cb.setChecked(false);
                } else {

                }
            }
        });
        push_missing_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    push_receive_cb.setEnabled(true);
                    push_missing_cb.setEnabled(false);
                    push_receive_cb.setChecked(false);
                } else {

                }
            }
        });
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //private:비공개, public:공개, protect:조건부 허용
                CATEGORY_TYPE = SET_FLAG_PUSH;
                if (push_receive_cb.isChecked()) {
                    setTeamInfo(-1, "0", "0", mTeam.getId(), 0);
                } else {
                    setTeamInfo(-1, "0", "0", "", 0);
                }
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void setFolderOption() {
        mDialog = new Dialog(TeamManagentActivity.this, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(TeamManagentActivity.this);
        View view = inflater.inflate(R.layout.dialog_team_management_folder_option, null, false);

        final CheckBox folder_private_cb = (CheckBox) view.findViewById(R.id.folder_private_cb);
        final CheckBox folder_public_cb = (CheckBox) view.findViewById(R.id.folder_public_cb);
        final CheckBox folder_protect_cb = (CheckBox) view.findViewById(R.id.folder_protect_cb);
        folder_private_cb.setEnabled(false);
        folder_private_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    folder_private_cb.setEnabled(false);
                    folder_public_cb.setEnabled(true);
                    folder_protect_cb.setEnabled(true);
                    folder_public_cb.setChecked(false);
                    folder_protect_cb.setChecked(false);
                } else {

                }
            }
        });
        folder_public_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    folder_private_cb.setEnabled(true);
                    folder_public_cb.setEnabled(false);
                    folder_protect_cb.setEnabled(true);
                    folder_private_cb.setChecked(false);
                    folder_protect_cb.setChecked(false);
                } else {

                }
            }
        });
        folder_protect_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    folder_private_cb.setEnabled(true);
                    folder_public_cb.setEnabled(true);
                    folder_protect_cb.setEnabled(false);
                    folder_private_cb.setChecked(false);
                    folder_public_cb.setChecked(false);
                } else {

                }
            }
        });
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //private:비공개, public:공개, protect:조건부 허용
                CATEGORY_TYPE = SET_JOIN_RULE;
                if (folder_private_cb.isChecked()) {
                    setTeamInfo(-1, "private", "0", "0", 0);
                } else if (folder_public_cb.isChecked()) {
                    setTeamInfo(-1, "public", "0", "0", 0);
                } else {
                    setTeamInfo(-1, "protect", "0", "0", 0);
                }
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void setTrashAutoOption() {
        mDialog = new Dialog(TeamManagentActivity.this, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(TeamManagentActivity.this);
        View view = inflater.inflate(R.layout.dialog_team_management_trash_auto_option, null, false);

        final CheckBox option_1_cb = (CheckBox) view.findViewById(R.id.option_1_cb);
        final CheckBox option_2_cb = (CheckBox) view.findViewById(R.id.option_2_cb);
        final CheckBox option_3_cb = (CheckBox) view.findViewById(R.id.option_3_cb);
        final CheckBox option_4_cb = (CheckBox) view.findViewById(R.id.option_4_cb);

        TextView option_1 = (TextView) view.findViewById(R.id.option_1);
        option_1.setText(getString(R.string.team_manage_trash_auto_option1) + "1" + getString(R.string.team_manage_trash_auto_option2));
        TextView option_2 = (TextView) view.findViewById(R.id.option_2);
        option_2.setText(getString(R.string.team_manage_trash_auto_option1) + "7" + getString(R.string.team_manage_trash_auto_option2));
        TextView option_3 = (TextView) view.findViewById(R.id.option_3);
        option_3.setText(getString(R.string.team_manage_trash_auto_option1) + "15" + getString(R.string.team_manage_trash_auto_option2));
        TextView option_4 = (TextView) view.findViewById(R.id.option_4);
        option_4.setText(getString(R.string.team_manage_trash_auto_option1) + "30" + getString(R.string.team_manage_trash_auto_option2));

        if (mTeam.getTrashOption() == 1) {
            option_1_cb.setEnabled(false);
            option_2_cb.setEnabled(true);
            option_3_cb.setEnabled(true);
            option_4_cb.setEnabled(true);
            option_1_cb.setChecked(true);
            option_2_cb.setChecked(false);
            option_3_cb.setChecked(false);
            option_4_cb.setChecked(false);
        } else if (mTeam.getTrashOption() == 7) {
            option_1_cb.setEnabled(true);
            option_2_cb.setEnabled(false);
            option_3_cb.setEnabled(true);
            option_4_cb.setEnabled(true);
            option_1_cb.setChecked(false);
            option_2_cb.setChecked(true);
            option_3_cb.setChecked(false);
            option_4_cb.setChecked(false);
        } else if (mTeam.getTrashOption() == 15) {
            option_1_cb.setEnabled(true);
            option_2_cb.setEnabled(true);
            option_3_cb.setEnabled(false);
            option_4_cb.setEnabled(true);
            option_1_cb.setChecked(false);
            option_2_cb.setChecked(false);
            option_3_cb.setChecked(true);
            option_4_cb.setChecked(false);
        } else {
            option_1_cb.setEnabled(true);
            option_2_cb.setEnabled(true);
            option_3_cb.setEnabled(true);
            option_4_cb.setEnabled(false);
            option_1_cb.setChecked(false);
            option_2_cb.setChecked(false);
            option_3_cb.setChecked(false);
            option_4_cb.setChecked(true);
        }

        option_1_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    option_1_cb.setEnabled(false);
                    option_2_cb.setEnabled(true);
                    option_3_cb.setEnabled(true);
                    option_4_cb.setEnabled(true);

                    option_1_cb.setChecked(true);
                    option_2_cb.setChecked(false);
                    option_3_cb.setChecked(false);
                    option_4_cb.setChecked(false);
                } else {

                }
            }
        });
        option_2_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    option_1_cb.setEnabled(true);
                    option_2_cb.setEnabled(false);
                    option_3_cb.setEnabled(true);
                    option_4_cb.setEnabled(true);

                    option_1_cb.setChecked(false);
                    option_2_cb.setChecked(true);
                    option_3_cb.setChecked(false);
                    option_4_cb.setChecked(false);
                } else {

                }
            }
        });
        option_3_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    option_1_cb.setEnabled(true);
                    option_2_cb.setEnabled(true);
                    option_3_cb.setEnabled(false);
                    option_4_cb.setEnabled(true);

                    option_1_cb.setChecked(false);
                    option_2_cb.setChecked(false);
                    option_3_cb.setChecked(true);
                    option_4_cb.setChecked(false);
                } else {

                }
            }
        });
        option_4_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    option_1_cb.setEnabled(true);
                    option_2_cb.setEnabled(true);
                    option_3_cb.setEnabled(true);
                    option_4_cb.setEnabled(false);

                    option_1_cb.setChecked(false);
                    option_2_cb.setChecked(false);
                    option_3_cb.setChecked(false);
                    option_4_cb.setChecked(true);
                } else {

                }
            }
        });
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //private:비공개, public:공개, protect:조건부 허용
                CATEGORY_TYPE = SET_TRASH_OPTION;
                if (option_1_cb.isChecked()) {
                    setTeamInfo(-1, "0", "0", "0", 1);
                } else if (option_2_cb.isChecked()) {
                    setTeamInfo(-1, "0", "0", "0", 7);
                } else if (option_3_cb.isChecked()) {
                    setTeamInfo(-1, "0", "0", "0", 15);
                } else {
                    setTeamInfo(-1, "0", "0", "0", 30);
                }
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    public void setTeamInfo(int volume, String join_rule, String join_protect_domain, String flag_push, int trash_option) {
        try {
            showProgress();
            //(String team_id, String volume, String join_rule, String join_protect_domain, String flag_push, String trash_option)
            JMF_network.ModifyTeamInfo(mTeam.getId(), volume, join_rule, join_protect_domain, flag_push, trash_option)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF ModifyTeamInfo", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    switch (CATEGORY_TYPE) {
                                        case SET_VOLUME:
                                            break;
                                        case SET_JOIN_RULE:
                                            find_folder_option_tv.setText(join_rule);
                                            mTeam.setJoinRule(join_rule);
                                            break;
                                        case SET_JOIN_PROTECT_DOMAIN:
                                            break;
                                        case SET_FLAG_PUSH:
                                            if (flag_push.length() > 0) {
                                                push_message_option_tv.setText(getString(R.string.team_manage_setting_message_option1));
                                                mTeam.setFlagPush("Y");
                                            } else {
                                                push_message_option_tv.setText(getString(R.string.team_manage_setting_message_option2));
                                                mTeam.setFlagPush("N");
                                            }
                                            break;
                                        case SET_TRASH_OPTION:
                                            if (trash_option == 1) {
                                                mTeam.setTrashOption(1);
                                            } else if (trash_option == 7) {
                                                mTeam.setTrashOption(7);
                                            } else if (trash_option == 15) {
                                                mTeam.setTrashOption(15);
                                            } else {
                                                mTeam.setTrashOption(30);
                                            }
                                            break;
                                    }
                                    uiUpdate();
                                } else {
                                    Toast.makeText(TeamManagentActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
            Toast.makeText(TeamManagentActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void getTeamInfo() {
        try {
            showProgress();
            JMF_network.getTeamInfo(mTeam.getId())
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF getTeamInfo", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    JsonObject team = result.getAsJsonObject("team");
                                    mTeam.setMyRole(result.get("my_role").getAsString());
                                    mTeam.setId(team.get("team_id").getAsString());
                                    //mTeam.setUserId(team.get("user_id").getAsString());
                                    //mTeam.setDriveId(team.get("drive_id").getAsString());
                                    mTeam.setTeamName(team.get("name").getAsString());
                                    mTeam.setVolume(team.get("volume").getAsLong());
                                    mTeam.setVolumeUsage(team.get("volume_usage").getAsLong());
                                    mTeam.setJoinRule(team.get("join_rule").getAsString());
                                    mTeam.setJoinProtectDomain(team.get("join_protect_domain").getAsString());
                                    mTeam.setFlagPush(team.get("flag_push").getAsString());
                                    mTeam.setTrashOption(team.get("trash_option").getAsInt());
                                    mTeam.setStatus(team.get("status").getAsInt());
                                    mTeam.setRegdate(Utils.formatDate(team.get("regdate").getAsString()));
                                    mTeam.setModified(Utils.formatDate(team.get("modified").getAsString()));

                                    JsonArray team_member = result.getAsJsonArray("team_member");
                                    ArrayList<UserItem> userItems = new ArrayList<>();
                                    for (int i = 0; i < team_member.size(); i++) {
                                        JsonObject member = team_member.get(i).getAsJsonObject();
                                        UserItem userItem = new UserItem();
                                        userItem.setMemberID(member.get("team_member_id").getAsString());
                                        userItem.setLeader(member.get("role").getAsString().equals("leader") ? true : false);
                                        userItem.setEmail(member.get("email").getAsString());
                                        userItem.setName(member.get("name").getAsString());
                                        userItem.setProfile(member.get("profile").getAsString());
                                        userItem.setProfileThumb(member.get("profile_thumb").getAsString());
                                        //userItem.setGrade(member.get("grade").getAsString());
                                        userItem.setStatus(member.get("status").getAsInt());
                                        userItem.setRegdate(Utils.formatDate(member.get("regdate").getAsString()));
//                                                    userItem.setName("김남오");
//                                                    userItem.setProfile("https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/26056134_1512623652118681_8894216410971930025_n.jpg?oh=0c96ee55b7f3e4b414a0b3027002793e&oe=5B0E540A");
                                        userItems.add(userItem);
                                    }
                                    mTeam.setUserList(userItems);
                                    for (int i = 0; i < MultiFragment.teamItams.size(); i++) {
                                        if (mTeam.getId().equals(MultiFragment.teamItams.get(i).getId())) {
                                            MultiFragment.teamItams.get(i).setUserList(mTeam.getUserList());
                                            break;
                                        }
                                    }
                                    setResult(RESULT_OK);
                                    uiUpdate();
                                } else {
                                    Toast.makeText(TeamManagentActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
            return;
        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
            Toast.makeText(TeamManagentActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void waitingMemberConfirm(UserItem wait) {
        try {
            showProgress();
            JMF_network.waitMemberConfirm(mTeam.getId(), wait.getMemberID())
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF waitMemberConfirm", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    Utils.notiAlert(TeamManagentActivity.this, getString(R.string.member_manage_member_add), new AlertCallback() {
                                        @Override
                                        public void ok(Object teamObj, Object obj) {
                                            for (int i = 0; i < MultiFragment.teamItams.size(); i++) {
                                                if (mTeam.getId().equals(MultiFragment.teamItams.get(i).getId())) {
                                                    for (UserItem item : MultiFragment.teamItams.get(i).getWaitList()) {
                                                        if (item.getMemberID().equals(wait.getMemberID())) {
                                                            MultiFragment.teamItams.get(i).getUserList().add(item);
                                                            MultiFragment.teamItams.get(i).getWaitList().remove(item);
                                                            mTeam = MultiFragment.teamItams.get(i);
                                                            break;
                                                        }
                                                    }
                                                    break;
                                                }
                                            }
                                            uiUpdate();
                                        }

                                        @Override
                                        public void cancle(Object obj) {
                                        }
                                    });
                                } else {
                                    Toast.makeText(TeamManagentActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
            Toast.makeText(TeamManagentActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }
}
