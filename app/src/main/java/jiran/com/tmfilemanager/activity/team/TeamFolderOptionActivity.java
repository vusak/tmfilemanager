package jiran.com.tmfilemanager.activity.team;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.LoginActivity;
import jiran.com.tmfilemanager.activity.MainActivity;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.TeamItem;
import jiran.com.tmfilemanager.network.JMF_network;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Namo on 2018-02-28.
 */

public class TeamFolderOptionActivity extends AppCompatActivity {
    TeamItem mTeam;

    LinearLayout folder_private_layout, folder_public_layout, folder_protect_layout;
    CheckBox folder_private_cb, folder_public_cb, folder_protect_cb, folder_protect_check;
    EditText email_edt;

    LinearLayout bottom_layout;

    Dialog mDialog;
    Dialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder_option);

        if (getIntent().getSerializableExtra("TeamItem") != null) {
            mTeam = (TeamItem) getIntent().getSerializableExtra("TeamItem");
        }


        LinearLayout title_left_btn = (LinearLayout) findViewById(R.id.title_left_btn);
        title_left_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        folder_private_layout = (LinearLayout) findViewById(R.id.folder_private_layout);
        folder_public_layout = (LinearLayout) findViewById(R.id.folder_public_layout);
        folder_protect_layout = (LinearLayout) findViewById(R.id.folder_protect_layout);

        folder_private_cb = (CheckBox) findViewById(R.id.folder_private_cb);
        folder_public_cb = (CheckBox) findViewById(R.id.folder_public_cb);
        folder_protect_cb = (CheckBox) findViewById(R.id.folder_protect_cb);
        email_edt = (EditText) findViewById(R.id.email_edt);


        if (mTeam.getJoinRule().equals("private")) {
            folder_private_layout.setBackgroundResource(R.color.member_join_top_bg);
            folder_public_layout.setBackgroundResource(R.color.white);
            folder_protect_layout.setBackgroundResource(R.color.white);
            folder_private_cb.setEnabled(false);
            folder_public_cb.setEnabled(true);
            folder_protect_cb.setEnabled(true);
            folder_private_cb.setChecked(true);
            folder_public_cb.setChecked(false);
            folder_protect_cb.setChecked(false);
        } else if (mTeam.getJoinRule().equals("public")) {
            folder_private_layout.setBackgroundResource(R.color.white);
            folder_public_layout.setBackgroundResource(R.color.member_join_top_bg);
            folder_protect_layout.setBackgroundResource(R.color.white);
            folder_private_cb.setEnabled(true);
            folder_public_cb.setEnabled(false);
            folder_protect_cb.setEnabled(true);
            folder_private_cb.setChecked(false);
            folder_public_cb.setChecked(true);
            folder_protect_cb.setChecked(false);
        } else {
            folder_private_layout.setBackgroundResource(R.color.white);
            folder_public_layout.setBackgroundResource(R.color.white);
            folder_protect_layout.setBackgroundResource(R.color.member_join_top_bg);
            folder_private_cb.setEnabled(true);
            folder_public_cb.setEnabled(true);
            folder_protect_cb.setEnabled(false);
            folder_private_cb.setChecked(false);
            folder_public_cb.setChecked(false);
            folder_protect_cb.setChecked(true);

            email_edt.setText(mTeam.getJoinProtectDomain());
        }

        folder_private_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    folder_private_cb.setEnabled(false);
                    folder_public_cb.setEnabled(true);
                    folder_protect_cb.setEnabled(true);
                    folder_public_cb.setChecked(false);
                    folder_protect_cb.setChecked(false);

                    email_edt.setEnabled(false);

                    folder_private_layout.setBackgroundResource(R.color.member_join_top_bg);
                    folder_public_layout.setBackgroundResource(R.color.white);
                    folder_protect_layout.setBackgroundResource(R.color.white);
                } else {

                }
            }
        });
        folder_public_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    folder_private_cb.setEnabled(true);
                    folder_public_cb.setEnabled(false);
                    folder_protect_cb.setEnabled(true);
                    folder_private_cb.setChecked(false);
                    folder_protect_cb.setChecked(false);

                    email_edt.setEnabled(false);

                    folder_private_layout.setBackgroundResource(R.color.white);
                    folder_public_layout.setBackgroundResource(R.color.member_join_top_bg);
                    folder_protect_layout.setBackgroundResource(R.color.white);
                } else {

                }
            }
        });
        folder_protect_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    folder_private_cb.setEnabled(true);
                    folder_public_cb.setEnabled(true);
                    folder_protect_cb.setEnabled(false);
                    folder_private_cb.setChecked(false);
                    folder_public_cb.setChecked(false);

                    email_edt.setEnabled(true);

                    folder_private_layout.setBackgroundResource(R.color.white);
                    folder_public_layout.setBackgroundResource(R.color.white);
                    folder_protect_layout.setBackgroundResource(R.color.member_join_top_bg);

                    if (email_edt.getText().toString().length() == 0) {
                        String[] domain = LoginActivity.mUserEmail.split("@");
                        if (domain.length > 1) {
                            email_edt.setText(domain[1]);
                        } else {
                            email_edt.setText("");
                        }
                    }
                } else {

                }
            }
        });

        folder_protect_check = (CheckBox) findViewById(R.id.folder_protect_check);

        bottom_layout = (LinearLayout) findViewById(R.id.bottom_layout);
        bottom_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //private:비공개, public:공개, protect:조건부 허용
                if (folder_private_cb.isChecked()) {
                    setTeamInfo(-1, "private", "0", "0", 0);
                } else if (folder_public_cb.isChecked()) {
                    setTeamInfo(-1, "public", "0", "0", 0);
                } else {
                    String domain = email_edt.getText().toString();
                    if (Utils.isValidEmail("test@" + domain)) {
                        setTeamInfo(-1, "protect", domain.trim(), "0", 0);
                    } else {
                        Toast.makeText(TeamFolderOptionActivity.this, getString(R.string.team_manage_setting_chekc_email), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public void setTeamInfo(int volume, String join_rule, String join_protect_domain, String flag_push, int trash_option) {
        try {
            showProgress();
            //(String team_id, String volume, String join_rule, String join_protect_domain, String flag_push, String trash_option)
            JMF_network.ModifyTeamInfo(mTeam.getId(), volume, join_rule, join_protect_domain, flag_push, trash_option)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF ModifyTeamInfo", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");

                                    mTeam.setJoinRule(join_rule);
                                    mTeam.setJoinProtectDomain(join_protect_domain);
                                    Intent intent = new Intent();
                                    intent.putExtra("TeamItem", mTeam);
                                    setResult(RESULT_OK, intent);

                                    Dialog mDialog = new Dialog(TeamFolderOptionActivity.this, R.style.Theme_TransparentBackground);
                                    LayoutInflater inflater = LayoutInflater.from(TeamFolderOptionActivity.this);
                                    View view = inflater.inflate(R.layout.dialog_base_one_button, null, false);
                                    TextView title = (TextView) view.findViewById(R.id.title);
                                    title.setText(getResources().getString(R.string.dialog_notice));
                                    TextView content = (TextView) view.findViewById(R.id.content);
                                    content.setText(getString(R.string.team_manage_setting_folder_option_modify));
                                    TextView ok = (TextView) view.findViewById(R.id.ok);
                                    ok.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (mDialog != null) {
                                                mDialog.dismiss();
                                                finish();
                                            }
                                        }
                                    });
                                    mDialog.setContentView(view);
                                    mDialog.show();
                                } else {
                                    Toast.makeText(TeamFolderOptionActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
            Toast.makeText(TeamFolderOptionActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case MainActivity.REQUEST_CODE_LEAVE_MEMBER_ACTIVITY:
                    setResult(RESULT_OK);
                    finish();
                    break;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void showProgress() {
        progressDialog = Utils.Progress(TeamFolderOptionActivity.this, "", "Login...", false);
        progressDialog.show();
        ;
//        mLoginProgressBar.setVisibility(View.VISIBLE);
//        mLoginProgressBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//            }
//        });
    }

    public void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
//        if(mLoginProgressBar != null){
//            mLoginProgressBar.setVisibility(View.GONE);
//        }
    }
}
