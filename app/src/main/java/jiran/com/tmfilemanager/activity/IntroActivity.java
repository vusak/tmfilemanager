package jiran.com.tmfilemanager.activity;

import android.Manifest;
import android.animation.Animator;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.gson.JsonObject;

import java.util.Random;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.UserItem;
import jiran.com.tmfilemanager.fagment.MultiFragment;
import jiran.com.tmfilemanager.network.JMF_network;
import jiran.com.tmfilemanager.service.FileObserverService;
import jiran.com.tmfilemanager.settings.MyFileSettings;
import jiran.com.tmfilemanager.utils.PreferenceHelper;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by user on 2016-08-09.
 */
public class IntroActivity extends Activity {

    private final int MY_PERMISSIONS_REQUEST_GET_ACCOUNT = 9999;
    private final int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 9998;
    private final int MY_PERMISSIONS_REQUEST_WRITE_SETTING = 9997;
    private final int MY_PERMISSIONS_REQUEST_KILL_BACKGROUND_PROCESSES = 9996;
    private final int MY_PERMISSIONS_REQUEST_GET_PACKAGE_SIZE = 9995;
    private final int MY_PERMISSIONS_REQUEST_CLEAR_APP_CACHE = 9994;
    private final int MY_PERMISSIONS_REQUEST_NOTIFICATION = 9993;
    private final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 9992;
    private final int INTRO_TIME_2 = 1000;
    private final int INTRO_TIME_3 = 1500;
    private final int INTRO_TIME_4 = 2000;
    private final int PROGRESS_TIME = 50;
    Button startButton;
    LottieAnimationView lottie;
    boolean repeat = false;
    private String path;
    private int intro_time;
    private int progress_time = PROGRESS_TIME;
    private ProgressBar intro_bar;
    private boolean introFinish = false;
    //    private TextView mDone;
    Handler mIntroHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            int per = progress_time * 100 / intro_time;
            intro_bar.setProgress(per);
            progress_time += PROGRESS_TIME;

            if (progress_time > intro_time) {
                introFinish = true;
//                mDone.setVisibility(View.VISIBLE);
                startButton.setVisibility(View.VISIBLE);
            } else {
                sendEmptyMessageDelayed(progress_time, PROGRESS_TIME);
            }
            super.handleMessage(msg);
        }
    };
    private ImageView filemanager_splash_iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        NotificationManager notificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        if (getIntent().getBooleanExtra("NO_ACTION", false)) {
            finish();
            return;
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_intro_baner);
        MyFileSettings.updatePreferences(this);
        MainActivity.path = getIntent().getStringExtra("path");

        if (MyFileSettings.getNewFilesAlram()) {
            startService(new Intent(this, FileObserverService.class));
        }

        if (requestPermission()) {
            if (MainActivity.path != null && MainActivity.path.length() > 0) {
                Handler han = new Handler();
                han.postDelayed(() -> {
                    Intent intent = new Intent(IntroActivity.this, MainActivity.class);
                    if (MainActivity.path != null && MainActivity.path.length() > 0) {
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    }
                    startActivity(intent);
                    finish();
                }, 0);
                return;
            }
        }

        startButton = findViewById(R.id.button_start);
        startButton.setOnClickListener(v -> {
            if (introFinish) {
                if (requestPermission()) {
                    //If permission is already having then showing the toast
                    //Toast.makeText(SplashActivity.this,"You already have the permission",Toast.LENGTH_LONG).show();
                    //Existing the method with return
                    Handler han = new Handler();
                    han.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(IntroActivity.this, MainActivity.class);
                            if (MainActivity.path != null && MainActivity.path.length() > 0) {
                                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            }
                            MultiFragment.teamItams = null;
                            startActivity(intent);
                            finish();
                        }
                    }, 0);
                    return;
                }
            }
        });
        startButton.setVisibility(View.GONE);

        lottie = findViewById(R.id.lottie_intro);
        lottie.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                startButton.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (repeat)
                    lottie.postDelayed(() -> lottie.playAnimation(), 50);
                else {
                    introFinish = true;
                    Animation anim = new AlphaAnimation(0, 1);
                    anim.setDuration(1000);
                    startButton.setAnimation(anim);
                    startButton.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

//        mDone = (TextView) findViewById(R.id.done);

        intro_bar = (ProgressBar) findViewById(R.id.intro_bar);
        intro_bar.setProgress(0);
        //int random = (int) (Math.random() % 3);
        Random random = new Random();
        int ran = random.nextInt(3);

        switch (ran) {
            case 0:
                intro_time = INTRO_TIME_2;
                break;
            case 1:
                intro_time = INTRO_TIME_3;
                break;
            case 2:
                intro_time = INTRO_TIME_4;
                break;
            default:
                intro_time = INTRO_TIME_3;
                break;
        }

        if (PreferenceHelper.getBoolean("auto_login", false)) {
            LoginActivity.mLoginToken = PreferenceHelper.getString("access_token", "");
        } else {
            LoginActivity.mLoginToken = "";
            PreferenceHelper.putString("access_token", "");
            PreferenceHelper.putString("user_name", "");
        }

        if (LoginActivity.mLoginToken != null && LoginActivity.mLoginToken.length() > 0) {
            //checkToken();
            getUserInfo();
            repeat = true;
            lottie.playAnimation();
        } else {
            Handler han = new Handler();
            han.postDelayed(() -> {
                // TODO Auto-generated method stub
//                    mIntroHandler.sendEmptyMessage(progress_time);
                lottie.playAnimation();
            }, PROGRESS_TIME);

        }
    }

    private void checkToken() {
        try {
            JMF_network.checkToken()
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        introFinish = true;
//                        mDone.setVisibility(View.VISIBLE);
                        repeat = false;
                        LoginActivity.mLoginToken = "";
                        PreferenceHelper.putString("access_token", "");
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                if (gigaPodPair.first) {
                                    introFinish = true;
                                    repeat = false;
//                                    mDone.setVisibility(View.VISIBLE);
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    //LoginActivity.mUserId = result.get("user_id").getAsString();
                                    LoginActivity.mUserEmail = result.get("email").getAsString();
                                    LoginActivity.mUserName = PreferenceHelper.getString("user_name", "");
                                    ;
                                } else {
                                    LoginActivity.mLoginToken = "";
                                    PreferenceHelper.putString("access_token", "");
                                    Toast.makeText(IntroActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                introFinish = true;
                                repeat = false;
//                                mDone.setVisibility(View.VISIBLE);
                                LoginActivity.mLoginToken = "";
                                PreferenceHelper.putString("access_token", "");
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(IntroActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    private void getUserInfo() {
        try {
            JMF_network.getUserInfo()
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        introFinish = true;
                        repeat = false;
//                        mDone.setVisibility(View.VISIBLE);
                        LoginActivity.mLoginToken = "";
                        PreferenceHelper.putString("access_token", "");
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                if (gigaPodPair.first) {
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    //LoginActivity.mUserId = result.get("user_id").getAsString();
                                    LoginActivity.mUserEmail = result.get("email").getAsString();
                                    LoginActivity.mUserName = PreferenceHelper.getString("user_name", "");

                                    UserItem userItem = new UserItem();
                                    userItem.setEmail(result.get("email").getAsString());
                                    userItem.setName(result.get("name").getAsString());
                                    userItem.setProfile(result.get("profile").getAsString());
                                    userItem.setProfileThumb(result.get("profile_thumb").getAsString());
                                    userItem.setGrade(result.get("grade").getAsString());
                                    userItem.setStatus(result.get("status").getAsInt());
                                    userItem.setRegdate(Utils.formatDate(result.get("regdate").getAsString()));
                                    LoginActivity.mUserItem = userItem;
                                } else {
                                    LoginActivity.mLoginToken = "";
                                    PreferenceHelper.putString("access_token", "");
                                    Toast.makeText(IntroActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                                introFinish = true;
                                repeat = false;
//                                mDone.setVisibility(View.VISIBLE);
                            },
                            throwable -> {
                                introFinish = true;
                                repeat = false;
//                                mDone.setVisibility(View.VISIBLE);
                                LoginActivity.mLoginToken = "";
                                PreferenceHelper.putString("access_token", "");
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(IntroActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private boolean requestPermission() {
        boolean result = true;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }

        if (!isWriteStorageAllowed()) {
            requestWriteStoragePermission();
            result = false;
        } else if (!isGetAccounted()) {
            requestGetAccountPermission();
            result = false;
        } else if (!isGetAccesFineLocation()) {
            requestGetAccesFineLocation();
            result = false;
        } else if (!isNotification()) {
            requestNotificationPermission();
            result = false;
        }
//        else if(!isWriteSetting()) {
//            requestWriteSettingPermission();
//            result = false;
//        }
//        else if(!isKillBackgroundProcesses()){
//            requestKillBackgroundProcessesPermission();
//            result = false;
//        }else if(!isGetPackage_Size()){
//            requestGetPackage_SizePermission();
//            result = false;
//        }else if(!isClearAppCache()){
//            requestClearAppCachePermission();
//            result = false;
//        }

        return result;
    }

    private boolean isWriteStorageAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    private boolean isGetAccounted() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    private boolean isGetAccesFineLocation() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    private boolean isNotification() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NOTIFICATION_POLICY);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    private boolean isWriteSetting() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.System.canWrite(this)) {
                //Toast.makeText(this, "onCreate: Already Granted", Toast.LENGTH_SHORT).show();
                return true;
            }
        } else {
            return true;
        }
//        //Getting the permission status
//        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_SETTINGS);
//
//        //If permission is granted returning true
//        if (result == PackageManager.PERMISSION_GRANTED)
//            return true;
//
//        //If permission is not granted returning false
        return false;
    }

    private boolean isKillBackgroundProcesses() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.KILL_BACKGROUND_PROCESSES);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    private boolean isGetPackage_Size() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.GET_PACKAGE_SIZE);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    private boolean isClearAppCache() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CLEAR_APP_CACHE);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    //Requesting permission
    private void requestWriteStoragePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
    }

    private void requestGetAccountPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.GET_ACCOUNTS)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.GET_ACCOUNTS}, MY_PERMISSIONS_REQUEST_GET_ACCOUNT);
    }

    private void requestGetAccesFineLocation() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
    }

    private void requestNotificationPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_NOTIFICATION_POLICY)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
//        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_NOTIFICATION_POLICY },MY_PERMISSIONS_REQUEST_NOTIFICATION);

        Intent intent = new Intent(
                android.provider.Settings
                        .ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
        startActivityForResult(intent, MY_PERMISSIONS_REQUEST_NOTIFICATION);
    }

    private void requestWriteSettingPermission() {
        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
        intent.setData(Uri.parse("package:" + this.getPackageName()));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        try {
            startActivityForResult(intent, MY_PERMISSIONS_REQUEST_WRITE_SETTING);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(IntroActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
        }

//        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_SETTINGS)){
//            //If the user has denied the permission previously your code will come to this block
//            //Here you can explain why you need this permission
//            //Explain here why you need this permission
//        }
//
//        //And finally ask for the permission
//        ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.WRITE_SETTINGS},MY_PERMISSIONS_REQUEST_WRITE_SETTING);
    }

    private void requestKillBackgroundProcessesPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.KILL_BACKGROUND_PROCESSES)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.KILL_BACKGROUND_PROCESSES}, MY_PERMISSIONS_REQUEST_KILL_BACKGROUND_PROCESSES);
    }

    private void requestGetPackage_SizePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.GET_PACKAGE_SIZE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.GET_PACKAGE_SIZE}, MY_PERMISSIONS_REQUEST_GET_PACKAGE_SIZE);
    }

    private void requestClearAppCachePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CLEAR_APP_CACHE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CLEAR_APP_CACHE}, MY_PERMISSIONS_REQUEST_CLEAR_APP_CACHE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        //Checking the request code of our request
        if (requestCode == MY_PERMISSIONS_REQUEST_WRITE_STORAGE) {
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requestPermission();
            } else {
                //Displaying another toast if permission is not granted
                //Toast.makeText(this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == MY_PERMISSIONS_REQUEST_GET_ACCOUNT) {
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requestPermission();
            } else {
                //Displaying another toast if permission is not granted
                //Toast.makeText(this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == MY_PERMISSIONS_REQUEST_NOTIFICATION) {
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requestPermission();
            } else {
                //Displaying another toast if permission is not granted
                //Toast.makeText(this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == MY_PERMISSIONS_REQUEST_WRITE_SETTING) {
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requestPermission();
            } else {
                //Displaying another toast if permission is not granted
                //Toast.makeText(this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == MY_PERMISSIONS_REQUEST_KILL_BACKGROUND_PROCESSES) {
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requestPermission();
            } else {
                //Displaying another toast if permission is not granted
                //Toast.makeText(this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == MY_PERMISSIONS_REQUEST_GET_PACKAGE_SIZE) {
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requestPermission();
            } else {
                //Displaying another toast if permission is not granted
                //Toast.makeText(this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == MY_PERMISSIONS_REQUEST_CLEAR_APP_CACHE) {
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requestPermission();
            } else {
                //Displaying another toast if permission is not granted
                //Toast.makeText(this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                requestPermission();
            } else {
                //Displaying another toast if permission is not granted
                //Toast.makeText(this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_PERMISSIONS_REQUEST_WRITE_SETTING && resultCode == RESULT_OK) {
            requestPermission();
        }
    }
}
