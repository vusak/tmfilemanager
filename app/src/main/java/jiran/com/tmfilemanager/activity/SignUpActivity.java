package jiran.com.tmfilemanager.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.data.UserItem;
import jiran.com.tmfilemanager.fagment.MultiFragment;
import jiran.com.tmfilemanager.network.JMF_network;
import jiran.com.tmfilemanager.utils.PreferenceHelper;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Namo on 2018-02-28.
 */

public class SignUpActivity extends AppCompatActivity {

    int STEP_POS = 0;

    LinearLayout join_step_1;
    EditText email_edt;
    TextView already_join;
    LinearLayout join_step_2;
    EditText email_check_edt;
    TextView recieve_email, resend_emailcode_btn;
    LinearLayout join_step_3;
    EditText step3_edt_name, step3_edt_confirm, step3_edt_password;
    TextView step3_edt_email;

    TextView member_leave_btn;

    Dialog mDialog;
    Dialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_join);

        ImageView title_left_btn = (ImageView) findViewById(R.id.title_left_btn);
        title_left_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                joinFinish();
            }
        });

        join_step_1 = (LinearLayout) findViewById(R.id.join_step_1);
        email_edt = (EditText) findViewById(R.id.email_edt);
        already_join = (TextView) findViewById(R.id.already_join);
        already_join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getIntent().getBooleanExtra("LoginActivity", false)) {
                    String email = email_edt.getText().toString().trim();
                    if (email != null && email.length() == 0) {
                        finish();
                    } else {
                        joinFinish();
                    }
                } else {
                    Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                    intent.putExtra("SignUpActivity", true);
                    startActivityForResult(intent, MainActivity.REQUEST_CODE_LOGIN_ACTIVITY);
                }
                //joinFinish();
            }
        });
        join_step_2 = (LinearLayout) findViewById(R.id.join_step_2);
        email_check_edt = (EditText) findViewById(R.id.email_check_edt);
        recieve_email = (TextView) findViewById(R.id.recieve_email);
        resend_emailcode_btn = (TextView) findViewById(R.id.resend_emailcode_btn);
        resend_emailcode_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String email = email_edt.getText().toString().trim();
                    if (email == null || email.length() == 0) {
                        Toast.makeText(SignUpActivity.this, getString(R.string.login_input_email), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    showProgress();
                    JMF_network.resendEmail(email)
                            .flatMap(gigaPodPair -> {
                                dismissProgress();
                                return Observable.just(gigaPodPair);
                            })
                            .doOnError(throwable -> {
                                dismissProgress();
                            })
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(gigaPodPair -> {
                                        dismissProgress();
                                        if (gigaPodPair.first) {
                                            Log.i("JMF_resendEmail", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                            mDialog = new Dialog(SignUpActivity.this, R.style.Theme_TransparentBackground);
                                            LayoutInflater inflater = LayoutInflater.from(SignUpActivity.this);
                                            View view = inflater.inflate(R.layout.dialog_base, null, false);
                                            TextView title = (TextView) view.findViewById(R.id.title);
                                            title.setText(getResources().getString(R.string.dialog_notice));
                                            TextView content = (TextView) view.findViewById(R.id.content);
                                            content.setText(getResources().getString(R.string.dialog_sign_up_resend_emailcode));
                                            TextView cancel = (TextView) view.findViewById(R.id.cancel);
                                            cancel.setVisibility(View.GONE);
                                            TextView ok = (TextView) view.findViewById(R.id.ok);
                                            ok.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    if (mDialog != null) {
                                                        mDialog.dismiss();
                                                    }
                                                }
                                            });
                                            mDialog.setContentView(view);
                                            mDialog.show();
                                        } else {
                                            Toast.makeText(SignUpActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                        }
                                    },
                                    throwable -> {
                                        dismissProgress();
                                        throwable.printStackTrace();
                                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    dismissProgress();
                    Toast.makeText(SignUpActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
                }
            }
        });
        join_step_3 = (LinearLayout) findViewById(R.id.join_step_3);
        step3_edt_email = (TextView) findViewById(R.id.step3_edt_email);
        step3_edt_confirm = (EditText) findViewById(R.id.step3_edt_confirm);
        step3_edt_password = (EditText) findViewById(R.id.step3_edt_password);
        step3_edt_name = (EditText) findViewById(R.id.step3_edt_name);

        member_leave_btn = (TextView) findViewById(R.id.member_leave_btn);
//        member_leave_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(STEP_POS == 0){
//                    STEP_POS = 1;
//                    join_step_1.setVisibility(View.GONE);
//                    join_step_2.setVisibility(View.VISIBLE);
//                    join_step_3.setVisibility(View.GONE);
//                }else if(STEP_POS == 1){
//                    STEP_POS = 2;
//                    join_step_1.setVisibility(View.GONE);
//                    join_step_2.setVisibility(View.GONE);
//                    join_step_3.setVisibility(View.VISIBLE);
//                    member_leave_btn.setText(R.string.member_join_done);
//                }else if(STEP_POS == 2){
//                    finish();
//                }
//            }
//        });


        member_leave_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (STEP_POS == 0) {
                    String email = email_edt.getText().toString().trim();
                    if (email == null || email.length() == 0) {
                        Toast.makeText(SignUpActivity.this, getString(R.string.login_input_email), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    try {
                        showProgress();
                        JMF_network.joinAndSendEmailCode(email, "")
                                .flatMap(gigaPodPair -> {
                                    dismissProgress();
                                    return Observable.just(gigaPodPair);
                                })
                                .doOnError(throwable -> {
                                    dismissProgress();
                                })
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(gigaPodPair -> {
                                            dismissProgress();
                                            if (gigaPodPair.first) {
                                                Log.i("JMF_joinAndSendEmail", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                                STEP_POS = 1;
                                                join_step_1.setVisibility(View.GONE);
                                                join_step_2.setVisibility(View.VISIBLE);
                                                join_step_3.setVisibility(View.GONE);
                                                recieve_email.setText(email);
                                                member_leave_btn.setText(R.string.member_join_next);
                                                JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                                Toast.makeText(SignUpActivity.this, result.getAsString(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(SignUpActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                            }
                                        },
                                        throwable -> {
                                            dismissProgress();
                                            throwable.printStackTrace();
                                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                        dismissProgress();
                        Toast.makeText(SignUpActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
                    }
                } else if (STEP_POS == 1) {
                    String email = email_edt.getText().toString().trim();
                    String email_code = email_check_edt.getText().toString().trim();
                    if (email_code == null || email_code.length() != 4) {
                        Toast.makeText(SignUpActivity.this, getString(R.string.dialog_sign_up_input_4number_code), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    try {
                        showProgress();
                        JMF_network.joinAndSendEmailCode(email, email_code)
                                .flatMap(gigaPodPair -> {
                                    dismissProgress();
                                    return Observable.just(gigaPodPair);
                                })
                                .doOnError(throwable -> {
                                    dismissProgress();
                                })
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(gigaPodPair -> {
                                            dismissProgress();
                                            if (gigaPodPair.first) {
                                                Log.i("JMF_joinAndSendEmail", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                                imm.hideSoftInputFromWindow(email_edt.getWindowToken(), 0);
                                                step3_edt_email.setText(email);
                                                STEP_POS = 2;
                                                join_step_1.setVisibility(View.GONE);
                                                join_step_2.setVisibility(View.GONE);
                                                join_step_3.setVisibility(View.VISIBLE);
                                                member_leave_btn.setText(R.string.member_join_done);
                                            } else {
                                                Toast.makeText(SignUpActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                            }
                                        },
                                        throwable -> {
                                            dismissProgress();
                                            throwable.printStackTrace();
                                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                        dismissProgress();
                        Toast.makeText(SignUpActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
                    }
                } else if (STEP_POS == 2) {
                    try {
                        String email = email_edt.getText().toString().trim();
                        String email_code = email_check_edt.getText().toString().trim();

                        step3_edt_password = (EditText) findViewById(R.id.step3_edt_password);
                        step3_edt_confirm = (EditText) findViewById(R.id.step3_edt_confirm);
                        step3_edt_name = (EditText) findViewById(R.id.step3_edt_name);

                        String password = step3_edt_password.getText().toString().trim();
                        if (password == null || password.length() == 0) {
                            Toast.makeText(SignUpActivity.this, getString(R.string.dialog_sign_up_input_password), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        String repassword = step3_edt_confirm.getText().toString().trim();
                        if (repassword == null || repassword.length() == 0) {
                            Toast.makeText(SignUpActivity.this, getString(R.string.dialog_sign_up_input_password_confirm), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (!password.equals(repassword)) {
                            Toast.makeText(SignUpActivity.this, getString(R.string.dialog_sign_up_password_not_match), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        String name = step3_edt_name.getText().toString().trim();
                        if (name == null || name.length() == 0) {
                            Toast.makeText(SignUpActivity.this, getString(R.string.dialog_sign_up_input_name), Toast.LENGTH_SHORT).show();
                            return;
                        }

                        showProgress();
                        JMF_network.join(email_code, email, password, repassword, name)
                                .flatMap(gigaPodPair -> {
                                    dismissProgress();
                                    return Observable.just(gigaPodPair);
                                })
                                .doOnError(throwable -> {
                                    dismissProgress();
                                })
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(gigaPodPair -> {
                                            dismissProgress();
                                            if (gigaPodPair.first) {
                                                Log.i("JMF_join", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                                mDialog = new Dialog(SignUpActivity.this, R.style.Theme_TransparentBackground);
                                                LayoutInflater inflater = LayoutInflater.from(SignUpActivity.this);
                                                View view = inflater.inflate(R.layout.dialog_base_one_button, null, false);
                                                TextView title = (TextView) view.findViewById(R.id.title);
                                                title.setText(getResources().getString(R.string.dialog_notice));
                                                TextView content = (TextView) view.findViewById(R.id.content);
                                                content.setText(getResources().getString(R.string.dialog_sign_up_success_message));
                                                TextView ok = (TextView) view.findViewById(R.id.ok);
                                                ok.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        if (mDialog != null) {
                                                            mDialog.dismiss();
                                                        }
                                                        requestLogin(email, password);
                                                    }
                                                });
                                                mDialog.setContentView(view);
                                                mDialog.show();
                                            } else {
                                                Toast.makeText(SignUpActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                            }
                                        },
                                        throwable -> {
                                            dismissProgress();
                                            throwable.printStackTrace();
                                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                        dismissProgress();
                        Toast.makeText(SignUpActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void joinFinish() {
        mDialog = new Dialog(SignUpActivity.this, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(SignUpActivity.this);
        View view = inflater.inflate(R.layout.dialog_base, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.dialog_notice));
        TextView content = (TextView) view.findViewById(R.id.content);
        content.setText(getResources().getString(R.string.dialog_sign_up_finish));
        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                finish();
            }
        });
        mDialog.setContentView(view);
        mDialog.show();
    }

    @Override
    public void onBackPressed() {
        String email = email_edt.getText().toString().trim();
        if (STEP_POS == 0 && email != null && email.length() == 0) {
            super.onBackPressed();
        } else {
            joinFinish();
        }
//        if(STEP_POS == 2){
//            STEP_POS = 1;
//            join_step_1.setVisibility(View.GONE);
//            join_step_2.setVisibility(View.VISIBLE);
//            join_step_3.setVisibility(View.GONE);
//            member_leave_btn.setText(R.string.member_join_next);
//        }else if(STEP_POS == 1){
//            STEP_POS = 0;
//            join_step_1.setVisibility(View.VISIBLE);
//            join_step_2.setVisibility(View.GONE);
//            join_step_3.setVisibility(View.GONE);
//            member_leave_btn.setText(R.string.member_join_next);
//        }else{
//            super.onBackPressed();
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case MainActivity.REQUEST_CODE_LOGIN_ACTIVITY:
                    setResult(RESULT_OK);
                    finish();
                    break;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void requestLogin(String email, String password) {
        if (email == null || email.length() == 0) {
            Toast.makeText(SignUpActivity.this, getString(R.string.login_input_email), Toast.LENGTH_SHORT).show();
            return;
        }
        if (password == null || password.length() == 0) {
            Toast.makeText(SignUpActivity.this, getString(R.string.login_input_password), Toast.LENGTH_SHORT).show();
            return;
        }

        showProgress();
        try {
            JMF_network.tokenLogin(email, password)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                if (gigaPodPair.first) {
                                    Log.i("JMF_tokenLogin", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    LoginActivity.mLoginToken = "Bearer " + result.get("access_token").getAsString();
                                    LoginActivity.mUserEmail = email;
                                    PreferenceHelper.putString("access_email", email);
                                    //checkToken();
                                    getUserInfo();
                                } else {
                                    dismissProgress();
                                    finish();
                                    Toast.makeText(SignUpActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgress();
            Toast.makeText(SignUpActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void getUserInfo() {
        try {
            JMF_network.getUserInfo()
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                        PreferenceHelper.putString("access_token", "");
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF_getUserInfo", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    PreferenceHelper.putString("access_token", LoginActivity.mLoginToken);
                                    LoginActivity.mUserName = result.get("name").getAsString();
                                    PreferenceHelper.putString("user_name", LoginActivity.mUserName);

                                    UserItem userItem = new UserItem();
                                    userItem.setEmail(result.get("email").getAsString());
                                    userItem.setName(result.get("name").getAsString());
                                    userItem.setProfile(result.get("profile").getAsString());
                                    userItem.setProfileThumb(result.get("profile_thumb").getAsString());
                                    userItem.setGrade(result.get("grade").getAsString());
                                    userItem.setStatus(result.get("status").getAsInt());
                                    userItem.setRegdate(Utils.formatDate(result.get("regdate").getAsString()));
                                    LoginActivity.mUserItem = userItem;
                                    MultiFragment.teamItams = null;
                                    setResult(RESULT_OK);
                                    finish();
                                } else {
                                    LoginActivity.mLoginToken = "";
                                    Toast.makeText(SignUpActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                PreferenceHelper.putString("access_token", "");
                                throwable.printStackTrace();
                            });
        } catch (Exception e) {
            e.printStackTrace();
            dismissProgress();
            Toast.makeText(SignUpActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void showProgress() {
        progressDialog = Utils.Progress(SignUpActivity.this, "", "Login...", false);
        progressDialog.show();
        ;
//        mLoginProgressBar.setVisibility(View.VISIBLE);
//        mLoginProgressBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//            }
//        });
    }

    public void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
//        if(mLoginProgressBar != null){
//            mLoginProgressBar.setVisibility(View.GONE);
//        }
    }
}
