package jiran.com.tmfilemanager.activity.team;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.LoginActivity;
import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.network.JMF_network;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Namo on 2018-02-28.
 */

public class SearchTeamActivity extends AppCompatActivity {

    EditText team_edt;
    LinearLayout team_contents_list;

    Dialog mDialog;
    Dialog progressDialog;

//    InputFilter filterText = new InputFilter() {
//        public CharSequence filter(CharSequence source, int start, int end,
//                                   Spanned dest, int dstart, int dend) {
//            if(!Utils.teamnamevalidate(source)) {
//                Toast.makeText(SearchTeamActivity.this, getString(R.string.dialog_make_team_toast_info), Toast.LENGTH_SHORT).show();
//                return "";
//            }
//            return null;
//        }
//    };
//    InputFilter filterLength = new InputFilter.LengthFilter(10);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_team);

        LinearLayout title_left_btn = (LinearLayout) findViewById(R.id.title_left_btn);
        title_left_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        team_edt = (EditText) findViewById(R.id.team_edt);

        team_edt.setPrivateImeOptions("defaultInputmode=english;");
        //team_edt.setFilters(new InputFilter[]{filterText, filterLength});

        team_contents_list = (LinearLayout) findViewById(R.id.team_contents_list);


        TextView team_add = (TextView) findViewById(R.id.team_add);
        team_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String edtTeamName = team_edt.getText().toString();

                if (edtTeamName != null && edtTeamName.length() > 0) {
                    searchTeam(edtTeamName);
                } else {
                    Toast.makeText(SearchTeamActivity.this, getString(R.string.dialog_make_team_input_teamname_hint), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void showProgress() {
        progressDialog = Utils.Progress(SearchTeamActivity.this, "", "Login...", false);
        progressDialog.show();
        ;
//        mLoginProgressBar.setVisibility(View.VISIBLE);
//        mLoginProgressBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//            }
//        });
    }

    public void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
//        if(mLoginProgressBar != null){
//            mLoginProgressBar.setVisibility(View.GONE);
//        }
    }

    public void searchTeam(String teamName) {
        try {
            showProgress();
            JMF_network.findTeam(teamName)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF findTeam", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    boolean joinable = result.get("joinable").getAsBoolean();
                                    JsonObject team = result.getAsJsonObject("team");
                                    String team_id = team.get("team_id").getAsString();
                                    String name = team.get("name").getAsString();
                                    String join_rule = team.get("join_rule").getAsString();
                                    String join_protect_domain = team.get("join_protect_domain").getAsString();

                                    JsonObject leader = result.getAsJsonObject("leader");
                                    String role = leader.get("role").getAsString();
                                    JsonObject user = leader.getAsJsonObject("User");
                                    String email = user.get("email").getAsString();
                                    String userName = user.get("name").getAsString();

                                    int count = result.get("count").getAsInt();
                                    boolean is_member = result.get("is_member").getAsBoolean();


                                    team_contents_list.removeAllViews();

                                    if (join_rule.equals("protect") || join_rule.equals("public")) {
                                        LayoutInflater inflater = LayoutInflater.from(SearchTeamActivity.this);
                                        View team_view = inflater.inflate(R.layout.list_team_item, null, false);

                                        TextView team_name_tv = (TextView) team_view.findViewById(R.id.team_name_tv);
                                        team_name_tv.setText(name + " " + getString(R.string.search_team_memory));
                                        TextView team_user_info_tv = (TextView) team_view.findViewById(R.id.team_user_info_tv);

                                        String infoStr = userName + "(" + email + ")";
                                        if (count > 0) {
                                            infoStr += (" " + count + getString(R.string.storage_list_top_person));
                                        }
                                        team_user_info_tv.setText(infoStr);

                                        TextView request_join_team_btn = (TextView) team_view.findViewById(R.id.request_join_team_btn);
                                        TextView signed_up_tv = (TextView) team_view.findViewById(R.id.signed_up_tv);


                                        if (joinable) {
                                            if (join_rule.equals("public")) {
                                                request_join_team_btn.setText(getString(R.string.search_team_request_join));
                                            } else {
                                                request_join_team_btn.setText(getString(R.string.search_team_join));
                                            }
                                            request_join_team_btn.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    if (join_rule.equals("public")) {
                                                        requestJoinEmailDialog(team_id);
                                                    } else {
                                                        requestJoin(team_id, name);
                                                    }
                                                    //Toast.makeText(SearchTeamActivity.this, "Request Join...", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        } else {
                                            request_join_team_btn.setVisibility(View.GONE);
                                            signed_up_tv.setVisibility(View.VISIBLE);
                                            if (is_member) {
                                                signed_up_tv.setText(getString(R.string.search_team_member));
                                            } else {
                                                signed_up_tv.setText(getString(R.string.search_team_member_ready));
                                            }
                                        }
                                        team_contents_list.addView(team_view);
                                    }
                                } else {
                                    team_contents_list.removeAllViews();
                                    String errorCode = (String) gigaPodPair.second;
                                    LayoutInflater inflater = LayoutInflater.from(SearchTeamActivity.this);
                                    if (errorCode != null && errorCode.equals("403")) {
                                        View team_view = inflater.inflate(R.layout.list_team_item_private, null, false);
                                        team_contents_list.addView(team_view);
                                    } else {
                                        View team_view = inflater.inflate(R.layout.list_team_item_no_result, null, false);
                                        team_contents_list.addView(team_view);
                                    }
                                    //Toast.makeText(SearchTeamActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });

        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
            Toast.makeText(SearchTeamActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void requestJoinEmail(String team_id, String memo) {
        try {
            showProgress();
            JMF_network.joinTeamSendEmail(team_id, memo)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF joinTeamSendEmail", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    Toast.makeText(SearchTeamActivity.this, getString(R.string.search_team_request_join_complete), Toast.LENGTH_SHORT).show();
                                    setResult(RESULT_OK);
                                    finish();
                                } else {
                                    Toast.makeText(SearchTeamActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });

        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
            Toast.makeText(SearchTeamActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }

    public void requestJoinEmailDialog(String team_id) {
        mDialog = new Dialog(SearchTeamActivity.this, R.style.Theme_TransparentBackground);
        LayoutInflater inflater = LayoutInflater.from(SearchTeamActivity.this);
        View view = inflater.inflate(R.layout.dialog_reqeust_join_team, null, false);

        TextView sender_email_tv = (TextView) view.findViewById(R.id.sender_email_tv);
        sender_email_tv.setText(LoginActivity.mUserEmail);
        EditText sender_message_edt = (EditText) view.findViewById(R.id.sender_message_edt);


        TextView cancel = (TextView) view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                requestJoinEmail(team_id, sender_message_edt.getText().toString());
            }
        });

        mDialog.setContentView(view);
        mDialog.show();
    }

    public void requestJoin(String team_id, String name) {
        try {
            showProgress();
            JMF_network.joinTeam(team_id)
                    .flatMap(gigaPodPair -> {
                        return Observable.just(gigaPodPair);
                    })
                    .doOnError(throwable -> {
                        dismissProgress();
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(gigaPodPair -> {
                                dismissProgress();
                                if (gigaPodPair.first) {
                                    Log.i("JMF joinTeamSendEmail", "=>=>" + ((JsonObject) gigaPodPair.second).toString());
                                    JsonObject result = ((JsonObject) gigaPodPair.second).getAsJsonObject("result");
                                    mDialog = new Dialog(SearchTeamActivity.this, R.style.Theme_TransparentBackground);
                                    LayoutInflater inflater = LayoutInflater.from(SearchTeamActivity.this);
                                    View view = inflater.inflate(R.layout.dialog_base_one_button, null, false);
                                    TextView title = (TextView) view.findViewById(R.id.title);
                                    title.setText(getResources().getString(R.string.dialog_notice));
                                    TextView content = (TextView) view.findViewById(R.id.content);
                                    content.setText(name + getString(R.string.search_team_join_complete));
                                    TextView ok = (TextView) view.findViewById(R.id.ok);
                                    ok.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (mDialog != null) {
                                                mDialog.dismiss();
                                            }
                                            setResult(RESULT_OK);
                                            finish();
                                        }
                                    });
                                    mDialog.setCancelable(false);
                                    mDialog.setContentView(view);
                                    mDialog.show();
                                    //Toast.makeText(SearchTeamActivity.this, getString(R.string.search_team_join_complete), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(SearchTeamActivity.this, (String) gigaPodPair.second, Toast.LENGTH_SHORT).show();
                                }
                            },
                            throwable -> {
                                dismissProgress();
                                throwable.printStackTrace();
                            });

        } catch (Exception e) {
            dismissProgress();
            e.printStackTrace();
            Toast.makeText(SearchTeamActivity.this, "Sever URL Error...", Toast.LENGTH_SHORT).show();
        }
    }
}
