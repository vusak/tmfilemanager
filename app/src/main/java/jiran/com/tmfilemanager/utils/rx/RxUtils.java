package jiran.com.tmfilemanager.utils.rx;

/**
 * Created by user on 2015-07-08.
 */

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.functions.Func1;

public class RxUtils {

    public static class RetryWithDelay
            implements Func1<Observable<? extends Throwable>, Observable<?>> {

        private final int _maxRetries;
        private final int _retryDelayMillis;
        private int _retryCount;

        public RetryWithDelay(final int maxRetries, final int retryDelayMillis) {
            _maxRetries = maxRetries;
            _retryDelayMillis = retryDelayMillis;
            _retryCount = 0;
        }

        @Override
        public Observable<?> call(Observable<? extends Throwable> attempts) {
            return attempts.flatMap(new Func1<Throwable, Observable<?>>() {
                @Override
                public Observable<?> call(Throwable throwable) {
                    if (++_retryCount < _maxRetries) {
                        // When this Observable calls onNext, the original
                        // Observable will be retried (i.e. re-subscribed).

                        return Observable.timer(_retryCount * _retryDelayMillis,
                                TimeUnit.MILLISECONDS);
                    }

                    return Observable.error(throwable);
                }
            });
        }
    }
}
