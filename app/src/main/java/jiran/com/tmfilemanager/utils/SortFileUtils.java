package jiran.com.tmfilemanager.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import jiran.com.tmfilemanager.activity.LoginActivity;
import jiran.com.tmfilemanager.data.FileItem;
import jiran.com.tmfilemanager.data.TeamItem;
import jiran.com.tmfilemanager.data.UserItem;
import jiran.com.tmfilemanager.settings.MyFileSettings;

public class SortFileUtils {

    public static final int SORT_ALPHA = 0;
    public static final int SORT_TYPE = 1;
    public static final int SORT_SIZE = 2;
    public static final int SORT_DATE = 3;
    private static final Comparator<? super FileItem> Comparator_ALPH = new Comparator<FileItem>() {

        @Override
        public int compare(FileItem arg0, FileItem arg1) {
            File a = new File(arg0.getPath());
            File b = new File(arg1.getPath());

            String first = "";
            String second = "";
            first = arg0.getName().startsWith(".") ? arg0.getName().replaceFirst(".", "") : arg0.getName();
            second = arg1.getName().startsWith(".") ? arg1.getName().replaceFirst(".", "") : arg1.getName();

            if (MyFileSettings.getSortReverse()) {
                return first.toLowerCase().compareTo(second.toLowerCase());
            } else {
                return -first.toLowerCase().compareTo(second.toLowerCase());
            }

        }
    };
    private final static Comparator<? super FileItem> Comparator_SIZE = new Comparator<FileItem>() {

        @Override
        public int compare(FileItem arg0, FileItem arg1) {
            int check = 0;
            if (MyFileSettings.getSortReverse()) {
                check = 1;
            } else {
                check = -1;
            }


            final long lenA = Long.getLong(arg0.getSize());
            final long lenB = Long.getLong(arg1.getSize());

            if (lenA == lenB) {
                String first = "";
                String second = "";
                first = arg0.getName().startsWith(".") ? arg0.getName().replaceFirst(".", "") : arg0.getName();
                second = arg1.getName().startsWith(".") ? arg1.getName().replaceFirst(".", "") : arg1.getName();

                return -first.toLowerCase().compareTo(second.toLowerCase()) * check;
            }

            if (lenA < lenB) {
                return -1 * check;
            }
            return 1 * check;
        }
    };
    private final static Comparator<? super FileItem> Comparator_TYPE = new Comparator<FileItem>() {

        @Override
        public int compare(FileItem arg0, FileItem arg1) {
            int check = 0;
            if (MyFileSettings.getSortReverse()) {
                check = 1;
            } else {
                check = -1;
            }

            final String extA = getExtension(arg0.getName());
            final String extB = getExtension(arg1.getName());

            if (extA.isEmpty() && extB.isEmpty()) {
                String first = "";
                String second = "";
                first = arg0.getName().startsWith(".") ? arg0.getName().replaceFirst(".", "") : arg0.getName();
                second = arg1.getName().startsWith(".") ? arg1.getName().replaceFirst(".", "") : arg1.getName();

                return -first.toLowerCase().compareTo(second.toLowerCase()) * check;
            }

            if (extA.isEmpty()) {
                return -1 * check;
            }

            if (extB.isEmpty()) {
                return 1 * check;
            }

            final int res = extA.compareTo(extB);
            if (res == 0) {
                String first = "";
                String second = "";
                first = arg0.getName().startsWith(".") ? arg0.getName().replaceFirst(".", "") : arg0.getName();
                second = arg1.getName().startsWith(".") ? arg1.getName().replaceFirst(".", "") : arg1.getName();

                return first.toLowerCase().compareTo(second.toLowerCase()) * check;
            }
            return res * check;
        }
    };
    private final static Comparator<? super FileItem> Comparator_DATE = new Comparator<FileItem>() {

        @Override
        public int compare(FileItem arg0, FileItem arg1) {
            String first = arg0.getSModifedDate();
            String second = arg1.getSModifedDate();

            if (MyFileSettings.getSortReverse()) {
                return -first.compareTo(second);
            } else {
                return first.compareTo(second);
            }

        }
    };
    private final static Comparator<? super UserItem> Comparator_LEADER = new Comparator<UserItem>() {

        @Override
        public int compare(UserItem arg0, UserItem arg1) {
            if (arg0.getLeader()) {
                return -1;
            }

            if (arg1.getLeader()) {
                return 1;
            }

            if (arg1.getEmail().equals(LoginActivity.mUserItem.getEmail())) {
                return 1;
            }

            return 0;

        }
    };
    private final static Comparator<? super TeamItem> Comparator_TEAM = new Comparator<TeamItem>() {

        @Override
        public int compare(TeamItem arg0, TeamItem arg1) {
            if (arg1.getTeamLeader()) {
                return 1;
            }

            return -1;

        }
    };

    private SortFileUtils() {
    }

    public static void sortList(ArrayList<FileItem> content,
                                String current, int sort_type) {
        int len = content != null ? content.size() : 0;

        if (len == 0)
            return;

        int index = 0;
        FileItem[] items = new FileItem[len];
        content.toArray(items);

        //switch (MyFileSettings.getSortType()) {
        switch (sort_type) {
            case SORT_ALPHA:
                Arrays.sort(items, Comparator_ALPH);
                content.clear();

                Collections.addAll(content, items);
                break;
            case SORT_SIZE:
                Arrays.sort(items, Comparator_SIZE);
                content.clear();

                Collections.addAll(content, items);
//                for (FileItem a : items) {
//                    if (current.length() > 0) {
//                        if (new File(current + "/" + a.getName()).isDirectory())
//                            content.add(index++, a);
//                        else
//                            content.add(a);
//                    } else {
//                        if (a.isDirectory())
//                            content.add(index++, a);
//                        else
//                            content.add(a);
//                    }
//                }
                break;
            case SORT_TYPE:
                Arrays.sort(items, Comparator_TYPE);
                content.clear();

                Collections.addAll(content, items);
//                for (FileItem a : items) {
//                    if (new File(current + "/" + a.getName()).isDirectory())
//                        content.add(index++, a);
//                    else
//                        content.add(a);
//                }
                break;

            case SORT_DATE:
                Arrays.sort(items, Comparator_DATE);
                content.clear();

                Collections.addAll(content, items);
//                for (FileItem a : items) {
//                    if (new File(current + "/" + a.getName()).isDirectory())
//                        content.add(index++, a);
//                    else
//                        content.add(a);
//                }
                break;
        }

//        if (MyFileSettings.reverseListView()) {
//            Collections.reverse(content);
//        }
    }

    public static String getExtension(String name) {
        String ext;

        if (name.lastIndexOf(".") == -1) {
            ext = "";

        } else {
            int index = name.lastIndexOf(".");
            ext = name.substring(index + 1, name.length());
        }
        return ext;
    }

    public static void sortUser(ArrayList<UserItem> content) {
        int len = content != null ? content.size() : 0;

        if (len == 0)
            return;

        int index = 0;
        UserItem[] items = new UserItem[len];
        content.toArray(items);

        Arrays.sort(items, Comparator_LEADER);
        content.clear();

        Collections.addAll(content, items);
    }

    public static void sortTeam(ArrayList<TeamItem> content) {
        int len = content != null ? content.size() : 0;

        if (len == 0)
            return;

        int index = 0;
        TeamItem[] items = new TeamItem[len];
        content.toArray(items);

        Arrays.sort(items, Comparator_TEAM);
        content.clear();

        Collections.addAll(content, items);
    }
}