package jiran.com.tmfilemanager.utils;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import java.util.List;

public final class Util {
    public static String PLAYSTORE_URL = "https://play.google.com/store/apps/details?id=";
    public static String PACKAGE_TM_BROWSER = "io.jmobile.tm.browser";

    public static void launchApp(Activity activity, String packageName) {
        try {
            if (getPackageList(activity, packageName)) {
                Intent intent = activity.getPackageManager().getLaunchIntentForPackage(packageName);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
            } else {
                Uri uri = Uri.parse(PLAYSTORE_URL + packageName);
                Intent i = new Intent(Intent.ACTION_VIEW, uri);
                activity.startActivity(i);
            }
        } catch (Exception e) {
            Uri uri = Uri.parse(PLAYSTORE_URL + packageName);
            Intent i = new Intent(Intent.ACTION_VIEW, uri);
            activity.startActivity(i);
        }
    }

    public static boolean getPackageList(Activity activity, String packageName) {
        boolean isExist = false;

        PackageManager manager = activity.getPackageManager();
        List<ResolveInfo> apps;
        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        apps = manager.queryIntentActivities(intent, 0);

        try {
            for (int i = 0; i < apps.size(); i++) {
                if (apps.get(i).activityInfo.packageName.startsWith(packageName)) {
                    isExist = true;
                    break;
                }
            }
        } catch (Exception e) {
            LogUtil.log(e.getMessage());
            isExist = false;
        }

        return isExist;
    }
}
