package jiran.com.tmfilemanager.utils;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import jiran.com.tmfilemanager.MyApplication;


public class PreferenceHelper {

    public static boolean AD_FLAG = true;


    public static String jmf_ip = "JMF_IP";

    public static String jmf_ssl = "JMF_SSL";

    public static String PROPERTY_DEVICE_ID = "PROPERTY_DEVICE_ID";
    public static String id = "ID";
    public static String password = "PAWD";

    public static SharedPreferences getDefaultPreferences() {
        return MyApplication.getInstance().getSharedPreferences("AppPreferences.xml", Activity.MODE_MULTI_PROCESS);
    }


    public static boolean putString(String key, String value) {
        SharedPreferences sp = getDefaultPreferences();
        Editor editor = sp.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    public static boolean putInt(String key, int value) {
        SharedPreferences sp = getDefaultPreferences();
        Editor editor = sp.edit();
        editor.putInt(key, value);
        return editor.commit();
    }

    public static boolean putLong(String key, long value) {
        SharedPreferences sp = getDefaultPreferences();
        Editor editor = sp.edit();
        editor.putLong(key, value);
        return editor.commit();
    }

    public static boolean remove(String key) {
        SharedPreferences sp = getDefaultPreferences();
        Editor editor = sp.edit();
        editor.remove(key);
        return editor.commit();
    }

    public static boolean putBoolean(String key, boolean value) {
        SharedPreferences sp = getDefaultPreferences();
        Editor editor = sp.edit();
        editor.putBoolean(key, value);
        return editor.commit();
    }

    public static String getString(String key, String defValue) {
        SharedPreferences sp = getDefaultPreferences();
        return sp.getString(key, defValue);
    }

    public static int getInt(String key, int defValue) {
        SharedPreferences sp = getDefaultPreferences();
        return sp.getInt(key, defValue);
    }

    public static long getLong(String key, long defValue) {
        SharedPreferences sp = getDefaultPreferences();
        return sp.getLong(key, defValue);
    }

    public static boolean getBoolean(String key, boolean defValue) {
        SharedPreferences sp = getDefaultPreferences();
        return sp.getBoolean(key, defValue);
    }

    public static boolean contains(String key) {
        SharedPreferences sp = getDefaultPreferences();
        return sp.contains(key);
    }

    public static boolean putDefaultBoolean(String key, boolean value) {
        SharedPreferences sp = getDefaultPreferences();
        if (!sp.contains(key)) {
            return putBoolean(key, value);
        }
        return false;
    }


    public static SharedPreferences getSharedPreferences() {
        return MyApplication.getInstance().getSharedPreferences("MobilePOD.Shared", Activity.MODE_MULTI_PROCESS | Activity.MODE_WORLD_READABLE);
    }

    public static boolean putSharedString(String key, String value) {
        SharedPreferences sp = getSharedPreferences();
        Editor editor = sp.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    public static String getSharedString(String key, String defValue) {
        SharedPreferences sp = getSharedPreferences();
        return sp.getString(key, defValue);
    }
}