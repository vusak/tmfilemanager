package jiran.com.tmfilemanager;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import java.io.File;

import jiran.com.flyingfile.util.SharedPreferenceUtil;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class MyApplication extends android.support.multidex.MultiDexApplication {
    public static final int MAIN_TAB_VIEW_ID_FILEVIEW = 1;
    public static final int MAIN_TAB_VIEW_ID_CLOUD = 3;
    //----------------------------------------------------///
    //  Added FlyingFile
    public static final int COLORMODE_LIME = 1;
    public static final int COLORMODE_BLUE = 2;
    //Level
    public static String Owner = "owner";
    public static String Admin = "admin";
    public static String Editor = "editor";
    public static String Viewer = "viewer";

    public static String NOTIFICATION_CHANNEL_ID = "tmfm_upload_s";

    public static boolean DEBUG = false;
    public static int MAIN_TAB_CURRENT_MODE = -1;
    private static MyApplication sInstance;

    static {
        cupboard().register(com.jiransoft.officedisplay.models.downloads.DownloadManga.class);
        cupboard().register(com.jiransoft.officedisplay.models.downloads.DownloadChapter.class);
        cupboard().register(com.jiransoft.officedisplay.models.downloads.DownloadPage.class);
        cupboard().register(com.jiransoft.officedisplay.models.downloads.DownloadQueue.class);
    }

    private String strCurrentPCPathForFileTransfer = null;
    private String strCurrentMobilePath = null;
    private String strRemotePCExplorerPath = null;
    /**
     * 에이젼트에서 파일송수신 및 파일리스트 송수신 작업요청을 취소 한다.
     */
    private boolean fileListSendable = true;
    private int nCurrentMobileTab = 0;

    public MyApplication() {
        sInstance = this;
    }

    public static synchronized MyApplication getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.DEBUG = isDebuggable(this);
//		initializePreferences();
        createNotificationChannel();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();


    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);

    }

    /**
     * get Debug Mode
     *
     * @param context
     * @return
     */
    private boolean isDebuggable(Context context) {
        boolean debuggable = false;

        PackageManager pm = context.getPackageManager();
        try {
            ApplicationInfo appinfo = pm.getApplicationInfo(context.getPackageName(), 0);
            debuggable = (0 != (appinfo.flags & ApplicationInfo.FLAG_DEBUGGABLE));
        } catch (PackageManager.NameNotFoundException e) {
            /* debuggable variable will remain false */
        }

        return debuggable;
    }

    public String getStrCurrentPCPathForFileTransfer() {
        return strCurrentPCPathForFileTransfer;
    }

    public void setStrCurrentPCPathForFileTransfer(
            String strCurrentPCPathForFileTransfer) {
        this.strCurrentPCPathForFileTransfer = strCurrentPCPathForFileTransfer;
    }

    public String getStrCurrentMobilePath() {
        if (strCurrentMobilePath == null)
            return getReceivedStorage(this);
        else
            return strCurrentMobilePath;
    }

    public void setStrCurrentMobilePath(String strCurrentMobilePath) {
        this.strCurrentMobilePath = strCurrentMobilePath;
    }

    public String getWIFIDirectSaveFileFolderPath() {
        String[] secret = {"Application", "TMFileManager", "WIFIDirectFileFolder"};
        String secretPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        for (String path : secret) {
            secretPath += File.separator + path;
            File file = new File(secretPath);
            if (!file.exists()) {
                file.mkdir();
            }
        }
        return secretPath;
    }

    public synchronized String getReceivedStorage(Context context) {

        String strDefaultPath = SharedPreferenceUtil.getInstance().getMobileDefaultPath(context);

        Log.d("ReceivePath", "getReceivedStorage strDefaultPath : " + strDefaultPath);

        if (strDefaultPath == null) {
            //String path = Environment.getExternalStorageDirectory().getAbsolutePath()+"/FileSender";
            String path = getWIFIDirectSaveFileFolderPath();
            Log.d("ReceivePath", "getReceivedStorage path : " + path);
            return path;
        } else {
            return strDefaultPath;
        }
    }

    public void setReceivedStorage(Context context, String strPath) {
        Log.d("ReceivePath", "setReceivedStorage" + strPath);
    }

    public synchronized String getDefaultStorage() {
        //String path =  Environment.getExternalStorageDirectory().getAbsolutePath()+"/FileSender";
        String path = getWIFIDirectSaveFileFolderPath();
        Log.d("ReceivePath", "getDefaultStorage" + path);
        return path;
    }

    public String getStrRemotePCExplorerPath() {
        return strRemotePCExplorerPath;
    }

    public void setStrRemotePCExplorerPath(String strRemotePCExplorerPath) {
        this.strRemotePCExplorerPath = strRemotePCExplorerPath;
    }

    public boolean isFileListSendable() {
        return fileListSendable;
    }

    public void setFileListSendable(boolean fileListSendable) {
        this.fileListSendable = fileListSendable;
    }

    public int getCurrentMobileTab() {
        // TODO Auto-generated method stub
        return nCurrentMobileTab;
    }

    public void setCurrentMobileTab(int nMobileTab) {
        this.nCurrentMobileTab = nMobileTab;
    }

    //----------------------------------------------------///

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "TMFM File Upload State";
            String description = "You can see the progress of the file upload.";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
