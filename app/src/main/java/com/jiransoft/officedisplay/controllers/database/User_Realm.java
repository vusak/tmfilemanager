package com.jiransoft.officedisplay.controllers.database;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by user on 2015-10-23.
 */
public class User_Realm extends RealmObject {
    @PrimaryKey
    @Required
    private String email;
    private String nick_name;
    private long storage_used;
    private long storage_limit;
    private String profile_thumb;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public long getStorage_used() {
        return storage_used;
    }

    public void setStorage_used(long storage_used) {
        this.storage_used = storage_used;
    }

    public long getStorage_limit() {
        return storage_limit;
    }

    public void setStorage_limit(long storage_limit) {
        this.storage_limit = storage_limit;
    }

    public String getProfile_thumb() {
        return profile_thumb;
    }

    public void setProfile_thumb(String profile_thumb) {
        this.profile_thumb = profile_thumb;
    }

}
