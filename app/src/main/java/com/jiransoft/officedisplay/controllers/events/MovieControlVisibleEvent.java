package com.jiransoft.officedisplay.controllers.events;

public class MovieControlVisibleEvent {
    public static final int FLAG_VISIBLE = -200;
    public static final int FLAG_INVISIBE = -100;
    private int mEvent;

    public MovieControlVisibleEvent(int event) {
        mEvent = event;
    }

    public int getmEvent() {
        return mEvent;
    }

}
