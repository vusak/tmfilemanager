package com.jiransoft.officedisplay.controllers.databases;

public class ApplicationContract {
    public static final String DATABASE_NAME = "AizobanApplication.db";
    public static final int DATABASE_VERSION = 4;

    private ApplicationContract() {
        throw new AssertionError();
    }

    public static final class DownloadChapter {
        public static final String TABLE_NAME = "DownloadChapter";

        public static final String COLUMN_ID = "_id";

        public static final String COLUMN_SOURCE = "Source";
        public static final String COLUMN_URL = "Url";
        public static final String COLUMN_PARENT_URL = "ParentUrl";

        public static final String COLUMN_NAME = "Name";

        public static final String COLUMN_DIRECTORY = "Directory";

        public static final String COLUMN_CURRENT_PAGE = "CurrentPage";
        public static final String COLUMN_TOTAL_PAGES = "TotalPages";
        public static final String COLUMN_FLAG = "Flag";

        private DownloadChapter() {
            throw new AssertionError();
        }
    }

    public static final class DownloadManga {
        public static final String TABLE_NAME = "DownloadManga";

        public static final String COLUMN_ID = "_id";

        public static final String COLUMN_SOURCE = "Source";
        public static final String COLUMN_URL = "Url";

        public static final String COLUMN_ARTIST = "Artist";
        public static final String COLUMN_AUTHOR = "Author";
        public static final String COLUMN_DESCRIPTION = "Description";
        public static final String COLUMN_GENRE = "Genre";
        public static final String COLUMN_NAME = "Name";
        public static final String COLUMN_COMPLETED = "Completed";
        public static final String COLUMN_THUMBNAIL_URL = "ThumbnailUrl";

        private DownloadManga() {
            throw new AssertionError();
        }
    }

    public static final class DownloadPage {
        public static final String TABLE_NAME = "DownloadPage";

        public static final String COLUMN_ID = "_id";

        public static final String COLUMN_SOURCE = "Source";
        public static final String COLUMN_URL = "Url";
        public static final String COLUMN_PARENT_URL = "ParentUrl";

        public static final String COLUMN_NAME = "Name";
        public static final String COLUMN_EXT = "Ext";

        public static final String COLUMN_DIRECTORY = "Directory";

        public static final String COLUMN_FLAG = "Flag";

        public static final String COLUMN_COMMENT = "Comment";
        public static final String COLUMN_MODIFIED = "Modified";

        private DownloadPage() {
            throw new AssertionError();
        }
    }
}
