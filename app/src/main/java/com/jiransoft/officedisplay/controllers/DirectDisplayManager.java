package com.jiransoft.officedisplay.controllers;

import android.content.ContentValues;
import android.database.Cursor;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.jiransoft.officedisplay.controllers.databases.ApplicationContract;
import com.jiransoft.officedisplay.controllers.events.DownloadChapterQueryEvent;
import com.jiransoft.officedisplay.controllers.factories.DirectFodlerSourceFactory;
import com.jiransoft.officedisplay.controllers.wrappers.DirectFolderRequestWrapper;
import com.jiransoft.officedisplay.models.ImageModel;
import com.jiransoft.officedisplay.models.downloads.DownloadChapter;
import com.jiransoft.officedisplay.models.downloads.DownloadPage;
import com.jiransoft.officedisplay.network.DownloadService_OfficeBox;
import com.jiransoft.officedisplay.utils.DiskUtils;
import com.jiransoft.officedisplay.utils.Dlog;
import com.jiransoft.officedisplay.utils.DownloadUtils;
import com.jiransoft.officedisplay.utils.Utils;

import java.io.File;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import de.greenrobot.event.EventBus;
import jiran.com.tmfilemanager.fagment.StorageFragment;
import jiran.com.tmfilemanager.network.JMF_RestfulAdapter;
import okhttp3.Response;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class DirectDisplayManager {
    public static int DirectFolder = 1;
    //public static int DirectCloudBox = 2;
    public static int OfficeBox = 3;
    //public static int GigaPod = 4;


    private DirectDisplayManager() {
        throw new AssertionError();
    }

    public static Observable<ImageModel> pullImageUrlsFromNetwork(final DirectFolderRequestWrapper request) {
        return DirectFodlerSourceFactory.constructSourceFromName(request.getSource()).pullImageUrlsFromNetwork(request).onBackpressureBuffer();

    }

    public static Observable<Integer> downloadChapterFromNetwork(final DownloadChapter downloadChapter) {
        final DirectFolderRequestWrapper downloadRequest = new DirectFolderRequestWrapper(downloadChapter.getSource(), downloadChapter.getUrl(), "");

        final AtomicBoolean isUnsubscribed = new AtomicBoolean(false);

        // 1. 리스트를 받아온다.
        return DirectDisplayManager
                .pullImageUrlsFromNetwork(downloadRequest)
                .subscribeOn(Schedulers.io())
                .toList()
                .flatMap(imageModels -> {
                    //기존에 있던 다운로드 리스트를 모두 FLAG_DELETE 상태로 바꿈
                    ContentValues pageValues = new ContentValues(1);
                    pageValues.put(ApplicationContract.DownloadPage.COLUMN_FLAG, DownloadUtils.FLAG_DELETE);
                    int updatecnt = QueryManager.updateDownloadPage(downloadRequest, pageValues).toBlocking().single();
                    Dlog.d("downloadChapterFromNetwork: updatecnt : " + updatecnt);
                    // 새로 다운로드 리스트를 더한다.
                    return QueryManager.addDownloadPagesForDownloadChapter(downloadChapter, imageModels);
                })
                .flatMap(downloadPages -> {
                    Dlog.d("downloadChapterFromNetwork: downloadPages.size : " + downloadPages.size());
                    //리스트에서 완료된 갯수를 찾는다.
                    int Completed_cnt = 0;
                    for (DownloadPage downloadPage : downloadPages) {
                        if (downloadPage.getFlag() == DownloadUtils.FLAG_COMPLETED) {
                            Completed_cnt++;
                        }
                    }
                    Dlog.d("downloadChapterFromNetwork: downloadPages.size : " + downloadPages.size());
                    Dlog.d("downloadChapterFromNetwork: Completed_cnt.size : " + Completed_cnt);

                    ContentValues updateValues = new ContentValues(2);
                    updateValues.put(ApplicationContract.DownloadChapter.COLUMN_TOTAL_PAGES, downloadPages.size());
                    updateValues.put(ApplicationContract.DownloadChapter.COLUMN_CURRENT_PAGE, Completed_cnt);
                    QueryManager.updateDownloadChapter(downloadChapter.getId(), updateValues).toBlocking().single();
                    return Observable.just(downloadPages);
                })
                .flatMap((List<DownloadPage> downloadPages) ->
                                Observable.from(downloadPages.toArray(new DownloadPage[downloadPages.size()]))
                                        .filter(downloadPage -> downloadPage.getFlag() != DownloadUtils.FLAG_COMPLETED) //Failed 나 Pending
                                        .flatMap(new Func1<DownloadPage, Observable<DownloadPage>>() {
                                            @Override
                                            public Observable<DownloadPage> call(DownloadPage downloadPage) {
                                                ContentValues pageValues = new ContentValues(1);
                                                pageValues.put(ApplicationContract.DownloadPage.COLUMN_FLAG, DownloadUtils.FLAG_RUNNING);
                                                QueryManager.updateDownloadPage(downloadPage.getSource(), downloadPage.getUrl(), pageValues).toBlocking().single();

                                                return JMF_RestfulAdapter.getInterface().Filedownload(StorageFragment.mTeam.getId(), downloadPage.getUrl())
                                                        .flatMap(response -> DirectDisplayManager.saveInputStreamToDirectory(response, downloadPage))
                                                        .doOnCompleted(() -> {
                                                            Dlog.d("파일 다운로드 오나료?? doOnCompleted");
                                                            EventBus.getDefault().post(new DownloadChapterQueryEvent(DownloadUtils.FLAG_RUNNING, ""));
                                                        })
                                                        .doOnError(throwable -> {
                                                            Dlog.d("downloadChapterFromNetwork_officebox: 여기 들어오나?");
                                                            throwable.printStackTrace();
                                                        });

//										return OFB_RestfulAdapter.getInterface().downloadfile(MainActivity.loginToken,
//																								HttpUtils.device_id,
//																								HttpUtils.app_version,
//																								HttpUtils.device_name,
//																								HttpUtils.os_version,
//																								downloadPage.getUrl())//(downloadPage.getParentUrl(), downloadPage.getUrl())
//												.flatMap(response -> DirectDisplayManager.saveInputStreamToDirectory(response, downloadPage))
//												.doOnCompleted(() -> {
//													Dlog.d("파일 다운로드 오나료?? doOnCompleted");
//													EventBus.getDefault().post(new DownloadChapterQueryEvent(DownloadUtils.FLAG_RUNNING, ""));
//												})
//												.doOnError(throwable -> {
//													Dlog.d("downloadChapterFromNetwork_officebox: 여기 들어오나?");
//													throwable.printStackTrace();
//												});
                                            }
                                        })
                                        .flatMap(new Func1<DownloadPage, Observable<Integer>>() {
                                            @Override
                                            public Observable<Integer> call(DownloadPage downloadPage) {
                                                Cursor downloadChapterCursor = null;
                                                try {
                                                    Dlog.d("파일 다운로드 오나료?? downloadPage.getId() : " + downloadPage.getId());
                                                    ContentValues pageValues = new ContentValues(1);
                                                    pageValues.put(ApplicationContract.DownloadPage.COLUMN_FLAG, DownloadUtils.FLAG_COMPLETED);

                                                    QueryManager.updateDownloadPage(downloadPage.getSource(), downloadPage.getUrl(), pageValues).toBlocking().single();

                                                    downloadChapterCursor = QueryManager.queryDownloadChapterFromRequest(downloadRequest)
                                                            .toBlocking()
                                                            .single();

                                                    DownloadChapter updatedDownloadChapter = QueryManager.toObject(downloadChapterCursor, DownloadChapter.class);
                                                    updatedDownloadChapter.getCurrentPage();

                                                    ContentValues chapterValues = new ContentValues(1);
                                                    chapterValues.put(ApplicationContract.DownloadChapter.COLUMN_CURRENT_PAGE, updatedDownloadChapter.getCurrentPage() + 1);


                                                    return QueryManager.updateDownloadChapter(downloadChapter.getId(), chapterValues);
                                                } finally {
                                                    if (downloadChapterCursor != null)
                                                        downloadChapterCursor.close();
                                                }

                                            }
                                        })

                )
                .doOnUnsubscribe(() -> isUnsubscribed.set(true))
                .doOnError(throwable -> {
                    Dlog.d("doOnError doOnError doOnError");
                    if (!isUnsubscribed.get()) {
                        ContentValues updateValues = new ContentValues(1);
                        updateValues.put(ApplicationContract.DownloadChapter.COLUMN_FLAG, DownloadUtils.FLAG_FAILED);

                        QueryManager.deleteDownloadChapter(downloadRequest).toBlocking().single();

                        EventBus.getDefault().post(new DownloadChapterQueryEvent(DownloadUtils.FLAG_FAILED, throwable.getMessage()));
                    }
                })
                .doOnCompleted(() -> {
                    Cursor downloadChapterCursor = null;
                    try {
                        Dlog.d("다 받음??????????????doOnCompleted");
                        downloadChapterCursor = QueryManager.queryDownloadChapterFromRequest(downloadRequest)
                                .toBlocking()
                                .single();

                        if (downloadChapterCursor != null) {
                            Dlog.d("다 받음??????????????1111111111111111111111111111111111111111111111111111111111111");
                            DownloadChapter updatedDownloadChapter = QueryManager.toObject(downloadChapterCursor, DownloadChapter.class);

                            if (updatedDownloadChapter != null) {
                                Dlog.d("다 받음??????????????222222222222222222222222222222222222222222222222222222222222222222222");
                                Dlog.d(updatedDownloadChapter.getCurrentPage() + "::::::::::" + updatedDownloadChapter.getTotalPages());
                                if (updatedDownloadChapter.getCurrentPage() != 0 && updatedDownloadChapter.getTotalPages() != 0) {
                                    Dlog.d("다 받음??????????????33333333333333333333333333333333333333333333333333333");
                                    if (updatedDownloadChapter.getCurrentPage() == updatedDownloadChapter.getTotalPages()) {
                                        Dlog.d("다 받음??????????????444444444444444444444444444444444444444444444444");
                                        ContentValues updateValues = new ContentValues(1);
                                        updateValues.put(ApplicationContract.DownloadChapter.COLUMN_FLAG, DownloadUtils.FLAG_COMPLETED);

                                        QueryManager.updateDownloadChapter(updatedDownloadChapter.getId(), updateValues)
                                                .toBlocking()
                                                .single();

                                        EventBus.getDefault().post(new DownloadChapterQueryEvent(DownloadUtils.FLAG_COMPLETED, ""));
                                    }
                                } else {
                                    EventBus.getDefault().post(new DownloadChapterQueryEvent(DownloadUtils.FLAG_NODATA, ""));
                                }
                            }
                        }

                        EventBus.getDefault().post(new DownloadChapterQueryEvent(DownloadUtils.FLAG_FINISH, ""));
                    } finally {
                        if (downloadChapterCursor != null)
                            downloadChapterCursor.close();
                    }
                });
    }

    public static Observable<Integer> downloadChapterFromNetwork_officebox(final DownloadChapter downloadChapter) {
        Dlog.d("downloadChapterFromNetwork_officebox: ");
        final DirectFolderRequestWrapper downloadRequest = new DirectFolderRequestWrapper(downloadChapter.getSource(), downloadChapter.getUrl(), "");
        final DownloadService_OfficeBox downloadService = DownloadService_OfficeBox.getTemporaryInstance();

        final AtomicBoolean isUnsubscribed = new AtomicBoolean(false);

        // 1. 리스트를 받아온다.
        return DirectDisplayManager
                .pullImageUrlsFromNetwork(downloadRequest)
                .subscribeOn(Schedulers.io())
                .toList()
                .flatMap(imageModels -> {
                    //기존에 있던 다운로드 리스트를 모두 FLAG_DELETE 상태로 바꿈
                    ContentValues pageValues = new ContentValues(1);
                    pageValues.put(ApplicationContract.DownloadPage.COLUMN_FLAG, DownloadUtils.FLAG_DELETE);
                    int updatecnt = QueryManager.updateDownloadPage(downloadRequest, pageValues).toBlocking().single();
                    Dlog.d("downloadChapterFromNetwork: updatecnt : " + updatecnt);
                    // 새로 다운로드 리스트를 더한다.
                    return QueryManager.addDownloadPagesForDownloadChapter(downloadChapter, imageModels);
                })
                .flatMap(downloadPages -> {
                    Dlog.d("downloadChapterFromNetwork: downloadPages.size : " + downloadPages.size());
                    //리스트에서 완료된 갯수를 찾는다.
                    int Completed_cnt = 0;
                    for (DownloadPage downloadPage : downloadPages) {
                        if (downloadPage.getFlag() == DownloadUtils.FLAG_COMPLETED) {
                            Completed_cnt++;
                        }
                    }
                    Dlog.d("downloadChapterFromNetwork: downloadPages.size : " + downloadPages.size());
                    Dlog.d("downloadChapterFromNetwork: Completed_cnt.size : " + Completed_cnt);

                    ContentValues updateValues = new ContentValues(2);
                    updateValues.put(ApplicationContract.DownloadChapter.COLUMN_TOTAL_PAGES, downloadPages.size());
                    updateValues.put(ApplicationContract.DownloadChapter.COLUMN_CURRENT_PAGE, Completed_cnt);
                    QueryManager.updateDownloadChapter(downloadChapter.getId(), updateValues).toBlocking().single();
                    return Observable.just(downloadPages);
                })
                .flatMap((List<DownloadPage> downloadPages) ->
                                Observable.from(downloadPages.toArray(new DownloadPage[downloadPages.size()]))
                                        .filter(downloadPage -> downloadPage.getFlag() != DownloadUtils.FLAG_COMPLETED) //Failed 나 Pending
                                        .flatMap(new Func1<DownloadPage, Observable<DownloadPage>>() {
                                            @Override
                                            public Observable<DownloadPage> call(DownloadPage downloadPage) {
                                                ContentValues pageValues = new ContentValues(1);
                                                pageValues.put(ApplicationContract.DownloadPage.COLUMN_FLAG, DownloadUtils.FLAG_RUNNING);
                                                QueryManager.updateDownloadPage(downloadPage.getSource(), downloadPage.getUrl(), pageValues).toBlocking().single();

                                                return JMF_RestfulAdapter.getInterface().Filedownload(StorageFragment.mTeam.getId(), downloadPage.getUrl())
                                                        .flatMap(response -> {
                                                            return DirectDisplayManager.saveInputStreamToDirectory(response, downloadPage);
                                                        })
                                                        .doOnCompleted(() -> {
                                                            Dlog.d("파일 다운로드 오나료?? doOnCompleted");
                                                            EventBus.getDefault().post(new DownloadChapterQueryEvent(DownloadUtils.FLAG_RUNNING, ""));
                                                        })
                                                        .doOnError(throwable -> {
                                                            Dlog.d("downloadChapterFromNetwork_officebox: 여기 들어오나?");
                                                            throwable.printStackTrace();
                                                        });

//										return OFB_RestfulAdapter.getInterface().downloadfile(MainActivity.loginToken,
//																								HttpUtils.device_id,
//																								HttpUtils.app_version,
//																								HttpUtils.device_name,
//																								HttpUtils.os_version,
//																								downloadPage.getUrl())
//												.flatMap(response -> {
//													return DirectDisplayManager.saveInputStreamToDirectory(response, downloadPage);
//												})
//												.doOnCompleted(() -> {
//													Dlog.d("파일 다운로드 오나료?? doOnCompleted");
//													EventBus.getDefault().post(new DownloadChapterQueryEvent(DownloadUtils.FLAG_RUNNING, ""));
//												})
//												.doOnError(throwable -> {
//													Dlog.d("downloadChapterFromNetwork_officebox: 여기 들어오나?");
//													throwable.printStackTrace();
//												});
                                            }
                                        })
                                        .flatMap(new Func1<DownloadPage, Observable<Integer>>() {
                                            @Override
                                            public Observable<Integer> call(DownloadPage downloadPage) {
                                                Cursor downloadChapterCursor = null;
                                                try {
                                                    Dlog.d("파일 다운로드 오나료?? downloadPage.getId() : " + downloadPage.getId());
                                                    ContentValues pageValues = new ContentValues(1);
                                                    pageValues.put(ApplicationContract.DownloadPage.COLUMN_FLAG, DownloadUtils.FLAG_COMPLETED);

                                                    QueryManager.updateDownloadPage(downloadPage.getSource(), downloadPage.getUrl(), pageValues).toBlocking().single();

                                                    downloadChapterCursor = QueryManager.queryDownloadChapterFromRequest(downloadRequest)
                                                            .toBlocking()
                                                            .single();

                                                    DownloadChapter updatedDownloadChapter = QueryManager.toObject(downloadChapterCursor, DownloadChapter.class);
                                                    updatedDownloadChapter.getCurrentPage();

                                                    ContentValues chapterValues = new ContentValues(1);
                                                    chapterValues.put(ApplicationContract.DownloadChapter.COLUMN_CURRENT_PAGE, updatedDownloadChapter.getCurrentPage() + 1);

                                                    return QueryManager.updateDownloadChapter(downloadChapter.getId(), chapterValues);
                                                } finally {
                                                    if (downloadChapterCursor != null)
                                                        downloadChapterCursor.close();
                                                }
                                            }
                                        })

                )
                .doOnUnsubscribe(() -> isUnsubscribed.set(true))
                .doOnError(throwable -> {
                    Dlog.d("doOnError doOnError doOnError");
                    if (!isUnsubscribed.get()) {
                        ContentValues updateValues = new ContentValues(1);
                        updateValues.put(ApplicationContract.DownloadChapter.COLUMN_FLAG, DownloadUtils.FLAG_FAILED);

                        QueryManager.deleteDownloadChapter(downloadRequest).toBlocking().single();

                        EventBus.getDefault().post(new DownloadChapterQueryEvent(DownloadUtils.FLAG_FAILED, throwable.getMessage()));
                    }
                })
                .doOnCompleted(() -> {
                    Cursor downloadChapterCursor = null;
                    try {
                        Dlog.d("다 받음??????????????doOnCompleted");
                        downloadChapterCursor = QueryManager.queryDownloadChapterFromRequest(downloadRequest)
                                .toBlocking()
                                .single();

                        if (downloadChapterCursor != null) {
                            Dlog.d("다 받음??????????????1111111111111111111111111111111111111111111111111111111111111");
                            DownloadChapter updatedDownloadChapter = QueryManager.toObject(downloadChapterCursor, DownloadChapter.class);

                            if (updatedDownloadChapter != null) {
                                Dlog.d("다 받음??????????????222222222222222222222222222222222222222222222222222222222222222222222");
                                Dlog.d(updatedDownloadChapter.getCurrentPage() + "::::::::::" + updatedDownloadChapter.getTotalPages());
                                if (updatedDownloadChapter.getCurrentPage() != 0 && updatedDownloadChapter.getTotalPages() != 0) {
                                    Dlog.d("다 받음??????????????33333333333333333333333333333333333333333333333333333");
                                    if (updatedDownloadChapter.getCurrentPage() == updatedDownloadChapter.getTotalPages()) {
                                        Dlog.d("다 받음??????????????444444444444444444444444444444444444444444444444");
                                        ContentValues updateValues = new ContentValues(1);
                                        updateValues.put(ApplicationContract.DownloadChapter.COLUMN_FLAG, DownloadUtils.FLAG_COMPLETED);

                                        QueryManager.updateDownloadChapter(updatedDownloadChapter.getId(), updateValues)
                                                .toBlocking()
                                                .single();

                                        EventBus.getDefault().post(new DownloadChapterQueryEvent(DownloadUtils.FLAG_COMPLETED, ""));
                                    }
                                } else {
                                    EventBus.getDefault().post(new DownloadChapterQueryEvent(DownloadUtils.FLAG_NODATA, ""));
                                }
                            }
                        }

                        EventBus.getDefault().post(new DownloadChapterQueryEvent(DownloadUtils.FLAG_FINISH, ""));
                    } finally {
                        if (downloadChapterCursor != null)
                            downloadChapterCursor.close();
                    }
                });
    }


    //    private static Observable<File> saveInputStreamToDirectory(final InputStream inputStream, final String directory, final String name) {
    private static Observable<DownloadPage> saveInputStreamToDirectory(Response response, DownloadPage downloadPage) {

        String fileDirectory = downloadPage.getDirectory();
        String fileName = downloadPage.getName();
        String fileType = downloadPage.getExt();

        return Observable.create(new Observable.OnSubscribe<DownloadPage>() {
            @Override
            public void call(Subscriber<? super DownloadPage> subscriber) {
                try {
                    Dlog.d("call: saveInputStreamToDirectory name." + " : " + fileName + "." + fileType);
                    DiskUtils.saveInputStreamToDirectory(response, fileDirectory, fileName + "." + fileType);
                    subscriber.onNext(downloadPage);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }


    //    private static Observable<File> saveInputStreamToDirectory(final InputStream inputStream, final String directory, final String name) {
    public static Observable<DownloadPage> saveInputStreamToDirectory(ResponseBody responseBody, DownloadPage downloadPage) {

        String fileDirectory = downloadPage.getDirectory();
        String fileName = downloadPage.getName();
        String fileType = downloadPage.getExt();

        return Observable.create(new Observable.OnSubscribe<DownloadPage>() {
            @Override
            public void call(Subscriber<? super DownloadPage> subscriber) {
                try {
                    Dlog.d("call: saveInputStreamToDirectory name." + " : " + fileName + "." + fileType);
                    subscriber.onNext(DiskUtils.saveInputStreamToDirectory(responseBody, fileDirectory, fileName + "." + fileType, downloadPage));
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static Observable<Boolean> clearImageCache() {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                try {
                    boolean isSuccessful = true;
                    Fresco.getImagePipeline().clearCaches();
                    deleteDir(Utils.getDownloadDirectory());
                    subscriber.onNext(isSuccessful);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static int deleteDir(String a_path) {
        File file = new File(a_path);
        if (file.exists()) {
            File[] childFileList = file.listFiles();
            for (File childFile : childFileList) {
                if (childFile.isDirectory()) {
                    deleteDir(childFile.getAbsolutePath());

                } else {
                    childFile.delete();
                }
            }
            file.delete();
            return 1;
        } else {
            return 0;
        }
    }
}
