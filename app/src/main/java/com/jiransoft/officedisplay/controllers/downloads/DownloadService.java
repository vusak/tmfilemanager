package com.jiransoft.officedisplay.controllers.downloads;

import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;

import com.cantrowitz.rxbroadcast.RxBroadcast;
import com.jiransoft.officedisplay.controllers.DirectDisplayManager;
import com.jiransoft.officedisplay.controllers.QueryManager;
import com.jiransoft.officedisplay.controllers.databases.ApplicationContract;
import com.jiransoft.officedisplay.controllers.databases.ApplicationSQLiteOpenHelper;
import com.jiransoft.officedisplay.controllers.events.DownloadChapterQueryEvent;
import com.jiransoft.officedisplay.controllers.events.DownloadFolderQueryEvent;
import com.jiransoft.officedisplay.controllers.events.FileDownloadEvent;
import com.jiransoft.officedisplay.controllers.factories.DefaultFactory;
import com.jiransoft.officedisplay.controllers.wrappers.DirectFolderRequestWrapper;
import com.jiransoft.officedisplay.models.downloads.DownloadChapter;
import com.jiransoft.officedisplay.utils.DiskUtils;
import com.jiransoft.officedisplay.utils.Dlog;
import com.jiransoft.officedisplay.utils.DownloadUtils;
import com.jiransoft.officedisplay.utils.NavigationUtils;
import com.jiransoft.officedisplay.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import de.greenrobot.event.EventBus;
import jiran.com.tmfilemanager.BuildConfig;
import jiran.com.tmfilemanager.MyApplication;
import jiran.com.tmfilemanager.R;
import jiran.com.tmfilemanager.activity.MainActivity;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

public class DownloadService extends Service implements Observer<Integer> {
    public static final String TAG = DownloadService.class.getSimpleName();

    public static final String INTENT_QUEUE_DOWNLOAD = TAG + ":" + "QueueDownloadIntent";
    public static final String INTENT_CANCEL_DOWNLOAD = TAG + ":" + "CancelDownloadIntent";
    public static final String INTENT_START_DOWNLOAD = TAG + ":" + "StartDownloadIntent";
    public static final String INTENT_STOP_DOWNLOAD = TAG + ":" + "StopDownloadIntent";
    public static final String INTENT_RESTART_DOWNLOAD = TAG + ":" + "RestartDownloadIntent";
    public static final String INTENT_CHECKNEW_DOWNLOAD = TAG + ":" + "CheckNewDownloadIntent";

    private final static int DOWNLOAD_NOTIFICATION_ID = 10337;

    private static final int DOWNLOAD_CORE_POOL_SIZE = Runtime.getRuntime().availableProcessors();
    private static final int DOWNLOAD_MAXIMUM_POOL_SIZE = (DOWNLOAD_CORE_POOL_SIZE > 0) ? DOWNLOAD_CORE_POOL_SIZE : 2;
    private static final int KEEP_ALIVE_TIME = 1;
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;

    private NotificationCompat.Builder mDownloadNotificationBuilder;

    private PowerManager.WakeLock mWakeLock;

    private ThreadPoolExecutor mDownloadThreadPoolExecutor;

    private PublishSubject<DownloadChapter> mDownloadChapterPublishSubject;

    private ConcurrentHashMap<String, Subscription> mDownloadUrlToSubscriptionMap;
    private Subscription mDownloadChapterPublishSubjectSubscription;
    private Subscription mNetworkChangeBroadcastSubscription;

    private boolean mIsInitialized;
    private boolean mIsStopping;

    @Override
    public void onCreate() {
        super.onCreate();
        EventBus.getDefault().register(this);
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        Observable.create(new Observable.OnSubscribe<Void>() {
            @Override
            public void call(Subscriber<? super Void> subscriber) {
                handleQueueDownloadIntent(intent);
                handleCancelDownloadIntent(intent);
                handleStartDownloadIntent(intent);
//                initializeNetworkChangeBroadcastObservable(intent);
                handleStopDownloadIntent(intent);
                handleRestartDownloadIntent(intent);
                handleCheckNewDownloadIntent(intent);

                subscriber.onCompleted();
            }
        })
                .subscribeOn(Schedulers.newThread())
                .subscribe();

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
        releaseWakeLock();
    }

    // Observer<File>:

    @Override
    public void onCompleted() {
        if (QueryManager.queryShouldDownloadServiceStop().toBlocking().single()) {
            stopForeground(false);
            stopSelf();
        }
    }

    @Override
    public void onError(Throwable e) {
        if (BuildConfig.DEBUG) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNext(Integer imageFile) {
        // Do Nothing.
    }

    private void initialize() {
        if (mIsInitialized) {
            return;
        }

        initializeWakeLock();
        initializeThreadPoolExecutor();
        initializeDownloadChapterPublishSubject();
        initializeNetworkChangeBroadcastObservable();
        initializeNotification();

        mIsInitialized = true;
    }

    private void initializeWakeLock() {
        mWakeLock = ((PowerManager) getSystemService(POWER_SERVICE)).newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG + ":" + "WakeLock");

        if (!mWakeLock.isHeld()) {
            mWakeLock.acquire();
        }
    }

    private void releaseWakeLock() {
        if (mWakeLock != null) {
            if (mWakeLock.isHeld()) {
                mWakeLock.release();
            }
        }
    }

    private void initializeThreadPoolExecutor() {
        mDownloadThreadPoolExecutor = new ThreadPoolExecutor(
                DOWNLOAD_MAXIMUM_POOL_SIZE,
                DOWNLOAD_MAXIMUM_POOL_SIZE,
                KEEP_ALIVE_TIME,
                KEEP_ALIVE_TIME_UNIT,
                new LinkedBlockingDeque<>()
        );
    }

    private void initializeDownloadChapterPublishSubject() {
        mDownloadUrlToSubscriptionMap = new ConcurrentHashMap<>();

        mDownloadChapterPublishSubject = PublishSubject.create();
        mDownloadChapterPublishSubjectSubscription = mDownloadChapterPublishSubject
                .filter(downloadChapter -> QueryManager.queryAllowDownloadServiceToPublishDownloadChapter(downloadChapter).toBlocking().single())
                .subscribeOn(Schedulers.io())
                .subscribe(allowedChapter -> {
                    startDownloadToSubscriptionMap(allowedChapter);
                });
    }

    private void initializeNetworkChangeBroadcastObservable() {
        if (mNetworkChangeBroadcastSubscription != null) {

            IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);

            mNetworkChangeBroadcastSubscription = RxBroadcast
                    .fromBroadcast(this, intentFilter)
                    .subscribe(intent -> {
                        if (isNetworkAvailableForDownloads()) {
                            Dlog.d("initializeNetworkChangeBroadcastObservable: ");
                            queueDownloadChapters();
                            startForeground(DOWNLOAD_NOTIFICATION_ID, mDownloadNotificationBuilder.build());
                            if (!mWakeLock.isHeld()) {
                                mWakeLock.acquire();
                            }
                        } else {
                            stopForeground(false);

                            if (mWakeLock.isHeld()) {
                                mWakeLock.release();
                            }
                        }
                    }, throwable -> {
                    });
        }
    }

    private void startDownloadToSubscriptionMap(final DownloadChapter downloadChapter) {
        Dlog.d("44444444444444444444444");
        final String hashKey = DiskUtils.hashKeyForDisk(downloadChapter.getUrl());
        //GIGAPOD

        Subscription newSubscription = DirectDisplayManager.downloadChapterFromNetwork_officebox(downloadChapter)
                .subscribeOn(Schedulers.from(mDownloadThreadPoolExecutor))
                .subscribe(this);
        mDownloadUrlToSubscriptionMap.put(hashKey, newSubscription);


    }

    private void destoryAllSubscriptions() {
        Dlog.d("destoryAllSubscriptions: ");
        if (mDownloadUrlToSubscriptionMap != null) {
            for (Subscription downloadSubscription : mDownloadUrlToSubscriptionMap.values()) {
                if (downloadSubscription != null) {
                    downloadSubscription.unsubscribe();
                    downloadSubscription = null;
                }
            }
        }
        if (mDownloadChapterPublishSubjectSubscription != null) {
            mDownloadChapterPublishSubjectSubscription.unsubscribe();
            mDownloadChapterPublishSubjectSubscription = null;
        }
        if (mNetworkChangeBroadcastSubscription != null) {
            mNetworkChangeBroadcastSubscription.unsubscribe();
            mNetworkChangeBroadcastSubscription = null;
        }
    }

    //1111111111111111111111111111111111111111111
    private synchronized void handleQueueDownloadIntent(Intent queueDownloadIntent) {
        if (queueDownloadIntent != null && queueDownloadIntent.hasExtra(INTENT_QUEUE_DOWNLOAD)) {
            Dlog.d("handleQueueDownloadIntent: 11111111111111111111111111111111111111");
            ArrayList<DirectFolderRequestWrapper> chaptersToDownload = queueDownloadIntent.getParcelableArrayListExtra(INTENT_QUEUE_DOWNLOAD);
            if (chaptersToDownload != null) {
                ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                SQLiteDatabase applicationDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

                applicationDatabase.beginTransaction();
                Cursor downloadChapterCursor = null;
                try {
                    for (DirectFolderRequestWrapper chapterToDownload : chaptersToDownload) {
                        if (chapterToDownload != null) {

                            //1.검색했는데 기존에 값이 있으면 업데이트
                            downloadChapterCursor = QueryManager.queryDownloadChapterFromRequest(chapterToDownload).toBlocking().single();

                            if (downloadChapterCursor != null && downloadChapterCursor.getCount() > 0) {
                                DownloadChapter updatedDownloadChapter = QueryManager.toObject(downloadChapterCursor, DownloadChapter.class);

                                if (updatedDownloadChapter != null) {
                                    ContentValues updateValues = new ContentValues(3);
                                    updateValues.put(ApplicationContract.DownloadChapter.COLUMN_FLAG, DownloadUtils.FLAG_PENDING);
                                    updateValues.put(ApplicationContract.DownloadChapter.COLUMN_CURRENT_PAGE, 0);
                                    updateValues.put(ApplicationContract.DownloadChapter.COLUMN_TOTAL_PAGES, 0);

                                    QueryManager.updateDownloadChapter(updatedDownloadChapter.getId(), updateValues).toBlocking().single();
                                }
                            } else {
                                DownloadChapter downloadChapter = DefaultFactory.DownloadChapter.constructDefault();
                                downloadChapter.setSource(chapterToDownload.getSource());
                                downloadChapter.setUrl(chapterToDownload.getUrl());
                                downloadChapter.setParentUrl(chapterToDownload.getUrl());
                                downloadChapter.setName(chapterToDownload.getName());

                                File externalDirectory = new File(Utils.getDownloadDirectory());
                                File sourceDirectory = new File(externalDirectory, downloadChapter.getSource());
                                File urlHashDirectory = new File(sourceDirectory, DiskUtils.hashKeyForDisk(downloadChapter.getUrl()));

                                downloadChapter.setDirectory(urlHashDirectory.getAbsolutePath());

                                File noMediaFile = new File(externalDirectory, ".nomedia");

                                if (!noMediaFile.exists()) {
                                    if (!externalDirectory.exists()) {
                                        externalDirectory.mkdirs();
                                    }

                                    try {
                                        noMediaFile.createNewFile();
                                    } catch (IOException e) {
                                        // Do Nothing.
                                    }
                                }
//                                }
                                downloadChapter.setFlag(DownloadUtils.FLAG_PENDING);
                                QueryManager.putObjectToApplicationDatabase(downloadChapter);
                            }
                        }
                    }
                    applicationDatabase.setTransactionSuccessful();
                } finally {
                    applicationDatabase.endTransaction();
                    if (downloadChapterCursor != null)
                        downloadChapterCursor.close();
                }
            }

            queueDownloadIntent.removeExtra(INTENT_QUEUE_DOWNLOAD);


            if (mIsInitialized) {
                if (QueryManager.queryShouldDownloadServiceStop().toBlocking().single()) {
                    stopForeground(false);
                    stopSelf();
                }
            } else {
                stopForeground(false);
                stopSelf();
            }

            if (queueDownloadIntent.getBooleanExtra("ScreenSaver", false))
                EventBus.getDefault().post(new DownloadFolderQueryEvent(DownloadUtils.FLAG_PENDING, "ScreenSaver"));
            else
                EventBus.getDefault().post(new DownloadFolderQueryEvent(DownloadUtils.FLAG_PENDING, ""));
        }

    }

    private synchronized void handleCheckNewDownloadIntent(Intent queueDownloadIntent) {
        if (queueDownloadIntent != null && queueDownloadIntent.hasExtra(INTENT_CHECKNEW_DOWNLOAD)) {
            Dlog.d("handleCheckNewDownloadIntent: CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC");
            ArrayList<DirectFolderRequestWrapper> chaptersToDownload = queueDownloadIntent.getParcelableArrayListExtra(INTENT_CHECKNEW_DOWNLOAD);
            if (chaptersToDownload != null) {
                ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                SQLiteDatabase applicationDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

                applicationDatabase.beginTransaction();
                Cursor downloadChapterCursor = null;
                try {
                    for (DirectFolderRequestWrapper chapterToDownload : chaptersToDownload) {
                        if (chapterToDownload != null) {

                            //1.검색했는데 기존에 값이 있으면 업데이트
                            downloadChapterCursor = QueryManager.queryDownloadChapterFromRequest(chapterToDownload).toBlocking().single();

                            if (downloadChapterCursor != null && downloadChapterCursor.getCount() > 0) {
                                DownloadChapter updatedDownloadChapter = QueryManager.toObject(downloadChapterCursor, DownloadChapter.class);

                                if (updatedDownloadChapter != null) {
                                    ContentValues updateValues = new ContentValues(3);
                                    updateValues.put(ApplicationContract.DownloadChapter.COLUMN_FLAG, DownloadUtils.FLAG_PENDING);
                                    updateValues.put(ApplicationContract.DownloadChapter.COLUMN_CURRENT_PAGE, 0);
                                    updateValues.put(ApplicationContract.DownloadChapter.COLUMN_TOTAL_PAGES, 0);

                                    QueryManager.updateDownloadChapter(updatedDownloadChapter.getId(), updateValues).toBlocking().single();
                                }
                            } else {
                                DownloadChapter downloadChapter = DefaultFactory.DownloadChapter.constructDefault();
                                downloadChapter.setSource(chapterToDownload.getSource());
                                downloadChapter.setUrl(chapterToDownload.getUrl());
                                downloadChapter.setParentUrl(chapterToDownload.getUrl());
                                downloadChapter.setName(chapterToDownload.getName());

                                File externalDirectory = new File(Utils.getDownloadDirectory());
                                File sourceDirectory = new File(externalDirectory, downloadChapter.getSource());
                                File urlHashDirectory = new File(sourceDirectory, DiskUtils.hashKeyForDisk(downloadChapter.getUrl()));


                                downloadChapter.setDirectory(urlHashDirectory.getAbsolutePath());

                                File noMediaFile = new File(externalDirectory, ".nomedia");

                                if (!noMediaFile.exists()) {
                                    if (!externalDirectory.exists()) {
                                        externalDirectory.mkdirs();
                                    }

                                    try {
                                        noMediaFile.createNewFile();
                                    } catch (IOException e) {
                                        // Do Nothing.
                                    }
                                }
                                downloadChapter.setFlag(DownloadUtils.FLAG_PENDING);
                                QueryManager.putObjectToApplicationDatabase(downloadChapter);
                            }
                        }
                    }
                    applicationDatabase.setTransactionSuccessful();
                } finally {
                    applicationDatabase.endTransaction();
                    if (downloadChapterCursor != null)
                        downloadChapterCursor.close();
                }
            }

            queueDownloadIntent.removeExtra(INTENT_CHECKNEW_DOWNLOAD);


            if (mIsInitialized) {
                if (QueryManager.queryShouldDownloadServiceStop().toBlocking().single()) {
                    stopForeground(false);
                    stopSelf();
                }
            } else {
                stopForeground(false);
                stopSelf();
            }

            EventBus.getDefault().post(new DownloadChapterQueryEvent(DownloadUtils.FLAG_CHECKNEW, ""));
        }
    }

    private synchronized void handleCancelDownloadIntent(Intent cancelDownloadIntent) {
        if (cancelDownloadIntent != null) {
            if (cancelDownloadIntent.hasExtra(INTENT_CANCEL_DOWNLOAD)) {
                ArrayList<DownloadChapter> downloadChaptersToCancel = cancelDownloadIntent.getParcelableArrayListExtra(INTENT_CANCEL_DOWNLOAD);
                if (downloadChaptersToCancel != null) {
                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

                    sqLiteDatabase.beginTransaction();
                    try {
                        for (DownloadChapter downloadChapter : downloadChaptersToCancel) {
                            if (downloadChapter != null) {
                                if (mIsInitialized) {
                                    String hashKey = DiskUtils.hashKeyForDisk(downloadChapter.getUrl());
                                    if (mDownloadUrlToSubscriptionMap.containsKey(hashKey)) {
                                        Subscription currentSubscription = mDownloadUrlToSubscriptionMap.remove(hashKey);
                                        currentSubscription.unsubscribe();
                                        currentSubscription = null;
                                    }
                                }

                                DiskUtils.deleteFiles(new File(downloadChapter.getDirectory()));

                                QueryManager.deleteObjectToApplicationDatabase(downloadChapter);
                                QueryManager.deleteDownloadPagesOfDownloadChapter(new DirectFolderRequestWrapper(downloadChapter.getSource(), downloadChapter.getUrl(), ""))
                                        .toBlocking()
                                        .single();
                            }
                        }
                        sqLiteDatabase.setTransactionSuccessful();
                    } finally {
                        sqLiteDatabase.endTransaction();
                    }
                }

                EventBus.getDefault().post(new DownloadChapterQueryEvent(DownloadUtils.FLAG_CANCELED, ""));

                cancelDownloadIntent.removeExtra(INTENT_CANCEL_DOWNLOAD);

                if (mIsInitialized) {
                    if (isNetworkAvailableForDownloads()) {
                        queueDownloadChapters();
                    }

                    if (QueryManager.queryShouldDownloadServiceStop().toBlocking().single()) {
                        stopForeground(false);
                        stopSelf();
                    }
                } else {
                    stopForeground(false);
                    stopSelf();
                }
            }
        }
    }

    //222222222222222222222222222222222222222222222222222
    private synchronized void handleStartDownloadIntent(Intent startDownloadIntent) {
        if (startDownloadIntent != null && startDownloadIntent.hasExtra(INTENT_START_DOWNLOAD)) {
            Dlog.d("handleStartDownloadIntent: 222222222222222222222222222222");
            initialize();
            startDownloadIntent.removeExtra(INTENT_START_DOWNLOAD);

            if (isNetworkAvailableForDownloads()) {
                queueDownloadChapters();
            }

            if (QueryManager.queryShouldDownloadServiceStop().toBlocking().single()) {
                stopForeground(false);
                stopSelf();
            }
        }
    }

    private void handleStopDownloadIntent(Intent stopDownloadIntent) {
        if (stopDownloadIntent != null) {
            if (stopDownloadIntent.hasExtra(INTENT_STOP_DOWNLOAD)) {
                mIsStopping = true;

                destoryAllSubscriptions();
                pauseDownloadChapters();
                stopDownloadIntent.removeExtra(INTENT_STOP_DOWNLOAD);

                stopForeground(false);
                stopSelf();

                EventBus.getDefault().post(new DownloadChapterQueryEvent(DownloadUtils.FLAG_PAUSED, ""));
            }
        }
    }

    private synchronized void handleRestartDownloadIntent(Intent restartDownloadIntent) {
        if (restartDownloadIntent != null) {
            if (restartDownloadIntent.hasExtra(INTENT_RESTART_DOWNLOAD)) {
                pauseDownloadChapters();

                restartDownloadIntent.removeExtra(INTENT_RESTART_DOWNLOAD);

                stopForeground(false);
                stopSelf();
            }
        }
    }

    private synchronized void queueDownloadChapters() {
        Cursor runningCursor = null;
        Cursor availableDownloadChapterCursor = null;
        try {
            if (mIsStopping) {
                return;
            }

            if (mDownloadChapterPublishSubject != null) {
                int dequeueLimit = DOWNLOAD_MAXIMUM_POOL_SIZE;
                runningCursor = QueryManager.queryRunningDownloadChapters().toBlocking().single();
                if (runningCursor != null) {
                    dequeueLimit -= runningCursor.getCount();
                }
                if (dequeueLimit > 0) {
                    availableDownloadChapterCursor = QueryManager.queryPendingDownloadChapters(dequeueLimit).toBlocking().single();
                    if (availableDownloadChapterCursor != null && availableDownloadChapterCursor.getCount() != 0) {
                        List<DownloadChapter> downloadChapters = QueryManager.toList(availableDownloadChapterCursor, DownloadChapter.class);
                        if (downloadChapters != null && downloadChapters.size() > 0) {
                            for (DownloadChapter currentDownloadChapter : downloadChapters) {
                                currentDownloadChapter.setFlag(DownloadUtils.FLAG_RUNNING);
                                QueryManager.putObjectToApplicationDatabase(currentDownloadChapter);
                            }

                            for (DownloadChapter streamDownloadChapter : downloadChapters) {
                                mDownloadChapterPublishSubject.onNext(streamDownloadChapter);
                            }
                        }
                    }

                }
            }
        } finally {
            if (runningCursor != null)
                runningCursor.close();

            if (availableDownloadChapterCursor != null)
                availableDownloadChapterCursor.close();
        }
    }


    private synchronized void pauseDownloadChapters() {
        Cursor nonCompletedCursor = null;
        try {
            nonCompletedCursor = QueryManager.queryNonCompletedDownloadChapters()
                    .toBlocking()
                    .single();

            if (nonCompletedCursor != null) {
                List<DownloadChapter> downloadChapters = QueryManager.toList(nonCompletedCursor, DownloadChapter.class);

                ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();
                sqLiteDatabase.beginTransaction();
                try {
                    for (DownloadChapter downloadChapter : downloadChapters) {
                        downloadChapter.setFlag(DownloadUtils.FLAG_PAUSED);

                        QueryManager.putObjectToApplicationDatabase(downloadChapter);
                    }
                    sqLiteDatabase.setTransactionSuccessful();
                } finally {
                    sqLiteDatabase.endTransaction();
                }

            }
        } finally {
            if (nonCompletedCursor != null)
                nonCompletedCursor.close();
        }
    }

    private boolean isNetworkAvailableForDownloads() {
        boolean isWiFiOnly = Utils.isWiFiOnly();

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        if (isConnected) {
            if (isWiFiOnly) {
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                    return true;
                }
            } else {
                return true;
            }
        }

        return false;
    }

    private void initializeNotification() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.putExtra(MainActivity.POSITION_ARGUMENT_KEY, NavigationUtils.POSITION_QUEUE);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(notificationIntent);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        mDownloadNotificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("OfficeDisplay Downloader")
                .setContentText("Downloading...")
                .setProgress(0, 0, true)
                .setContentIntent(pendingIntent);

        startForeground(DOWNLOAD_NOTIFICATION_ID, mDownloadNotificationBuilder.build());
    }

    private void sendNotification(String current, String progress) {
        mDownloadNotificationBuilder.setProgress(100, Integer.valueOf(progress), false);
        mDownloadNotificationBuilder.setContentText("Downloading file " + current);
        NotificationManagerCompat.from(this).notify(DOWNLOAD_NOTIFICATION_ID, mDownloadNotificationBuilder.build());
    }

    public void onEventMainThread(FileDownloadEvent fileDownloadEvent) {
        sendNotification(fileDownloadEvent.getCurrentSize(), fileDownloadEvent.getProgress());
    }

}
