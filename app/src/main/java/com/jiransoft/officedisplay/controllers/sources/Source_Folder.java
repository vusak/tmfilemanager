package com.jiransoft.officedisplay.controllers.sources;

import com.jiransoft.officedisplay.controllers.wrappers.DirectFolderRequestWrapper;
import com.jiransoft.officedisplay.models.ImageModel;

import rx.Observable;

public interface Source_Folder {
    Observable<String> getName();

    Observable<ImageModel> pullImageUrlsFromNetwork(DirectFolderRequestWrapper request);

}
