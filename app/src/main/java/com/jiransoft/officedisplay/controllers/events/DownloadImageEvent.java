package com.jiransoft.officedisplay.controllers.events;

public class DownloadImageEvent {
    boolean isdownload;
    String imageUrl;

    public DownloadImageEvent(boolean isdownload, String imageUrl) {
        this.isdownload = isdownload;
        this.imageUrl = imageUrl;
    }

    public boolean isdownload() {
        return isdownload;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
