package com.jiransoft.officedisplay.controllers.sources;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jiransoft.officedisplay.controllers.wrappers.DirectFolderRequestWrapper;
import com.jiransoft.officedisplay.models.ImageListModel;
import com.jiransoft.officedisplay.models.ImageModel;
import com.jiransoft.officedisplay.utils.DownloadUtils;
import com.jiransoft.officedisplay.utils.PreferenceHelper;
import com.jiransoft.officedisplay.utils.rx.RxUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jiran.com.tmfilemanager.common.Utils;
import jiran.com.tmfilemanager.fagment.StorageFragment;
import jiran.com.tmfilemanager.network.JMF_RestfulAdapter;
import rx.Observable;
import rx.Subscriber;

public class OFFICEBOX implements Source_Folder {
    public static final String NAME = "OfficeBox";
    public static final String BASE_URL = "www.OfficeBox.us";

    private static final String INITIAL_UPDATE_URL = "";

    @Override
    public Observable<String> getName() {
        return Observable.just(NAME);
    }

    /**
     * 챕터의 리스트를 가지고 옴
     *
     * @param request
     * @return
     */
    @Override
    public Observable<ImageModel> pullImageUrlsFromNetwork(final DirectFolderRequestWrapper request) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("sort", "+regdate");
        params.put("offset", "");
        params.put("limit", "10000");
        params.put("keyword", "");
        params.put("assort", "");
        return JMF_RestfulAdapter.getInterface().GetFolderList(StorageFragment.mTeam.getId(), request.getUrl(), params)
//		return OFB_RestfulAdapter.getInterface().GetFolderList(MainActivity.loginToken,
//																	HttpUtils.device_id,
//																	HttpUtils.app_version,
//																	HttpUtils.device_name,
//																	HttpUtils.os_version,
//																	request.getUrl(), "+datetime")
                .retryWhen(new RxUtils.RetryWithDelay(5, 5000))
                .flatMap(jsonObject -> Observable.create(new Observable.OnSubscribe<ImageListModel>() {
                    @Override
                    public void call(Subscriber<? super ImageListModel> subscriber) {
                        try {
                            ImageListModel imageListModel = new ImageListModel();
                            List<ImageModel> ImageModels = new ArrayList<ImageModel>();
                            JsonObject result = jsonObject.getAsJsonObject("result");
                            JsonObject lists = result.getAsJsonObject("lists");
                            JsonArray jsonArray = lists.getAsJsonArray("rows");
                            if (jsonArray != null && jsonArray.size() > 0) {

                                for (int i = 0; i < jsonArray.size(); i++) {
                                    JsonObject listitem = (JsonObject) jsonArray.get(i);
                                    String ext = listitem.get("extension").getAsString();
                                    if (DownloadUtils.isImageFile(ext) || DownloadUtils.isMovieFile(ext)) {

                                        ImageModels.add(new ImageModel(
                                                request.getUrl(), //project_id
                                                "", //comment
                                                "", //email
                                                listitem.get("extension").getAsString(), //ext
                                                listitem.get("object_id").getAsString(),//file_id
                                                Utils.formatDate(listitem.get("regdate").getAsString()),
                                                listitem.get("name").getAsString(),
                                                "",
                                                listitem.get("size").getAsInt(),
                                                "",
                                                DownloadUtils.FLAG_PENDING,
                                                ""
                                        ));
                                    }
                                }
                                imageListModel.setImageModels(ImageModels);

                                subscriber.onNext(imageListModel);
                                subscriber.onCompleted();
                            } else {
                                if (jsonArray == null) {
                                    throw new IllegalStateException(jsonObject.get("all").getAsString());
                                } else {
                                    throw new IllegalStateException("ImageList size is Zero");
                                }
                            }
                        } catch (Throwable e) {
                            subscriber.onError(e);
                        }
                    }
                }))
                .flatMap(TotalListModel -> {

                    Collections.sort(TotalListModel.getImageModels(), (o1, o2) -> {
                        //SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        SimpleDateFormat transFormat = new SimpleDateFormat("yyyy. MM. dd. HH:mm");
                        String firstValue = "1900-01-01 00:00:01";
                        String secondValue = "1900-01-01 00:00:01";
                        firstValue = o1.getModified();
                        secondValue = o2.getModified();
                        Date date1 = null;
                        Date date2 = null;
                        try {
                            date1 = transFormat.parse(firstValue);
                            date2 = transFormat.parse(secondValue);
                        } catch (ParseException e) {
                        }
                        if (date1 != null && date2 != null) {
                            return date2.compareTo(date1);
                        } else {
                            return 0;
                        }
                    });
                    if (!PreferenceHelper.getBoolean(PreferenceHelper.isUseFullPage, false)) {
                        int limit = PreferenceHelper.getInt(PreferenceHelper.slide_cnt, 10);
                        return Observable.from(TotalListModel.getImageModels().toArray(new ImageModel[TotalListModel.getImageModels().size()])).limit(limit);
                    } else {
                        return Observable.from(TotalListModel.getImageModels().toArray(new ImageModel[TotalListModel.getImageModels().size()]));
                    }
                });

    }
}
