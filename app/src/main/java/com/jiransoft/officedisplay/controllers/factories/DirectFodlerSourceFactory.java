package com.jiransoft.officedisplay.controllers.factories;

import com.jiransoft.officedisplay.controllers.sources.OFFICEBOX;
import com.jiransoft.officedisplay.controllers.sources.Source_Folder;

public class DirectFodlerSourceFactory {
    private DirectFodlerSourceFactory() {
        throw new AssertionError();
    }

    public static Source_Folder checkNames(String sourceName) {
        Source_Folder currentSource = null;

//		if (sourceName.contains(Direct_Folder.BASE_URL)) {
//			currentSource = new Direct_Folder();
//		} else if (sourceName.contains(Directcloud_BOX.BASE_URL)) {
//			currentSource = new Directcloud_BOX();
//		} else if (sourceName.contains(OFFICEBOX.BASE_URL)) {
//			currentSource = new OFFICEBOX();
//		} else
        if (sourceName.contains(OFFICEBOX.BASE_URL)) {
            currentSource = new OFFICEBOX();
        }

        return currentSource;
    }

    public static Source_Folder constructSourceFromName(String sourceName) {
        return checkNames(sourceName);
    }
}
