package com.jiransoft.officedisplay.controllers.events;

public class FileDownloadEvent {
    String CurrentSize;
    String Progress;

    public FileDownloadEvent(String currentsize, String progress) {
        this.CurrentSize = currentsize;
        this.Progress = progress;
    }

    public String getCurrentSize() {
        return CurrentSize;
    }

    public String getProgress() {
        return Progress;
    }
}
