package com.jiransoft.officedisplay.controllers.events;

public class NavigationItemSelectEvent {
    private String mSelectedPosition;

    public NavigationItemSelectEvent(String selectedPosition) {
        mSelectedPosition = selectedPosition;
    }

    public String getSelectedPosition() {
        return mSelectedPosition;
    }
}
