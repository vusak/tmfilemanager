package com.jiransoft.officedisplay.controllers.events;

public class FullScreenChangeEvent {
    private boolean mEvent;

    public FullScreenChangeEvent(boolean event) {
        mEvent = event;
    }

    public boolean getmEvent() {
        return mEvent;
    }

}
