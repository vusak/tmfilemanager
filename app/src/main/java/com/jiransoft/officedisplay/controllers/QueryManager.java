package com.jiransoft.officedisplay.controllers;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.util.Pair;

import com.jiransoft.officedisplay.controllers.databases.ApplicationContract;
import com.jiransoft.officedisplay.controllers.databases.ApplicationSQLiteOpenHelper;
import com.jiransoft.officedisplay.controllers.factories.DefaultFactory;
import com.jiransoft.officedisplay.controllers.wrappers.DirectFolderRequestWrapper;
import com.jiransoft.officedisplay.controllers.wrappers.RequestWrapper;
import com.jiransoft.officedisplay.models.ImageModel;
import com.jiransoft.officedisplay.models.downloads.DownloadChapter;
import com.jiransoft.officedisplay.models.downloads.DownloadManga;
import com.jiransoft.officedisplay.models.downloads.DownloadPage;
import com.jiransoft.officedisplay.utils.DownloadUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class QueryManager {
    private static final Object mAddPagesLock = new Object();
    private static final Object mAddMangaLock = new Object();

    private QueryManager() {
        throw new AssertionError();
    }

    public static Observable<DownloadManga> addDownloadMangaIfNone(final RequestWrapper request) {
        return Observable.create(new Observable.OnSubscribe<DownloadManga>() {
            @Override
            public void call(Subscriber<? super DownloadManga> subscriber) {
                synchronized (mAddMangaLock) {
                    try {
                        ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                        SQLiteDatabase applicationDatabase = applicationSQLiteOpenHelper.getWritableDatabase();
                        StringBuilder selection = new StringBuilder();
                        List<String> selectionArgs = new ArrayList<String>();

                        selection.append(ApplicationContract.DownloadManga.COLUMN_SOURCE + " = ?");
                        selectionArgs.add(request.getSource());
                        selection.append(" AND ").append(ApplicationContract.DownloadManga.COLUMN_URL + " = ?");
                        selectionArgs.add(request.getUrl());

                        DownloadManga mangaToDownload = null;

                        applicationDatabase.beginTransaction();
                        try {
                            mangaToDownload = cupboard().withDatabase(applicationDatabase).query(DownloadManga.class)
                                    .withSelection(selection.toString(), selectionArgs.toArray(new String[selectionArgs.size()]))
                                    .limit(1)
                                    .get();

                            applicationDatabase.setTransactionSuccessful();
                        } finally {
                            applicationDatabase.endTransaction();
                        }

                        subscriber.onNext(mangaToDownload);
                        subscriber.onCompleted();
                    } catch (Throwable e) {
                        subscriber.onError(e);
                    }
                }
            }
        });
    }

    public static Observable<Cursor> queryDownloadChapterFromRequest(final DirectFolderRequestWrapper request) {
        return Observable.create(new Observable.OnSubscribe<Cursor>() {
            @Override
            public void call(Subscriber<? super Cursor> subscriber) {
                try {
                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();
                    StringBuilder selection = new StringBuilder();
                    List<String> selectionArgs = new ArrayList<String>();

                    selection.append(ApplicationContract.DownloadChapter.COLUMN_SOURCE + " = ?");
                    selectionArgs.add(request.getSource());
                    selection.append(" AND ").append(ApplicationContract.DownloadChapter.COLUMN_URL + " = ?");
                    selectionArgs.add(request.getUrl());

                    Cursor downloadChapterCursor = cupboard().withDatabase(sqLiteDatabase).query(DownloadChapter.class)
                            .withSelection(selection.toString(), selectionArgs.toArray(new String[selectionArgs.size()]))
                            .getCursor();

                    subscriber.onNext(downloadChapterCursor);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static Observable<Cursor> queryDownloadChaptersOfDownloadManga(final RequestWrapper request, final boolean isCompleted) {
        return Observable.create(new Observable.OnSubscribe<Cursor>() {
            @Override
            public void call(Subscriber<? super Cursor> subscriber) {
                try {
                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();
                    StringBuilder selection = new StringBuilder();
                    List<String> selectionArgs = new ArrayList<String>();

                    selection.append(ApplicationContract.DownloadChapter.COLUMN_SOURCE + " = ?");
                    selectionArgs.add(request.getSource());
                    selection.append(" AND ").append(ApplicationContract.DownloadChapter.COLUMN_PARENT_URL + " = ?");
                    selectionArgs.add(request.getUrl());

                    if (isCompleted) {
                        selection.append(" AND ").append(ApplicationContract.DownloadChapter.COLUMN_FLAG + " = ?");
                        selectionArgs.add(String.valueOf(DownloadUtils.FLAG_COMPLETED));
                    }

                    Cursor downloadChaptersOfDownloadMangaCursor = cupboard().withDatabase(sqLiteDatabase).query(DownloadChapter.class)
                            .withSelection(selection.toString(), selectionArgs.toArray(new String[selectionArgs.size()]))
                            .getCursor();

                    subscriber.onNext(downloadChaptersOfDownloadMangaCursor);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static Observable<Cursor> queryPendingDownloadChapters(final int dequeueLimit) {
        return Observable.create(new Observable.OnSubscribe<Cursor>() {
            @Override
            public void call(Subscriber<? super Cursor> subscriber) {
                try {
                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

                    Cursor runningDownloadChapterCursor = cupboard().withDatabase(sqLiteDatabase).query(DownloadChapter.class)
                            .withSelection(ApplicationContract.DownloadChapter.COLUMN_FLAG + " = ?", String.valueOf(DownloadUtils.FLAG_PENDING))
                            .limit(dequeueLimit)
                            .getCursor();

                    subscriber.onNext(runningDownloadChapterCursor);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static Observable<Cursor> queryRunningDownloadChapters() {
        return Observable.create(new Observable.OnSubscribe<Cursor>() {
            @Override
            public void call(Subscriber<? super Cursor> subscriber) {
                try {
                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

                    Cursor runningDownloadChapterCursor = cupboard().withDatabase(sqLiteDatabase).query(DownloadChapter.class)
                            .withSelection(ApplicationContract.DownloadChapter.COLUMN_FLAG + " = ?", String.valueOf(DownloadUtils.FLAG_RUNNING))
                            .getCursor();

                    subscriber.onNext(runningDownloadChapterCursor);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static Observable<Cursor> queryNonCompletedDownloadChapters() {
        return Observable.create(new Observable.OnSubscribe<Cursor>() {
            @Override
            public void call(Subscriber<? super Cursor> subscriber) {
                try {
                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

                    Cursor nonCompletedDownloadChaptersCursor = cupboard().withDatabase(sqLiteDatabase).query(DownloadChapter.class)
                            .withSelection(ApplicationContract.DownloadChapter.COLUMN_FLAG + " != ?", String.valueOf(DownloadUtils.FLAG_COMPLETED))
                            .getCursor();

                    subscriber.onNext(nonCompletedDownloadChaptersCursor);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static Observable<Cursor> queryNonCompletedDownloadChapter(final DirectFolderRequestWrapper request) {
        return Observable.create(new Observable.OnSubscribe<Cursor>() {
            @Override
            public void call(Subscriber<? super Cursor> subscriber) {
                try {
                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();


                    StringBuilder selection = new StringBuilder();
                    List<String> selectionArgs = new ArrayList<>();

                    selection
                            .append(ApplicationContract.DownloadChapter.COLUMN_FLAG + " != ?")
                            .append(" AND ")
                            .append(ApplicationContract.DownloadChapter.COLUMN_URL + " = ?");

                    selectionArgs.add(String.valueOf(DownloadUtils.FLAG_COMPLETED));
                    selectionArgs.add(request.getUrl());

                    Cursor nonCompletedDownloadChaptersCursor = cupboard().withDatabase(sqLiteDatabase).query(DownloadChapter.class)
                            .withSelection(selection.toString(), selectionArgs.toArray(new String[selectionArgs.size()]))
                            .getCursor();

                    subscriber.onNext(nonCompletedDownloadChaptersCursor);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static Observable<Integer> updateDownloadChapter(final Long id, final ContentValues updateValues) {
        return Observable.create(new Observable.OnSubscribe<Integer>() {
            @Override
            public void call(Subscriber<? super Integer> subscriber) {
                try {
                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

                    int amountUpdated = cupboard().withDatabase(sqLiteDatabase).update(DownloadChapter.class, updateValues, ApplicationContract.DownloadChapter.COLUMN_ID + " = ?", String.valueOf(id));

                    subscriber.onNext(amountUpdated);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static Observable<Cursor> queryDownloadPagesOfDownloadChapter(final DirectFolderRequestWrapper request) {
        return Observable.create(new Observable.OnSubscribe<Cursor>() {
            @Override
            public void call(Subscriber<? super Cursor> subscriber) {
                try {
                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

                    StringBuilder selection = new StringBuilder();
                    List<String> selectionArgs = new ArrayList<>();

                    selection
                            .append(ApplicationContract.DownloadPage.COLUMN_PARENT_URL + " = ?")
                            .append(" AND ")
                            .append(ApplicationContract.DownloadPage.COLUMN_SOURCE + " = ?")
                            .append("Order by " + ApplicationContract.DownloadPage.COLUMN_MODIFIED + " desc");

                    selectionArgs.add(request.getUrl());
                    selectionArgs.add(request.getSource());

                    Cursor downloadPagesOfDownloadChapterCursor = cupboard().withDatabase(sqLiteDatabase).query(DownloadPage.class)
                            .withSelection(selection.toString(), selectionArgs.toArray(new String[selectionArgs.size()]))
                            .getCursor();

                    if (downloadPagesOfDownloadChapterCursor != null && downloadPagesOfDownloadChapterCursor.getCount() != 0) {
                        subscriber.onNext(downloadPagesOfDownloadChapterCursor);
                    } else {
                        throw new IllegalArgumentException("No DownloadPages of Download Chapter: " + request.getSource());
                    }

                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static Observable<Cursor> queryDownloadedPagesOfDownloadChapter(final DirectFolderRequestWrapper request, int fileStatus) {
        return Observable.create(new Observable.OnSubscribe<Cursor>() {
            @Override
            public void call(Subscriber<? super Cursor> subscriber) {
                try {
                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

                    StringBuilder selection = new StringBuilder();
                    List<String> selectionArgs = new ArrayList<>();

                    selection
                            .append(ApplicationContract.DownloadPage.COLUMN_FLAG + " = ?")
                            .append(" AND ")
                            .append(ApplicationContract.DownloadPage.COLUMN_PARENT_URL + " = ?")
                            .append(" AND ")
                            .append(ApplicationContract.DownloadPage.COLUMN_SOURCE + " = ?")
                            .append("Order by " + ApplicationContract.DownloadPage.COLUMN_MODIFIED + " desc");

                    selectionArgs.add(String.valueOf(fileStatus));
                    selectionArgs.add(request.getUrl());
                    selectionArgs.add(request.getSource());

                    Cursor downloadPagesOfDownloadChapterCursor = cupboard().withDatabase(sqLiteDatabase).query(DownloadPage.class)
                            .withSelection(selection.toString(), selectionArgs.toArray(new String[selectionArgs.size()]))
                            .getCursor();

                    if (downloadPagesOfDownloadChapterCursor != null && downloadPagesOfDownloadChapterCursor.getCount() != 0) {
                        subscriber.onNext(downloadPagesOfDownloadChapterCursor);
                    } else {
//                        throw new IllegalArgumentException("No DownloadPages of Download Chapter: " + request.getSource());
                        subscriber.onNext(null);
                    }

                    subscriber.onCompleted();
                } catch (Throwable e) {
//                    Dlog.d( "call: 다운 받은 페이지가 없으면 오류 남");
                    subscriber.onError(e);
                }
            }
        });
    }

    public static Observable<List<DownloadPage>> addDownloadPagesForDownloadChapter(final DownloadChapter downloadChapter, final List<ImageModel> imageUrls) {
        return Observable.create(new Observable.OnSubscribe<List<DownloadPage>>() {
            @Override
            public void call(Subscriber<? super List<DownloadPage>> subscriber) {
                synchronized (mAddPagesLock) {
                    try {
                        List<DownloadPage> downloadPageList = new ArrayList<DownloadPage>(imageUrls.size());
                        //저장된 폴더가 실제로 존재?
                        File chapterDirectory = new File(downloadChapter.getDirectory());
                        for (ImageModel imageModel : imageUrls) {
                            DownloadPage downloadPage = DefaultFactory.DownloadPage.constructDefault();
                            downloadPage.setSource(downloadChapter.getSource());
                            downloadPage.setUrl(imageModel.getFile_id());
                            downloadPage.setParentUrl(downloadChapter.getUrl());
                            downloadPage.setDirectory(downloadChapter.getDirectory());
                            //파일명을 파일 아이디로 변경
                            downloadPage.setName(imageModel.getFile_id());
                            downloadPage.setExt(imageModel.getName().substring(imageModel.getName().lastIndexOf(".") + 1));
                            downloadPage.setFlag(DownloadUtils.FLAG_PENDING);
                            downloadPage.setComment(imageModel.getComment());
                            downloadPage.setModified(imageModel.getModified());

                            //폴더가 존재하면
                            if (chapterDirectory.exists() && chapterDirectory.isDirectory()) {
                                //이미지가 저장된 폴더에 이미지가 있는지 확인해서 실제로 있는 것만 추가
                                File SearchFile = new File(chapterDirectory.getAbsolutePath() + "/" + imageModel.getFile_id() + "." + imageModel.getExt());
                                if (SearchFile.exists()) {
//                                    Dlog.d( "파일 있음");
//									if (downloadChapter.getSource().equals(OFFICEBOX.BASE_URL)) {
//										//OFFICEBOX암호화로 인한 DB컬럼의 filesize값이 +-32바이트 커서 별도 처리
//										if (Math.abs(SearchFile.length() - imageModel.getSize()) <= 32) {
//											downloadPage.setFlag(DownloadUtils.FLAG_COMPLETED);
//										} else {
//											downloadPage.setFlag(DownloadUtils.FLAG_FAILED);
//										}
//									} else
                                    {
                                        if (SearchFile.length() == imageModel.getSize()) {
                                            downloadPage.setFlag(DownloadUtils.FLAG_COMPLETED);
                                        } else {
                                            downloadPage.setFlag(DownloadUtils.FLAG_FAILED);
                                        }
                                    }
                                }
                            }
                            downloadPageList.add(downloadPage);
                        }
                        ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                        SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

                        sqLiteDatabase.beginTransaction();
                        try {
							/*
                            file이 있고 사이즈가 같으면 FLAG_COMPLETED
                            file이 있고 사이즈가 다르면 FLAG_FAILED
                            file없으면  FLAG_PENDING
                             */
                            for (DownloadPage downloadPage : downloadPageList) {
                                if (downloadPage.getFlag() == DownloadUtils.FLAG_PENDING) {
                                    //add
                                    ContentValues updateValues = new ContentValues(2);
                                    updateValues.put(ApplicationContract.DownloadPage.COLUMN_FLAG, downloadPage.getFlag());
                                    updateValues.put(ApplicationContract.DownloadPage.COLUMN_SOURCE, downloadPage.getSource());
                                    int updatecnt = cupboard().withDatabase(sqLiteDatabase).update(DownloadPage.class, updateValues, ApplicationContract.DownloadPage.COLUMN_URL + " = ?", downloadPage.getUrl());
                                    if (updatecnt == 0)
                                        cupboard().withDatabase(sqLiteDatabase).put(downloadPage);

                                } else if (downloadPage.getFlag() == DownloadUtils.FLAG_COMPLETED || downloadPage.getFlag() == DownloadUtils.FLAG_FAILED) {
                                    //update
                                    ContentValues updateValues = new ContentValues(2);
                                    updateValues.put(ApplicationContract.DownloadPage.COLUMN_FLAG, downloadPage.getFlag());
                                    updateValues.put(ApplicationContract.DownloadPage.COLUMN_SOURCE, downloadPage.getSource());
                                    cupboard().withDatabase(sqLiteDatabase).update(DownloadPage.class, updateValues, ApplicationContract.DownloadPage.COLUMN_URL + " = ?", downloadPage.getUrl());
                                }

                            }
                            sqLiteDatabase.setTransactionSuccessful();
                        } finally {
                            sqLiteDatabase.endTransaction();
                        }

                        subscriber.onNext(downloadPageList);
                        subscriber.onCompleted();
                    } catch (Throwable e) {
                        subscriber.onError(e);
                    }
                }
            }
        });
    }

    public static Observable<Integer> updateDownloadPage(final String source, final String url, final ContentValues updateValues) {
        return Observable.create(new Observable.OnSubscribe<Integer>() {
            @Override
            public void call(Subscriber<? super Integer> subscriber) {
                try {
                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

                    StringBuilder selection = new StringBuilder();

                    selection
                            .append(ApplicationContract.DownloadPage.COLUMN_SOURCE + " = ?")
                            .append(" AND ")
                            .append(ApplicationContract.DownloadPage.COLUMN_URL + " = ?");

                    List<String> selectionArgs = new ArrayList<String>();
                    selectionArgs.add(String.valueOf(source));
                    selectionArgs.add(String.valueOf(url));

                    int amountUpdated = cupboard().withDatabase(sqLiteDatabase).update(
                            DownloadPage.class
                            , updateValues
                            , selection.toString()
                            , selectionArgs.toArray(new String[selectionArgs.size()]));
                    subscriber.onNext(amountUpdated);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static Observable<Integer> updateDownloadPage(final DirectFolderRequestWrapper request, final ContentValues updateValues) {
        return Observable.create(new Observable.OnSubscribe<Integer>() {
            @Override
            public void call(Subscriber<? super Integer> subscriber) {
                try {
                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();


                    StringBuilder selection = new StringBuilder();

                    selection
                            .append(ApplicationContract.DownloadPage.COLUMN_PARENT_URL + " = ?")
                            .append(" AND ")
                            .append(ApplicationContract.DownloadPage.COLUMN_SOURCE + " = ?");

                    List<String> selectionArgs = new ArrayList<String>();
                    selectionArgs.add(String.valueOf(request.getUrl()));
                    selectionArgs.add(String.valueOf(request.getSource()));


                    int amountUpdated = cupboard().withDatabase(sqLiteDatabase).update(
                            DownloadPage.class
                            , updateValues
                            , selection.toString()
                            , selectionArgs.toArray(new String[selectionArgs.size()]));

                    subscriber.onNext(amountUpdated);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static Observable<Integer> deleteDownloadPagesOfDownloadChapter(final DirectFolderRequestWrapper request) {
        return Observable.create(new Observable.OnSubscribe<Integer>() {
            @Override
            public void call(Subscriber<? super Integer> subscriber) {
                try {
                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

                    int amountDeleted = cupboard().withDatabase(sqLiteDatabase).delete(DownloadPage.class, ApplicationContract.DownloadPage.COLUMN_PARENT_URL + " = ?", request.getUrl());

                    subscriber.onNext(amountDeleted);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static Observable<Integer> deleteDownloadPage(final DirectFolderRequestWrapper request) {
        return Observable.create(new Observable.OnSubscribe<Integer>() {
            @Override
            public void call(Subscriber<? super Integer> subscriber) {
                try {
                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

                    StringBuilder selection = new StringBuilder();
                    selection
                            .append(ApplicationContract.DownloadPage.COLUMN_NAME + " = ?")
                            .append(" AND ")
                            .append(ApplicationContract.DownloadPage.COLUMN_SOURCE + " = ?");

                    List<String> selectionArgs = new ArrayList<String>();
                    selectionArgs.add(String.valueOf(request.getUrl()));
                    selectionArgs.add(String.valueOf(request.getSource()));


                    int amountDeleted = cupboard().withDatabase(sqLiteDatabase).delete(
                            DownloadPage.class
                            , selection.toString()
                            , selectionArgs.toArray(new String[selectionArgs.size()]));


                    subscriber.onNext(amountDeleted);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }


    public static Observable<Integer> deleteDownloadChapter(final DirectFolderRequestWrapper request) {
        return Observable.create(new Observable.OnSubscribe<Integer>() {
            @Override
            public void call(Subscriber<? super Integer> subscriber) {
                try {
                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

                    StringBuilder selection = new StringBuilder();
                    List<String> selectionArgs = new ArrayList<String>();

                    selection.append(ApplicationContract.DownloadManga.COLUMN_SOURCE + " = ?");
                    selection.append(" AND ").append(ApplicationContract.DownloadManga.COLUMN_URL + " = ?");

                    int amountDeleted = cupboard().withDatabase(sqLiteDatabase).delete(DownloadChapter.class, selection.toString(), request.getSource(), request.getUrl());

                    subscriber.onNext(amountDeleted);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }
//
//    public static Observable<Integer> deleteAllDownloadPagesOfDownloadChapter() {
//        return Observable.create(new Observable.OnSubscribe<Integer>() {
//            @Override
//            public void call(Subscriber<? super Integer> subscriber) {
//                try {
//                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
//                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();
//
//                    int amountDeleted = cupboard().withDatabase(sqLiteDatabase).delete(DownloadPage.class, null, null);
//
//                    subscriber.onNext(amountDeleted);
//                    subscriber.onCompleted();
//                } catch (Throwable e) {
//                    subscriber.onError(e);
//                }
//            }
//        });
//    }

    public static Observable<Boolean> queryDownloadPageExist(String file_id, long Size) {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                try {
                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

                    StringBuilder selection = new StringBuilder();
                    List<String> selectionArgs = new ArrayList<String>();

                    selection.append(ApplicationContract.DownloadPage.COLUMN_NAME + " == ?");
                    selectionArgs.add(file_id);

                    Cursor findfile = cupboard().withDatabase(sqLiteDatabase).query(DownloadPage.class)
                            .withSelection(selection.toString(), selectionArgs.toArray(new String[selectionArgs.size()]))
                            .getCursor();

                    boolean file_Exist = false;
                    if (findfile != null) {
                        try {
                            if (findfile.getCount() == 1) {
                                DownloadPage downloadPage = toObject(findfile, DownloadPage.class);
                                String filepath = downloadPage.getDirectory();
                                File file = new File(filepath + "/" + downloadPage.getName() + "." + downloadPage.getExt());

                                if (file.exists() && Math.abs(file.length() - Size) <= 32) {
                                    file_Exist = true;
                                } else {
                                    file_Exist = false;
                                }
                            }
                        } finally {
                            if (findfile != null) {
                                findfile.close();
                            }
                        }
                    }

                    subscriber.onNext(file_Exist);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static Observable<Pair<Boolean, String>> queryDownloadPageExistwithPath(String file_id, long Size) {
        return Observable.create(new Observable.OnSubscribe<Pair<Boolean, String>>() {
            @Override
            public void call(Subscriber<? super Pair<Boolean, String>> subscriber) {
                try {
                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

                    StringBuilder selection = new StringBuilder();
                    List<String> selectionArgs = new ArrayList<String>();

                    selection.append(ApplicationContract.DownloadPage.COLUMN_NAME + " == ?");
                    selectionArgs.add(file_id);

                    Cursor findfile = cupboard().withDatabase(sqLiteDatabase).query(DownloadPage.class)
                            .withSelection(selection.toString(), selectionArgs.toArray(new String[selectionArgs.size()]))
                            .getCursor();

                    boolean file_Exist = false;
                    String filepath = "";
                    if (findfile != null) {
                        try {
                            if (findfile.getCount() == 1) {
                                DownloadPage downloadPage = toObject(findfile, DownloadPage.class);
                                String fileDirpath = downloadPage.getDirectory();
                                File file = new File(fileDirpath + "/" + downloadPage.getName() + "." + downloadPage.getExt());

                                if (file.exists() && Math.abs(file.length() - Size) <= 32) {
                                    file_Exist = true;
                                    filepath = file.getAbsolutePath();
                                } else {
                                    file_Exist = false;
                                }
                            }
                        } finally {
                            if (findfile != null) {
                                findfile.close();
                            }
                        }
                    }

                    subscriber.onNext(new Pair<>(file_Exist, filepath));
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static Observable<Boolean> queryShouldDownloadServiceStop() {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                try {
                    boolean shouldStop = false;

                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();
                    StringBuilder selection = new StringBuilder();
                    List<String> selectionArgs = new ArrayList<String>();

                    selection.append(ApplicationContract.DownloadChapter.COLUMN_FLAG + " != ?");
                    selectionArgs.add(String.valueOf(DownloadUtils.FLAG_COMPLETED));
                    selection.append(" AND ").append(ApplicationContract.DownloadChapter.COLUMN_FLAG + " != ?");
                    selectionArgs.add(String.valueOf(DownloadUtils.FLAG_CANCELED));

                    Cursor notCompletedCursor = cupboard().withDatabase(sqLiteDatabase).query(DownloadChapter.class)
                            .withSelection(selection.toString(), selectionArgs.toArray(new String[selectionArgs.size()]))
                            .getCursor();

                    if (notCompletedCursor != null) {
                        try {
                            if (notCompletedCursor.getCount() == 0) {
                                shouldStop = true;
                            }
                        } finally {
                            if (notCompletedCursor != null)
                                notCompletedCursor.close();
                        }
                    }

                    subscriber.onNext(shouldStop);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static Observable<Boolean> queryAllowDownloadServiceToPublishDownloadChapter(final DownloadChapter downloadChapter) {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                try {
                    boolean allowPublishing = false;

                    if (downloadChapter != null) {
                        ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                        SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

                        DownloadChapter updatedDownloadChapter = cupboard().withDatabase(sqLiteDatabase).query(DownloadChapter.class)
                                .withSelection(ApplicationContract.DownloadChapter.COLUMN_ID + " = ?", String.valueOf(downloadChapter.getId()))
                                .limit(1)
                                .get();

                        if (updatedDownloadChapter != null) {
                            if (updatedDownloadChapter.getFlag() != DownloadUtils.FLAG_CANCELED) {
                                allowPublishing = true;
                            }
                        }
                    }

                    subscriber.onNext(allowPublishing);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static <T> T toObject(Cursor objectCursor, Class<T> classType) {
        return cupboard().withCursor(objectCursor).get(classType);
    }

    public static <T> List<T> toList(Cursor listCursor, Class<T> classType) {
        return cupboard().withCursor(listCursor).list(classType);
    }

    public static void putObjectToApplicationDatabase(Object object) {
        ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
        SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

        cupboard().withDatabase(sqLiteDatabase).put(object);
    }

    public static void deleteObjectToApplicationDatabase(Object object) {
        ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
        SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

        cupboard().withDatabase(sqLiteDatabase).delete(object);
    }
}
