package com.jiransoft.officedisplay.controllers.events;

public class PhotoOnTabEvent {

    Direction direction;

    public PhotoOnTabEvent(Direction direction) {
        this.direction = direction;
    }

    public Direction getSelectPage() {
        return this.direction;
    }

    public enum Direction {Left, Center, Right}
}
