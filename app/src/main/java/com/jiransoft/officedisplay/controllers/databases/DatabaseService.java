package com.jiransoft.officedisplay.controllers.databases;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.jiransoft.officedisplay.controllers.QueryManager;
import com.jiransoft.officedisplay.controllers.events.DownloadChapterQueryEvent;
import com.jiransoft.officedisplay.controllers.wrappers.RequestWrapper;
import com.jiransoft.officedisplay.models.downloads.DownloadChapter;
import com.jiransoft.officedisplay.models.downloads.DownloadManga;
import com.jiransoft.officedisplay.utils.DiskUtils;
import com.jiransoft.officedisplay.utils.DownloadUtils;

import java.io.File;
import java.util.ArrayList;

import de.greenrobot.event.EventBus;

public class DatabaseService extends IntentService {
    public static final String TAG = DatabaseService.class.getSimpleName();

    public static final String INTENT_DELETE_DOWNLOAD_MANGA = TAG + ":" + "DeleteDownloadMangaIntent";
    public static final String INTENT_DELETE_DOWNLOAD_CHAPTERS = TAG + ":" + "DeleteDownloadChaptersIntent";

    public DatabaseService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(final Intent intent) {

        handleDeleteDownloadMangaIntent(intent);
        handleDeleteDownloadChaptersIntent(intent);
    }

    private void handleDeleteDownloadMangaIntent(Intent deleteDownloadMangaIntent) {
        Cursor downloadCursor = null;
        try {
            if (deleteDownloadMangaIntent != null) {
                if (deleteDownloadMangaIntent.hasExtra(INTENT_DELETE_DOWNLOAD_MANGA)) {
                    DownloadManga downloadMangaToDelete = deleteDownloadMangaIntent.getParcelableExtra(INTENT_DELETE_DOWNLOAD_MANGA);
                    if (downloadMangaToDelete != null) {
                        downloadCursor = QueryManager.queryDownloadChaptersOfDownloadManga(new RequestWrapper(downloadMangaToDelete.getSource(), downloadMangaToDelete.getUrl()), true)
                                .toBlocking()
                                .single();

                        if (downloadCursor != null && downloadCursor.getCount() != 0) {

                        } else {
                            QueryManager.deleteObjectToApplicationDatabase(downloadMangaToDelete);
                        }
                    }

                    deleteDownloadMangaIntent.removeExtra(INTENT_DELETE_DOWNLOAD_MANGA);
                }
            }
        } finally {
            if (downloadCursor != null)
                downloadCursor.close();
        }
    }

    private void handleDeleteDownloadChaptersIntent(Intent deleteDownloadChaptersIntent) {
        if (deleteDownloadChaptersIntent != null) {
            if (deleteDownloadChaptersIntent.hasExtra(INTENT_DELETE_DOWNLOAD_CHAPTERS)) {
                ArrayList<DownloadChapter> downloadChaptersToDelete = deleteDownloadChaptersIntent.getParcelableArrayListExtra(INTENT_DELETE_DOWNLOAD_CHAPTERS);
                if (downloadChaptersToDelete != null) {
                    ApplicationSQLiteOpenHelper applicationSQLiteOpenHelper = ApplicationSQLiteOpenHelper.getInstance();
                    SQLiteDatabase sqLiteDatabase = applicationSQLiteOpenHelper.getWritableDatabase();

                    sqLiteDatabase.beginTransaction();
                    try {
                        for (DownloadChapter downloadChapter : downloadChaptersToDelete) {
                            if (downloadChapter != null) {
                                DiskUtils.deleteFiles(new File(downloadChapter.getDirectory()));
                                QueryManager.deleteObjectToApplicationDatabase(downloadChapter);
                            }
                        }
                        sqLiteDatabase.setTransactionSuccessful();
                    } finally {
                        sqLiteDatabase.endTransaction();
                    }
                }

                EventBus.getDefault().post(new DownloadChapterQueryEvent(DownloadUtils.FLAG_DELETE, ""));

                deleteDownloadChaptersIntent.removeExtra(INTENT_DELETE_DOWNLOAD_CHAPTERS);
            }
        }
    }
}
