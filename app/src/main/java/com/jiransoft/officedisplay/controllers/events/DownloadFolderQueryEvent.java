package com.jiransoft.officedisplay.controllers.events;

public class DownloadFolderQueryEvent {
    private int mEvent;
    private String mMSG;

    public DownloadFolderQueryEvent(int event, String msg) {
        mEvent = event;
        mMSG = msg;
    }

    public int getmEvent() {
        return mEvent;
    }

    public String getmMSG() {
        return mMSG;
    }

    public void setmMSG(String mMSG) {
        this.mMSG = mMSG;
    }
}
