package com.jiransoft.officedisplay.controllers.wrappers;

import android.os.Parcel;
import android.os.Parcelable;

public class DirectFolderRequestWrapper implements Parcelable {
    public static final String TAG = DirectFolderRequestWrapper.class.getSimpleName();
    public static final String PARCELABLE_KEY = TAG + ":" + "ParcelableKey";
    public static final Creator<DirectFolderRequestWrapper> CREATOR = new Creator<DirectFolderRequestWrapper>() {
        public DirectFolderRequestWrapper createFromParcel(Parcel source) {
            return new DirectFolderRequestWrapper(source);
        }

        public DirectFolderRequestWrapper[] newArray(int size) {
            return new DirectFolderRequestWrapper[size];
        }
    };

    private String mSource;
    private String mParentUrl;
    private String mUrl;
    private String mName;

    public DirectFolderRequestWrapper(String source, String url, String name) {
        mSource = source;
        mUrl = url;
        mName = name;
    }

    public DirectFolderRequestWrapper(String source, String ParentUrl, String url, String name) {
        mParentUrl = ParentUrl;
        mSource = source;
        mUrl = url;
        mName = name;
    }

    protected DirectFolderRequestWrapper(Parcel in) {
        this.mSource = in.readString();
        this.mParentUrl = in.readString();
        this.mUrl = in.readString();
        this.mName = in.readString();
    }

    public String getSource() {
        return mSource;
    }

    public void setSource(String mSource) {
        this.mSource = mSource;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getmParentUrl() {
        return mParentUrl;
    }

    public void setmParentUrl(String mParentUrl) {
        this.mParentUrl = mParentUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mSource);
        dest.writeString(this.mParentUrl);
        dest.writeString(this.mUrl);
        dest.writeString(this.mName);
    }

}
