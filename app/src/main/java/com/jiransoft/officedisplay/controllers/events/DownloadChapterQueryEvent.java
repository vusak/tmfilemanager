package com.jiransoft.officedisplay.controllers.events;

public class DownloadChapterQueryEvent {
    private int mEvent;
    private String mMSG;

    public DownloadChapterQueryEvent(int event, String msg) {
        mEvent = event;
        mMSG = msg;
    }

    public int getmEvent() {
        return mEvent;
    }

    public String getmMSG() {
        return mMSG;
    }

    public void setmMSG(String mMSG) {
        this.mMSG = mMSG;
    }
}
