package com.jiransoft.officedisplay.controllers.database;

import android.content.Context;

import com.jiransoft.officedisplay.models.ImageModel;
import com.jiransoft.officedisplay.models.ProjectModel;
import com.jiransoft.officedisplay.utils.PreferenceHelper;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import rx.Observable;

/**
 * Created by user on 2015-10-27.
 */
public class DataService_Realm {

    private final Context context;

    public DataService_Realm(Context context) {
        this.context = context;
    }

    private static ProjectModel ProjectFromRealm(Project_Realm project_realm) {
        return new ProjectModel(project_realm.getAccess_key(),
                project_realm.getAuth(),
                project_realm.getBeacon_id(),
                project_realm.getExpired(),
                project_realm.isExpired_flag(),
                project_realm.getId(),
                project_realm.is_member(),
                project_realm.getLast_thumb_fid(),
                project_realm.getMember_count(),
                project_realm.getModified(),
                project_realm.getName(),
                project_realm.getOwner(),
                project_realm.isPublic(),
                project_realm.getPublic_auth(),
                project_realm.getLogin_id(),
                project_realm.isNew()
        );
    }

    private static ProjectModel ChannelFromRealm(channel_Realm channel_Realm) {
        return new ProjectModel(channel_Realm.getAccess_key(),
                channel_Realm.getAuth(),
                channel_Realm.getBeacon_id(),
                channel_Realm.getExpired(),
                channel_Realm.isExpired_flag(),
                channel_Realm.getId(),
                channel_Realm.is_member(),
                channel_Realm.getLast_thumb_fid(),
                channel_Realm.getMember_count(),
                channel_Realm.getModified(),
                channel_Realm.getName(),
                channel_Realm.getOwner(),
                channel_Realm.isPublic(),
                channel_Realm.getPublic_auth(),
                channel_Realm.getLogin_id(),
                channel_Realm.isNew()
        );
    }

    private static ImageModel ProjectImageFromRealm(Project_image_Realm project_image_realm) {
        return new ImageModel(
                project_image_realm.getProject_id(),
                project_image_realm.getComment(),
                project_image_realm.getEmail(),
                project_image_realm.getExt(),
                project_image_realm.getFile_id(),
                project_image_realm.getModified(),
                project_image_realm.getName(),
                project_image_realm.getNick_name(),
                project_image_realm.getSize(),
                project_image_realm.getThumb(),
                project_image_realm.getFlag(),
                project_image_realm.getUrl()
        );
    }

    public Observable<List<ProjectModel>> ProjectListList() {
        return Realm.getDefaultInstance().where(Project_Realm.class).equalTo("login_id", PreferenceHelper.getString(PreferenceHelper.id, ""))
                .findAll().asObservable().map(realmProjects -> {
                    // map them to UI objects
                    final List<ProjectModel> folders = new ArrayList<ProjectModel>(realmProjects.size());
                    for (Project_Realm realmProject : realmProjects) {
                        folders.add(ProjectFromRealm(realmProject));
                    }
                    return folders;
                });
    }

    public Observable<List<ProjectModel>> ChannelListList() {
        return Realm.getDefaultInstance().where(channel_Realm.class).equalTo("login_id", "channel@jiran.com").findAll().asObservable()
                .map(realmProjects -> {
                    // map them to UI objects
                    final List<ProjectModel> folders = new ArrayList<ProjectModel>(realmProjects.size());
                    for (channel_Realm realmProject : realmProjects) {
                        folders.add(ChannelFromRealm(realmProject));
                    }
                    return folders;
                });
    }


    public Observable<ArrayList<ImageModel>> ImageListList(String project_id, int Limit) {
        return Realm.getDefaultInstance().where(Project_image_Realm.class).equalTo("project_id", project_id).findAll().asObservable()
                .map(project_image_realms -> {
                    // map them to UI objects
                    final ArrayList<ImageModel> Images = new ArrayList<>(project_image_realms.size());
                    for (Project_image_Realm project_image_realm : project_image_realms) {
                        Images.add(ProjectImageFromRealm(project_image_realm));
                        if (Limit != 0) {
                            if (Limit == Images.size())
                                break;
                        }
                    }
                    return Images;
                });
    }


}
