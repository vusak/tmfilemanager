package com.jiransoft.officedisplay.controllers.events;

public class AutoSlidePlayEvent {
    public static final int FLAG_PLAY = -200;
    public static final int FLAG_PAUSED = -100;

    private int mEvent;

    public AutoSlidePlayEvent(int event) {
        mEvent = event;
    }

    public int getmEvent() {
        return mEvent;
    }

}
