package com.jiransoft.officedisplay.controllers.events;

public class MoviePlayEvent {
    public static final int FLAG_PLAY = -200;
    public static final int FLAG_PAUSED = -100;
    public static final int FLAG_COMPLETED = 200;

    private int mEvent;
    private String mTag;

    public MoviePlayEvent(int event, String tag) {
        mEvent = event;
        mTag = tag;
    }

    public int getmEvent() {
        return mEvent;
    }

    public String getmTag() {
        return mTag;
    }
}
