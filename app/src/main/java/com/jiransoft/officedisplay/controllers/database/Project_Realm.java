package com.jiransoft.officedisplay.controllers.database;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by user on 2015-10-27.
 */
public class Project_Realm extends RealmObject {
    private String access_key;
    private String auth;
    private String beacon_id;
    private String expired;
    private boolean expired_flag;
    @PrimaryKey
    @Required
    private String id;
    private String login_id; //로그인 아이디
    private boolean is_member;
    private String last_thumb_fid;
    private int member_count;
    private String modified;
    private String name;
    private String owner;
    private boolean Public;
    private String public_auth;
    private boolean New; //업데이트 되었는지


    private int flag;
    private String Directory;

    public String getAccess_key() {
        return access_key;
    }

    public void setAccess_key(String access_key) {
        this.access_key = access_key;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getBeacon_id() {
        return beacon_id;
    }

    public void setBeacon_id(String beacon_id) {
        this.beacon_id = beacon_id;
    }

    public String getExpired() {
        return expired;
    }

    public void setExpired(String expired) {
        this.expired = expired;
    }

    public boolean isExpired_flag() {
        return expired_flag;
    }

    public void setExpired_flag(boolean expired_flag) {
        this.expired_flag = expired_flag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean is_member() {
        return is_member;
    }

    public void setIs_member(boolean is_member) {
        this.is_member = is_member;
    }

    public String getLast_thumb_fid() {
        return last_thumb_fid;
    }

    public void setLast_thumb_fid(String last_thumb_fid) {
        this.last_thumb_fid = last_thumb_fid;
    }

    public int getMember_count() {
        return member_count;
    }

    public void setMember_count(int member_count) {
        this.member_count = member_count;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public boolean isPublic() {
        return Public;
    }

    public void setPublic(boolean Public) {
        this.Public = Public;
    }

    public String getPublic_auth() {
        return public_auth;
    }

    public void setPublic_auth(String public_auth) {
        this.public_auth = public_auth;
    }


    public boolean isNew() {
        return New;
    }

    public void setNew(boolean aNew) {
        New = aNew;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getDirectory() {
        return Directory;
    }

    public void setDirectory(String directory) {
        Directory = directory;
    }

    public String getLogin_id() {
        return login_id;
    }

    public void setLogin_id(String login_id) {
        this.login_id = login_id;
    }
}
