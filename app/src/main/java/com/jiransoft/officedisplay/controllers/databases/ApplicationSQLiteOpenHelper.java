package com.jiransoft.officedisplay.controllers.databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.jiransoft.officedisplay.models.downloads.DownloadChapter;
import com.jiransoft.officedisplay.models.downloads.DownloadManga;
import com.jiransoft.officedisplay.models.downloads.DownloadPage;
import com.jiransoft.officedisplay.models.downloads.DownloadQueue;

import jiran.com.tmfilemanager.MyApplication;
import nl.qbusict.cupboard.Cupboard;
import nl.qbusict.cupboard.CupboardBuilder;

public class ApplicationSQLiteOpenHelper extends SQLiteOpenHelper {
    private static ApplicationSQLiteOpenHelper sInstance;

    public ApplicationSQLiteOpenHelper(Context context) {
        super(context, ApplicationContract.DATABASE_NAME, null, ApplicationContract.DATABASE_VERSION);
    }

    public static synchronized ApplicationSQLiteOpenHelper getInstance() {
        if (sInstance == null) {
            sInstance = new ApplicationSQLiteOpenHelper(MyApplication.getInstance());
        }

        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Cupboard applicationCupboard = constructCustomCupboard();
        applicationCupboard.withDatabase(db).createTables();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Cupboard applicationCupboard = constructCustomCupboard();
        applicationCupboard.withDatabase(db).upgradeTables();
    }

    private Cupboard constructCustomCupboard() {
        Cupboard customCupboard = new CupboardBuilder().build();
        customCupboard.register(DownloadManga.class);
        customCupboard.register(DownloadChapter.class);
        customCupboard.register(DownloadPage.class);
        customCupboard.register(DownloadQueue.class);

        return customCupboard;
    }
}
