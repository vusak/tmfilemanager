package com.jiransoft.officedisplay.controllers.events;

public class MovieLoadingEvent {
    public static final int FLAG_COMPLETED = 200;
    public static final int FLAG_PREPARED = 300;


    private int mEvent;
    private String mTag;

    public MovieLoadingEvent(int event, String tag) {
        mEvent = event;
        mTag = tag;
    }

    public int getmEvent() {
        return mEvent;
    }

    public String getmTag() {
        return mTag;
    }
}
