package com.jiransoft.officedisplay.controllers.factories;


import com.jiransoft.officedisplay.utils.DownloadUtils;

public class DefaultFactory {
    private DefaultFactory() {
        throw new AssertionError();
    }

    public static final class DownloadChapter {
        public static final String DEFAULT_SOURCE = "No Source";
        public static final String DEFAULT_URL = "No Url";
        public static final String DEFAULT_PARENT_URL = "No Parent Url";

        public static final String DEFAULT_NAME = "No Name";

        public static final String DEFAULT_DIRECTORY = "No Directory";

        public static final int DEFAULT_CURRENT_PAGE = 0;
        public static final int DEFAULT_TOTAL_PAGES = 0;
        public static final int DEFAULT_FLAG = DownloadUtils.FLAG_FAILED;

        private DownloadChapter() {
            throw new AssertionError();
        }

        public static com.jiransoft.officedisplay.models.downloads.DownloadChapter constructDefault() {
            com.jiransoft.officedisplay.models.downloads.DownloadChapter newInstance = new com.jiransoft.officedisplay.models.downloads.DownloadChapter();

            newInstance.setSource(DEFAULT_SOURCE);
            newInstance.setUrl(DEFAULT_URL);
            newInstance.setParentUrl(DEFAULT_PARENT_URL);

            newInstance.setName(DEFAULT_NAME);

            newInstance.setDirectory(DEFAULT_DIRECTORY);

            newInstance.setCurrentPage(DEFAULT_CURRENT_PAGE);
            newInstance.setTotalPages(DEFAULT_TOTAL_PAGES);
            newInstance.setFlag(DEFAULT_FLAG);

            return newInstance;
        }
    }

    public static final class DownloadPage {
        public static final String DEFAULT_URL = "No Url";
        public static final String DEFAULT_PARENT_URL = "No Parent Url";

        public static final String DEFAULT_DIRECTORY = "No Directory";

        public static final String DEFAULT_NAME = "No Name";
        public static final String DEFAULT_COMMENT = "";
        public static final String DEFAULT_MODIFIED = "";

        public static final int DEFAULT_FLAG = DownloadUtils.FLAG_FAILED;

        private DownloadPage() {
            throw new AssertionError();
        }

        public static com.jiransoft.officedisplay.models.downloads.DownloadPage constructDefault() {
            com.jiransoft.officedisplay.models.downloads.DownloadPage newInstance = new com.jiransoft.officedisplay.models.downloads.DownloadPage();

            newInstance.setUrl(DEFAULT_URL);
            newInstance.setParentUrl(DEFAULT_PARENT_URL);

            newInstance.setDirectory(DEFAULT_DIRECTORY);

            newInstance.setName(DEFAULT_NAME);

            newInstance.setFlag(DEFAULT_FLAG);
            newInstance.setComment(DEFAULT_COMMENT);
            newInstance.setModified(DEFAULT_MODIFIED);


            return newInstance;
        }
    }

    public static final class DownloadQueue {
        public static final String DEFAULT_URL = "No Url";
        public static final String DEFAULT_PARENT_URL = "No Parent Url";

        public static final String DEFAULT_DIRECTORY = "No Directory";

        public static final String DEFAULT_NAME = "No Name";

        public static final int DEFAULT_FLAG = DownloadUtils.FLAG_FAILED;

        private DownloadQueue() {
            throw new AssertionError();
        }

        public static com.jiransoft.officedisplay.models.downloads.DownloadQueue constructDefault() {
            com.jiransoft.officedisplay.models.downloads.DownloadQueue newInstance = new com.jiransoft.officedisplay.models.downloads.DownloadQueue();

            newInstance.setUrl(DEFAULT_URL);
            newInstance.setParentUrl(DEFAULT_PARENT_URL);

            newInstance.setDirectory(DEFAULT_DIRECTORY);

            newInstance.setName(DEFAULT_NAME);

            newInstance.setFlag(DEFAULT_FLAG);

            return newInstance;
        }
    }
}
