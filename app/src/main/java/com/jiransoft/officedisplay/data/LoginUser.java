package com.jiransoft.officedisplay.data;

import android.util.Log;

import java.io.Serializable;


public class LoginUser implements Serializable {

    public static final long serialVersionUID = 1L;
    private final String tag = this.getClass().getName();
    private final boolean isDebug = false;
    private String userId;
    private String userName;
    private String userSessionId;
    private boolean pwdTimeLimitExceed;
    private boolean pwdSecure;
    private boolean authCreate;
    private boolean authUpload;
    private boolean authDownload;
    private boolean authReadonly;
    private boolean authFileLink;
    private boolean authGuestFolder;
    private boolean authSharedFolder;
    private boolean admin;
    private boolean license;

    public LoginUser() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String data) {
        this.userId = data;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String data) {
        this.userName = data;
    }

    public String getUserSessionId() {
        return userSessionId;
    }

    public void setUserSessionId(String data) {
        this.userSessionId = data;
    }

    public boolean getPwdTimeLimitExceed() {
        return pwdTimeLimitExceed;
    }

    public void setPwdTimeLimitExceed(boolean data) {
        this.pwdTimeLimitExceed = data;
    }

    public boolean getPwdSecure() {
        return pwdSecure;
    }

    public void setPwdSecure(boolean data) {
        this.pwdSecure = data;
    }

    public boolean getAuthCreate() {
        return authCreate;
    }

    public void setAuthCreate(boolean data) {
        this.authCreate = data;
    }

    public boolean getAuthUpload() {
        return authUpload;
    }

    public void setAuthUpload(boolean data) {
        this.authUpload = data;
    }

    public boolean getAuthDownload() {
        return authDownload;
    }

    public void setAuthDownload(boolean data) {
        this.authDownload = data;
    }

    public boolean getAuthReadonly() {
        return authReadonly;
    }

    public void setAuthReadonly(boolean data) {
        this.authReadonly = data;
    }

    public boolean getAuthFileLink() {
        return authFileLink;
    }

    public void setAuthFileLink(boolean data) {
        this.authFileLink = data;
    }

    public boolean getAuthGuestFolder() {
        return authGuestFolder;
    }

    public void setAuthGuestFolder(boolean data) {
        this.authGuestFolder = data;
    }

    public boolean getAuthSharedFolder() {
        return authSharedFolder;
    }

    public void setAuthSharedFolder(boolean data) {
        this.authSharedFolder = data;
    }

    public boolean getAdmin() {
        return admin;
    }

    public void setAdmin(boolean data) {
        this.admin = data;
    }

    public boolean getLicense() {
        return license;
    }

    public void setLicense(boolean data) {
        this.license = data;
    }

    public void log(String msg) {
        if (isDebug)
            Log.i(tag, msg);
    }

}
