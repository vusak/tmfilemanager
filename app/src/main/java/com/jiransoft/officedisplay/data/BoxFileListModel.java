package com.jiransoft.officedisplay.data;

import android.os.Parcel;
import android.os.Parcelable;


public class BoxFileListModel implements Parcelable {

    public static final int FOLDER = 0;
    public static final int FILE = 1;
    public static final int SERVICE = 2;
    public static final Creator<BoxFileListModel> CREATOR = new Creator<BoxFileListModel>() {
        @Override
        public BoxFileListModel createFromParcel(Parcel source) {
            return new BoxFileListModel(source);
        }

        @Override
        public BoxFileListModel[] newArray(int size) {
            return new BoxFileListModel[size];
        }
    };
    public String listType;
    public int itemType; // 0:Folder, 1:File
    public int flag; //다운로드용 플레그
    public String parentNode;
    public String itemName;
    public String itemDate;
    public String itemNode;
    public String itemCurrentNode;
    public String itemId;
    public String itemExtension;
    public String itemSizeDisplay;
    public String itemSize;
    public int itemCompSize;
    public String itemCreator;
    public String itemPath;
    public String itemImageUrl;
    public String itemLinkId;
    public String Comment;
    public boolean itemLink;
    public boolean itemThumbnail;
    public boolean itemSelected = false;
    public boolean itemValid;

    public BoxFileListModel() {

    }

    protected BoxFileListModel(Parcel in) {
        this.listType = in.readString();
        this.itemType = in.readInt();
        this.flag = in.readInt();
        this.parentNode = in.readString();
        this.itemName = in.readString();
        this.itemDate = in.readString();
        this.itemNode = in.readString();
        this.itemCurrentNode = in.readString();
        this.itemId = in.readString();
        this.itemExtension = in.readString();
        this.itemSizeDisplay = in.readString();
        this.itemSize = in.readString();
        this.itemCompSize = in.readInt();
        this.itemCreator = in.readString();
        this.itemPath = in.readString();
        this.itemImageUrl = in.readString();
        this.itemLinkId = in.readString();
        this.Comment = in.readString();
        this.itemLink = in.readByte() != 0;
        this.itemThumbnail = in.readByte() != 0;
        this.itemSelected = in.readByte() != 0;
        this.itemValid = in.readByte() != 0;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getListType() {
        return listType;
    }

    public void setListType(String listtype) {
        this.listType = listtype;
    }

    public int getType() {
        return itemType;
    }

    public void setType(int type) {
        this.itemType = type;
    }

    public String getParentNode() {
        return parentNode;
    }

    public void setParentNode(String node) {
        this.parentNode = node;
    }

    public String getName() {
        return itemName;
    }

    public void setName(String name) {
        this.itemName = name;
    }

    public String getDate() {
        return itemDate;
    }

    public void setDate(String date) {
        this.itemDate = date;
    }

    public String getNode() {
        return itemNode;
    }

    public void setNode(String node) {
        this.itemNode = node;
    }

    public String getCurrentNode() {
        return itemCurrentNode;
    }

    public void setCurrentNode(String node) {
        this.itemCurrentNode = node;
    }

    public String getId() {
        return itemId;
    }

    public void setId(String id) {
        this.itemId = id;
    }

    public String getLinkId() {
        return itemLinkId;
    }

    public void setLinkId(String linkId) {
        this.itemLinkId = linkId;
    }

    public String getExtension() {
        return itemExtension;
    }

    public void setExtension(String extension) {
        this.itemExtension = extension;
    }

    public String getSizeDP() {
        return itemSizeDisplay;
    }

    public void setSizeDP(String size) {
        this.itemSizeDisplay = size;
    }

    public String getCreator() {
        return itemCreator;
    }

    public void setCreator(String size) {
        this.itemCreator = size;
    }

    public String getSize() {
        return itemSize;
    }

    public void setSize(String size) {
        this.itemSize = size;
    }

    public int getCompSize() {
        return itemCompSize;
    }

    public void setCompSize(int size) {
        this.itemCompSize = size;
    }

    public String getPath() {
        return itemPath;
    }

    public void setPath(String path) {
        this.itemPath = path;
    }

    public boolean getLink() {
        return itemLink;
    }

    public void setLink(boolean link) {
        this.itemLink = link;
    }

    public boolean getThumbnail() {
        return itemThumbnail;
    }

    public void setThumbnail(boolean thumbnail) {
        this.itemThumbnail = thumbnail;
    }

    public boolean getSelected() {
        return itemSelected;
    }

    public void setSelected(boolean selected) {
        this.itemSelected = selected;
    }

    public boolean getValid() {
        return itemValid;
    }

    public void setValid(boolean valid) {
        this.itemValid = valid;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.listType);
        dest.writeInt(this.itemType);
        dest.writeInt(this.flag);
        dest.writeString(this.parentNode);
        dest.writeString(this.itemName);
        dest.writeString(this.itemDate);
        dest.writeString(this.itemNode);
        dest.writeString(this.itemCurrentNode);
        dest.writeString(this.itemId);
        dest.writeString(this.itemExtension);
        dest.writeString(this.itemSizeDisplay);
        dest.writeString(this.itemSize);
        dest.writeInt(this.itemCompSize);
        dest.writeString(this.itemCreator);
        dest.writeString(this.itemPath);
        dest.writeString(this.itemImageUrl);
        dest.writeString(this.itemLinkId);
        dest.writeString(this.Comment);
        dest.writeByte(this.itemLink ? (byte) 1 : (byte) 0);
        dest.writeByte(this.itemThumbnail ? (byte) 1 : (byte) 0);
        dest.writeByte(this.itemSelected ? (byte) 1 : (byte) 0);
        dest.writeByte(this.itemValid ? (byte) 1 : (byte) 0);
    }
}
