package com.jiransoft.officedisplay.data;

import java.io.Serializable;


public class UploadItem implements Serializable {
    public static final int UPLOAD_STATUS_NONE = 0;
    public static final int UPLOAD_STATUS_PROGRESS = 1;
    public static final int UPLOAD_STATUS_DONE = 2;
    public static final long serialVersionUID = 1L;

    public String itemId;
    public String itemPath;
    public String itemThumbnailPath;
    public String itemName;
    public String itemExtension;
    public String itemSize;
    public String itemDate;
    public String itemOrientation;
    public Boolean itemSelect = false;
    public Integer uploadStatus = UPLOAD_STATUS_NONE;
    private int itemUpSize;
    private int itemType;
    private int itemThumbnailType;

    public UploadItem() {
    }

    public String getId() {
        return itemId;
    }

    public void setId(String id) {
        this.itemId = id;
    }

    public String getPath() {
        return itemPath;
    }

    public void setPath(String data) {
        this.itemPath = data;
    }

    public String getThumbnailPath() {
        return itemThumbnailPath;
    }

    public void setThumbnailPath(String data) {
        this.itemThumbnailPath = data;
    }

    public String getName() {
        return itemName;
    }

    public void setName(String data) {
        this.itemName = data;
    }

    public String getExtension() {
        return itemExtension;
    }

    public void setExtension(String data) {
        this.itemExtension = data;
    }

    public String getDate() {
        return itemDate;
    }

    public void setDate(String date) {
        this.itemDate = date;
    }

    public String getSize() {
        return itemSize;
    }

    public void setSize(String data) {
        this.itemSize = data;
    }

    public String getOrientation() {
        return itemOrientation;
    }

    public void setOrientation(String data) {
        this.itemOrientation = data;
    }

    public Boolean getSelected() {
        return itemSelect;
    }

    public void setSelected(Boolean sel) {
        this.itemSelect = sel;
    }

    public int getUpSize() {
        return itemUpSize;
    }

    public void setUpSize(int size) {
        this.itemUpSize = size;
    }

    public int getType() {
        return itemType;
    }

    public void setType(int size) {
        this.itemType = size;
    }

    public int getThumbnailType() {
        return itemThumbnailType;
    }

    public void setThumbnailType(int t) {
        this.itemThumbnailType = t;
    }

    public int getStatus() {
        return uploadStatus;
    }

    public void setStatus(int data) {
        this.uploadStatus = data;
    }


}
