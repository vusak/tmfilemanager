package com.jiransoft.officedisplay.data;

import java.io.Serializable;


public class LeftMenu implements Serializable {

    public static final long serialVersionUID = 1L;

    public static final int MENU_MY_FOLDER = 0;
    public static final int MENU_SHARE_FOLDER = 1;
    public static final int MENU_GUEST_FOLDER = 2;
    public static final int MENU_RECENT_FOLDER = 3;
    public static final int MENU_SETTING = 4;
    public static int MENU_CURRENT = 0;

    public static String getRootNode(int select) {
        String node = "1";
        switch (select) {
            case LeftMenu.MENU_MY_FOLDER:
                node = "1";
                break;
            case LeftMenu.MENU_SHARE_FOLDER:
                node = "1{2";
                //node = "1%7B2";
                break;
            case LeftMenu.MENU_GUEST_FOLDER:
                node = "1{3";
                break;
//			case LeftMenu.MENU_HISTORY:
//				node = "1";
//				break;
//			case LeftMenu.MENU_SETTING:
//				node = "1";
//				break;
        }


        return node;
    }
}
