package com.jiransoft.officedisplay.views.base;

public interface BaseEmptyRelativeLayoutView {
    void initializeEmptyRelativeLayout();

    void hideEmptyRelativeLayout();

    void showEmptyRelativeLayout();
}
