package com.jiransoft.officedisplay.views.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

/**
 * 키보드 hide/show 감시
 * Created by user on 2015-12-15.
 */
public class ObservableKeyboardEditText extends EditText {

    public ObservableKeyboardEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }

    public ObservableKeyboardEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public ObservableKeyboardEditText(Context context) {
        super(context);

    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            dispatchKeyEvent(event);
            return false;
        }
        return super.onKeyPreIme(keyCode, event);
    }

}
