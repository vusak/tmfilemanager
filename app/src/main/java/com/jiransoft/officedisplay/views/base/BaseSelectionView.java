package com.jiransoft.officedisplay.views.base;

public interface BaseSelectionView {
    void selectAll();

    void clear();
}
