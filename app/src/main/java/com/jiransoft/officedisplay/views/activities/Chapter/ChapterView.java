package com.jiransoft.officedisplay.views.activities.Chapter;

import android.content.Intent;

import com.jiransoft.officedisplay.views.base.BaseContextView;
import com.jiransoft.officedisplay.views.base.BaseEmptyRelativeLayoutView;
import com.jiransoft.officedisplay.views.base.BaseToolbarView;

public interface ChapterView extends BaseContextView, BaseToolbarView, BaseEmptyRelativeLayoutView {
    void initializeHardwareAcceleration();

    void initializeSystemUIVisibility();

    void initializeViewPager();

    void DownloadDataChangeEmptyRelativeLayout();

    void initializeButtons();

    void initializeTextView();

    void initializeFullscreen();

    void enableFullscreen();

    void disableFullscreen();

    void setTitleText(String title);

    void setSubtitleProgressText(String Current, String progress);

    void setSubtitlePositionText(int position);

    void setImmersivePositionText(int position);

    void setOptionOrientationText(boolean isLockOrientation);

    void setOptionVisibleCommentText(boolean isLockZoom);

    void toastNotInitializedError();

    void toastChapterError();

    void toastNetWorkError();

    void toastNoPreviousChapter();

    void toastNoNextChapter();

    void finishAndLaunchActivity(Intent launchIntent, boolean isFadeTransition);

    void ToggleFullscreen();

    void setOptionVisibleTimer(boolean mIsVisibleTimer);

    boolean IsFullScreen();
}
