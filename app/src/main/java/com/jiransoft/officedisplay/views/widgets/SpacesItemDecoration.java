package com.jiransoft.officedisplay.views.widgets;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by user on 2016-03-22.
 */
public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        outRect.left = space;
        outRect.right = space * 3;
        outRect.bottom = space * 3;

        // Add top margin only for the first item to avoid double space between items
//		if (parent.getChildLayoutPosition(view) == 0) {
        outRect.top = space;
//		} else {
//			outRect.top = 0;
//		}
    }
}
