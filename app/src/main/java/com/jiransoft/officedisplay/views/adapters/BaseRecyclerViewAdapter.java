package com.jiransoft.officedisplay.views.adapters;

import android.support.v7.widget.RecyclerView;

public abstract class BaseRecyclerViewAdapter<VH extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<VH> {
    public abstract Object getItem(int position);
}
