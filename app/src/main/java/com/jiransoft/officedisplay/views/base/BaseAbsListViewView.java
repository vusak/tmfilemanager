package com.jiransoft.officedisplay.views.base;

public interface BaseAbsListViewView {
    void initializeAbsListView();

    void scrollToTop();
}
