package com.jiransoft.officedisplay.views.widgets.LoadingView;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

import jiran.com.tmfilemanager.R;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;


/**
 * Created by zzz40500 on 15/4/6.
 */
public class LoadingView extends FrameLayout {

    private static final int ANIMATION_DURATION_UP = 500;
    private static final int ANIMATION_DURATION_DOWN = 500;

    private static float mDistance = 200;
    public float factor = 1.2f;
    private ImageView mShapeLoadingView;
    private ImageView mIndicationIm;
    private TextView mLoadTextView;
    private int mTextAppearance;
    private String mLoadText;
    private AnimatorSet mAnimatorSet = new AnimatorSet();

    private Subscription mFreeFallSubscription;

    public LoadingView(Context context) {
        super(context);
    }

    public LoadingView(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
        init(context, attrs);

    }

    public LoadingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LoadingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.LoadingView);
        mLoadText = typedArray.getString(R.styleable.LoadingView_loadingText);
        mTextAppearance = typedArray.getResourceId(R.styleable.LoadingView_loadingTextAppearance, -1);

        typedArray.recycle();
    }

    public int dip2px(float dipValue) {
        final float scale = getContext().getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        View view = LayoutInflater.from(getContext()).inflate(R.layout.load_view, null);

        mDistance = dip2px(54f);

        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        layoutParams.gravity = Gravity.CENTER;

        mShapeLoadingView = (ImageView) view.findViewById(R.id.shapeLoadingView);

        mIndicationIm = (ImageView) view.findViewById(R.id.indication);
        mLoadTextView = (TextView) view.findViewById(R.id.promptTV);

        if (mTextAppearance != -1) {
            mLoadTextView.setTextAppearance(getContext(), mTextAppearance);
        }
        setLoadingText(mLoadText);

        addView(view, layoutParams);


//        startLoading(0);
    }

    private void startLoading(long delay) {
        if (mAnimatorSet != null && mAnimatorSet.isRunning()) {
            return;
        }
//        this.removeCallbacks(mFreeFallRunnable);
        if (mFreeFallSubscription != null) {
            mFreeFallSubscription.unsubscribe();
            mFreeFallSubscription = null;
        }


        if (delay > 0) {
            mFreeFallSubscription = rx.Observable.just(null).delay(delay, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(o -> {
                        upThrow_Right();
                    });

        } else {
            mFreeFallSubscription = rx.Observable.just(null)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(o -> {
                        upThrow_Right();
                    });
        }
    }

    public void stopLoading() {
        if (mAnimatorSet != null) {
            if (mAnimatorSet.isRunning()) {
                mAnimatorSet.cancel();
                mAnimatorSet.removeAllListeners();
            }
            mAnimatorSet = null;
        }
        if (mFreeFallSubscription != null) {
            mFreeFallSubscription.unsubscribe();
            mFreeFallSubscription = null;
        }
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (visibility == View.VISIBLE) {
            startLoading(200);
        } else {
            stopLoading();
        }
    }

    public void setLoadingText(CharSequence loadingText) {


        if (TextUtils.isEmpty(loadingText)) {
            mLoadTextView.setVisibility(GONE);
        } else {
            mLoadTextView.setVisibility(VISIBLE);
        }

        mLoadTextView.setText(loadingText);
    }

    /**
     * 상승
     */
    public void upThrow_Right() {
        mAnimatorSet.removeAllListeners();
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(mShapeLoadingView, "translationY", mDistance, 0);
        ObjectAnimator scaleIndication = ObjectAnimator.ofFloat(mIndicationIm, "scaleX", 2, 0.2f);
        ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(mShapeLoadingView, "rotation", 0, -60);

        objectAnimator.setDuration(ANIMATION_DURATION_UP);
        objectAnimator1.setDuration(ANIMATION_DURATION_UP);
        scaleIndication.setDuration(ANIMATION_DURATION_UP);
        objectAnimator.setInterpolator(new DecelerateInterpolator(factor));
        objectAnimator1.setInterpolator(new DecelerateInterpolator(factor));
        scaleIndication.setInterpolator(new DecelerateInterpolator(factor));

        mAnimatorSet.setDuration(ANIMATION_DURATION_UP);
//        animatorSet.playTogether(/*objectAnimator, *//*objectAnimator1,*/ scaleIndication);
        mAnimatorSet.playTogether(objectAnimator, objectAnimator1, scaleIndication);


        mAnimatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                freeFall_Right();


            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mAnimatorSet.start();


    }

    /**
     * 상승
     */
    public void upThrow_Left() {
        mAnimatorSet.removeAllListeners();
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(mShapeLoadingView, "translationY", mDistance, 0);
        ObjectAnimator scaleIndication = ObjectAnimator.ofFloat(mIndicationIm, "scaleX", 2, 0.2f);
        ObjectAnimator objectAnimator1 = ObjectAnimator.ofFloat(mShapeLoadingView, "rotation", 0, 60);

        objectAnimator.setDuration(ANIMATION_DURATION_UP);
        objectAnimator1.setDuration(ANIMATION_DURATION_UP);
        scaleIndication.setDuration(ANIMATION_DURATION_UP);

        objectAnimator.setInterpolator(new DecelerateInterpolator(factor));
        objectAnimator1.setInterpolator(new DecelerateInterpolator(factor));
        scaleIndication.setInterpolator(new DecelerateInterpolator(factor));


        mAnimatorSet.setDuration(ANIMATION_DURATION_UP);
//        animatorSet.playTogether(/*objectAnimator, objectAnimator1, */scaleIndication);
        mAnimatorSet.playTogether(objectAnimator, objectAnimator1, scaleIndication);


        mAnimatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                freeFall_Left();


            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mAnimatorSet.start();


    }

    /**
     * 하락
     */
    public void freeFall_Right() {
        if (mAnimatorSet.isRunning() || mAnimatorSet.isStarted()) {
            mAnimatorSet.removeAllListeners();
        }
        ObjectAnimator translationY_Animator = ObjectAnimator.ofFloat(mShapeLoadingView, "translationY", 0, mDistance);
        ObjectAnimator scaleX_Animator = ObjectAnimator.ofFloat(mIndicationIm, "scaleX", 0.2f, 2f);
        ObjectAnimator Rotate_Animator = ObjectAnimator.ofFloat(mShapeLoadingView, "rotation", -60, 0);

        translationY_Animator.setDuration(ANIMATION_DURATION_DOWN);
        Rotate_Animator.setDuration(ANIMATION_DURATION_DOWN);
        scaleX_Animator.setDuration(ANIMATION_DURATION_DOWN);

        translationY_Animator.setInterpolator(new AccelerateInterpolator(factor));
        Rotate_Animator.setInterpolator(new AccelerateInterpolator(factor));
        scaleX_Animator.setInterpolator(new AccelerateInterpolator(factor));

        mAnimatorSet.setDuration(ANIMATION_DURATION_DOWN);
        mAnimatorSet.playTogether(translationY_Animator, Rotate_Animator, scaleX_Animator);

        mAnimatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                upThrow_Left();


            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mAnimatorSet.start();


    }

    public void freeFall_Left() {
        mAnimatorSet.removeAllListeners();
        ObjectAnimator translationY_Animator = ObjectAnimator.ofFloat(mShapeLoadingView, "translationY", 0, mDistance);
        ObjectAnimator scaleX_Animator = ObjectAnimator.ofFloat(mIndicationIm, "scaleX", 0.2f, 2f);
        ObjectAnimator Rotate_Animator = ObjectAnimator.ofFloat(mShapeLoadingView, "rotation", 60, 0);


        translationY_Animator.setDuration(ANIMATION_DURATION_DOWN);
        Rotate_Animator.setDuration(ANIMATION_DURATION_DOWN);
        scaleX_Animator.setDuration(ANIMATION_DURATION_DOWN);
        translationY_Animator.setInterpolator(new AccelerateInterpolator(factor));
        Rotate_Animator.setInterpolator(new AccelerateInterpolator(factor));
        scaleX_Animator.setInterpolator(new AccelerateInterpolator(factor));

        mAnimatorSet.setDuration(ANIMATION_DURATION_DOWN);
        mAnimatorSet.playTogether(translationY_Animator, Rotate_Animator, scaleX_Animator);

        mAnimatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                upThrow_Right();


            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mAnimatorSet.start();


    }

}
