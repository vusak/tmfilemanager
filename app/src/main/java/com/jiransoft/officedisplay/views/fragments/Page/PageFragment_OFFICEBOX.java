package com.jiransoft.officedisplay.views.fragments.Page;

import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.jiransoft.officedisplay.controllers.events.DownloadImageEvent;
import com.jiransoft.officedisplay.controllers.events.PhotoOnTabEvent;
import com.jiransoft.officedisplay.models.ImageModel;
import com.jiransoft.officedisplay.utils.DisplayUtils;
import com.jiransoft.officedisplay.utils.Dlog;
import com.jiransoft.officedisplay.utils.PreferenceUtils;
import com.jiransoft.officedisplay.views.widgets.CustomProgressbarDrawable;
import com.jiransoft.officedisplay.views.widgets.ImageDownloadListener;
import com.jiransoft.officedisplay.views.widgets.LoadingView.LoadingView;
import com.jiransoft.officedisplay.views.widgets.photodraweeview.PhotoDraweeView;

import de.greenrobot.event.EventBus;
import jiran.com.tmfilemanager.R;

public class PageFragment_OFFICEBOX extends Fragment implements ImageDownloadListener {
    public static final String TAG = PageFragment_OFFICEBOX.class.getSimpleName();

    public static final String PROJECTID_ARGUMENT_KEY = TAG + ":" + "ProjectDArgumentKey";
    public static final String IMAGE_ARGUMENT_KEY = TAG + ":" + "ImagesArgumentKey";
    public static final String OFFLINE_ARGUMENT_KEY = TAG + ":" + "OfflineArgumentKey";
    ImageRequest request;
    GenericDraweeHierarchyBuilder builder;
    GenericDraweeHierarchy hierarchy;
    DraweeController controller;
    private PhotoDraweeView mPhotoImageView;
    private LoadingView m_ImageLoad_Progress;
    private RelativeLayout mRelativeLayout_comment;
    private TextView mTextview_Comment;
    private String mProjectId;
    private ImageModel mImageModel;
    private boolean mIsOffline;

    public static PageFragment_OFFICEBOX newInstance(String ProjectId, ImageModel imageModel, boolean isOffline) {
        PageFragment_OFFICEBOX newInstance = new PageFragment_OFFICEBOX();

        Bundle arguments = new Bundle();
        arguments.putString(PROJECTID_ARGUMENT_KEY, ProjectId);
        arguments.putParcelable(IMAGE_ARGUMENT_KEY, imageModel);
        arguments.putBoolean(OFFLINE_ARGUMENT_KEY, isOffline);
        newInstance.setArguments(arguments);

        return newInstance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();
        if (arguments != null) {
            if (arguments.containsKey(PROJECTID_ARGUMENT_KEY)) {
                mProjectId = arguments.getString(PROJECTID_ARGUMENT_KEY);
            }
            if (arguments.containsKey(IMAGE_ARGUMENT_KEY)) {
                mImageModel = arguments.getParcelable(IMAGE_ARGUMENT_KEY);
            }

            if (arguments.containsKey(OFFLINE_ARGUMENT_KEY)) {
                mIsOffline = arguments.getBoolean(OFFLINE_ARGUMENT_KEY, false);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View pageView = inflater.inflate(R.layout.fragment_page, container, false);

        m_ImageLoad_Progress = (LoadingView) pageView.findViewById(R.id.loadView);
        mPhotoImageView = (PhotoDraweeView) pageView.findViewById(R.id.gestureImageView);
        mRelativeLayout_comment = (RelativeLayout) pageView.findViewById(R.id.RelativeLayout_comment);
        mTextview_Comment = (TextView) pageView.findViewById(R.id.textView_comment);

        return pageView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPhotoImageView.setMinimumScale(1.0F);
        mPhotoImageView.setOnViewTapListener((view, x, y) -> {
            EventBus.getDefault().post(new PhotoOnTabEvent(PhotoOnTabEvent.Direction.Center));
        });

        Uri uri;
        uri = Uri.parse(mImageModel.getFile_id());

        Dlog.d("uri : " + uri);
//
        request = ImageRequestBuilder.newBuilderWithSource(uri)
                .setResizeOptions(new ResizeOptions(DisplayUtils.getScreenWidth(getActivity()), DisplayUtils.getScreenHeight(getActivity())))
                .setAutoRotateEnabled(true)
                .build();

//        <item>CENTER_CROP</item>
//        <item>CENTER_INSIDE</item>
//        <item>FIT_XY</item>
        ScalingUtils.ScaleType ScaleType = ScalingUtils.ScaleType.FIT_CENTER;
        if (PreferenceUtils.getImageScaleType().equals("CENTER_CROP")) {
            ScaleType = ScalingUtils.ScaleType.CENTER_CROP;
        } else if (PreferenceUtils.getImageScaleType().equals("FIT_CENTER")) {
            ScaleType = ScalingUtils.ScaleType.FIT_CENTER;
        }

        builder = new GenericDraweeHierarchyBuilder(getResources());
        hierarchy = builder.setActualImageScaleType(ScaleType)
                .setProgressBarImage(new CustomProgressbarDrawable(this))
                .build();

        controller = Fresco.newDraweeControllerBuilder()
                .setImageRequest(request)
                .setOldController(mPhotoImageView.getController())
//                .setAutoPlayAnimations(true)
                .setControllerListener(new BaseControllerListener<ImageInfo>() {
                    @Override
                    public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                        super.onFinalImageSet(id, imageInfo, animatable);
                        if (imageInfo == null || mPhotoImageView == null) {
                            return;
                        }
                        mPhotoImageView.update(imageInfo.getWidth(), imageInfo.getHeight());
                    }
                })
                .build();

        mPhotoImageView.setHierarchy(hierarchy);
        mPhotoImageView.setController(controller);


//		if (PreferenceUtils.isVisibleComment() && (!mImageModel.getComment().equals("No message") && !mImageModel.getComment().equals("")))
//			mTextview_Comment.setText(mImageModel.getComment());
//		else
        mRelativeLayout_comment.setVisibility(View.GONE);

    }

    @Override
    public void onUpdate(int progress) {
        Dlog.d("onUpdate: " + progress);
//        Dlog.d( "mImageModel.getFile_id(): "+mImageModel.getFile_id());
        m_ImageLoad_Progress.setLoadingText("Download " + progress + "%");
        EventBus.getDefault().post(new DownloadImageEvent(true, mImageModel.getFile_id()));

        if (progress == 100) {
            m_ImageLoad_Progress.setVisibility(View.GONE);
            EventBus.getDefault().post(new DownloadImageEvent(false, mImageModel.getFile_id()));
        } else {
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
