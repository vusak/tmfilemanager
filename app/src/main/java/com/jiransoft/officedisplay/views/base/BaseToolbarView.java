package com.jiransoft.officedisplay.views.base;

public interface BaseToolbarView {
    void initializeToolbar();
}
