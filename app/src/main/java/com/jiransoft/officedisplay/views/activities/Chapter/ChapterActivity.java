package com.jiransoft.officedisplay.views.activities.Chapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jiransoft.officedisplay.controllers.wrappers.DirectFolderRequestWrapper;
import com.jiransoft.officedisplay.presenters.Chapter.ChapterPresenter;
import com.jiransoft.officedisplay.presenters.Chapter.officedisplay.ChapterPresenterDirectDisplay_offlineImpl;
import com.jiransoft.officedisplay.presenters.mapper.ChapterMapper;
import com.jiransoft.officedisplay.utils.Dlog;
import com.jiransoft.officedisplay.utils.DownloadUtils;
import com.jiransoft.officedisplay.utils.PageChangeTransFormer;
import com.jiransoft.officedisplay.utils.PreferenceHelper;
import com.jiransoft.officedisplay.utils.PreferenceUtils;
import com.jiransoft.officedisplay.views.activities.BaseActivity;
import com.jiransoft.officedisplay.views.widgets.CustomDurationViewPager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import io.realm.Realm;
import jiran.com.tmfilemanager.R;
import me.grantland.widget.AutofitTextView;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

//import butterknife.Bind;

public class ChapterActivity extends BaseActivity implements ChapterView, ChapterMapper {
    public static final String TAG = ChapterActivity.class.getSimpleName();

    public static final String PRESENTER_ARGUMENT_KEY = TAG + ":" + "PresenterArgumentKey";
    public static final String REQUEST_ARGUMENT_KEY = TAG + ":" + "RequestArgumentKey";
    public static final String POSITION_ARGUMENT_KEY = TAG + ":" + "PositionArgumentKey";
    @BindView(R.id.mainToolbar)
    Toolbar mToolbar;
    @BindView(R.id.viewPager)
    CustomDurationViewPager mViewPager;
    @BindView(R.id.emptyRelativeLayout)
    RelativeLayout mEmptyRelativeLayout;
    @BindView(R.id.emptyImageView)
    ImageView mEmptyImageView;
    @BindView(R.id.emptyTextView)
    TextView mEmptyTextView;
    @BindView(R.id.instructionsTextView)
    TextView mInstructionsTextView;
//	//Add
//	private MenuItem mPageAnimationMenuItem;
//	private MenuItem mPageChangeTimeMenuItem;
//	private MenuItem mPageScaleTypeMenuItem;
    @BindView(R.id.previousButton)
    FloatingActionButton mPreviousButton;
    @BindView(R.id.nextButton)
    FloatingActionButton mNextButton;
    @BindView(R.id.numberTextView)
    TextView mPageNumberView;
    //setting for clock
    @BindView(R.id.time_frameLayout)
    FrameLayout mTimerView;
    @BindView(R.id.hourText)
    TextView mHourText;
    @BindView(R.id.minuteText)
    TextView mMinuteText;
    @BindView(R.id.amPmText)
    TextView mAmPmText;
    @BindView(R.id.date)
    TextView mDate;
    //message
    Realm realm;
    Subscription Messagelist_FCM_Subscription;
    Subscription MessagelistSubscription;
    Subscription MessageCheckSubscription;
    @BindView(R.id.frame_wellcome)
    FrameLayout frameLayout_wellcome;
    @BindView(R.id.textview_wellcomemessage)
    AutofitTextView textView_wellcome;
    private Subscription mInitializeFullscreen;
    private ChapterPresenter mChapterPresenter;
    private MenuItem mOrientationMenuItem;
    private MenuItem mVisibleCommentMenuItem;
    private MenuItem mVisibleTimerMenuItem;
    private boolean mSystemUIVisibility;
    private BroadcastReceiver mMinuteReceiver;
    private String mSeperator = ":"; // Seperator for the clock
    private boolean mClockFormatIs24 = false; // defines if the clock-format is 24-hours or 12-hours
    private int mOptionSelected = 1; // saves the option checked in the clock settings

    public static Intent constructFolderChapterActivityIntent(Context context, DirectFolderRequestWrapper chapterRequest, int position) {
        Intent argumentIntent = new Intent(context, ChapterActivity.class);
        argumentIntent.putExtra(PRESENTER_ARGUMENT_KEY, ChapterPresenterDirectDisplay_offlineImpl.TAG);
        argumentIntent.putExtra(REQUEST_ARGUMENT_KEY, chapterRequest);
        argumentIntent.putExtra(POSITION_ARGUMENT_KEY, position);

        return argumentIntent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra(PRESENTER_ARGUMENT_KEY)) {
                mChapterPresenter = new ChapterPresenterDirectDisplay_offlineImpl(this, this);
            }
        }

        setContentView(R.layout.activity_chapter);
        mToolbar = (Toolbar) findViewById(R.id.mainToolbar);

        mViewPager = (CustomDurationViewPager) findViewById(R.id.viewPager);
        mEmptyRelativeLayout = (RelativeLayout) findViewById(R.id.emptyRelativeLayout);
        mEmptyImageView = (ImageView) findViewById(R.id.emptyImageView);
        mEmptyTextView = (TextView) findViewById(R.id.emptyTextView);
        mInstructionsTextView = (TextView) findViewById(R.id.instructionsTextView);

        mPreviousButton = (FloatingActionButton) findViewById(R.id.previousButton);
        mNextButton = (FloatingActionButton) findViewById(R.id.nextButton);
        mPageNumberView = (TextView) findViewById(R.id.numberTextView);

        mTimerView = (FrameLayout) findViewById(R.id.time_frameLayout);
        mHourText = (TextView) findViewById(R.id.hourText);
        mMinuteText = (TextView) findViewById(R.id.minuteText);
        mAmPmText = (TextView) findViewById(R.id.amPmText);
        mDate = (TextView) findViewById(R.id.date);

        frameLayout_wellcome = (FrameLayout) findViewById(R.id.frame_wellcome);
        textView_wellcome = (AutofitTextView) findViewById(R.id.textview_wellcomemessage);
        //ButterKnife.bind(this);

        if (savedInstanceState != null) {
            mChapterPresenter.restoreState(savedInstanceState);
        } else {
            mChapterPresenter.handleInitialArguments(getIntent());
        }

        mChapterPresenter.initializeRealm();
        mChapterPresenter.initializeViews();
        mChapterPresenter.initializeOptions();
        mChapterPresenter.initializeDataFromUrl(getSupportFragmentManager());

//        settingClock();


    }

    @Override
    protected void onStart() {
        super.onStart();
        mChapterPresenter.registerForEvents();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mChapterPresenter.saveChapterToRecentChapters();
        if (frameLayout_wellcome.getVisibility() == View.VISIBLE)
            frameLayout_wellcome.setVisibility(View.INVISIBLE);

        if (MessageCheckSubscription != null) {
            MessageCheckSubscription.unsubscribe();
            MessageCheckSubscription = null;
        }

        if (Messagelist_FCM_Subscription != null) {
            Messagelist_FCM_Subscription.unsubscribe();
            Messagelist_FCM_Subscription = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mChapterPresenter.MoveToNextPage();
        if (DownloadUtils.isNetworkAvailable(getContext())) {
            mChapterPresenter.CheckNewPost();
        } else {
            SnackBarChapterError(new Throwable(getString(R.string.snackbar_network_error)));
        }
        settingClock();
        //웰컴메세지를 저장한다.
//		GetBoardMessageList();
//		GetBoardMessageList_FCM();
//		CheckWellcomeMessage();
//		MessageRetry();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mInitializeFullscreen != null) {
            mInitializeFullscreen.unsubscribe();
            mInitializeFullscreen = null;
        }

        mChapterPresenter.unregisterForEvents();
        if (mMinuteReceiver != null) {
            unregisterReceiver(mMinuteReceiver);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mInitializeFullscreen != null) {
            mInitializeFullscreen.unsubscribe();
            mInitializeFullscreen = null;
        }
        mChapterPresenter.destroyAllSubscriptions();
        mChapterPresenter.StopService();
        mEmptyImageView.clearAnimation();

        if (MessageCheckSubscription != null) {
            MessageCheckSubscription.unsubscribe();
            MessageCheckSubscription = null;
        }
        if (MessagelistSubscription != null) {
            MessagelistSubscription.unsubscribe();
            MessagelistSubscription = null;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        mChapterPresenter.saveState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chapter, menu);
        //mOrientationMenuItem = menu.findItem(R.id.action_orientation);
        mVisibleTimerMenuItem = menu.findItem(R.id.action_visible_clock);
//		mPageAnimationMenuItem = menu.findItem(R.id.action_page_animation);
//		mPageChangeTimeMenuItem = menu.findItem(R.id.action_page_change_time);
//		mPageScaleTypeMenuItem = menu.findItem(R.id.action_page_scale_type);
        mChapterPresenter.initializeMenu();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mChapterPresenter.onOptionParent();
                return true;
//			case R.id.action_refresh:
//				mChapterPresenter.onOptionRefresh();
//				return true;
//			case R.id.action_orientation:
//				mChapterPresenter.onOptionOrientation();
//				return true;
            case R.id.action_visible_clock:
                mChapterPresenter.onOptionVisibleTimer();
                return true;
            case R.id.action_page_animation:
                pageAnimation();
                return true;
            case R.id.action_page_change_time:
                pageChangeTime();
                return true;
            case R.id.action_page_scale_type:
                pageScaleType();
                return true;
//            case R.id.action_help:
//                mChapterPresenter.onOptionHelp();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        mChapterPresenter.onTrimMemory(level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mChapterPresenter.onLowMemory();
    }

    // ChapterView:

    @Override
    public void initializeHardwareAcceleration() {
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
    }

    @Override
    public void initializeSystemUIVisibility() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void initializeToolbar() {
        if (mToolbar != null) {
            mToolbar.setTitle(R.string.fragment_chapter);
//            mToolbar.setBackgroundColor(getResources().getColor(R.color.primaryBlue700));
//			mToolbar.setTitleTextColor(Color.WHITE);
//			mToolbar.setSubtitleTextColor(Color.WHITE);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public void initializeViewPager() {
        if (mViewPager != null) {
            mViewPager.setOffscreenPageLimit(1);
            mViewPager.setPageMargin(16);
            mViewPager.setPageTransformer(true, PageChangeTransFormer.setPresetTransformer(PreferenceUtils.getPageAnimation()));
            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    mChapterPresenter.onPageScrolled(position, positionOffset, positionOffsetPixels);
                }

                @Override
                public void onPageSelected(int position) {
                    mChapterPresenter.onPageSelected(position);
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                    mChapterPresenter.onPageScrollStateChanged(state);
                }
            });
        }

    }

    @Override
    public void initializeEmptyRelativeLayout() {
        if (mEmptyRelativeLayout != null) {
            mEmptyRelativeLayout.findViewById(R.id.try_button).setVisibility(View.INVISIBLE);
            mEmptyImageView.clearAnimation();
            if (DownloadUtils.isNetworkAvailable(getContext())) {
                mEmptyImageView.setImageResource(R.drawable.ic_image_white_48dp);
                mEmptyImageView.setColorFilter(getResources().getColor(R.color.accentPinkA200), PorterDuff.Mode.MULTIPLY);
                mEmptyTextView.setText(R.string.no_data);
                mInstructionsTextView.setText(R.string.chapter_instructions);
            } else {
                mEmptyImageView.setImageResource(R.drawable.ic_wifi_white_48dp);
                mEmptyImageView.setColorFilter(getResources().getColor(R.color.accentPinkA200), PorterDuff.Mode.MULTIPLY);
                ((TextView) mEmptyRelativeLayout.findViewById(R.id.emptyTextView)).setText(R.string.error_loading_failed);
                ((TextView) mEmptyRelativeLayout.findViewById(R.id.instructionsTextView)).setText(R.string.snackbar_network_error);
            }

        }
    }

    @Override
    public void DownloadDataChangeEmptyRelativeLayout() {
        mEmptyImageView.setImageResource(R.drawable.icon_file_loading);
        mEmptyImageView.setColorFilter(getResources().getColor(R.color.accentPinkA200), PorterDuff.Mode.MULTIPLY);
        mEmptyTextView.setText(R.string.downloading_data);
        mInstructionsTextView.setText(R.string.Downloading_now_chapter_instructions);

        RotateAnimation anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(3000);
        mEmptyImageView.setAnimation(anim);
        mEmptyImageView.startAnimation(anim);
    }

    @Override
    public void initializeButtons() {
        if (mPreviousButton != null) {
            mPreviousButton.setOnClickListener(v -> {
                mChapterPresenter.onPreviousClick();
            });
        }
        if (mNextButton != null) {
            mNextButton.setOnClickListener(v -> {
                mChapterPresenter.onNextClick();
            });
        }

    }

    @Override
    public void initializeTextView() {
        if (mPageNumberView != null) {
            mPageNumberView.setVisibility(View.GONE);
            mPageNumberView.getBackground().setAlpha(100);
        }
    }

    @Override
    public void initializeFullscreen() {
        mInitializeFullscreen = Observable.timer(2500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(nullObject -> {
                    enableFullscreen();
                });
    }

    @Override
    public void hideEmptyRelativeLayout() {
        if (mEmptyRelativeLayout != null) {
            mEmptyRelativeLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void showEmptyRelativeLayout() {
        if (mEmptyRelativeLayout != null) {
            mEmptyRelativeLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void enableFullscreen() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LOW_PROFILE
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            );
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
        }
        mSystemUIVisibility = false;
        hideControls();
    }

    @Override
    public void disableFullscreen() {
        Dlog.d("disableFullscreen: ");
        if (mInitializeFullscreen != null) {
            mInitializeFullscreen.unsubscribe();
        }
        mSystemUIVisibility = true;
        showControls();
        mChapterPresenter.StopToNextPage();

    }

    @Override
    public void setTitleText(String title) {
        if (mToolbar != null) {
            mToolbar.setTitle(title);
        }
    }

    @Override
    public void setSubtitleProgressText(String Current, String progress) {
        mInstructionsTextView.setText(Current + "\n" + progress + "%");
    }

    @Override
    public void setSubtitlePositionText(int position) {
        if (mToolbar != null) {
            if (mViewPager.getAdapter() != null) {
                StringBuilder currentSubtitle = new StringBuilder(getString(R.string.chapter_subtitle_page));
                currentSubtitle.append(" ");
                currentSubtitle.append(position);
                currentSubtitle.append("/");
                currentSubtitle.append(mViewPager.getAdapter().getCount());

                mToolbar.setSubtitle(currentSubtitle.toString());
            }
        }
    }

    @Override
    public void setImmersivePositionText(int position) {
        //// TODO: 2016-06-17 삭제예정
    }

    @Override
    public void setOptionOrientationText(boolean isLockOrientation) {
        if (mOrientationMenuItem != null) {
            if (isLockOrientation) {
                mOrientationMenuItem.setTitle(R.string.action_lock_orientation);
            } else {
                mOrientationMenuItem.setTitle(R.string.action_unlock_orientation);
            }
        }
    }

    @Override
    public void setOptionVisibleCommentText(boolean isvisible) {
        if (mVisibleCommentMenuItem != null) {
            if (isvisible) {
                mVisibleCommentMenuItem.setTitle(R.string.action_invisible_comment);
            } else {
                mVisibleCommentMenuItem.setTitle(R.string.action_visible_comment);
            }
        }
    }

    @Override
    public void setOptionVisibleTimer(boolean mIsVisibleTimer) {
        if (mIsVisibleTimer) {
            mTimerView.setVisibility(View.VISIBLE);
            if (mVisibleTimerMenuItem != null)
                mVisibleTimerMenuItem.setTitle(R.string.action_invisible_clock);
        } else {
            mTimerView.setVisibility(View.INVISIBLE);
            if (mVisibleTimerMenuItem != null)
                mVisibleTimerMenuItem.setTitle(R.string.action_visible_clock);

        }
    }

    @Override
    public boolean IsFullScreen() {
        return !mSystemUIVisibility;
    }

    @Override
    public void toastNotInitializedError() {
        Toast.makeText(this, R.string.toast_not_initialized, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void toastChapterError() {
        Toast.makeText(this, R.string.toast_chapter_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void toastNoPreviousChapter() {
        Toast.makeText(this, R.string.toast_no_previous_chapter, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void toastNoNextChapter() {
        Toast.makeText(this, R.string.toast_no_next_chapter, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void toastNetWorkError() {
        Toast.makeText(this, R.string.snackbar_network_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void SnackBarChapterError(Throwable throwable) {
        Snackbar.make(mViewPager, throwable.getMessage(), Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void finishAndLaunchActivity(Intent launchIntent, boolean isFadeTransition) {
        finish();

        if (isFadeTransition) {
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
        if (launchIntent != null)
            startActivity(launchIntent);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void ToggleFullscreen() {
        if (mSystemUIVisibility) {
            enableFullscreen();
            mChapterPresenter.MoveToNextPage();
//            mChapterPresenter.MoveToNextPage();
        } else {
            disableFullscreen();
//            mChapterPresenter.StopToNextPage();
        }


    }


    public void hideControls() {
        if (mToolbar != null) {
            if (mToolbar.getVisibility() != View.GONE) {
                mToolbar.animate()
                        .y(0)
                        .translationY(-1 * mToolbar.getHeight())
                        .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime))
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                mToolbar.setVisibility(View.GONE);
                            }
                        });
            }
        }
        if (mPreviousButton != null) {
            mPreviousButton.hide();
        }
        if (mNextButton != null) {
            mNextButton.hide();
        }
    }

    public void showControls() {
        if (mToolbar != null) {
            if (mToolbar.getVisibility() != View.VISIBLE) {
                mToolbar.setVisibility(View.VISIBLE);
                mToolbar.animate()
                        .translationY(mToolbar.getHeight())
                        .y(0)
                        .setDuration(getResources().getInteger(android.R.integer.config_shortAnimTime))
                        .setListener(null);
            }
        }
        if (mPreviousButton != null) {
            mPreviousButton.show();
        }
        if (mNextButton != null) {
            mNextButton.show();
        }
    }

    // ChapterMapper;

    @Override
    public void registerAdapter(PagerAdapter adapter) {
        if (mViewPager != null) {
            mViewPager.setAdapter(adapter);
        }
    }

    @Override
    public int getPosition() {
        if (mViewPager != null) {
            return mViewPager.getCurrentItem();
        }

        return -1;
    }

    @Override
    public void setPosition(int position, boolean smooth) {
        if (mViewPager != null) {
            mViewPager.setCurrentItem(position, smooth);
        }
    }

    @Override
    public void applyIsLockOrientation(boolean isLockOrientation) {
        if (isLockOrientation) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }
    }

    @Override
    public void applyIsVisibleTimer(boolean mIsVisibleTimer) {
        setOptionVisibleTimer(mIsVisibleTimer);
    }


    private void settingClock() {
        // Set the time to the current system time
        updateTime();
        updateDate();

        // Our BroadcastReceiver to handle the time change
        mMinuteReceiver = new BroadcastReceiver() {
            @Override
            // This method is called when a broadcast is received
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().compareTo(Intent.ACTION_TIME_TICK) == 0) {
                    // If the system time is updated, update the app's time and date
                    updateTime();
                    updateDate();
                }
            }
        };

        // Registering the Broadcast receiver in order to receive a time change
        registerReceiver(mMinuteReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));
    }

    // Private methods
    @SuppressLint("SimpleDateFormat")
    public void updateTime() {
        // Create a new Date variable
        Date today = new Date();
        // Using SimpleDateFormat to convert the system time into a readable string
        SimpleDateFormat hour24 = new SimpleDateFormat("H");
        SimpleDateFormat hour12 = new SimpleDateFormat("h");
        SimpleDateFormat minutes = new SimpleDateFormat("m");
        SimpleDateFormat amPm = new SimpleDateFormat("a");

        // Format the date and save it into a String
        String currentHour24 = hour24.format(today);
        String currentHour12 = hour12.format(today);
        String currentMinutes = minutes.format(today);
        String currentAmPm = amPm.format(today);

        // Update the corresponding TextViews
        if (currentMinutes.length() == 1) {
            currentMinutes = "0" + currentMinutes;
        }

        // If the current Clock format is 24-hour clock
        if (mClockFormatIs24) {
            if (currentHour24.length() == 1) {
                currentHour24 = "0" + currentHour24;
            }
            mHourText.setText(currentHour24);
        } else {
            if (currentHour12.length() == 1) {
                currentHour12 = "0" + currentHour12;
            }
            mHourText.setText(currentHour12);
        }
        mMinuteText.setText(mSeperator + currentMinutes);
        mAmPmText.setText(currentAmPm);
    }

    @SuppressLint("SimpleDateFormat")
    public void updateDate() {
        Date today = new Date();
        // Using SimpleDateFormat to convert the system date into a readable string
        SimpleDateFormat day = new SimpleDateFormat("E");
        SimpleDateFormat month = new SimpleDateFormat("MMM");
        SimpleDateFormat dayInMonth = new SimpleDateFormat("dd");
        SimpleDateFormat year = new SimpleDateFormat("y");

        String currentDay = day.format(today);
        String currentMonth = month.format(today);
        String currentdayInMonth = dayInMonth.format(today);
        String currentYear = year.format(today);

//        mDate.setText(currentDay + ", " + currentMonth + " " + currentdayInMonth + ", " + currentYear);
//        mDate.setText(currentYear + getString(R.string.year) + currentMonth + " " + currentdayInMonth + getString(R.string.day) + "(" + currentDay + ")");
        mDate.setText(getString(R.string.yyyymmdd, currentYear + getString(R.string.year), currentMonth, currentdayInMonth + "" + getString(R.string.day), "(" + currentDay + ")"));
    }

    public void pageChangeTime() {
        String[] items = getResources().getStringArray(R.array.preference_page_change_time_values);
        //String pageChangeTime = PreferenceHelper.getString(getResources().getString(R.string.preference_page_change_time_key), getResources().getString(R.string.preference_page_change_time_default_value));
        String pageChangeTime = PreferenceUtils.getPageChangeTime();

        int position = 1;
        for (int i = 0; i < items.length; i++) {
            if (pageChangeTime.equals(items[i])) {
                position = i;
                break;
            }
        }
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.preference_page_change_time_title))
                .setSingleChoiceItems(R.array.preference_page_change_time, position, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int idx) {
                        String select_str = getResources().getStringArray(R.array.preference_page_change_time)[idx];
                        //change_time_txt.setText(select_str);
                        String select_value = getResources().getStringArray(R.array.preference_page_change_time_values)[idx];
                        //change_time_txt.setText(select_str);
                        PreferenceHelper.putString(getResources().getString(R.string.preference_page_change_time_key), select_value);
                        dialog.dismiss();
                    }
                }).setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        Dialog d = builder.show();
        try {
            int dividerId = d.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
            View divider = d.findViewById(dividerId);
            divider.setBackgroundColor(getResources().getColor(R.color.main_font_color));
            int textViewId = d.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
            TextView tv = (TextView) d.findViewById(textViewId);
            tv.setTextColor(getResources().getColor(R.color.main_font_color));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pageAnimation() {
        String[] items = getResources().getStringArray(R.array.preference_page_animation_values);
        //String pageAnimation = PreferenceHelper.getString(getResources().getString(R.string.preference_page_animation_key), getResources().getString(R.string.preference_page_animation_entries_Default));
        String pageAnimation = PreferenceUtils.getPageAnimationString();

        int position = 1;
        for (int i = 0; i < items.length; i++) {
            if (pageAnimation.equals(items[i])) {
                position = i;
                break;
            }
        }
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.preference_page_animation_title))
                .setSingleChoiceItems(R.array.preference_page_animation_values, position, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int idx) {
                        String select_str = getResources().getStringArray(R.array.preference_page_animation_values)[idx];
                        //animation_txt.setText(select_str);
                        PreferenceHelper.putString(getResources().getString(R.string.preference_page_animation_key), select_str);
                        dialog.dismiss();

                        if (mViewPager != null) {
                            mViewPager.setPageTransformer(true, PageChangeTransFormer.setPresetTransformer(PreferenceUtils.getPageAnimation()));
                        }

                    }
                })
                .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        Dialog d = builder.show();
        try {
            int dividerId = d.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
            View divider = d.findViewById(dividerId);
            divider.setBackgroundColor(getResources().getColor(R.color.main_font_color));
            int textViewId = d.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
            TextView tv = (TextView) d.findViewById(textViewId);
            tv.setTextColor(getResources().getColor(R.color.main_font_color));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void pageScaleType() {
        String[] items = getResources().getStringArray(R.array.preference_ScaleType_values);
        String pageAnimation = PreferenceHelper.getString(getResources().getString(R.string.preference_ScaleType_key), getResources().getString(R.string.preference_ScaleType_default_value));

        int position = 1;
        for (int i = 0; i < items.length; i++) {
            if (pageAnimation.equals(items[i])) {
                position = i;
                break;
            }
        }
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.preference_ScaleType_title))
                .setSingleChoiceItems(R.array.preference_ScaleType_values, position, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int idx) {
                        String select_str = getResources().getStringArray(R.array.preference_ScaleType_values)[idx];
                        //scaletype_txt.setText(select_str);
                        PreferenceHelper.putString(getResources().getString(R.string.preference_ScaleType_key), select_str);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        Dialog d = builder.show();
        try {
            int dividerId = d.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
            View divider = d.findViewById(dividerId);
            divider.setBackgroundColor(getResources().getColor(R.color.main_font_color));
            int textViewId = d.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
            TextView tv = (TextView) d.findViewById(textViewId);
            tv.setTextColor(getResources().getColor(R.color.main_font_color));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//	DevicePolicyManager devicePolicyManager;
//	ComponentName adminComponent;
//	public void scheduleSet(){
//		if (!devicePolicyManager.isAdminActive(adminComponent)) {
//			Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
//			intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, adminComponent);
//			intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, getContext().getString(R.string.alert_accept_device_administrator));
//			startActivityForResult(intent, PrefFragment.RETURN_DEVICE_POLICYMANAGER);
//		} else {
//			Dialog_Scheduler dialog_scheduler = new Dialog_Scheduler();
//			dialog_scheduler.setCancelable(false);
//			dialog_scheduler.setMenuVisibility(false);
//
//			dialog_scheduler.show(getFragmentManager(), Dialog_Scheduler.TAG);
//		}
//	}


//	//30초에 1번씩 서버에 접속하여 웰컴보드 리스트를 가지고 온다.
//	void GetBoardMessageList(){
//		Dlog.d("GetBoardMessageList:start");
//		MessagelistSubscription = Observable.interval(5, 60, TimeUnit.SECONDS)
//				.flatMap(aLong -> {
//					Dlog.d(" flatmap");
//					return df_RestfulAdapter.getInstance().BoardMessageList(0);
//				})
//				.observeOn(AndroidSchedulers.mainThread())
//				.subscribe(jsonObject -> {
//					JsonArray dataJsonArray = jsonObject.getAsJsonArray("data");
//					ArrayList<Board_realm> board_realmlist = new ArrayList<Board_realm>();
//					SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//					Calendar cal  = Calendar.getInstance();
//
//					for(int i = 0; i < dataJsonArray.size() ; i++){
//						JsonObject jsonObject1 = dataJsonArray.get(i).getAsJsonObject();
//						Board_realm board_realm = new Board_realm();
//						board_realm.setBoard_seq(jsonObject1.get("board_seq").getAsInt());
//						board_realm.setBg_color(jsonObject1.get("bg_color").getAsString());
//						board_realm.setFont_color(jsonObject1.get("font_color").getAsString());
//						board_realm.setMessage(jsonObject1.get("message").getAsString());
//						board_realm.setBg_type(jsonObject1.get("bg_type").getAsString());
//
//						try {
//							cal.setTime(transFormat.parse(jsonObject1.get("added").getAsString()));
//							board_realm.setAdded(cal.getTimeInMillis());
//							cal.setTime(transFormat.parse(jsonObject1.get("modified").getAsString()));
//							board_realm.setModified(cal.getTimeInMillis());
//							cal.setTime(transFormat.parse(jsonObject1.get("start_time").getAsString()));
//							board_realm.setStart_time(cal.getTimeInMillis());
//							cal.setTime(transFormat.parse(jsonObject1.get("end_time").getAsString()));
//							board_realm.setEnd_time(cal.getTimeInMillis());
//						} catch (ParseException e) {
//							Calendar NewCal  = Calendar.getInstance();
//							board_realm.setAdded(NewCal.getTimeInMillis());
//							board_realm.setModified(NewCal.getTimeInMillis());
//							board_realm.setStart_time(cal.getTimeInMillis());
//							board_realm.setEnd_time(cal.getTimeInMillis());
//
//						}
//						board_realmlist.add(board_realm);
//					}
//
//					realm = Realm.getDefaultInstance();
//					realm.beginTransaction();
//					if(board_realmlist.size() > 0)
//						realm.copyToRealmOrUpdate(board_realmlist);
//					else
//						realm.delete(Board_realm.class);
//					realm.commitTransaction();
//
//				}, Throwable::printStackTrace);
//	}

//	//푸시메세지가 오면 처리한다.
//	void GetBoardMessageList_FCM() {
//		Dlog.d("GetBoardMessageList:start");
//		Messagelist_FCM_Subscription = FirebaseMessagingService.WellcomeMsgPublishSubject
//				.subscribeOn(Schedulers.io())
//				.observeOn(AndroidSchedulers.mainThread()).
//						flatMap(aBoolean1 -> {
//							realm = Realm.getDefaultInstance();
//							long CurrentTime = Calendar.getInstance().getTimeInMillis();
//							return realm.where(Board_realm.class)
//									.lessThanOrEqualTo("start_time", CurrentTime)
//									.greaterThanOrEqualTo("end_time", CurrentTime)
//									.findFirst().asObservable();
//						})
//				.doOnError(throwable -> {
//					Dlog.d("GetBoardMessageList:ERROR");
//					throwable.printStackTrace();
//					if (frameLayout_wellcome.getVisibility() == View.VISIBLE) {
//						//이전 이벤트가 있었으므로 숨김
//						frameLayout_wellcome.setVisibility(View.INVISIBLE);
//						mChapterPresenter.MoveToNextPage();
//					}
//				}).
//						subscribe(messageRealmObject -> {
//							Dlog.e("realmObject : " + messageRealmObject);
//							if (messageRealmObject != null) {
//								String bg_type = ((Board_realm) messageRealmObject).getBg_type() == null ? "" : ((Board_realm) messageRealmObject).getBg_type();
//								switch (bg_type) {
//									case "0":
//										frameLayout_wellcome.setBackgroundResource(R.drawable.bg1);
//										break;
//									case "1":
//										frameLayout_wellcome.setBackgroundResource(R.drawable.bg2);
//										break;
//									case "2":
//										frameLayout_wellcome.setBackgroundResource(R.drawable.bg3);
//										break;
//									case "3":
//										frameLayout_wellcome.setBackgroundResource(R.drawable.bg4);
//										break;
//									default:
//										frameLayout_wellcome.setBackgroundResource(R.drawable.orange_bg);
//										break;
//
//								}
//								enableFullscreen();
//								mChapterPresenter.StopToNextPage();
//								mChapterPresenter.StopMovie();
//								if (frameLayout_wellcome.getVisibility() == View.VISIBLE) {
//									textView_wellcome.setText(((Board_realm) messageRealmObject).getMessage().replace("\\n", System.getProperty("line.separator")));
//								} else {
//
//									frameLayout_wellcome.setVisibility(View.VISIBLE);
//									textView_wellcome.setText(((Board_realm) messageRealmObject).getMessage().replace("\\n", System.getProperty("line.separator")));
//								}
//							} else {
//								if (frameLayout_wellcome.getVisibility() == View.VISIBLE) {
//									//이전 이벤트가 있었으므로 숨김
//									frameLayout_wellcome.setVisibility(View.INVISIBLE);
//									mChapterPresenter.MoveToNextPage();
//								}
//							}
//						}, throwable -> {
//							Dlog.d("GetBoardMessageList:throwable");
//							throwable.printStackTrace();
//						});
//	}
    /**
     * 1분마다 db에서 표시할 메세지가 있는지 체크한다. 재시도 30초
     * 메세지가 있다. --> View가 visible 이면 text 업데이트 한다. , View가 invisible이면..재생중인 슬라이드를 멈추고 보여준다.
     * 메세지가 없다. --> View가 visible 이면  View를 숨기고 슬라이드를 재생한다.
     */
//	void CheckWellcomeMessage(){
//		Dlog.d("CheckWellcomeMessage start");
//		MessageCheckSubscription = Observable.interval(3, 60, TimeUnit.SECONDS)
//				.observeOn(AndroidSchedulers.mainThread())
//				.flatMap(seq -> {
//					Dlog.d("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//					long CurrentTime = Calendar.getInstance().getTimeInMillis();
//					return realm.where(Board_realm.class)
//							.lessThanOrEqualTo("start_time", CurrentTime)
//							.greaterThanOrEqualTo("end_time", CurrentTime)
//							.findFirst()
//							.asObservable();
//				})
//				.doOnError(throwable -> {
//					Dlog.d("ERROR");
//					if(frameLayout_wellcome.getVisibility() == View.VISIBLE){
//						//이전 이벤트가 있었으므로 숨김
//						frameLayout_wellcome.setVisibility(View.INVISIBLE);
//						mChapterPresenter.MoveToNextPage();
//					}
//				})
//				.retryWhen(RetryWhen.delay(30, TimeUnit.SECONDS).build())
//				.subscribe(realmObject -> {
//					Dlog.e("realmObject : "+ realmObject);
//					if(realmObject != null){
//						String bg_type = ((Board_realm) realmObject).getBg_type() == null ? "" : ((Board_realm) realmObject).getBg_type();
//						switch (bg_type) {
//							case "0":
//								frameLayout_wellcome.setBackgroundResource(R.drawable.bg1);
//								break;
//							case "1":
//								frameLayout_wellcome.setBackgroundResource(R.drawable.bg2);
//								break;
//							case "2":
//								frameLayout_wellcome.setBackgroundResource(R.drawable.bg3);
//								break;
//							case "3":
//								frameLayout_wellcome.setBackgroundResource(R.drawable.bg4);
//								break;
//							default:
//								frameLayout_wellcome.setBackgroundResource(R.drawable.orange_bg);
//								break;
//
//						}
//						enableFullscreen();
//						mChapterPresenter.StopToNextPage();
//						mChapterPresenter.StopMovie();
//						if (frameLayout_wellcome.getVisibility() == View.VISIBLE) {
//							textView_wellcome.setText(((Board_realm) realmObject).getMessage().replace("\\n", System.getProperty("line.separator")));
//						} else {
//
//							frameLayout_wellcome.setVisibility(View.VISIBLE);
//							textView_wellcome.setText(((Board_realm) realmObject).getMessage().replace("\\n", System.getProperty("line.separator")));
//						}
//					}
//				}, Throwable::printStackTrace);
//	}
}
