package com.jiransoft.officedisplay.views.widgets;

/**
 * Created by 06peng on 15/6/26.
 */
public interface ImageDownloadListener {
    void onUpdate(int progress);
}
