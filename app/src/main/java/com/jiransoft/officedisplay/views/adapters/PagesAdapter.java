package com.jiransoft.officedisplay.views.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.jiransoft.officedisplay.controllers.DirectDisplayManager;
import com.jiransoft.officedisplay.models.ImageModel;
import com.jiransoft.officedisplay.utils.Dlog;
import com.jiransoft.officedisplay.views.fragments.Movie.MoviePageFragment;
import com.jiransoft.officedisplay.views.fragments.Page.PageFragment;
import com.jiransoft.officedisplay.views.fragments.Page.PageFragment_OFFICEBOX;

import java.util.List;

public class PagesAdapter extends BaseFragmentStatePagerAdapter {
    private Boolean mIsOffline;
    private int mConnectService = 0;
    private String mProjectid;
    private List<ImageModel> mImageModels;

    private boolean mIsRightToLeftDirection;

    public PagesAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public int getCount() {
        if (mImageModels != null) {
            return mImageModels.size();
        } else {
            return 0;
        }
    }

    @Override
    public String getTag(int position) {
//		if (mConnectService == DirectDisplayManager.DirectCloudBox) {
//			return PageFragment_BOX.TAG + ":" + position + ":" + mImageModels.get(position).getFile_id();
//		} else if (mConnectService == DirectDisplayManager.GigaPod) {
//			return PageFragment_GIGAPOD.TAG + ":" + position + ":" + mImageModels.get(position).getFile_id();
//		} else
        if (mConnectService == DirectDisplayManager.OfficeBox) {
            return PageFragment_OFFICEBOX.TAG + ":" + position + ":" + mImageModels.get(position).getFile_id();
        } else {
            return PageFragment.TAG + ":" + position + ":" + mImageModels.get(position).getFile_id();
        }
//                (mIsRightToLeftDirection ? "RTL" : "LTR");
    }

    @Override
    public Fragment getItem(int position) {
        if (mProjectid == null) {
            throw new IllegalStateException("Null Project ID");
        }

        if (mIsOffline == null) {
            throw new IllegalStateException("Null Offline Value");
        }

        if (mConnectService == 0) {
            throw new IllegalStateException("Null BOX Value");
        }

        if (mImageModels == null) {
            throw new IllegalStateException("Null ImageModels Value");
        }


        if (mImageModels.get(position).getExt().equalsIgnoreCase("mp4")) {
            return MoviePageFragment.newInstance(mProjectid, mImageModels.get(position), mIsOffline);
        } else {
//			if (mConnectService == DirectDisplayManager.DirectCloudBox)
//				return PageFragment_BOX.newInstance(mProjectid, mImageModels.get(position), mIsOffline);
//			else if (mConnectService == DirectDisplayManager.GigaPod)
//				return PageFragment_GIGAPOD.newInstance(mProjectid, mImageModels.get(position), mIsOffline);
//			else
            if (mConnectService == DirectDisplayManager.OfficeBox)
                return PageFragment_OFFICEBOX.newInstance(mProjectid, mImageModels.get(position), mIsOffline);
            else {
                return PageFragment.newInstance(mProjectid, mImageModels.get(position), mIsOffline);
            }
        }
    }


    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public void setmProjectid(String mProjectid) {
        this.mProjectid = mProjectid;
    }

    public boolean getIsRightToLeftDirection() {
        return mIsRightToLeftDirection;
    }

    public void setIsRightToLeftDirection(boolean isRightToLeftDirection) {
        mIsRightToLeftDirection = isRightToLeftDirection;
    }

    public void setmIsOffline(Boolean mIsOffline) {
        this.mIsOffline = mIsOffline;
    }

    public void setmConnectService(int mConnectService) {
        this.mConnectService = mConnectService;
    }

    public void setmImageModels(List<ImageModel> mImageModels) {
        if (mImageModels != null && mImageModels.size() > 1) {
            Dlog.d(mImageModels.get(0).toString());

        }
        this.mImageModels = mImageModels;
    }

}
