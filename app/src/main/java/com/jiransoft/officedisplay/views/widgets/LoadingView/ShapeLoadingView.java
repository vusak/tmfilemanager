package com.jiransoft.officedisplay.views.widgets.LoadingView;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import java.lang.ref.WeakReference;

import jiran.com.tmfilemanager.R;


/**
 * Created by zzz40500 on 15/4/4.
 */
public class ShapeLoadingView extends View {

    public boolean mIsLoading = false;
    private Context mContext;
    private WeakReference<Bitmap> mybitmap;
    private int mWidth;
    private int mHeight;

    public ShapeLoadingView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public ShapeLoadingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public ShapeLoadingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ShapeLoadingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;
        init();
    }

    private void init() {
        setBackgroundColor(getResources().getColor(R.color.view_bg));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        mWidth = MeasureSpec.getSize(widthMeasureSpec);
        mHeight = MeasureSpec.getSize(heightMeasureSpec);

        setMeasuredDimension(mWidth, mHeight);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (getVisibility() == GONE) {
            return;
        }


        Resources res = mContext.getResources();
        BitmapDrawable drawable = (BitmapDrawable) getResources().getDrawable(R.mipmap.ic_launcher);
        mybitmap = new WeakReference(drawable.getBitmap());

        // Here's the magic. Whatever way you do it, the logic is:
        // space available - bitmap size and divide the result by two.
        // There must be an equal amount of pixels on both sides of the image.
        // Therefore whatever space is left after displaying the image, half goes to
        // left/up and half to right/down. The available space you get by subtracting the
        // image's width/height from the screen dimensions. Good luck.

        int cx = (mWidth - mybitmap.get().getWidth()) >> 1; // same as (...) / 2
        int cy = (mHeight - mybitmap.get().getHeight()) >> 1;
        canvas.drawBitmap(mybitmap.get(), cx, cy, null);

//        setDataSubscriber(mContext,canvas, R.mipmap.ic_launcher,0,0);

    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
    }
}
