package com.jiransoft.officedisplay.views.base;

import android.content.Context;

public interface BaseContextView {
    Context getContext();

    void SnackBarChapterError(Throwable throwable);
}
