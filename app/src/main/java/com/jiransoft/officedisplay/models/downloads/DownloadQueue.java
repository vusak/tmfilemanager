package com.jiransoft.officedisplay.models.downloads;

import android.os.Parcel;
import android.os.Parcelable;

public class DownloadQueue implements Parcelable {
    public static final String TAG = DownloadQueue.class.getSimpleName();

    public static final String PARCELABLE_KEY = TAG + ":" + "ParcelableKey";
    public static final Creator<DownloadQueue> CREATOR = new Creator<DownloadQueue>() {
        @Override
        public DownloadQueue createFromParcel(Parcel inputParcel) {
            return new DownloadQueue(inputParcel);
        }

        @Override
        public DownloadQueue[] newArray(int size) {
            return new DownloadQueue[size];
        }
    };

    private Long _id;

    private String Url;
    private String ParentUrl;

    private String Directory;

    private String Name;
    private String Ext;
    private String Comment;

    private int Flag;

    public DownloadQueue() {
    }

    private DownloadQueue(Parcel inputParcel) {
        _id = inputParcel.readLong();
        if (_id < 0) {
            _id = null;
        }

        Url = inputParcel.readString();
        ParentUrl = inputParcel.readString();

        Directory = inputParcel.readString();

        Name = inputParcel.readString();
        Ext = inputParcel.readString();
        Comment = inputParcel.readString();

        Flag = inputParcel.readInt();
    }

    public Long getId() {
        return _id;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getParentUrl() {
        return ParentUrl;
    }

    public void setParentUrl(String parentUrl) {
        ParentUrl = parentUrl;
    }

    public String getDirectory() {
        return Directory;
    }

    public void setDirectory(String directory) {
        Directory = directory;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getExt() {
        return Ext;
    }

    public void setExt(String ext) {
        Ext = ext;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public int getFlag() {
        return Flag;
    }

    public void setFlag(int flag) {
        Flag = flag;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel outputParcel, int flags) {
        if (_id != null) {
            outputParcel.writeLong(_id);
        } else {
            outputParcel.writeLong(-1);
        }

        outputParcel.writeString(Url);
        outputParcel.writeString(ParentUrl);

        outputParcel.writeString(Directory);

        outputParcel.writeString(Name);
        outputParcel.writeString(Ext);
        outputParcel.writeString(Comment);

        outputParcel.writeInt(Flag);
    }
}
