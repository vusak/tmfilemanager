package com.jiransoft.officedisplay.models;

/**
 * Created by user on 2015-10-27.
 */
public class ProjectModel {

    String access_key;
    String auth;
    String beacon_id;
    String expired;
    boolean expired_flag;
    String id;
    boolean is_member;
    String last_thumb_fid;
    int member_count;
    String modified;
    String name;
    String owner;
    boolean Public;
    String public_auth;

    private String login_id; //로그인 아이디
    private boolean New; //업데이트 되었는지


    public ProjectModel(String access_key, String auth, String beacon_id, String expired, boolean expired_flag, String id, boolean is_member, String last_thumb_fid, int member_count, String modified, String name, String owner, boolean aPublic, String public_auth, String login_id, boolean aNew) {
        this.access_key = access_key;
        this.auth = auth;
        this.beacon_id = beacon_id;
        this.expired = expired;
        this.expired_flag = expired_flag;
        this.id = id;
        this.is_member = is_member;
        this.last_thumb_fid = last_thumb_fid;
        this.member_count = member_count;
        this.modified = modified;
        this.name = name;
        this.owner = owner;
        Public = aPublic;
        this.public_auth = public_auth;
        this.login_id = login_id;
        New = aNew;
    }

    public String getAccess_key() {
        return access_key;
    }

    public void setAccess_key(String access_key) {
        this.access_key = access_key;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getBeacon_id() {
        return beacon_id;
    }

    public void setBeacon_id(String beacon_id) {
        this.beacon_id = beacon_id;
    }

    public String getExpired() {
        return expired;
    }

    public void setExpired(String expired) {
        this.expired = expired;
    }

    public boolean isExpired_flag() {
        return expired_flag;
    }

    public void setExpired_flag(boolean expired_flag) {
        this.expired_flag = expired_flag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean is_member() {
        return is_member;
    }

    public void setIs_member(boolean is_member) {
        this.is_member = is_member;
    }

    public String getLast_thumb_fid() {
        return last_thumb_fid;
    }

    public void setLast_thumb_fid(String last_thumb_fid) {
        this.last_thumb_fid = last_thumb_fid;
    }

    public int getMember_count() {
        return member_count;
    }

    public void setMember_count(int member_count) {
        this.member_count = member_count;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public boolean isPublic() {
        return Public;
    }

    public void setPublic(boolean Public) {
        this.Public = Public;
    }

    public String getPublic_auth() {
        return public_auth;
    }

    public void setPublic_auth(String public_auth) {
        this.public_auth = public_auth;
    }

    public String getLogin_id() {
        return login_id;
    }

    public void setLogin_id(String login_id) {
        this.login_id = login_id;
    }

    public boolean isNew() {
        return New;
    }

    public void setNew(boolean aNew) {
        New = aNew;
    }
}
