package com.jiransoft.officedisplay.models;

import java.util.List;

/**
 * Created by dhawal sodha parmar on 5/4/2015.
 */
public class ImageListModel {
    private List<ImageModel> data;

    public List<ImageModel> getImageModels() {
        return data;
    }

    public void setImageModels(List<ImageModel> ImageModels) {
        this.data = ImageModels;
    }
}
