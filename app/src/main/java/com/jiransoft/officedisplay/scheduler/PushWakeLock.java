package com.jiransoft.officedisplay.scheduler;

import android.app.KeyguardManager;
import android.content.Context;
import android.os.PowerManager;

import com.jiransoft.officedisplay.utils.Dlog;

public class PushWakeLock {
    private static PowerManager.WakeLock sCpuWakeLock;
    private static KeyguardManager.KeyguardLock mKeyguardLock;
    private static boolean isScreenLock;
    private static PowerManager pm;

    static void acquireCpuWakeLock(Context context) {
        Dlog.d("Acquiring cpu wake lock");
        if (sCpuWakeLock != null) {
            return;
        }
        pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        sCpuWakeLock = pm.newWakeLock(
                PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                        PowerManager.ACQUIRE_CAUSES_WAKEUP |
                        PowerManager.ON_AFTER_RELEASE, "hello");
        sCpuWakeLock.acquire();
    }

    static void releaseCpuLock() {
        Dlog.d("Releasing cpu wake lock");

        if (sCpuWakeLock != null) {
            sCpuWakeLock.release();
            sCpuWakeLock = null;
        }
    }

    static void goToSleep() {
        sCpuWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "DoNotDimScreen");
        sCpuWakeLock.acquire();
    }
}



