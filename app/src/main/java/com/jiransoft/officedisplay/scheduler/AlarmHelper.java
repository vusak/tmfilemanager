package com.jiransoft.officedisplay.scheduler;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.jiransoft.officedisplay.utils.PreferenceHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by user on 2016-01-21.
 */
public class AlarmHelper {
    /**
     * 화면 꺼짐 알람 등록
     */
    public static void register_ScreenOff_Alarm(Context context) {
        String[] offtime = PreferenceHelper.getString(PreferenceHelper.offtime, "18:00").split(":");
        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
        java.util.Date date = calendar.getTime();
        String Today = new SimpleDateFormat("yyyyMMdd").format(date);

        try {
            Today = Today + (offtime[0].length() == 1 ? "0" + offtime[0] : offtime[0]) + offtime[1];
            calendar.setTime(formatter.parse(Today));
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.set(Calendar.SECOND, 0);


        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        long CurrentTime = System.currentTimeMillis();

        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        alarmIntent.putExtra(AlarmReceiver.ALRAM_ACTION, AlarmReceiver.SCREENOFF);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, AlarmReceiver.REGISTER_SCREENOFF_ALARM, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        if (CurrentTime > calendar.getTimeInMillis()) {
//            Dlog.d( "내일 꺼짐 예약");
            manager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis() + (24 * 60 * 60 * 1000), pendingIntent);
        } else {
//            Dlog.d( "오늘 꺼짐 예약");
            manager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }

    }

    public static void Unregister_ScreenOff_Alarm(Context context) {
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, AlarmReceiver.REGISTER_SCREENOFF_ALARM, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        manager.cancel(pendingIntent);
    }

    /**
     * 화면 켜짐 알람 등록
     */
    public static void register_ScreenOn_Alarm(Context context) {
        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Activity.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock lock = keyguardManager.newKeyguardLock("test");
        lock.disableKeyguard();

        String[] ontime = PreferenceHelper.getString(PreferenceHelper.ontime, "18:00").split(":");
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
        java.util.Date date = calendar.getTime();
        String Today = new SimpleDateFormat("yyyyMMdd").format(date);

        try {
            Today = Today + (ontime[0].length() == 1 ? "0" + ontime[0] : ontime[0]) + ontime[1];
            calendar.setTime(formatter.parse(Today));
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.set(Calendar.SECOND, 0);

        } catch (ParseException e1) {
            e1.printStackTrace();
        }
        long CurrentTime = System.currentTimeMillis();

        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        alarmIntent.putExtra(AlarmReceiver.ALRAM_ACTION, AlarmReceiver.SCREENON);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, AlarmReceiver.REGISTER_SCREENON_ALARM, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        //현재시간이 예약시간보다 크면 //다음날 예약
        if (CurrentTime > calendar.getTimeInMillis()) {
            manager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis() + (24 * 60 * 60 * 1000), pendingIntent);
        } else {
            manager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }
    }

    public static void Unregister_ScreenOn_Alarm(Context context) {
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, AlarmReceiver.REGISTER_SCREENON_ALARM, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        manager.cancel(pendingIntent);
    }
}
