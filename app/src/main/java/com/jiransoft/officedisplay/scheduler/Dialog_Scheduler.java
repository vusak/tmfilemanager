package com.jiransoft.officedisplay.scheduler;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.jiransoft.officedisplay.utils.PreferenceHelper;

import butterknife.BindView;
import jiran.com.tmfilemanager.R;


/**
 * Created by user on 2016-01-13.
 */
public class Dialog_Scheduler extends DialogFragment implements View.OnClickListener {
    public static final String TAG = DialogFragment.class.getSimpleName();
    static int DEFAULT = 0;
    static int START_TIME = 1;
    static int END_TIME = 2;
    OnTimeRangeSelectedListener onTimeRangeSelectedListener;
    @BindView(R.id.cb_onScr)
    CheckBox checkBox_on;
    @BindView(R.id.cb_offScr)
    CheckBox checkBox_off;

    @BindView(R.id.linearLayout_time)
    LinearLayout linearLayout_time;
    @BindView(R.id.tv_starttime)
    TextView textView_start_time;
    @BindView(R.id.tv_endtime)
    TextView textView_end_time;

    @BindView(R.id.linearLayout_timePicker)
    LinearLayout linearLayout_timePicker;
    @BindView(R.id.timePicker)
    TimePicker timePicker;

    @BindView(R.id.btn_close)
    Button button_close;
    @BindView(R.id.btn_set)
    Button button_set;
    int mcheckmode = DEFAULT;
    String[] starttime = PreferenceHelper.getString(PreferenceHelper.ontime, "9:00").split(":");
    String[] offtime = PreferenceHelper.getString(PreferenceHelper.offtime, "18:00").split(":");
    boolean isUseOnScreen = PreferenceHelper.getBoolean(PreferenceHelper.isUseOnScr, false);
    boolean isUseOffScreen = PreferenceHelper.getBoolean(PreferenceHelper.isUseOffScr, false);

    public static Dialog_Scheduler newInstance(OnTimeRangeSelectedListener callback, boolean is24HourMode) {
        Dialog_Scheduler ret = new Dialog_Scheduler();
//        ret.initialize(callback, is24HourMode);
        return ret;
    }

    public void setOnTimeRangeSetListener(OnTimeRangeSelectedListener callback) {
        onTimeRangeSelectedListener = callback;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.dialog_timeselector, container, false);
        checkBox_on = (CheckBox) root.findViewById(R.id.cb_onScr);
        checkBox_off = (CheckBox) root.findViewById(R.id.cb_offScr);

        linearLayout_time = (LinearLayout) root.findViewById(R.id.linearLayout_time);
        textView_start_time = (TextView) root.findViewById(R.id.tv_starttime);
        textView_end_time = (TextView) root.findViewById(R.id.tv_endtime);

        linearLayout_timePicker = (LinearLayout) root.findViewById(R.id.linearLayout_timePicker);
        timePicker = (TimePicker) root.findViewById(R.id.timePicker);

        button_close = (Button) root.findViewById(R.id.btn_close);
        button_set = (Button) root.findViewById(R.id.btn_set);
        //ButterKnife.bind(this, root);
        getDialog().setTitle(getString(R.string.preference_schedule_title));


        //init view
        button_set.setVisibility(View.GONE);
        linearLayout_timePicker.setVisibility(View.GONE);


        textView_start_time.setText(String.format("%d : %s %s", Integer.valueOf(starttime[0]) % 12, starttime[1].length() == 1 ? "0" + starttime[1] : starttime[1], ((Integer.valueOf(starttime[0]) >= 12) ? getString(R.string.PM) : getString(R.string.AM))));
        textView_end_time.setText(String.format("%d : %s %s", Integer.valueOf(offtime[0]) % 12, offtime[1].length() == 1 ? "0" + offtime[1] : offtime[1], ((Integer.valueOf(offtime[0]) >= 12) ? getString(R.string.PM) : getString(R.string.AM))));


        textView_start_time.setEnabled(false);
        textView_start_time.setOnClickListener(this);

        checkBox_on.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                textView_start_time.setEnabled(true);
            else
                textView_start_time.setEnabled(false);

            PreferenceHelper.putBoolean(PreferenceHelper.isUseOnScr, isChecked);
            isUseOnScreen = isChecked;
        });

        textView_end_time.setEnabled(false);
        textView_end_time.setOnClickListener(this);
        checkBox_off.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                textView_end_time.setEnabled(true);
            else
                textView_end_time.setEnabled(false);

            PreferenceHelper.putBoolean(PreferenceHelper.isUseOffScr, isChecked);
            isUseOffScreen = isChecked;
        });

        if (isUseOnScreen)
            checkBox_on.setChecked(true);
        if (isUseOffScreen)
            checkBox_off.setChecked(true);

        button_close.setOnClickListener(this);
        button_set.setOnClickListener(this);

//		try{
//			int dividerId = getDialog().getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
//			View divider = getDialog().findViewById(dividerId);
//			divider.setBackgroundColor(getResources().getColor(R.color.main));
//			int textViewId = getDialog().getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
//			TextView tv = (TextView) getDialog().findViewById(textViewId);
//			tv.setTextColor(getResources().getColor(R.color.main));
//		}catch(Exception e){
//			e.printStackTrace();
//		}
        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onStart() {
        super.onStart();

        // safety check
//        if (getDialog() == null)
//            return;
//        getDialog().getWindow().setLayout((int) (DisplayUtils.getScreenWidth(getContext()) * 0.8), WindowManager.LayoutParams.WRAP_CONTENT);
    }

    void setViewVisible(int type, boolean visible) {


//        if(checkBox_on.isChecked())
        checkBox_on.setEnabled(!visible);
//        if(checkBox_off.isChecked())
        checkBox_off.setEnabled(!visible);

        if (type == START_TIME || type == END_TIME) {
            if (visible) {
                linearLayout_time.setVisibility(View.GONE);
                if (type == START_TIME) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        timePicker.setHour(Integer.valueOf(starttime[0]));
                        timePicker.setMinute(Integer.valueOf(starttime[1]));
                    } else {
                        timePicker.setCurrentHour(Integer.valueOf(starttime[0]));
                        timePicker.setCurrentMinute(Integer.valueOf(starttime[1]));
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        timePicker.setHour(Integer.valueOf(offtime[0]));
                        timePicker.setMinute(Integer.valueOf(offtime[1]));
                    } else {
                        timePicker.setCurrentHour(Integer.valueOf(offtime[0]));
                        timePicker.setCurrentMinute(Integer.valueOf(offtime[1]));
                    }
                }
                linearLayout_timePicker.setVisibility(View.VISIBLE);
                button_set.setVisibility(View.VISIBLE);
            }
            mcheckmode = type;
        } else {
            linearLayout_time.setVisibility(View.VISIBLE);
            linearLayout_timePicker.setVisibility(View.GONE);
            mcheckmode = DEFAULT;
            button_set.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //시작 시간을 누르면
            case R.id.tv_starttime:
                setViewVisible(START_TIME, true);
                //타임피커에 스타트 시간 설정
                break;
            //종료시간을 누르면
            case R.id.tv_endtime:
                setViewVisible(END_TIME, true);
                //타임피커에 종료시간 설정

                break;
            case R.id.btn_close:
                if (linearLayout_timePicker.getVisibility() == View.VISIBLE) {
                    setViewVisible(DEFAULT, false);

                } else {
                    dismiss();
                }
                break;
            case R.id.btn_set:
                if (mcheckmode == START_TIME) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        PreferenceHelper.putString(PreferenceHelper.ontime, timePicker.getHour() + ":" + timePicker.getMinute());
                    } else {
                        PreferenceHelper.putString(PreferenceHelper.ontime, timePicker.getCurrentHour() + ":" + timePicker.getCurrentMinute());
                    }

                    starttime = PreferenceHelper.getString(PreferenceHelper.ontime, "9:00").split(":");
                    textView_start_time.setText(String.format("%d : %s %s", Integer.valueOf(starttime[0]) % 12, starttime[1].length() == 1 ? "0" + starttime[1] : starttime[1], ((Integer.valueOf(starttime[0]) >= 12) ? getString(R.string.PM) : getString(R.string.AM))));


                } else if (mcheckmode == END_TIME) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        PreferenceHelper.putString(PreferenceHelper.offtime, timePicker.getHour() + ":" + timePicker.getMinute());
                    } else {
                        PreferenceHelper.putString(PreferenceHelper.offtime, timePicker.getCurrentHour() + ":" + timePicker.getCurrentMinute());
                    }

                    offtime = PreferenceHelper.getString(PreferenceHelper.offtime, "18:00").split(":");
                    textView_end_time.setText(String.format("%d : %s %s", Integer.valueOf(offtime[0]) % 12, offtime[1].length() == 1 ? "0" + offtime[1] : offtime[1], ((Integer.valueOf(offtime[0]) >= 12) ? getString(R.string.PM) : getString(R.string.AM))));

                }

                setViewVisible(DEFAULT, false);

        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        //켜짐알람을 등록
        if (isUseOnScreen) {
            //등록 되어 있으면 수정
            AlarmHelper.register_ScreenOn_Alarm(getContext());

        } else {
            //켜짐 알람 삭제
            AlarmHelper.Unregister_ScreenOn_Alarm(getContext());
        }

        //꺼짐 알람 등록
        if (isUseOffScreen) {
            AlarmHelper.register_ScreenOff_Alarm(getContext());
        } else {
            //꺼지면 알람 삭제
            AlarmHelper.Unregister_ScreenOff_Alarm(getContext());
        }
    }

    public interface OnTimeRangeSelectedListener {
        void onTimeRangeSelected(int startHour, int startMin, int endHour, int endMin);
    }


}
