package com.jiransoft.officedisplay.scheduler;

import android.app.ActivityManager;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by user on 2016-01-15.
 */
public class AlarmReceiver extends BroadcastReceiver {

    /*
     * casenumber 0 = 서비스 종료
     * casenumber 1 = 한번만 실행
     * casenumber 2 = 주간 반복 실행
     * casenumber 3 = 화면 끄기 알람
     * casenumber 4 = 화면 켜기 알람
     * */
    public static String ALRAM_ACTION = "casenumber";
    public static int REGISTER_SCREENOFF_ALARM = 12391;
    public static int REGISTER_SCREENON_ALARM = 37483;

    public static int SCREENON = 4;
    public static int SCREENOFF = 3;
    public DevicePolicyManager devicePolicyManager;
    ComponentName adminComponent;


    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extra = intent.getExtras();
        String action = intent.getAction();
        int casenumber = extra.getInt(ALRAM_ACTION, 0);
        adminComponent = new ComponentName(context, DeviceAdmReceiver.class);

        switch (casenumber) {
            case 3:
//                Dlog.d( "스크린이 꺼짐- 알람 실행.");
                devicePolicyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
                if (devicePolicyManager.isAdminActive(adminComponent)) {
                    devicePolicyManager.lockNow();
                }
                AlarmHelper.register_ScreenOff_Alarm(context);
                break;
            case 4:
//                Dlog.d( "스크린이 켜짐- 알람 실행.");
                PushWakeLock.acquireCpuWakeLock(context);
                PushWakeLock.releaseCpuLock();
                AlarmHelper.register_ScreenOn_Alarm(context);

                CurrentApplicationPackageRetriever currentApplicationPackageRetriever = new CurrentApplicationPackageRetriever(context);
                currentApplicationPackageRetriever.get();

                break;
        }
    }

    public class CurrentApplicationPackageRetriever {

        private final Context context;

        public CurrentApplicationPackageRetriever(Context context) {
            this.context = context;
        }

        public String[] get() {
            if (Build.VERSION.SDK_INT < 21)
                return getPreLollipop();
            else
                return getLollipop();
        }

        private String[] getPreLollipop() {
            @SuppressWarnings("deprecation")
            List<ActivityManager.RunningTaskInfo> tasks =
                    activityManager().getRunningTasks(1);
            ActivityManager.RunningTaskInfo currentTask = tasks.get(0);
            ComponentName currentActivity = currentTask.topActivity;
            return new String[]{currentActivity.getPackageName()};
        }

        private String[] getLollipop() {
            final int PROCESS_STATE_TOP = 2;

            try {
                Field processStateField = ActivityManager.RunningAppProcessInfo.class.getDeclaredField("processState");

                List<ActivityManager.RunningAppProcessInfo> processes =
                        activityManager().getRunningAppProcesses();
                for (ActivityManager.RunningAppProcessInfo process : processes) {
                    if (
                        // Filters out most non-activity processes
                            process.importance <= ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND
                                    &&
                                    // Filters out processes that are just being
                                    // _used_ by the process with the activity
                                    process.importanceReasonCode == 0
                            ) {
                        int state = processStateField.getInt(process);

                        if (state == PROCESS_STATE_TOP)
						/*
                         If multiple candidate processes can get here,
                         it's most likely that apps are being switched.
                         The first one provided by the OS seems to be
                         the one being switched to, so we stop here.
                         */
                            return process.pkgList;
                    }
                }
            } catch (NoSuchFieldException | IllegalAccessException e) {
                throw new RuntimeException(e);
            }

            return new String[]{};
        }

        private ActivityManager activityManager() {
            return (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        }

    }

}