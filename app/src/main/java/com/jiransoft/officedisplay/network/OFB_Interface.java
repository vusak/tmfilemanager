package com.jiransoft.officedisplay.network;

import com.google.gson.JsonObject;

import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import rx.Observable;

/**
 * Created by user on 2015-04-30.
 */
public interface OFB_Interface {

    /**
     * 로그인
     *
     * @param lang
     * @param code
     * @param id
     * @param password
     * @param service
     * @param service_key
     * @return
     */
    @FormUrlEncoded
    @POST("openapi/jauth/token")
    Observable<JsonObject> Login(@Field("lang") String lang, @Field("code") String code, @Field("id") String id, @Field("password") String password, @Field("service") String service, @Field("service_key") String service_key);

    /**
     * 로그인 정보
     *
     * @param lang
     * @return
     */
    @FormUrlEncoded
    @POST("app/user/getLoginInfo")
    Observable<JsonObject> LoginInfo(@Header("access_token") String token,
                                     @Header("device_id") String device_id,
                                     @Header("app_version") String app_version,
                                     @Header("device_name") String device_name,
                                     @Header("os_version") String os_version,
                                     @Field("lang") String lang);

    /**
     * 사용량 정보
     *
     * @return
     */
    @GET("app/user/diskinfo")
    Observable<JsonObject> Diskinfo();

    /**
     * 폴더 리스트를 불러 온다,
     *
     * @param folder_id
     * @param sort
     * @return
     */
    @FormUrlEncoded
    @POST("app/item/lists/{folder_id}")
    Observable<JsonObject> GetFolderList(@Header("access_token") String token,
                                         @Header("device_id") String device_id,
                                         @Header("app_version") String app_version,
                                         @Header("device_name") String device_name,
                                         @Header("os_version") String os_version,
                                         @Path("folder_id") String folder_id, @Field("sort") String sort);

    /**
     * 특정 폴더의 이미지 리스트를 불러온다.
     *
     * @param folder_id
     * @param limit
     * @return
     */
    @GET("app/image/lists/{folder_id}")
    Observable<JsonObject> GetBoxImageList(@Header("access_token") String token,
                                           @Header("device_id") String device_id,
                                           @Header("app_version") String app_version,
                                           @Header("device_name") String device_name,
                                           @Header("os_version") String os_version,
                                           @Path("folder_id") String folder_id, @Query("limit") String limit);

    /**
     * 파일 업로드
     *
     * @return
     */
    @Multipart
    @POST("app/item/upload")
    Observable<JsonObject> FileUpload(@PartMap Map<String, RequestBody> params);
//    Observable<JsonObject> FileUpload(@Part("node") String node, @Part("uploadkey") String uploadkey, @Part("Filedata") RequestBody typedFile);

    /**
     * 파일 다운로드
     *
     * @param file_id
     * @return
     */
    @GET("app/item/download/{file_id}")
    @Streaming
    Observable<ResponseBody> downloadfile(@Header("access_token") String token,
                                          @Header("device_id") String device_id,
                                          @Header("app_version") String app_version,
                                          @Header("device_name") String device_name,
                                          @Header("os_version") String os_version,
                                          @Path("file_id") String file_id);

    /**
     * 파일 다운로드
     *
     * @param file_id
     * @return
     */
    @GET("app/item/image/{file_id}/1000")
    @Streaming
    Observable<ResponseBody> downloadimagefile(@Header("access_token") String token,
                                               @Header("device_id") String device_id,
                                               @Header("app_version") String app_version,
                                               @Header("device_name") String device_name,
                                               @Header("os_version") String os_version, @Path("file_id") String file_id);

}
