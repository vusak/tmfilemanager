package com.jiransoft.officedisplay.network;

import android.os.Handler;
import android.os.Message;

import com.jiransoft.officedisplay.models.downloads.DownloadPage;
import com.jiransoft.officedisplay.utils.TextUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by user on 2016-10-13.
 */

public class DownloadUtils {

    public static Observable<DownloadPage> saveInputStreamToDirectory(ResponseBody responseBody, DownloadPage downloadPage, Handler han) {

        String fileDirectory = downloadPage.getDirectory();
        String fileName = downloadPage.getName();
        String fileType = downloadPage.getExt();

        return Observable.create(new Observable.OnSubscribe<DownloadPage>() {
            @Override
            public void call(Subscriber<? super DownloadPage> subscriber) {
                try {
                    //Log.d("saveInputStreamToDirectory", "call: saveInputStreamToDirectory name." + " : " + fileName + "." + fileType);
                    //subscriber.onNext(saveInputStreamToDirectory(responseBody, fileDirectory, fileName + "." + fileType, downloadPage));
                    subscriber.onNext(saveInputStreamToDirectory(responseBody, fileDirectory, fileName, downloadPage, han));
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static DownloadPage saveInputStreamToDirectory(ResponseBody responseBody, String directory, String name, DownloadPage downloadPage, Handler han) throws IOException {
        File fileDirectory = new File(directory);
        if (!fileDirectory.exists()) {
            if (!fileDirectory.mkdirs()) {
                throw new IOException("Failed Creating  Directory");
            }
        }

        File writeFile = new File(directory + name);
        if (writeFile.exists()) {
            if (writeFile.delete()) {
                writeFile = new File(directory + name);
            } else {
                throw new IOException("Failed Deleting Existing File for Overwrite");
            }
        }


        try {
            String totalFileSize;
            int count;
            byte data[] = new byte[1024 * 4];
            long fileSize = responseBody.contentLength();
            InputStream bis = new BufferedInputStream(responseBody.byteStream(), 1024 * 8);
            OutputStream output = new FileOutputStream(writeFile);
            long total = 0;
            long startTime = System.currentTimeMillis();
            int timeCount = 1;
            while ((count = bis.read(data)) != -1) {

                total += count;
                totalFileSize = TextUtils.sizeConverter(String.valueOf(fileSize));
                String current = TextUtils.sizeConverter(String.valueOf(total));

                int progress = (int) ((total * 100) / fileSize);
                long currentTime = System.currentTimeMillis();

                if (han != null) {
                    if (currentTime - startTime > 1000 * timeCount) {
                        //EventBus.getDefault().post(new FileDownloadEvent(totalFileSize + "/ " + current, String.valueOf(progress)));
                        Message msg = new Message();
                        msg.arg1 = progress;
                        han.sendMessage(msg);
                        startTime = currentTime;
                    }
                }
                output.write(data, 0, count);
            }
            output.flush();
            output.close();
            bis.close();


        } catch (Exception e) {
            responseBody.close();
            throw e;
        }

        return downloadPage;
    }
}
