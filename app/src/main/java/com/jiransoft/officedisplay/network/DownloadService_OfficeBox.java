package com.jiransoft.officedisplay.network;

import jiran.com.tmfilemanager.network.JMF_RestfulAdapter;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import rx.Observable;
import rx.Subscriber;

public class DownloadService_OfficeBox {
    public static final int CONNECT_TIMEOUT = 10;
    public static final int WRITE_TIMEOUT = 10;
    public static final int READ_TIMEOUT = 30;

    private static DownloadService_OfficeBox sInstance;

    private OkHttpClient mClient;

    private DownloadService_OfficeBox() {
        mClient = JMF_RestfulAdapter.getClient();
    }


    public static DownloadService_OfficeBox getTemporaryInstance() {
        return new DownloadService_OfficeBox();
    }

    /**
     * @param parentUrl : 프로젝트 아이디
     * @param url       : file 아이디
     * @return
     */
    public Observable<Response> getResponse_OFFICEBOX(final String parentUrl, final String url) {
        return Observable.create(new Observable.OnSubscribe<Response>() {
            @Override
            public void call(Subscriber<? super Response> subscriber) {
                try {
                    Request request = new Request.Builder()
                            .url(JMF_RestfulAdapter.getOriginalUrl(url))
                            .header("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64)")
                            .build();

                    subscriber.onNext(mClient.newCall(request).execute());
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }
}
