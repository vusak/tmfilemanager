package com.jiransoft.officedisplay.utils;

import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import de.greenrobot.event.EventBus;
import jiran.com.tmfilemanager.MyApplication;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.io.FileSystem;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;
import rx.Observable;
import rx.Subscriber;

public class DiskUtils {
    private static final Pattern DIR_SEPORATOR = Pattern.compile("/");

    private DiskUtils() {
        throw new AssertionError();
    }

    // http://stackoverflow.com/questions/13976982/removable-storage-external-sdcard-path-by-manufacturers
    // http://stackoverflow.com/questions/11281010/how-can-i-get-external-sd-card-path-for-android-4-0
    public static String[] getStorageDirectories() {
        final Set<String> storageDirectories = new HashSet<String>();

        storageDirectories.add(MyApplication.getInstance().getFilesDir().getAbsolutePath());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            File[] directories = MyApplication.getInstance().getExternalFilesDirs(null);
            if (directories != null) {
                for (File storage : directories) {
                    if (storage != null) {
                        storageDirectories.add(storage.getAbsolutePath());
                    }
                }
            }
        } else {
            final String rawExternalStorage = System.getenv("EXTERNAL_STORAGE");
            final String rawSecondaryStoragesStr = System.getenv("SECONDARY_STORAGE");
            final String rawEmulatedStorageTarget = System.getenv("EMULATED_STORAGE_TARGET");

            if (TextUtils.isEmpty(rawEmulatedStorageTarget)) {
                if (TextUtils.isEmpty(rawExternalStorage)) {
                    storageDirectories.add("/storage/sdcard0" + File.separator + MyApplication.getInstance().getPackageName());
                } else {
                    storageDirectories.add(rawExternalStorage + File.separator + MyApplication.getInstance().getPackageName());
                }
            } else {
                final String rawUserId;

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    rawUserId = "";
                } else {
                    final String path = Environment.getExternalStorageDirectory().getAbsolutePath();
                    final String[] folders = DIR_SEPORATOR.split(path);
                    final String lastFolder = folders[folders.length - 1];
                    boolean isDigit = false;

                    try {
                        Integer.valueOf(lastFolder);
                        isDigit = true;
                    } catch (NumberFormatException e) {
                        // Do Nothing.
                    }

                    rawUserId = isDigit ? lastFolder : "";
                }

                if (TextUtils.isEmpty(rawUserId)) {
                    storageDirectories.add(rawEmulatedStorageTarget + File.separator + MyApplication.getInstance().getPackageName());
                } else {
                    storageDirectories.add(rawEmulatedStorageTarget + File.separator + rawUserId + File.separator + MyApplication.getInstance().getPackageName());
                }
            }

            if (!TextUtils.isEmpty(rawSecondaryStoragesStr)) {
                String[] rawSecondaryStorages = rawSecondaryStoragesStr.split(File.pathSeparator);
                for (int index = 0; index < rawSecondaryStorages.length; index++) {
                    storageDirectories.add(rawSecondaryStorages[index] + File.separator + MyApplication.getInstance().getPackageName());
                }
            }
        }

        return storageDirectories.toArray(new String[storageDirectories.size()]);
    }

    public static String hashKeyForDisk(String key) {
        String cacheKey;
        try {
            final MessageDigest mDigest = MessageDigest.getInstance("MD5");
            mDigest.update(key.getBytes());
            cacheKey = bytesToHexString(mDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            cacheKey = String.valueOf(key.hashCode());
        }
        return cacheKey;
    }

    private static String bytesToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public static File saveInputStreamToDirectory(Response response, String directory, String name) throws IOException {
        File fileDirectory = new File(directory);
        if (!fileDirectory.exists()) {
            if (!fileDirectory.mkdirs()) {
                throw new IOException("Failed Creating  Directory");
            }
        }

        File writeFile = new File(fileDirectory, name);
        if (writeFile.exists()) {
            if (writeFile.delete()) {
                writeFile = new File(fileDirectory, name);
            } else {
                throw new IOException("Failed Deleting Existing File for Overwrite");
            }
        }

        try {
            BufferedSink sink = Okio.buffer(Okio.sink(writeFile));
            sink.writeAll(response.body().source());
            sink.close();
        } catch (Exception e) {
            response.body().close();
            throw e;
        }

        return writeFile;
    }

    public static com.jiransoft.officedisplay.models.downloads.DownloadPage saveInputStreamToDirectory(ResponseBody responseBody, String directory, String name, com.jiransoft.officedisplay.models.downloads.DownloadPage downloadPage) throws IOException {
        File fileDirectory = new File(directory);
        if (!fileDirectory.exists()) {
            if (!fileDirectory.mkdirs()) {
                throw new IOException("Failed Creating  Directory");
            }
        }

        File writeFile = new File(fileDirectory, name);
        if (writeFile.exists()) {
            if (writeFile.delete()) {
                writeFile = new File(fileDirectory, name);
            } else {
                throw new IOException("Failed Deleting Existing File for Overwrite");
            }
        }


        try {
            String totalFileSize;
            int count;
            byte data[] = new byte[1024 * 4];
            long fileSize = responseBody.contentLength();
            InputStream bis = new BufferedInputStream(responseBody.byteStream(), 1024 * 8);
            OutputStream output = new FileOutputStream(writeFile);
            long total = 0;
            long startTime = System.currentTimeMillis();
            int timeCount = 1;
            while ((count = bis.read(data)) != -1) {

                total += count;
                totalFileSize = com.jiransoft.officedisplay.utils.TextUtils.sizeConverter(String.valueOf(fileSize));
                String current = com.jiransoft.officedisplay.utils.TextUtils.sizeConverter(String.valueOf(total));

                int progress = (int) ((total * 100) / fileSize);
                long currentTime = System.currentTimeMillis();

                if (currentTime - startTime > 1000 * timeCount) {
                    EventBus.getDefault().post(new com.jiransoft.officedisplay.controllers.events.FileDownloadEvent(totalFileSize + "/ " + current, String.valueOf(progress)));
                    startTime = currentTime;
                }
                output.write(data, 0, count);
            }
            output.flush();
            output.close();
            bis.close();


        } catch (Exception e) {
            responseBody.close();
            throw e;
        }

        return downloadPage;
    }

    public static File saveInputStreamToDirectory(String response_body, String directory, String name) throws IOException {
        File fileDirectory = new File(directory);
        if (!fileDirectory.exists()) {
            if (!fileDirectory.mkdirs()) {
                throw new IOException("Failed Creating  Directory");
            }
        }

        File writeFile = new File(fileDirectory, name);
        if (writeFile.exists()) {
            if (writeFile.delete()) {
                writeFile = new File(fileDirectory, name);
            } else {
                throw new IOException("Failed Deleting Existing File for Overwrite");
            }
        }


        BufferedSink sink = Okio.buffer(Okio.sink(writeFile));
        sink.writeUtf8(response_body);
        sink.close();

        return writeFile;
    }

    public static void deleteFiles(File inputFile) {
        if (inputFile.isDirectory()) {
            for (File childFile : inputFile.listFiles()) {
                deleteFiles(childFile);
            }
        }

        inputFile.delete();
    }


    /*설정 저장용 */
    //설정파일 저장용 코드
    public static synchronized Observable<Boolean> makeDIR(String filePath) {
//        Dlog.d( "makeDIR");
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                try {
                    boolean isSuccessful = true;
                    File file = new File(filePath);
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                    subscriber.onNext(isSuccessful);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }


    public static synchronized Observable<Boolean> MakeFile(String filepath, byte[] file_byte) {
//        Dlog.d( "MakeFile");
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                try {
                    boolean isSuccessful = true;
                    // txt 파일 생성
                    File savefile = new File(filepath);
                    BufferedSink sink = Okio.buffer(Okio.sink(savefile));
                    sink.write(file_byte);
                    sink.close();

                    subscriber.onNext(isSuccessful);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static synchronized Observable<String> ReadFile(String filepath) {
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                try {

                    File savefile = new File(filepath);
                    BufferedSource source = Okio.buffer(FileSystem.SYSTEM.source(savefile));
                    String result = source.readUtf8();
                    source.close();


                    subscriber.onNext(result);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public static String DrawableResourceURI(int Drawavle_id) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("res://com.jiran.directdisplay/");
        stringBuilder.append(Drawavle_id);
        return stringBuilder.toString();
    }
}
