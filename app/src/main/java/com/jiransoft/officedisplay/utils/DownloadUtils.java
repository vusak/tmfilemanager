package com.jiransoft.officedisplay.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Subscriber;

public class DownloadUtils {
    public static final long TIMEOUT = 100;

    public static final int FLAG_FAILED = -200;
    public static final int FLAG_PAUSED = -100;
    public static final int FLAG_PENDING = 0;
    public static final int FLAG_RUNNING = 100;
    public static final int FLAG_COMPLETED = 200;
    public static final int FLAG_NODATA = 400;

    public static final int FLAG_CANCELED = 1337;
    public static final int FLAG_CHECKNEW = 1;

    public static final int FLAG_DELETE = -7777;
    public static final int FLAG_FINISH = 9999;

    private DownloadUtils() {
        throw new AssertionError();
    }

    public static boolean isNetworkAvailable(Context context) {
        //boolean isWiFiOnly = PreferenceUtils.isWiFiOnly();
        boolean isWiFiOnly = false;

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        boolean returnvalue = false;
        if (isConnected) {
            if (isWiFiOnly) {
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                    returnvalue = true;
                }
            } else {
                returnvalue = true;
            }
        }
        return returnvalue;
    }


    public static RequestBody toRequestBody(String value) {
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), value);
        return body;
    }

    public static Observable<Boolean> ObservableServiceCheck(Context context) {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {

            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                try {
                    boolean isServiceRunning = false;

                    ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
                    for (ActivityManager.RunningServiceInfo serviceInfo : activityManager.getRunningServices(Integer.MAX_VALUE)) {
                        if (serviceInfo.service.getClassName().equals(com.jiransoft.officedisplay.controllers.downloads.DownloadService.class.getName())) {
                            isServiceRunning = true;
                            break;
                        }
                    }
                    subscriber.onNext(isServiceRunning);
                    subscriber.onCompleted();
                } catch (Throwable e) {
                    subscriber.onError(e);
                }
            }

        });
    }

    public static boolean isImageFile(String extension) {
        if (extension.equalsIgnoreCase("jpg")
                || extension.equalsIgnoreCase("gif")
                || extension.equalsIgnoreCase("jpeg")
                || extension.equalsIgnoreCase("bmp")
                || extension.equalsIgnoreCase("png")) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isMovieFile(String extension) {
        if (extension.equalsIgnoreCase("mp4")) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isPDFFile(String extension) {
        if (extension.equalsIgnoreCase("pdf")) {
            return true;
        } else {
            return false;
        }
    }


}
