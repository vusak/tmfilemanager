package com.jiransoft.officedisplay.utils;

public class NavigationUtils {
    public static int POSITION_FOLDER = 0;
    public static int POSITION_CHANNEL = 1;
    public static int POSITION_LOGOUT = 2;
    public static int POSITION_QUEUE = 3;
    public static int POSITION_SETTINGS = 4;
    public static int POSITION_ADD = 5;

    public static String FOLDER = "FOLDER";
    public static String CHANNEL = "CHANNEL";
    public static String LOGOUT = "LOGOUT";
    public static String QUEUE = "QUEUE";
    public static String SETTINGS = "SETTINGS";
    public static String ADD = "ADD";

    private NavigationUtils() {
        throw new AssertionError();
    }
}
