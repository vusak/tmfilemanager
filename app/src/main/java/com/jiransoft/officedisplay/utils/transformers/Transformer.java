package com.jiransoft.officedisplay.utils.transformers;

/**
 * Created by user on 2015-06-14.
 */
public enum Transformer {
    Default("Default"),
    Accordion("Accordion"),
    Background2Foreground("Background2Foreground"),
    CubeIn("CubeIn"),
    DepthPage("DepthPage"),
    Fade("Fade"),
    FlipHorizontal("FlipHorizontal"),
    FlipPage("FlipPage"),
    Foreground2Background("Foreground2Background"),
    RotateDown("RotateDown"),
    RotateUp("RotateUp"),
    Stack("Stack"),
    Tablet("Tablet"),
    ZoomIn("ZoomIn"),
    ZoomOutSlide("ZoomOutSlide"),
    ZoomOut("ZoomOut");

    private final String name;

    Transformer(String s) {
        name = s;
    }

    public String toString() {
        return name;
    }

    public boolean equals(String other) {
        return (other == null) ? false : name.equals(other);
    }
}

