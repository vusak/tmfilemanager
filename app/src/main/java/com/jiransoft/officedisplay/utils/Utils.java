package com.jiransoft.officedisplay.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;

import jiran.com.tmfilemanager.MyApplication;

/**
 * Created by user on 2016-10-07.
 */

public class Utils {

    public static Dialog Progress(Context ctx, String title, String contents, Boolean cancel) {
        ProgressDialog dialog = new ProgressDialog(ctx);
        // dialog.setTitle(title);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // dialog.setProgressStyle(R.drawable.my_progress_indeterminate);
        // dialog.setProgressDrawable(ctx.getResources().getDrawable(R.drawable.progressbar_custom));
        dialog.setMessage(contents);
        dialog.setIndeterminate(true);
        dialog.setCancelable(cancel);
        return dialog;
    }

    public static Boolean isImage(String ext) {
        Boolean img;
        if (ext.equalsIgnoreCase("jpg") || ext.equalsIgnoreCase("gif")
                || ext.equalsIgnoreCase("jpeg")
                || ext.equalsIgnoreCase("bmp") || ext.equalsIgnoreCase("png"))
            img = true;
        else
            img = false;

        return img;
    }

    public static Boolean isDoc(String ext) {
        Boolean doc;
        if (ext.equalsIgnoreCase("pdf") || ext.equalsIgnoreCase("ppt")
                || ext.equalsIgnoreCase("pptx") || ext.equalsIgnoreCase("doc")
                || ext.equalsIgnoreCase("docx") || ext.equalsIgnoreCase("xls")
                || ext.equalsIgnoreCase("xlsx") || ext.equalsIgnoreCase("txt")
                || ext.equalsIgnoreCase("hwp"))
            doc = true;
        else
            doc = false;

        return doc;
    }

    public static Boolean isMedia(String ext) {
        Boolean doc;
        if (ext.equalsIgnoreCase("avi") || ext.equalsIgnoreCase("mkv")
                || ext.equalsIgnoreCase("mp4") || ext.equalsIgnoreCase("mov")
                || ext.equalsIgnoreCase("mp3") || ext.equalsIgnoreCase("wma")
                || ext.equalsIgnoreCase("wmv"))
            doc = true;
        else
            doc = false;

        return doc;
    }

//    public static int getExtensionIcon(String extension) {
//
//        int bitmap;
//
//        if (extension.equalsIgnoreCase("doc") || extension.equalsIgnoreCase("docx"))
//            bitmap = R.mipmap.file_s_doc;
//        else if (extension.equalsIgnoreCase("hwp"))
//            bitmap = R.mipmap.file_s_hwp;
//        else if (extension.equalsIgnoreCase("pdf"))
//            bitmap = R.mipmap.file_s_pdf;
//        else if (extension.equalsIgnoreCase("ppt") || extension.equalsIgnoreCase("pptx"))
//            bitmap = R.mipmap.file_s_ppt;
//        else if (extension.equalsIgnoreCase("txt"))
//            bitmap = R.mipmap.file_s_txt;
//        else if (extension.equalsIgnoreCase("xls") || extension.equalsIgnoreCase("xlsx"))
//            bitmap = R.mipmap.file_s_xls;
//        else if (extension.equalsIgnoreCase("folder"))
//            bitmap = R.mipmap.file_s_folder;
//        else if (extension.equalsIgnoreCase("avi") || extension.equalsIgnoreCase("asf")
//                || extension.equalsIgnoreCase("mpg") || extension.equalsIgnoreCase("mpeg")
//                || extension.equalsIgnoreCase("wmv")
//                || extension.equalsIgnoreCase("mov") || extension.equalsIgnoreCase("flv")
//                || extension.equalsIgnoreCase("mkv") || extension.equalsIgnoreCase("mp4") || extension.equalsIgnoreCase("3pg"))
//            bitmap = R.mipmap.file_s_movie;
//        else if (extension.equalsIgnoreCase("mp3") || extension.equalsIgnoreCase("m4a")
//                || extension.equalsIgnoreCase("flac") || extension.equalsIgnoreCase("wma")
//                || extension.equalsIgnoreCase("ogg")
//                || extension.equalsIgnoreCase("wav"))
//            bitmap = R.mipmap.file_s_music;
//        else
//            bitmap = R.mipmap.file_s_etc;
//
//        return bitmap;
//    }

    public static int getNetworkState(Context context) {
        Log.i("AppUtils", "getNetworkState()..");
        int connected = 0;

        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                connected = 1;
            }

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                connected = 1;
            }
        }
        return connected;
    }

    public static boolean checkIsTablet(Context context) {
        boolean isTablet = false;
        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        float widthInches = metrics.widthPixels / metrics.xdpi;
        float heightInches = metrics.heightPixels / metrics.ydpi;
        double diagonalInches = Math.sqrt(Math.pow(widthInches, 2) + Math.pow(heightInches, 2));
        //if (diagonalInches >= 7.0) {
        if (diagonalInches >= 6.5) {
            isTablet = true;
            //((Activity)context).setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        }

        if (isTablet) {
            ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        }
//        else{
//            ((Activity) context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
//        }

        return isTablet;
    }

    public static boolean isWiFiOnly() {
        Context context = MyApplication.getInstance();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean("preference_download_wifi", true);
    }


    public static String getDownloadDirectory() {
        Context context = MyApplication.getInstance();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString("preference_storage_directory", context.getFilesDir().getAbsolutePath() + "/down");
    }
}
