package com.jiransoft.officedisplay.utils;

import android.content.Context;

import jiran.com.tmfilemanager.MyApplication;
import jiran.com.tmfilemanager.R;

public class PreferenceUtils {
    private PreferenceUtils() {
        throw new AssertionError();
    }

//	public static int getStartupScreen() {
//		Context context = MyApplication.getInstance();
//
//		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//		// Hack Fix: http://stackoverflow.com/questions/5227478/getting-integer-or-index-values-from-a-list-preference
//		return Integer.valueOf(sharedPreferences.getString(context.getString(R.string.preference_startup_key), context.getString(R.string.preference_startup_default_value)));
//	}

    public static PageChangeTransFormer.Transformer getPageAnimation() {
        Context context = MyApplication.getInstance();

        return PageChangeTransFormer.Transformer.valueOf(PreferenceHelper.getString(context.getResources().getString(R.string.preference_page_animation_key), context.getResources().getString(R.string.preference_page_animation_default_value)));
    }

    public static String getPageAnimationString() {
        Context context = MyApplication.getInstance();

        return PreferenceHelper.getString(context.getResources().getString(R.string.preference_page_animation_key), context.getResources().getString(R.string.preference_page_animation_default_value));
    }

    public static String getPageChangeTime() {
        Context context = MyApplication.getInstance();

        return PreferenceHelper.getString(context.getResources().getString(R.string.preference_page_change_time_key), context.getResources().getString(R.string.preference_page_change_time_default_value));
    }

    public static String getImageScaleType() {
        Context context = MyApplication.getInstance();

        return PreferenceHelper.getString(context.getResources().getString(R.string.preference_ScaleType_key), context.getResources().getString(R.string.preference_ScaleType_default_value));
    }

    public static boolean isVisibleTimer() {
        Context context = MyApplication.getInstance();

        return PreferenceHelper.getBoolean(context.getResources().getString(R.string.preference_visible_timer_key), true);
    }

    public static void setVisibleTimer(boolean isVisibleTimer) {
        Context context = MyApplication.getInstance();

        PreferenceHelper.putBoolean(context.getString(R.string.preference_visible_timer_key), isVisibleTimer);
    }


//
//	public static boolean isAutoLogin() {
//		Context context = MyApplication.getInstance();
//
//		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//		return sharedPreferences.getBoolean(context.getString(R.string.preference_autologin_key), false);
//	}
//
//	public static void setAutoLogin(boolean isAutoLogin) {
//		Context context = MyApplication.getInstance();
//
//		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//
//		SharedPreferences.Editor editor = sharedPreferences.edit();
//		editor.putBoolean(context.getString(R.string.preference_autologin_key), isAutoLogin);
//		editor.commit();
//	}
//
//	public static boolean isAutoStart() {
//		Context context = MyApplication.getInstance();
//
//		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//		return sharedPreferences.getBoolean(context.getString(R.string.preference_autostart_key), false);
//	}
//
//	public static void setAutoStart(boolean autoStart) {
//		Context context = MyApplication.getInstance();
//
//		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//
//		SharedPreferences.Editor editor = sharedPreferences.edit();
//		editor.putBoolean(context.getString(R.string.preference_autostart_key), autoStart);
//		editor.commit();
//	}
//
//	public static boolean isRightToLeftDirection() {
//		Context context = MyApplication.getInstance();
//
//		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//		return sharedPreferences.getBoolean(context.getString(R.string.preference_direction_key), false);
//	}
//
//	public static boolean isLockOrientation() {
//		Context context = MyApplication.getInstance();
//
//		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//		return sharedPreferences.getBoolean(context.getString(R.string.preference_orientation_key), false);
//	}
//
//	public static void setOrientation(boolean isLockOrientation) {
//		Context context = MyApplication.getInstance();
//
//		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//
//		SharedPreferences.Editor editor = sharedPreferences.edit();
//		editor.putBoolean(context.getString(R.string.preference_orientation_key), isLockOrientation);
//		editor.commit();
//	}
//
//	public static boolean isVisibleComment() {
//		Context context = MyApplication.getInstance();
//
//		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//		return sharedPreferences.getBoolean(context.getString(R.string.preference_visible_comment_key), false);
//	}
//
//	public static void setVisibleComment(boolean isVisibleComment) {
//		Context context = MyApplication.getInstance();
//
//		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//
//		SharedPreferences.Editor editor = sharedPreferences.edit();
//		editor.putBoolean(context.getString(R.string.preference_visible_comment_key), isVisibleComment);
//		editor.commit();
//	}
//
//	public static boolean isWiFiOnly() {
//		Context context = MyApplication.getInstance();
//
//		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//		return sharedPreferences.getBoolean(context.getString(R.string.preference_download_wifi_key), true);
//	}
//
//	public static boolean isExternalStorage() {
//		Context context = MyApplication.getInstance();
//
//		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//		String preferenceDirectory = sharedPreferences.getString(context.getString(R.string.preference_download_storage_key), null);
//		String internalDirectory = context.getFilesDir().getAbsolutePath();
//
//		return !preferenceDirectory.equals(internalDirectory);
//	}
//
//	public static String getDownloadDirectory() {
//		Context context = MyApplication.getInstance();
//
//		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//		return sharedPreferences.getString(context.getString(R.string.preference_download_storage_key), context.getFilesDir().getAbsolutePath() + "/down");
//	}
//
//	public static boolean isVisibleTimer() {
//		Context context = MyApplication.getInstance();
//
//		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//		return sharedPreferences.getBoolean(context.getString(R.string.preference_visible_timer_key), false);
//	}
//
//	public static void setVisibleTimer(boolean isVisibleTimer) {
//		Context context = MyApplication.getInstance();
//
//		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//
//		SharedPreferences.Editor editor = sharedPreferences.edit();
//		editor.putBoolean(context.getString(R.string.preference_visible_timer_key), isVisibleTimer);
//		editor.commit();
//	}
}
