package com.jiransoft.officedisplay.utils;

import com.jiransoft.officedisplay.utils.transformers.AccordionTransformer;
import com.jiransoft.officedisplay.utils.transformers.BackgroundToForegroundTransformer;
import com.jiransoft.officedisplay.utils.transformers.BaseTransformer;
import com.jiransoft.officedisplay.utils.transformers.CubeInTransformer;
import com.jiransoft.officedisplay.utils.transformers.DefaultTransformer;
import com.jiransoft.officedisplay.utils.transformers.DepthPageTransformer;
import com.jiransoft.officedisplay.utils.transformers.FadeTransformer;
import com.jiransoft.officedisplay.utils.transformers.FlipHorizontalTransformer;
import com.jiransoft.officedisplay.utils.transformers.FlipPageViewTransformer;
import com.jiransoft.officedisplay.utils.transformers.ForegroundToBackgroundTransformer;
import com.jiransoft.officedisplay.utils.transformers.RotateDownTransformer;
import com.jiransoft.officedisplay.utils.transformers.RotateUpTransformer;
import com.jiransoft.officedisplay.utils.transformers.StackTransformer;
import com.jiransoft.officedisplay.utils.transformers.TabletTransformer;
import com.jiransoft.officedisplay.utils.transformers.ZoomInTransformer;
import com.jiransoft.officedisplay.utils.transformers.ZoomOutSlideTransformer;
import com.jiransoft.officedisplay.utils.transformers.ZoomOutTransformer;

/**
 * Created by user on 2015-11-10.
 */
public class PageChangeTransFormer {
    public static BaseTransformer setPresetTransformer(Transformer transformer) {
        //
        // special thanks to https://github.com/ToxicBakery/ViewPagerTransforms
        //
        BaseTransformer t = null;
        switch (transformer) {
            case Default:
                t = new DefaultTransformer();
                break;
            case Accordion:
                t = new AccordionTransformer();
                break;
            case Background2Foreground:
                t = new BackgroundToForegroundTransformer();
                break;
            case CubeIn:
                t = new CubeInTransformer();
                break;
            case DepthPage:
                t = new DepthPageTransformer();
                break;
            case Fade:
                t = new FadeTransformer();
                break;
            case FlipHorizontal:
                t = new FlipHorizontalTransformer();
                break;
            case FlipPage:
                t = new FlipPageViewTransformer();
                break;
            case Foreground2Background:
                t = new ForegroundToBackgroundTransformer();
                break;
            case RotateDown:
                t = new RotateDownTransformer();
                break;
            case RotateUp:
                t = new RotateUpTransformer();
                break;
            case Stack:
                t = new StackTransformer();
                break;
            case Tablet:
                t = new TabletTransformer();
                break;
            case ZoomIn:
                t = new ZoomInTransformer();
                break;
            case ZoomOutSlide:
                t = new ZoomOutSlideTransformer();
                break;
            case ZoomOut:
                t = new ZoomOutTransformer();
                break;
        }
        return t;
    }

    public enum Transformer {
        Default, Accordion, Background2Foreground, CubeIn, DepthPage, Fade, FlipHorizontal, FlipPage, Foreground2Background, RotateDown, RotateUp, Stack, Tablet, ZoomIn, ZoomOutSlide, ZoomOut
    }

}

