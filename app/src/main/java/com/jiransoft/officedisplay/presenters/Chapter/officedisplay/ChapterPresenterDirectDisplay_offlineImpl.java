package com.jiransoft.officedisplay.presenters.Chapter.officedisplay;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineFactory;
import com.jiransoft.officedisplay.controllers.DirectDisplayManager;
import com.jiransoft.officedisplay.controllers.QueryManager;
import com.jiransoft.officedisplay.controllers.downloads.DownloadService;
import com.jiransoft.officedisplay.controllers.events.AfterinstantiateItemEvent;
import com.jiransoft.officedisplay.controllers.events.DownloadChapterQueryEvent;
import com.jiransoft.officedisplay.controllers.events.DownloadImageEvent;
import com.jiransoft.officedisplay.controllers.events.FileDownloadEvent;
import com.jiransoft.officedisplay.controllers.events.MovieControlVisibleEvent;
import com.jiransoft.officedisplay.controllers.events.MovieLoadingEvent;
import com.jiransoft.officedisplay.controllers.events.MoviePlayEvent;
import com.jiransoft.officedisplay.controllers.events.PhotoOnTabEvent;
import com.jiransoft.officedisplay.controllers.wrappers.DirectFolderRequestWrapper;
import com.jiransoft.officedisplay.models.ImageModel;
import com.jiransoft.officedisplay.models.downloads.DownloadPage;
import com.jiransoft.officedisplay.presenters.Chapter.ChapterPresenter;
import com.jiransoft.officedisplay.presenters.mapper.ChapterMapper;
import com.jiransoft.officedisplay.utils.Dlog;
import com.jiransoft.officedisplay.utils.DownloadUtils;
import com.jiransoft.officedisplay.utils.ImagePipelineConfigFactory;
import com.jiransoft.officedisplay.utils.PreferenceHelper;
import com.jiransoft.officedisplay.utils.PreferenceUtils;
import com.jiransoft.officedisplay.views.activities.Chapter.ChapterActivity;
import com.jiransoft.officedisplay.views.activities.Chapter.ChapterView;
import com.jiransoft.officedisplay.views.adapters.PagesAdapter;
import com.jiransoft.officedisplay.views.fragments.Movie.MoviePageFragment;
import com.jiransoft.officedisplay.views.fragments.Page.PageFragment;

import java.io.File;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import de.greenrobot.event.EventBus;
import jiran.com.tmfilemanager.network.JMF_RestfulAdapter;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ChapterPresenterDirectDisplay_offlineImpl implements ChapterPresenter {
    public static final String TAG = ChapterPresenterDirectDisplay_offlineImpl.class.getSimpleName();

    private static final String REQUEST_PARCELABLE_KEY = TAG + ":" + "RequestParcelableKey";
    private static final String IMAGES_PARCELABLE_KEY = TAG + ":" + "ImagesParcelableKey";

    private static final String INITIALIZED_PARCELABLE_KEY = TAG + ":" + "InitializedParcelableKey";
    private static final String POSITION_PARCELABLE_KEY = TAG + ":" + "PositionParcelableKey";
    /**
     * 이미지 목록을 다시 불러 온다.
     */
    List<DownloadPage> mDownloadPages;
    int scrollState = 0;
    int targetPage = 0;
    int prevPage = 0;
    Intent startService;
    //declare key 첫번째 프레그먼트를 찾기 위한 boolean값
//onPageSelected가 첫번째 프레그먼트 일때 fire 하지 않음.
    Boolean first = true;
    //이미지 리스트의 첫번째가 슬라이드 첫번째보다 최신 일 경우
    Boolean RefreashNewImage = false;
    //
    Boolean Loading = true;

    private ChapterView mChapterView;
    private ChapterMapper mChapterMapper;
    private PagesAdapter mPagesAdapter;
    private DirectFolderRequestWrapper mDirectFolderRequest;
    private ArrayList<ImageModel> mImageModels;
    private boolean mIsRightToLeftDirection;
    private boolean mIsLockOrientation;
    private boolean mIsVisibleComment;
    private boolean mIsVisibleTimer;
    private boolean mInitialized;
    private int mInitialPosition;

    private Subscription mSaveImageUrlsSubscription;
    private Subscription mAutoScrollViewPagerSubscription;
    private Subscription mAutoPlaySubscription;
    private Subscription mCheckServiceSubscription;
    private Subscription mCheckLastFileSubscription;
    private Subscription mCheckNewPostSubscription;
    private Subscription mReStartCheckNewPostSubscription;
    private Subscription mDeleteFileSubscription;

    public ChapterPresenterDirectDisplay_offlineImpl(ChapterView chapterView, ChapterMapper chapterMapper) {
        mChapterView = chapterView;
        mChapterMapper = chapterMapper;
    }

    @Override
    public void handleInitialArguments(Intent arguments) {
        if (arguments != null) {
            if (arguments.hasExtra(ChapterActivity.REQUEST_ARGUMENT_KEY)) {
                mDirectFolderRequest = arguments.getParcelableExtra(ChapterActivity.REQUEST_ARGUMENT_KEY);

                arguments.removeExtra(ChapterActivity.REQUEST_ARGUMENT_KEY);
            }
            if (arguments.hasExtra(ChapterActivity.POSITION_ARGUMENT_KEY)) {
                mInitialPosition = arguments.getIntExtra(ChapterActivity.POSITION_ARGUMENT_KEY, 0);

                arguments.removeExtra(ChapterActivity.POSITION_ARGUMENT_KEY);
            }
        }
    }

    @Override
    public void StartService() {
        Dlog.d("StartService: 서비스 시작");
        startService = new Intent(mChapterView.getContext(), DownloadService.class);
        startService.putExtra(DownloadService.INTENT_START_DOWNLOAD, DownloadService.INTENT_START_DOWNLOAD);
        mChapterView.getContext().startService(startService);
    }

    @Override
    public void StopService() {
        Dlog.d("StopService: 서비스 종료");
        startService.putExtra(DownloadService.INTENT_STOP_DOWNLOAD, DownloadService.INTENT_STOP_DOWNLOAD);
        mChapterView.getContext().startService(startService);
    }

    @Override
    public void initializeRealm() {
    }

    @Override
    public void initializeViews() {
        ImagePipelineFactory.initialize(ImagePipelineConfigFactory.getNewOkHttpImagePipelineConfig(mChapterView.getContext(), JMF_RestfulAdapter.getClient()));
        startService = new Intent(mChapterView.getContext(), DownloadService.class);
        mChapterView.initializeHardwareAcceleration();
        mChapterView.initializeSystemUIVisibility();
        mChapterView.initializeToolbar();
        mChapterView.initializeViewPager();
//        mChapterView.initializeEmptyRelativeLayout();
        mChapterView.DownloadDataChangeEmptyRelativeLayout();
        mChapterView.initializeButtons();
        mChapterView.initializeTextView();
    }

    @Override
    public void initializeOptions() {
        mIsRightToLeftDirection = false;//PreferenceUtils.isRightToLeftDirection();
        mIsLockOrientation = false;//PreferenceUtils.isLockOrientation();
        mIsVisibleComment = false;//PreferenceUtils.isVisibleComment();
        mIsVisibleTimer = PreferenceUtils.isVisibleTimer();

        mChapterMapper.applyIsLockOrientation(mIsLockOrientation);
        mChapterMapper.applyIsVisibleTimer(mIsVisibleTimer);
    }

    @Override
    public void initializeMenu() {
        mChapterView.setOptionOrientationText(mIsLockOrientation);
        mChapterView.setOptionVisibleCommentText(mIsVisibleComment);
        mChapterView.setOptionVisibleTimer(mIsVisibleTimer);
    }

    @Override
    public void initializeDataFromUrl(FragmentManager fragmentManager) {
        mPagesAdapter = new PagesAdapter(fragmentManager);
        mPagesAdapter.setIsRightToLeftDirection(mIsRightToLeftDirection);
        mChapterMapper.registerAdapter(mPagesAdapter);

        if (!mInitialized) {
            mChapterView.setTitleText(mDirectFolderRequest.getName());
        } else {
            mChapterView.setTitleText(mDirectFolderRequest.getName());
            if (mImageModels != null && mImageModels.size() != 0) {
                updateAdapter();
                mChapterView.hideEmptyRelativeLayout();
            }
        }
    }

    @Override
    public void registerForEvents() {
        EventBus.getDefault().register(this);
    }

    @Override
    public void unregisterForEvents() {
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void saveState(Bundle outState) {
        if (mDirectFolderRequest != null) {
            outState.putParcelable(REQUEST_PARCELABLE_KEY, mDirectFolderRequest);
        }

        if (mImageModels != null) {
            outState.putParcelableArrayList(IMAGES_PARCELABLE_KEY, mImageModels);
        }

        outState.putBoolean(INITIALIZED_PARCELABLE_KEY, mInitialized);

        outState.putInt(POSITION_PARCELABLE_KEY, mInitialPosition);
    }

    @Override
    public void restoreState(Bundle savedState) {
        if (savedState.containsKey(REQUEST_PARCELABLE_KEY)) {
            mDirectFolderRequest = savedState.getParcelable(REQUEST_PARCELABLE_KEY);

            savedState.remove(REQUEST_PARCELABLE_KEY);
        }

        if (savedState.containsKey(IMAGES_PARCELABLE_KEY)) {
            mImageModels = savedState.getParcelableArrayList(IMAGES_PARCELABLE_KEY);
            savedState.remove(IMAGES_PARCELABLE_KEY);
        }

        if (savedState.containsKey(INITIALIZED_PARCELABLE_KEY)) {
            mInitialized = savedState.getBoolean(INITIALIZED_PARCELABLE_KEY, false);

            savedState.remove(INITIALIZED_PARCELABLE_KEY);
        }
        if (savedState.containsKey(POSITION_PARCELABLE_KEY)) {
            mInitialPosition = savedState.getInt(POSITION_PARCELABLE_KEY, 0);

            savedState.remove(POSITION_PARCELABLE_KEY);
        }
    }

    @Override
    public void saveChapterToRecentChapters() {
        //화면이 꺼지면 오토슬라이드를 종료합니다.
        Dlog.d("saveChapterToRecentChapters: 화면이 꺼지면 오토슬라이드를 종료합니다.");

        if (mAutoPlaySubscription != null) {
            mAutoPlaySubscription.unsubscribe();
            mAutoPlaySubscription = null;
        }

        if (mCheckNewPostSubscription != null) {
            mCheckNewPostSubscription.unsubscribe();
            mCheckNewPostSubscription = null;
        }


        if (mCheckServiceSubscription != null) {
            mCheckServiceSubscription.unsubscribe();
            mCheckServiceSubscription = null;
        }

        if (mCheckLastFileSubscription != null) {
            mCheckLastFileSubscription.unsubscribe();
            mCheckLastFileSubscription = null;
        }

        if (mReStartCheckNewPostSubscription != null) {
            mReStartCheckNewPostSubscription.unsubscribe();
            mReStartCheckNewPostSubscription = null;
        }

        if (mDeleteFileSubscription != null) {
            mDeleteFileSubscription.unsubscribe();
            mDeleteFileSubscription = null;
        }


        StopToNextPage();
        StopService();

        if (mPagesAdapter.getmCurrentItem(getActualPosition()) instanceof MoviePageFragment) {
            ((MoviePageFragment) mPagesAdapter.getmCurrentItem(getActualPosition())).pausemovie();
        }

    }

    @Override
    public void destroyAllSubscriptions() {
        Dlog.d("destroyAllSubscriptions: 모든 서브 스크립션 종료.");

        if (mSaveImageUrlsSubscription != null) {
            mSaveImageUrlsSubscription.unsubscribe();
            mSaveImageUrlsSubscription = null;
        }

        if (mAutoPlaySubscription != null) {
            mAutoPlaySubscription.unsubscribe();
            mAutoPlaySubscription = null;
        }

        if (mCheckServiceSubscription != null) {
            mCheckServiceSubscription.unsubscribe();
            mCheckServiceSubscription = null;
        }

        if (mCheckNewPostSubscription != null) {
            mCheckNewPostSubscription.unsubscribe();
            mCheckNewPostSubscription = null;
        }

        if (mCheckLastFileSubscription != null) {
            mCheckLastFileSubscription.unsubscribe();
            mCheckLastFileSubscription = null;
        }

        if (mReStartCheckNewPostSubscription != null) {
            mReStartCheckNewPostSubscription.unsubscribe();
            mReStartCheckNewPostSubscription = null;
        }

        if (mDeleteFileSubscription != null) {
            mDeleteFileSubscription.unsubscribe();
            mDeleteFileSubscription = null;
        }

        StopToNextPage();
    }

    @Override
    public void onTrimMemory(int level) {
    }

    @Override
    public void onLowMemory() {
        Fresco.getImagePipeline().clearMemoryCaches();

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//    dragging 상태인데 target이 0이면 0번 탭에서 한창 손으로 끌고 있는 중이므로 해당.
//    settling 상태인데 target, prev 모두 0이면 드래깅하다 관둔 경우이므로 해당.
//    settling 상태인데 target=1, prev=0 이면 드래깅 충분히 해서 안착하는 경우이므로 해당.
//    Dlog.d( "onPageScrolled: targetPage :" +targetPage);
//    Dlog.d( "onPageScrolled:   :" +getActualPosition());
//    Dlog.d( "onPageScrolled: prevPage :" +prevPage);
        if (targetPage != prevPage) {
            //페이지가 넘어 갔는데 이전 페이지가 동영상 일 경우 정지
            if (mPagesAdapter.getmCurrentItem(prevPage) != null) {
                if (mPagesAdapter.getmCurrentItem(prevPage) instanceof MoviePageFragment) {
                    ((MoviePageFragment) mPagesAdapter.getmCurrentItem(prevPage)).pausemovie();
                }

                if (mPagesAdapter.getmCurrentItem(getActualPosition()) instanceof MoviePageFragment) {
                    //현재 페이지가 전체 페이지가 아니면 재생
                    if (mChapterView.IsFullScreen()) {
                        playMovie();
                    }
                }
            }
        }
    }

    @Override
    public void onPageSelected(int position) {
        //페이지가 바뀔 때마다 이리로 들어오나??
        mChapterView.setSubtitlePositionText(getActualPosition() + 1);

        Dlog.d("onPageSelected:페이지가 바뀌면 들어온다");
        //다운로드 중이 아니면
//        if (!isDownlaoding) {
//            Dlog.d( "onPageSelected: 다운로드 중이 아니면");
        if (mPagesAdapter.getmCurrentItem(getActualPosition()) instanceof MoviePageFragment) {
            Dlog.d("onPageSelected: 비디오 페이지?");
            if (mAutoScrollViewPagerSubscription != null) {
                mAutoScrollViewPagerSubscription.unsubscribe();
                mAutoScrollViewPagerSubscription = null;
            }
            if (mChapterView.IsFullScreen()) {
                Dlog.d("onPageSelected: 비디오 페이지 - 풀스크린");
                playMovie();
            } else {
                Dlog.d("onPageSelected: 비디오 페이지 - 풀스크린 아님");
                EventBus.getDefault().post(new MovieControlVisibleEvent(MovieControlVisibleEvent.FLAG_VISIBLE));
            }
        } else if (mPagesAdapter.getmCurrentItem(getActualPosition()) instanceof PageFragment) {
            Dlog.d("onPageSelected: 사진 페이지?");
            if (mChapterView.IsFullScreen()) {
                MoveNextPage(Integer.valueOf(PreferenceUtils.getPageChangeTime()));
                //MoveNextPage(10);
            } else {
            }
//            }
        }
        targetPage = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (state == ViewPager.SCROLL_STATE_SETTLING) {
            prevPage = targetPage;
        }
        scrollState = state;
    }

    @Override
    public void MoveToNextPage() {
        Dlog.d("EnableAutoScrollViewPager: start");
        if (mAutoScrollViewPagerSubscription != null) {
            mAutoScrollViewPagerSubscription.unsubscribe();
            mAutoScrollViewPagerSubscription = null;
        }

        if (mPagesAdapter != null) {
            mChapterView.initializeFullscreen();
//                Dlog.d( "mPagesAdapter: null!");
            if (mPagesAdapter.getmCurrentItem(getActualPosition()) != null) {
//                    Dlog.d( mPagesAdapter.getmCurrentItem(getActualPosition()).getTag());
                if (mPagesAdapter.getmCurrentItem(getActualPosition()) instanceof MoviePageFragment) {
                    Dlog.d("동영상");
                    playMovie();
                } else {
                    Dlog.d("사진");
                    MoveNextPage(Integer.valueOf(PreferenceUtils.getPageChangeTime()));
                    //MoveNextPage(10);
                }
            } else {
                Dlog.d("EnableAutoScrollViewPager: mPagesAdapter.getmCurrentItem(getActualPosition()) is null ");
                Dlog.d("MoveToNextPage: 리스트에 값이 없으면 로컬에 저장된 목록을 찾음 ");
                LocalDownloadfile();
            }
        }
    }

    @Override
    public void onPreviousClick() {
        if (mChapterMapper.getPosition() == 0) {
            mChapterView.toastNoPreviousChapter();
        } else {
            mChapterMapper.setPosition(mChapterMapper.getPosition() - 1, false);
        }
    }

    @Override
    public void onNextClick() {
        if (mPagesAdapter.getCount() == mChapterMapper.getPosition() + 1) {
            mChapterView.toastNoNextChapter();
        } else {
            mChapterMapper.setPosition(mChapterMapper.getPosition() + 1, false);
        }
    }

    @Override
    public void onOptionParent() {
        mChapterView.finishAndLaunchActivity(null, true);
    }

    @Override
    public void onOptionRefresh() {
        if (!mInitialized) {
            LocalDownloadfile();
        } else {
            mPagesAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onOptionOrientation() {
        if (mInitialized) {
            mIsLockOrientation = !mIsLockOrientation;

            mChapterMapper.applyIsLockOrientation(mIsLockOrientation);

            mChapterView.setOptionOrientationText(mIsLockOrientation);

            //PreferenceUtils.setOrientation(mIsLockOrientation);
        } else {
            mChapterView.toastNotInitializedError();
        }
    }

    @Override
    public void onOptionVisibleComment() {
        if (mInitialized) {
            mIsVisibleComment = !mIsVisibleComment;
            mChapterView.setOptionVisibleCommentText(mIsVisibleComment);
            //PreferenceUtils.setVisibleComment(mIsVisibleComment);
        } else {
            mChapterView.toastNotInitializedError();
        }
    }

    @Override
    public void onOptionHelp() {
//		if (((FragmentActivity) mChapterView.getContext()).getSupportFragmentManager().findFragmentByTag(ChapterHelpFragment.TAG) == null) {
//			ChapterHelpFragment chapterHelpFragment = new ChapterHelpFragment();
//
//			chapterHelpFragment.show(((FragmentActivity) mChapterView.getContext()).getSupportFragmentManager(), ChapterHelpFragment.TAG);
//		}
    }

    @Override
    public void onOptionVisibleTimer() {
        if (mInitialized) {
            mIsVisibleTimer = !mIsVisibleTimer;

            mChapterMapper.applyIsVisibleTimer(mIsVisibleTimer);

            mChapterView.setOptionVisibleTimer(mIsVisibleTimer);

            PreferenceUtils.setVisibleTimer(mIsVisibleTimer);
        } else {
            mChapterView.toastNotInitializedError();
        }
    }

    @Override
    public void StopToNextPage() {
        Dlog.d("StopToNextPage: Stop");
        if (mAutoScrollViewPagerSubscription != null) {
            mAutoScrollViewPagerSubscription.unsubscribe();
            mAutoScrollViewPagerSubscription = null;
        }
    }

    /**
     * 로컬에 저장된 리스트를 불러온다.
     */
    private void LocalDownloadfile() {
        Dlog.d("LocalDownloadfile: start");
        Loading = false;
        first = true;

        if (mSaveImageUrlsSubscription != null) {
            mSaveImageUrlsSubscription.unsubscribe();
            mSaveImageUrlsSubscription = null;
        }

        if (mDirectFolderRequest != null) {

            final DirectFolderRequestWrapper downloadRequest = new DirectFolderRequestWrapper(mDirectFolderRequest.getSource(), mDirectFolderRequest.getUrl(), "");
//            mSaveImageUrlsSubscription
            Observable<List<DownloadPage>> CurrentDownloadedImageObservable;
            if (DownloadUtils.isNetworkAvailable(mChapterView.getContext())) {
                CurrentDownloadedImageObservable = Observable.zip(
                        QueryManager.queryDownloadedPagesOfDownloadChapter(mDirectFolderRequest, DownloadUtils.FLAG_COMPLETED)
//                        QueryManager.queryDownloadPagesOfDownloadChapter(mDirectFolderRequest)
                        , DirectDisplayManager.pullImageUrlsFromNetwork(downloadRequest).toList(), (cursor, imageModels) -> {
                            try {
                                List<DownloadPage> CurrentDownloadedImage = new ArrayList<>();
                                if (cursor != null) {
                                    List<DownloadPage> localdownloadlist = QueryManager.toList(cursor, DownloadPage.class);
                                    for (DownloadPage downloadPage : localdownloadlist) {
                                        for (ImageModel image : imageModels) {
                                            if (image.getFile_id().equals(downloadPage.getName())) {
                                                CurrentDownloadedImage.add(downloadPage);
                                                break;
                                            }
                                        }
                                        if (CurrentDownloadedImage.size() == PreferenceHelper.getInt(PreferenceHelper.slide_cnt, 10)) {
                                            break;
                                        }
                                    }
                                }

                                return CurrentDownloadedImage;
                            } finally {
                                if (cursor != null)
                                    cursor.close();
                            }
                        });
            } else {
                CurrentDownloadedImageObservable = QueryManager.queryDownloadedPagesOfDownloadChapter(mDirectFolderRequest, DownloadUtils.FLAG_COMPLETED)
                        .flatMap(cursor -> {
                            try {
                                List<DownloadPage> localdownloadlist = new ArrayList<DownloadPage>();
                                if (cursor != null) {
                                    localdownloadlist = QueryManager.toList(cursor, DownloadPage.class);
                                }
                                return Observable.just(localdownloadlist).limit(PreferenceHelper.getInt(PreferenceHelper.slide_cnt, 10));
                            } finally {
                                if (cursor != null)
                                    cursor.close();
                            }
                        });

            }

            mSaveImageUrlsSubscription = CurrentDownloadedImageObservable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(downloadPages -> {
                        mDownloadPages = downloadPages;
                        if (mDownloadPages != null && mDownloadPages.size() > 0) {
                            mImageModels = new ArrayList<>();
//                            Collections.reverse(mDownloadPages);
                            //저장된 폴더가 실제로 존재?
                            File chapterDirectory = new File(mDownloadPages.get(0).getDirectory());
                            if (chapterDirectory.exists() && chapterDirectory.isDirectory()) {
                                //이미지가 저장된 폴더에 이미지가 있는지 확인해서 실제로 있는 것만 추가
                                for (DownloadPage downloadPage : mDownloadPages) {
                                    for (File file : chapterDirectory.listFiles()) {
                                        String leftFileName = file.getName().substring(0, file.getName().lastIndexOf("."));
                                        String rightFileName = downloadPage.getName();

                                        if (leftFileName.equals(rightFileName)) {
                                            ImageModel imageModel = new ImageModel();
                                            imageModel.setFile_id("file://" + file.getPath());
                                            imageModel.setComment(downloadPage.getComment());
                                            imageModel.setExt(downloadPage.getExt());
                                            imageModel.setModified(downloadPage.getModified());
                                            mImageModels.add(imageModel);

                                            break;
                                        }

                                    }
                                }

                                if (mImageModels != null) {
                                    Dlog.d("LocalDownloadfile: 로컬에 저장된 리스트가 있음. ");
                                    Dlog.d("LocalDownloadfile: mImageModels: size : " + mImageModels.size());
                                    if (mImageModels.size() > 0) {
                                        for (ImageModel imageModel : mImageModels) {
                                            if (DownloadUtils.isImageFile(imageModel.getExt())) {
                                                PreferenceHelper.putString(mDirectFolderRequest.getSource() + mDirectFolderRequest.getUrl(), mImageModels.get(0).getFile_id());
                                                break;
                                            }
                                        }
                                    }
                                    if (mPagesAdapter != null) {
                                        mPagesAdapter.setmConnectService(DirectDisplayManager.DirectFolder);
                                        mPagesAdapter.setmIsOffline(true);
                                        mPagesAdapter.setmProjectid(mDirectFolderRequest.getUrl());
                                        mPagesAdapter.setmImageModels(mImageModels);
                                        mPagesAdapter.setIsRightToLeftDirection(mIsRightToLeftDirection);
                                        mPagesAdapter.notifyDataSetChanged();
                                    }
                                }

                                mChapterMapper.setPosition(0, false);
                                mPagesAdapter.notifyDataSetChanged();

                                mChapterView.hideEmptyRelativeLayout();

                                mChapterView.setTitleText(mDirectFolderRequest.getName());

                                mChapterView.setSubtitlePositionText(getActualPosition() + 1);

                                mInitialized = true;
                            }
                        } else {
                            Dlog.d("LocalDownloadfile: 로컬에 저장된 리스트가 없음. ");
                            mChapterView.initializeEmptyRelativeLayout();
                            mChapterView.setSubtitlePositionText(0);
//                            mChapterView.showEmptyRelativeLayout();
                            mChapterView.disableFullscreen();
                            mImageModels = new ArrayList<>();
                            updateAdapter();
                        }
                    }, throwable -> {
//						mChapterView.toastNetWorkError();
                        if (mImageModels != null) {
                            Dlog.d("LocalDownloadfile: Errror. ");
                            Dlog.d("LocalDownloadfile: mImageModels: size : " + mImageModels.size());
                            if (mImageModels.size() > 0) {
                                for (ImageModel imageModel : mImageModels) {
                                    if (DownloadUtils.isImageFile(imageModel.getExt())) {
                                        PreferenceHelper.putString(mDirectFolderRequest.getSource() + mDirectFolderRequest.getUrl(), mImageModels.get(0).getFile_id());
                                        break;
                                    }
                                }
                            }
                            if (mPagesAdapter != null) {
                                mPagesAdapter.setmConnectService(DirectDisplayManager.DirectFolder);
                                mPagesAdapter.setmIsOffline(true);
                                mPagesAdapter.setmProjectid(mDirectFolderRequest.getUrl());
                                mPagesAdapter.setmImageModels(mImageModels);
                                mPagesAdapter.setIsRightToLeftDirection(mIsRightToLeftDirection);
                                mPagesAdapter.notifyDataSetChanged();
                            }

                            mChapterMapper.setPosition(0, false);
                            mPagesAdapter.notifyDataSetChanged();

                            mChapterView.hideEmptyRelativeLayout();

                            mChapterView.setTitleText(mDirectFolderRequest.getName());

                            mChapterView.setSubtitlePositionText(getActualPosition() + 1);

                            mInitialized = true;
                        }

                    });

        }
    }

    private void updateAdapter() {
        if (mImageModels != null && mPagesAdapter != null) {
            if (mImageModels.size() > 0) {
                for (ImageModel imageModel : mImageModels) {
                    if (DownloadUtils.isImageFile(imageModel.getExt())) {
                        PreferenceHelper.putString(mDirectFolderRequest.getSource() + mDirectFolderRequest.getUrl(), mImageModels.get(0).getFile_id());
                        break;
                    }
                }
            }
            mPagesAdapter.setmConnectService(DirectDisplayManager.OfficeBox);
            mPagesAdapter.setmIsOffline(true);
            mPagesAdapter.setmProjectid(mDirectFolderRequest.getUrl());
            mPagesAdapter.setmImageModels(mImageModels);
            mPagesAdapter.setIsRightToLeftDirection(mIsRightToLeftDirection);
            mPagesAdapter.notifyDataSetChanged();
        }
    }

    private void setPosition(int position) {
        if (mPagesAdapter != null && mPagesAdapter.getCount() != 0) {
            if (position >= 0 && position <= mPagesAdapter.getCount() - 1) {
                int currentPosition = position;

                if (mIsRightToLeftDirection) {
                    currentPosition = mPagesAdapter.getCount() - currentPosition - 1;
                }

                mChapterMapper.setPosition(currentPosition, true);
            }
        }
    }

    private int getActualPosition() {
        int currentPosition = mChapterMapper.getPosition();

        if (mPagesAdapter != null && mPagesAdapter.getCount() != 0) {
            if (mPagesAdapter.getIsRightToLeftDirection()) {
                currentPosition = mPagesAdapter.getCount() - currentPosition - 1;
            }
        }

        return currentPosition;
    }

    private void MoveNextPage(int duration) {
        Dlog.d("MoveNextPage: start");
        if (mAutoScrollViewPagerSubscription != null) {
            mAutoScrollViewPagerSubscription.unsubscribe();
            mAutoScrollViewPagerSubscription = null;
        }

        mAutoScrollViewPagerSubscription =
                Observable.timer(duration, TimeUnit.SECONDS)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(aLong -> {
                            Dlog.d("mAutoScrollViewPagerSubscription: subscribe");
                            if (mPagesAdapter.getCount() == (getActualPosition() + 1)) {
                                //마지막 슬라이드면 이미지를 다시 불러 온다.
                                Dlog.d("MoveNextPage: 마지막 페이지");
                                StopToNextPage();
                                if (mDeleteFileSubscription != null) {
                                    mDeleteFileSubscription.unsubscribe();
                                    mDeleteFileSubscription = null;
                                }
                                mDeleteFileSubscription = DeleteFile().subscribe(aBoolean -> {
                                    Dlog.w("파일 삭제 완료");
                                    LocalDownloadfile();
                                }, throwable -> {
                                    Dlog.w("파일 삭제 실패");
                                    LocalDownloadfile();

                                });
                            } else {
                                Dlog.d("MoveNextPage: 다음페이지 이동");
                                setPosition(getActualPosition() + 1);
                            }

                        }, throwable -> {
                        });
    }

    /**
     * 1초 후에 비디오 플레이 재생 & 컨트롤러 숨김
     */
    @Override
    public void playMovie() {
        if (mAutoPlaySubscription != null) {
            mAutoPlaySubscription.unsubscribe();
            mAutoPlaySubscription = null;
        }
        if (mPagesAdapter.getmCurrentItem(getActualPosition()) instanceof MoviePageFragment) {
            mAutoPlaySubscription = Observable.timer(1, TimeUnit.SECONDS)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(aLong -> {
                        if (mPagesAdapter.getmCurrentItem(getActualPosition()) instanceof MoviePageFragment) {
                            EventBus.getDefault().post(new MoviePlayEvent(MoviePlayEvent.FLAG_PLAY, mPagesAdapter.getmCurrentItem(getActualPosition()).getTag()));
                            EventBus.getDefault().post(new MovieControlVisibleEvent(MovieControlVisibleEvent.FLAG_INVISIBE));
                        }
                    });
        }
    }

    @Override
    public void StopMovie() {
        if (mAutoPlaySubscription != null) {
            mAutoPlaySubscription.unsubscribe();
            mAutoPlaySubscription = null;
        }
        if (mPagesAdapter.getmCurrentItem(getActualPosition()) instanceof MoviePageFragment) {
            mAutoPlaySubscription = Observable.timer(300, TimeUnit.MICROSECONDS)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(aLong -> {
                        if (mPagesAdapter.getmCurrentItem(getActualPosition()) instanceof MoviePageFragment) {
                            EventBus.getDefault().post(new MoviePlayEvent(MoviePlayEvent.FLAG_PAUSED, mPagesAdapter.getmCurrentItem(getActualPosition()).getTag()));
                            EventBus.getDefault().post(new MovieControlVisibleEvent(MovieControlVisibleEvent.FLAG_VISIBLE));
                        }
                    });
        }
    }

    @Override
    public void CheckNewPost() {
        Dlog.d("CheckNewPost: start");
        mChapterView.enableFullscreen();
        if (mImageModels == null || mImageModels.size() == 0) {
            mChapterView.DownloadDataChangeEmptyRelativeLayout();
        }

        if (mCheckNewPostSubscription != null) {
            mCheckNewPostSubscription.unsubscribe();
            mCheckNewPostSubscription = null;
        }

        mCheckNewPostSubscription =
                Observable.interval(5, 30, TimeUnit.SECONDS, Schedulers.io())
                        .flatMap(aLong1 -> {
                            return Observable.just(DownloadUtils.isNetworkAvailable(mChapterView.getContext()));
                        })
                        .flatMap(isNetworkAvailable -> {
                                    if (isNetworkAvailable) {
                                        final DirectFolderRequestWrapper downloadRequest = new DirectFolderRequestWrapper(mDirectFolderRequest.getSource(), mDirectFolderRequest.getUrl(), "");
                                        return DirectDisplayManager.pullImageUrlsFromNetwork(downloadRequest).first()
                                                .zipWith(DownloadUtils.ObservableServiceCheck(mChapterView.getContext()), (imageModel, aBoolean) -> {
                                                            Cursor NonCompletedcursor = null;
                                                            Cursor cursor = null;
                                                            try {
                                                                //SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                                SimpleDateFormat transFormat = new SimpleDateFormat("yyyy. MM. dd. HH:mm");
                                                                //새로 받을 다운로드 첫번째 파일이 이미 받은 파일인지 확인
                                                                boolean FileExist = QueryManager.queryDownloadPageExist(imageModel.getFile_id(), imageModel.getSize()).toBlocking().single();
                                                                if (FileExist) {
                                                                    Dlog.d("CheckNewPost: 다운로드 목록 최신파일이 이미 받은 파일임 ");
                                                                    NonCompletedcursor = QueryManager.queryNonCompletedDownloadChapter(mDirectFolderRequest).toBlocking().single();
                                                                    if (NonCompletedcursor != null && NonCompletedcursor.getCount() == 1) {
                                                                        Dlog.d("CheckNewPost: 중간에 받지 못한 파일이 있음");
                                                                        if (aBoolean) {
                                                                            Dlog.d("CheckNewPost:  서비스가 실행 상태");
                                                                            return false;
                                                                        } else {
                                                                            Dlog.d("CheckNewPost:  서비스가 종료 상태");
                                                                            return true;
                                                                        }
                                                                    } else {
                                                                        Dlog.d("CheckNewPost:  완료 상태가 아닌 것이 없음");
                                                                        //삭제 로직을 위해
                                                                        return true;
                                                                    }
                                                                } else {
                                                                    //
                                                                    Dlog.d("CheckNewPost:  최신 파일이 존재 하지 않음");
                                                                    if (aBoolean) {
                                                                        //서비스 o
                                                                        cursor = QueryManager.queryDownloadedPagesOfDownloadChapter(mDirectFolderRequest, DownloadUtils.FLAG_RUNNING).toBlocking().single();
                                                                        if (cursor != null && cursor.getCount() > 0) {
                                                                            List<DownloadPage> mDownloadingPage = QueryManager.toList(cursor, DownloadPage.class);
                                                                            try {
                                                                                //새로 받을 다운로드 목록 최신파일 날짜
                                                                                Date LastDownloadlistModifyDate = transFormat.parse(imageModel.getModified());
                                                                                //현재 다운로드 중인 최신파일 날짜
                                                                                Date CurrentDownloadFileModifyDate = transFormat.parse(mDownloadingPage.get(0).getModified());
                                                                                Dlog.d("CheckNewPost: 새로 받을 다운로드 목록 최신파일 날짜 : " + LastDownloadlistModifyDate);
                                                                                Dlog.d("CheckNewPost: 다운로드 중인 파일 날짜              : " + CurrentDownloadFileModifyDate);
                                                                                if (LastDownloadlistModifyDate.after(CurrentDownloadFileModifyDate)) {
                                                                                    //새로 받을 다운로드 목록이 최신
                                                                                    Dlog.d("CheckNewPost: 새로 받을 다운로드 목록이 최신");
                                                                                    StopService();
                                                                                    while (!DownloadUtils.ObservableServiceCheck(mChapterView.getContext()).toBlocking().single()) {
                                                                                        //서비스 종료 중
                                                                                        Dlog.d("CheckNewPost: 서비스 종료 중");
                                                                                    }
                                                                                    Dlog.d("CheckNewPost: 서비스 종료!!!!!!!!!!!!!!!!!!!!!");
                                                                                    return true;

                                                                                } else {
                                                                                    //다운로드 중인게 최신
                                                                                    Dlog.d("CheckNewPost: 다운로드 중인게 최신");
                                                                                    return false;
                                                                                }

                                                                            } catch (ParseException e) {
                                                                                Dlog.d("CheckNewPost: 날짜 파싱 에러");
                                                                                return false;
                                                                            }
                                                                        } else {
                                                                            //다운로드 중인 리스트가 없음.
                                                                            Dlog.d("CheckNewPost: 다운로드 중인 리스트가 없음.- 신규파일");
                                                                            return true;
                                                                        }

                                                                    } else {
                                                                        //서비스 x
                                                                        if (mImageModels != null && mImageModels.size() > 0) {
                                                                            //서비스 x, 재생되고 있는 리스트 o
                                                                            try {
                                                                                //새로 받을 다운로드 목록 최신파일 날짜
                                                                                Date LastDownloadlistModifyDate = transFormat.parse(imageModel.getModified());
                                                                                //현재 슬라이드 쇼의 첫번째 파일 날짜
                                                                                Date FirstSlideModifyDate = transFormat.parse(mImageModels.get(0).getModified()).after(transFormat.parse(mImageModels.get(mImageModels.size() - 1).getModified())) ?
                                                                                        transFormat.parse(mImageModels.get(0).getModified()) : transFormat.parse(mImageModels.get(mImageModels.size() - 1).getModified());
                                                                                Dlog.d("CheckNewPost: 새로 받을 다운로드 목록 최신파일 날짜 : " + LastDownloadlistModifyDate);
                                                                                Dlog.d("CheckNewPost: 현재 슬라이드 쇼의 첫번째 날짜       : " + FirstSlideModifyDate);
                                                                                if (LastDownloadlistModifyDate.after(FirstSlideModifyDate)) {
                                                                                    Dlog.d("CheckNewPost: 새로 받을 다운로드 목록이 최신");
                                                                                    return true;
                                                                                } else {
                                                                                    Dlog.d("CheckNewPost: 현재 슬라이드 쇼의 첫번째가 최신");
                                                                                    return false;
                                                                                }

                                                                            } catch (ParseException e) {
                                                                                Dlog.d("CheckNewPost: 날짜 파싱 오류");
                                                                                return false;
                                                                            }
                                                                        } else {
                                                                            //서비스 x, 재생되고 있는 리스트 x
                                                                            Dlog.d("CheckNewPost: 서비스 작동안함, 현재 리스트 없음");
                                                                            return true;
                                                                        }
                                                                    }
                                                                }
                                                            } finally {
                                                                if (cursor != null)
                                                                    cursor.close();
                                                                if (NonCompletedcursor != null)
                                                                    NonCompletedcursor.close();
                                                            }
                                                        }
                                                );
                                    } else {
                                        return Observable.just(false);
                                    }
                                }
                        )

                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnError(throwable1 -> {
                            throwable1.printStackTrace();
                            Dlog.d("CheckNewPost: 처리 중 에러 발생" + throwable1.getMessage());
                            if (throwable1 instanceof UnknownHostException) {
                                if (mImageModels == null || mImageModels.size() == 0) {
                                    LocalDownloadfile();
                                }
                            }
                            if (throwable1.getMessage().equals("ImageList size is Zero")) {
                                mChapterView.initializeEmptyRelativeLayout();
                                mChapterView.setSubtitlePositionText(0);
                                mChapterView.showEmptyRelativeLayout();
                                mChapterView.disableFullscreen();
                                mImageModels = new ArrayList<>();
                                updateAdapter();
                            } else {
                                mChapterView.toastChapterError();
                            }
                            Dlog.v("30초 후에  CheckNewPost() 시작");
                            if (mReStartCheckNewPostSubscription != null) {
                                mReStartCheckNewPostSubscription.unsubscribe();
                                mReStartCheckNewPostSubscription = null;
                            }
                            mReStartCheckNewPostSubscription = Observable.timer(30, TimeUnit.SECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe(aLong ->
                            {
                                CheckNewPost();
                            });
                        })
                        .subscribe(aBoolean -> {
                                    Cursor cursor = null;
                                    try {
                                        Dlog.d("CheckNewPost: 결과값 리턴 :" + aBoolean);
                                        if (aBoolean) {

                                            RefreashNewImage = true;
//
                                            Dlog.d("CheckNewPost: 새로 받을 파일이 있는지 확인한다.");
                                            ArrayList<DirectFolderRequestWrapper> chaptersToDownload = new ArrayList<>();
                                            chaptersToDownload.add(mDirectFolderRequest);
                                            startService.putExtra(DownloadService.INTENT_CHECKNEW_DOWNLOAD, chaptersToDownload);
                                            mChapterView.getContext().startService(startService);
                                        } else {
                                            if (DownloadUtils.ObservableServiceCheck(mChapterView.getContext()).toBlocking().single()) {
                                                cursor = QueryManager.queryDownloadedPagesOfDownloadChapter(mDirectFolderRequest, DownloadUtils.FLAG_RUNNING).toBlocking().single();
                                                if (cursor != null && cursor.getCount() > 0) {
                                                    Dlog.d("다운로드 중인 파일이 있음. - 서비스 대기");
                                                } else {
                                                    Dlog.d(" * 다운로드 목록이 없을 경우 서비스가 실행 중으로 남아 있으므로\n" +
                                                            "                     * 실행 여부를 확인 후 중지한다.\n" +
                                                            "                     */");
                                                    if (mImageModels == null || mImageModels.size() == 0) {
                                                        mChapterView.toastChapterError();
                                                    }
                                                    StopService();
                                                }
                                            } else {
                                                if (mImageModels == null || mImageModels.size() == 0) {
                                                    mChapterView.initializeEmptyRelativeLayout();
                                                    mChapterView.showEmptyRelativeLayout();
//													mChapterView.toastNetWorkError();
                                                }
                                            }
                                        }
                                    } finally {
                                        if (cursor != null)
                                            cursor.close();
                                    }
                                }, throwable -> {

//                            mChapterView.showEmptyRelativeLayout();
//                            mChapterView.disableFullscreen();
//                            mImageModels = new ArrayList<>();
//                            updateAdapter();
                                }
                        );
    }

    public Observable<Boolean> DeleteFile() {
        return QueryManager.queryDownloadedPagesOfDownloadChapter(mDirectFolderRequest, DownloadUtils.FLAG_DELETE)
                .flatMap(cursor -> {
                    try {
                        return Observable.from(QueryManager.toList(cursor, DownloadPage.class));
                    } finally {
                        if (cursor != null)
                            cursor.close();
                    }
                })
                .flatMap(downloadPage -> {
                    String filePath = downloadPage.getDirectory() + "/" + downloadPage.getName() + "." + downloadPage.getExt();
                    File file = new File(filePath);
                    if (file.exists()) {
                        if (file.delete()) {
                            Dlog.e("File 삭제 : " + downloadPage.getName());
                            if (QueryManager.deleteDownloadPage(new DirectFolderRequestWrapper(downloadPage.getSource(), downloadPage.getName(), "")).toBlocking().single() > 0) {
                                Dlog.e("DB 삭제 : " + downloadPage.getName());
                                return Observable.just(true);
                            } else {
                                return Observable.just(false);
                            }
                        } else {
                            return Observable.just(false);
                        }
                    } else {
                        Dlog.e("File 없음");
                        if (QueryManager.deleteDownloadPage(new DirectFolderRequestWrapper(downloadPage.getSource(), downloadPage.getName(), "")).toBlocking().single() > 0) {
                            Dlog.e("DB 삭제 : " + downloadPage.getName());
                            return Observable.just(true);
                        } else {
                            return Observable.just(false);
                        }
                    }
                }).lastOrDefault(true).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    //EVENTBUS
    public void onEventMainThread(AfterinstantiateItemEvent event) {
        if (event != null) {
            if (first) {
                first = false;
                Dlog.d("onEventMainThread: AfterinstantiateItemEvent");
                MoveToNextPage();
            }
        }
    }

    //사진 중앙 터치 이밴트
    public void onEventMainThread(PhotoOnTabEvent event) {
        Dlog.d("onEventMainThread: PhotoOnTabEvent : " + event.getSelectPage());
        switch (event.getSelectPage()) {
            case Center:
                mChapterView.ToggleFullscreen();
                break;
        }
    }

    /**
     * 파일 다운로드 서비스 리턴 이벤트 버스
     *
     * @param event
     */
    public void onEventMainThread(DownloadChapterQueryEvent event) {
        Dlog.d("onEventMainThread: !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + event.getmEvent());
        switch (event.getmEvent()) {
            case DownloadUtils.FLAG_PAUSED:
                break;
            case DownloadUtils.FLAG_COMPLETED:
                /**
                 * 다운로드 목록이 없을 경우 서비스가 실행 중으로 남아 있으므로
                 * 실행 여부를 확인 후 중지한다.
                 */
                Dlog.d("onEventMainThread: DownloadUtils.FLAG_COMPLETED");
                if (DownloadUtils.ObservableServiceCheck(mChapterView.getContext()).toBlocking().single()) {
                    Dlog.d(" * 다운로드 목록이 없을 경우 서비스가 실행 중으로 남아 있으므로\n" +
                            "                     * 실행 여부를 확인 후 중지한다.\n" +
                            "                     */");
                    StopService();
//                    if(mDeleteFileSubscription != null){
//                        mDeleteFileSubscription.unsubscribe();
//                        mDeleteFileSubscription = null;
//                    }
//                    mDeleteFileSubscription = DeleteFile().subscribe(aBoolean -> {
//                        Dlog.w("파일 삭제 완료");
//                    }, throwable -> {});
                }


                if (mImageModels == null || mImageModels.size() == 0) {
                    Dlog.d("onEventMainThread: 다운로드를 모두 받았는데 현재 슬라이드가 비어 있을 경우 로컬 저장된 파일을 불러온다");
                    LocalDownloadfile();
                }
                break;
            case DownloadUtils.FLAG_RUNNING:
                Dlog.d("onEventMainThread: FLAG_RUNNING");
                if (mImageModels == null || mImageModels.size() == 0) {
                    if (RefreashNewImage) {
                        LocalDownloadfile();
                        RefreashNewImage = false;
                    }
                }

                if (mImageModels != null && mImageModels.size() > 0) {
                    //db 상의 최신 파일과 슬라이드 첫번째와 비교하여 db가 최신이면 새로고침
                    Cursor cursor = null;
                    try {
                        //SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        SimpleDateFormat transFormat = new SimpleDateFormat("yyyy. MM. dd. HH:mm");
                        cursor = QueryManager.queryDownloadedPagesOfDownloadChapter(mDirectFolderRequest, DownloadUtils.FLAG_COMPLETED).toBlocking().single();
                        if (cursor != null && cursor.getCount() > 0) {
                            List<DownloadPage> mDownloadingPage = QueryManager.toList(cursor, DownloadPage.class);
                            try {
                                //현재 다운로드 완료한 최신파일 날짜
                                Date CurrentDownloadedFileModifyDate = transFormat.parse(mDownloadingPage.get(0).getModified());
                                //현재 슬라이드
                                Date FirstSlideModifyDate = transFormat.parse(mImageModels.get(0).getModified()).after(transFormat.parse(mImageModels.get(mImageModels.size() - 1).getModified())) ?
                                        transFormat.parse(mImageModels.get(0).getModified()) : transFormat.parse(mImageModels.get(mImageModels.size() - 1).getModified());

                                Dlog.d("DownloadChapterQueryEvent: 현재 다운로드 완료한 최신파일 날짜    : " + CurrentDownloadedFileModifyDate);
                                Dlog.d("DownloadChapterQueryEvent: 현재 슬라이드 쇼의 첫번째 날짜       : " + FirstSlideModifyDate);
                                if (CurrentDownloadedFileModifyDate.after(FirstSlideModifyDate)) {
                                    LocalDownloadfile();
                                }

                            } catch (ParseException e) {
                                Dlog.d("CheckNewPost: 날짜 파싱 에러");

                            }
                        }
                    } finally {
                        if (cursor != null)
                            cursor.close();
                    }
                }

                break;
            case DownloadUtils.FLAG_NODATA:
                Dlog.d("onEventMainThread: FLAG_NODATA");
                mChapterView.initializeEmptyRelativeLayout();
                StopService();
                break;
            case DownloadUtils.FLAG_FINISH:
                Dlog.d("onEventMainThread: FLAG_FINISH");
                break;
            case DownloadUtils.FLAG_CHECKNEW:
                Dlog.d("onEventMainThread: DownloadUtils.FLAG_CHECKNEW FLAG_CHECKNEW FLAG_CHECKNEW FLAG_CHECKNEW");
                mCheckServiceSubscription = DownloadUtils.ObservableServiceCheck(mChapterView.getContext())
                        .subscribe(isRun -> {
                                    if (!isRun) {
                                        mChapterView.DownloadDataChangeEmptyRelativeLayout();
                                        StartService();
                                    }
                                }, throwable -> {
                                    StopService();
                                }
                        );
                break;
        }
    }

    //비디오 넘기기
    public void onEventMainThread(MovieLoadingEvent event) {
        if (event != null) {
            switch (event.getmEvent()) {
                case MovieLoadingEvent.FLAG_PREPARED:
                    Dlog.d("onEventMainThread: MovieLoadingEvent FLAG_PREPARED");
                    if (mChapterView.IsFullScreen()) {
                        EventBus.getDefault().post(new MoviePlayEvent(MoviePlayEvent.FLAG_PLAY, mPagesAdapter.getmCurrentItem(getActualPosition()).getTag()));
                    } else {
                        EventBus.getDefault().post(new MoviePlayEvent(MoviePlayEvent.FLAG_PAUSED, mPagesAdapter.getmCurrentItem(getActualPosition()).getTag()));
                    }
                    break;
                case MovieLoadingEvent.FLAG_COMPLETED:
                    Dlog.d("onEventMainThread: MovieLoadingEvent FLAG_COMPLETED");
                    MoveNextPage(1);
                    break;
            }

        }
    }

    public void onEventMainThread(FileDownloadEvent fileDownloadEvent) {
        mChapterView.setSubtitleProgressText(fileDownloadEvent.getCurrentSize(), fileDownloadEvent.getProgress());
    }


    //이미지 다운로드 이벤트
    public void onEventMainThread(DownloadImageEvent downloadImageEvent) {
        String s1 = downloadImageEvent.getImageUrl();

        String s2;
        if (mPagesAdapter.getItem(getActualPosition()) instanceof PageFragment) {
            Dlog.d("onEventMainThread:  instanceof PageFragment");
            s2 = ((ImageModel) mPagesAdapter.getItem(getActualPosition()).getArguments().getParcelable(PageFragment.IMAGE_ARGUMENT_KEY)).getFile_id();
        } else {
            Dlog.d("onEventMainThread:  instanceof MoviePageFragment");
            s2 = ((ImageModel) mPagesAdapter.getItem(getActualPosition()).getArguments().getParcelable(MoviePageFragment.IMAGE_ARGUMENT_KEY)).getFile_id();
        }

        if (s1.equals(s2) && downloadImageEvent.isdownload()) {
            Dlog.d("onEventMainThread: 다운로드 중");
        } else if (s1.equals(s2) && !downloadImageEvent.isdownload()) {
            Dlog.d("onEventMainThread: 다운로드 완료");
            Dlog.d("onEventMainThread: mChapterView.IsFullScreen() :" + mChapterView.IsFullScreen());

        }
    }

}


