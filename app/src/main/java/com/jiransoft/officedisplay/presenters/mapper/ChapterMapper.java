package com.jiransoft.officedisplay.presenters.mapper;

import android.support.v4.view.PagerAdapter;

public interface ChapterMapper {
    void registerAdapter(PagerAdapter adapter);

    int getPosition();

    void setPosition(int position, boolean smooth);

    void applyIsLockOrientation(boolean isLockOrientation);

    void applyIsVisibleTimer(boolean mIsVisibleTimer);
}
