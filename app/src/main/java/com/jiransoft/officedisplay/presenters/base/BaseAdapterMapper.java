package com.jiransoft.officedisplay.presenters.base;

import android.widget.BaseAdapter;

public interface BaseAdapterMapper {
    void registerAdapter(BaseAdapter adapter);
}
