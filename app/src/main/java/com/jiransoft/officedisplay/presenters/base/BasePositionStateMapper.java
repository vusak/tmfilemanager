package com.jiransoft.officedisplay.presenters.base;

import android.os.Parcelable;

public interface BasePositionStateMapper {
    Parcelable getPositionState();

    void setPositionState(Parcelable state);
}
