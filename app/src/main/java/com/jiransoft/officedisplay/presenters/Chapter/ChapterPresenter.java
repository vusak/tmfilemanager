package com.jiransoft.officedisplay.presenters.Chapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;

public interface ChapterPresenter {
    void handleInitialArguments(Intent arguments);

    void StartService();

    void StopService();

    void initializeRealm();

    void initializeViews();

    void initializeOptions();

    void initializeMenu();

    void initializeDataFromUrl(FragmentManager fragmentManager);

    void registerForEvents();

    void unregisterForEvents();

    void saveState(Bundle outState);

    void restoreState(Bundle savedState);

    void saveChapterToRecentChapters();

    void destroyAllSubscriptions();

    void onTrimMemory(int level);

    void onLowMemory();

    void onPageScrolled(int position, float positionOffset, int positionOffsetPixels);

    void onPageSelected(int position);

    void onPageScrollStateChanged(int state);

    void MoveToNextPage();

    void StopToNextPage();

    void onPreviousClick();

    void onNextClick();

    void onOptionParent();

    void onOptionRefresh();

    void onOptionOrientation();

    void onOptionVisibleComment();

    void onOptionHelp();

    void onOptionVisibleTimer();

    void playMovie();

    void StopMovie();

    void CheckNewPost();
}
