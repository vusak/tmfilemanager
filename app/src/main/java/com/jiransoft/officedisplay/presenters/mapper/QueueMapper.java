package com.jiransoft.officedisplay.presenters.mapper;

import com.jiransoft.officedisplay.presenters.base.BaseAdapterMapper;
import com.jiransoft.officedisplay.presenters.base.BasePositionStateMapper;
import com.jiransoft.officedisplay.presenters.base.BaseSelectionMapper;

public interface QueueMapper extends BaseAdapterMapper, BasePositionStateMapper, BaseSelectionMapper {
}
