package com.jiransoft.officedisplay.presenters.base;

import android.util.SparseBooleanArray;

public interface BaseSelectionMapper {
    SparseBooleanArray getCheckedItemPositions();
}
