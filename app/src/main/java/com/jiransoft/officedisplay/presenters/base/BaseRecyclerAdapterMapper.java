package com.jiransoft.officedisplay.presenters.base;

import com.jiransoft.officedisplay.views.adapters.BaseRecyclerViewAdapter;

public interface BaseRecyclerAdapterMapper {
    void registerAdapter(BaseRecyclerViewAdapter adapter);
}
